[Uno.Compiler.UxGenerated]
public partial class TopBar: Fuse.Controls.DockPanel
{
    string _field_Title;
    [global::Uno.UX.UXOriginSetter("SetTitle")]
    public string Title
    {
        get { return _field_Title; }
        set { SetTitle(value, null); }
    }
    public void SetTitle(string value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_Title)
        {
            _field_Title = value;
            OnPropertyChanged("Title", origin);
        }
    }
    global::Uno.UX.Property<string> temp_Value_inst;
    internal global::Fuse.Reactive.EventBinding temp_eb22;
    static TopBar()
    {
    }
    [global::Uno.UX.UXConstructor]
    public TopBar()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp1 = new global::Fuse.Reactive.Data("goBack");
        var temp2 = new global::Fuse.Reactive.Constant(this);
        var temp = new global::Fuse.Controls.Text();
        temp_Value_inst = new OSP_FuseControlsTextControl_Value_Property(temp, __selector0);
        var temp3 = new global::Fuse.Reactive.Property(temp2, OSP_accessor_TopBar_Title.Singleton);
        var temp4 = new global::Fuse.Controls.Rectangle();
        var temp5 = new global::Fuse.Controls.Image();
        temp_eb22 = new global::Fuse.Reactive.EventBinding(temp1);
        var temp6 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp3, Fuse.Reactive.BindingMode.Default);
        this.Color = float4(0.1137255f, 0.4901961f, 0.2588235f, 1f);
        this.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        this.Height = new Uno.UX.Size(40f, Uno.UX.Unit.Unspecified);
        this.Alignment = Fuse.Elements.Alignment.Top;
        this.SourceLineNumber = 109;
        this.SourceFileName = "profile.ux";
        temp4.Color = float4(0.2f, 0.2f, 0.2f, 1f);
        temp4.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp4.Height = new Uno.UX.Size(1f, Uno.UX.Unit.Unspecified);
        temp4.SourceLineNumber = 112;
        temp4.SourceFileName = "profile.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp4, Fuse.Layouts.Dock.Bottom);
        temp5.Width = new Uno.UX.Size(30f, Uno.UX.Unit.Unspecified);
        temp5.Alignment = Fuse.Elements.Alignment.CenterLeft;
        temp5.Margin = float4(10f, 0f, 0f, 0f);
        temp5.SourceLineNumber = 114;
        temp5.SourceFileName = "profile.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp5, Fuse.Layouts.Dock.Left);
        global::Fuse.Gestures.Clicked.AddHandler(temp5, temp_eb22.OnEvent);
        temp5.File = new global::Uno.UX.BundleFileSource(import("../../Assets/goback.png"));
        temp5.Bindings.Add(temp_eb22);
        temp1.SourceLineNumber = 114;
        temp1.SourceFileName = "profile.ux";
        temp.FontSize = 15f;
        temp.TextColor = float4(1f, 1f, 1f, 1f);
        temp.Alignment = Fuse.Elements.Alignment.Center;
        temp.Margin = float4(-20f, 0f, 0f, 0f);
        temp.SourceLineNumber = 115;
        temp.SourceFileName = "profile.ux";
        temp.Font = global::MainView.Raleway;
        temp.Bindings.Add(temp6);
        temp3.SourceLineNumber = 115;
        temp3.SourceFileName = "profile.ux";
        temp2.SourceLineNumber = 115;
        temp2.SourceFileName = "profile.ux";
        this.Children.Add(temp4);
        this.Children.Add(temp5);
        this.Children.Add(temp);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
}
