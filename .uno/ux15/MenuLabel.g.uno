[Uno.Compiler.UxGenerated]
public partial class MenuLabel: Fuse.Controls.Text
{
    static MenuLabel()
    {
    }
    [global::Uno.UX.UXConstructor]
    public MenuLabel()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp = new global::Fuse.Font(new global::Uno.UX.BundleFileSource(import("../../Assets/fonts/Lato-Bold.ttf")));
        this.FontSize = 15f;
        this.TextAlignment = Fuse.Controls.TextAlignment.Center;
        this.Color = float4(0.4666667f, 0.4666667f, 0.4666667f, 1f);
        this.Margin = float4(0f, 0f, 0f, 0f);
        this.SourceLineNumber = 17;
        this.SourceFileName = "Sidebar.ux";
        this.Font = temp;
    }
}
