[Uno.Compiler.UxGenerated]
public partial class Item: Fuse.Controls.DockPanel
{
    global::Uno.UX.Property<string> temp_Value_inst;
    global::Uno.UX.Property<string> temp1_Value_inst;
    internal global::Fuse.Controls.Rectangle underlay;
    static Item()
    {
    }
    [global::Uno.UX.UXConstructor]
    public Item()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp = new global::Fuse.Controls.Text();
        temp_Value_inst = new OSP_FuseControlsTextControl_Value_Property(temp, __selector0);
        var temp2 = new global::Fuse.Reactive.Data("title");
        var temp1 = new global::Fuse.Controls.Text();
        temp1_Value_inst = new OSP_FuseControlsTextControl_Value_Property(temp1, __selector0);
        var temp3 = new global::Fuse.Reactive.Data("date");
        var temp4 = new global::Fuse.Controls.StackPanel();
        var temp5 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp2, Fuse.Reactive.BindingMode.Default);
        var temp6 = new global::Fuse.Reactive.DataBinding(temp1_Value_inst, temp3, Fuse.Reactive.BindingMode.Default);
        var temp7 = new global::Fuse.Controls.Panel();
        var temp8 = new global::Fuse.Controls.StackPanel();
        var temp9 = new global::Fuse.Controls.Rectangle();
        var temp10 = new global::Fuse.Rotation();
        var temp11 = new global::Fuse.Controls.Rectangle();
        var temp12 = new global::Fuse.Rotation();
        underlay = new global::Fuse.Controls.Rectangle();
        this.Height = new Uno.UX.Size(56f, Uno.UX.Unit.Unspecified);
        this.SourceLineNumber = 73;
        this.SourceFileName = "dashboardlist.ux";
        temp4.Alignment = Fuse.Elements.Alignment.CenterLeft;
        temp4.Padding = float4(10f, 0f, 0f, 0f);
        temp4.SourceLineNumber = 74;
        temp4.SourceFileName = "dashboardlist.ux";
        temp4.Children.Add(temp);
        temp4.Children.Add(temp1);
        temp.TextColor = float4(0.2666667f, 0.2666667f, 0.2666667f, 1f);
        temp.Alignment = Fuse.Elements.Alignment.CenterLeft;
        temp.Margin = float4(8f, 0f, 8f, 0f);
        temp.SourceLineNumber = 75;
        temp.SourceFileName = "dashboardlist.ux";
        temp.Bindings.Add(temp5);
        temp2.SourceLineNumber = 75;
        temp2.SourceFileName = "dashboardlist.ux";
        temp1.FontSize = 9f;
        temp1.TextColor = float4(0f, 0.1333333f, 0.2313726f, 1f);
        temp1.Alignment = Fuse.Elements.Alignment.CenterLeft;
        temp1.Margin = float4(8f, 0f, 8f, 0f);
        temp1.SourceLineNumber = 76;
        temp1.SourceFileName = "dashboardlist.ux";
        temp1.Bindings.Add(temp6);
        temp3.SourceLineNumber = 76;
        temp3.SourceFileName = "dashboardlist.ux";
        temp7.HitTestMode = Fuse.Elements.HitTestMode.LocalBounds;
        temp7.Width = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        temp7.SourceLineNumber = 79;
        temp7.SourceFileName = "dashboardlist.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp7, Fuse.Layouts.Dock.Right);
        temp7.Children.Add(temp8);
        temp8.ItemSpacing = 3f;
        temp8.Width = new Uno.UX.Size(12f, Uno.UX.Unit.Unspecified);
        temp8.Alignment = Fuse.Elements.Alignment.Center;
        temp8.SourceLineNumber = 83;
        temp8.SourceFileName = "dashboardlist.ux";
        temp8.Children.Add(temp9);
        temp8.Children.Add(temp11);
        temp9.CornerRadius = float4(1f, 1f, 1f, 1f);
        temp9.Color = float4(0f, 0.1333333f, 0.2313726f, 1f);
        temp9.Height = new Uno.UX.Size(2f, Uno.UX.Unit.Unspecified);
        temp9.SourceLineNumber = 85;
        temp9.SourceFileName = "dashboardlist.ux";
        temp9.Children.Add(temp10);
        temp10.DegreesZ = 30f;
        temp10.SourceLineNumber = 86;
        temp10.SourceFileName = "dashboardlist.ux";
        temp11.CornerRadius = float4(1f, 1f, 1f, 1f);
        temp11.Color = float4(0f, 0.1333333f, 0.2313726f, 1f);
        temp11.Height = new Uno.UX.Size(2f, Uno.UX.Unit.Unspecified);
        temp11.SourceLineNumber = 88;
        temp11.SourceFileName = "dashboardlist.ux";
        temp11.Children.Add(temp12);
        temp12.DegreesZ = -30f;
        temp12.SourceLineNumber = 89;
        temp12.SourceFileName = "dashboardlist.ux";
        underlay.CornerRadius = float4(2f, 2f, 2f, 2f);
        underlay.Color = float4(1f, 1f, 1f, 1f);
        underlay.Opacity = 1f;
        underlay.Layer = Fuse.Layer.Background;
        underlay.Name = __selector1;
        underlay.SourceLineNumber = 103;
        underlay.SourceFileName = "dashboardlist.ux";
        this.Children.Add(temp4);
        this.Children.Add(temp7);
        this.Children.Add(underlay);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
    static global::Uno.UX.Selector __selector1 = "underlay";
}
