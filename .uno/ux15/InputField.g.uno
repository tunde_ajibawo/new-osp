[Uno.Compiler.UxGenerated]
public partial class InputField: Fuse.Controls.Panel
{
    string _field_PlaceHolderName;
    [global::Uno.UX.UXOriginSetter("SetPlaceHolderName")]
    public string PlaceHolderName
    {
        get { return _field_PlaceHolderName; }
        set { SetPlaceHolderName(value, null); }
    }
    public void SetPlaceHolderName(string value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_PlaceHolderName)
        {
            _field_PlaceHolderName = value;
            OnPropertyChanged("PlaceHolderName", origin);
        }
    }
    string _field_Title;
    [global::Uno.UX.UXOriginSetter("SetTitle")]
    public string Title
    {
        get { return _field_Title; }
        set { SetTitle(value, null); }
    }
    public void SetTitle(string value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_Title)
        {
            _field_Title = value;
            OnPropertyChanged("Title", origin);
        }
    }
    float4 _field_BorderColor;
    [global::Uno.UX.UXOriginSetter("SetBorderColor")]
    public float4 BorderColor
    {
        get { return _field_BorderColor; }
        set { SetBorderColor(value, null); }
    }
    public void SetBorderColor(float4 value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_BorderColor)
        {
            _field_BorderColor = value;
            OnPropertyChanged("BorderColor", origin);
        }
    }
    float4 _field_FieldColor;
    [global::Uno.UX.UXOriginSetter("SetFieldColor")]
    public float4 FieldColor
    {
        get { return _field_FieldColor; }
        set { SetFieldColor(value, null); }
    }
    public void SetFieldColor(float4 value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_FieldColor)
        {
            _field_FieldColor = value;
            OnPropertyChanged("FieldColor", origin);
        }
    }
    global::Uno.UX.Property<string> temp_Value_inst;
    global::Uno.UX.Property<float4> temp_TextColor_inst;
    global::Uno.UX.Property<string> temp1_Value_inst;
    global::Uno.UX.Property<float4> temp1_TextColor_inst;
    global::Uno.UX.Property<float4> temp2_Color_inst;
    static InputField()
    {
    }
    [global::Uno.UX.UXConstructor]
    public InputField()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp3 = new global::Fuse.Reactive.Constant(this);
        var temp = new global::Fuse.Controls.Text();
        temp_Value_inst = new OSP_FuseControlsTextControl_Value_Property(temp, __selector0);
        var temp4 = new global::Fuse.Reactive.Property(temp3, OSP_accessor_InputField_Title.Singleton);
        var temp5 = new global::Fuse.Reactive.Constant(this);
        temp_TextColor_inst = new OSP_FuseControlsTextControl_TextColor_Property(temp, __selector1);
        var temp6 = new global::Fuse.Reactive.Property(temp5, OSP_accessor_InputField_FieldColor.Singleton);
        var temp7 = new global::Fuse.Reactive.Constant(this);
        var temp1 = new global::Fuse.Controls.Text();
        temp1_Value_inst = new OSP_FuseControlsTextControl_Value_Property(temp1, __selector0);
        var temp8 = new global::Fuse.Reactive.Property(temp7, OSP_accessor_InputField_PlaceHolderName.Singleton);
        var temp9 = new global::Fuse.Reactive.Constant(this);
        temp1_TextColor_inst = new OSP_FuseControlsTextControl_TextColor_Property(temp1, __selector1);
        var temp10 = new global::Fuse.Reactive.Property(temp9, OSP_accessor_InputField_FieldColor.Singleton);
        var temp11 = new global::Fuse.Reactive.Constant(this);
        var temp2 = new global::Fuse.Controls.Rectangle();
        temp2_Color_inst = new OSP_FuseControlsShape_Color_Property(temp2, __selector2);
        var temp12 = new global::Fuse.Reactive.Property(temp11, OSP_accessor_InputField_BorderColor.Singleton);
        var temp13 = new global::Fuse.Controls.DockPanel();
        var temp14 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp4, Fuse.Reactive.BindingMode.Default);
        var temp15 = new global::Fuse.Reactive.DataBinding(temp_TextColor_inst, temp6, Fuse.Reactive.BindingMode.Default);
        var temp16 = new global::Fuse.Reactive.DataBinding(temp1_Value_inst, temp8, Fuse.Reactive.BindingMode.Default);
        var temp17 = new global::Fuse.Reactive.DataBinding(temp1_TextColor_inst, temp10, Fuse.Reactive.BindingMode.Default);
        var temp18 = new global::Fuse.Reactive.DataBinding(temp2_Color_inst, temp12, Fuse.Reactive.BindingMode.Default);
        this.BorderColor = float4(0.8352941f, 0.8352941f, 0.8352941f, 1f);
        this.FieldColor = float4(0.6666667f, 0.6666667f, 0.6666667f, 1f);
        this.Width = new Uno.UX.Size(80f, Uno.UX.Unit.Percent);
        this.Margin = float4(0f, 5f, 0f, 5f);
        this.SourceLineNumber = 118;
        this.SourceFileName = "profile.ux";
        temp13.SourceLineNumber = 124;
        temp13.SourceFileName = "profile.ux";
        temp13.Children.Add(temp);
        temp13.Children.Add(temp1);
        temp.TextWrapping = Fuse.Controls.TextWrapping.Wrap;
        temp.FontSize = 11f;
        temp.Width = new Uno.UX.Size(30f, Uno.UX.Unit.Percent);
        temp.Alignment = Fuse.Elements.Alignment.VerticalCenter;
        temp.SourceLineNumber = 126;
        temp.SourceFileName = "profile.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp, Fuse.Layouts.Dock.Left);
        temp.Font = global::MainView.Raleway;
        temp.Bindings.Add(temp14);
        temp.Bindings.Add(temp15);
        temp4.SourceLineNumber = 126;
        temp4.SourceFileName = "profile.ux";
        temp3.SourceLineNumber = 126;
        temp3.SourceFileName = "profile.ux";
        temp6.SourceLineNumber = 126;
        temp6.SourceFileName = "profile.ux";
        temp5.SourceLineNumber = 126;
        temp5.SourceFileName = "profile.ux";
        temp1.TextWrapping = Fuse.Controls.TextWrapping.Wrap;
        temp1.FontSize = 11f;
        temp1.Width = new Uno.UX.Size(60f, Uno.UX.Unit.Percent);
        temp1.Alignment = Fuse.Elements.Alignment.VerticalCenter;
        temp1.Margin = float4(20f, 0f, 0f, 0f);
        temp1.SourceLineNumber = 128;
        temp1.SourceFileName = "profile.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp1, Fuse.Layouts.Dock.Left);
        temp1.Font = global::MainView.Raleway;
        temp1.Bindings.Add(temp16);
        temp1.Bindings.Add(temp17);
        temp8.SourceLineNumber = 128;
        temp8.SourceFileName = "profile.ux";
        temp7.SourceLineNumber = 128;
        temp7.SourceFileName = "profile.ux";
        temp10.SourceLineNumber = 128;
        temp10.SourceFileName = "profile.ux";
        temp9.SourceLineNumber = 128;
        temp9.SourceFileName = "profile.ux";
        temp2.Height = new Uno.UX.Size(1f, Uno.UX.Unit.Unspecified);
        temp2.Alignment = Fuse.Elements.Alignment.Bottom;
        temp2.SourceLineNumber = 130;
        temp2.SourceFileName = "profile.ux";
        temp2.Bindings.Add(temp18);
        temp12.SourceLineNumber = 130;
        temp12.SourceFileName = "profile.ux";
        temp11.SourceLineNumber = 130;
        temp11.SourceFileName = "profile.ux";
        this.Children.Add(temp13);
        this.Children.Add(temp2);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
    static global::Uno.UX.Selector __selector1 = "TextColor";
    static global::Uno.UX.Selector __selector2 = "Color";
}
