[Uno.Compiler.UxGenerated]
public partial class SidebarIcon: Fuse.Controls.Image
{
    static SidebarIcon()
    {
    }
    [global::Uno.UX.UXConstructor]
    public SidebarIcon()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        this.Color = float4(0.1137255f, 0.4901961f, 0.2588235f, 1f);
        this.Width = new Uno.UX.Size(20f, Uno.UX.Unit.Unspecified);
        this.Height = new Uno.UX.Size(22f, Uno.UX.Unit.Unspecified);
        this.Margin = float4(0f, 24f, 0f, 9f);
        this.SourceLineNumber = 45;
        this.SourceFileName = "Sidebar.ux";
    }
}
