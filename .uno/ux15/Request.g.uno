[Uno.Compiler.UxGenerated]
public partial class Request: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router"
    };
    static Request()
    {
    }
    [global::Uno.UX.UXConstructor]
    public Request(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp1 = new global::Fuse.Controls.Grid();
        var temp2 = new global::TopBar();
        var temp3 = new global::Fuse.Controls.StackPanel();
        var temp4 = new global::Fuse.Controls.Panel();
        var temp5 = new global::SubButton();
        this.Color = float4(1f, 1f, 1f, 1f);
        this.SourceLineNumber = 1;
        this.SourceFileName = "request.ux";
        temp.LineNumber = 3;
        temp.FileName = "request.ux";
        temp.SourceLineNumber = 3;
        temp.SourceFileName = "request.ux";
        temp.File = new global::Uno.UX.BundleFileSource(import("../../js_files/navigation.js"));
        temp1.Rows = "1*,15*";
        temp1.SourceLineNumber = 5;
        temp1.SourceFileName = "request.ux";
        temp1.Children.Add(temp2);
        temp1.Children.Add(temp3);
        temp2.Title = "REQUEST";
        temp2.SourceLineNumber = 7;
        temp2.SourceFileName = "request.ux";
        temp3.Padding = float4(0f, 20f, 0f, 0f);
        temp3.SourceLineNumber = 9;
        temp3.SourceFileName = "request.ux";
        temp3.Children.Add(temp4);
        temp4.Width = new Uno.UX.Size(80f, Uno.UX.Unit.Percent);
        temp4.Margin = float4(0f, 5f, 0f, 5f);
        temp4.SourceLineNumber = 15;
        temp4.SourceFileName = "request.ux";
        temp4.Children.Add(temp5);
        temp5.ButtonName = "SUBMIT";
        temp5.Width = new Uno.UX.Size(40f, Uno.UX.Unit.Percent);
        temp5.Alignment = Fuse.Elements.Alignment.CenterRight;
        temp5.SourceLineNumber = 16;
        temp5.SourceFileName = "request.ux";
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        this.Children.Add(temp);
        this.Children.Add(temp1);
    }
}
