[Uno.Compiler.UxGenerated]
public partial class SquareButton: Fuse.Controls.Button
{
    string _field_Textq;
    [global::Uno.UX.UXOriginSetter("SetTextq")]
    public string Textq
    {
        get { return _field_Textq; }
        set { SetTextq(value, null); }
    }
    public void SetTextq(string value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_Textq)
        {
            _field_Textq = value;
            OnPropertyChanged("Textq", origin);
        }
    }
    global::Uno.UX.Property<string> temp_Value_inst;
    static SquareButton()
    {
    }
    [global::Uno.UX.UXConstructor]
    public SquareButton()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp1 = new global::Fuse.Reactive.Constant(this);
        var temp = new global::Fuse.Controls.Text();
        temp_Value_inst = new OSP_FuseControlsTextControl_Value_Property(temp, __selector0);
        var temp2 = new global::Fuse.Reactive.Property(temp1, OSP_accessor_SquareButton_Textq.Singleton);
        var temp3 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp2, Fuse.Reactive.BindingMode.Read);
        var temp4 = new global::Fuse.Controls.Rectangle();
        var temp5 = new global::Fuse.Drawing.Stroke();
        var temp6 = new global::Fuse.Drawing.SolidColor();
        this.Height = new Uno.UX.Size(30f, Uno.UX.Unit.Unspecified);
        this.Margin = float4(30f, 5f, 30f, 25f);
        this.SourceLineNumber = 103;
        this.SourceFileName = "dashboard.ux";
        temp.TextWrapping = Fuse.Controls.TextWrapping.Wrap;
        temp.FontSize = 15f;
        temp.TextAlignment = Fuse.Controls.TextAlignment.Center;
        temp.Color = float4(1f, 1f, 1f, 1f);
        temp.Alignment = Fuse.Elements.Alignment.Center;
        temp.SourceLineNumber = 105;
        temp.SourceFileName = "dashboard.ux";
        temp.Font = global::MainView.Raleway;
        temp.Bindings.Add(temp3);
        temp2.SourceLineNumber = 105;
        temp2.SourceFileName = "dashboard.ux";
        temp1.SourceLineNumber = 105;
        temp1.SourceFileName = "dashboard.ux";
        temp4.Color = float4(0.1137255f, 0.4901961f, 0.2588235f, 1f);
        temp4.Layer = Fuse.Layer.Background;
        temp4.SourceLineNumber = 107;
        temp4.SourceFileName = "dashboard.ux";
        temp4.Strokes.Add(temp5);
        temp5.Width = 2f;
        temp5.Brush = temp6;
        temp6.Color = float4(1f, 1f, 1f, 1f);
        this.Children.Add(temp);
        this.Children.Add(temp4);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
}
