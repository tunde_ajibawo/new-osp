[Uno.Compiler.UxGenerated]
public partial class Profile: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly Profile __parent;
        [Uno.WeakReference] internal readonly Profile __parentInstance;
        public Template(Profile parent, Profile parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> __self_PlaceHolderName_inst;
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::InputField();
            __self_PlaceHolderName_inst = new OSP_InputField_PlaceHolderName_Property(__self, __selector0);
            var temp = new global::Fuse.Reactive.Data("surname");
            var temp1 = new global::Fuse.Reactive.DataBinding(__self_PlaceHolderName_inst, temp, Fuse.Reactive.BindingMode.Default);
            __self.Title = "SURNAME";
            __self.SourceLineNumber = 159;
            __self.SourceFileName = "profile.ux";
            temp.SourceLineNumber = 159;
            temp.SourceFileName = "profile.ux";
            __self.Bindings.Add(temp1);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "PlaceHolderName";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template1: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly Profile __parent;
        [Uno.WeakReference] internal readonly Profile __parentInstance;
        public Template1(Profile parent, Profile parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> __self_PlaceHolderName_inst;
        static Template1()
        {
        }
        public override object New()
        {
            var __self = new global::InputField();
            __self_PlaceHolderName_inst = new OSP_InputField_PlaceHolderName_Property(__self, __selector0);
            var temp = new global::Fuse.Reactive.Data("forenames");
            var temp1 = new global::Fuse.Reactive.DataBinding(__self_PlaceHolderName_inst, temp, Fuse.Reactive.BindingMode.Default);
            __self.Title = "OTHER NAMES";
            __self.SourceLineNumber = 160;
            __self.SourceFileName = "profile.ux";
            temp.SourceLineNumber = 160;
            temp.SourceFileName = "profile.ux";
            __self.Bindings.Add(temp1);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "PlaceHolderName";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template2: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly Profile __parent;
        [Uno.WeakReference] internal readonly Profile __parentInstance;
        public Template2(Profile parent, Profile parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> __self_PlaceHolderName_inst;
        static Template2()
        {
        }
        public override object New()
        {
            var __self = new global::InputField();
            __self_PlaceHolderName_inst = new OSP_InputField_PlaceHolderName_Property(__self, __selector0);
            var temp = new global::Fuse.Reactive.Data("sex");
            var temp1 = new global::Fuse.Reactive.DataBinding(__self_PlaceHolderName_inst, temp, Fuse.Reactive.BindingMode.Default);
            __self.Title = "SEX";
            __self.SourceLineNumber = 162;
            __self.SourceFileName = "profile.ux";
            temp.SourceLineNumber = 162;
            temp.SourceFileName = "profile.ux";
            __self.Bindings.Add(temp1);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "PlaceHolderName";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template3: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly Profile __parent;
        [Uno.WeakReference] internal readonly Profile __parentInstance;
        public Template3(Profile parent, Profile parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> temp_PlaceHolderName_inst;
        static Template3()
        {
        }
        public override object New()
        {
            var __self = new global::Fuse.Controls.DockPanel();
            var temp = new global::InputField();
            temp_PlaceHolderName_inst = new OSP_InputField_PlaceHolderName_Property(temp, __selector0);
            var temp1 = new global::Fuse.Reactive.Data("birth_date");
            var temp2 = new global::Fuse.Reactive.DataBinding(temp_PlaceHolderName_inst, temp1, Fuse.Reactive.BindingMode.Default);
            __self.SourceLineNumber = 164;
            __self.SourceFileName = "profile.ux";
            temp.Title = "D.O.B.";
            temp.SourceLineNumber = 165;
            temp.SourceFileName = "profile.ux";
            temp.Bindings.Add(temp2);
            temp1.SourceLineNumber = 165;
            temp1.SourceFileName = "profile.ux";
            __self.Children.Add(temp);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "PlaceHolderName";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template4: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly Profile __parent;
        [Uno.WeakReference] internal readonly Profile __parentInstance;
        public Template4(Profile parent, Profile parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> __self_PlaceHolderName_inst;
        static Template4()
        {
        }
        public override object New()
        {
            var __self = new global::InputField();
            __self_PlaceHolderName_inst = new OSP_InputField_PlaceHolderName_Property(__self, __selector0);
            var temp = new global::Fuse.Reactive.Data("nationality");
            var temp1 = new global::Fuse.Reactive.DataBinding(__self_PlaceHolderName_inst, temp, Fuse.Reactive.BindingMode.Default);
            __self.Title = "NATIONALITY";
            __self.SourceLineNumber = 168;
            __self.SourceFileName = "profile.ux";
            temp.SourceLineNumber = 168;
            temp.SourceFileName = "profile.ux";
            __self.Bindings.Add(temp1);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "PlaceHolderName";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template5: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly Profile __parent;
        [Uno.WeakReference] internal readonly Profile __parentInstance;
        public Template5(Profile parent, Profile parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> __self_PlaceHolderName_inst;
        static Template5()
        {
        }
        public override object New()
        {
            var __self = new global::InputField();
            __self_PlaceHolderName_inst = new OSP_InputField_PlaceHolderName_Property(__self, __selector0);
            var temp = new global::Fuse.Reactive.Data("email_address");
            var temp1 = new global::Fuse.Reactive.DataBinding(__self_PlaceHolderName_inst, temp, Fuse.Reactive.BindingMode.Default);
            __self.Title = "EMAIL";
            __self.BorderColor = float4(0.8352941f, 0.8352941f, 0.8352941f, 1f);
            __self.SourceLineNumber = 169;
            __self.SourceFileName = "profile.ux";
            temp.SourceLineNumber = 169;
            temp.SourceFileName = "profile.ux";
            __self.Bindings.Add(temp1);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "PlaceHolderName";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template6: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly Profile __parent;
        [Uno.WeakReference] internal readonly Profile __parentInstance;
        public Template6(Profile parent, Profile parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> __self_PlaceHolderName_inst;
        static Template6()
        {
        }
        public override object New()
        {
            var __self = new global::InputField();
            __self_PlaceHolderName_inst = new OSP_InputField_PlaceHolderName_Property(__self, __selector0);
            var temp = new global::Fuse.Reactive.Data("daytime_phone");
            var temp1 = new global::Fuse.Reactive.DataBinding(__self_PlaceHolderName_inst, temp, Fuse.Reactive.BindingMode.Default);
            __self.Title = "PHONE";
            __self.BorderColor = float4(0.8352941f, 0.8352941f, 0.8352941f, 1f);
            __self.SourceLineNumber = 170;
            __self.SourceFileName = "profile.ux";
            temp.SourceLineNumber = 170;
            temp.SourceFileName = "profile.ux";
            __self.Bindings.Add(temp1);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "PlaceHolderName";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template7: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly Profile __parent;
        [Uno.WeakReference] internal readonly Profile __parentInstance;
        public Template7(Profile parent, Profile parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        static Template7()
        {
        }
        public override object New()
        {
            var __self = new global::Fuse.Controls.Panel();
            var temp = new global::Fuse.Controls.Text();
            __self.Width = new Uno.UX.Size(80f, Uno.UX.Unit.Percent);
            __self.SourceLineNumber = 171;
            __self.SourceFileName = "profile.ux";
            temp.Value = "COMPANY INFORMATION";
            temp.FontSize = 12f;
            temp.TextColor = float4(0f, 0.1333333f, 0.2313726f, 1f);
            temp.Alignment = Fuse.Elements.Alignment.CenterLeft;
            temp.Margin = float4(0f, 20f, 0f, 10f);
            temp.SourceLineNumber = 172;
            temp.SourceFileName = "profile.ux";
            temp.Font = global::MainView.Raleway;
            __self.Children.Add(temp);
            return __self;
        }
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template8: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly Profile __parent;
        [Uno.WeakReference] internal readonly Profile __parentInstance;
        public Template8(Profile parent, Profile parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> __self_PlaceHolderName_inst;
        static Template8()
        {
        }
        public override object New()
        {
            var __self = new global::InputField();
            __self_PlaceHolderName_inst = new OSP_InputField_PlaceHolderName_Property(__self, __selector0);
            var temp = new global::Fuse.Reactive.Data("company_name");
            var temp1 = new global::Fuse.Reactive.DataBinding(__self_PlaceHolderName_inst, temp, Fuse.Reactive.BindingMode.Default);
            __self.Title = "OCCUPATION";
            __self.BorderColor = float4(0.8352941f, 0.8352941f, 0.8352941f, 1f);
            __self.SourceLineNumber = 174;
            __self.SourceFileName = "profile.ux";
            temp.SourceLineNumber = 174;
            temp.SourceFileName = "profile.ux";
            __self.Bindings.Add(temp1);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "PlaceHolderName";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template9: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly Profile __parent;
        [Uno.WeakReference] internal readonly Profile __parentInstance;
        public Template9(Profile parent, Profile parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> __self_PlaceHolderName_inst;
        static Template9()
        {
        }
        public override object New()
        {
            var __self = new global::InputField();
            __self_PlaceHolderName_inst = new OSP_InputField_PlaceHolderName_Property(__self, __selector0);
            var temp = new global::Fuse.Reactive.Data("job_title");
            var temp1 = new global::Fuse.Reactive.DataBinding(__self_PlaceHolderName_inst, temp, Fuse.Reactive.BindingMode.Default);
            __self.Title = "JOB TITLE";
            __self.BorderColor = float4(0.8352941f, 0.8352941f, 0.8352941f, 1f);
            __self.SourceLineNumber = 175;
            __self.SourceFileName = "profile.ux";
            temp.SourceLineNumber = 175;
            temp.SourceFileName = "profile.ux";
            __self.Bindings.Add(temp1);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "PlaceHolderName";
    }
    global::Uno.UX.Property<float> loadingPanel_Opacity_inst;
    global::Uno.UX.Property<float> ProfilePage_Opacity_inst;
    global::Uno.UX.Property<Uno.UX.FileSource> temp_File_inst;
    global::Uno.UX.Property<string> temp1_Url_inst;
    global::Uno.UX.Property<object> temp2_Items_inst;
    global::Uno.UX.Property<string> temp3_Value_inst;
    global::Uno.UX.Property<float> load_Opacity_inst;
    global::Uno.UX.Property<float> errorMsg_Opacity_inst;
    global::Uno.UX.Property<bool> temp4_Value_inst;
    global::Uno.UX.Property<bool> temp5_Value_inst;
    internal global::Fuse.Controls.DockPanel ProfilePage;
    internal global::Fuse.Reactive.EventBinding temp_eb23;
    internal global::Fuse.Triggers.Busy busy;
    internal global::Fuse.Controls.ClientPanel loadingPanel;
    internal global::Fuse.Controls.Image load;
    internal global::Fuse.Controls.StackPanel errorMsg;
    internal global::Fuse.Reactive.EventBinding temp_eb24;
    internal global::Fuse.Reactive.EventBinding temp_eb25;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "ProfilePage",
        "temp_eb23",
        "busy",
        "loadingPanel",
        "load",
        "errorMsg",
        "temp_eb24",
        "temp_eb25"
    };
    static Profile()
    {
    }
    [global::Uno.UX.UXConstructor]
    public Profile(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        loadingPanel = new global::Fuse.Controls.ClientPanel();
        loadingPanel_Opacity_inst = new OSP_FuseElementsElement_Opacity_Property(loadingPanel, __selector0);
        ProfilePage = new global::Fuse.Controls.DockPanel();
        ProfilePage_Opacity_inst = new OSP_FuseElementsElement_Opacity_Property(ProfilePage, __selector0);
        var temp6 = new global::Fuse.Reactive.Data("startLoad");
        var temp = new global::Fuse.Controls.Image();
        temp_File_inst = new OSP_FuseControlsImage_File_Property(temp, __selector1);
        var temp7 = new global::Fuse.Reactive.Data("image");
        var temp1 = new global::Fuse.Resources.HttpImageSource();
        temp1_Url_inst = new OSP_FuseResourcesHttpImageSource_Url_Property(temp1, __selector2);
        var temp8 = new global::Fuse.Reactive.Data("image");
        var temp2 = new global::Fuse.Reactive.Each();
        temp2_Items_inst = new OSP_FuseReactiveEach_Items_Property(temp2, __selector3);
        var temp9 = new global::Fuse.Reactive.Data("data");
        var temp3 = new global::Fuse.Controls.Text();
        temp3_Value_inst = new OSP_FuseControlsTextControl_Value_Property(temp3, __selector4);
        var temp10 = new global::Fuse.Reactive.Data("Msg");
        var temp11 = new global::Fuse.Reactive.Data("startLoad");
        var temp12 = new global::Fuse.Reactive.Data("dashboard");
        load = new global::Fuse.Controls.Image();
        load_Opacity_inst = new OSP_FuseElementsElement_Opacity_Property(load, __selector0);
        errorMsg = new global::Fuse.Controls.StackPanel();
        errorMsg_Opacity_inst = new OSP_FuseElementsElement_Opacity_Property(errorMsg, __selector0);
        var temp4 = new global::Fuse.Triggers.WhileTrue();
        temp4_Value_inst = new OSP_FuseTriggersWhileBool_Value_Property(temp4, __selector4);
        var temp13 = new global::Fuse.Reactive.Data("error");
        var temp5 = new global::Fuse.Triggers.WhileFalse();
        temp5_Value_inst = new OSP_FuseTriggersWhileBool_Value_Property(temp5, __selector4);
        var temp14 = new global::Fuse.Reactive.Data("error");
        var temp15 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp16 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp17 = new global::Fuse.Triggers.WhileBusy();
        var temp18 = new global::Fuse.Animations.Change<float>(loadingPanel_Opacity_inst);
        var temp19 = new global::Fuse.Animations.Change<float>(ProfilePage_Opacity_inst);
        var temp20 = new global::Fuse.Navigation.Activated();
        temp_eb23 = new global::Fuse.Reactive.EventBinding(temp6);
        busy = new global::Fuse.Triggers.Busy();
        var temp21 = new global::TopBar();
        var temp22 = new global::Fuse.Controls.StackPanel();
        var temp23 = new global::Fuse.Reactive.DataBinding(temp_File_inst, temp7, Fuse.Reactive.BindingMode.Default);
        var temp24 = new global::Fuse.Controls.Image();
        var temp25 = new global::Fuse.Reactive.DataBinding(temp1_Url_inst, temp8, Fuse.Reactive.BindingMode.Default);
        var temp26 = new Template(this, this);
        var temp27 = new Template1(this, this);
        var temp28 = new Template2(this, this);
        var temp29 = new Template3(this, this);
        var temp30 = new Template4(this, this);
        var temp31 = new Template5(this, this);
        var temp32 = new Template6(this, this);
        var temp33 = new Template7(this, this);
        var temp34 = new Template8(this, this);
        var temp35 = new Template9(this, this);
        var temp36 = new global::Fuse.Reactive.DataBinding(temp2_Items_inst, temp9, Fuse.Reactive.BindingMode.Default);
        var temp37 = new global::Fuse.Reactive.DataBinding(temp3_Value_inst, temp10, Fuse.Reactive.BindingMode.Default);
        var temp38 = new global::Fuse.Controls.DockPanel();
        var temp39 = new global::SubBut();
        temp_eb24 = new global::Fuse.Reactive.EventBinding(temp11);
        var temp40 = new global::SubBut();
        temp_eb25 = new global::Fuse.Reactive.EventBinding(temp12);
        var temp41 = new global::Fuse.Animations.Change<float>(load_Opacity_inst);
        var temp42 = new global::Fuse.Animations.Change<float>(errorMsg_Opacity_inst);
        var temp43 = new global::Fuse.Reactive.DataBinding(temp4_Value_inst, temp13, Fuse.Reactive.BindingMode.Default);
        var temp44 = new global::Fuse.Animations.Change<float>(load_Opacity_inst);
        var temp45 = new global::Fuse.Animations.Spin();
        var temp46 = new global::Fuse.Animations.Change<float>(errorMsg_Opacity_inst);
        var temp47 = new global::Fuse.Reactive.DataBinding(temp5_Value_inst, temp14, Fuse.Reactive.BindingMode.Default);
        this.Color = float4(1f, 1f, 1f, 1f);
        this.SourceLineNumber = 1;
        this.SourceFileName = "profile.ux";
        temp15.LineNumber = 3;
        temp15.FileName = "profile.ux";
        temp15.SourceLineNumber = 3;
        temp15.SourceFileName = "profile.ux";
        temp15.File = new global::Uno.UX.BundleFileSource(import("../../../js_files/navigation.js"));
        temp16.Code = "\n\t\tvar Observable = require('FuseJS/Observable');\n\t\tvar data = Observable();\n\t\tvar ImageTools = require(\"FuseJS/ImageTools\");\n\t\tvar imagePath = Observable();\n\t\tvar API = require('js_files/api.js');\n\n\t\tvar personnel_id = Observable();\n\t\tvar Storage = require(\"FuseJS/Storage\");\n\t\tdataPath = API.readOSPID();\n\n\n\t\n\t\t\n\t\tvar error = Observable(false);\n\t\tvar Msg = Observable(\"There was an error\");\n\n\t\t//console.log(JSON.stringify(OSP_details));\n\n\t\tfunction startLoad() {\n\t\t\tbusy.activate();\n\t\t\t\n\t\t\t\n\t\t\tStorage.read(dataPath).then(function(content) {\n\t\t\t    var cont = JSON.parse(content);\n\t\t\t    data.replaceAll(cont.personnel)\n\t\t\t    \n\n\t\t\t    ImageTools.getImageFromBase64(cont.picture).\n        \t\t\tthen(function (image) \n                { \n                    //console.log(\"Scratch image path is: \" + image.path); \n                    imagePath.value = image.path\n                }).catch(function(anerr){console.log(JSON.stringify(anerr))});\n\n\t\t\t    \n        \t\t\t//console.dir(cont)\n\n\n\t\t\t}, function(error) {\n\t\t\t    //For now, let's expect the error to be because of the file not being found.\n\t\t\t    //welcomeText.value = \"There is currently no local data stored\";\n\t\t\t});\n\n\t\t\t\n\t\t\t/*surname.value = OSP_details['surname'];\n\t\t\tforenames.value = OSP_details['forenames'];\n\t\t\tif(OSP_details['sex'] == 'M')\n\t\t\t\tsex.value = 'Male';\n\t\t\tif(OSP_details['sex'] == 'F')\n\t\t\t\tsex.value ='Female';\n\t\t\n\t\t\t\n\t\t\tbirth_date.value = OSP_details['birth_date'];\n\t\t\tnationality.value = OSP_details['nationality'];\n\t\t\temail_address.value = OSP_details['email_address'];\n\t\t\tdaytime_phone.value = OSP_details['daytime_phone'];\n\t\t\tcompany_name.value = OSP_details['company_name'];\n\t\t\tjob_title.value = OSP_details['job_title'];*/\n            \n\n\n\t\t\t\n\t\t\t\n\t\t\tbusy.deactivate();\t\t\t\n\t\t}\n\t\t\n\t\tfunction reload() {\n\n\t\t\t// busy.deactivate();\n\t\t}\n\n\n        // var data = Observable();\n\n        // fetch('https://gist.githubusercontent.com/petterroea/5ed146454706990ea8386f147d592eff/raw/b157cfed331da3cb88150051ab74aa131022fef8/colors.json')\n        //     .then(function(response) { return response.json(); })\n        //     .then(function(responseObject) { data.replaceAll(responseObject); });\n\t\t\t\n\n\t\t// console.log(JSON.stringify(data));\n\n\t\tmodule.exports = {\n\t\t\tdata: data,\n\t\t\tstartLoad: startLoad,\n\t\t\terror: error,\n\t\t\tMsg: Msg,\n\t\t\timage: imagePath\n\n\t\t};\n\n\t";
        temp16.LineNumber = 4;
        temp16.FileName = "profile.ux";
        temp16.SourceLineNumber = 4;
        temp16.SourceFileName = "profile.ux";
        ProfilePage.Opacity = 1f;
        ProfilePage.Name = __selector5;
        ProfilePage.SourceLineNumber = 135;
        ProfilePage.SourceFileName = "profile.ux";
        ProfilePage.Children.Add(temp17);
        ProfilePage.Children.Add(temp20);
        ProfilePage.Children.Add(busy);
        ProfilePage.Children.Add(temp21);
        ProfilePage.Children.Add(temp22);
        temp17.SourceLineNumber = 137;
        temp17.SourceFileName = "profile.ux";
        temp17.Animators.Add(temp18);
        temp17.Animators.Add(temp19);
        temp18.Value = 1f;
        temp18.Duration = 0.5;
        temp19.Value = 0f;
        temp20.SourceLineNumber = 142;
        temp20.SourceFileName = "profile.ux";
        temp20.Handler += temp_eb23.OnEvent;
        temp20.Bindings.Add(temp_eb23);
        temp6.SourceLineNumber = 142;
        temp6.SourceFileName = "profile.ux";
        busy.IsActive = false;
        busy.Name = __selector6;
        busy.SourceLineNumber = 145;
        busy.SourceFileName = "profile.ux";
        temp21.Title = "PROFILE";
        temp21.Height = new Uno.UX.Size(6f, Uno.UX.Unit.Percent);
        temp21.SourceLineNumber = 147;
        temp21.SourceFileName = "profile.ux";
        temp22.Height = new Uno.UX.Size(90f, Uno.UX.Unit.Percent);
        temp22.Padding = float4(0f, 20f, 0f, 0f);
        temp22.SourceLineNumber = 149;
        temp22.SourceFileName = "profile.ux";
        temp22.Children.Add(temp);
        temp22.Children.Add(temp24);
        temp22.Children.Add(temp2);
        temp.Width = new Uno.UX.Size(50f, Uno.UX.Unit.Percent);
        temp.SourceLineNumber = 150;
        temp.SourceFileName = "profile.ux";
        temp.Bindings.Add(temp23);
        temp7.SourceLineNumber = 150;
        temp7.SourceFileName = "profile.ux";
        temp24.SourceLineNumber = 151;
        temp24.SourceFileName = "profile.ux";
        temp24.Source = temp1;
        temp24.Bindings.Add(temp25);
        temp8.SourceLineNumber = 152;
        temp8.SourceFileName = "profile.ux";
        temp2.SourceLineNumber = 158;
        temp2.SourceFileName = "profile.ux";
        temp2.Templates.Add(temp26);
        temp2.Templates.Add(temp27);
        temp2.Templates.Add(temp28);
        temp2.Templates.Add(temp29);
        temp2.Templates.Add(temp30);
        temp2.Templates.Add(temp31);
        temp2.Templates.Add(temp32);
        temp2.Templates.Add(temp33);
        temp2.Templates.Add(temp34);
        temp2.Templates.Add(temp35);
        temp2.Bindings.Add(temp36);
        temp9.SourceLineNumber = 158;
        temp9.SourceFileName = "profile.ux";
        loadingPanel.Color = float4(1f, 1f, 1f, 1f);
        loadingPanel.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        loadingPanel.Height = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        loadingPanel.Alignment = Fuse.Elements.Alignment.Top;
        loadingPanel.Opacity = 0f;
        loadingPanel.Name = __selector7;
        loadingPanel.SourceLineNumber = 186;
        loadingPanel.SourceFileName = "profile.ux";
        loadingPanel.Children.Add(load);
        loadingPanel.Children.Add(errorMsg);
        loadingPanel.Children.Add(temp4);
        loadingPanel.Children.Add(temp5);
        load.Width = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        load.Height = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        load.Alignment = Fuse.Elements.Alignment.Center;
        load.Opacity = 1f;
        load.Name = __selector8;
        load.SourceLineNumber = 188;
        load.SourceFileName = "profile.ux";
        load.File = new global::Uno.UX.BundleFileSource(import("../../../Assets/disk.png"));
        errorMsg.Alignment = Fuse.Elements.Alignment.Center;
        errorMsg.Opacity = 0f;
        errorMsg.Name = __selector9;
        errorMsg.SourceLineNumber = 189;
        errorMsg.SourceFileName = "profile.ux";
        errorMsg.Children.Add(temp3);
        errorMsg.Children.Add(temp38);
        temp3.TextAlignment = Fuse.Controls.TextAlignment.Center;
        temp3.Color = float4(0.8f, 0f, 0f, 1f);
        temp3.Alignment = Fuse.Elements.Alignment.Center;
        temp3.SourceLineNumber = 190;
        temp3.SourceFileName = "profile.ux";
        temp3.Font = global::MainView.Raleway;
        temp3.Bindings.Add(temp37);
        temp10.SourceLineNumber = 190;
        temp10.SourceFileName = "profile.ux";
        temp38.Margin = float4(0f, 10f, 0f, 10f);
        temp38.SourceLineNumber = 191;
        temp38.SourceFileName = "profile.ux";
        temp38.Children.Add(temp39);
        temp38.Children.Add(temp40);
        temp39.Btext = "Retry";
        temp39.Icon = "\uF021";
        temp39.IconColor = float4(0.3333333f, 1f, 0.4980392f, 1f);
        temp39.Margin = float4(10f, 0f, 10f, 0f);
        temp39.SourceLineNumber = 192;
        temp39.SourceFileName = "profile.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp39, Fuse.Layouts.Dock.Left);
        global::Fuse.Gestures.Clicked.AddHandler(temp39, temp_eb24.OnEvent);
        temp39.Bindings.Add(temp_eb24);
        temp11.SourceLineNumber = 192;
        temp11.SourceFileName = "profile.ux";
        temp40.Btext = "Cancel";
        temp40.Icon = "\uF00D";
        temp40.IconColor = float4(0.8f, 0f, 0f, 1f);
        temp40.Margin = float4(10f, 0f, 10f, 0f);
        temp40.SourceLineNumber = 193;
        temp40.SourceFileName = "profile.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp40, Fuse.Layouts.Dock.Left);
        global::Fuse.Gestures.Clicked.AddHandler(temp40, temp_eb25.OnEvent);
        temp40.Bindings.Add(temp_eb25);
        temp12.SourceLineNumber = 193;
        temp12.SourceFileName = "profile.ux";
        temp4.SourceLineNumber = 199;
        temp4.SourceFileName = "profile.ux";
        temp4.Animators.Add(temp41);
        temp4.Animators.Add(temp42);
        temp4.Bindings.Add(temp43);
        temp41.Value = 0f;
        temp41.Duration = 0.5;
        temp42.Value = 1f;
        temp42.Duration = 0.5;
        temp13.SourceLineNumber = 199;
        temp13.SourceFileName = "profile.ux";
        temp5.SourceLineNumber = 204;
        temp5.SourceFileName = "profile.ux";
        temp5.Animators.Add(temp44);
        temp5.Animators.Add(temp45);
        temp5.Animators.Add(temp46);
        temp5.Bindings.Add(temp47);
        temp44.Value = 1f;
        temp44.Duration = 0.5;
        temp45.Frequency = 1;
        temp45.Target = load;
        temp46.Value = 0f;
        temp46.Duration = 0.5;
        temp14.SourceLineNumber = 204;
        temp14.SourceFileName = "profile.ux";
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(ProfilePage);
        __g_nametable.Objects.Add(temp_eb23);
        __g_nametable.Objects.Add(busy);
        __g_nametable.Objects.Add(loadingPanel);
        __g_nametable.Objects.Add(load);
        __g_nametable.Objects.Add(errorMsg);
        __g_nametable.Objects.Add(temp_eb24);
        __g_nametable.Objects.Add(temp_eb25);
        this.Children.Add(temp15);
        this.Children.Add(temp16);
        this.Children.Add(ProfilePage);
        this.Children.Add(loadingPanel);
    }
    static global::Uno.UX.Selector __selector0 = "Opacity";
    static global::Uno.UX.Selector __selector1 = "File";
    static global::Uno.UX.Selector __selector2 = "Url";
    static global::Uno.UX.Selector __selector3 = "Items";
    static global::Uno.UX.Selector __selector4 = "Value";
    static global::Uno.UX.Selector __selector5 = "ProfilePage";
    static global::Uno.UX.Selector __selector6 = "busy";
    static global::Uno.UX.Selector __selector7 = "loadingPanel";
    static global::Uno.UX.Selector __selector8 = "load";
    static global::Uno.UX.Selector __selector9 = "errorMsg";
}
