[Uno.Compiler.UxGenerated]
public partial class SubButton: Fuse.Controls.Rectangle
{
    string _field_ButtonName;
    [global::Uno.UX.UXOriginSetter("SetButtonName")]
    public string ButtonName
    {
        get { return _field_ButtonName; }
        set { SetButtonName(value, null); }
    }
    public void SetButtonName(string value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_ButtonName)
        {
            _field_ButtonName = value;
            OnPropertyChanged("ButtonName", origin);
        }
    }
    float4 _field_ButtonTextColor;
    [global::Uno.UX.UXOriginSetter("SetButtonTextColor")]
    public float4 ButtonTextColor
    {
        get { return _field_ButtonTextColor; }
        set { SetButtonTextColor(value, null); }
    }
    public void SetButtonTextColor(float4 value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_ButtonTextColor)
        {
            _field_ButtonTextColor = value;
            OnPropertyChanged("ButtonTextColor", origin);
        }
    }
    global::Uno.UX.Property<string> sub_Value_inst;
    global::Uno.UX.Property<float4> sub_TextColor_inst;
    global::Uno.UX.Property<float> sub_Opacity_inst;
    global::Uno.UX.Property<float> loadn_Opacity_inst;
    global::Uno.UX.Property<bool> temp_Value_inst;
    global::Uno.UX.Property<bool> temp1_Value_inst;
    internal global::Fuse.Controls.Text sub;
    internal global::Fuse.Controls.Image loadn;
    static SubButton()
    {
    }
    [global::Uno.UX.UXConstructor]
    public SubButton()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp2 = new global::Fuse.Reactive.Constant(this);
        sub = new global::Fuse.Controls.Text();
        sub_Value_inst = new OSP_FuseControlsTextControl_Value_Property(sub, __selector0);
        var temp3 = new global::Fuse.Reactive.Property(temp2, OSP_accessor_SubButton_ButtonName.Singleton);
        var temp4 = new global::Fuse.Reactive.Constant(this);
        sub_TextColor_inst = new OSP_FuseControlsTextControl_TextColor_Property(sub, __selector1);
        var temp5 = new global::Fuse.Reactive.Property(temp4, OSP_accessor_SubButton_ButtonTextColor.Singleton);
        sub_Opacity_inst = new OSP_FuseElementsElement_Opacity_Property(sub, __selector2);
        loadn = new global::Fuse.Controls.Image();
        loadn_Opacity_inst = new OSP_FuseElementsElement_Opacity_Property(loadn, __selector2);
        var temp = new global::Fuse.Triggers.WhileTrue();
        temp_Value_inst = new OSP_FuseTriggersWhileBool_Value_Property(temp, __selector0);
        var temp6 = new global::Fuse.Reactive.Data("visible");
        var temp1 = new global::Fuse.Triggers.WhileFalse();
        temp1_Value_inst = new OSP_FuseTriggersWhileBool_Value_Property(temp1, __selector0);
        var temp7 = new global::Fuse.Reactive.Data("visible");
        var temp8 = new global::Fuse.Reactive.DataBinding(sub_Value_inst, temp3, Fuse.Reactive.BindingMode.Default);
        var temp9 = new global::Fuse.Reactive.DataBinding(sub_TextColor_inst, temp5, Fuse.Reactive.BindingMode.Default);
        var temp10 = new global::Fuse.Animations.Change<float>(sub_Opacity_inst);
        var temp11 = new global::Fuse.Animations.Spin();
        var temp12 = new global::Fuse.Animations.Change<float>(loadn_Opacity_inst);
        var temp13 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp6, Fuse.Reactive.BindingMode.Default);
        var temp14 = new global::Fuse.Animations.Change<float>(sub_Opacity_inst);
        var temp15 = new global::Fuse.Animations.Change<float>(loadn_Opacity_inst);
        var temp16 = new global::Fuse.Reactive.DataBinding(temp1_Value_inst, temp7, Fuse.Reactive.BindingMode.Default);
        this.ButtonTextColor = float4(1f, 1f, 1f, 1f);
        this.Color = float4(0.1137255f, 0.4901961f, 0.2588235f, 1f);
        this.Width = new Uno.UX.Size(80f, Uno.UX.Unit.Percent);
        this.Margin = float4(0f, 5f, 0f, 5f);
        this.Padding = float4(5f, 5f, 5f, 5f);
        this.SourceLineNumber = 15;
        this.SourceFileName = "homepage.ux";
        sub.FontSize = 15f;
        sub.TextAlignment = Fuse.Controls.TextAlignment.Center;
        sub.Alignment = Fuse.Elements.Alignment.Center;
        sub.Opacity = 1f;
        sub.Name = __selector3;
        sub.SourceLineNumber = 18;
        sub.SourceFileName = "homepage.ux";
        sub.Font = global::MainView.Raleway;
        sub.Bindings.Add(temp8);
        sub.Bindings.Add(temp9);
        temp3.SourceLineNumber = 18;
        temp3.SourceFileName = "homepage.ux";
        temp2.SourceLineNumber = 18;
        temp2.SourceFileName = "homepage.ux";
        temp5.SourceLineNumber = 18;
        temp5.SourceFileName = "homepage.ux";
        temp4.SourceLineNumber = 18;
        temp4.SourceFileName = "homepage.ux";
        loadn.Width = new Uno.UX.Size(30f, Uno.UX.Unit.Unspecified);
        loadn.Opacity = 0f;
        loadn.Name = __selector4;
        loadn.SourceLineNumber = 19;
        loadn.SourceFileName = "homepage.ux";
        loadn.File = new global::Uno.UX.BundleFileSource(import("../../../Assets/disk.png"));
        temp.SourceLineNumber = 23;
        temp.SourceFileName = "homepage.ux";
        temp.Animators.Add(temp10);
        temp.Animators.Add(temp11);
        temp.Animators.Add(temp12);
        temp.Bindings.Add(temp13);
        temp10.Value = 0f;
        temp11.Frequency = 1;
        temp11.Target = loadn;
        temp12.Value = 1f;
        temp6.SourceLineNumber = 23;
        temp6.SourceFileName = "homepage.ux";
        temp1.SourceLineNumber = 29;
        temp1.SourceFileName = "homepage.ux";
        temp1.Animators.Add(temp14);
        temp1.Animators.Add(temp15);
        temp1.Bindings.Add(temp16);
        temp14.Value = 1f;
        temp15.Value = 0f;
        temp7.SourceLineNumber = 29;
        temp7.SourceFileName = "homepage.ux";
        this.Children.Add(sub);
        this.Children.Add(loadn);
        this.Children.Add(temp);
        this.Children.Add(temp1);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
    static global::Uno.UX.Selector __selector1 = "TextColor";
    static global::Uno.UX.Selector __selector2 = "Opacity";
    static global::Uno.UX.Selector __selector3 = "sub";
    static global::Uno.UX.Selector __selector4 = "loadn";
}
