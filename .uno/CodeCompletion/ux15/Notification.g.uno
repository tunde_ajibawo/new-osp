[Uno.Compiler.UxGenerated]
public partial class Notification: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly Notification __parent;
        [Uno.WeakReference] internal readonly Notification __parentInstance;
        public Template(Notification parent, Notification parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::TaskItem();
            __self.SourceLineNumber = 55;
            __self.SourceFileName = "notification.ux";
            return __self;
        }
    }
    global::Uno.UX.Property<object> temp_Items_inst;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router"
    };
    static Notification()
    {
    }
    [global::Uno.UX.UXConstructor]
    public Notification(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp = new global::Fuse.Reactive.Each();
        temp_Items_inst = new OSP_FuseReactiveEach_Items_Property(temp, __selector0);
        var temp1 = new global::Fuse.Reactive.Data("items");
        var temp2 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp3 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp4 = new global::Fuse.Controls.Grid();
        var temp5 = new global::TopBar();
        var temp6 = new global::Fuse.Controls.DockPanel();
        var temp7 = new global::Fuse.Controls.ScrollView();
        var temp8 = new global::Fuse.Controls.StackPanel();
        var temp9 = new Template(this, this);
        var temp10 = new global::Fuse.Reactive.DataBinding(temp_Items_inst, temp1, Fuse.Reactive.BindingMode.Default);
        var temp11 = new global::Fuse.Drawing.StaticSolidColor(float4(0.9333333f, 0.9333333f, 0.9333333f, 1f));
        this.Color = float4(1f, 1f, 1f, 1f);
        this.SourceLineNumber = 1;
        this.SourceFileName = "notification.ux";
        temp2.LineNumber = 3;
        temp2.FileName = "notification.ux";
        temp2.SourceLineNumber = 3;
        temp2.SourceFileName = "notification.ux";
        temp2.File = new global::Uno.UX.BundleFileSource(import("../../../js_files/navigation.js"));
        temp3.Code = "\n\t\tvar Observable = require(\"FuseJS/Observable\");\n\n\t\tfunction TaskItem(text, date, timeSlot, i, letter){\n\t\t\tthis.text = Observable(text);\n\t\t\tthis.date = Observable(date);\n\t\t\tthis.timeSlot = Observable(timeSlot);\n\t\t\tthis.delay = Observable(i * 0.05);\n\t\t\tthis.letter = Observable(letter);\n\t\t}\n\n\t\tvar items = Observable(\n\t\t\tnew TaskItem(\"Permit about to Expire\", \"20 Sept 2017\", \"\", 1, \"P\"),\n\t\t\tnew TaskItem(\"Oil & Gas Training\", \"22 Oct 2017\", \"11 - 12am\", 2, \"T\"),\n\t\t\tnew TaskItem(\"Lunch with Diane\", \"22 Oct 2017\", \"12am\", 3, \"M\")\n\t\t);\n\n\n\t\tmodule.exports = {\n\t\t\titems: items\n\t\t};\n\t";
        temp3.LineNumber = 4;
        temp3.FileName = "notification.ux";
        temp3.SourceLineNumber = 4;
        temp3.SourceFileName = "notification.ux";
        temp4.Rows = "1*,15*";
        temp4.SourceLineNumber = 48;
        temp4.SourceFileName = "notification.ux";
        temp4.Children.Add(temp5);
        temp4.Children.Add(temp6);
        temp5.Title = "NOTIFICATIONS";
        temp5.SourceLineNumber = 50;
        temp5.SourceFileName = "notification.ux";
        temp6.SourceLineNumber = 51;
        temp6.SourceFileName = "notification.ux";
        global::Fuse.Controls.Grid.SetRow(temp6, 1);
        temp6.Background = temp11;
        temp6.Children.Add(temp7);
        temp7.SourceLineNumber = 52;
        temp7.SourceFileName = "notification.ux";
        temp7.Children.Add(temp8);
        temp8.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp8.Padding = float4(0f, 10f, 0f, 0f);
        temp8.SourceLineNumber = 53;
        temp8.SourceFileName = "notification.ux";
        temp8.Children.Add(temp);
        temp.SourceLineNumber = 54;
        temp.SourceFileName = "notification.ux";
        temp.Templates.Add(temp9);
        temp.Bindings.Add(temp10);
        temp1.SourceLineNumber = 54;
        temp1.SourceFileName = "notification.ux";
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        this.Children.Add(temp2);
        this.Children.Add(temp3);
        this.Children.Add(temp4);
    }
    static global::Uno.UX.Selector __selector0 = "Items";
}
