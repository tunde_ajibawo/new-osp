[Uno.Compiler.UxGenerated]
public partial class History: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly History __parent;
        [Uno.WeakReference] internal readonly History __parentInstance;
        public Template(History parent, History parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> temp_Value_inst;
        global::Uno.UX.Property<string> temp1_Value_inst;
        global::Uno.UX.Property<string> temp2_Value_inst;
        global::Uno.UX.Property<string> temp3_Value_inst;
        global::Uno.UX.Property<string> temp4_Value_inst;
        internal global::Fuse.Controls.Rectangle item;
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::Fuse.Controls.DockPanel();
            var temp = new global::Fuse.Controls.Text();
            temp_Value_inst = new OSP_FuseControlsTextControl_Value_Property(temp, __selector0);
            var temp5 = new global::Fuse.Reactive.Data("location_name");
            var temp1 = new global::Fuse.Controls.Text();
            temp1_Value_inst = new OSP_FuseControlsTextControl_Value_Property(temp1, __selector0);
            var temp6 = new global::Fuse.Reactive.Data("departure_location");
            var temp2 = new global::Fuse.Controls.Text();
            temp2_Value_inst = new OSP_FuseControlsTextControl_Value_Property(temp2, __selector0);
            var temp7 = new global::Fuse.Reactive.Data("departure_datetime");
            var temp3 = new global::Fuse.Controls.Text();
            temp3_Value_inst = new OSP_FuseControlsTextControl_Value_Property(temp3, __selector0);
            var temp8 = new global::Fuse.Reactive.Data("arrival_location");
            var temp4 = new global::Fuse.Controls.Text();
            temp4_Value_inst = new OSP_FuseControlsTextControl_Value_Property(temp4, __selector0);
            var temp9 = new global::Fuse.Reactive.Data("arrival_datetime");
            var temp10 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp5, Fuse.Reactive.BindingMode.Default);
            var temp11 = new global::Fuse.Reactive.DataBinding(temp1_Value_inst, temp6, Fuse.Reactive.BindingMode.Default);
            var temp12 = new global::Fuse.Reactive.DataBinding(temp2_Value_inst, temp7, Fuse.Reactive.BindingMode.Default);
            var temp13 = new global::Fuse.Reactive.DataBinding(temp3_Value_inst, temp8, Fuse.Reactive.BindingMode.Default);
            var temp14 = new global::Fuse.Reactive.DataBinding(temp4_Value_inst, temp9, Fuse.Reactive.BindingMode.Default);
            item = new global::Fuse.Controls.Rectangle();
            var temp15 = new global::Fuse.Controls.Shadow();
            __self.Margin = float4(0f, 5f, 0f, 0f);
            __self.Padding = float4(15f, 15f, 15f, 15f);
            __self.SourceLineNumber = 119;
            __self.SourceFileName = "history.ux";
            temp.FontSize = 15f;
            temp.TextColor = float4(0.2f, 0.2f, 0.2f, 1f);
            temp.Margin = float4(0f, 0f, 0f, 10f);
            temp.SourceLineNumber = 120;
            temp.SourceFileName = "history.ux";
            global::Fuse.Controls.DockPanel.SetDock(temp, Fuse.Layouts.Dock.Top);
            temp.Font = global::MainView.Raleway;
            temp.Bindings.Add(temp10);
            temp5.SourceLineNumber = 120;
            temp5.SourceFileName = "history.ux";
            temp1.FontSize = 13f;
            temp1.SourceLineNumber = 121;
            temp1.SourceFileName = "history.ux";
            global::Fuse.Controls.DockPanel.SetDock(temp1, Fuse.Layouts.Dock.Left);
            temp1.Bindings.Add(temp11);
            temp6.SourceLineNumber = 121;
            temp6.SourceFileName = "history.ux";
            temp2.FontSize = 12f;
            temp2.TextColor = float4(0.4666667f, 0.4666667f, 0.4666667f, 1f);
            temp2.SourceLineNumber = 122;
            temp2.SourceFileName = "history.ux";
            global::Fuse.Controls.DockPanel.SetDock(temp2, Fuse.Layouts.Dock.Right);
            temp2.Bindings.Add(temp12);
            temp7.SourceLineNumber = 122;
            temp7.SourceFileName = "history.ux";
            temp3.FontSize = 13f;
            temp3.SourceLineNumber = 124;
            temp3.SourceFileName = "history.ux";
            global::Fuse.Controls.DockPanel.SetDock(temp3, Fuse.Layouts.Dock.Bottom);
            temp3.Bindings.Add(temp13);
            temp8.SourceLineNumber = 124;
            temp8.SourceFileName = "history.ux";
            temp4.FontSize = 12f;
            temp4.TextColor = float4(0.4666667f, 0.4666667f, 0.4666667f, 1f);
            temp4.SourceLineNumber = 125;
            temp4.SourceFileName = "history.ux";
            global::Fuse.Controls.DockPanel.SetDock(temp4, Fuse.Layouts.Dock.Bottom);
            temp4.Bindings.Add(temp14);
            temp9.SourceLineNumber = 125;
            temp9.SourceFileName = "history.ux";
            item.Color = float4(1f, 0.9921569f, 0.9372549f, 1f);
            item.Layer = Fuse.Layer.Background;
            item.Name = __selector1;
            item.SourceLineNumber = 126;
            item.SourceFileName = "history.ux";
            item.Children.Add(temp15);
            temp15.Distance = 2f;
            temp15.Size = 2f;
            temp15.SourceLineNumber = 127;
            temp15.SourceFileName = "history.ux";
            __self.Children.Add(temp);
            __self.Children.Add(temp1);
            __self.Children.Add(temp2);
            __self.Children.Add(temp3);
            __self.Children.Add(temp4);
            __self.Children.Add(item);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "Value";
        static global::Uno.UX.Selector __selector1 = "item";
    }
    global::Uno.UX.Property<Fuse.Visual> navigation_Active_inst;
    global::Uno.UX.Property<float4> trip_Color_inst;
    global::Uno.UX.Property<object> temp_Items_inst;
    internal global::Fuse.Controls.Panel page1Tab;
    internal global::Tab trip;
    internal global::Fuse.Reactive.EventBinding temp_eb15;
    internal global::Fuse.Controls.PageControl navigation;
    internal global::Fuse.Controls.Page page1;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "page1Tab",
        "trip",
        "temp_eb15",
        "navigation",
        "page1"
    };
    static History()
    {
    }
    [global::Uno.UX.UXConstructor]
    public History(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        navigation = new global::Fuse.Controls.PageControl();
        navigation_Active_inst = new OSP_FuseControlsNavigationControl_Active_Property(navigation, __selector0);
        var temp1 = new global::Fuse.Reactive.Data("Loadtraining");
        trip = new global::Tab();
        trip_Color_inst = new OSP_FuseControlsPanel_Color_Property(trip, __selector1);
        var temp = new global::Fuse.Reactive.Each();
        temp_Items_inst = new OSP_FuseReactiveEach_Items_Property(temp, __selector2);
        var temp2 = new global::Fuse.Reactive.Data("history");
        var temp3 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp4 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp5 = new global::Fuse.Controls.DockPanel();
        var temp6 = new global::TopBar();
        var temp7 = new global::Fuse.Controls.DockPanel();
        var temp8 = new global::Fuse.Controls.Grid();
        page1Tab = new global::Fuse.Controls.Panel();
        var temp9 = new global::Fuse.Gestures.Clicked();
        var temp10 = new global::Fuse.Triggers.Actions.Set<Fuse.Visual>(navigation_Active_inst);
        var temp11 = new global::Fuse.Triggers.Actions.Callback();
        temp_eb15 = new global::Fuse.Reactive.EventBinding(temp1);
        var temp12 = new global::Fuse.Drawing.StaticSolidColor(float4(0.9686275f, 0.8980392f, 0.3686275f, 1f));
        page1 = new global::Fuse.Controls.Page();
        var temp13 = new global::Fuse.Navigation.WhileActive();
        var temp14 = new global::Fuse.Animations.Change<float4>(trip_Color_inst);
        var temp15 = new global::Fuse.Controls.ScrollView();
        var temp16 = new global::Fuse.Controls.StackPanel();
        var temp17 = new Template(this, this);
        var temp18 = new global::Fuse.Reactive.DataBinding(temp_Items_inst, temp2, Fuse.Reactive.BindingMode.Default);
        var temp19 = new global::Fuse.Drawing.StaticSolidColor(float4(1f, 1f, 1f, 1f));
        this.Color = float4(1f, 1f, 1f, 1f);
        this.SourceLineNumber = 1;
        this.SourceFileName = "history.ux";
        temp3.LineNumber = 3;
        temp3.FileName = "history.ux";
        temp3.SourceLineNumber = 3;
        temp3.SourceFileName = "history.ux";
        temp3.File = new global::Uno.UX.BundleFileSource(import("../../../js_files/navigation.js"));
        temp4.Code = "\n\t\tvar API = require('/js_files/api.js');\n\t\tvar Observable = require('FuseJS/Observable');\n\t\tvar permit = Observable();\n\t\tvar history = Observable();\n\t\tvar medical = Observable();\n\t\tvar Storage = require(\"FuseJS/Storage\");\n\t\tdataPath = API.readOSPID();\n\n\t\tvar error = Observable(false);\n\t\tvar Msg = Observable(\"There was an error\");\n\n\t\tfunction Loadtraining() {\n\t\t\terror.value = false;\n\t\t\t//busy1.activate();\n\t\t\tStorage.read(dataPath).then(function(content) {\n\t\t\t    var cont = JSON.parse(content);\n\t\t\t    personnel_id = cont.personnel[0].personnel_id;\n\t\t\t\t\tfetch('http://41.75.207.60/osp/public/api/history/'+personnel_id )\n\t\t\t\t.then(function(response) { \n\t\t\t\tstatus = response.status;  // Get the HTTP status code\n    \t\t\tresponse_ok = response.ok; // Is response.status in the 200-range?\n\t\t\t\treturn response.json(); \n\t\t\t})\n\t\t\t.then(function (responseObject) {\n\t\t\t\tconsole.dir(responseObject)\n\t\t\t\thistory.replaceAll(responseObject);\n\t\t\t\tconsole.log(\"it works: \"+JSON.stringify(responseObject));\n\t\t\t\t// console.log(error.value);\n\t\t\t\t //busy1.deactivate();\n\t\t\t}).catch(function(err) {\n\t\t\t\tconsole.log(\"There was an error\");\n\t\t\t\t // busy.deactivate();\n\t\t\t\t if (status == 502) {\n\t\t\t\t\t// console.log(\"Error Address\");\n\t\t\t\t\tMsg.value = \"Wrong Address\";\n\t\t\t\t}else{\n\t\t\t\t\t// console.log(JSON.stringify(responseObject));\n\t\t\t\t\t// console.log(\"Record was not found\");\n\t\t\t\t\tMsg.value = \"User has no trips record\";\n\t\t\t\t}\n\t\t\t\terror.value = true;\n\t\t\t});\n\t\t\t    \n\t\t\t    \n\t\t\t    \n\n\t\t\t}, function(error) {\n\t\t\t    //For now, let's expect the error to be because of the file not being found.\n\t\t\t    //welcomeText.value = \"There is currently no local data stored\";\n\t\t\t});\n\n\t\t\t\n\t\t\t\n\t\t\t\n\t\t}\n\n\t\t\n\n\t\t\n\t\tmodule.exports={\n\t\t\tpermit: permit,\n\t\t\thistory: history,\n\t\t\tmedical: medical,\n\t\t\t\n\t\t\tLoadtraining: Loadtraining,\n\t\t\t\n\t\t\terror: error,\n\t\t\tMsg: Msg\n\t\t};\n\t";
        temp4.LineNumber = 4;
        temp4.FileName = "history.ux";
        temp4.SourceLineNumber = 4;
        temp4.SourceFileName = "history.ux";
        temp5.SourceLineNumber = 93;
        temp5.SourceFileName = "history.ux";
        temp5.Children.Add(temp6);
        temp5.Children.Add(temp7);
        temp6.Title = "HISTORY";
        temp6.SourceLineNumber = 95;
        temp6.SourceFileName = "history.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp6, Fuse.Layouts.Dock.Top);
        temp7.Padding = float4(0f, 20f, 0f, 0f);
        temp7.SourceLineNumber = 97;
        temp7.SourceFileName = "history.ux";
        temp7.Children.Add(temp8);
        temp7.Children.Add(navigation);
        temp8.ColumnCount = 3;
        temp8.Width = new Uno.UX.Size(90f, Uno.UX.Unit.Percent);
        temp8.Height = new Uno.UX.Size(40f, Uno.UX.Unit.Unspecified);
        temp8.SourceLineNumber = 98;
        temp8.SourceFileName = "history.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp8, Fuse.Layouts.Dock.Top);
        temp8.Background = temp12;
        temp8.Children.Add(page1Tab);
        page1Tab.Name = __selector3;
        page1Tab.SourceLineNumber = 99;
        page1Tab.SourceFileName = "history.ux";
        page1Tab.Children.Add(trip);
        trip.Text = "HISTORY";
        trip.Color = float4(0.1137255f, 0.4901961f, 0.2588235f, 1f);
        trip.Name = __selector4;
        trip.SourceLineNumber = 100;
        trip.SourceFileName = "history.ux";
        trip.Children.Add(temp9);
        temp9.SourceLineNumber = 101;
        temp9.SourceFileName = "history.ux";
        temp9.Actions.Add(temp10);
        temp9.Actions.Add(temp11);
        temp9.Bindings.Add(temp_eb15);
        temp10.Value = page1;
        temp10.SourceLineNumber = 102;
        temp10.SourceFileName = "history.ux";
        temp11.SourceLineNumber = 103;
        temp11.SourceFileName = "history.ux";
        temp11.Handler += temp_eb15.OnEvent;
        temp1.SourceLineNumber = 103;
        temp1.SourceFileName = "history.ux";
        navigation.Name = __selector5;
        navigation.SourceLineNumber = 110;
        navigation.SourceFileName = "history.ux";
        navigation.Children.Add(page1);
        page1.Name = __selector6;
        page1.SourceLineNumber = 111;
        page1.SourceFileName = "history.ux";
        page1.Background = temp19;
        page1.Children.Add(temp13);
        page1.Children.Add(temp15);
        temp13.Threshold = 0.5f;
        temp13.SourceLineNumber = 113;
        temp13.SourceFileName = "history.ux";
        temp13.Animators.Add(temp14);
        temp14.Value = float4(0.9686275f, 0.8980392f, 0.3686275f, 1f);
        temp15.SourceLineNumber = 116;
        temp15.SourceFileName = "history.ux";
        temp15.Children.Add(temp16);
        temp16.Width = new Uno.UX.Size(90f, Uno.UX.Unit.Percent);
        temp16.Margin = float4(0f, 10f, 0f, 0f);
        temp16.SourceLineNumber = 117;
        temp16.SourceFileName = "history.ux";
        temp16.Children.Add(temp);
        temp.SourceLineNumber = 118;
        temp.SourceFileName = "history.ux";
        temp.Templates.Add(temp17);
        temp.Bindings.Add(temp18);
        temp2.SourceLineNumber = 118;
        temp2.SourceFileName = "history.ux";
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(page1Tab);
        __g_nametable.Objects.Add(trip);
        __g_nametable.Objects.Add(temp_eb15);
        __g_nametable.Objects.Add(navigation);
        __g_nametable.Objects.Add(page1);
        this.Children.Add(temp3);
        this.Children.Add(temp4);
        this.Children.Add(temp5);
    }
    static global::Uno.UX.Selector __selector0 = "Active";
    static global::Uno.UX.Selector __selector1 = "Color";
    static global::Uno.UX.Selector __selector2 = "Items";
    static global::Uno.UX.Selector __selector3 = "page1Tab";
    static global::Uno.UX.Selector __selector4 = "trip";
    static global::Uno.UX.Selector __selector5 = "navigation";
    static global::Uno.UX.Selector __selector6 = "page1";
}
