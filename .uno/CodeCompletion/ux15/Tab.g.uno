[Uno.Compiler.UxGenerated]
public partial class Tab: Fuse.Controls.Panel
{
    string _field_Text;
    [global::Uno.UX.UXOriginSetter("SetText")]
    public string Text
    {
        get { return _field_Text; }
        set { SetText(value, null); }
    }
    public void SetText(string value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_Text)
        {
            _field_Text = value;
            OnPropertyChanged("Text", origin);
        }
    }
    float4 _field_TColor;
    [global::Uno.UX.UXOriginSetter("SetTColor")]
    public float4 TColor
    {
        get { return _field_TColor; }
        set { SetTColor(value, null); }
    }
    public void SetTColor(float4 value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_TColor)
        {
            _field_TColor = value;
            OnPropertyChanged("TColor", origin);
        }
    }
    global::Uno.UX.Property<string> temp_Value_inst;
    global::Uno.UX.Property<float4> temp_Color_inst;
    static Tab()
    {
    }
    [global::Uno.UX.UXConstructor]
    public Tab()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp1 = new global::Fuse.Reactive.Constant(this);
        var temp = new global::Fuse.Controls.Text();
        temp_Value_inst = new OSP_FuseControlsTextControl_Value_Property(temp, __selector0);
        var temp2 = new global::Fuse.Reactive.Property(temp1, OSP_accessor_Tab_Text.Singleton);
        var temp3 = new global::Fuse.Reactive.Constant(this);
        temp_Color_inst = new OSP_FuseControlsTextControl_Color_Property(temp, __selector1);
        var temp4 = new global::Fuse.Reactive.Property(temp3, OSP_accessor_Tab_TColor.Singleton);
        var temp5 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp2, Fuse.Reactive.BindingMode.Read);
        var temp6 = new global::Fuse.Reactive.DataBinding(temp_Color_inst, temp4, Fuse.Reactive.BindingMode.Default);
        var temp7 = new global::Fuse.Drawing.StaticSolidColor(float4(0.1137255f, 0.4901961f, 0.2588235f, 1f));
        this.TColor = float4(1f, 1f, 1f, 1f);
        this.Margin = float4(0f, 0f, 0f, 4f);
        this.ClipToBounds = false;
        this.SourceLineNumber = 76;
        this.SourceFileName = "history.ux";
        temp.FontSize = 12f;
        temp.Alignment = Fuse.Elements.Alignment.Center;
        temp.SourceLineNumber = 79;
        temp.SourceFileName = "history.ux";
        temp.Font = global::MainView.Raleway;
        temp.Bindings.Add(temp5);
        temp.Bindings.Add(temp6);
        temp2.SourceLineNumber = 79;
        temp2.SourceFileName = "history.ux";
        temp1.SourceLineNumber = 79;
        temp1.SourceFileName = "history.ux";
        temp4.SourceLineNumber = 79;
        temp4.SourceFileName = "history.ux";
        temp3.SourceLineNumber = 79;
        temp3.SourceFileName = "history.ux";
        this.Background = temp7;
        this.Children.Add(temp);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
    static global::Uno.UX.Selector __selector1 = "Color";
}
