//console.log("I called this one");
var Timer = require("FuseJS/Timer");
var login = require("js_files/login");
var API = require('js_files/api');
var Observable = require("FuseJS/Observable");

var LoaderOpacity = Observable(1);
var PageOpacity = Observable(0);
var status = Observable(false);
var msg = Observable();
var msgColor = Observable();
var visible = Observable(false);

var OSPID = Observable();
var password = Observable();


Timer.create(function () {
    console.log("This will run once, after 3 seconds");
   
    LoaderOpacity.value = 0;
    PageOpacity.value = 1;
}, 1000, false);



function login_val() {

    visible.value = true;
    var db = API.checkDB();
    var user_details = db.getCollection('user_details')
    
    //var personnel = JSON.parse(obj['personnel'])
    //var odin = user_details.findOne({ personnel_id:OSPID.value });
    
    //this could mean this is the first time to login on these device
    if (user_details == null)
    {
        API.auth(OSPID.value)
        .then(function (response) {
            ok = response.ok
            var obj = JSON.parse(response._bodyText)
            if (ok) {
                status.value = true;
                msgColor.value = "#2AFFAA";
                msg.value = "";
                visible.value = false;

                
                //console.dir(user_details)
                //console.log(JSON.stringify(user_details.data))
                //console.log(personnel[0]['surname']);
                //router.push("dashboard");
            } else {
                status.value = false;
                msgColor.value = "#ff7f7f";
                msg.value = obj["msg"];
                visible.value = false;
            }
            
        })
        .catch(function (error) {
            console.log(JSON.stringify(error));
        });
     
    }
    console.dir(db)


    /**var user_details = db.addCollection('user_details')
    var permits
    var training
    var history
    var picture
    user_details.insert(personnel[0])
    ***/
    

    /***
     * API.auth(OSPID.value)
        .then(function (response) {
            ok = response.ok
            var obj = JSON.parse(response._bodyText)

            if (ok) {
                
                status.value = true;
                msgColor.value = "#2AFFAA";
                msg.value = "";
                visible.value = false;

                
                //console.dir(user_details)
                //console.log(JSON.stringify(user_details.data))
                //console.log(personnel[0]['surname']);
                //router.push("dashboard");
            } else {
                status.value = false;
                msgColor.value = "#ff7f7f";
                msg.value = obj["msg"];
                visible.value = false;
            }
            //debug_log(JSON.stringify(obj))
        })
        .catch(function (error) {
            console.log(JSON.stringify(error));
        });


        var users = db.addCollection('users', ['email'], true, false);
        console.dir(users.data);

        var stan = users.insert({
            name: 'odin',
            email: 'odin.soap@lokijs.org',
            age: 38
        });
        console.dir(users.data);
        stan.name = 'Stan Laurel';

        // update object (this really only syncs the index)
        users.update(stan);
        console.dir(users.data);
        users.remove(stan);
        console.dir(users.data);
     */
    

}

function logout() {
    var userID = ""
    API.saveUserID("")
    msg.value = null;
    msgColor.value = null;
    visible.value = true;
    status.value = false;

    //console.log(login.OSPID.value);

}

function home() {
    router.push("home");
}

function dashboard() {
    router.push("dashboard");
}

function capture() {
    router.push("capture");
}

function history() {
    router.push("history");
}

function profile() {
    router.push("profile");
}

function pictureUpload() {
    router.push("picture");
}

function workinfo() {
    router.push("workinfo");
}

function swipecard() {
    router.push("swipecard");
}

function notification() {
    router.push("notification");
}

function news() {
    router.push("news");
}

function request() {
    router.push("request");
}

function forgotPassword() {
    router.push("password");
}

function signup() {
    router.push("signup");
}

function goBack() {
    router.push("dashboard");
}

module.exports = {
    home: home,
    dashboard: dashboard,
    capture: capture,
    history: history,
    profile: profile,
    request: request,
    workinfo: workinfo,
    swipecard: swipecard,
    notification: notification,
    news: news,
    forgotPassword: forgotPassword,
    signup: signup,
    pictureUpload: pictureUpload,
    goBack: goBack,

    LoaderOpacity: LoaderOpacity,
    PageOpacity: PageOpacity,
    visible: visible,
    status: status,
    msg: msg,
    msgColor: msgColor,
    login_val: login_val,
    OSPID: OSPID,
    password: password,
    logout: logout
};