// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.DropdownOption.h>
#include <_root.OSP_accessor_Dr-6cc2ce14.h>
#include <Fuse.Drawing.Brush.h>
#include <Uno.Bool.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.Type.h>
#include <Uno.UX.IPropertyListener.h>
#include <Uno.UX.PropertyObject.h>
#include <Uno.UX.Selector.h>
static uString* STRINGS[1];
static uType* TYPES[3];

namespace g{

// internal sealed class OSP_accessor_DropdownOption_BackgroundColor :21
// {
// static generated OSP_accessor_DropdownOption_BackgroundColor() :21
static void OSP_accessor_DropdownOption_BackgroundColor__cctor__fn(uType* __type)
{
    OSP_accessor_DropdownOption_BackgroundColor::Singleton_ = OSP_accessor_DropdownOption_BackgroundColor::New1();
    OSP_accessor_DropdownOption_BackgroundColor::_name_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[0/*"BackgroundC...*/]);
}

static void OSP_accessor_DropdownOption_BackgroundColor_build(uType* type)
{
    ::STRINGS[0] = uString::Const("BackgroundColor");
    ::TYPES[0] = ::g::DropdownOption_typeof();
    ::TYPES[1] = ::g::Fuse::Drawing::Brush_typeof();
    ::TYPES[2] = ::g::Uno::Type_typeof();
    type->SetFields(0,
        ::g::Uno::UX::PropertyAccessor_typeof(), (uintptr_t)&OSP_accessor_DropdownOption_BackgroundColor::Singleton_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&OSP_accessor_DropdownOption_BackgroundColor::_name_, uFieldFlagsStatic);
}

::g::Uno::UX::PropertyAccessor_type* OSP_accessor_DropdownOption_BackgroundColor_typeof()
{
    static uSStrong< ::g::Uno::UX::PropertyAccessor_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::PropertyAccessor_typeof();
    options.FieldCount = 2;
    options.ObjectSize = sizeof(OSP_accessor_DropdownOption_BackgroundColor);
    options.TypeSize = sizeof(::g::Uno::UX::PropertyAccessor_type);
    type = (::g::Uno::UX::PropertyAccessor_type*)uClassType::New("OSP_accessor_DropdownOption_BackgroundColor", options);
    type->fp_build_ = OSP_accessor_DropdownOption_BackgroundColor_build;
    type->fp_ctor_ = (void*)OSP_accessor_DropdownOption_BackgroundColor__New1_fn;
    type->fp_cctor_ = OSP_accessor_DropdownOption_BackgroundColor__cctor__fn;
    type->fp_GetAsObject = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::PropertyObject*, uObject**))OSP_accessor_DropdownOption_BackgroundColor__GetAsObject_fn;
    type->fp_get_Name = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::Selector*))OSP_accessor_DropdownOption_BackgroundColor__get_Name_fn;
    type->fp_get_PropertyType = (void(*)(::g::Uno::UX::PropertyAccessor*, uType**))OSP_accessor_DropdownOption_BackgroundColor__get_PropertyType_fn;
    type->fp_SetAsObject = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::PropertyObject*, uObject*, uObject*))OSP_accessor_DropdownOption_BackgroundColor__SetAsObject_fn;
    type->fp_get_SupportsOriginSetter = (void(*)(::g::Uno::UX::PropertyAccessor*, bool*))OSP_accessor_DropdownOption_BackgroundColor__get_SupportsOriginSetter_fn;
    return type;
}

// public generated OSP_accessor_DropdownOption_BackgroundColor() :21
void OSP_accessor_DropdownOption_BackgroundColor__ctor_1_fn(OSP_accessor_DropdownOption_BackgroundColor* __this)
{
    __this->ctor_1();
}

// public override sealed object GetAsObject(Uno.UX.PropertyObject obj) :27
void OSP_accessor_DropdownOption_BackgroundColor__GetAsObject_fn(OSP_accessor_DropdownOption_BackgroundColor* __this, ::g::Uno::UX::PropertyObject* obj, uObject** __retval)
{
    return *__retval = uPtr(uCast< ::g::DropdownOption*>(obj, ::TYPES[0/*DropdownOption*/]))->BackgroundColor(), void();
}

// public override sealed Uno.UX.Selector get_Name() :24
void OSP_accessor_DropdownOption_BackgroundColor__get_Name_fn(OSP_accessor_DropdownOption_BackgroundColor* __this, ::g::Uno::UX::Selector* __retval)
{
    return *__retval = OSP_accessor_DropdownOption_BackgroundColor::_name_, void();
}

// public generated OSP_accessor_DropdownOption_BackgroundColor New() :21
void OSP_accessor_DropdownOption_BackgroundColor__New1_fn(OSP_accessor_DropdownOption_BackgroundColor** __retval)
{
    *__retval = OSP_accessor_DropdownOption_BackgroundColor::New1();
}

// public override sealed Uno.Type get_PropertyType() :26
void OSP_accessor_DropdownOption_BackgroundColor__get_PropertyType_fn(OSP_accessor_DropdownOption_BackgroundColor* __this, uType** __retval)
{
    return *__retval = ::TYPES[1/*Fuse.Drawing.Brush*/], void();
}

// public override sealed void SetAsObject(Uno.UX.PropertyObject obj, object v, Uno.UX.IPropertyListener origin) :28
void OSP_accessor_DropdownOption_BackgroundColor__SetAsObject_fn(OSP_accessor_DropdownOption_BackgroundColor* __this, ::g::Uno::UX::PropertyObject* obj, uObject* v, uObject* origin)
{
    uPtr(uCast< ::g::DropdownOption*>(obj, ::TYPES[0/*DropdownOption*/]))->SetBackgroundColor(uCast< ::g::Fuse::Drawing::Brush*>(v, ::TYPES[1/*Fuse.Drawing.Brush*/]), origin);
}

// public override sealed bool get_SupportsOriginSetter() :29
void OSP_accessor_DropdownOption_BackgroundColor__get_SupportsOriginSetter_fn(OSP_accessor_DropdownOption_BackgroundColor* __this, bool* __retval)
{
    return *__retval = true, void();
}

uSStrong< ::g::Uno::UX::PropertyAccessor*> OSP_accessor_DropdownOption_BackgroundColor::Singleton_;
::g::Uno::UX::Selector OSP_accessor_DropdownOption_BackgroundColor::_name_;

// public generated OSP_accessor_DropdownOption_BackgroundColor() [instance] :21
void OSP_accessor_DropdownOption_BackgroundColor::ctor_1()
{
    ctor_();
}

// public generated OSP_accessor_DropdownOption_BackgroundColor New() [static] :21
OSP_accessor_DropdownOption_BackgroundColor* OSP_accessor_DropdownOption_BackgroundColor::New1()
{
    OSP_accessor_DropdownOption_BackgroundColor* obj1 = (OSP_accessor_DropdownOption_BackgroundColor*)uNew(OSP_accessor_DropdownOption_BackgroundColor_typeof());
    obj1->ctor_1();
    return obj1;
}
// }

} // ::g
