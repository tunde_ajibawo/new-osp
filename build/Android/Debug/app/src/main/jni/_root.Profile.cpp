// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/Profile.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.MainView.h>
#include <_root.OSP_bundle.h>
#include <_root.OSP_FuseControl-b26a5bbf.h>
#include <_root.OSP_FuseControl-b6a7f6ab.h>
#include <_root.OSP_FuseElement-78dfea6e.h>
#include <_root.OSP_FuseReactiv-f855d0e4.h>
#include <_root.OSP_FuseResourc-c6e4c88c.h>
#include <_root.OSP_FuseTrigger-1375289b.h>
#include <_root.Profile.h>
#include <_root.Profile.Template.h>
#include <_root.Profile.Template1.h>
#include <_root.Profile.Template2.h>
#include <_root.Profile.Template3.h>
#include <_root.Profile.Template4.h>
#include <_root.Profile.Template5.h>
#include <_root.Profile.Template6.h>
#include <_root.Profile.Template7.h>
#include <_root.Profile.Template8.h>
#include <_root.Profile.Template9.h>
#include <_root.SubBut.h>
#include <_root.TopBar.h>
#include <Fuse.Animations.Animator.h>
#include <Fuse.Animations.Change-1.h>
#include <Fuse.Animations.Spin.h>
#include <Fuse.Animations.TrackAnimator.h>
#include <Fuse.Controls.ClientPanel.h>
#include <Fuse.Controls.DockPanel.h>
#include <Fuse.Controls.Image.h>
#include <Fuse.Controls.Panel.h>
#include <Fuse.Controls.StackPanel.h>
#include <Fuse.Controls.Text.h>
#include <Fuse.Controls.TextAlignment.h>
#include <Fuse.Controls.TextControl.h>
#include <Fuse.Elements.Alignment.h>
#include <Fuse.Elements.Element.h>
#include <Fuse.Font.h>
#include <Fuse.Gestures.Clicked.h>
#include <Fuse.Gestures.ClickedHandler.h>
#include <Fuse.Layouts.Dock.h>
#include <Fuse.Navigation.Activated.h>
#include <Fuse.Navigation.Router.h>
#include <Fuse.Reactive.BindingMode.h>
#include <Fuse.Reactive.Data.h>
#include <Fuse.Reactive.DataBinding.h>
#include <Fuse.Reactive.Each.h>
#include <Fuse.Reactive.EventBinding.h>
#include <Fuse.Reactive.Expression.h>
#include <Fuse.Reactive.IExpression.h>
#include <Fuse.Reactive.Instantiator.h>
#include <Fuse.Reactive.JavaScript.h>
#include <Fuse.Resources.HttpImageSource.h>
#include <Fuse.Resources.ImageSource.h>
#include <Fuse.Triggers.Busy.h>
#include <Fuse.Triggers.PulseTr-e6f97a32.h>
#include <Fuse.Triggers.PulseTrigger-1.h>
#include <Fuse.Triggers.Trigger.h>
#include <Fuse.Triggers.WhileBool.h>
#include <Fuse.Triggers.WhileBusy.h>
#include <Fuse.Triggers.WhileFalse.h>
#include <Fuse.Triggers.WhileTrue.h>
#include <Uno.Bool.h>
#include <Uno.Double.h>
#include <Uno.EventArgs.h>
#include <Uno.Float.h>
#include <Uno.Float4.h>
#include <Uno.Int.h>
#include <Uno.IO.BundleFile.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.UX.BundleFileSource.h>
#include <Uno.UX.FileSource.h>
#include <Uno.UX.NameTable.h>
#include <Uno.UX.Property.h>
#include <Uno.UX.Property1-1.h>
#include <Uno.UX.Selector.h>
#include <Uno.UX.Size.h>
#include <Uno.UX.Template.h>
#include <Uno.UX.Unit.h>

namespace g{

// public partial sealed class Profile :2
// {
// static Profile() :345
static void Profile__cctor_4_fn(uType* __type)
{
    Profile::__g_static_nametable1_ = uArray::Init<uString*>(::g::Uno::String_typeof()->Array(), 9, uString::Const("router"), uString::Const("ProfilePage"), uString::Const("temp_eb23"), uString::Const("busy"), uString::Const("loadingPanel"), uString::Const("load"), uString::Const("errorMsg"), uString::Const("temp_eb24"), uString::Const("temp_eb25"));
    Profile::__selector0_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("Opacity"));
    Profile::__selector1_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("File"));
    Profile::__selector2_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("Url"));
    Profile::__selector3_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("Items"));
    Profile::__selector4_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("Value"));
    Profile::__selector5_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("ProfilePage"));
    Profile::__selector6_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("busy"));
    Profile::__selector7_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("loadingPanel"));
    Profile::__selector8_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("load"));
    Profile::__selector9_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("errorMsg"));
}

static void Profile_build(uType* type)
{
    type->SetDependencies(
        ::g::MainView_typeof(),
        ::g::OSP_bundle_typeof());
    type->SetInterfaces(
        ::g::Uno::Collections::IList_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface0),
        ::g::Fuse::Scripting::IScriptObject_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface1),
        ::g::Fuse::IProperties_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface2),
        ::g::Fuse::INotifyUnrooted_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface3),
        ::g::Fuse::ISourceLocation_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface4),
        ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface5),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface6),
        ::g::Uno::Collections::IList_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface7),
        ::g::Uno::UX::IPropertyListener_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface8),
        ::g::Fuse::ITemplateSource_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface9),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Visual_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface10),
        ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface11),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface12),
        ::g::Fuse::Triggers::Actions::IShow_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface13),
        ::g::Fuse::Triggers::Actions::IHide_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface14),
        ::g::Fuse::Triggers::Actions::ICollapse_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface15),
        ::g::Fuse::IActualPlacement_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface16),
        ::g::Fuse::Animations::IResize_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface17),
        ::g::Fuse::Drawing::ISurfaceDrawable_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface18));
    type->SetFields(121,
        ::g::Fuse::Navigation::Router_typeof(), offsetof(Profile, router), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(Profile, loadingPanel_Opacity_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(Profile, ProfilePage_Opacity_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::UX::FileSource_typeof(), NULL), offsetof(Profile, temp_File_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(Profile, temp1_Url_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(uObject_typeof(), NULL), offsetof(Profile, temp2_Items_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(Profile, temp3_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(Profile, load_Opacity_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(Profile, errorMsg_Opacity_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Bool_typeof(), NULL), offsetof(Profile, temp4_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Bool_typeof(), NULL), offsetof(Profile, temp5_Value_inst), 0,
        ::g::Fuse::Controls::DockPanel_typeof(), offsetof(Profile, ProfilePage), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(Profile, temp_eb23), 0,
        ::g::Fuse::Triggers::Busy_typeof(), offsetof(Profile, busy), 0,
        ::g::Fuse::Controls::ClientPanel_typeof(), offsetof(Profile, loadingPanel), 0,
        ::g::Fuse::Controls::Image_typeof(), offsetof(Profile, load), 0,
        ::g::Fuse::Controls::StackPanel_typeof(), offsetof(Profile, errorMsg), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(Profile, temp_eb24), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(Profile, temp_eb25), 0,
        ::g::Uno::UX::NameTable_typeof(), offsetof(Profile, __g_nametable1), 0,
        ::g::Uno::String_typeof()->Array(), (uintptr_t)&Profile::__g_static_nametable1_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Profile::__selector0_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Profile::__selector1_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Profile::__selector2_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Profile::__selector3_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Profile::__selector4_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Profile::__selector5_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Profile::__selector6_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Profile::__selector7_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Profile::__selector8_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Profile::__selector9_, uFieldFlagsStatic);
}

::g::Fuse::Controls::Panel_type* Profile_typeof()
{
    static uSStrong< ::g::Fuse::Controls::Panel_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Fuse::Controls::Page_typeof();
    options.FieldCount = 152;
    options.InterfaceCount = 19;
    options.DependencyCount = 2;
    options.ObjectSize = sizeof(Profile);
    options.TypeSize = sizeof(::g::Fuse::Controls::Panel_type);
    type = (::g::Fuse::Controls::Panel_type*)uClassType::New("Profile", options);
    type->fp_build_ = Profile_build;
    type->fp_cctor_ = Profile__cctor_4_fn;
    type->interface18.fp_Draw = (void(*)(uObject*, ::g::Fuse::Drawing::Surface*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableDraw_fn;
    type->interface18.fp_get_IsPrimary = (void(*)(uObject*, bool*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableget_IsPrimary_fn;
    type->interface18.fp_get_ElementSize = (void(*)(uObject*, ::g::Uno::Float2*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableget_ElementSize_fn;
    type->interface13.fp_Show = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsIShowShow_fn;
    type->interface15.fp_Collapse = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsICollapseCollapse_fn;
    type->interface14.fp_Hide = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsIHideHide_fn;
    type->interface17.fp_SetSize = (void(*)(uObject*, ::g::Uno::Float2*))::g::Fuse::Elements::Element__FuseAnimationsIResizeSetSize_fn;
    type->interface16.fp_get_ActualSize = (void(*)(uObject*, ::g::Uno::Float3*))::g::Fuse::Elements::Element__FuseIActualPlacementget_ActualSize_fn;
    type->interface16.fp_add_Placed = (void(*)(uObject*, uDelegate*))::g::Fuse::Elements::Element__add_Placed_fn;
    type->interface16.fp_remove_Placed = (void(*)(uObject*, uDelegate*))::g::Fuse::Elements::Element__remove_Placed_fn;
    type->interface10.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Visual__UnoCollectionsIEnumerableFuseVisualGetEnumerator_fn;
    type->interface11.fp_Clear = (void(*)(uObject*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeClear_fn;
    type->interface11.fp_Contains = (void(*)(uObject*, void*, bool*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeContains_fn;
    type->interface7.fp_RemoveAt = (void(*)(uObject*, int32_t*))::g::Fuse::Visual__UnoCollectionsIListFuseNodeRemoveAt_fn;
    type->interface12.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Visual__UnoCollectionsIEnumerableFuseNodeGetEnumerator_fn;
    type->interface11.fp_get_Count = (void(*)(uObject*, int32_t*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeget_Count_fn;
    type->interface7.fp_get_Item = (void(*)(uObject*, int32_t*, uTRef))::g::Fuse::Visual__UnoCollectionsIListFuseNodeget_Item_fn;
    type->interface7.fp_Insert = (void(*)(uObject*, int32_t*, void*))::g::Fuse::Visual__Insert1_fn;
    type->interface8.fp_OnPropertyChanged = (void(*)(uObject*, ::g::Uno::UX::PropertyObject*, ::g::Uno::UX::Selector*))::g::Fuse::Controls::Control__OnPropertyChanged2_fn;
    type->interface9.fp_FindTemplate = (void(*)(uObject*, uString*, ::g::Uno::UX::Template**))::g::Fuse::Visual__FindTemplate_fn;
    type->interface11.fp_Add = (void(*)(uObject*, void*))::g::Fuse::Visual__Add1_fn;
    type->interface11.fp_Remove = (void(*)(uObject*, void*, bool*))::g::Fuse::Visual__Remove1_fn;
    type->interface5.fp_Clear = (void(*)(uObject*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingClear_fn;
    type->interface5.fp_Contains = (void(*)(uObject*, void*, bool*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingContains_fn;
    type->interface0.fp_RemoveAt = (void(*)(uObject*, int32_t*))::g::Fuse::Node__UnoCollectionsIListFuseBindingRemoveAt_fn;
    type->interface6.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Node__UnoCollectionsIEnumerableFuseBindingGetEnumerator_fn;
    type->interface1.fp_SetScriptObject = (void(*)(uObject*, uObject*, ::g::Fuse::Scripting::Context*))::g::Fuse::Node__FuseScriptingIScriptObjectSetScriptObject_fn;
    type->interface5.fp_get_Count = (void(*)(uObject*, int32_t*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingget_Count_fn;
    type->interface0.fp_get_Item = (void(*)(uObject*, int32_t*, uTRef))::g::Fuse::Node__UnoCollectionsIListFuseBindingget_Item_fn;
    type->interface1.fp_get_ScriptObject = (void(*)(uObject*, uObject**))::g::Fuse::Node__FuseScriptingIScriptObjectget_ScriptObject_fn;
    type->interface1.fp_get_ScriptContext = (void(*)(uObject*, ::g::Fuse::Scripting::Context**))::g::Fuse::Node__FuseScriptingIScriptObjectget_ScriptContext_fn;
    type->interface4.fp_get_SourceNearest = (void(*)(uObject*, uObject**))::g::Fuse::Node__FuseISourceLocationget_SourceNearest_fn;
    type->interface3.fp_add_Unrooted = (void(*)(uObject*, uDelegate*))::g::Fuse::Node__FuseINotifyUnrootedadd_Unrooted_fn;
    type->interface3.fp_remove_Unrooted = (void(*)(uObject*, uDelegate*))::g::Fuse::Node__FuseINotifyUnrootedremove_Unrooted_fn;
    type->interface0.fp_Insert = (void(*)(uObject*, int32_t*, void*))::g::Fuse::Node__Insert_fn;
    type->interface2.fp_get_Properties = (void(*)(uObject*, ::g::Fuse::Properties**))::g::Fuse::Node__get_Properties_fn;
    type->interface4.fp_get_SourceLineNumber = (void(*)(uObject*, int32_t*))::g::Fuse::Node__get_SourceLineNumber_fn;
    type->interface4.fp_get_SourceFileName = (void(*)(uObject*, uString**))::g::Fuse::Node__get_SourceFileName_fn;
    type->interface5.fp_Add = (void(*)(uObject*, void*))::g::Fuse::Node__Add_fn;
    type->interface5.fp_Remove = (void(*)(uObject*, void*, bool*))::g::Fuse::Node__Remove_fn;
    return type;
}

// public Profile(Fuse.Navigation.Router router) :349
void Profile__ctor_8_fn(Profile* __this, ::g::Fuse::Navigation::Router* router1)
{
    __this->ctor_8(router1);
}

// private void InitializeUX() :355
void Profile__InitializeUX_fn(Profile* __this)
{
    __this->InitializeUX();
}

// public Profile New(Fuse.Navigation.Router router) :349
void Profile__New5_fn(::g::Fuse::Navigation::Router* router1, Profile** __retval)
{
    *__retval = Profile::New5(router1);
}

uSStrong<uArray*> Profile::__g_static_nametable1_;
::g::Uno::UX::Selector Profile::__selector0_;
::g::Uno::UX::Selector Profile::__selector1_;
::g::Uno::UX::Selector Profile::__selector2_;
::g::Uno::UX::Selector Profile::__selector3_;
::g::Uno::UX::Selector Profile::__selector4_;
::g::Uno::UX::Selector Profile::__selector5_;
::g::Uno::UX::Selector Profile::__selector6_;
::g::Uno::UX::Selector Profile::__selector7_;
::g::Uno::UX::Selector Profile::__selector8_;
::g::Uno::UX::Selector Profile::__selector9_;

// public Profile(Fuse.Navigation.Router router) [instance] :349
void Profile::ctor_8(::g::Fuse::Navigation::Router* router1)
{
    ctor_7();
    router = router1;
    InitializeUX();
}

// private void InitializeUX() [instance] :355
void Profile::InitializeUX()
{
    __g_nametable1 = ::g::Uno::UX::NameTable::New1(NULL, Profile::__g_static_nametable1_);
    loadingPanel = ::g::Fuse::Controls::ClientPanel::New5();
    loadingPanel_Opacity_inst = ::g::OSP_FuseElementsElement_Opacity_Property::New1(loadingPanel, Profile::__selector0_);
    ProfilePage = ::g::Fuse::Controls::DockPanel::New4();
    ProfilePage_Opacity_inst = ::g::OSP_FuseElementsElement_Opacity_Property::New1(ProfilePage, Profile::__selector0_);
    ::g::Fuse::Reactive::Data* temp6 = ::g::Fuse::Reactive::Data::New1(uString::Const("startLoad"));
    ::g::Fuse::Controls::Image* temp = ::g::Fuse::Controls::Image::New3();
    temp_File_inst = ::g::OSP_FuseControlsImage_File_Property::New1(temp, Profile::__selector1_);
    ::g::Fuse::Reactive::Data* temp7 = ::g::Fuse::Reactive::Data::New1(uString::Const("image"));
    ::g::Fuse::Resources::HttpImageSource* temp1 = ::g::Fuse::Resources::HttpImageSource::New2();
    temp1_Url_inst = ::g::OSP_FuseResourcesHttpImageSource_Url_Property::New1(temp1, Profile::__selector2_);
    ::g::Fuse::Reactive::Data* temp8 = ::g::Fuse::Reactive::Data::New1(uString::Const("image"));
    ::g::Fuse::Reactive::Each* temp2 = ::g::Fuse::Reactive::Each::New4();
    temp2_Items_inst = ::g::OSP_FuseReactiveEach_Items_Property::New1(temp2, Profile::__selector3_);
    ::g::Fuse::Reactive::Data* temp9 = ::g::Fuse::Reactive::Data::New1(uString::Const("data"));
    ::g::Fuse::Controls::Text* temp3 = ::g::Fuse::Controls::Text::New3();
    temp3_Value_inst = ::g::OSP_FuseControlsTextControl_Value_Property::New1(temp3, Profile::__selector4_);
    ::g::Fuse::Reactive::Data* temp10 = ::g::Fuse::Reactive::Data::New1(uString::Const("Msg"));
    ::g::Fuse::Reactive::Data* temp11 = ::g::Fuse::Reactive::Data::New1(uString::Const("startLoad"));
    ::g::Fuse::Reactive::Data* temp12 = ::g::Fuse::Reactive::Data::New1(uString::Const("dashboard"));
    load = ::g::Fuse::Controls::Image::New3();
    load_Opacity_inst = ::g::OSP_FuseElementsElement_Opacity_Property::New1(load, Profile::__selector0_);
    errorMsg = ::g::Fuse::Controls::StackPanel::New4();
    errorMsg_Opacity_inst = ::g::OSP_FuseElementsElement_Opacity_Property::New1(errorMsg, Profile::__selector0_);
    ::g::Fuse::Triggers::WhileTrue* temp4 = ::g::Fuse::Triggers::WhileTrue::New2();
    temp4_Value_inst = ::g::OSP_FuseTriggersWhileBool_Value_Property::New1(temp4, Profile::__selector4_);
    ::g::Fuse::Reactive::Data* temp13 = ::g::Fuse::Reactive::Data::New1(uString::Const("error"));
    ::g::Fuse::Triggers::WhileFalse* temp5 = ::g::Fuse::Triggers::WhileFalse::New2();
    temp5_Value_inst = ::g::OSP_FuseTriggersWhileBool_Value_Property::New1(temp5, Profile::__selector4_);
    ::g::Fuse::Reactive::Data* temp14 = ::g::Fuse::Reactive::Data::New1(uString::Const("error"));
    ::g::Fuse::Reactive::JavaScript* temp15 = ::g::Fuse::Reactive::JavaScript::New2(__g_nametable1);
    ::g::Fuse::Reactive::JavaScript* temp16 = ::g::Fuse::Reactive::JavaScript::New2(__g_nametable1);
    ::g::Fuse::Triggers::WhileBusy* temp17 = ::g::Fuse::Triggers::WhileBusy::New2();
    ::g::Fuse::Animations::Change* temp18 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), loadingPanel_Opacity_inst);
    ::g::Fuse::Animations::Change* temp19 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), ProfilePage_Opacity_inst);
    ::g::Fuse::Navigation::Activated* temp20 = ::g::Fuse::Navigation::Activated::New2();
    temp_eb23 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp6);
    busy = ::g::Fuse::Triggers::Busy::New2();
    ::g::TopBar* temp21 = ::g::TopBar::New5();
    ::g::Fuse::Controls::StackPanel* temp22 = ::g::Fuse::Controls::StackPanel::New4();
    ::g::Fuse::Reactive::DataBinding* temp23 = ::g::Fuse::Reactive::DataBinding::New1(temp_File_inst, (uObject*)temp7, 3);
    ::g::Fuse::Controls::Image* temp24 = ::g::Fuse::Controls::Image::New3();
    ::g::Fuse::Reactive::DataBinding* temp25 = ::g::Fuse::Reactive::DataBinding::New1(temp1_Url_inst, (uObject*)temp8, 3);
    Profile__Template* temp26 = Profile__Template::New2(this, this);
    Profile__Template1* temp27 = Profile__Template1::New2(this, this);
    Profile__Template2* temp28 = Profile__Template2::New2(this, this);
    Profile__Template3* temp29 = Profile__Template3::New2(this, this);
    Profile__Template4* temp30 = Profile__Template4::New2(this, this);
    Profile__Template5* temp31 = Profile__Template5::New2(this, this);
    Profile__Template6* temp32 = Profile__Template6::New2(this, this);
    Profile__Template7* temp33 = Profile__Template7::New2(this, this);
    Profile__Template8* temp34 = Profile__Template8::New2(this, this);
    Profile__Template9* temp35 = Profile__Template9::New2(this, this);
    ::g::Fuse::Reactive::DataBinding* temp36 = ::g::Fuse::Reactive::DataBinding::New1(temp2_Items_inst, (uObject*)temp9, 3);
    ::g::Fuse::Reactive::DataBinding* temp37 = ::g::Fuse::Reactive::DataBinding::New1(temp3_Value_inst, (uObject*)temp10, 3);
    ::g::Fuse::Controls::DockPanel* temp38 = ::g::Fuse::Controls::DockPanel::New4();
    ::g::SubBut* temp39 = ::g::SubBut::New6();
    temp_eb24 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp11);
    ::g::SubBut* temp40 = ::g::SubBut::New6();
    temp_eb25 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp12);
    ::g::Fuse::Animations::Change* temp41 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), load_Opacity_inst);
    ::g::Fuse::Animations::Change* temp42 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), errorMsg_Opacity_inst);
    ::g::Fuse::Reactive::DataBinding* temp43 = ::g::Fuse::Reactive::DataBinding::New1(temp4_Value_inst, (uObject*)temp13, 3);
    ::g::Fuse::Animations::Change* temp44 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), load_Opacity_inst);
    ::g::Fuse::Animations::Spin* temp45 = ::g::Fuse::Animations::Spin::New2();
    ::g::Fuse::Animations::Change* temp46 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), errorMsg_Opacity_inst);
    ::g::Fuse::Reactive::DataBinding* temp47 = ::g::Fuse::Reactive::DataBinding::New1(temp5_Value_inst, (uObject*)temp14, 3);
    Color(::g::Uno::Float4__New2(1.0f, 1.0f, 1.0f, 1.0f));
    SourceLineNumber(1);
    SourceFileName(uString::Const("profile.ux"));
    temp15->LineNumber(3);
    temp15->FileName(uString::Const("profile.ux"));
    temp15->SourceLineNumber(3);
    temp15->SourceFileName(uString::Const("profile.ux"));
    temp15->File(::g::Uno::UX::BundleFileSource::New1(::g::OSP_bundle::navigationb176862e()));
    temp16->Code(uString::Const("\n"
        "\t\tvar Observable = require('FuseJS/Observable');\n"
        "\t\tvar data = Observable();\n"
        "\t\tvar ImageTools = require(\"FuseJS/ImageTools\");\n"
        "\t\tvar imagePath = Observable();\n"
        "\t\tvar API = require('js_files/api.js');\n"
        "\n"
        "\t\tvar personnel_id = Observable();\n"
        "\t\tvar Storage = require(\"FuseJS/Storage\");\n"
        "\t\tdataPath = API.readOSPID();\n"
        "\n"
        "\n"
        "\t\n"
        "\t\t\n"
        "\t\tvar error = Observable(false);\n"
        "\t\tvar Msg = Observable(\"There was an error\");\n"
        "\n"
        "\t\t//console.log(JSON.stringify(OSP_details));\n"
        "\n"
        "\t\tfunction startLoad() {\n"
        "\t\t\tbusy.activate();\n"
        "\t\t\t\n"
        "\t\t\t\n"
        "\t\t\tStorage.read(dataPath).then(function(content) {\n"
        "\t\t\t    var cont = JSON.parse(content);\n"
        "\t\t\t    data.replaceAll(cont.personnel)\n"
        "\t\t\t    \n"
        "\n"
        "\t\t\t    ImageTools.getImageFromBase64(cont.picture).\n"
        "        \t\t\tthen(function (image) \n"
        "                { \n"
        "                    //console.log(\"Scratch image path is: \" + image.path); \n"
        "                    imagePath.value = image.path\n"
        "                }).catch(function(anerr){console.log(JSON.stringify(anerr))});\n"
        "\n"
        "\t\t\t    \n"
        "        \t\t\t//console.dir(cont)\n"
        "\n"
        "\n"
        "\t\t\t}, function(error) {\n"
        "\t\t\t    //For now, let's expect the error to be because of the file not being found.\n"
        "\t\t\t    //welcomeText.value = \"There is currently no local data stored\";\n"
        "\t\t\t});\n"
        "\n"
        "\t\t\t\n"
        "\t\t\t/*surname.value = OSP_details['surname'];\n"
        "\t\t\tforenames.value = OSP_details['forenames'];\n"
        "\t\t\tif(OSP_details['sex'] == 'M')\n"
        "\t\t\t\tsex.value = 'Male';\n"
        "\t\t\tif(OSP_details['sex'] == 'F')\n"
        "\t\t\t\tsex.value ='Female';\n"
        "\t\t\n"
        "\t\t\t\n"
        "\t\t\tbirth_date.value = OSP_details['birth_date'];\n"
        "\t\t\tnationality.value = OSP_details['nationality'];\n"
        "\t\t\temail_address.value = OSP_details['email_address'];\n"
        "\t\t\tdaytime_phone.value = OSP_details['daytime_phone'];\n"
        "\t\t\tcompany_name.value = OSP_details['company_name'];\n"
        "\t\t\tjob_title.value = OSP_details['job_title'];*/\n"
        "            \n"
        "\n"
        "\n"
        "\t\t\t\n"
        "\t\t\t\n"
        "\t\t\tbusy.deactivate();\t\t\t\n"
        "\t\t}\n"
        "\t\t\n"
        "\t\tfunction reload() {\n"
        "\n"
        "\t\t\t// busy.deactivate();\n"
        "\t\t}\n"
        "\n"
        "\n"
        "        // var data = Observable();\n"
        "\n"
        "        // fetch('https://gist.githubusercontent.com/petterroea/5ed146454706990ea8386f147d592eff/raw/b157cfed331da3cb88150051ab74aa131022fef8/colors.json')\n"
        "        //     .then(function(response) { return response.json(); })\n"
        "        //     .then(function(responseObject) { data.replaceAll(responseObject); });\n"
        "\t\t\t\n"
        "\n"
        "\t\t// console.log(JSON.stringify(data));\n"
        "\n"
        "\t\tmodule.exports = {\n"
        "\t\t\tdata: data,\n"
        "\t\t\tstartLoad: startLoad,\n"
        "\t\t\terror: error,\n"
        "\t\t\tMsg: Msg,\n"
        "\t\t\timage: imagePath\n"
        "\n"
        "\t\t};\n"
        "\n"
        "\t"));
    temp16->LineNumber(4);
    temp16->FileName(uString::Const("profile.ux"));
    temp16->SourceLineNumber(4);
    temp16->SourceFileName(uString::Const("profile.ux"));
    uPtr(ProfilePage)->Opacity(1.0f);
    uPtr(ProfilePage)->Name(Profile::__selector5_);
    uPtr(ProfilePage)->SourceLineNumber(135);
    uPtr(ProfilePage)->SourceFileName(uString::Const("profile.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(ProfilePage)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp17);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(ProfilePage)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp20);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(ProfilePage)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), busy);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(ProfilePage)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp21);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(ProfilePage)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp22);
    temp17->SourceLineNumber(137);
    temp17->SourceFileName(uString::Const("profile.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp17->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp18);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp17->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp19);
    ::g::Fuse::Animations::Change__set_Value_fn(temp18, uCRef(1.0f));
    temp18->Duration(0.5);
    ::g::Fuse::Animations::Change__set_Value_fn(temp19, uCRef(0.0f));
    temp20->SourceLineNumber(142);
    temp20->SourceFileName(uString::Const("profile.ux"));
    temp20->add_Handler(uDelegate::New(::g::Fuse::Triggers::PulseTrigger__PulseHandler_typeof()->MakeType(::g::Uno::EventArgs_typeof(), NULL), (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb23)));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp20->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp_eb23);
    temp6->SourceLineNumber(142);
    temp6->SourceFileName(uString::Const("profile.ux"));
    uPtr(busy)->IsActive(false);
    uPtr(busy)->Name(Profile::__selector6_);
    uPtr(busy)->SourceLineNumber(145);
    uPtr(busy)->SourceFileName(uString::Const("profile.ux"));
    temp21->Title(uString::Const("PROFILE"));
    temp21->Height(::g::Uno::UX::Size__New1(6.0f, 4));
    temp21->SourceLineNumber(147);
    temp21->SourceFileName(uString::Const("profile.ux"));
    temp22->Height(::g::Uno::UX::Size__New1(90.0f, 4));
    temp22->Padding(::g::Uno::Float4__New2(0.0f, 20.0f, 0.0f, 0.0f));
    temp22->SourceLineNumber(149);
    temp22->SourceFileName(uString::Const("profile.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp22->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp22->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp24);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp22->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp2);
    temp->Width(::g::Uno::UX::Size__New1(50.0f, 4));
    temp->SourceLineNumber(150);
    temp->SourceFileName(uString::Const("profile.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp23);
    temp7->SourceLineNumber(150);
    temp7->SourceFileName(uString::Const("profile.ux"));
    temp24->SourceLineNumber(151);
    temp24->SourceFileName(uString::Const("profile.ux"));
    temp24->Source(temp1);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp24->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp25);
    temp8->SourceLineNumber(152);
    temp8->SourceFileName(uString::Const("profile.ux"));
    temp2->SourceLineNumber(158);
    temp2->SourceFileName(uString::Const("profile.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp2->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp26);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp2->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp27);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp2->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp28);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp2->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp29);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp2->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp30);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp2->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp31);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp2->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp32);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp2->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp33);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp2->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp34);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp2->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp35);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp2->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp36);
    temp9->SourceLineNumber(158);
    temp9->SourceFileName(uString::Const("profile.ux"));
    uPtr(loadingPanel)->Color(::g::Uno::Float4__New2(1.0f, 1.0f, 1.0f, 1.0f));
    uPtr(loadingPanel)->Width(::g::Uno::UX::Size__New1(100.0f, 4));
    uPtr(loadingPanel)->Height(::g::Uno::UX::Size__New1(100.0f, 4));
    uPtr(loadingPanel)->Alignment(4);
    uPtr(loadingPanel)->Opacity(0.0f);
    uPtr(loadingPanel)->Name(Profile::__selector7_);
    uPtr(loadingPanel)->SourceLineNumber(186);
    uPtr(loadingPanel)->SourceFileName(uString::Const("profile.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(loadingPanel)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), load);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(loadingPanel)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), errorMsg);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(loadingPanel)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp4);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(loadingPanel)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp5);
    uPtr(load)->Width(::g::Uno::UX::Size__New1(50.0f, 1));
    uPtr(load)->Height(::g::Uno::UX::Size__New1(50.0f, 1));
    uPtr(load)->Alignment(10);
    uPtr(load)->Opacity(1.0f);
    uPtr(load)->Name(Profile::__selector8_);
    uPtr(load)->SourceLineNumber(188);
    uPtr(load)->SourceFileName(uString::Const("profile.ux"));
    uPtr(load)->File(::g::Uno::UX::BundleFileSource::New1(::g::OSP_bundle::diskc6e80153()));
    uPtr(errorMsg)->Alignment(10);
    uPtr(errorMsg)->Opacity(0.0f);
    uPtr(errorMsg)->Name(Profile::__selector9_);
    uPtr(errorMsg)->SourceLineNumber(189);
    uPtr(errorMsg)->SourceFileName(uString::Const("profile.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(errorMsg)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp3);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(errorMsg)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp38);
    temp3->TextAlignment(1);
    temp3->Color(::g::Uno::Float4__New2(0.8f, 0.0f, 0.0f, 1.0f));
    temp3->Alignment(10);
    temp3->SourceLineNumber(190);
    temp3->SourceFileName(uString::Const("profile.ux"));
    temp3->Font(::g::MainView::Raleway());
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp3->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp37);
    temp10->SourceLineNumber(190);
    temp10->SourceFileName(uString::Const("profile.ux"));
    temp38->Margin(::g::Uno::Float4__New2(0.0f, 10.0f, 0.0f, 10.0f));
    temp38->SourceLineNumber(191);
    temp38->SourceFileName(uString::Const("profile.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp38->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp39);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp38->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp40);
    temp39->Btext(uString::Const("Retry"));
    temp39->Icon(uString::Const("\357\200\241"));
    temp39->IconColor(::g::Uno::Float4__New2(0.3333333f, 1.0f, 0.4980392f, 1.0f));
    temp39->Margin(::g::Uno::Float4__New2(10.0f, 0.0f, 10.0f, 0.0f));
    temp39->SourceLineNumber(192);
    temp39->SourceFileName(uString::Const("profile.ux"));
    ::g::Fuse::Controls::DockPanel::SetDock(temp39, 0);
    ::g::Fuse::Gestures::Clicked::AddHandler(temp39, uDelegate::New(::g::Fuse::Gestures::ClickedHandler_typeof(), (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb24)));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp39->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp_eb24);
    temp11->SourceLineNumber(192);
    temp11->SourceFileName(uString::Const("profile.ux"));
    temp40->Btext(uString::Const("Cancel"));
    temp40->Icon(uString::Const("\357\200\215"));
    temp40->IconColor(::g::Uno::Float4__New2(0.8f, 0.0f, 0.0f, 1.0f));
    temp40->Margin(::g::Uno::Float4__New2(10.0f, 0.0f, 10.0f, 0.0f));
    temp40->SourceLineNumber(193);
    temp40->SourceFileName(uString::Const("profile.ux"));
    ::g::Fuse::Controls::DockPanel::SetDock(temp40, 0);
    ::g::Fuse::Gestures::Clicked::AddHandler(temp40, uDelegate::New(::g::Fuse::Gestures::ClickedHandler_typeof(), (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb25)));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp40->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp_eb25);
    temp12->SourceLineNumber(193);
    temp12->SourceFileName(uString::Const("profile.ux"));
    temp4->SourceLineNumber(199);
    temp4->SourceFileName(uString::Const("profile.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp4->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp41);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp4->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp42);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp4->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp43);
    ::g::Fuse::Animations::Change__set_Value_fn(temp41, uCRef(0.0f));
    temp41->Duration(0.5);
    ::g::Fuse::Animations::Change__set_Value_fn(temp42, uCRef(1.0f));
    temp42->Duration(0.5);
    temp13->SourceLineNumber(199);
    temp13->SourceFileName(uString::Const("profile.ux"));
    temp5->SourceLineNumber(204);
    temp5->SourceFileName(uString::Const("profile.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp5->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp44);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp5->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp45);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp5->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp46);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp5->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp47);
    ::g::Fuse::Animations::Change__set_Value_fn(temp44, uCRef(1.0f));
    temp44->Duration(0.5);
    temp45->Frequency(1.0);
    temp45->Target(load);
    ::g::Fuse::Animations::Change__set_Value_fn(temp46, uCRef(0.0f));
    temp46->Duration(0.5);
    temp14->SourceLineNumber(204);
    temp14->SourceFileName(uString::Const("profile.ux"));
    uPtr(__g_nametable1)->This(this);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), router);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), ProfilePage);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), temp_eb23);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), busy);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), loadingPanel);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), load);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), errorMsg);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), temp_eb24);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), temp_eb25);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp15);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp16);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), ProfilePage);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), loadingPanel);
}

// public Profile New(Fuse.Navigation.Router router) [static] :349
Profile* Profile::New5(::g::Fuse::Navigation::Router* router1)
{
    Profile* obj1 = (Profile*)uNew(Profile_typeof());
    obj1->ctor_8(router1);
    return obj1;
}
// }

} // ::g
