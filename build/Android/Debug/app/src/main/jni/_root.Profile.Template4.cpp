// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/Profile.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.InputField.h>
#include <_root.OSP_InputField_-d24e30b7.h>
#include <_root.Profile.h>
#include <_root.Profile.Template4.h>
#include <Fuse.Binding.h>
#include <Fuse.Node.h>
#include <Fuse.Reactive.BindingMode.h>
#include <Fuse.Reactive.Data.h>
#include <Fuse.Reactive.DataBinding.h>
#include <Fuse.Reactive.Expression.h>
#include <Fuse.Reactive.IExpression.h>
#include <Uno.Bool.h>
#include <Uno.Collections.ICollection-1.h>
#include <Uno.Collections.IList-1.h>
#include <Uno.Int.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.UX.Property.h>
#include <Uno.UX.Property1-1.h>
#include <Uno.UX.Selector.h>
static uString* STRINGS[4];
static uType* TYPES[1];

namespace g{

// public partial sealed class Profile.Template4 :130
// {
// static Template4() :140
static void Profile__Template4__cctor__fn(uType* __type)
{
    Profile__Template4::__selector0_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[0/*"PlaceHolder...*/]);
}

static void Profile__Template4_build(uType* type)
{
    ::STRINGS[0] = uString::Const("PlaceHolderName");
    ::STRINGS[1] = uString::Const("nationality");
    ::STRINGS[2] = uString::Const("NATIONALITY");
    ::STRINGS[3] = uString::Const("profile.ux");
    ::TYPES[0] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL);
    type->SetFields(2,
        ::g::Profile_typeof(), offsetof(Profile__Template4, __parent1), uFieldFlagsWeak,
        ::g::Profile_typeof(), offsetof(Profile__Template4, __parentInstance1), uFieldFlagsWeak,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(Profile__Template4, __self_PlaceHolderName_inst1), 0,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Profile__Template4::__selector0_, uFieldFlagsStatic);
}

::g::Uno::UX::Template_type* Profile__Template4_typeof()
{
    static uSStrong< ::g::Uno::UX::Template_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::Template_typeof();
    options.FieldCount = 6;
    options.ObjectSize = sizeof(Profile__Template4);
    options.TypeSize = sizeof(::g::Uno::UX::Template_type);
    type = (::g::Uno::UX::Template_type*)uClassType::New("Profile.Template4", options);
    type->fp_build_ = Profile__Template4_build;
    type->fp_cctor_ = Profile__Template4__cctor__fn;
    type->fp_New1 = (void(*)(::g::Uno::UX::Template*, uObject**))Profile__Template4__New1_fn;
    return type;
}

// public Template4(Profile parent, Profile parentInstance) :134
void Profile__Template4__ctor_1_fn(Profile__Template4* __this, ::g::Profile* parent, ::g::Profile* parentInstance)
{
    __this->ctor_1(parent, parentInstance);
}

// public override sealed object New() :143
void Profile__Template4__New1_fn(Profile__Template4* __this, uObject** __retval)
{
    ::g::InputField* __self1 = ::g::InputField::New4();
    __this->__self_PlaceHolderName_inst1 = ::g::OSP_InputField_PlaceHolderName_Property::New1(__self1, Profile__Template4::__selector0_);
    ::g::Fuse::Reactive::Data* temp = ::g::Fuse::Reactive::Data::New1(::STRINGS[1/*"nationality"*/]);
    ::g::Fuse::Reactive::DataBinding* temp1 = ::g::Fuse::Reactive::DataBinding::New1(__this->__self_PlaceHolderName_inst1, (uObject*)temp, 3);
    __self1->Title(::STRINGS[2/*"NATIONALITY"*/]);
    __self1->SourceLineNumber(168);
    __self1->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    temp->SourceLineNumber(168);
    temp->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(__self1->Bindings()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp1);
    return *__retval = __self1, void();
}

// public Template4 New(Profile parent, Profile parentInstance) :134
void Profile__Template4__New2_fn(::g::Profile* parent, ::g::Profile* parentInstance, Profile__Template4** __retval)
{
    *__retval = Profile__Template4::New2(parent, parentInstance);
}

::g::Uno::UX::Selector Profile__Template4::__selector0_;

// public Template4(Profile parent, Profile parentInstance) [instance] :134
void Profile__Template4::ctor_1(::g::Profile* parent, ::g::Profile* parentInstance)
{
    ctor_(NULL, false);
    __parent1 = parent;
    __parentInstance1 = parentInstance;
}

// public Template4 New(Profile parent, Profile parentInstance) [static] :134
Profile__Template4* Profile__Template4::New2(::g::Profile* parent, ::g::Profile* parentInstance)
{
    Profile__Template4* obj1 = (Profile__Template4*)uNew(Profile__Template4_typeof());
    obj1->ctor_1(parent, parentInstance);
    return obj1;
}
// }

} // ::g
