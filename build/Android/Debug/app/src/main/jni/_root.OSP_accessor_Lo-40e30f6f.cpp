// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.LoginField.h>
#include <_root.OSP_accessor_Lo-40e30f6f.h>
#include <Uno.Bool.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.Type.h>
#include <Uno.UX.IPropertyListener.h>
#include <Uno.UX.PropertyObject.h>
#include <Uno.UX.Selector.h>
static uString* STRINGS[1];
static uType* TYPES[3];

namespace g{

// internal sealed class OSP_accessor_LoginField_PlaceHolder :211
// {
// static generated OSP_accessor_LoginField_PlaceHolder() :211
static void OSP_accessor_LoginField_PlaceHolder__cctor__fn(uType* __type)
{
    OSP_accessor_LoginField_PlaceHolder::Singleton_ = OSP_accessor_LoginField_PlaceHolder::New1();
    OSP_accessor_LoginField_PlaceHolder::_name_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[0/*"PlaceHolder"*/]);
}

static void OSP_accessor_LoginField_PlaceHolder_build(uType* type)
{
    ::STRINGS[0] = uString::Const("PlaceHolder");
    ::TYPES[0] = ::g::LoginField_typeof();
    ::TYPES[1] = ::g::Uno::String_typeof();
    ::TYPES[2] = ::g::Uno::Type_typeof();
    type->SetFields(0,
        ::g::Uno::UX::PropertyAccessor_typeof(), (uintptr_t)&OSP_accessor_LoginField_PlaceHolder::Singleton_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&OSP_accessor_LoginField_PlaceHolder::_name_, uFieldFlagsStatic);
}

::g::Uno::UX::PropertyAccessor_type* OSP_accessor_LoginField_PlaceHolder_typeof()
{
    static uSStrong< ::g::Uno::UX::PropertyAccessor_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::PropertyAccessor_typeof();
    options.FieldCount = 2;
    options.ObjectSize = sizeof(OSP_accessor_LoginField_PlaceHolder);
    options.TypeSize = sizeof(::g::Uno::UX::PropertyAccessor_type);
    type = (::g::Uno::UX::PropertyAccessor_type*)uClassType::New("OSP_accessor_LoginField_PlaceHolder", options);
    type->fp_build_ = OSP_accessor_LoginField_PlaceHolder_build;
    type->fp_ctor_ = (void*)OSP_accessor_LoginField_PlaceHolder__New1_fn;
    type->fp_cctor_ = OSP_accessor_LoginField_PlaceHolder__cctor__fn;
    type->fp_GetAsObject = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::PropertyObject*, uObject**))OSP_accessor_LoginField_PlaceHolder__GetAsObject_fn;
    type->fp_get_Name = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::Selector*))OSP_accessor_LoginField_PlaceHolder__get_Name_fn;
    type->fp_get_PropertyType = (void(*)(::g::Uno::UX::PropertyAccessor*, uType**))OSP_accessor_LoginField_PlaceHolder__get_PropertyType_fn;
    type->fp_SetAsObject = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::PropertyObject*, uObject*, uObject*))OSP_accessor_LoginField_PlaceHolder__SetAsObject_fn;
    type->fp_get_SupportsOriginSetter = (void(*)(::g::Uno::UX::PropertyAccessor*, bool*))OSP_accessor_LoginField_PlaceHolder__get_SupportsOriginSetter_fn;
    return type;
}

// public generated OSP_accessor_LoginField_PlaceHolder() :211
void OSP_accessor_LoginField_PlaceHolder__ctor_1_fn(OSP_accessor_LoginField_PlaceHolder* __this)
{
    __this->ctor_1();
}

// public override sealed object GetAsObject(Uno.UX.PropertyObject obj) :217
void OSP_accessor_LoginField_PlaceHolder__GetAsObject_fn(OSP_accessor_LoginField_PlaceHolder* __this, ::g::Uno::UX::PropertyObject* obj, uObject** __retval)
{
    return *__retval = uPtr(uCast< ::g::LoginField*>(obj, ::TYPES[0/*LoginField*/]))->PlaceHolder(), void();
}

// public override sealed Uno.UX.Selector get_Name() :214
void OSP_accessor_LoginField_PlaceHolder__get_Name_fn(OSP_accessor_LoginField_PlaceHolder* __this, ::g::Uno::UX::Selector* __retval)
{
    return *__retval = OSP_accessor_LoginField_PlaceHolder::_name_, void();
}

// public generated OSP_accessor_LoginField_PlaceHolder New() :211
void OSP_accessor_LoginField_PlaceHolder__New1_fn(OSP_accessor_LoginField_PlaceHolder** __retval)
{
    *__retval = OSP_accessor_LoginField_PlaceHolder::New1();
}

// public override sealed Uno.Type get_PropertyType() :216
void OSP_accessor_LoginField_PlaceHolder__get_PropertyType_fn(OSP_accessor_LoginField_PlaceHolder* __this, uType** __retval)
{
    return *__retval = ::TYPES[1/*string*/], void();
}

// public override sealed void SetAsObject(Uno.UX.PropertyObject obj, object v, Uno.UX.IPropertyListener origin) :218
void OSP_accessor_LoginField_PlaceHolder__SetAsObject_fn(OSP_accessor_LoginField_PlaceHolder* __this, ::g::Uno::UX::PropertyObject* obj, uObject* v, uObject* origin)
{
    uPtr(uCast< ::g::LoginField*>(obj, ::TYPES[0/*LoginField*/]))->SetPlaceHolder(uCast<uString*>(v, ::TYPES[1/*string*/]), origin);
}

// public override sealed bool get_SupportsOriginSetter() :219
void OSP_accessor_LoginField_PlaceHolder__get_SupportsOriginSetter_fn(OSP_accessor_LoginField_PlaceHolder* __this, bool* __retval)
{
    return *__retval = true, void();
}

uSStrong< ::g::Uno::UX::PropertyAccessor*> OSP_accessor_LoginField_PlaceHolder::Singleton_;
::g::Uno::UX::Selector OSP_accessor_LoginField_PlaceHolder::_name_;

// public generated OSP_accessor_LoginField_PlaceHolder() [instance] :211
void OSP_accessor_LoginField_PlaceHolder::ctor_1()
{
    ctor_();
}

// public generated OSP_accessor_LoginField_PlaceHolder New() [static] :211
OSP_accessor_LoginField_PlaceHolder* OSP_accessor_LoginField_PlaceHolder::New1()
{
    OSP_accessor_LoginField_PlaceHolder* obj1 = (OSP_accessor_LoginField_PlaceHolder*)uNew(OSP_accessor_LoginField_PlaceHolder_typeof());
    obj1->ctor_1();
    return obj1;
}
// }

} // ::g
