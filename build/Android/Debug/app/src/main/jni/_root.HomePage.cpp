// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/HomePage.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.HomePage.h>
#include <_root.LoginField.h>
#include <_root.MainView.h>
#include <_root.OSP_bundle.h>
#include <_root.OSP_FuseControl-4d1c57a9.h>
#include <_root.OSP_FuseControl-8faf8b28.h>
#include <_root.OSP_FuseControl-b26a5bbf.h>
#include <_root.OSP_FuseElement-78dfea6e.h>
#include <_root.SubButton.h>
#include <Fuse.Animations.Animator.h>
#include <Fuse.Animations.Spin.h>
#include <Fuse.Controls.DockPanel.h>
#include <Fuse.Controls.Grid.h>
#include <Fuse.Controls.Image.h>
#include <Fuse.Controls.Panel.h>
#include <Fuse.Controls.Rectangle.h>
#include <Fuse.Controls.Shape.h>
#include <Fuse.Controls.StackPanel.h>
#include <Fuse.Controls.Text.h>
#include <Fuse.Controls.TextAlignment.h>
#include <Fuse.Controls.TextControl.h>
#include <Fuse.Controls.TextInput.h>
#include <Fuse.Controls.TextInputControl.h>
#include <Fuse.Elements.Alignment.h>
#include <Fuse.Elements.Element.h>
#include <Fuse.Font.h>
#include <Fuse.Gestures.Clicked.h>
#include <Fuse.Gestures.ClickedHandler.h>
#include <Fuse.Layouts.Dock.h>
#include <Fuse.Navigation.Router.h>
#include <Fuse.Reactive.BindingMode.h>
#include <Fuse.Reactive.Data.h>
#include <Fuse.Reactive.DataBinding.h>
#include <Fuse.Reactive.EventBinding.h>
#include <Fuse.Reactive.Expression.h>
#include <Fuse.Reactive.IExpression.h>
#include <Fuse.Reactive.JavaScript.h>
#include <Fuse.Triggers.Trigger.h>
#include <Fuse.Triggers.WhileBool.h>
#include <Fuse.Triggers.WhileTrue.h>
#include <Uno.Bool.h>
#include <Uno.Double.h>
#include <Uno.Float.h>
#include <Uno.Int.h>
#include <Uno.IO.BundleFile.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.UX.BundleFileSource.h>
#include <Uno.UX.FileSource.h>
#include <Uno.UX.NameTable.h>
#include <Uno.UX.Property.h>
#include <Uno.UX.Property1-1.h>
#include <Uno.UX.Selector.h>
#include <Uno.UX.Size.h>
#include <Uno.UX.Unit.h>
static uString* STRINGS[28];
static uType* TYPES[6];

namespace g{

// public partial sealed class HomePage :2
// {
// static HomePage() :27
static void HomePage__cctor_4_fn(uType* __type)
{
    HomePage::__g_static_nametable1_ = uArray::Init<uString*>(::TYPES[0/*string[]*/], 7, ::STRINGS[0/*"router"*/], ::STRINGS[1/*"pageContent"*/], ::STRINGS[2/*"temp_eb16"*/], ::STRINGS[3/*"temp_eb17"*/], ::STRINGS[4/*"temp_eb18"*/], ::STRINGS[5/*"loading"*/], ::STRINGS[6/*"load"*/]);
    HomePage::__selector0_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[7/*"Value"*/]);
    HomePage::__selector1_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[8/*"TextColor"*/]);
    HomePage::__selector2_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[9/*"Opacity"*/]);
    HomePage::__selector3_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[1/*"pageContent"*/]);
    HomePage::__selector4_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[5/*"loading"*/]);
    HomePage::__selector5_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[6/*"load"*/]);
}

static void HomePage_build(uType* type)
{
    ::STRINGS[0] = uString::Const("router");
    ::STRINGS[1] = uString::Const("pageContent");
    ::STRINGS[2] = uString::Const("temp_eb16");
    ::STRINGS[3] = uString::Const("temp_eb17");
    ::STRINGS[4] = uString::Const("temp_eb18");
    ::STRINGS[5] = uString::Const("loading");
    ::STRINGS[6] = uString::Const("load");
    ::STRINGS[7] = uString::Const("Value");
    ::STRINGS[8] = uString::Const("TextColor");
    ::STRINGS[9] = uString::Const("Opacity");
    ::STRINGS[10] = uString::Const("msg");
    ::STRINGS[11] = uString::Const("msgColor");
    ::STRINGS[12] = uString::Const("OSPID");
    ::STRINGS[13] = uString::Const("password");
    ::STRINGS[14] = uString::Const("login_val");
    ::STRINGS[15] = uString::Const("signup");
    ::STRINGS[16] = uString::Const("forgotPassword");
    ::STRINGS[17] = uString::Const("PageOpacity");
    ::STRINGS[18] = uString::Const("LoaderOpacity");
    ::STRINGS[19] = uString::Const("homepage.ux");
    ::STRINGS[20] = uString::Const("1*,4*");
    ::STRINGS[21] = uString::Const("3*,3*");
    ::STRINGS[22] = uString::Const("LOGIN");
    ::STRINGS[23] = uString::Const("OSP ID");
    ::STRINGS[24] = uString::Const("Password");
    ::STRINGS[25] = uString::Const("SUBMIT");
    ::STRINGS[26] = uString::Const("Sign Up");
    ::STRINGS[27] = uString::Const("Forgot Password?");
    ::TYPES[0] = ::g::Uno::String_typeof()->Array();
    ::TYPES[1] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL);
    ::TYPES[2] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL);
    ::TYPES[3] = ::g::Fuse::Gestures::ClickedHandler_typeof();
    ::TYPES[4] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL);
    ::TYPES[5] = ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL);
    type->SetDependencies(
        ::g::MainView_typeof(),
        ::g::OSP_bundle_typeof());
    type->SetInterfaces(
        ::g::Uno::Collections::IList_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface0),
        ::g::Fuse::Scripting::IScriptObject_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface1),
        ::g::Fuse::IProperties_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface2),
        ::g::Fuse::INotifyUnrooted_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface3),
        ::g::Fuse::ISourceLocation_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface4),
        ::TYPES[2/*Uno.Collections.ICollection<Fuse.Binding>*/], offsetof(::g::Fuse::Controls::Panel_type, interface5),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface6),
        ::g::Uno::Collections::IList_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface7),
        ::g::Uno::UX::IPropertyListener_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface8),
        ::g::Fuse::ITemplateSource_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface9),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Visual_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface10),
        ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/], offsetof(::g::Fuse::Controls::Panel_type, interface11),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface12),
        ::g::Fuse::Triggers::Actions::IShow_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface13),
        ::g::Fuse::Triggers::Actions::IHide_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface14),
        ::g::Fuse::Triggers::Actions::ICollapse_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface15),
        ::g::Fuse::IActualPlacement_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface16),
        ::g::Fuse::Animations::IResize_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface17),
        ::g::Fuse::Drawing::ISurfaceDrawable_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface18));
    type->SetFields(121,
        ::g::Fuse::Navigation::Router_typeof(), offsetof(HomePage, router), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(HomePage, temp_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL), offsetof(HomePage, temp_TextColor_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(HomePage, temp1_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(HomePage, temp2_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(HomePage, pageContent_Opacity_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(HomePage, loading_Opacity_inst), 0,
        ::g::Fuse::Controls::DockPanel_typeof(), offsetof(HomePage, pageContent), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(HomePage, temp_eb16), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(HomePage, temp_eb17), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(HomePage, temp_eb18), 0,
        ::g::Fuse::Controls::DockPanel_typeof(), offsetof(HomePage, loading), 0,
        ::g::Fuse::Controls::Image_typeof(), offsetof(HomePage, load), 0,
        ::g::Uno::UX::NameTable_typeof(), offsetof(HomePage, __g_nametable1), 0,
        ::TYPES[0/*string[]*/], (uintptr_t)&HomePage::__g_static_nametable1_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&HomePage::__selector0_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&HomePage::__selector1_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&HomePage::__selector2_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&HomePage::__selector3_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&HomePage::__selector4_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&HomePage::__selector5_, uFieldFlagsStatic);
}

::g::Fuse::Controls::Panel_type* HomePage_typeof()
{
    static uSStrong< ::g::Fuse::Controls::Panel_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Fuse::Controls::Page_typeof();
    options.FieldCount = 142;
    options.InterfaceCount = 19;
    options.DependencyCount = 2;
    options.ObjectSize = sizeof(HomePage);
    options.TypeSize = sizeof(::g::Fuse::Controls::Panel_type);
    type = (::g::Fuse::Controls::Panel_type*)uClassType::New("HomePage", options);
    type->fp_build_ = HomePage_build;
    type->fp_cctor_ = HomePage__cctor_4_fn;
    type->interface18.fp_Draw = (void(*)(uObject*, ::g::Fuse::Drawing::Surface*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableDraw_fn;
    type->interface18.fp_get_IsPrimary = (void(*)(uObject*, bool*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableget_IsPrimary_fn;
    type->interface18.fp_get_ElementSize = (void(*)(uObject*, ::g::Uno::Float2*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableget_ElementSize_fn;
    type->interface13.fp_Show = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsIShowShow_fn;
    type->interface15.fp_Collapse = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsICollapseCollapse_fn;
    type->interface14.fp_Hide = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsIHideHide_fn;
    type->interface17.fp_SetSize = (void(*)(uObject*, ::g::Uno::Float2*))::g::Fuse::Elements::Element__FuseAnimationsIResizeSetSize_fn;
    type->interface16.fp_get_ActualSize = (void(*)(uObject*, ::g::Uno::Float3*))::g::Fuse::Elements::Element__FuseIActualPlacementget_ActualSize_fn;
    type->interface16.fp_add_Placed = (void(*)(uObject*, uDelegate*))::g::Fuse::Elements::Element__add_Placed_fn;
    type->interface16.fp_remove_Placed = (void(*)(uObject*, uDelegate*))::g::Fuse::Elements::Element__remove_Placed_fn;
    type->interface10.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Visual__UnoCollectionsIEnumerableFuseVisualGetEnumerator_fn;
    type->interface11.fp_Clear = (void(*)(uObject*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeClear_fn;
    type->interface11.fp_Contains = (void(*)(uObject*, void*, bool*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeContains_fn;
    type->interface7.fp_RemoveAt = (void(*)(uObject*, int32_t*))::g::Fuse::Visual__UnoCollectionsIListFuseNodeRemoveAt_fn;
    type->interface12.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Visual__UnoCollectionsIEnumerableFuseNodeGetEnumerator_fn;
    type->interface11.fp_get_Count = (void(*)(uObject*, int32_t*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeget_Count_fn;
    type->interface7.fp_get_Item = (void(*)(uObject*, int32_t*, uTRef))::g::Fuse::Visual__UnoCollectionsIListFuseNodeget_Item_fn;
    type->interface7.fp_Insert = (void(*)(uObject*, int32_t*, void*))::g::Fuse::Visual__Insert1_fn;
    type->interface8.fp_OnPropertyChanged = (void(*)(uObject*, ::g::Uno::UX::PropertyObject*, ::g::Uno::UX::Selector*))::g::Fuse::Controls::Control__OnPropertyChanged2_fn;
    type->interface9.fp_FindTemplate = (void(*)(uObject*, uString*, ::g::Uno::UX::Template**))::g::Fuse::Visual__FindTemplate_fn;
    type->interface11.fp_Add = (void(*)(uObject*, void*))::g::Fuse::Visual__Add1_fn;
    type->interface11.fp_Remove = (void(*)(uObject*, void*, bool*))::g::Fuse::Visual__Remove1_fn;
    type->interface5.fp_Clear = (void(*)(uObject*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingClear_fn;
    type->interface5.fp_Contains = (void(*)(uObject*, void*, bool*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingContains_fn;
    type->interface0.fp_RemoveAt = (void(*)(uObject*, int32_t*))::g::Fuse::Node__UnoCollectionsIListFuseBindingRemoveAt_fn;
    type->interface6.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Node__UnoCollectionsIEnumerableFuseBindingGetEnumerator_fn;
    type->interface1.fp_SetScriptObject = (void(*)(uObject*, uObject*, ::g::Fuse::Scripting::Context*))::g::Fuse::Node__FuseScriptingIScriptObjectSetScriptObject_fn;
    type->interface5.fp_get_Count = (void(*)(uObject*, int32_t*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingget_Count_fn;
    type->interface0.fp_get_Item = (void(*)(uObject*, int32_t*, uTRef))::g::Fuse::Node__UnoCollectionsIListFuseBindingget_Item_fn;
    type->interface1.fp_get_ScriptObject = (void(*)(uObject*, uObject**))::g::Fuse::Node__FuseScriptingIScriptObjectget_ScriptObject_fn;
    type->interface1.fp_get_ScriptContext = (void(*)(uObject*, ::g::Fuse::Scripting::Context**))::g::Fuse::Node__FuseScriptingIScriptObjectget_ScriptContext_fn;
    type->interface4.fp_get_SourceNearest = (void(*)(uObject*, uObject**))::g::Fuse::Node__FuseISourceLocationget_SourceNearest_fn;
    type->interface3.fp_add_Unrooted = (void(*)(uObject*, uDelegate*))::g::Fuse::Node__FuseINotifyUnrootedadd_Unrooted_fn;
    type->interface3.fp_remove_Unrooted = (void(*)(uObject*, uDelegate*))::g::Fuse::Node__FuseINotifyUnrootedremove_Unrooted_fn;
    type->interface0.fp_Insert = (void(*)(uObject*, int32_t*, void*))::g::Fuse::Node__Insert_fn;
    type->interface2.fp_get_Properties = (void(*)(uObject*, ::g::Fuse::Properties**))::g::Fuse::Node__get_Properties_fn;
    type->interface4.fp_get_SourceLineNumber = (void(*)(uObject*, int32_t*))::g::Fuse::Node__get_SourceLineNumber_fn;
    type->interface4.fp_get_SourceFileName = (void(*)(uObject*, uString**))::g::Fuse::Node__get_SourceFileName_fn;
    type->interface5.fp_Add = (void(*)(uObject*, void*))::g::Fuse::Node__Add_fn;
    type->interface5.fp_Remove = (void(*)(uObject*, void*, bool*))::g::Fuse::Node__Remove_fn;
    return type;
}

// public HomePage(Fuse.Navigation.Router router) :31
void HomePage__ctor_8_fn(HomePage* __this, ::g::Fuse::Navigation::Router* router1)
{
    __this->ctor_8(router1);
}

// private void InitializeUX() :37
void HomePage__InitializeUX_fn(HomePage* __this)
{
    __this->InitializeUX();
}

// public HomePage New(Fuse.Navigation.Router router) :31
void HomePage__New5_fn(::g::Fuse::Navigation::Router* router1, HomePage** __retval)
{
    *__retval = HomePage::New5(router1);
}

uSStrong<uArray*> HomePage::__g_static_nametable1_;
::g::Uno::UX::Selector HomePage::__selector0_;
::g::Uno::UX::Selector HomePage::__selector1_;
::g::Uno::UX::Selector HomePage::__selector2_;
::g::Uno::UX::Selector HomePage::__selector3_;
::g::Uno::UX::Selector HomePage::__selector4_;
::g::Uno::UX::Selector HomePage::__selector5_;

// public HomePage(Fuse.Navigation.Router router) [instance] :31
void HomePage::ctor_8(::g::Fuse::Navigation::Router* router1)
{
    ctor_7();
    router = router1;
    InitializeUX();
}

// private void InitializeUX() [instance] :37
void HomePage::InitializeUX()
{
    __g_nametable1 = ::g::Uno::UX::NameTable::New1(NULL, HomePage::__g_static_nametable1_);
    ::g::Fuse::Controls::Text* temp = ::g::Fuse::Controls::Text::New3();
    temp_Value_inst = ::g::OSP_FuseControlsTextControl_Value_Property::New1(temp, HomePage::__selector0_);
    ::g::Fuse::Reactive::Data* temp3 = ::g::Fuse::Reactive::Data::New1(::STRINGS[10/*"msg"*/]);
    temp_TextColor_inst = ::g::OSP_FuseControlsTextControl_TextColor_Property::New1(temp, HomePage::__selector1_);
    ::g::Fuse::Reactive::Data* temp4 = ::g::Fuse::Reactive::Data::New1(::STRINGS[11/*"msgColor"*/]);
    ::g::LoginField* temp1 = ::g::LoginField::New4();
    temp1_Value_inst = ::g::OSP_FuseControlsTextInputControl_Value_Property::New1(temp1, HomePage::__selector0_);
    ::g::Fuse::Reactive::Data* temp5 = ::g::Fuse::Reactive::Data::New1(::STRINGS[12/*"OSPID"*/]);
    ::g::LoginField* temp2 = ::g::LoginField::New4();
    temp2_Value_inst = ::g::OSP_FuseControlsTextInputControl_Value_Property::New1(temp2, HomePage::__selector0_);
    ::g::Fuse::Reactive::Data* temp6 = ::g::Fuse::Reactive::Data::New1(::STRINGS[13/*"password"*/]);
    ::g::Fuse::Reactive::Data* temp7 = ::g::Fuse::Reactive::Data::New1(::STRINGS[14/*"login_val"*/]);
    ::g::Fuse::Reactive::Data* temp8 = ::g::Fuse::Reactive::Data::New1(::STRINGS[15/*"signup"*/]);
    ::g::Fuse::Reactive::Data* temp9 = ::g::Fuse::Reactive::Data::New1(::STRINGS[16/*"forgotPassw...*/]);
    pageContent = ::g::Fuse::Controls::DockPanel::New4();
    pageContent_Opacity_inst = ::g::OSP_FuseElementsElement_Opacity_Property::New1(pageContent, HomePage::__selector2_);
    ::g::Fuse::Reactive::Data* temp10 = ::g::Fuse::Reactive::Data::New1(::STRINGS[17/*"PageOpacity"*/]);
    loading = ::g::Fuse::Controls::DockPanel::New4();
    loading_Opacity_inst = ::g::OSP_FuseElementsElement_Opacity_Property::New1(loading, HomePage::__selector2_);
    ::g::Fuse::Reactive::Data* temp11 = ::g::Fuse::Reactive::Data::New1(::STRINGS[18/*"LoaderOpacity"*/]);
    ::g::Fuse::Reactive::JavaScript* temp12 = ::g::Fuse::Reactive::JavaScript::New2(__g_nametable1);
    ::g::Fuse::Controls::Grid* temp13 = ::g::Fuse::Controls::Grid::New4();
    ::g::Fuse::Controls::Image* temp14 = ::g::Fuse::Controls::Image::New3();
    ::g::Fuse::Controls::Grid* temp15 = ::g::Fuse::Controls::Grid::New4();
    ::g::Fuse::Controls::Image* temp16 = ::g::Fuse::Controls::Image::New3();
    ::g::Fuse::Controls::DockPanel* temp17 = ::g::Fuse::Controls::DockPanel::New4();
    ::g::Fuse::Controls::Rectangle* temp18 = ::g::Fuse::Controls::Rectangle::New3();
    ::g::Fuse::Controls::DockPanel* temp19 = ::g::Fuse::Controls::DockPanel::New4();
    ::g::Fuse::Controls::Rectangle* temp20 = ::g::Fuse::Controls::Rectangle::New3();
    ::g::Fuse::Controls::Text* temp21 = ::g::Fuse::Controls::Text::New3();
    ::g::Fuse::Controls::StackPanel* temp22 = ::g::Fuse::Controls::StackPanel::New4();
    ::g::Fuse::Reactive::DataBinding* temp23 = ::g::Fuse::Reactive::DataBinding::New1(temp_Value_inst, (uObject*)temp3, 3);
    ::g::Fuse::Reactive::DataBinding* temp24 = ::g::Fuse::Reactive::DataBinding::New1(temp_TextColor_inst, (uObject*)temp4, 3);
    ::g::Fuse::Reactive::DataBinding* temp25 = ::g::Fuse::Reactive::DataBinding::New1(temp1_Value_inst, (uObject*)temp5, 3);
    ::g::Fuse::Reactive::DataBinding* temp26 = ::g::Fuse::Reactive::DataBinding::New1(temp2_Value_inst, (uObject*)temp6, 3);
    ::g::SubButton* temp27 = ::g::SubButton::New4();
    temp_eb16 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp7);
    ::g::Fuse::Controls::DockPanel* temp28 = ::g::Fuse::Controls::DockPanel::New4();
    ::g::Fuse::Controls::Text* temp29 = ::g::Fuse::Controls::Text::New3();
    temp_eb17 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp8);
    ::g::Fuse::Controls::Text* temp30 = ::g::Fuse::Controls::Text::New3();
    temp_eb18 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp9);
    ::g::Fuse::Reactive::DataBinding* temp31 = ::g::Fuse::Reactive::DataBinding::New1(pageContent_Opacity_inst, (uObject*)temp10, 3);
    ::g::Fuse::Triggers::WhileTrue* temp32 = ::g::Fuse::Triggers::WhileTrue::New2();
    ::g::Fuse::Animations::Spin* temp33 = ::g::Fuse::Animations::Spin::New2();
    ::g::Fuse::Controls::Grid* temp34 = ::g::Fuse::Controls::Grid::New4();
    load = ::g::Fuse::Controls::Image::New3();
    ::g::Fuse::Reactive::DataBinding* temp35 = ::g::Fuse::Reactive::DataBinding::New1(loading_Opacity_inst, (uObject*)temp11, 3);
    Color(::g::Uno::Float4__New2(1.0f, 1.0f, 1.0f, 1.0f));
    SourceLineNumber(1);
    SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    temp12->LineNumber(3);
    temp12->FileName(::STRINGS[19/*"homepage.ux"*/]);
    temp12->SourceLineNumber(3);
    temp12->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    temp12->File(::g::Uno::UX::BundleFileSource::New1(::g::OSP_bundle::navigationb176862e()));
    uPtr(pageContent)->Name(HomePage::__selector3_);
    uPtr(pageContent)->SourceLineNumber(35);
    uPtr(pageContent)->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(pageContent)->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp13);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(pageContent)->Bindings()), ::TYPES[2/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp31);
    temp13->Rows(::STRINGS[20/*"1*,4*"*/]);
    temp13->Padding(::g::Uno::Float4__New2(0.0f, 30.0f, 0.0f, 30.0f));
    temp13->SourceLineNumber(37);
    temp13->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp13->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp14);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp13->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp15);
    temp14->Width(::g::Uno::UX::Size__New1(55.0f, 4));
    temp14->SourceLineNumber(38);
    temp14->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    temp14->File(::g::Uno::UX::BundleFileSource::New1(::g::OSP_bundle::dprb1594be8()));
    temp15->Rows(::STRINGS[21/*"3*,3*"*/]);
    temp15->SourceLineNumber(39);
    temp15->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp15->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp16);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp15->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp17);
    temp16->Width(::g::Uno::UX::Size__New1(100.0f, 4));
    temp16->Alignment(6);
    temp16->SourceLineNumber(41);
    temp16->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    temp16->File(::g::Uno::UX::BundleFileSource::New1(::g::OSP_bundle::home1fe12191()));
    temp17->Color(::g::Uno::Float4__New2(0.1137255f, 0.4901961f, 0.2588235f, 1.0f));
    temp17->Width(::g::Uno::UX::Size__New1(80.0f, 4));
    temp17->Height(::g::Uno::UX::Size__New1(100.0f, 4));
    temp17->Alignment(4);
    temp17->SourceLineNumber(45);
    temp17->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp17->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp18);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp17->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp22);
    temp18->Color(::g::Uno::Float4__New2(0.9490196f, 0.9490196f, 0.9490196f, 1.0f));
    temp18->Height(::g::Uno::UX::Size__New1(13.0f, 4));
    temp18->Alignment(4);
    temp18->Padding(::g::Uno::Float4__New2(0.0f, 7.0f, 0.0f, 0.0f));
    temp18->SourceLineNumber(47);
    temp18->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp18->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp19);
    temp19->SourceLineNumber(48);
    temp19->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp19->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp20);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp19->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp21);
    temp20->Color(::g::Uno::Float4__New2(0.9686275f, 0.8980392f, 0.372549f, 1.0f));
    temp20->Height(::g::Uno::UX::Size__New1(1.0f, 1));
    temp20->Margin(::g::Uno::Float4__New2(0.0f, 7.0f, 0.0f, 0.0f));
    temp20->Padding(::g::Uno::Float4__New2(0.0f, 0.0f, 0.0f, 0.0f));
    temp20->SourceLineNumber(49);
    temp20->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    ::g::Fuse::Controls::DockPanel::SetDock(temp20, 3);
    temp21->Value(::STRINGS[22/*"LOGIN"*/]);
    temp21->TextAlignment(1);
    temp21->TextColor(::g::Uno::Float4__New2(0.0f, 0.0f, 0.0f, 1.0f));
    temp21->Alignment(10);
    temp21->SourceLineNumber(50);
    temp21->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    temp21->Font(::g::MainView::Raleway());
    temp22->Width(::g::Uno::UX::Size__New1(100.0f, 4));
    temp22->Alignment(10);
    temp22->Margin(::g::Uno::Float4__New2(0.0f, 10.0f, 0.0f, 0.0f));
    temp22->SourceLineNumber(54);
    temp22->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp22->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp22->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp1);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp22->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp2);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp22->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp27);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp22->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp28);
    temp->FontSize(12.0f);
    temp->TextAlignment(1);
    temp->Margin(::g::Uno::Float4__New2(0.0f, 20.0f, 0.0f, 0.0f));
    temp->SourceLineNumber(55);
    temp->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Bindings()), ::TYPES[2/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp23);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Bindings()), ::TYPES[2/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp24);
    temp3->SourceLineNumber(55);
    temp3->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    temp4->SourceLineNumber(55);
    temp4->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    temp1->PlaceHolder(::STRINGS[23/*"OSP ID"*/]);
    temp1->PlaceHolderColor(::g::Uno::Float4__New2(0.5490196f, 0.5843138f, 0.6313726f, 1.0f));
    temp1->BgColor(::g::Uno::Float4__New2(1.0f, 1.0f, 1.0f, 1.0f));
    temp1->StrokeColor(::g::Uno::Float4__New2(0.1137255f, 0.4901961f, 0.2588235f, 1.0f));
    temp1->Height(::g::Uno::UX::Size__New1(38.0f, 1));
    temp1->Padding(::g::Uno::Float4__New2(20.0f, 5.0f, 20.0f, 5.0f));
    temp1->SourceLineNumber(56);
    temp1->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp1->Bindings()), ::TYPES[2/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp25);
    temp5->SourceLineNumber(56);
    temp5->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    temp2->PlaceHolder(::STRINGS[24/*"Password"*/]);
    temp2->PlaceHolderColor(::g::Uno::Float4__New2(0.5490196f, 0.5843138f, 0.6313726f, 1.0f));
    temp2->BgColor(::g::Uno::Float4__New2(1.0f, 1.0f, 1.0f, 1.0f));
    temp2->StrokeColor(::g::Uno::Float4__New2(0.1137255f, 0.4901961f, 0.2588235f, 1.0f));
    temp2->IsPassword(true);
    temp2->Height(::g::Uno::UX::Size__New1(38.0f, 1));
    temp2->Padding(::g::Uno::Float4__New2(20.0f, 5.0f, 20.0f, 5.0f));
    temp2->SourceLineNumber(57);
    temp2->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp2->Bindings()), ::TYPES[2/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp26);
    temp6->SourceLineNumber(57);
    temp6->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    temp27->ButtonName(::STRINGS[25/*"SUBMIT"*/]);
    temp27->ButtonTextColor(::g::Uno::Float4__New2(0.0f, 0.0f, 0.0f, 1.0f));
    temp27->Color(::g::Uno::Float4__New2(0.9686275f, 0.8980392f, 0.372549f, 1.0f));
    temp27->SourceLineNumber(59);
    temp27->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    ::g::Fuse::Gestures::Clicked::AddHandler(temp27, uDelegate::New(::TYPES[3/*Fuse.Gestures.ClickedHandler*/], (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb16)));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp27->Bindings()), ::TYPES[2/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp_eb16);
    temp7->SourceLineNumber(59);
    temp7->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    temp28->Width(::g::Uno::UX::Size__New1(80.0f, 4));
    temp28->Margin(::g::Uno::Float4__New2(0.0f, 3.0f, 0.0f, 2.0f));
    temp28->SourceLineNumber(63);
    temp28->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp28->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp29);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp28->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp30);
    temp29->Value(::STRINGS[26/*"Sign Up"*/]);
    temp29->FontSize(12.0f);
    temp29->Color(::g::Uno::Float4__New2(1.0f, 1.0f, 1.0f, 1.0f));
    temp29->Alignment(15);
    temp29->SourceLineNumber(64);
    temp29->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    ::g::Fuse::Controls::DockPanel::SetDock(temp29, 0);
    ::g::Fuse::Gestures::Clicked::AddHandler(temp29, uDelegate::New(::TYPES[3/*Fuse.Gestures.ClickedHandler*/], (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb17)));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp29->Bindings()), ::TYPES[2/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp_eb17);
    temp8->SourceLineNumber(64);
    temp8->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    temp30->Value(::STRINGS[27/*"Forgot Pass...*/]);
    temp30->FontSize(12.0f);
    temp30->Color(::g::Uno::Float4__New2(1.0f, 1.0f, 1.0f, 1.0f));
    temp30->Alignment(15);
    temp30->SourceLineNumber(65);
    temp30->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    ::g::Fuse::Controls::DockPanel::SetDock(temp30, 1);
    ::g::Fuse::Gestures::Clicked::AddHandler(temp30, uDelegate::New(::TYPES[3/*Fuse.Gestures.ClickedHandler*/], (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb18)));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp30->Bindings()), ::TYPES[2/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp_eb18);
    temp9->SourceLineNumber(65);
    temp9->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    temp10->SourceLineNumber(35);
    temp10->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    uPtr(loading)->Color(::g::Uno::Float4__New2(1.0f, 1.0f, 1.0f, 1.0f));
    uPtr(loading)->Name(HomePage::__selector4_);
    uPtr(loading)->SourceLineNumber(77);
    uPtr(loading)->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(loading)->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp32);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(loading)->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp34);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(loading)->Bindings()), ::TYPES[2/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp35);
    temp32->Value1(true);
    temp32->SourceLineNumber(78);
    temp32->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp32->Animators()), ::TYPES[4/*Uno.Collections.ICollection<Fuse.Animations.Animator>*/]), temp33);
    temp33->Frequency(1.0);
    temp33->Target(load);
    temp34->RowCount(2);
    temp34->SourceLineNumber(82);
    temp34->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp34->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), load);
    uPtr(load)->Width(::g::Uno::UX::Size__New1(50.0f, 1));
    uPtr(load)->Height(::g::Uno::UX::Size__New1(50.0f, 1));
    uPtr(load)->Alignment(10);
    uPtr(load)->Opacity(1.0f);
    uPtr(load)->Name(HomePage::__selector5_);
    uPtr(load)->SourceLineNumber(83);
    uPtr(load)->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    uPtr(load)->File(::g::Uno::UX::BundleFileSource::New1(::g::OSP_bundle::diskc6e80153()));
    temp11->SourceLineNumber(77);
    temp11->SourceFileName(::STRINGS[19/*"homepage.ux"*/]);
    uPtr(__g_nametable1)->This(this);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::TYPES[5/*Uno.Collections.ICollection<object>*/]), router);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::TYPES[5/*Uno.Collections.ICollection<object>*/]), pageContent);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::TYPES[5/*Uno.Collections.ICollection<object>*/]), temp_eb16);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::TYPES[5/*Uno.Collections.ICollection<object>*/]), temp_eb17);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::TYPES[5/*Uno.Collections.ICollection<object>*/]), temp_eb18);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::TYPES[5/*Uno.Collections.ICollection<object>*/]), loading);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::TYPES[5/*Uno.Collections.ICollection<object>*/]), load);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp12);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), pageContent);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), loading);
}

// public HomePage New(Fuse.Navigation.Router router) [static] :31
HomePage* HomePage::New5(::g::Fuse::Navigation::Router* router1)
{
    HomePage* obj1 = (HomePage*)uNew(HomePage_typeof());
    obj1->ctor_8(router1);
    return obj1;
}
// }

} // ::g
