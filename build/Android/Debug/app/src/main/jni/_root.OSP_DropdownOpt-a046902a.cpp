// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.DropdownOption.h>
#include <_root.OSP_DropdownOpt-a046902a.h>
#include <Uno.Bool.h>
#include <Uno.UX.IPropertyListener.h>
#include <Uno.UX.PropertyObject.h>
#include <Uno.UX.Selector.h>
static uType* TYPES[1];

namespace g{

// internal sealed class OSP_DropdownOption_BorderColor_Property :592
// {
static void OSP_DropdownOption_BorderColor_Property_build(uType* type)
{
    ::TYPES[0] = ::g::DropdownOption_typeof();
    type->SetBase(::g::Uno::UX::Property1_typeof()->MakeType(::g::Fuse::Drawing::Brush_typeof(), NULL));
    type->SetFields(1,
        ::TYPES[0/*DropdownOption*/], offsetof(OSP_DropdownOption_BorderColor_Property, _obj), uFieldFlagsWeak);
}

::g::Uno::UX::Property1_type* OSP_DropdownOption_BorderColor_Property_typeof()
{
    static uSStrong< ::g::Uno::UX::Property1_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::Property1_typeof();
    options.FieldCount = 2;
    options.ObjectSize = sizeof(OSP_DropdownOption_BorderColor_Property);
    options.TypeSize = sizeof(::g::Uno::UX::Property1_type);
    type = (::g::Uno::UX::Property1_type*)uClassType::New("OSP_DropdownOption_BorderColor_Property", options);
    type->fp_build_ = OSP_DropdownOption_BorderColor_Property_build;
    type->fp_Get1 = (void(*)(::g::Uno::UX::Property1*, ::g::Uno::UX::PropertyObject*, uTRef))OSP_DropdownOption_BorderColor_Property__Get1_fn;
    type->fp_get_Object = (void(*)(::g::Uno::UX::Property*, ::g::Uno::UX::PropertyObject**))OSP_DropdownOption_BorderColor_Property__get_Object_fn;
    type->fp_Set1 = (void(*)(::g::Uno::UX::Property1*, ::g::Uno::UX::PropertyObject*, void*, uObject*))OSP_DropdownOption_BorderColor_Property__Set1_fn;
    type->fp_get_SupportsOriginSetter = (void(*)(::g::Uno::UX::PropertyAccessor*, bool*))OSP_DropdownOption_BorderColor_Property__get_SupportsOriginSetter_fn;
    return type;
}

// public OSP_DropdownOption_BorderColor_Property(DropdownOption obj, Uno.UX.Selector name) :595
void OSP_DropdownOption_BorderColor_Property__ctor_3_fn(OSP_DropdownOption_BorderColor_Property* __this, ::g::DropdownOption* obj, ::g::Uno::UX::Selector* name)
{
    __this->ctor_3(obj, *name);
}

// public override sealed Fuse.Drawing.Brush Get(Uno.UX.PropertyObject obj) :597
void OSP_DropdownOption_BorderColor_Property__Get1_fn(OSP_DropdownOption_BorderColor_Property* __this, ::g::Uno::UX::PropertyObject* obj, ::g::Fuse::Drawing::Brush** __retval)
{
    return *__retval = uPtr(uCast< ::g::DropdownOption*>(obj, ::TYPES[0/*DropdownOption*/]))->BorderColor(), void();
}

// public OSP_DropdownOption_BorderColor_Property New(DropdownOption obj, Uno.UX.Selector name) :595
void OSP_DropdownOption_BorderColor_Property__New1_fn(::g::DropdownOption* obj, ::g::Uno::UX::Selector* name, OSP_DropdownOption_BorderColor_Property** __retval)
{
    *__retval = OSP_DropdownOption_BorderColor_Property::New1(obj, *name);
}

// public override sealed Uno.UX.PropertyObject get_Object() :596
void OSP_DropdownOption_BorderColor_Property__get_Object_fn(OSP_DropdownOption_BorderColor_Property* __this, ::g::Uno::UX::PropertyObject** __retval)
{
    return *__retval = __this->_obj, void();
}

// public override sealed void Set(Uno.UX.PropertyObject obj, Fuse.Drawing.Brush v, Uno.UX.IPropertyListener origin) :598
void OSP_DropdownOption_BorderColor_Property__Set1_fn(OSP_DropdownOption_BorderColor_Property* __this, ::g::Uno::UX::PropertyObject* obj, ::g::Fuse::Drawing::Brush* v, uObject* origin)
{
    uPtr(uCast< ::g::DropdownOption*>(obj, ::TYPES[0/*DropdownOption*/]))->SetBorderColor(v, origin);
}

// public override sealed bool get_SupportsOriginSetter() :599
void OSP_DropdownOption_BorderColor_Property__get_SupportsOriginSetter_fn(OSP_DropdownOption_BorderColor_Property* __this, bool* __retval)
{
    return *__retval = true, void();
}

// public OSP_DropdownOption_BorderColor_Property(DropdownOption obj, Uno.UX.Selector name) [instance] :595
void OSP_DropdownOption_BorderColor_Property::ctor_3(::g::DropdownOption* obj, ::g::Uno::UX::Selector name)
{
    ctor_2(name);
    _obj = obj;
}

// public OSP_DropdownOption_BorderColor_Property New(DropdownOption obj, Uno.UX.Selector name) [static] :595
OSP_DropdownOption_BorderColor_Property* OSP_DropdownOption_BorderColor_Property::New1(::g::DropdownOption* obj, ::g::Uno::UX::Selector name)
{
    OSP_DropdownOption_BorderColor_Property* obj1 = (OSP_DropdownOption_BorderColor_Property*)uNew(OSP_DropdownOption_BorderColor_Property_typeof());
    obj1->ctor_3(obj, name);
    return obj1;
}
// }

} // ::g
