// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.OSP_FuseControl-b0f0449b.h>
#include <Fuse.Controls.TextInput.h>
#include <Uno.UX.IPropertyListener.h>
#include <Uno.UX.PropertyObject.h>
#include <Uno.UX.Selector.h>
static uType* TYPES[1];

namespace g{

// internal sealed class OSP_FuseControlsTextInput_PlaceholderColor_Property :634
// {
static void OSP_FuseControlsTextInput_PlaceholderColor_Property_build(uType* type)
{
    ::TYPES[0] = ::g::Fuse::Controls::TextInput_typeof();
    type->SetBase(::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL));
    type->SetFields(1,
        ::TYPES[0/*Fuse.Controls.TextInput*/], offsetof(OSP_FuseControlsTextInput_PlaceholderColor_Property, _obj), uFieldFlagsWeak);
}

::g::Uno::UX::Property1_type* OSP_FuseControlsTextInput_PlaceholderColor_Property_typeof()
{
    static uSStrong< ::g::Uno::UX::Property1_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::Property1_typeof();
    options.FieldCount = 2;
    options.ObjectSize = sizeof(OSP_FuseControlsTextInput_PlaceholderColor_Property);
    options.TypeSize = sizeof(::g::Uno::UX::Property1_type);
    type = (::g::Uno::UX::Property1_type*)uClassType::New("OSP_FuseControlsTextInput_PlaceholderColor_Property", options);
    type->fp_build_ = OSP_FuseControlsTextInput_PlaceholderColor_Property_build;
    type->fp_Get1 = (void(*)(::g::Uno::UX::Property1*, ::g::Uno::UX::PropertyObject*, uTRef))OSP_FuseControlsTextInput_PlaceholderColor_Property__Get1_fn;
    type->fp_get_Object = (void(*)(::g::Uno::UX::Property*, ::g::Uno::UX::PropertyObject**))OSP_FuseControlsTextInput_PlaceholderColor_Property__get_Object_fn;
    type->fp_Set1 = (void(*)(::g::Uno::UX::Property1*, ::g::Uno::UX::PropertyObject*, void*, uObject*))OSP_FuseControlsTextInput_PlaceholderColor_Property__Set1_fn;
    return type;
}

// public OSP_FuseControlsTextInput_PlaceholderColor_Property(Fuse.Controls.TextInput obj, Uno.UX.Selector name) :637
void OSP_FuseControlsTextInput_PlaceholderColor_Property__ctor_3_fn(OSP_FuseControlsTextInput_PlaceholderColor_Property* __this, ::g::Fuse::Controls::TextInput* obj, ::g::Uno::UX::Selector* name)
{
    __this->ctor_3(obj, *name);
}

// public override sealed float4 Get(Uno.UX.PropertyObject obj) :639
void OSP_FuseControlsTextInput_PlaceholderColor_Property__Get1_fn(OSP_FuseControlsTextInput_PlaceholderColor_Property* __this, ::g::Uno::UX::PropertyObject* obj, ::g::Uno::Float4* __retval)
{
    return *__retval = uPtr(uCast< ::g::Fuse::Controls::TextInput*>(obj, ::TYPES[0/*Fuse.Controls.TextInput*/]))->PlaceholderColor(), void();
}

// public OSP_FuseControlsTextInput_PlaceholderColor_Property New(Fuse.Controls.TextInput obj, Uno.UX.Selector name) :637
void OSP_FuseControlsTextInput_PlaceholderColor_Property__New1_fn(::g::Fuse::Controls::TextInput* obj, ::g::Uno::UX::Selector* name, OSP_FuseControlsTextInput_PlaceholderColor_Property** __retval)
{
    *__retval = OSP_FuseControlsTextInput_PlaceholderColor_Property::New1(obj, *name);
}

// public override sealed Uno.UX.PropertyObject get_Object() :638
void OSP_FuseControlsTextInput_PlaceholderColor_Property__get_Object_fn(OSP_FuseControlsTextInput_PlaceholderColor_Property* __this, ::g::Uno::UX::PropertyObject** __retval)
{
    return *__retval = __this->_obj, void();
}

// public override sealed void Set(Uno.UX.PropertyObject obj, float4 v, Uno.UX.IPropertyListener origin) :640
void OSP_FuseControlsTextInput_PlaceholderColor_Property__Set1_fn(OSP_FuseControlsTextInput_PlaceholderColor_Property* __this, ::g::Uno::UX::PropertyObject* obj, ::g::Uno::Float4* v, uObject* origin)
{
    ::g::Uno::Float4 v_ = *v;
    uPtr(uCast< ::g::Fuse::Controls::TextInput*>(obj, ::TYPES[0/*Fuse.Controls.TextInput*/]))->PlaceholderColor(v_);
}

// public OSP_FuseControlsTextInput_PlaceholderColor_Property(Fuse.Controls.TextInput obj, Uno.UX.Selector name) [instance] :637
void OSP_FuseControlsTextInput_PlaceholderColor_Property::ctor_3(::g::Fuse::Controls::TextInput* obj, ::g::Uno::UX::Selector name)
{
    ctor_2(name);
    _obj = obj;
}

// public OSP_FuseControlsTextInput_PlaceholderColor_Property New(Fuse.Controls.TextInput obj, Uno.UX.Selector name) [static] :637
OSP_FuseControlsTextInput_PlaceholderColor_Property* OSP_FuseControlsTextInput_PlaceholderColor_Property::New1(::g::Fuse::Controls::TextInput* obj, ::g::Uno::UX::Selector name)
{
    OSP_FuseControlsTextInput_PlaceholderColor_Property* obj1 = (OSP_FuseControlsTextInput_PlaceholderColor_Property*)uNew(OSP_FuseControlsTextInput_PlaceholderColor_Property_typeof());
    obj1->ctor_3(obj, name);
    return obj1;
}
// }

} // ::g
