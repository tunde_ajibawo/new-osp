// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/Profile.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.MainView.h>
#include <_root.Profile.h>
#include <_root.Profile.Template7.h>
#include <Fuse.Controls.Panel.h>
#include <Fuse.Controls.Text.h>
#include <Fuse.Controls.TextControl.h>
#include <Fuse.Elements.Alignment.h>
#include <Fuse.Elements.Element.h>
#include <Fuse.Font.h>
#include <Fuse.Node.h>
#include <Fuse.Visual.h>
#include <Uno.Bool.h>
#include <Uno.Collections.ICollection-1.h>
#include <Uno.Collections.IList-1.h>
#include <Uno.Float.h>
#include <Uno.Float4.h>
#include <Uno.Int.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.UX.Size.h>
#include <Uno.UX.Unit.h>
static uString* STRINGS[2];
static uType* TYPES[1];

namespace g{

// public partial sealed class Profile.Template7 :222
// {
// static Template7() :231
static void Profile__Template7__cctor__fn(uType* __type)
{
}

static void Profile__Template7_build(uType* type)
{
    ::STRINGS[0] = uString::Const("profile.ux");
    ::STRINGS[1] = uString::Const("COMPANY INFORMATION");
    ::TYPES[0] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL);
    type->SetDependencies(
        ::g::MainView_typeof());
    type->SetFields(2,
        ::g::Profile_typeof(), offsetof(Profile__Template7, __parent1), uFieldFlagsWeak,
        ::g::Profile_typeof(), offsetof(Profile__Template7, __parentInstance1), uFieldFlagsWeak);
}

::g::Uno::UX::Template_type* Profile__Template7_typeof()
{
    static uSStrong< ::g::Uno::UX::Template_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::Template_typeof();
    options.FieldCount = 4;
    options.DependencyCount = 1;
    options.ObjectSize = sizeof(Profile__Template7);
    options.TypeSize = sizeof(::g::Uno::UX::Template_type);
    type = (::g::Uno::UX::Template_type*)uClassType::New("Profile.Template7", options);
    type->fp_build_ = Profile__Template7_build;
    type->fp_cctor_ = Profile__Template7__cctor__fn;
    type->fp_New1 = (void(*)(::g::Uno::UX::Template*, uObject**))Profile__Template7__New1_fn;
    return type;
}

// public Template7(Profile parent, Profile parentInstance) :226
void Profile__Template7__ctor_1_fn(Profile__Template7* __this, ::g::Profile* parent, ::g::Profile* parentInstance)
{
    __this->ctor_1(parent, parentInstance);
}

// public override sealed object New() :234
void Profile__Template7__New1_fn(Profile__Template7* __this, uObject** __retval)
{
    ::g::Fuse::Controls::Panel* __self1 = ::g::Fuse::Controls::Panel::New3();
    ::g::Fuse::Controls::Text* temp = ::g::Fuse::Controls::Text::New3();
    __self1->Width(::g::Uno::UX::Size__New1(80.0f, 4));
    __self1->SourceLineNumber(171);
    __self1->SourceFileName(::STRINGS[0/*"profile.ux"*/]);
    temp->Value(::STRINGS[1/*"COMPANY INF...*/]);
    temp->FontSize(12.0f);
    temp->TextColor(::g::Uno::Float4__New2(0.0f, 0.1333333f, 0.2313726f, 1.0f));
    temp->Alignment(9);
    temp->Margin(::g::Uno::Float4__New2(0.0f, 20.0f, 0.0f, 10.0f));
    temp->SourceLineNumber(172);
    temp->SourceFileName(::STRINGS[0/*"profile.ux"*/]);
    temp->Font(::g::MainView::Raleway());
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(__self1->Children()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Node>*/]), temp);
    return *__retval = __self1, void();
}

// public Template7 New(Profile parent, Profile parentInstance) :226
void Profile__Template7__New2_fn(::g::Profile* parent, ::g::Profile* parentInstance, Profile__Template7** __retval)
{
    *__retval = Profile__Template7::New2(parent, parentInstance);
}

// public Template7(Profile parent, Profile parentInstance) [instance] :226
void Profile__Template7::ctor_1(::g::Profile* parent, ::g::Profile* parentInstance)
{
    ctor_(NULL, false);
    __parent1 = parent;
    __parentInstance1 = parentInstance;
}

// public Template7 New(Profile parent, Profile parentInstance) [static] :226
Profile__Template7* Profile__Template7::New2(::g::Profile* parent, ::g::Profile* parentInstance)
{
    Profile__Template7* obj1 = (Profile__Template7*)uNew(Profile__Template7_typeof());
    obj1->ctor_1(parent, parentInstance);
    return obj1;
}
// }

} // ::g
