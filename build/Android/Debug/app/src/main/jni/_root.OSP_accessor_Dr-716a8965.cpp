// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.DropdownMenu.h>
#include <_root.OSP_accessor_Dr-716a8965.h>
#include <Uno.Bool.h>
#include <Uno.Float4.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.Type.h>
#include <Uno.UX.IPropertyListener.h>
#include <Uno.UX.PropertyObject.h>
#include <Uno.UX.Selector.h>
static uString* STRINGS[1];
static uType* TYPES[2];

namespace g{

// internal sealed class OSP_accessor_DropdownMenu_TextColor :121
// {
// static generated OSP_accessor_DropdownMenu_TextColor() :121
static void OSP_accessor_DropdownMenu_TextColor__cctor__fn(uType* __type)
{
    OSP_accessor_DropdownMenu_TextColor::Singleton_ = OSP_accessor_DropdownMenu_TextColor::New1();
    OSP_accessor_DropdownMenu_TextColor::_name_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[0/*"TextColor"*/]);
}

static void OSP_accessor_DropdownMenu_TextColor_build(uType* type)
{
    ::STRINGS[0] = uString::Const("TextColor");
    ::TYPES[0] = ::g::DropdownMenu_typeof();
    ::TYPES[1] = ::g::Uno::Type_typeof();
    type->SetFields(0,
        ::g::Uno::UX::PropertyAccessor_typeof(), (uintptr_t)&OSP_accessor_DropdownMenu_TextColor::Singleton_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&OSP_accessor_DropdownMenu_TextColor::_name_, uFieldFlagsStatic);
}

::g::Uno::UX::PropertyAccessor_type* OSP_accessor_DropdownMenu_TextColor_typeof()
{
    static uSStrong< ::g::Uno::UX::PropertyAccessor_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::PropertyAccessor_typeof();
    options.FieldCount = 2;
    options.ObjectSize = sizeof(OSP_accessor_DropdownMenu_TextColor);
    options.TypeSize = sizeof(::g::Uno::UX::PropertyAccessor_type);
    type = (::g::Uno::UX::PropertyAccessor_type*)uClassType::New("OSP_accessor_DropdownMenu_TextColor", options);
    type->fp_build_ = OSP_accessor_DropdownMenu_TextColor_build;
    type->fp_ctor_ = (void*)OSP_accessor_DropdownMenu_TextColor__New1_fn;
    type->fp_cctor_ = OSP_accessor_DropdownMenu_TextColor__cctor__fn;
    type->fp_GetAsObject = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::PropertyObject*, uObject**))OSP_accessor_DropdownMenu_TextColor__GetAsObject_fn;
    type->fp_get_Name = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::Selector*))OSP_accessor_DropdownMenu_TextColor__get_Name_fn;
    type->fp_get_PropertyType = (void(*)(::g::Uno::UX::PropertyAccessor*, uType**))OSP_accessor_DropdownMenu_TextColor__get_PropertyType_fn;
    type->fp_SetAsObject = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::PropertyObject*, uObject*, uObject*))OSP_accessor_DropdownMenu_TextColor__SetAsObject_fn;
    type->fp_get_SupportsOriginSetter = (void(*)(::g::Uno::UX::PropertyAccessor*, bool*))OSP_accessor_DropdownMenu_TextColor__get_SupportsOriginSetter_fn;
    return type;
}

// public generated OSP_accessor_DropdownMenu_TextColor() :121
void OSP_accessor_DropdownMenu_TextColor__ctor_1_fn(OSP_accessor_DropdownMenu_TextColor* __this)
{
    __this->ctor_1();
}

// public override sealed object GetAsObject(Uno.UX.PropertyObject obj) :127
void OSP_accessor_DropdownMenu_TextColor__GetAsObject_fn(OSP_accessor_DropdownMenu_TextColor* __this, ::g::Uno::UX::PropertyObject* obj, uObject** __retval)
{
    return *__retval = uBox(::g::Uno::Float4_typeof(), uPtr(uCast< ::g::DropdownMenu*>(obj, ::TYPES[0/*DropdownMenu*/]))->TextColor()), void();
}

// public override sealed Uno.UX.Selector get_Name() :124
void OSP_accessor_DropdownMenu_TextColor__get_Name_fn(OSP_accessor_DropdownMenu_TextColor* __this, ::g::Uno::UX::Selector* __retval)
{
    return *__retval = OSP_accessor_DropdownMenu_TextColor::_name_, void();
}

// public generated OSP_accessor_DropdownMenu_TextColor New() :121
void OSP_accessor_DropdownMenu_TextColor__New1_fn(OSP_accessor_DropdownMenu_TextColor** __retval)
{
    *__retval = OSP_accessor_DropdownMenu_TextColor::New1();
}

// public override sealed Uno.Type get_PropertyType() :126
void OSP_accessor_DropdownMenu_TextColor__get_PropertyType_fn(OSP_accessor_DropdownMenu_TextColor* __this, uType** __retval)
{
    return *__retval = ::g::Uno::Float4_typeof(), void();
}

// public override sealed void SetAsObject(Uno.UX.PropertyObject obj, object v, Uno.UX.IPropertyListener origin) :128
void OSP_accessor_DropdownMenu_TextColor__SetAsObject_fn(OSP_accessor_DropdownMenu_TextColor* __this, ::g::Uno::UX::PropertyObject* obj, uObject* v, uObject* origin)
{
    uPtr(uCast< ::g::DropdownMenu*>(obj, ::TYPES[0/*DropdownMenu*/]))->SetTextColor(uUnbox< ::g::Uno::Float4>(::g::Uno::Float4_typeof(), v), origin);
}

// public override sealed bool get_SupportsOriginSetter() :129
void OSP_accessor_DropdownMenu_TextColor__get_SupportsOriginSetter_fn(OSP_accessor_DropdownMenu_TextColor* __this, bool* __retval)
{
    return *__retval = true, void();
}

uSStrong< ::g::Uno::UX::PropertyAccessor*> OSP_accessor_DropdownMenu_TextColor::Singleton_;
::g::Uno::UX::Selector OSP_accessor_DropdownMenu_TextColor::_name_;

// public generated OSP_accessor_DropdownMenu_TextColor() [instance] :121
void OSP_accessor_DropdownMenu_TextColor::ctor_1()
{
    ctor_();
}

// public generated OSP_accessor_DropdownMenu_TextColor New() [static] :121
OSP_accessor_DropdownMenu_TextColor* OSP_accessor_DropdownMenu_TextColor::New1()
{
    OSP_accessor_DropdownMenu_TextColor* obj1 = (OSP_accessor_DropdownMenu_TextColor*)uNew(OSP_accessor_DropdownMenu_TextColor_typeof());
    obj1->ctor_1();
    return obj1;
}
// }

} // ::g
