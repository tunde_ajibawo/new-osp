// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/WorkInfo.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.MainView.h>
#include <_root.OSP_bundle.h>
#include <_root.OSP_FuseControl-83fd107.h>
#include <_root.OSP_FuseControl-b26a5bbf.h>
#include <_root.OSP_FuseControl-bf62c319.h>
#include <_root.OSP_FuseElement-78dfea6e.h>
#include <_root.OSP_FuseReactiv-f855d0e4.h>
#include <_root.OSP_FuseTrigger-1375289b.h>
#include <_root.OSP_Tab_TColor_Property.h>
#include <_root.SubBut.h>
#include <_root.Tab.h>
#include <_root.TopBar.h>
#include <_root.WorkInfo.h>
#include <_root.WorkInfo.Template.h>
#include <_root.WorkInfo.Template1.h>
#include <_root.WorkInfo.Template10.h>
#include <_root.WorkInfo.Template11.h>
#include <_root.WorkInfo.Template12.h>
#include <_root.WorkInfo.Template13.h>
#include <_root.WorkInfo.Template14.h>
#include <_root.WorkInfo.Template15.h>
#include <_root.WorkInfo.Template2.h>
#include <_root.WorkInfo.Template3.h>
#include <_root.WorkInfo.Template4.h>
#include <_root.WorkInfo.Template5.h>
#include <_root.WorkInfo.Template6.h>
#include <_root.WorkInfo.Template7.h>
#include <_root.WorkInfo.Template8.h>
#include <_root.WorkInfo.Template9.h>
#include <Fuse.Animations.Animator.h>
#include <Fuse.Animations.Change-1.h>
#include <Fuse.Animations.Spin.h>
#include <Fuse.Animations.TrackAnimator.h>
#include <Fuse.Controls.Control.h>
#include <Fuse.Controls.DockPanel.h>
#include <Fuse.Controls.Grid.h>
#include <Fuse.Controls.Image.h>
#include <Fuse.Controls.Navigat-70e90308.h>
#include <Fuse.Controls.PageControl.h>
#include <Fuse.Controls.Panel.h>
#include <Fuse.Controls.ScrollView.h>
#include <Fuse.Controls.StackPanel.h>
#include <Fuse.Controls.Text.h>
#include <Fuse.Controls.TextAlignment.h>
#include <Fuse.Controls.TextControl.h>
#include <Fuse.Drawing.Brush.h>
#include <Fuse.Drawing.StaticSolidColor.h>
#include <Fuse.Elements.Alignment.h>
#include <Fuse.Elements.Element.h>
#include <Fuse.Font.h>
#include <Fuse.Gestures.Clicked.h>
#include <Fuse.Gestures.ClickedHandler.h>
#include <Fuse.Layouts.Dock.h>
#include <Fuse.Navigation.Activated.h>
#include <Fuse.Navigation.Router.h>
#include <Fuse.Navigation.While-89f5a828.h>
#include <Fuse.Navigation.WhileActive.h>
#include <Fuse.Navigation.WhileInactive.h>
#include <Fuse.Reactive.BindingMode.h>
#include <Fuse.Reactive.Data.h>
#include <Fuse.Reactive.DataBinding.h>
#include <Fuse.Reactive.Each.h>
#include <Fuse.Reactive.EventBinding.h>
#include <Fuse.Reactive.Expression.h>
#include <Fuse.Reactive.IExpression.h>
#include <Fuse.Reactive.Instantiator.h>
#include <Fuse.Reactive.JavaScript.h>
#include <Fuse.Triggers.Actions.Callback.h>
#include <Fuse.Triggers.Actions.Set-1.h>
#include <Fuse.Triggers.Actions-fcab7e57.h>
#include <Fuse.Triggers.Busy.h>
#include <Fuse.Triggers.PulseTr-e6f97a32.h>
#include <Fuse.Triggers.PulseTrigger-1.h>
#include <Fuse.Triggers.Trigger.h>
#include <Fuse.Triggers.WhileBool.h>
#include <Fuse.Triggers.WhileBusy.h>
#include <Fuse.Triggers.WhileFalse.h>
#include <Fuse.Triggers.WhileTrue.h>
#include <Fuse.VisualEventHandler.h>
#include <Uno.Bool.h>
#include <Uno.Double.h>
#include <Uno.EventArgs.h>
#include <Uno.Float.h>
#include <Uno.Int.h>
#include <Uno.IO.BundleFile.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.UX.BundleFileSource.h>
#include <Uno.UX.FileSource.h>
#include <Uno.UX.NameTable.h>
#include <Uno.UX.Property.h>
#include <Uno.UX.Property1-1.h>
#include <Uno.UX.Selector.h>
#include <Uno.UX.Size.h>
#include <Uno.UX.Template.h>
#include <Uno.UX.Unit.h>

namespace g{

// public partial sealed class WorkInfo :2
// {
// static WorkInfo() :604
static void WorkInfo__cctor_4_fn(uType* __type)
{
    WorkInfo::__g_static_nametable1_ = uArray::Init<uString*>(::g::Uno::String_typeof()->Array(), 36, uString::Const("router"), uString::Const("page1Tab"), uString::Const("permit"), uString::Const("page2Tab"), uString::Const("train"), uString::Const("temp_eb26"), uString::Const("page3Tab"), uString::Const("medical"), uString::Const("navigation"), uString::Const("page1"), uString::Const("loadingPanel"), uString::Const("load"), uString::Const("errorMsg"), uString::Const("temp_eb27"), uString::Const("temp_eb28"), uString::Const("temp_eb29"), uString::Const("busy"), uString::Const("WorkPage"), uString::Const("page2"), uString::Const("loadingPanel1"), uString::Const("load1"), uString::Const("errorMsg1"), uString::Const("temp_eb30"), uString::Const("temp_eb31"), uString::Const("temp_eb32"), uString::Const("busy1"), uString::Const("WorkPage1"), uString::Const("page3"), uString::Const("loadingPanel2"), uString::Const("load2"), uString::Const("errorMsg2"), uString::Const("temp_eb33"), uString::Const("temp_eb34"), uString::Const("temp_eb35"), uString::Const("busy2"), uString::Const("WorkPage2"));
    WorkInfo::__selector0_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("Active"));
    WorkInfo::__selector1_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("Color"));
    WorkInfo::__selector2_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("TColor"));
    WorkInfo::__selector3_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("Opacity"));
    WorkInfo::__selector4_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("Value"));
    WorkInfo::__selector5_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("Items"));
    WorkInfo::__selector6_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("page1Tab"));
    WorkInfo::__selector7_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("permit"));
    WorkInfo::__selector8_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("page2Tab"));
    WorkInfo::__selector9_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("train"));
    WorkInfo::__selector10_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("page3Tab"));
    WorkInfo::__selector11_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("medical"));
    WorkInfo::__selector12_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("navigation"));
    WorkInfo::__selector13_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("page1"));
    WorkInfo::__selector14_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("loadingPanel"));
    WorkInfo::__selector15_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("load"));
    WorkInfo::__selector16_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("errorMsg"));
    WorkInfo::__selector17_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("busy"));
    WorkInfo::__selector18_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("WorkPage"));
    WorkInfo::__selector19_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("page2"));
    WorkInfo::__selector20_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("loadingPanel1"));
    WorkInfo::__selector21_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("load1"));
    WorkInfo::__selector22_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("errorMsg1"));
    WorkInfo::__selector23_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("busy1"));
    WorkInfo::__selector24_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("WorkPage1"));
    WorkInfo::__selector25_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("page3"));
    WorkInfo::__selector26_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("loadingPanel2"));
    WorkInfo::__selector27_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("load2"));
    WorkInfo::__selector28_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("errorMsg2"));
    WorkInfo::__selector29_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("busy2"));
    WorkInfo::__selector30_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("WorkPage2"));
}

static void WorkInfo_build(uType* type)
{
    type->SetDependencies(
        ::g::MainView_typeof(),
        ::g::OSP_bundle_typeof());
    type->SetInterfaces(
        ::g::Uno::Collections::IList_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface0),
        ::g::Fuse::Scripting::IScriptObject_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface1),
        ::g::Fuse::IProperties_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface2),
        ::g::Fuse::INotifyUnrooted_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface3),
        ::g::Fuse::ISourceLocation_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface4),
        ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface5),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface6),
        ::g::Uno::Collections::IList_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface7),
        ::g::Uno::UX::IPropertyListener_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface8),
        ::g::Fuse::ITemplateSource_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface9),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Visual_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface10),
        ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface11),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface12),
        ::g::Fuse::Triggers::Actions::IShow_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface13),
        ::g::Fuse::Triggers::Actions::IHide_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface14),
        ::g::Fuse::Triggers::Actions::ICollapse_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface15),
        ::g::Fuse::IActualPlacement_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface16),
        ::g::Fuse::Animations::IResize_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface17),
        ::g::Fuse::Drawing::ISurfaceDrawable_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface18));
    type->SetFields(121,
        ::g::Fuse::Navigation::Router_typeof(), offsetof(WorkInfo, router), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Fuse::Visual_typeof(), NULL), offsetof(WorkInfo, navigation_Active_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL), offsetof(WorkInfo, permit_Color_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL), offsetof(WorkInfo, permit_TColor_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(WorkInfo, loadingPanel_Opacity_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(WorkInfo, WorkPage_Opacity_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(WorkInfo, temp_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(WorkInfo, load_Opacity_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(WorkInfo, errorMsg_Opacity_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Bool_typeof(), NULL), offsetof(WorkInfo, temp1_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Bool_typeof(), NULL), offsetof(WorkInfo, temp2_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(uObject_typeof(), NULL), offsetof(WorkInfo, temp3_Items_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL), offsetof(WorkInfo, train_Color_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL), offsetof(WorkInfo, train_TColor_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(WorkInfo, loadingPanel1_Opacity_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(WorkInfo, WorkPage1_Opacity_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(WorkInfo, temp4_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(WorkInfo, load1_Opacity_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(WorkInfo, errorMsg1_Opacity_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Bool_typeof(), NULL), offsetof(WorkInfo, temp5_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Bool_typeof(), NULL), offsetof(WorkInfo, temp6_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(uObject_typeof(), NULL), offsetof(WorkInfo, temp7_Items_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL), offsetof(WorkInfo, medical_Color_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL), offsetof(WorkInfo, medical_TColor_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(WorkInfo, loadingPanel2_Opacity_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(WorkInfo, WorkPage2_Opacity_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(WorkInfo, temp8_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(WorkInfo, load2_Opacity_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(WorkInfo, errorMsg2_Opacity_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Bool_typeof(), NULL), offsetof(WorkInfo, temp9_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Bool_typeof(), NULL), offsetof(WorkInfo, temp10_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(uObject_typeof(), NULL), offsetof(WorkInfo, temp11_Items_inst), 0,
        ::g::Fuse::Controls::Panel_typeof(), offsetof(WorkInfo, page1Tab), 0,
        ::g::Tab_typeof(), offsetof(WorkInfo, permit), 0,
        ::g::Fuse::Controls::Panel_typeof(), offsetof(WorkInfo, page2Tab), 0,
        ::g::Tab_typeof(), offsetof(WorkInfo, train), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(WorkInfo, temp_eb26), 0,
        ::g::Fuse::Controls::Panel_typeof(), offsetof(WorkInfo, page3Tab), 0,
        ::g::Tab_typeof(), offsetof(WorkInfo, medical), 0,
        ::g::Fuse::Controls::PageControl_typeof(), offsetof(WorkInfo, navigation), 0,
        ::g::Fuse::Controls::Page_typeof(), offsetof(WorkInfo, page1), 0,
        ::g::Fuse::Controls::Panel_typeof(), offsetof(WorkInfo, loadingPanel), 0,
        ::g::Fuse::Controls::Image_typeof(), offsetof(WorkInfo, load), 0,
        ::g::Fuse::Controls::StackPanel_typeof(), offsetof(WorkInfo, errorMsg), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(WorkInfo, temp_eb27), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(WorkInfo, temp_eb28), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(WorkInfo, temp_eb29), 0,
        ::g::Fuse::Triggers::Busy_typeof(), offsetof(WorkInfo, busy), 0,
        ::g::Fuse::Controls::ScrollView_typeof(), offsetof(WorkInfo, WorkPage), 0,
        ::g::Fuse::Controls::Page_typeof(), offsetof(WorkInfo, page2), 0,
        ::g::Fuse::Controls::Panel_typeof(), offsetof(WorkInfo, loadingPanel1), 0,
        ::g::Fuse::Controls::Image_typeof(), offsetof(WorkInfo, load1), 0,
        ::g::Fuse::Controls::StackPanel_typeof(), offsetof(WorkInfo, errorMsg1), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(WorkInfo, temp_eb30), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(WorkInfo, temp_eb31), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(WorkInfo, temp_eb32), 0,
        ::g::Fuse::Triggers::Busy_typeof(), offsetof(WorkInfo, busy1), 0,
        ::g::Fuse::Controls::ScrollView_typeof(), offsetof(WorkInfo, WorkPage1), 0,
        ::g::Fuse::Controls::Page_typeof(), offsetof(WorkInfo, page3), 0,
        ::g::Fuse::Controls::Panel_typeof(), offsetof(WorkInfo, loadingPanel2), 0,
        ::g::Fuse::Controls::Image_typeof(), offsetof(WorkInfo, load2), 0,
        ::g::Fuse::Controls::StackPanel_typeof(), offsetof(WorkInfo, errorMsg2), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(WorkInfo, temp_eb33), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(WorkInfo, temp_eb34), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(WorkInfo, temp_eb35), 0,
        ::g::Fuse::Triggers::Busy_typeof(), offsetof(WorkInfo, busy2), 0,
        ::g::Fuse::Controls::ScrollView_typeof(), offsetof(WorkInfo, WorkPage2), 0,
        ::g::Uno::UX::NameTable_typeof(), offsetof(WorkInfo, __g_nametable1), 0,
        ::g::Uno::String_typeof()->Array(), (uintptr_t)&WorkInfo::__g_static_nametable1_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector0_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector1_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector2_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector3_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector4_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector5_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector6_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector7_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector8_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector9_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector10_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector11_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector12_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector13_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector14_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector15_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector16_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector17_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector18_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector19_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector20_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector21_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector22_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector23_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector24_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector25_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector26_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector27_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector28_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector29_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo::__selector30_, uFieldFlagsStatic);
}

::g::Fuse::Controls::Panel_type* WorkInfo_typeof()
{
    static uSStrong< ::g::Fuse::Controls::Panel_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Fuse::Controls::Page_typeof();
    options.FieldCount = 221;
    options.InterfaceCount = 19;
    options.DependencyCount = 2;
    options.ObjectSize = sizeof(WorkInfo);
    options.TypeSize = sizeof(::g::Fuse::Controls::Panel_type);
    type = (::g::Fuse::Controls::Panel_type*)uClassType::New("WorkInfo", options);
    type->fp_build_ = WorkInfo_build;
    type->fp_cctor_ = WorkInfo__cctor_4_fn;
    type->interface18.fp_Draw = (void(*)(uObject*, ::g::Fuse::Drawing::Surface*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableDraw_fn;
    type->interface18.fp_get_IsPrimary = (void(*)(uObject*, bool*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableget_IsPrimary_fn;
    type->interface18.fp_get_ElementSize = (void(*)(uObject*, ::g::Uno::Float2*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableget_ElementSize_fn;
    type->interface13.fp_Show = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsIShowShow_fn;
    type->interface15.fp_Collapse = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsICollapseCollapse_fn;
    type->interface14.fp_Hide = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsIHideHide_fn;
    type->interface17.fp_SetSize = (void(*)(uObject*, ::g::Uno::Float2*))::g::Fuse::Elements::Element__FuseAnimationsIResizeSetSize_fn;
    type->interface16.fp_get_ActualSize = (void(*)(uObject*, ::g::Uno::Float3*))::g::Fuse::Elements::Element__FuseIActualPlacementget_ActualSize_fn;
    type->interface16.fp_add_Placed = (void(*)(uObject*, uDelegate*))::g::Fuse::Elements::Element__add_Placed_fn;
    type->interface16.fp_remove_Placed = (void(*)(uObject*, uDelegate*))::g::Fuse::Elements::Element__remove_Placed_fn;
    type->interface10.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Visual__UnoCollectionsIEnumerableFuseVisualGetEnumerator_fn;
    type->interface11.fp_Clear = (void(*)(uObject*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeClear_fn;
    type->interface11.fp_Contains = (void(*)(uObject*, void*, bool*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeContains_fn;
    type->interface7.fp_RemoveAt = (void(*)(uObject*, int32_t*))::g::Fuse::Visual__UnoCollectionsIListFuseNodeRemoveAt_fn;
    type->interface12.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Visual__UnoCollectionsIEnumerableFuseNodeGetEnumerator_fn;
    type->interface11.fp_get_Count = (void(*)(uObject*, int32_t*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeget_Count_fn;
    type->interface7.fp_get_Item = (void(*)(uObject*, int32_t*, uTRef))::g::Fuse::Visual__UnoCollectionsIListFuseNodeget_Item_fn;
    type->interface7.fp_Insert = (void(*)(uObject*, int32_t*, void*))::g::Fuse::Visual__Insert1_fn;
    type->interface8.fp_OnPropertyChanged = (void(*)(uObject*, ::g::Uno::UX::PropertyObject*, ::g::Uno::UX::Selector*))::g::Fuse::Controls::Control__OnPropertyChanged2_fn;
    type->interface9.fp_FindTemplate = (void(*)(uObject*, uString*, ::g::Uno::UX::Template**))::g::Fuse::Visual__FindTemplate_fn;
    type->interface11.fp_Add = (void(*)(uObject*, void*))::g::Fuse::Visual__Add1_fn;
    type->interface11.fp_Remove = (void(*)(uObject*, void*, bool*))::g::Fuse::Visual__Remove1_fn;
    type->interface5.fp_Clear = (void(*)(uObject*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingClear_fn;
    type->interface5.fp_Contains = (void(*)(uObject*, void*, bool*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingContains_fn;
    type->interface0.fp_RemoveAt = (void(*)(uObject*, int32_t*))::g::Fuse::Node__UnoCollectionsIListFuseBindingRemoveAt_fn;
    type->interface6.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Node__UnoCollectionsIEnumerableFuseBindingGetEnumerator_fn;
    type->interface1.fp_SetScriptObject = (void(*)(uObject*, uObject*, ::g::Fuse::Scripting::Context*))::g::Fuse::Node__FuseScriptingIScriptObjectSetScriptObject_fn;
    type->interface5.fp_get_Count = (void(*)(uObject*, int32_t*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingget_Count_fn;
    type->interface0.fp_get_Item = (void(*)(uObject*, int32_t*, uTRef))::g::Fuse::Node__UnoCollectionsIListFuseBindingget_Item_fn;
    type->interface1.fp_get_ScriptObject = (void(*)(uObject*, uObject**))::g::Fuse::Node__FuseScriptingIScriptObjectget_ScriptObject_fn;
    type->interface1.fp_get_ScriptContext = (void(*)(uObject*, ::g::Fuse::Scripting::Context**))::g::Fuse::Node__FuseScriptingIScriptObjectget_ScriptContext_fn;
    type->interface4.fp_get_SourceNearest = (void(*)(uObject*, uObject**))::g::Fuse::Node__FuseISourceLocationget_SourceNearest_fn;
    type->interface3.fp_add_Unrooted = (void(*)(uObject*, uDelegate*))::g::Fuse::Node__FuseINotifyUnrootedadd_Unrooted_fn;
    type->interface3.fp_remove_Unrooted = (void(*)(uObject*, uDelegate*))::g::Fuse::Node__FuseINotifyUnrootedremove_Unrooted_fn;
    type->interface0.fp_Insert = (void(*)(uObject*, int32_t*, void*))::g::Fuse::Node__Insert_fn;
    type->interface2.fp_get_Properties = (void(*)(uObject*, ::g::Fuse::Properties**))::g::Fuse::Node__get_Properties_fn;
    type->interface4.fp_get_SourceLineNumber = (void(*)(uObject*, int32_t*))::g::Fuse::Node__get_SourceLineNumber_fn;
    type->interface4.fp_get_SourceFileName = (void(*)(uObject*, uString**))::g::Fuse::Node__get_SourceFileName_fn;
    type->interface5.fp_Add = (void(*)(uObject*, void*))::g::Fuse::Node__Add_fn;
    type->interface5.fp_Remove = (void(*)(uObject*, void*, bool*))::g::Fuse::Node__Remove_fn;
    return type;
}

// public WorkInfo(Fuse.Navigation.Router router) :608
void WorkInfo__ctor_8_fn(WorkInfo* __this, ::g::Fuse::Navigation::Router* router1)
{
    __this->ctor_8(router1);
}

// private void InitializeUX() :614
void WorkInfo__InitializeUX_fn(WorkInfo* __this)
{
    __this->InitializeUX();
}

// public WorkInfo New(Fuse.Navigation.Router router) :608
void WorkInfo__New5_fn(::g::Fuse::Navigation::Router* router1, WorkInfo** __retval)
{
    *__retval = WorkInfo::New5(router1);
}

uSStrong<uArray*> WorkInfo::__g_static_nametable1_;
::g::Uno::UX::Selector WorkInfo::__selector0_;
::g::Uno::UX::Selector WorkInfo::__selector1_;
::g::Uno::UX::Selector WorkInfo::__selector2_;
::g::Uno::UX::Selector WorkInfo::__selector3_;
::g::Uno::UX::Selector WorkInfo::__selector4_;
::g::Uno::UX::Selector WorkInfo::__selector5_;
::g::Uno::UX::Selector WorkInfo::__selector6_;
::g::Uno::UX::Selector WorkInfo::__selector7_;
::g::Uno::UX::Selector WorkInfo::__selector8_;
::g::Uno::UX::Selector WorkInfo::__selector9_;
::g::Uno::UX::Selector WorkInfo::__selector10_;
::g::Uno::UX::Selector WorkInfo::__selector11_;
::g::Uno::UX::Selector WorkInfo::__selector12_;
::g::Uno::UX::Selector WorkInfo::__selector13_;
::g::Uno::UX::Selector WorkInfo::__selector14_;
::g::Uno::UX::Selector WorkInfo::__selector15_;
::g::Uno::UX::Selector WorkInfo::__selector16_;
::g::Uno::UX::Selector WorkInfo::__selector17_;
::g::Uno::UX::Selector WorkInfo::__selector18_;
::g::Uno::UX::Selector WorkInfo::__selector19_;
::g::Uno::UX::Selector WorkInfo::__selector20_;
::g::Uno::UX::Selector WorkInfo::__selector21_;
::g::Uno::UX::Selector WorkInfo::__selector22_;
::g::Uno::UX::Selector WorkInfo::__selector23_;
::g::Uno::UX::Selector WorkInfo::__selector24_;
::g::Uno::UX::Selector WorkInfo::__selector25_;
::g::Uno::UX::Selector WorkInfo::__selector26_;
::g::Uno::UX::Selector WorkInfo::__selector27_;
::g::Uno::UX::Selector WorkInfo::__selector28_;
::g::Uno::UX::Selector WorkInfo::__selector29_;
::g::Uno::UX::Selector WorkInfo::__selector30_;

// public WorkInfo(Fuse.Navigation.Router router) [instance] :608
void WorkInfo::ctor_8(::g::Fuse::Navigation::Router* router1)
{
    ctor_7();
    router = router1;
    InitializeUX();
}

// private void InitializeUX() [instance] :614
void WorkInfo::InitializeUX()
{
    __g_nametable1 = ::g::Uno::UX::NameTable::New1(NULL, WorkInfo::__g_static_nametable1_);
    navigation = ::g::Fuse::Controls::PageControl::New4();
    navigation_Active_inst = ::g::OSP_FuseControlsNavigationControl_Active_Property::New1(navigation, WorkInfo::__selector0_);
    ::g::Fuse::Reactive::Data* temp12 = ::g::Fuse::Reactive::Data::New1(uString::Const("Loadtraining"));
    permit = ::g::Tab::New4();
    permit_Color_inst = ::g::OSP_FuseControlsPanel_Color_Property::New1(permit, WorkInfo::__selector1_);
    permit_TColor_inst = ::g::OSP_Tab_TColor_Property::New1(permit, WorkInfo::__selector2_);
    loadingPanel = ::g::Fuse::Controls::Panel::New3();
    loadingPanel_Opacity_inst = ::g::OSP_FuseElementsElement_Opacity_Property::New1(loadingPanel, WorkInfo::__selector3_);
    WorkPage = ::g::Fuse::Controls::ScrollView::New4();
    WorkPage_Opacity_inst = ::g::OSP_FuseElementsElement_Opacity_Property::New1(WorkPage, WorkInfo::__selector3_);
    ::g::Fuse::Controls::Text* temp = ::g::Fuse::Controls::Text::New3();
    temp_Value_inst = ::g::OSP_FuseControlsTextControl_Value_Property::New1(temp, WorkInfo::__selector4_);
    ::g::Fuse::Reactive::Data* temp13 = ::g::Fuse::Reactive::Data::New1(uString::Const("Msg"));
    ::g::Fuse::Reactive::Data* temp14 = ::g::Fuse::Reactive::Data::New1(uString::Const("startLoad"));
    ::g::Fuse::Reactive::Data* temp15 = ::g::Fuse::Reactive::Data::New1(uString::Const("dashboard"));
    load = ::g::Fuse::Controls::Image::New3();
    load_Opacity_inst = ::g::OSP_FuseElementsElement_Opacity_Property::New1(load, WorkInfo::__selector3_);
    errorMsg = ::g::Fuse::Controls::StackPanel::New4();
    errorMsg_Opacity_inst = ::g::OSP_FuseElementsElement_Opacity_Property::New1(errorMsg, WorkInfo::__selector3_);
    ::g::Fuse::Triggers::WhileTrue* temp1 = ::g::Fuse::Triggers::WhileTrue::New2();
    temp1_Value_inst = ::g::OSP_FuseTriggersWhileBool_Value_Property::New1(temp1, WorkInfo::__selector4_);
    ::g::Fuse::Reactive::Data* temp16 = ::g::Fuse::Reactive::Data::New1(uString::Const("error"));
    ::g::Fuse::Triggers::WhileFalse* temp2 = ::g::Fuse::Triggers::WhileFalse::New2();
    temp2_Value_inst = ::g::OSP_FuseTriggersWhileBool_Value_Property::New1(temp2, WorkInfo::__selector4_);
    ::g::Fuse::Reactive::Data* temp17 = ::g::Fuse::Reactive::Data::New1(uString::Const("error"));
    ::g::Fuse::Reactive::Data* temp18 = ::g::Fuse::Reactive::Data::New1(uString::Const("startLoad"));
    ::g::Fuse::Reactive::Each* temp3 = ::g::Fuse::Reactive::Each::New4();
    temp3_Items_inst = ::g::OSP_FuseReactiveEach_Items_Property::New1(temp3, WorkInfo::__selector5_);
    ::g::Fuse::Reactive::Data* temp19 = ::g::Fuse::Reactive::Data::New1(uString::Const("permit"));
    train = ::g::Tab::New4();
    train_Color_inst = ::g::OSP_FuseControlsPanel_Color_Property::New1(train, WorkInfo::__selector1_);
    train_TColor_inst = ::g::OSP_Tab_TColor_Property::New1(train, WorkInfo::__selector2_);
    loadingPanel1 = ::g::Fuse::Controls::Panel::New3();
    loadingPanel1_Opacity_inst = ::g::OSP_FuseElementsElement_Opacity_Property::New1(loadingPanel1, WorkInfo::__selector3_);
    WorkPage1 = ::g::Fuse::Controls::ScrollView::New4();
    WorkPage1_Opacity_inst = ::g::OSP_FuseElementsElement_Opacity_Property::New1(WorkPage1, WorkInfo::__selector3_);
    ::g::Fuse::Controls::Text* temp4 = ::g::Fuse::Controls::Text::New3();
    temp4_Value_inst = ::g::OSP_FuseControlsTextControl_Value_Property::New1(temp4, WorkInfo::__selector4_);
    ::g::Fuse::Reactive::Data* temp20 = ::g::Fuse::Reactive::Data::New1(uString::Const("Msg"));
    ::g::Fuse::Reactive::Data* temp21 = ::g::Fuse::Reactive::Data::New1(uString::Const("Loadtraining"));
    ::g::Fuse::Reactive::Data* temp22 = ::g::Fuse::Reactive::Data::New1(uString::Const("dashboard"));
    load1 = ::g::Fuse::Controls::Image::New3();
    load1_Opacity_inst = ::g::OSP_FuseElementsElement_Opacity_Property::New1(load1, WorkInfo::__selector3_);
    errorMsg1 = ::g::Fuse::Controls::StackPanel::New4();
    errorMsg1_Opacity_inst = ::g::OSP_FuseElementsElement_Opacity_Property::New1(errorMsg1, WorkInfo::__selector3_);
    ::g::Fuse::Triggers::WhileTrue* temp5 = ::g::Fuse::Triggers::WhileTrue::New2();
    temp5_Value_inst = ::g::OSP_FuseTriggersWhileBool_Value_Property::New1(temp5, WorkInfo::__selector4_);
    ::g::Fuse::Reactive::Data* temp23 = ::g::Fuse::Reactive::Data::New1(uString::Const("error"));
    ::g::Fuse::Triggers::WhileFalse* temp6 = ::g::Fuse::Triggers::WhileFalse::New2();
    temp6_Value_inst = ::g::OSP_FuseTriggersWhileBool_Value_Property::New1(temp6, WorkInfo::__selector4_);
    ::g::Fuse::Reactive::Data* temp24 = ::g::Fuse::Reactive::Data::New1(uString::Const("error"));
    ::g::Fuse::Reactive::Data* temp25 = ::g::Fuse::Reactive::Data::New1(uString::Const("Loadtraining"));
    ::g::Fuse::Reactive::Each* temp7 = ::g::Fuse::Reactive::Each::New4();
    temp7_Items_inst = ::g::OSP_FuseReactiveEach_Items_Property::New1(temp7, WorkInfo::__selector5_);
    ::g::Fuse::Reactive::Data* temp26 = ::g::Fuse::Reactive::Data::New1(uString::Const("training"));
    medical = ::g::Tab::New4();
    medical_Color_inst = ::g::OSP_FuseControlsPanel_Color_Property::New1(medical, WorkInfo::__selector1_);
    medical_TColor_inst = ::g::OSP_Tab_TColor_Property::New1(medical, WorkInfo::__selector2_);
    loadingPanel2 = ::g::Fuse::Controls::Panel::New3();
    loadingPanel2_Opacity_inst = ::g::OSP_FuseElementsElement_Opacity_Property::New1(loadingPanel2, WorkInfo::__selector3_);
    WorkPage2 = ::g::Fuse::Controls::ScrollView::New4();
    WorkPage2_Opacity_inst = ::g::OSP_FuseElementsElement_Opacity_Property::New1(WorkPage2, WorkInfo::__selector3_);
    ::g::Fuse::Controls::Text* temp8 = ::g::Fuse::Controls::Text::New3();
    temp8_Value_inst = ::g::OSP_FuseControlsTextControl_Value_Property::New1(temp8, WorkInfo::__selector4_);
    ::g::Fuse::Reactive::Data* temp27 = ::g::Fuse::Reactive::Data::New1(uString::Const("Msg"));
    ::g::Fuse::Reactive::Data* temp28 = ::g::Fuse::Reactive::Data::New1(uString::Const("LoadMedical"));
    ::g::Fuse::Reactive::Data* temp29 = ::g::Fuse::Reactive::Data::New1(uString::Const("dashboard"));
    load2 = ::g::Fuse::Controls::Image::New3();
    load2_Opacity_inst = ::g::OSP_FuseElementsElement_Opacity_Property::New1(load2, WorkInfo::__selector3_);
    errorMsg2 = ::g::Fuse::Controls::StackPanel::New4();
    errorMsg2_Opacity_inst = ::g::OSP_FuseElementsElement_Opacity_Property::New1(errorMsg2, WorkInfo::__selector3_);
    ::g::Fuse::Triggers::WhileTrue* temp9 = ::g::Fuse::Triggers::WhileTrue::New2();
    temp9_Value_inst = ::g::OSP_FuseTriggersWhileBool_Value_Property::New1(temp9, WorkInfo::__selector4_);
    ::g::Fuse::Reactive::Data* temp30 = ::g::Fuse::Reactive::Data::New1(uString::Const("error"));
    ::g::Fuse::Triggers::WhileFalse* temp10 = ::g::Fuse::Triggers::WhileFalse::New2();
    temp10_Value_inst = ::g::OSP_FuseTriggersWhileBool_Value_Property::New1(temp10, WorkInfo::__selector4_);
    ::g::Fuse::Reactive::Data* temp31 = ::g::Fuse::Reactive::Data::New1(uString::Const("error"));
    ::g::Fuse::Reactive::Data* temp32 = ::g::Fuse::Reactive::Data::New1(uString::Const("LoadMedical"));
    ::g::Fuse::Reactive::Each* temp11 = ::g::Fuse::Reactive::Each::New4();
    temp11_Items_inst = ::g::OSP_FuseReactiveEach_Items_Property::New1(temp11, WorkInfo::__selector5_);
    ::g::Fuse::Reactive::Data* temp33 = ::g::Fuse::Reactive::Data::New1(uString::Const("medical"));
    ::g::Fuse::Reactive::JavaScript* temp34 = ::g::Fuse::Reactive::JavaScript::New2(__g_nametable1);
    ::g::Fuse::Reactive::JavaScript* temp35 = ::g::Fuse::Reactive::JavaScript::New2(__g_nametable1);
    ::g::Fuse::Controls::Grid* temp36 = ::g::Fuse::Controls::Grid::New4();
    ::g::TopBar* temp37 = ::g::TopBar::New5();
    ::g::Fuse::Controls::StackPanel* temp38 = ::g::Fuse::Controls::StackPanel::New4();
    ::g::Fuse::Controls::Grid* temp39 = ::g::Fuse::Controls::Grid::New4();
    page1Tab = ::g::Fuse::Controls::Panel::New3();
    ::g::Fuse::Gestures::Clicked* temp40 = ::g::Fuse::Gestures::Clicked::New2();
    ::g::Fuse::Triggers::Actions::Set* temp41 = (::g::Fuse::Triggers::Actions::Set*)::g::Fuse::Triggers::Actions::Set::New2(::g::Fuse::Triggers::Actions::Set_typeof()->MakeType(::g::Fuse::Visual_typeof(), NULL), navigation_Active_inst);
    page2Tab = ::g::Fuse::Controls::Panel::New3();
    ::g::Fuse::Gestures::Clicked* temp42 = ::g::Fuse::Gestures::Clicked::New2();
    ::g::Fuse::Triggers::Actions::Set* temp43 = (::g::Fuse::Triggers::Actions::Set*)::g::Fuse::Triggers::Actions::Set::New2(::g::Fuse::Triggers::Actions::Set_typeof()->MakeType(::g::Fuse::Visual_typeof(), NULL), navigation_Active_inst);
    ::g::Fuse::Triggers::Actions::Callback* temp44 = ::g::Fuse::Triggers::Actions::Callback::New2();
    temp_eb26 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp12);
    page3Tab = ::g::Fuse::Controls::Panel::New3();
    ::g::Fuse::Gestures::Clicked* temp45 = ::g::Fuse::Gestures::Clicked::New2();
    ::g::Fuse::Triggers::Actions::Set* temp46 = (::g::Fuse::Triggers::Actions::Set*)::g::Fuse::Triggers::Actions::Set::New2(::g::Fuse::Triggers::Actions::Set_typeof()->MakeType(::g::Fuse::Visual_typeof(), NULL), navigation_Active_inst);
    ::g::Fuse::Drawing::StaticSolidColor* temp47 = ::g::Fuse::Drawing::StaticSolidColor::New2(::g::Uno::Float4__New2(0.9686275f, 0.8980392f, 0.3686275f, 1.0f));
    page1 = ::g::Fuse::Controls::Page::New4();
    ::g::Fuse::Navigation::WhileActive* temp48 = ::g::Fuse::Navigation::WhileActive::New2();
    ::g::Fuse::Animations::Change* temp49 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL), permit_Color_inst);
    ::g::Fuse::Animations::Change* temp50 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL), permit_TColor_inst);
    ::g::Fuse::Triggers::WhileBusy* temp51 = ::g::Fuse::Triggers::WhileBusy::New2();
    ::g::Fuse::Animations::Change* temp52 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), loadingPanel_Opacity_inst);
    ::g::Fuse::Animations::Change* temp53 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), WorkPage_Opacity_inst);
    ::g::Fuse::Reactive::DataBinding* temp54 = ::g::Fuse::Reactive::DataBinding::New1(temp_Value_inst, (uObject*)temp13, 3);
    ::g::Fuse::Controls::DockPanel* temp55 = ::g::Fuse::Controls::DockPanel::New4();
    ::g::SubBut* temp56 = ::g::SubBut::New6();
    temp_eb27 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp14);
    ::g::SubBut* temp57 = ::g::SubBut::New6();
    temp_eb28 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp15);
    ::g::Fuse::Animations::Change* temp58 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), load_Opacity_inst);
    ::g::Fuse::Animations::Change* temp59 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), errorMsg_Opacity_inst);
    ::g::Fuse::Reactive::DataBinding* temp60 = ::g::Fuse::Reactive::DataBinding::New1(temp1_Value_inst, (uObject*)temp16, 3);
    ::g::Fuse::Animations::Change* temp61 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), load_Opacity_inst);
    ::g::Fuse::Animations::Spin* temp62 = ::g::Fuse::Animations::Spin::New2();
    ::g::Fuse::Animations::Change* temp63 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), errorMsg_Opacity_inst);
    ::g::Fuse::Reactive::DataBinding* temp64 = ::g::Fuse::Reactive::DataBinding::New1(temp2_Value_inst, (uObject*)temp17, 3);
    ::g::Fuse::Navigation::Activated* temp65 = ::g::Fuse::Navigation::Activated::New2();
    temp_eb29 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp18);
    busy = ::g::Fuse::Triggers::Busy::New2();
    ::g::Fuse::Controls::StackPanel* temp66 = ::g::Fuse::Controls::StackPanel::New4();
    WorkInfo__Template* temp67 = WorkInfo__Template::New2(this, this);
    WorkInfo__Template1* temp68 = WorkInfo__Template1::New2(this, this);
    WorkInfo__Template2* temp69 = WorkInfo__Template2::New2(this, this);
    WorkInfo__Template3* temp70 = WorkInfo__Template3::New2(this, this);
    WorkInfo__Template4* temp71 = WorkInfo__Template4::New2(this, this);
    WorkInfo__Template5* temp72 = WorkInfo__Template5::New2(this, this);
    ::g::Fuse::Reactive::DataBinding* temp73 = ::g::Fuse::Reactive::DataBinding::New1(temp3_Items_inst, (uObject*)temp19, 3);
    ::g::Fuse::Drawing::StaticSolidColor* temp74 = ::g::Fuse::Drawing::StaticSolidColor::New2(::g::Uno::Float4__New2(1.0f, 1.0f, 1.0f, 1.0f));
    page2 = ::g::Fuse::Controls::Page::New4();
    ::g::Fuse::Navigation::WhileActive* temp75 = ::g::Fuse::Navigation::WhileActive::New2();
    ::g::Fuse::Animations::Change* temp76 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL), train_Color_inst);
    ::g::Fuse::Animations::Change* temp77 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL), train_TColor_inst);
    ::g::Fuse::Navigation::WhileInactive* temp78 = ::g::Fuse::Navigation::WhileInactive::New2();
    ::g::Fuse::Animations::Change* temp79 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL), train_Color_inst);
    ::g::Fuse::Triggers::WhileBusy* temp80 = ::g::Fuse::Triggers::WhileBusy::New2();
    ::g::Fuse::Animations::Change* temp81 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), loadingPanel1_Opacity_inst);
    ::g::Fuse::Animations::Change* temp82 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), WorkPage1_Opacity_inst);
    ::g::Fuse::Reactive::DataBinding* temp83 = ::g::Fuse::Reactive::DataBinding::New1(temp4_Value_inst, (uObject*)temp20, 3);
    ::g::Fuse::Controls::DockPanel* temp84 = ::g::Fuse::Controls::DockPanel::New4();
    ::g::SubBut* temp85 = ::g::SubBut::New6();
    temp_eb30 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp21);
    ::g::SubBut* temp86 = ::g::SubBut::New6();
    temp_eb31 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp22);
    ::g::Fuse::Animations::Change* temp87 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), load1_Opacity_inst);
    ::g::Fuse::Animations::Change* temp88 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), errorMsg1_Opacity_inst);
    ::g::Fuse::Reactive::DataBinding* temp89 = ::g::Fuse::Reactive::DataBinding::New1(temp5_Value_inst, (uObject*)temp23, 3);
    ::g::Fuse::Animations::Change* temp90 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), load1_Opacity_inst);
    ::g::Fuse::Animations::Spin* temp91 = ::g::Fuse::Animations::Spin::New2();
    ::g::Fuse::Animations::Change* temp92 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), errorMsg1_Opacity_inst);
    ::g::Fuse::Reactive::DataBinding* temp93 = ::g::Fuse::Reactive::DataBinding::New1(temp6_Value_inst, (uObject*)temp24, 3);
    ::g::Fuse::Navigation::Activated* temp94 = ::g::Fuse::Navigation::Activated::New2();
    temp_eb32 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp25);
    busy1 = ::g::Fuse::Triggers::Busy::New2();
    ::g::Fuse::Controls::StackPanel* temp95 = ::g::Fuse::Controls::StackPanel::New4();
    WorkInfo__Template6* temp96 = WorkInfo__Template6::New2(this, this);
    WorkInfo__Template7* temp97 = WorkInfo__Template7::New2(this, this);
    WorkInfo__Template8* temp98 = WorkInfo__Template8::New2(this, this);
    WorkInfo__Template9* temp99 = WorkInfo__Template9::New2(this, this);
    WorkInfo__Template10* temp100 = WorkInfo__Template10::New2(this, this);
    ::g::Fuse::Reactive::DataBinding* temp101 = ::g::Fuse::Reactive::DataBinding::New1(temp7_Items_inst, (uObject*)temp26, 3);
    ::g::Fuse::Drawing::StaticSolidColor* temp102 = ::g::Fuse::Drawing::StaticSolidColor::New2(::g::Uno::Float4__New2(1.0f, 1.0f, 1.0f, 1.0f));
    page3 = ::g::Fuse::Controls::Page::New4();
    ::g::Fuse::Navigation::WhileActive* temp103 = ::g::Fuse::Navigation::WhileActive::New2();
    ::g::Fuse::Animations::Change* temp104 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL), medical_Color_inst);
    ::g::Fuse::Animations::Change* temp105 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL), medical_TColor_inst);
    ::g::Fuse::Navigation::WhileInactive* temp106 = ::g::Fuse::Navigation::WhileInactive::New2();
    ::g::Fuse::Animations::Change* temp107 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL), medical_Color_inst);
    ::g::Fuse::Triggers::WhileBusy* temp108 = ::g::Fuse::Triggers::WhileBusy::New2();
    ::g::Fuse::Animations::Change* temp109 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), loadingPanel2_Opacity_inst);
    ::g::Fuse::Animations::Change* temp110 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), WorkPage2_Opacity_inst);
    ::g::Fuse::Reactive::DataBinding* temp111 = ::g::Fuse::Reactive::DataBinding::New1(temp8_Value_inst, (uObject*)temp27, 3);
    ::g::Fuse::Controls::DockPanel* temp112 = ::g::Fuse::Controls::DockPanel::New4();
    ::g::SubBut* temp113 = ::g::SubBut::New6();
    temp_eb33 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp28);
    ::g::SubBut* temp114 = ::g::SubBut::New6();
    temp_eb34 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp29);
    ::g::Fuse::Animations::Change* temp115 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), load2_Opacity_inst);
    ::g::Fuse::Animations::Change* temp116 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), errorMsg2_Opacity_inst);
    ::g::Fuse::Reactive::DataBinding* temp117 = ::g::Fuse::Reactive::DataBinding::New1(temp9_Value_inst, (uObject*)temp30, 3);
    ::g::Fuse::Animations::Change* temp118 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), load2_Opacity_inst);
    ::g::Fuse::Animations::Spin* temp119 = ::g::Fuse::Animations::Spin::New2();
    ::g::Fuse::Animations::Change* temp120 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), errorMsg2_Opacity_inst);
    ::g::Fuse::Reactive::DataBinding* temp121 = ::g::Fuse::Reactive::DataBinding::New1(temp10_Value_inst, (uObject*)temp31, 3);
    ::g::Fuse::Navigation::Activated* temp122 = ::g::Fuse::Navigation::Activated::New2();
    temp_eb35 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp32);
    busy2 = ::g::Fuse::Triggers::Busy::New2();
    ::g::Fuse::Controls::StackPanel* temp123 = ::g::Fuse::Controls::StackPanel::New4();
    WorkInfo__Template11* temp124 = WorkInfo__Template11::New2(this, this);
    WorkInfo__Template12* temp125 = WorkInfo__Template12::New2(this, this);
    WorkInfo__Template13* temp126 = WorkInfo__Template13::New2(this, this);
    WorkInfo__Template14* temp127 = WorkInfo__Template14::New2(this, this);
    WorkInfo__Template15* temp128 = WorkInfo__Template15::New2(this, this);
    ::g::Fuse::Reactive::DataBinding* temp129 = ::g::Fuse::Reactive::DataBinding::New1(temp11_Items_inst, (uObject*)temp33, 3);
    ::g::Fuse::Drawing::StaticSolidColor* temp130 = ::g::Fuse::Drawing::StaticSolidColor::New2(::g::Uno::Float4__New2(1.0f, 1.0f, 1.0f, 1.0f));
    Color(::g::Uno::Float4__New2(1.0f, 1.0f, 1.0f, 1.0f));
    SourceLineNumber(1);
    SourceFileName(uString::Const("workinfo.ux"));
    temp34->LineNumber(3);
    temp34->FileName(uString::Const("workinfo.ux"));
    temp34->SourceLineNumber(3);
    temp34->SourceFileName(uString::Const("workinfo.ux"));
    temp34->File(::g::Uno::UX::BundleFileSource::New1(::g::OSP_bundle::navigationb176862e()));
    temp35->Code(uString::Const("\n"
        "\t\tvar API = require('/js_files/api.js');\n"
        "\t\tvar Observable = require('FuseJS/Observable');\n"
        "\t\tvar Storage = require(\"FuseJS/Storage\");\n"
        "\t\tdataPath = API.readOSPID();\n"
        "\t\tvar permit = Observable();\n"
        "\t\tvar training = Observable();\n"
        "\t\tvar medical = Observable();\n"
        "\n"
        "\t\tvar login = require('js_files/login.js');\n"
        "\t\tvar error = Observable(false);\n"
        "\t\tvar Msg = Observable(\"There was an error\");\n"
        "\n"
        "\t\tfunction startLoad() {\n"
        "\t\t\terror.value = false;\n"
        "\t\t\tbusy.activate();\n"
        "\t\t\tStorage.read(dataPath).then(function(content) {\n"
        "\t\t\t    var cont = JSON.parse(content);\n"
        "\t\t\t    personnel_id = cont.personnel[0].personnel_id;\n"
        "\t\t\t    fetch('http://41.75.207.60/osp/public/api/permits/'+personnel_id )\n"
        "\t\t\t\t\t.then(function(response) { \n"
        "\t\t\t\t\t\tstatus = response.status;  // Get the HTTP status code\n"
        "\t\t    \t\t\tresponse_ok = response.ok; // Is response.status in the 200-range?\n"
        "\t\t\t\t\t\treturn response.json(); \n"
        "\t\t\t\t\t})\n"
        "\t\t\t\t.then(function (responseObject) {\n"
        "\t\t\t\tif(responseObject != 0)\n"
        "\t\t\t\t{\n"
        "\t\t\t\t\tpermit.replaceAll(responseObject);\n"
        "\t\t\t\t\t//console.log(\"it works: \"+JSON.stringify(responseObject));\t\n"
        "\t\t\t\t}\n"
        "\t\t\t\telse\n"
        "\t\t\t\t{\n"
        "\t\t\t\t\tMsg.value = \"User has no permits record\";\n"
        "\t\t\t\t\t \n"
        "\t\t\t\t}\n"
        "\t\t\t\tbusy.deactivate();\n"
        "\t\t\t}).catch(function(err) {\n"
        "\t\t\t\tconsole.log(err+ \" dSome There was an error\");\n"
        "\t\t\t\tconsole.log(status)\n"
        "\t\t\t\tif (status == 502) {\n"
        "\t\t\t\t\t// console.log(\"Error Address\");\n"
        "\t\t\t\t\tMsg.value = \"Wrong Address\";\n"
        "\t\t\t\t}else{\n"
        "\t\t\t\t console.log(JSON.stringify(responseObject));\n"
        "\t\t\t\t\tconsole.log(\"Record was not found\");\n"
        "\t\t\t\t\tMsg.value = \"User has no permits record\";\n"
        "\t\t\t\t}\n"
        "\t\t\t\t// console.log(response_ok);\n"
        "\t\t\t\t busy.deactivate();\n"
        "\t\t\t\terror.value = true;\n"
        "\t\t\t});\n"
        "\t\t\t    \n"
        "\t\t\t    \n"
        "\t\t\t    \n"
        "\n"
        "\t\t\t}, function(error) {\n"
        "\t\t\t    //For now, let's expect the error to be because of the file not being found.\n"
        "\t\t\t    //welcomeText.value = \"There is currently no local data stored\";\n"
        "\t\t\t});\n"
        "\t\t\t\n"
        "\t\t}\n"
        "\n"
        "\t\tfunction dstartLoad() {\n"
        "\t\t\terror.value = false;\n"
        "\t\t\tbusy.activate();\n"
        "\t\t\t\n"
        "\t\t}\n"
        "\n"
        "\t\tfunction Loadtraining() {\n"
        "\t\t\terror.value = false;\n"
        "\t\t\tbusy1.activate();\n"
        "\t\t\tStorage.read(dataPath).then(function(content) {\n"
        "\t\t\t    var cont = JSON.parse(content);\n"
        "\t\t\t    personnel_id = cont.personnel[0].personnel_id;\n"
        "\t\t\t    fetch('http://41.75.207.60/osp/public/api/training/'+personnel_id )\n"
        "\t\t\t\t\t.then(function(response) { \n"
        "\t\t\t\t\t\tstatus = response.status;  // Get the HTTP status code\n"
        "\t\t    \t\t\tresponse_ok = response.ok; // Is response.status in the 200-range?\n"
        "\t\t\t\t\t\treturn response.json(); \n"
        "\t\t\t\t\t})\n"
        "\t\t\t\t.then(function (responseObject) {\n"
        "\t\t\t\tif(responseObject != 0)\n"
        "\t\t\t\t{\n"
        "\t\t\t\t\ttraining.replaceAll(responseObject);\n"
        "\t\t\t\t\t//console.log(\"it works: \"+JSON.stringify(responseObject));\t\n"
        "\t\t\t\t}\n"
        "\t\t\t\telse\n"
        "\t\t\t\t{\n"
        "\t\t\t\t\tMsg.value = \"User has no training record\";\n"
        "\t\t\t\t\t \n"
        "\t\t\t\t}\n"
        "\t\t\t\tbusy1.deactivate();\n"
        "\t\t\t}).catch(function(err) {\n"
        "\t\t\t\tconsole.log(err+ \" dSome There was an error\");\n"
        "\t\t\t\tconsole.log(status)\n"
        "\t\t\t\tif (status == 502) {\n"
        "\t\t\t\t\t// console.log(\"Error Address\");\n"
        "\t\t\t\t\tMsg.value = \"Wrong Address\";\n"
        "\t\t\t\t}else{\n"
        "\t\t\t\t console.log(JSON.stringify(responseObject));\n"
        "\t\t\t\t\tconsole.log(\"Record was not found\");\n"
        "\t\t\t\t\tMsg.value = \"User has no training record\";\n"
        "\t\t\t\t}\n"
        "\t\t\t\t// console.log(response_ok);\n"
        "\t\t\t\t busy1.deactivate();\n"
        "\t\t\t\terror.value = true;\n"
        "\t\t\t});\n"
        "\t\t\t    \n"
        "\t\t\t    \n"
        "\t\t\t    \n"
        "\n"
        "\t\t\t}, function(error) {\n"
        "\t\t\t    //For now, let's expect the error to be because of the file not being found.\n"
        "\t\t\t    //welcomeText.value = \"There is currently no local data stored\";\n"
        "\t\t\t});\n"
        "\t\t}\n"
        "\n"
        "\t\tfunction LoadMedical() {\n"
        "\t\t\terror.value = false;\n"
        "\t\t\tbusy2.activate();\n"
        "\t\t\tStorage.read(dataPath).then(function(content) {\n"
        "\t\t\t    var cont = JSON.parse(content);\n"
        "\t\t\t    personnel_id = cont.personnel[0].personnel_id;\n"
        "\t\t\t    fetch('http://41.75.207.60/osp/public/api/medical/'+personnel_id )\n"
        "\t\t\t\t\t.then(function(response) { \n"
        "\t\t\t\t\t\tstatus = response.status;  // Get the HTTP status code\n"
        "\t\t    \t\t\tresponse_ok = response.ok; // Is response.status in the 200-range?\n"
        "\t\t\t\t\t\treturn response.json(); \n"
        "\t\t\t\t\t})\n"
        "\t\t\t\t.then(function (responseObject) {\n"
        "\t\t\t\tif(responseObject != 0)\n"
        "\t\t\t\t{\n"
        "\t\t\t\t\tmedical.replaceAll(responseObject);\n"
        "\t\t\t\t\t//console.log(\"it works: \"+JSON.stringify(responseObject));\t\n"
        "\t\t\t\t}\n"
        "\t\t\t\telse\n"
        "\t\t\t\t{\n"
        "\t\t\t\t\tMsg.value = \"User has no medical record\";\n"
        "\t\t\t\t\t \n"
        "\t\t\t\t}\n"
        "\t\t\t\tbusy2.deactivate();\n"
        "\t\t\t}).catch(function(err) {\n"
        "\t\t\t\tconsole.log(err+ \" dSome There was an error\");\n"
        "\t\t\t\tconsole.log(status)\n"
        "\t\t\t\tif (status == 502) {\n"
        "\t\t\t\t\t// console.log(\"Error Address\");\n"
        "\t\t\t\t\tMsg.value = \"Wrong Address\";\n"
        "\t\t\t\t}else{\n"
        "\t\t\t\t console.log(JSON.stringify(responseObject));\n"
        "\t\t\t\t\tconsole.log(\"Record was not found\");\n"
        "\t\t\t\t\tMsg.value = \"User has no medical record\";\n"
        "\t\t\t\t}\n"
        "\t\t\t\t// console.log(response_ok);\n"
        "\t\t\t\t busy2.deactivate();\n"
        "\t\t\t\terror.value = true;\n"
        "\t\t\t});\n"
        "\t\t\t    \n"
        "\t\t\t    \n"
        "\t\t\t    \n"
        "\n"
        "\t\t\t}, function(error) {\n"
        "\t\t\t    //For now, let's expect the error to be because of the file not being found.\n"
        "\t\t\t    //welcomeText.value = \"There is currently no local data stored\";\n"
        "\t\t\t});\n"
        "\n"
        "\t\t}\n"
        "\n"
        "\t\t\n"
        "\t\tmodule.exports={\n"
        "\t\t\tpermit: permit,\n"
        "\t\t\ttraining: training,\n"
        "\t\t\tmedical: medical,\n"
        "\t\t\tstartLoad: startLoad,\n"
        "\t\t\tLoadtraining: Loadtraining,\n"
        "\t\t\tLoadMedical: LoadMedical,\n"
        "\t\t\terror: error,\n"
        "\t\t\tMsg: Msg\n"
        "\t\t};\n"
        "\t"));
    temp35->LineNumber(4);
    temp35->FileName(uString::Const("workinfo.ux"));
    temp35->SourceLineNumber(4);
    temp35->SourceFileName(uString::Const("workinfo.ux"));
    temp36->Rows(uString::Const("1*,15*"));
    temp36->SourceLineNumber(189);
    temp36->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp36->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp37);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp36->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp38);
    temp37->Title(uString::Const("WORK INFO"));
    temp37->SourceLineNumber(191);
    temp37->SourceFileName(uString::Const("workinfo.ux"));
    temp38->Padding(::g::Uno::Float4__New2(0.0f, 20.0f, 0.0f, 0.0f));
    temp38->SourceLineNumber(193);
    temp38->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp38->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp39);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp38->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), navigation);
    temp39->ColumnCount(3);
    temp39->Width(::g::Uno::UX::Size__New1(90.0f, 4));
    temp39->Height(::g::Uno::UX::Size__New1(50.0f, 1));
    temp39->SourceLineNumber(194);
    temp39->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Fuse::Controls::DockPanel::SetDock(temp39, 2);
    temp39->Background(temp47);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp39->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), page1Tab);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp39->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), page2Tab);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp39->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), page3Tab);
    uPtr(page1Tab)->Name(WorkInfo::__selector6_);
    uPtr(page1Tab)->SourceLineNumber(195);
    uPtr(page1Tab)->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(page1Tab)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), permit);
    uPtr(permit)->Text(uString::Const("PERMIT"));
    uPtr(permit)->Color(::g::Uno::Float4__New2(0.0f, 0.1647059f, 0.2901961f, 1.0f));
    uPtr(permit)->Name(WorkInfo::__selector7_);
    uPtr(permit)->SourceLineNumber(196);
    uPtr(permit)->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(permit)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp40);
    temp40->SourceLineNumber(197);
    temp40->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp40->Actions()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Triggers::Actions::TriggerAction_typeof(), NULL)), temp41);
    ::g::Fuse::Triggers::Actions::Set__set_Value_fn(temp41, page1);
    temp41->SourceLineNumber(198);
    temp41->SourceFileName(uString::Const("workinfo.ux"));
    uPtr(page2Tab)->Name(WorkInfo::__selector8_);
    uPtr(page2Tab)->SourceLineNumber(202);
    uPtr(page2Tab)->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(page2Tab)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), train);
    uPtr(train)->Text(uString::Const("TRAINING"));
    uPtr(train)->Name(WorkInfo::__selector9_);
    uPtr(train)->SourceLineNumber(203);
    uPtr(train)->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(train)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp42);
    temp42->SourceLineNumber(204);
    temp42->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp42->Actions()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Triggers::Actions::TriggerAction_typeof(), NULL)), temp43);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp42->Actions()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Triggers::Actions::TriggerAction_typeof(), NULL)), temp44);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp42->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp_eb26);
    ::g::Fuse::Triggers::Actions::Set__set_Value_fn(temp43, page2);
    temp43->SourceLineNumber(205);
    temp43->SourceFileName(uString::Const("workinfo.ux"));
    temp44->SourceLineNumber(206);
    temp44->SourceFileName(uString::Const("workinfo.ux"));
    temp44->add_Handler(uDelegate::New(::g::Fuse::VisualEventHandler_typeof(), (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb26)));
    temp12->SourceLineNumber(206);
    temp12->SourceFileName(uString::Const("workinfo.ux"));
    uPtr(page3Tab)->Name(WorkInfo::__selector10_);
    uPtr(page3Tab)->SourceLineNumber(210);
    uPtr(page3Tab)->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(page3Tab)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), medical);
    uPtr(medical)->Text(uString::Const("MEDICAL"));
    uPtr(medical)->Name(WorkInfo::__selector11_);
    uPtr(medical)->SourceLineNumber(211);
    uPtr(medical)->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(medical)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp45);
    temp45->SourceLineNumber(212);
    temp45->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp45->Actions()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Triggers::Actions::TriggerAction_typeof(), NULL)), temp46);
    ::g::Fuse::Triggers::Actions::Set__set_Value_fn(temp46, page3);
    temp46->SourceLineNumber(213);
    temp46->SourceFileName(uString::Const("workinfo.ux"));
    uPtr(navigation)->Name(WorkInfo::__selector12_);
    uPtr(navigation)->SourceLineNumber(219);
    uPtr(navigation)->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(navigation)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), page1);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(navigation)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), page2);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(navigation)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), page3);
    uPtr(page1)->Name(WorkInfo::__selector13_);
    uPtr(page1)->SourceLineNumber(220);
    uPtr(page1)->SourceFileName(uString::Const("workinfo.ux"));
    uPtr(page1)->Background(temp74);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(page1)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp48);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(page1)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp51);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(page1)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), loadingPanel);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(page1)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp65);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(page1)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), busy);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(page1)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), WorkPage);
    temp48->Threshold(0.5f);
    temp48->SourceLineNumber(222);
    temp48->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp48->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp49);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp48->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp50);
    ::g::Fuse::Animations::Change__set_Value_fn(temp49, uCRef(::g::Uno::Float4__New2(0.9686275f, 0.8980392f, 0.3686275f, 1.0f)));
    ::g::Fuse::Animations::Change__set_Value_fn(temp50, uCRef(::g::Uno::Float4__New2(0.0f, 0.1647059f, 0.2901961f, 1.0f)));
    temp51->SourceLineNumber(227);
    temp51->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp51->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp52);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp51->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp53);
    ::g::Fuse::Animations::Change__set_Value_fn(temp52, uCRef(1.0f));
    temp52->Duration(0.5);
    ::g::Fuse::Animations::Change__set_Value_fn(temp53, uCRef(0.0f));
    uPtr(loadingPanel)->Color(::g::Uno::Float4__New2(0.9686275f, 0.8980392f, 0.372549f, 1.0f));
    uPtr(loadingPanel)->Width(::g::Uno::UX::Size__New1(90.0f, 4));
    uPtr(loadingPanel)->Height(::g::Uno::UX::Size__New1(500.0f, 1));
    uPtr(loadingPanel)->Alignment(10);
    uPtr(loadingPanel)->Opacity(0.0f);
    uPtr(loadingPanel)->Name(WorkInfo::__selector14_);
    uPtr(loadingPanel)->SourceLineNumber(232);
    uPtr(loadingPanel)->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(loadingPanel)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), load);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(loadingPanel)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), errorMsg);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(loadingPanel)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp1);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(loadingPanel)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp2);
    uPtr(load)->Width(::g::Uno::UX::Size__New1(50.0f, 1));
    uPtr(load)->Height(::g::Uno::UX::Size__New1(50.0f, 1));
    uPtr(load)->Alignment(10);
    uPtr(load)->Opacity(1.0f);
    uPtr(load)->Name(WorkInfo::__selector15_);
    uPtr(load)->SourceLineNumber(234);
    uPtr(load)->SourceFileName(uString::Const("workinfo.ux"));
    uPtr(load)->File(::g::Uno::UX::BundleFileSource::New1(::g::OSP_bundle::diskc6e80153()));
    uPtr(errorMsg)->Alignment(10);
    uPtr(errorMsg)->Opacity(0.0f);
    uPtr(errorMsg)->Name(WorkInfo::__selector16_);
    uPtr(errorMsg)->SourceLineNumber(235);
    uPtr(errorMsg)->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(errorMsg)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(errorMsg)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp55);
    temp->TextAlignment(1);
    temp->Color(::g::Uno::Float4__New2(0.8f, 0.0f, 0.0f, 1.0f));
    temp->Alignment(10);
    temp->SourceLineNumber(236);
    temp->SourceFileName(uString::Const("workinfo.ux"));
    temp->Font(::g::MainView::Raleway());
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp54);
    temp13->SourceLineNumber(236);
    temp13->SourceFileName(uString::Const("workinfo.ux"));
    temp55->Margin(::g::Uno::Float4__New2(0.0f, 10.0f, 0.0f, 10.0f));
    temp55->SourceLineNumber(237);
    temp55->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp55->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp56);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp55->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp57);
    temp56->Btext(uString::Const("Retry"));
    temp56->Icon(uString::Const("\357\200\241"));
    temp56->IconColor(::g::Uno::Float4__New2(0.3333333f, 1.0f, 0.4980392f, 1.0f));
    temp56->Margin(::g::Uno::Float4__New2(10.0f, 0.0f, 10.0f, 0.0f));
    temp56->SourceLineNumber(238);
    temp56->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Fuse::Controls::DockPanel::SetDock(temp56, 0);
    ::g::Fuse::Gestures::Clicked::AddHandler(temp56, uDelegate::New(::g::Fuse::Gestures::ClickedHandler_typeof(), (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb27)));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp56->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp_eb27);
    temp14->SourceLineNumber(238);
    temp14->SourceFileName(uString::Const("workinfo.ux"));
    temp57->Btext(uString::Const("Cancel"));
    temp57->Icon(uString::Const("\357\200\215"));
    temp57->IconColor(::g::Uno::Float4__New2(0.8f, 0.0f, 0.0f, 1.0f));
    temp57->Margin(::g::Uno::Float4__New2(10.0f, 0.0f, 10.0f, 0.0f));
    temp57->SourceLineNumber(239);
    temp57->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Fuse::Controls::DockPanel::SetDock(temp57, 0);
    ::g::Fuse::Gestures::Clicked::AddHandler(temp57, uDelegate::New(::g::Fuse::Gestures::ClickedHandler_typeof(), (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb28)));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp57->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp_eb28);
    temp15->SourceLineNumber(239);
    temp15->SourceFileName(uString::Const("workinfo.ux"));
    temp1->SourceLineNumber(245);
    temp1->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp1->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp58);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp1->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp59);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp1->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp60);
    ::g::Fuse::Animations::Change__set_Value_fn(temp58, uCRef(0.0f));
    temp58->Duration(0.5);
    ::g::Fuse::Animations::Change__set_Value_fn(temp59, uCRef(1.0f));
    temp59->Duration(0.5);
    temp16->SourceLineNumber(245);
    temp16->SourceFileName(uString::Const("workinfo.ux"));
    temp2->SourceLineNumber(250);
    temp2->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp2->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp61);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp2->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp62);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp2->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp63);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp2->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp64);
    ::g::Fuse::Animations::Change__set_Value_fn(temp61, uCRef(1.0f));
    temp61->Duration(0.5);
    temp62->Frequency(1.0);
    temp62->Target(load);
    ::g::Fuse::Animations::Change__set_Value_fn(temp63, uCRef(0.0f));
    temp63->Duration(0.5);
    temp17->SourceLineNumber(250);
    temp17->SourceFileName(uString::Const("workinfo.ux"));
    temp65->SourceLineNumber(263);
    temp65->SourceFileName(uString::Const("workinfo.ux"));
    temp65->add_Handler(uDelegate::New(::g::Fuse::Triggers::PulseTrigger__PulseHandler_typeof()->MakeType(::g::Uno::EventArgs_typeof(), NULL), (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb29)));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp65->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp_eb29);
    temp18->SourceLineNumber(263);
    temp18->SourceFileName(uString::Const("workinfo.ux"));
    uPtr(busy)->IsActive(false);
    uPtr(busy)->Name(WorkInfo::__selector17_);
    uPtr(busy)->SourceLineNumber(265);
    uPtr(busy)->SourceFileName(uString::Const("workinfo.ux"));
    uPtr(WorkPage)->Opacity(1.0f);
    uPtr(WorkPage)->Name(WorkInfo::__selector18_);
    uPtr(WorkPage)->SourceLineNumber(267);
    uPtr(WorkPage)->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(WorkPage)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp66);
    temp66->Margin(::g::Uno::Float4__New2(0.0f, 20.0f, 0.0f, 20.0f));
    temp66->SourceLineNumber(268);
    temp66->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp66->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp3);
    temp3->SourceLineNumber(269);
    temp3->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp3->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp67);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp3->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp68);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp3->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp69);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp3->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp70);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp3->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp71);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp3->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp72);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp3->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp73);
    temp19->SourceLineNumber(269);
    temp19->SourceFileName(uString::Const("workinfo.ux"));
    uPtr(page2)->Name(WorkInfo::__selector19_);
    uPtr(page2)->SourceLineNumber(285);
    uPtr(page2)->SourceFileName(uString::Const("workinfo.ux"));
    uPtr(page2)->Background(temp102);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(page2)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp75);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(page2)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp78);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(page2)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp80);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(page2)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), loadingPanel1);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(page2)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp94);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(page2)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), busy1);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(page2)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), WorkPage1);
    temp75->Threshold(0.5f);
    temp75->SourceLineNumber(289);
    temp75->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp75->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp76);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp75->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp77);
    ::g::Fuse::Animations::Change__set_Value_fn(temp76, uCRef(::g::Uno::Float4__New2(0.9686275f, 0.8980392f, 0.3686275f, 1.0f)));
    ::g::Fuse::Animations::Change__set_Value_fn(temp77, uCRef(::g::Uno::Float4__New2(0.0f, 0.1647059f, 0.2901961f, 1.0f)));
    temp78->SourceLineNumber(293);
    temp78->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp78->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp79);
    ::g::Fuse::Animations::Change__set_Value_fn(temp79, uCRef(::g::Uno::Float4__New2(0.0f, 0.1647059f, 0.2901961f, 1.0f)));
    temp80->SourceLineNumber(297);
    temp80->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp80->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp81);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp80->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp82);
    ::g::Fuse::Animations::Change__set_Value_fn(temp81, uCRef(1.0f));
    temp81->Duration(0.5);
    ::g::Fuse::Animations::Change__set_Value_fn(temp82, uCRef(0.0f));
    uPtr(loadingPanel1)->Color(::g::Uno::Float4__New2(0.9686275f, 0.8980392f, 0.372549f, 1.0f));
    uPtr(loadingPanel1)->Width(::g::Uno::UX::Size__New1(90.0f, 4));
    uPtr(loadingPanel1)->Height(::g::Uno::UX::Size__New1(500.0f, 1));
    uPtr(loadingPanel1)->Alignment(10);
    uPtr(loadingPanel1)->Opacity(0.0f);
    uPtr(loadingPanel1)->Name(WorkInfo::__selector20_);
    uPtr(loadingPanel1)->SourceLineNumber(302);
    uPtr(loadingPanel1)->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(loadingPanel1)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), load1);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(loadingPanel1)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), errorMsg1);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(loadingPanel1)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp5);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(loadingPanel1)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp6);
    uPtr(load1)->Width(::g::Uno::UX::Size__New1(50.0f, 1));
    uPtr(load1)->Height(::g::Uno::UX::Size__New1(50.0f, 1));
    uPtr(load1)->Alignment(10);
    uPtr(load1)->Opacity(1.0f);
    uPtr(load1)->Name(WorkInfo::__selector21_);
    uPtr(load1)->SourceLineNumber(304);
    uPtr(load1)->SourceFileName(uString::Const("workinfo.ux"));
    uPtr(load1)->File(::g::Uno::UX::BundleFileSource::New1(::g::OSP_bundle::diskc6e80153()));
    uPtr(errorMsg1)->Alignment(10);
    uPtr(errorMsg1)->Opacity(0.0f);
    uPtr(errorMsg1)->Name(WorkInfo::__selector22_);
    uPtr(errorMsg1)->SourceLineNumber(305);
    uPtr(errorMsg1)->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(errorMsg1)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp4);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(errorMsg1)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp84);
    temp4->TextAlignment(1);
    temp4->Color(::g::Uno::Float4__New2(0.8f, 0.0f, 0.0f, 1.0f));
    temp4->Alignment(10);
    temp4->SourceLineNumber(306);
    temp4->SourceFileName(uString::Const("workinfo.ux"));
    temp4->Font(::g::MainView::Raleway());
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp4->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp83);
    temp20->SourceLineNumber(306);
    temp20->SourceFileName(uString::Const("workinfo.ux"));
    temp84->Margin(::g::Uno::Float4__New2(0.0f, 10.0f, 0.0f, 10.0f));
    temp84->SourceLineNumber(307);
    temp84->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp84->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp85);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp84->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp86);
    temp85->Btext(uString::Const("Retry"));
    temp85->Icon(uString::Const("\357\200\241"));
    temp85->IconColor(::g::Uno::Float4__New2(0.3333333f, 1.0f, 0.4980392f, 1.0f));
    temp85->Margin(::g::Uno::Float4__New2(10.0f, 0.0f, 10.0f, 0.0f));
    temp85->SourceLineNumber(308);
    temp85->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Fuse::Controls::DockPanel::SetDock(temp85, 0);
    ::g::Fuse::Gestures::Clicked::AddHandler(temp85, uDelegate::New(::g::Fuse::Gestures::ClickedHandler_typeof(), (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb30)));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp85->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp_eb30);
    temp21->SourceLineNumber(308);
    temp21->SourceFileName(uString::Const("workinfo.ux"));
    temp86->Btext(uString::Const("Cancel"));
    temp86->Icon(uString::Const("\357\200\215"));
    temp86->IconColor(::g::Uno::Float4__New2(0.8f, 0.0f, 0.0f, 1.0f));
    temp86->Margin(::g::Uno::Float4__New2(10.0f, 0.0f, 10.0f, 0.0f));
    temp86->SourceLineNumber(309);
    temp86->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Fuse::Controls::DockPanel::SetDock(temp86, 0);
    ::g::Fuse::Gestures::Clicked::AddHandler(temp86, uDelegate::New(::g::Fuse::Gestures::ClickedHandler_typeof(), (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb31)));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp86->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp_eb31);
    temp22->SourceLineNumber(309);
    temp22->SourceFileName(uString::Const("workinfo.ux"));
    temp5->SourceLineNumber(315);
    temp5->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp5->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp87);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp5->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp88);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp5->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp89);
    ::g::Fuse::Animations::Change__set_Value_fn(temp87, uCRef(0.0f));
    temp87->Duration(0.5);
    ::g::Fuse::Animations::Change__set_Value_fn(temp88, uCRef(1.0f));
    temp88->Duration(0.5);
    temp23->SourceLineNumber(315);
    temp23->SourceFileName(uString::Const("workinfo.ux"));
    temp6->SourceLineNumber(320);
    temp6->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp6->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp90);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp6->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp91);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp6->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp92);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp6->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp93);
    ::g::Fuse::Animations::Change__set_Value_fn(temp90, uCRef(1.0f));
    temp90->Duration(0.5);
    temp91->Frequency(1.0);
    temp91->Target(load1);
    ::g::Fuse::Animations::Change__set_Value_fn(temp92, uCRef(0.0f));
    temp92->Duration(0.5);
    temp24->SourceLineNumber(320);
    temp24->SourceFileName(uString::Const("workinfo.ux"));
    temp94->SourceLineNumber(333);
    temp94->SourceFileName(uString::Const("workinfo.ux"));
    temp94->add_Handler(uDelegate::New(::g::Fuse::Triggers::PulseTrigger__PulseHandler_typeof()->MakeType(::g::Uno::EventArgs_typeof(), NULL), (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb32)));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp94->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp_eb32);
    temp25->SourceLineNumber(333);
    temp25->SourceFileName(uString::Const("workinfo.ux"));
    uPtr(busy1)->IsActive(false);
    uPtr(busy1)->Name(WorkInfo::__selector23_);
    uPtr(busy1)->SourceLineNumber(335);
    uPtr(busy1)->SourceFileName(uString::Const("workinfo.ux"));
    uPtr(WorkPage1)->Opacity(1.0f);
    uPtr(WorkPage1)->Name(WorkInfo::__selector24_);
    uPtr(WorkPage1)->SourceLineNumber(337);
    uPtr(WorkPage1)->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(WorkPage1)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp95);
    temp95->Margin(::g::Uno::Float4__New2(0.0f, 20.0f, 0.0f, 20.0f));
    temp95->SourceLineNumber(338);
    temp95->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp95->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp7);
    temp7->SourceLineNumber(339);
    temp7->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp7->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp96);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp7->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp97);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp7->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp98);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp7->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp99);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp7->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp100);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp7->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp101);
    temp26->SourceLineNumber(339);
    temp26->SourceFileName(uString::Const("workinfo.ux"));
    uPtr(page3)->Name(WorkInfo::__selector25_);
    uPtr(page3)->SourceLineNumber(353);
    uPtr(page3)->SourceFileName(uString::Const("workinfo.ux"));
    uPtr(page3)->Background(temp130);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(page3)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp103);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(page3)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp106);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(page3)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp108);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(page3)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), loadingPanel2);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(page3)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp122);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(page3)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), busy2);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(page3)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), WorkPage2);
    temp103->Threshold(0.5f);
    temp103->SourceLineNumber(354);
    temp103->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp103->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp104);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp103->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp105);
    ::g::Fuse::Animations::Change__set_Value_fn(temp104, uCRef(::g::Uno::Float4__New2(0.9686275f, 0.8980392f, 0.3686275f, 1.0f)));
    ::g::Fuse::Animations::Change__set_Value_fn(temp105, uCRef(::g::Uno::Float4__New2(0.0f, 0.1647059f, 0.2901961f, 1.0f)));
    temp106->SourceLineNumber(358);
    temp106->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp106->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp107);
    ::g::Fuse::Animations::Change__set_Value_fn(temp107, uCRef(::g::Uno::Float4__New2(0.0f, 0.1647059f, 0.2901961f, 1.0f)));
    temp108->SourceLineNumber(365);
    temp108->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp108->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp109);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp108->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp110);
    ::g::Fuse::Animations::Change__set_Value_fn(temp109, uCRef(1.0f));
    temp109->Duration(0.5);
    ::g::Fuse::Animations::Change__set_Value_fn(temp110, uCRef(0.0f));
    uPtr(loadingPanel2)->Color(::g::Uno::Float4__New2(0.9686275f, 0.8980392f, 0.372549f, 1.0f));
    uPtr(loadingPanel2)->Width(::g::Uno::UX::Size__New1(90.0f, 4));
    uPtr(loadingPanel2)->Height(::g::Uno::UX::Size__New1(500.0f, 1));
    uPtr(loadingPanel2)->Alignment(10);
    uPtr(loadingPanel2)->Opacity(0.0f);
    uPtr(loadingPanel2)->Name(WorkInfo::__selector26_);
    uPtr(loadingPanel2)->SourceLineNumber(370);
    uPtr(loadingPanel2)->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(loadingPanel2)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), load2);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(loadingPanel2)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), errorMsg2);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(loadingPanel2)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp9);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(loadingPanel2)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp10);
    uPtr(load2)->Width(::g::Uno::UX::Size__New1(50.0f, 1));
    uPtr(load2)->Height(::g::Uno::UX::Size__New1(50.0f, 1));
    uPtr(load2)->Alignment(10);
    uPtr(load2)->Opacity(1.0f);
    uPtr(load2)->Name(WorkInfo::__selector27_);
    uPtr(load2)->SourceLineNumber(372);
    uPtr(load2)->SourceFileName(uString::Const("workinfo.ux"));
    uPtr(load2)->File(::g::Uno::UX::BundleFileSource::New1(::g::OSP_bundle::diskc6e80153()));
    uPtr(errorMsg2)->Alignment(10);
    uPtr(errorMsg2)->Opacity(0.0f);
    uPtr(errorMsg2)->Name(WorkInfo::__selector28_);
    uPtr(errorMsg2)->SourceLineNumber(373);
    uPtr(errorMsg2)->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(errorMsg2)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp8);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(errorMsg2)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp112);
    temp8->TextAlignment(1);
    temp8->Color(::g::Uno::Float4__New2(0.8f, 0.0f, 0.0f, 1.0f));
    temp8->Alignment(10);
    temp8->SourceLineNumber(374);
    temp8->SourceFileName(uString::Const("workinfo.ux"));
    temp8->Font(::g::MainView::Raleway());
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp8->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp111);
    temp27->SourceLineNumber(374);
    temp27->SourceFileName(uString::Const("workinfo.ux"));
    temp112->Margin(::g::Uno::Float4__New2(0.0f, 10.0f, 0.0f, 10.0f));
    temp112->SourceLineNumber(375);
    temp112->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp112->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp113);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp112->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp114);
    temp113->Btext(uString::Const("Retry"));
    temp113->Icon(uString::Const("\357\200\241"));
    temp113->IconColor(::g::Uno::Float4__New2(0.3333333f, 1.0f, 0.4980392f, 1.0f));
    temp113->Margin(::g::Uno::Float4__New2(10.0f, 0.0f, 10.0f, 0.0f));
    temp113->SourceLineNumber(376);
    temp113->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Fuse::Controls::DockPanel::SetDock(temp113, 0);
    ::g::Fuse::Gestures::Clicked::AddHandler(temp113, uDelegate::New(::g::Fuse::Gestures::ClickedHandler_typeof(), (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb33)));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp113->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp_eb33);
    temp28->SourceLineNumber(376);
    temp28->SourceFileName(uString::Const("workinfo.ux"));
    temp114->Btext(uString::Const("Cancel"));
    temp114->Icon(uString::Const("\357\200\215"));
    temp114->IconColor(::g::Uno::Float4__New2(0.8f, 0.0f, 0.0f, 1.0f));
    temp114->Margin(::g::Uno::Float4__New2(10.0f, 0.0f, 10.0f, 0.0f));
    temp114->SourceLineNumber(377);
    temp114->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Fuse::Controls::DockPanel::SetDock(temp114, 0);
    ::g::Fuse::Gestures::Clicked::AddHandler(temp114, uDelegate::New(::g::Fuse::Gestures::ClickedHandler_typeof(), (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb34)));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp114->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp_eb34);
    temp29->SourceLineNumber(377);
    temp29->SourceFileName(uString::Const("workinfo.ux"));
    temp9->SourceLineNumber(383);
    temp9->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp9->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp115);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp9->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp116);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp9->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp117);
    ::g::Fuse::Animations::Change__set_Value_fn(temp115, uCRef(0.0f));
    temp115->Duration(0.5);
    ::g::Fuse::Animations::Change__set_Value_fn(temp116, uCRef(1.0f));
    temp116->Duration(0.5);
    temp30->SourceLineNumber(383);
    temp30->SourceFileName(uString::Const("workinfo.ux"));
    temp10->SourceLineNumber(388);
    temp10->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp10->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp118);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp10->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp119);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp10->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp120);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp10->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp121);
    ::g::Fuse::Animations::Change__set_Value_fn(temp118, uCRef(1.0f));
    temp118->Duration(0.5);
    temp119->Frequency(1.0);
    temp119->Target(load1);
    ::g::Fuse::Animations::Change__set_Value_fn(temp120, uCRef(0.0f));
    temp120->Duration(0.5);
    temp31->SourceLineNumber(388);
    temp31->SourceFileName(uString::Const("workinfo.ux"));
    temp122->SourceLineNumber(401);
    temp122->SourceFileName(uString::Const("workinfo.ux"));
    temp122->add_Handler(uDelegate::New(::g::Fuse::Triggers::PulseTrigger__PulseHandler_typeof()->MakeType(::g::Uno::EventArgs_typeof(), NULL), (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb35)));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp122->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp_eb35);
    temp32->SourceLineNumber(401);
    temp32->SourceFileName(uString::Const("workinfo.ux"));
    uPtr(busy2)->IsActive(false);
    uPtr(busy2)->Name(WorkInfo::__selector29_);
    uPtr(busy2)->SourceLineNumber(403);
    uPtr(busy2)->SourceFileName(uString::Const("workinfo.ux"));
    uPtr(WorkPage2)->Opacity(1.0f);
    uPtr(WorkPage2)->Name(WorkInfo::__selector30_);
    uPtr(WorkPage2)->SourceLineNumber(405);
    uPtr(WorkPage2)->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(WorkPage2)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp123);
    temp123->Margin(::g::Uno::Float4__New2(0.0f, 20.0f, 0.0f, 20.0f));
    temp123->SourceLineNumber(406);
    temp123->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp123->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp11);
    temp11->SourceLineNumber(407);
    temp11->SourceFileName(uString::Const("workinfo.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp11->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp124);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp11->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp125);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp11->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp126);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp11->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp127);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp11->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp128);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp11->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp129);
    temp33->SourceLineNumber(407);
    temp33->SourceFileName(uString::Const("workinfo.ux"));
    uPtr(__g_nametable1)->This(this);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), router);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), page1Tab);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), permit);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), page2Tab);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), train);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), temp_eb26);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), page3Tab);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), medical);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), navigation);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), page1);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), loadingPanel);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), load);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), errorMsg);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), temp_eb27);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), temp_eb28);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), temp_eb29);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), busy);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), WorkPage);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), page2);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), loadingPanel1);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), load1);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), errorMsg1);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), temp_eb30);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), temp_eb31);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), temp_eb32);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), busy1);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), WorkPage1);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), page3);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), loadingPanel2);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), load2);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), errorMsg2);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), temp_eb33);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), temp_eb34);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), temp_eb35);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), busy2);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), WorkPage2);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp34);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp35);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp36);
}

// public WorkInfo New(Fuse.Navigation.Router router) [static] :608
WorkInfo* WorkInfo::New5(::g::Fuse::Navigation::Router* router1)
{
    WorkInfo* obj1 = (WorkInfo*)uNew(WorkInfo_typeof());
    obj1->ctor_8(router1);
    return obj1;
}
// }

} // ::g
