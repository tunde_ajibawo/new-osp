// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/WorkInfo.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.InputField.h>
#include <_root.OSP_InputField_-d24e30b7.h>
#include <_root.WorkInfo.h>
#include <_root.WorkInfo.Template10.h>
#include <Fuse.Binding.h>
#include <Fuse.Node.h>
#include <Fuse.Reactive.Add.h>
#include <Fuse.Reactive.BindingMode.h>
#include <Fuse.Reactive.Constant.h>
#include <Fuse.Reactive.Data.h>
#include <Fuse.Reactive.DataBinding.h>
#include <Fuse.Reactive.Expression.h>
#include <Fuse.Reactive.IExpression.h>
#include <Uno.Bool.h>
#include <Uno.Collections.ICollection-1.h>
#include <Uno.Collections.IList-1.h>
#include <Uno.Int.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.UX.Property.h>
#include <Uno.UX.Property1-1.h>
#include <Uno.UX.Selector.h>
static uString* STRINGS[5];
static uType* TYPES[1];

namespace g{

// public partial sealed class WorkInfo.Template10 :306
// {
// static Template10() :316
static void WorkInfo__Template10__cctor__fn(uType* __type)
{
    WorkInfo__Template10::__selector0_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[0/*"PlaceHolder...*/]);
}

static void WorkInfo__Template10_build(uType* type)
{
    ::STRINGS[0] = uString::Const("PlaceHolderName");
    ::STRINGS[1] = uString::Const("valid_for");
    ::STRINGS[2] = uString::Const(" months");
    ::STRINGS[3] = uString::Const("VALID FOR");
    ::STRINGS[4] = uString::Const("workinfo.ux");
    ::TYPES[0] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL);
    type->SetFields(2,
        ::g::WorkInfo_typeof(), offsetof(WorkInfo__Template10, __parent1), uFieldFlagsWeak,
        ::g::WorkInfo_typeof(), offsetof(WorkInfo__Template10, __parentInstance1), uFieldFlagsWeak,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(WorkInfo__Template10, __self_PlaceHolderName_inst1), 0,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo__Template10::__selector0_, uFieldFlagsStatic);
}

::g::Uno::UX::Template_type* WorkInfo__Template10_typeof()
{
    static uSStrong< ::g::Uno::UX::Template_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::Template_typeof();
    options.FieldCount = 6;
    options.ObjectSize = sizeof(WorkInfo__Template10);
    options.TypeSize = sizeof(::g::Uno::UX::Template_type);
    type = (::g::Uno::UX::Template_type*)uClassType::New("WorkInfo.Template10", options);
    type->fp_build_ = WorkInfo__Template10_build;
    type->fp_cctor_ = WorkInfo__Template10__cctor__fn;
    type->fp_New1 = (void(*)(::g::Uno::UX::Template*, uObject**))WorkInfo__Template10__New1_fn;
    return type;
}

// public Template10(WorkInfo parent, WorkInfo parentInstance) :310
void WorkInfo__Template10__ctor_1_fn(WorkInfo__Template10* __this, ::g::WorkInfo* parent, ::g::WorkInfo* parentInstance)
{
    __this->ctor_1(parent, parentInstance);
}

// public override sealed object New() :319
void WorkInfo__Template10__New1_fn(WorkInfo__Template10* __this, uObject** __retval)
{
    ::g::InputField* __self1 = ::g::InputField::New4();
    ::g::Fuse::Reactive::Data* temp = ::g::Fuse::Reactive::Data::New1(::STRINGS[1/*"valid_for"*/]);
    uString* temp1 = ::STRINGS[2/*" months"*/];
    ::g::Fuse::Reactive::Constant* temp2 = ::g::Fuse::Reactive::Constant::New1(temp1);
    __this->__self_PlaceHolderName_inst1 = ::g::OSP_InputField_PlaceHolderName_Property::New1(__self1, WorkInfo__Template10::__selector0_);
    ::g::Fuse::Reactive::Add* temp3 = ::g::Fuse::Reactive::Add::New1(temp, temp2);
    ::g::Fuse::Reactive::DataBinding* temp4 = ::g::Fuse::Reactive::DataBinding::New1(__this->__self_PlaceHolderName_inst1, (uObject*)temp3, 3);
    __self1->Title(::STRINGS[3/*"VALID FOR"*/]);
    __self1->SourceLineNumber(346);
    __self1->SourceFileName(::STRINGS[4/*"workinfo.ux"*/]);
    temp3->SourceLineNumber(346);
    temp3->SourceFileName(::STRINGS[4/*"workinfo.ux"*/]);
    temp->SourceLineNumber(346);
    temp->SourceFileName(::STRINGS[4/*"workinfo.ux"*/]);
    temp2->SourceLineNumber(346);
    temp2->SourceFileName(::STRINGS[4/*"workinfo.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(__self1->Bindings()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp4);
    return *__retval = __self1, void();
}

// public Template10 New(WorkInfo parent, WorkInfo parentInstance) :310
void WorkInfo__Template10__New2_fn(::g::WorkInfo* parent, ::g::WorkInfo* parentInstance, WorkInfo__Template10** __retval)
{
    *__retval = WorkInfo__Template10::New2(parent, parentInstance);
}

::g::Uno::UX::Selector WorkInfo__Template10::__selector0_;

// public Template10(WorkInfo parent, WorkInfo parentInstance) [instance] :310
void WorkInfo__Template10::ctor_1(::g::WorkInfo* parent, ::g::WorkInfo* parentInstance)
{
    ctor_(NULL, false);
    __parent1 = parent;
    __parentInstance1 = parentInstance;
}

// public Template10 New(WorkInfo parent, WorkInfo parentInstance) [static] :310
WorkInfo__Template10* WorkInfo__Template10::New2(::g::WorkInfo* parent, ::g::WorkInfo* parentInstance)
{
    WorkInfo__Template10* obj1 = (WorkInfo__Template10*)uNew(WorkInfo__Template10_typeof());
    obj1->ctor_1(parent, parentInstance);
    return obj1;
}
// }

} // ::g
