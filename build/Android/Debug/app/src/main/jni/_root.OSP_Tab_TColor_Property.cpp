// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.OSP_Tab_TColor_Property.h>
#include <_root.Tab.h>
#include <Uno.Bool.h>
#include <Uno.UX.IPropertyListener.h>
#include <Uno.UX.PropertyObject.h>
#include <Uno.UX.Selector.h>
static uType* TYPES[1];

namespace g{

// internal sealed class OSP_Tab_TColor_Property :693
// {
static void OSP_Tab_TColor_Property_build(uType* type)
{
    ::TYPES[0] = ::g::Tab_typeof();
    type->SetBase(::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL));
    type->SetFields(1,
        ::TYPES[0/*Tab*/], offsetof(OSP_Tab_TColor_Property, _obj), uFieldFlagsWeak);
}

::g::Uno::UX::Property1_type* OSP_Tab_TColor_Property_typeof()
{
    static uSStrong< ::g::Uno::UX::Property1_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::Property1_typeof();
    options.FieldCount = 2;
    options.ObjectSize = sizeof(OSP_Tab_TColor_Property);
    options.TypeSize = sizeof(::g::Uno::UX::Property1_type);
    type = (::g::Uno::UX::Property1_type*)uClassType::New("OSP_Tab_TColor_Property", options);
    type->fp_build_ = OSP_Tab_TColor_Property_build;
    type->fp_Get1 = (void(*)(::g::Uno::UX::Property1*, ::g::Uno::UX::PropertyObject*, uTRef))OSP_Tab_TColor_Property__Get1_fn;
    type->fp_get_Object = (void(*)(::g::Uno::UX::Property*, ::g::Uno::UX::PropertyObject**))OSP_Tab_TColor_Property__get_Object_fn;
    type->fp_Set1 = (void(*)(::g::Uno::UX::Property1*, ::g::Uno::UX::PropertyObject*, void*, uObject*))OSP_Tab_TColor_Property__Set1_fn;
    type->fp_get_SupportsOriginSetter = (void(*)(::g::Uno::UX::PropertyAccessor*, bool*))OSP_Tab_TColor_Property__get_SupportsOriginSetter_fn;
    return type;
}

// public OSP_Tab_TColor_Property(Tab obj, Uno.UX.Selector name) :696
void OSP_Tab_TColor_Property__ctor_3_fn(OSP_Tab_TColor_Property* __this, ::g::Tab* obj, ::g::Uno::UX::Selector* name)
{
    __this->ctor_3(obj, *name);
}

// public override sealed float4 Get(Uno.UX.PropertyObject obj) :698
void OSP_Tab_TColor_Property__Get1_fn(OSP_Tab_TColor_Property* __this, ::g::Uno::UX::PropertyObject* obj, ::g::Uno::Float4* __retval)
{
    return *__retval = uPtr(uCast< ::g::Tab*>(obj, ::TYPES[0/*Tab*/]))->TColor(), void();
}

// public OSP_Tab_TColor_Property New(Tab obj, Uno.UX.Selector name) :696
void OSP_Tab_TColor_Property__New1_fn(::g::Tab* obj, ::g::Uno::UX::Selector* name, OSP_Tab_TColor_Property** __retval)
{
    *__retval = OSP_Tab_TColor_Property::New1(obj, *name);
}

// public override sealed Uno.UX.PropertyObject get_Object() :697
void OSP_Tab_TColor_Property__get_Object_fn(OSP_Tab_TColor_Property* __this, ::g::Uno::UX::PropertyObject** __retval)
{
    return *__retval = __this->_obj, void();
}

// public override sealed void Set(Uno.UX.PropertyObject obj, float4 v, Uno.UX.IPropertyListener origin) :699
void OSP_Tab_TColor_Property__Set1_fn(OSP_Tab_TColor_Property* __this, ::g::Uno::UX::PropertyObject* obj, ::g::Uno::Float4* v, uObject* origin)
{
    ::g::Uno::Float4 v_ = *v;
    uPtr(uCast< ::g::Tab*>(obj, ::TYPES[0/*Tab*/]))->SetTColor(v_, origin);
}

// public override sealed bool get_SupportsOriginSetter() :700
void OSP_Tab_TColor_Property__get_SupportsOriginSetter_fn(OSP_Tab_TColor_Property* __this, bool* __retval)
{
    return *__retval = true, void();
}

// public OSP_Tab_TColor_Property(Tab obj, Uno.UX.Selector name) [instance] :696
void OSP_Tab_TColor_Property::ctor_3(::g::Tab* obj, ::g::Uno::UX::Selector name)
{
    ctor_2(name);
    _obj = obj;
}

// public OSP_Tab_TColor_Property New(Tab obj, Uno.UX.Selector name) [static] :696
OSP_Tab_TColor_Property* OSP_Tab_TColor_Property::New1(::g::Tab* obj, ::g::Uno::UX::Selector name)
{
    OSP_Tab_TColor_Property* obj1 = (OSP_Tab_TColor_Property*)uNew(OSP_Tab_TColor_Property_typeof());
    obj1->ctor_3(obj, name);
    return obj1;
}
// }

} // ::g
