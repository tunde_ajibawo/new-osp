// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/News.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.News.h>
#include <_root.News.Template.h>
#include <_root.NewsItem.h>
#include <Fuse.Node.h>
#include <Uno.Bool.h>
#include <Uno.Int.h>
#include <Uno.Object.h>
#include <Uno.String.h>
static uString* STRINGS[1];

namespace g{

// public partial sealed class News.Template :6
// {
// static Template() :15
static void News__Template__cctor__fn(uType* __type)
{
}

static void News__Template_build(uType* type)
{
    ::STRINGS[0] = uString::Const("news.ux");
    type->SetFields(2,
        ::g::News_typeof(), offsetof(News__Template, __parent1), uFieldFlagsWeak,
        ::g::News_typeof(), offsetof(News__Template, __parentInstance1), uFieldFlagsWeak);
}

::g::Uno::UX::Template_type* News__Template_typeof()
{
    static uSStrong< ::g::Uno::UX::Template_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::Template_typeof();
    options.FieldCount = 4;
    options.ObjectSize = sizeof(News__Template);
    options.TypeSize = sizeof(::g::Uno::UX::Template_type);
    type = (::g::Uno::UX::Template_type*)uClassType::New("News.Template", options);
    type->fp_build_ = News__Template_build;
    type->fp_cctor_ = News__Template__cctor__fn;
    type->fp_New1 = (void(*)(::g::Uno::UX::Template*, uObject**))News__Template__New1_fn;
    return type;
}

// public Template(News parent, News parentInstance) :10
void News__Template__ctor_1_fn(News__Template* __this, ::g::News* parent, ::g::News* parentInstance)
{
    __this->ctor_1(parent, parentInstance);
}

// public override sealed object New() :18
void News__Template__New1_fn(News__Template* __this, uObject** __retval)
{
    ::g::NewsItem* __self1 = ::g::NewsItem::New5();
    __self1->SourceLineNumber(58);
    __self1->SourceFileName(::STRINGS[0/*"news.ux"*/]);
    return *__retval = __self1, void();
}

// public Template New(News parent, News parentInstance) :10
void News__Template__New2_fn(::g::News* parent, ::g::News* parentInstance, News__Template** __retval)
{
    *__retval = News__Template::New2(parent, parentInstance);
}

// public Template(News parent, News parentInstance) [instance] :10
void News__Template::ctor_1(::g::News* parent, ::g::News* parentInstance)
{
    ctor_(NULL, false);
    __parent1 = parent;
    __parentInstance1 = parentInstance;
}

// public Template New(News parent, News parentInstance) [static] :10
News__Template* News__Template::New2(::g::News* parent, ::g::News* parentInstance)
{
    News__Template* obj1 = (News__Template*)uNew(News__Template_typeof());
    obj1->ctor_1(parent, parentInstance);
    return obj1;
}
// }

} // ::g
