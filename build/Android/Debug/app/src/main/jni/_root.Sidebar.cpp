// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/Sidebar.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.MenuLabel.h>
#include <_root.OSP_bundle.h>
#include <_root.OSP_FuseControl-b26a5bbf.h>
#include <_root.OSP_FuseDrawing-f0d4d386.h>
#include <_root.Sidebar.h>
#include <_root.SidebarIcon.h>
#include <_root.Stats.h>
#include <_root.Username.h>
#include <Fuse.Animations.Animator.h>
#include <Fuse.Animations.Easing.h>
#include <Fuse.Animations.Move.h>
#include <Fuse.Animations.TrackAnimator.h>
#include <Fuse.Animations.Trans-997c8373.h>
#include <Fuse.Controls.Circle.h>
#include <Fuse.Controls.Control.h>
#include <Fuse.Controls.Grid.h>
#include <Fuse.Controls.Image.h>
#include <Fuse.Controls.Panel.h>
#include <Fuse.Controls.Rectangle.h>
#include <Fuse.Controls.Shape.h>
#include <Fuse.Controls.StackPanel.h>
#include <Fuse.Controls.Text.h>
#include <Fuse.Controls.TextAlignment.h>
#include <Fuse.Controls.TextControl.h>
#include <Fuse.Drawing.Brush.h>
#include <Fuse.Drawing.ImageFill.h>
#include <Fuse.Drawing.SolidColor.h>
#include <Fuse.Drawing.StaticSolidColor.h>
#include <Fuse.Drawing.WrapMode.h>
#include <Fuse.Elements.Alignment.h>
#include <Fuse.Elements.Element.h>
#include <Fuse.Elements.HitTestMode.h>
#include <Fuse.Gestures.Clicked.h>
#include <Fuse.Gestures.ClickedHandler.h>
#include <Fuse.Navigation.While-89f5a828.h>
#include <Fuse.Navigation.WhileInactive.h>
#include <Fuse.Reactive.BindingMode.h>
#include <Fuse.Reactive.Data.h>
#include <Fuse.Reactive.DataBinding.h>
#include <Fuse.Reactive.EventBinding.h>
#include <Fuse.Reactive.Expression.h>
#include <Fuse.Reactive.IExpression.h>
#include <Fuse.Translation.h>
#include <Fuse.Triggers.Trigger.h>
#include <Uno.Double.h>
#include <Uno.Float.h>
#include <Uno.Float4.h>
#include <Uno.Int.h>
#include <Uno.IO.BundleFile.h>
#include <Uno.String.h>
#include <Uno.UX.BundleFileSource.h>
#include <Uno.UX.FileSource.h>
#include <Uno.UX.Property.h>
#include <Uno.UX.Property1-1.h>
#include <Uno.UX.Selector.h>
#include <Uno.UX.Size.h>
#include <Uno.UX.Size2.h>
#include <Uno.UX.Unit.h>
static uString* STRINGS[21];
static uType* TYPES[5];

namespace g{

// public partial sealed class Sidebar :2
// {
// static Sidebar() :18
static void Sidebar__cctor_4_fn(uType* __type)
{
    Sidebar::__selector0_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[0/*"File"*/]);
    Sidebar::__selector1_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[1/*"Value"*/]);
    Sidebar::__selector2_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[2/*"user"*/]);
    Sidebar::__selector3_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[3/*"stats"*/]);
    Sidebar::__selector4_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[4/*"work"*/]);
    Sidebar::__selector5_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[5/*"notification"*/]);
    Sidebar::__selector6_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[6/*"history"*/]);
    Sidebar::__selector7_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[7/*"news"*/]);
}

static void Sidebar_build(uType* type)
{
    ::STRINGS[0] = uString::Const("File");
    ::STRINGS[1] = uString::Const("Value");
    ::STRINGS[2] = uString::Const("user");
    ::STRINGS[3] = uString::Const("stats");
    ::STRINGS[4] = uString::Const("work");
    ::STRINGS[5] = uString::Const("notification");
    ::STRINGS[6] = uString::Const("history");
    ::STRINGS[7] = uString::Const("news");
    ::STRINGS[8] = uString::Const("image");
    ::STRINGS[9] = uString::Const("username");
    ::STRINGS[10] = uString::Const("profile");
    ::STRINGS[11] = uString::Const("logout");
    ::STRINGS[12] = uString::Const("workinfo");
    ::STRINGS[13] = uString::Const("Sidebar.ux");
    ::STRINGS[14] = uString::Const("PROFILE");
    ::STRINGS[15] = uString::Const("LOG OUT");
    ::STRINGS[16] = uString::Const("WORK INFO");
    ::STRINGS[17] = uString::Const("3");
    ::STRINGS[18] = uString::Const("NOTIFICATIONS");
    ::STRINGS[19] = uString::Const("TRIPS");
    ::STRINGS[20] = uString::Const("NEWS");
    ::TYPES[0] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL);
    ::TYPES[1] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL);
    ::TYPES[2] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Drawing::Brush_typeof(), NULL);
    ::TYPES[3] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL);
    ::TYPES[4] = ::g::Fuse::Gestures::ClickedHandler_typeof();
    type->SetDependencies(
        ::g::Fuse::Animations::Easing_typeof(),
        ::g::OSP_bundle_typeof());
    type->SetInterfaces(
        ::g::Uno::Collections::IList_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface0),
        ::g::Fuse::Scripting::IScriptObject_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface1),
        ::g::Fuse::IProperties_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface2),
        ::g::Fuse::INotifyUnrooted_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface3),
        ::g::Fuse::ISourceLocation_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface4),
        ::TYPES[3/*Uno.Collections.ICollection<Fuse.Binding>*/], offsetof(::g::Fuse::Controls::Panel_type, interface5),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface6),
        ::g::Uno::Collections::IList_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface7),
        ::g::Uno::UX::IPropertyListener_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface8),
        ::g::Fuse::ITemplateSource_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface9),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Visual_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface10),
        ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/], offsetof(::g::Fuse::Controls::Panel_type, interface11),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface12),
        ::g::Fuse::Triggers::Actions::IShow_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface13),
        ::g::Fuse::Triggers::Actions::IHide_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface14),
        ::g::Fuse::Triggers::Actions::ICollapse_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface15),
        ::g::Fuse::IActualPlacement_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface16),
        ::g::Fuse::Animations::IResize_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface17),
        ::g::Fuse::Drawing::ISurfaceDrawable_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface18));
    type->SetFields(116,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::UX::FileSource_typeof(), NULL), offsetof(Sidebar, temp_File_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(Sidebar, temp1_Value_inst), 0,
        ::g::Fuse::Controls::StackPanel_typeof(), offsetof(Sidebar, user), 0,
        ::g::Fuse::Controls::Grid_typeof(), offsetof(Sidebar, stats), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(Sidebar, temp_eb0), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(Sidebar, temp_eb1), 0,
        ::g::Fuse::Controls::StackPanel_typeof(), offsetof(Sidebar, work), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(Sidebar, temp_eb2), 0,
        ::g::Fuse::Controls::StackPanel_typeof(), offsetof(Sidebar, notification), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(Sidebar, temp_eb3), 0,
        ::g::Fuse::Controls::StackPanel_typeof(), offsetof(Sidebar, history), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(Sidebar, temp_eb4), 0,
        ::g::Fuse::Controls::StackPanel_typeof(), offsetof(Sidebar, news), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(Sidebar, temp_eb5), 0,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Sidebar::__selector0_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Sidebar::__selector1_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Sidebar::__selector2_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Sidebar::__selector3_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Sidebar::__selector4_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Sidebar::__selector5_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Sidebar::__selector6_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Sidebar::__selector7_, uFieldFlagsStatic);
}

::g::Fuse::Controls::Panel_type* Sidebar_typeof()
{
    static uSStrong< ::g::Fuse::Controls::Panel_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Fuse::Controls::DockPanel_typeof();
    options.FieldCount = 138;
    options.InterfaceCount = 19;
    options.DependencyCount = 2;
    options.ObjectSize = sizeof(Sidebar);
    options.TypeSize = sizeof(::g::Fuse::Controls::Panel_type);
    type = (::g::Fuse::Controls::Panel_type*)uClassType::New("Sidebar", options);
    type->fp_build_ = Sidebar_build;
    type->fp_ctor_ = (void*)Sidebar__New5_fn;
    type->fp_cctor_ = Sidebar__cctor_4_fn;
    type->interface18.fp_Draw = (void(*)(uObject*, ::g::Fuse::Drawing::Surface*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableDraw_fn;
    type->interface18.fp_get_IsPrimary = (void(*)(uObject*, bool*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableget_IsPrimary_fn;
    type->interface18.fp_get_ElementSize = (void(*)(uObject*, ::g::Uno::Float2*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableget_ElementSize_fn;
    type->interface13.fp_Show = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsIShowShow_fn;
    type->interface15.fp_Collapse = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsICollapseCollapse_fn;
    type->interface14.fp_Hide = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsIHideHide_fn;
    type->interface17.fp_SetSize = (void(*)(uObject*, ::g::Uno::Float2*))::g::Fuse::Elements::Element__FuseAnimationsIResizeSetSize_fn;
    type->interface16.fp_get_ActualSize = (void(*)(uObject*, ::g::Uno::Float3*))::g::Fuse::Elements::Element__FuseIActualPlacementget_ActualSize_fn;
    type->interface16.fp_add_Placed = (void(*)(uObject*, uDelegate*))::g::Fuse::Elements::Element__add_Placed_fn;
    type->interface16.fp_remove_Placed = (void(*)(uObject*, uDelegate*))::g::Fuse::Elements::Element__remove_Placed_fn;
    type->interface10.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Visual__UnoCollectionsIEnumerableFuseVisualGetEnumerator_fn;
    type->interface11.fp_Clear = (void(*)(uObject*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeClear_fn;
    type->interface11.fp_Contains = (void(*)(uObject*, void*, bool*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeContains_fn;
    type->interface7.fp_RemoveAt = (void(*)(uObject*, int32_t*))::g::Fuse::Visual__UnoCollectionsIListFuseNodeRemoveAt_fn;
    type->interface12.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Visual__UnoCollectionsIEnumerableFuseNodeGetEnumerator_fn;
    type->interface11.fp_get_Count = (void(*)(uObject*, int32_t*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeget_Count_fn;
    type->interface7.fp_get_Item = (void(*)(uObject*, int32_t*, uTRef))::g::Fuse::Visual__UnoCollectionsIListFuseNodeget_Item_fn;
    type->interface7.fp_Insert = (void(*)(uObject*, int32_t*, void*))::g::Fuse::Visual__Insert1_fn;
    type->interface8.fp_OnPropertyChanged = (void(*)(uObject*, ::g::Uno::UX::PropertyObject*, ::g::Uno::UX::Selector*))::g::Fuse::Controls::Control__OnPropertyChanged2_fn;
    type->interface9.fp_FindTemplate = (void(*)(uObject*, uString*, ::g::Uno::UX::Template**))::g::Fuse::Visual__FindTemplate_fn;
    type->interface11.fp_Add = (void(*)(uObject*, void*))::g::Fuse::Visual__Add1_fn;
    type->interface11.fp_Remove = (void(*)(uObject*, void*, bool*))::g::Fuse::Visual__Remove1_fn;
    type->interface5.fp_Clear = (void(*)(uObject*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingClear_fn;
    type->interface5.fp_Contains = (void(*)(uObject*, void*, bool*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingContains_fn;
    type->interface0.fp_RemoveAt = (void(*)(uObject*, int32_t*))::g::Fuse::Node__UnoCollectionsIListFuseBindingRemoveAt_fn;
    type->interface6.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Node__UnoCollectionsIEnumerableFuseBindingGetEnumerator_fn;
    type->interface1.fp_SetScriptObject = (void(*)(uObject*, uObject*, ::g::Fuse::Scripting::Context*))::g::Fuse::Node__FuseScriptingIScriptObjectSetScriptObject_fn;
    type->interface5.fp_get_Count = (void(*)(uObject*, int32_t*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingget_Count_fn;
    type->interface0.fp_get_Item = (void(*)(uObject*, int32_t*, uTRef))::g::Fuse::Node__UnoCollectionsIListFuseBindingget_Item_fn;
    type->interface1.fp_get_ScriptObject = (void(*)(uObject*, uObject**))::g::Fuse::Node__FuseScriptingIScriptObjectget_ScriptObject_fn;
    type->interface1.fp_get_ScriptContext = (void(*)(uObject*, ::g::Fuse::Scripting::Context**))::g::Fuse::Node__FuseScriptingIScriptObjectget_ScriptContext_fn;
    type->interface4.fp_get_SourceNearest = (void(*)(uObject*, uObject**))::g::Fuse::Node__FuseISourceLocationget_SourceNearest_fn;
    type->interface3.fp_add_Unrooted = (void(*)(uObject*, uDelegate*))::g::Fuse::Node__FuseINotifyUnrootedadd_Unrooted_fn;
    type->interface3.fp_remove_Unrooted = (void(*)(uObject*, uDelegate*))::g::Fuse::Node__FuseINotifyUnrootedremove_Unrooted_fn;
    type->interface0.fp_Insert = (void(*)(uObject*, int32_t*, void*))::g::Fuse::Node__Insert_fn;
    type->interface2.fp_get_Properties = (void(*)(uObject*, ::g::Fuse::Properties**))::g::Fuse::Node__get_Properties_fn;
    type->interface4.fp_get_SourceLineNumber = (void(*)(uObject*, int32_t*))::g::Fuse::Node__get_SourceLineNumber_fn;
    type->interface4.fp_get_SourceFileName = (void(*)(uObject*, uString**))::g::Fuse::Node__get_SourceFileName_fn;
    type->interface5.fp_Add = (void(*)(uObject*, void*))::g::Fuse::Node__Add_fn;
    type->interface5.fp_Remove = (void(*)(uObject*, void*, bool*))::g::Fuse::Node__Remove_fn;
    return type;
}

// public Sidebar() :22
void Sidebar__ctor_8_fn(Sidebar* __this)
{
    __this->ctor_8();
}

// private void InitializeUX() :26
void Sidebar__InitializeUX_fn(Sidebar* __this)
{
    __this->InitializeUX();
}

// public Sidebar New() :22
void Sidebar__New5_fn(Sidebar** __retval)
{
    *__retval = Sidebar::New5();
}

::g::Uno::UX::Selector Sidebar::__selector0_;
::g::Uno::UX::Selector Sidebar::__selector1_;
::g::Uno::UX::Selector Sidebar::__selector2_;
::g::Uno::UX::Selector Sidebar::__selector3_;
::g::Uno::UX::Selector Sidebar::__selector4_;
::g::Uno::UX::Selector Sidebar::__selector5_;
::g::Uno::UX::Selector Sidebar::__selector6_;
::g::Uno::UX::Selector Sidebar::__selector7_;

// public Sidebar() [instance] :22
void Sidebar::ctor_8()
{
    ctor_7();
    InitializeUX();
}

// private void InitializeUX() [instance] :26
void Sidebar::InitializeUX()
{
    ::g::Fuse::Drawing::ImageFill* temp = ::g::Fuse::Drawing::ImageFill::New2();
    temp_File_inst = ::g::OSP_FuseDrawingImageFill_File_Property::New1(temp, Sidebar::__selector0_);
    ::g::Fuse::Reactive::Data* temp2 = ::g::Fuse::Reactive::Data::New1(::STRINGS[8/*"image"*/]);
    ::g::Username* temp1 = ::g::Username::New4();
    temp1_Value_inst = ::g::OSP_FuseControlsTextControl_Value_Property::New1(temp1, Sidebar::__selector1_);
    ::g::Fuse::Reactive::Data* temp3 = ::g::Fuse::Reactive::Data::New1(::STRINGS[9/*"username"*/]);
    ::g::Fuse::Reactive::Data* temp4 = ::g::Fuse::Reactive::Data::New1(::STRINGS[10/*"profile"*/]);
    ::g::Fuse::Reactive::Data* temp5 = ::g::Fuse::Reactive::Data::New1(::STRINGS[11/*"logout"*/]);
    ::g::Fuse::Reactive::Data* temp6 = ::g::Fuse::Reactive::Data::New1(::STRINGS[12/*"workinfo"*/]);
    ::g::Fuse::Reactive::Data* temp7 = ::g::Fuse::Reactive::Data::New1(::STRINGS[5/*"notification"*/]);
    ::g::Fuse::Reactive::Data* temp8 = ::g::Fuse::Reactive::Data::New1(::STRINGS[6/*"history"*/]);
    ::g::Fuse::Reactive::Data* temp9 = ::g::Fuse::Reactive::Data::New1(::STRINGS[7/*"news"*/]);
    ::g::Fuse::Navigation::WhileInactive* temp10 = ::g::Fuse::Navigation::WhileInactive::New2();
    ::g::Fuse::Animations::Move* temp11 = ::g::Fuse::Animations::Move::New2();
    ::g::Fuse::Animations::Move* temp12 = ::g::Fuse::Animations::Move::New2();
    ::g::Fuse::Animations::Move* temp13 = ::g::Fuse::Animations::Move::New2();
    ::g::Fuse::Animations::Move* temp14 = ::g::Fuse::Animations::Move::New2();
    ::g::Fuse::Animations::Move* temp15 = ::g::Fuse::Animations::Move::New2();
    ::g::Fuse::Animations::Move* temp16 = ::g::Fuse::Animations::Move::New2();
    ::g::Fuse::Controls::StackPanel* temp17 = ::g::Fuse::Controls::StackPanel::New4();
    user = ::g::Fuse::Controls::StackPanel::New4();
    ::g::Fuse::Controls::Panel* temp18 = ::g::Fuse::Controls::Panel::New3();
    ::g::Fuse::Controls::Circle* temp19 = ::g::Fuse::Controls::Circle::New3();
    ::g::Fuse::Reactive::DataBinding* temp20 = ::g::Fuse::Reactive::DataBinding::New1(temp_File_inst, (uObject*)temp2, 3);
    ::g::Fuse::Reactive::DataBinding* temp21 = ::g::Fuse::Reactive::DataBinding::New1(temp1_Value_inst, (uObject*)temp3, 3);
    stats = ::g::Fuse::Controls::Grid::New4();
    ::g::Fuse::Controls::StackPanel* temp22 = ::g::Fuse::Controls::StackPanel::New4();
    ::g::Stats* temp23 = ::g::Stats::New4();
    temp_eb0 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp4);
    ::g::Fuse::Controls::Rectangle* temp24 = ::g::Fuse::Controls::Rectangle::New3();
    ::g::Fuse::Drawing::SolidColor* temp25 = ::g::Fuse::Drawing::SolidColor::New2();
    ::g::Fuse::Controls::StackPanel* temp26 = ::g::Fuse::Controls::StackPanel::New4();
    ::g::Stats* temp27 = ::g::Stats::New4();
    temp_eb1 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp5);
    ::g::Fuse::Controls::StackPanel* temp28 = ::g::Fuse::Controls::StackPanel::New4();
    work = ::g::Fuse::Controls::StackPanel::New4();
    ::g::SidebarIcon* temp29 = ::g::SidebarIcon::New4();
    ::g::MenuLabel* temp30 = ::g::MenuLabel::New4();
    temp_eb2 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp6);
    notification = ::g::Fuse::Controls::StackPanel::New4();
    ::g::Fuse::Controls::Panel* temp31 = ::g::Fuse::Controls::Panel::New3();
    ::g::SidebarIcon* temp32 = ::g::SidebarIcon::New4();
    ::g::Fuse::Controls::Circle* temp33 = ::g::Fuse::Controls::Circle::New3();
    ::g::Fuse::Controls::Text* temp34 = ::g::Fuse::Controls::Text::New3();
    ::g::MenuLabel* temp35 = ::g::MenuLabel::New4();
    temp_eb3 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp7);
    history = ::g::Fuse::Controls::StackPanel::New4();
    ::g::SidebarIcon* temp36 = ::g::SidebarIcon::New4();
    ::g::MenuLabel* temp37 = ::g::MenuLabel::New4();
    temp_eb4 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp8);
    news = ::g::Fuse::Controls::StackPanel::New4();
    ::g::SidebarIcon* temp38 = ::g::SidebarIcon::New4();
    ::g::MenuLabel* temp39 = ::g::MenuLabel::New4();
    temp_eb5 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp9);
    ::g::Fuse::Drawing::StaticSolidColor* temp40 = ::g::Fuse::Drawing::StaticSolidColor::New2(::g::Uno::Float4__New2(0.9686275f, 0.9686275f, 0.9686275f, 1.0f));
    SourceLineNumber(1);
    SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    temp10->Threshold(0.4f);
    temp10->SourceLineNumber(2);
    temp10->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp10->Animators()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Animations.Animator>*/]), temp11);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp10->Animators()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Animations.Animator>*/]), temp12);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp10->Animators()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Animations.Animator>*/]), temp13);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp10->Animators()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Animations.Animator>*/]), temp14);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp10->Animators()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Animations.Animator>*/]), temp15);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp10->Animators()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Animations.Animator>*/]), temp16);
    temp11->X(-180.0f);
    temp11->Duration(0.2);
    temp11->Delay(0.3);
    temp11->Target(user);
    temp11->Easing(::g::Fuse::Animations::Easing::CircularIn());
    temp12->X(-180.0f);
    temp12->Duration(0.2);
    temp12->Delay(0.2);
    temp12->Target(stats);
    temp12->Easing(::g::Fuse::Animations::Easing::CircularIn());
    temp13->X(-180.0f);
    temp13->Duration(0.2);
    temp13->Delay(0.15);
    temp13->Target(work);
    temp13->Easing(::g::Fuse::Animations::Easing::CircularIn());
    temp14->X(-180.0f);
    temp14->Duration(0.2);
    temp14->Delay(0.1);
    temp14->Target(notification);
    temp14->Easing(::g::Fuse::Animations::Easing::CircularIn());
    temp15->X(-180.0f);
    temp15->Duration(0.2);
    temp15->Delay(0.05);
    temp15->Target(history);
    temp15->Easing(::g::Fuse::Animations::Easing::CircularIn());
    temp16->X(-180.0f);
    temp16->Duration(0.2);
    temp16->Delay(0.05);
    temp16->Target(news);
    temp16->Easing(::g::Fuse::Animations::Easing::CircularIn());
    temp17->Alignment(9);
    temp17->Margin(::g::Uno::Float4__New2(15.0f, 0.0f, 0.0f, 0.0f));
    temp17->SourceLineNumber(22);
    temp17->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp17->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), user);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp17->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), stats);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp17->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp28);
    uPtr(user)->Name(Sidebar::__selector2_);
    uPtr(user)->SourceLineNumber(23);
    uPtr(user)->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(user)->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp18);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(user)->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp19);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(user)->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp1);
    temp18->Height(::g::Uno::UX::Size__New1(0.0f, 1));
    temp18->SourceLineNumber(24);
    temp18->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    temp19->HitTestMode(6);
    temp19->Width(::g::Uno::UX::Size__New1(85.0f, 1));
    temp19->Height(::g::Uno::UX::Size__New1(85.0f, 1));
    temp19->SourceLineNumber(25);
    temp19->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp19->Fills()), ::TYPES[2/*Uno.Collections.ICollection<Fuse.Drawing.Brush>*/]), temp);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp19->Bindings()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp20);
    temp->WrapMode(1);
    temp2->SourceLineNumber(26);
    temp2->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    temp1->SourceLineNumber(28);
    temp1->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp1->Bindings()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp21);
    temp3->SourceLineNumber(28);
    temp3->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    uPtr(stats)->ColumnCount(3);
    uPtr(stats)->Alignment(2);
    uPtr(stats)->Margin(::g::Uno::Float4__New2(0.0f, 10.0f, 0.0f, 15.0f));
    uPtr(stats)->Name(Sidebar::__selector3_);
    uPtr(stats)->SourceLineNumber(31);
    uPtr(stats)->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(stats)->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp22);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(stats)->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp24);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(stats)->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp26);
    temp22->HitTestMode(6);
    temp22->Width(::g::Uno::UX::Size__New1(70.0f, 1));
    temp22->SourceLineNumber(32);
    temp22->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    ::g::Fuse::Gestures::Clicked::AddHandler(temp22, uDelegate::New(::TYPES[4/*Fuse.Gestures.ClickedHandler*/], (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb0)));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp22->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp23);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp22->Bindings()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp_eb0);
    temp23->Value(::STRINGS[14/*"PROFILE"*/]);
    temp23->SourceLineNumber(33);
    temp23->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    temp4->SourceLineNumber(32);
    temp4->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    temp24->Width(::g::Uno::UX::Size__New1(1.0f, 1));
    temp24->Height(::g::Uno::UX::Size__New1(20.0f, 1));
    temp24->SourceLineNumber(36);
    temp24->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp24->Fills()), ::TYPES[2/*Uno.Collections.ICollection<Fuse.Drawing.Brush>*/]), temp25);
    temp25->Color(::g::Uno::Float4__New2(0.254902f, 0.3098039f, 0.3686275f, 1.0f));
    temp26->HitTestMode(6);
    temp26->Width(::g::Uno::UX::Size__New1(70.0f, 1));
    temp26->SourceLineNumber(39);
    temp26->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    ::g::Fuse::Gestures::Clicked::AddHandler(temp26, uDelegate::New(::TYPES[4/*Fuse.Gestures.ClickedHandler*/], (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb1)));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp26->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp27);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp26->Bindings()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp_eb1);
    temp27->Value(::STRINGS[15/*"LOG OUT"*/]);
    temp27->SourceLineNumber(40);
    temp27->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    temp5->SourceLineNumber(39);
    temp5->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    temp28->SourceLineNumber(47);
    temp28->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp28->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), work);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp28->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), notification);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp28->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), history);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp28->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), news);
    uPtr(work)->HitTestMode(6);
    uPtr(work)->Name(Sidebar::__selector4_);
    uPtr(work)->SourceLineNumber(48);
    uPtr(work)->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    ::g::Fuse::Gestures::Clicked::AddHandler(work, uDelegate::New(::TYPES[4/*Fuse.Gestures.ClickedHandler*/], (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb2)));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(work)->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp29);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(work)->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp30);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(work)->Bindings()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp_eb2);
    temp29->SourceLineNumber(49);
    temp29->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    temp29->File(::g::Uno::UX::BundleFileSource::New1(::g::OSP_bundle::Browser48e90536()));
    temp30->Value(::STRINGS[16/*"WORK INFO"*/]);
    temp30->SourceLineNumber(50);
    temp30->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    temp6->SourceLineNumber(48);
    temp6->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    uPtr(notification)->HitTestMode(6);
    uPtr(notification)->Name(Sidebar::__selector5_);
    uPtr(notification)->SourceLineNumber(52);
    uPtr(notification)->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    ::g::Fuse::Gestures::Clicked::AddHandler(notification, uDelegate::New(::TYPES[4/*Fuse.Gestures.ClickedHandler*/], (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb3)));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(notification)->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp31);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(notification)->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp35);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(notification)->Bindings()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp_eb3);
    temp31->SourceLineNumber(53);
    temp31->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp31->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp32);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp31->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp33);
    temp32->SourceLineNumber(54);
    temp32->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    temp32->File(::g::Uno::UX::BundleFileSource::New1(::g::OSP_bundle::Feeda3231344()));
    temp33->Color(::g::Uno::Float4__New2(0.8f, 0.2f, 0.2f, 1.0f));
    temp33->Width(::g::Uno::UX::Size__New1(17.0f, 1));
    temp33->Height(::g::Uno::UX::Size__New1(17.0f, 1));
    temp33->Offset(::g::Uno::UX::Size2__New1(::g::Uno::UX::Size__New1(10.0f, 1), ::g::Uno::UX::Size__New1(-5.0f, 1)));
    temp33->ZOffset(1.0f);
    temp33->SourceLineNumber(55);
    temp33->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp33->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp34);
    temp34->Value(::STRINGS[17/*"3"*/]);
    temp34->FontSize(11.0f);
    temp34->TextAlignment(1);
    temp34->TextColor(::g::Uno::Float4__New2(1.0f, 1.0f, 1.0f, 1.0f));
    temp34->Alignment(10);
    temp34->SourceLineNumber(56);
    temp34->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    temp35->Value(::STRINGS[18/*"NOTIFICATIONS"*/]);
    temp35->Alignment(8);
    temp35->SourceLineNumber(59);
    temp35->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    temp7->SourceLineNumber(52);
    temp7->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    uPtr(history)->HitTestMode(6);
    uPtr(history)->Name(Sidebar::__selector6_);
    uPtr(history)->SourceLineNumber(61);
    uPtr(history)->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    ::g::Fuse::Gestures::Clicked::AddHandler(history, uDelegate::New(::TYPES[4/*Fuse.Gestures.ClickedHandler*/], (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb4)));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(history)->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp36);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(history)->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp37);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(history)->Bindings()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp_eb4);
    temp36->SourceLineNumber(62);
    temp36->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    temp36->File(::g::Uno::UX::BundleFileSource::New1(::g::OSP_bundle::historya6ead22a()));
    temp37->Value(::STRINGS[19/*"TRIPS"*/]);
    temp37->SourceLineNumber(63);
    temp37->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    temp8->SourceLineNumber(61);
    temp8->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    uPtr(news)->HitTestMode(6);
    uPtr(news)->Name(Sidebar::__selector7_);
    uPtr(news)->SourceLineNumber(69);
    uPtr(news)->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    ::g::Fuse::Gestures::Clicked::AddHandler(news, uDelegate::New(::TYPES[4/*Fuse.Gestures.ClickedHandler*/], (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb5)));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(news)->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp38);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(news)->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp39);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(news)->Bindings()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp_eb5);
    temp38->SourceLineNumber(70);
    temp38->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    temp38->File(::g::Uno::UX::BundleFileSource::New1(::g::OSP_bundle::Chat187ba3a6()));
    temp39->Value(::STRINGS[20/*"NEWS"*/]);
    temp39->SourceLineNumber(71);
    temp39->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    temp9->SourceLineNumber(69);
    temp9->SourceFileName(::STRINGS[13/*"Sidebar.ux"*/]);
    Background(temp40);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp10);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp17);
}

// public Sidebar New() [static] :22
Sidebar* Sidebar::New5()
{
    Sidebar* obj1 = (Sidebar*)uNew(Sidebar_typeof());
    obj1->ctor_8();
    return obj1;
}
// }

} // ::g
