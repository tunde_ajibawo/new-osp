// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/SubButton.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.MainView.h>
#include <_root.OSP_accessor_Su-56996969.h>
#include <_root.OSP_accessor_Su-debd1484.h>
#include <_root.OSP_bundle.h>
#include <_root.OSP_FuseControl-8faf8b28.h>
#include <_root.OSP_FuseControl-b26a5bbf.h>
#include <_root.OSP_FuseElement-78dfea6e.h>
#include <_root.OSP_FuseTrigger-1375289b.h>
#include <_root.SubButton.h>
#include <Fuse.Animations.Animator.h>
#include <Fuse.Animations.Change-1.h>
#include <Fuse.Animations.Spin.h>
#include <Fuse.Controls.Image.h>
#include <Fuse.Controls.Shape.h>
#include <Fuse.Controls.Text.h>
#include <Fuse.Controls.TextAlignment.h>
#include <Fuse.Controls.TextControl.h>
#include <Fuse.Elements.Alignment.h>
#include <Fuse.Elements.Element.h>
#include <Fuse.Font.h>
#include <Fuse.Reactive.BindingMode.h>
#include <Fuse.Reactive.Constan-264ec80.h>
#include <Fuse.Reactive.Constant.h>
#include <Fuse.Reactive.Data.h>
#include <Fuse.Reactive.DataBinding.h>
#include <Fuse.Reactive.Expression.h>
#include <Fuse.Reactive.IExpression.h>
#include <Fuse.Reactive.Property.h>
#include <Fuse.Triggers.Trigger.h>
#include <Fuse.Triggers.WhileBool.h>
#include <Fuse.Triggers.WhileFalse.h>
#include <Fuse.Triggers.WhileTrue.h>
#include <Uno.Bool.h>
#include <Uno.Double.h>
#include <Uno.Float.h>
#include <Uno.Int.h>
#include <Uno.IO.BundleFile.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.UX.BundleFileSource.h>
#include <Uno.UX.FileSource.h>
#include <Uno.UX.Property.h>
#include <Uno.UX.Property1-1.h>
#include <Uno.UX.PropertyAccessor.h>
#include <Uno.UX.PropertyObject.h>
#include <Uno.UX.Selector.h>
#include <Uno.UX.Size.h>
#include <Uno.UX.Unit.h>
static uString* STRINGS[9];
static uType* TYPES[4];

namespace g{

// public partial sealed class SubButton :2
// {
// static SubButton() :42
static void SubButton__cctor_5_fn(uType* __type)
{
    SubButton::__selector0_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[0/*"Value"*/]);
    SubButton::__selector1_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[1/*"TextColor"*/]);
    SubButton::__selector2_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[2/*"Opacity"*/]);
    SubButton::__selector3_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[3/*"sub"*/]);
    SubButton::__selector4_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[4/*"loadn"*/]);
}

static void SubButton_build(uType* type)
{
    ::STRINGS[0] = uString::Const("Value");
    ::STRINGS[1] = uString::Const("TextColor");
    ::STRINGS[2] = uString::Const("Opacity");
    ::STRINGS[3] = uString::Const("sub");
    ::STRINGS[4] = uString::Const("loadn");
    ::STRINGS[5] = uString::Const("visible");
    ::STRINGS[6] = uString::Const("homepage.ux");
    ::STRINGS[7] = uString::Const("ButtonName");
    ::STRINGS[8] = uString::Const("ButtonTextColor");
    ::TYPES[0] = ::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL);
    ::TYPES[1] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL);
    ::TYPES[2] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL);
    ::TYPES[3] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL);
    type->SetDependencies(
        ::g::MainView_typeof(),
        ::g::OSP_accessor_SubButton_ButtonName_typeof(),
        ::g::OSP_accessor_SubButton_ButtonTextColor_typeof(),
        ::g::OSP_bundle_typeof());
    type->SetInterfaces(
        ::g::Uno::Collections::IList_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Shape_type, interface0),
        ::g::Fuse::Scripting::IScriptObject_typeof(), offsetof(::g::Fuse::Controls::Shape_type, interface1),
        ::g::Fuse::IProperties_typeof(), offsetof(::g::Fuse::Controls::Shape_type, interface2),
        ::g::Fuse::INotifyUnrooted_typeof(), offsetof(::g::Fuse::Controls::Shape_type, interface3),
        ::g::Fuse::ISourceLocation_typeof(), offsetof(::g::Fuse::Controls::Shape_type, interface4),
        ::TYPES[1/*Uno.Collections.ICollection<Fuse.Binding>*/], offsetof(::g::Fuse::Controls::Shape_type, interface5),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Shape_type, interface6),
        ::g::Uno::Collections::IList_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Shape_type, interface7),
        ::g::Uno::UX::IPropertyListener_typeof(), offsetof(::g::Fuse::Controls::Shape_type, interface8),
        ::g::Fuse::ITemplateSource_typeof(), offsetof(::g::Fuse::Controls::Shape_type, interface9),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Visual_typeof(), NULL), offsetof(::g::Fuse::Controls::Shape_type, interface10),
        ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/], offsetof(::g::Fuse::Controls::Shape_type, interface11),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Shape_type, interface12),
        ::g::Fuse::Triggers::Actions::IShow_typeof(), offsetof(::g::Fuse::Controls::Shape_type, interface13),
        ::g::Fuse::Triggers::Actions::IHide_typeof(), offsetof(::g::Fuse::Controls::Shape_type, interface14),
        ::g::Fuse::Triggers::Actions::ICollapse_typeof(), offsetof(::g::Fuse::Controls::Shape_type, interface15),
        ::g::Fuse::IActualPlacement_typeof(), offsetof(::g::Fuse::Controls::Shape_type, interface16),
        ::g::Fuse::Animations::IResize_typeof(), offsetof(::g::Fuse::Controls::Shape_type, interface17),
        ::g::Fuse::Drawing::ISurfaceDrawable_typeof(), offsetof(::g::Fuse::Controls::Shape_type, interface18),
        ::g::Fuse::Drawing::IDrawObjectWatcherFeedback_typeof(), offsetof(::g::Fuse::Controls::Shape_type, interface19));
    type->SetFields(117,
        ::g::Uno::String_typeof(), offsetof(SubButton, _field_ButtonName), 0,
        ::g::Uno::Float4_typeof(), offsetof(SubButton, _field_ButtonTextColor), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(SubButton, sub_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL), offsetof(SubButton, sub_TextColor_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(SubButton, sub_Opacity_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(SubButton, loadn_Opacity_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Bool_typeof(), NULL), offsetof(SubButton, temp_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Bool_typeof(), NULL), offsetof(SubButton, temp1_Value_inst), 0,
        ::g::Fuse::Controls::Text_typeof(), offsetof(SubButton, sub), 0,
        ::g::Fuse::Controls::Image_typeof(), offsetof(SubButton, loadn), 0,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&SubButton::__selector0_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&SubButton::__selector1_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&SubButton::__selector2_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&SubButton::__selector3_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&SubButton::__selector4_, uFieldFlagsStatic);
}

::g::Fuse::Controls::Shape_type* SubButton_typeof()
{
    static uSStrong< ::g::Fuse::Controls::Shape_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Fuse::Controls::Rectangle_typeof();
    options.FieldCount = 132;
    options.InterfaceCount = 20;
    options.DependencyCount = 4;
    options.ObjectSize = sizeof(SubButton);
    options.TypeSize = sizeof(::g::Fuse::Controls::Shape_type);
    type = (::g::Fuse::Controls::Shape_type*)uClassType::New("SubButton", options);
    type->fp_build_ = SubButton_build;
    type->fp_ctor_ = (void*)SubButton__New4_fn;
    type->fp_cctor_ = SubButton__cctor_5_fn;
    type->interface19.fp_Changed = (void(*)(uObject*, uObject*))::g::Fuse::Controls::Shape__FuseDrawingIDrawObjectWatcherFeedbackChanged_fn;
    type->interface19.fp_Prepare = (void(*)(uObject*, uObject*))::g::Fuse::Controls::Shape__FuseDrawingIDrawObjectWatcherFeedbackPrepare_fn;
    type->interface19.fp_Unprepare = (void(*)(uObject*, uObject*))::g::Fuse::Controls::Shape__FuseDrawingIDrawObjectWatcherFeedbackUnprepare_fn;
    type->interface18.fp_Draw = (void(*)(uObject*, ::g::Fuse::Drawing::Surface*))::g::Fuse::Controls::Shape__FuseDrawingISurfaceDrawableDraw_fn;
    type->interface18.fp_get_IsPrimary = (void(*)(uObject*, bool*))::g::Fuse::Controls::Shape__FuseDrawingISurfaceDrawableget_IsPrimary_fn;
    type->interface18.fp_get_ElementSize = (void(*)(uObject*, ::g::Uno::Float2*))::g::Fuse::Controls::Shape__FuseDrawingISurfaceDrawableget_ElementSize_fn;
    type->interface8.fp_OnPropertyChanged = (void(*)(uObject*, ::g::Uno::UX::PropertyObject*, ::g::Uno::UX::Selector*))::g::Fuse::Controls::Shape__OnPropertyChanged2_fn;
    type->interface13.fp_Show = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsIShowShow_fn;
    type->interface15.fp_Collapse = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsICollapseCollapse_fn;
    type->interface14.fp_Hide = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsIHideHide_fn;
    type->interface17.fp_SetSize = (void(*)(uObject*, ::g::Uno::Float2*))::g::Fuse::Elements::Element__FuseAnimationsIResizeSetSize_fn;
    type->interface16.fp_get_ActualSize = (void(*)(uObject*, ::g::Uno::Float3*))::g::Fuse::Elements::Element__FuseIActualPlacementget_ActualSize_fn;
    type->interface16.fp_add_Placed = (void(*)(uObject*, uDelegate*))::g::Fuse::Elements::Element__add_Placed_fn;
    type->interface16.fp_remove_Placed = (void(*)(uObject*, uDelegate*))::g::Fuse::Elements::Element__remove_Placed_fn;
    type->interface10.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Visual__UnoCollectionsIEnumerableFuseVisualGetEnumerator_fn;
    type->interface11.fp_Clear = (void(*)(uObject*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeClear_fn;
    type->interface11.fp_Contains = (void(*)(uObject*, void*, bool*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeContains_fn;
    type->interface7.fp_RemoveAt = (void(*)(uObject*, int32_t*))::g::Fuse::Visual__UnoCollectionsIListFuseNodeRemoveAt_fn;
    type->interface12.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Visual__UnoCollectionsIEnumerableFuseNodeGetEnumerator_fn;
    type->interface11.fp_get_Count = (void(*)(uObject*, int32_t*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeget_Count_fn;
    type->interface7.fp_get_Item = (void(*)(uObject*, int32_t*, uTRef))::g::Fuse::Visual__UnoCollectionsIListFuseNodeget_Item_fn;
    type->interface7.fp_Insert = (void(*)(uObject*, int32_t*, void*))::g::Fuse::Visual__Insert1_fn;
    type->interface9.fp_FindTemplate = (void(*)(uObject*, uString*, ::g::Uno::UX::Template**))::g::Fuse::Visual__FindTemplate_fn;
    type->interface11.fp_Add = (void(*)(uObject*, void*))::g::Fuse::Visual__Add1_fn;
    type->interface11.fp_Remove = (void(*)(uObject*, void*, bool*))::g::Fuse::Visual__Remove1_fn;
    type->interface5.fp_Clear = (void(*)(uObject*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingClear_fn;
    type->interface5.fp_Contains = (void(*)(uObject*, void*, bool*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingContains_fn;
    type->interface0.fp_RemoveAt = (void(*)(uObject*, int32_t*))::g::Fuse::Node__UnoCollectionsIListFuseBindingRemoveAt_fn;
    type->interface6.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Node__UnoCollectionsIEnumerableFuseBindingGetEnumerator_fn;
    type->interface1.fp_SetScriptObject = (void(*)(uObject*, uObject*, ::g::Fuse::Scripting::Context*))::g::Fuse::Node__FuseScriptingIScriptObjectSetScriptObject_fn;
    type->interface5.fp_get_Count = (void(*)(uObject*, int32_t*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingget_Count_fn;
    type->interface0.fp_get_Item = (void(*)(uObject*, int32_t*, uTRef))::g::Fuse::Node__UnoCollectionsIListFuseBindingget_Item_fn;
    type->interface1.fp_get_ScriptObject = (void(*)(uObject*, uObject**))::g::Fuse::Node__FuseScriptingIScriptObjectget_ScriptObject_fn;
    type->interface1.fp_get_ScriptContext = (void(*)(uObject*, ::g::Fuse::Scripting::Context**))::g::Fuse::Node__FuseScriptingIScriptObjectget_ScriptContext_fn;
    type->interface4.fp_get_SourceNearest = (void(*)(uObject*, uObject**))::g::Fuse::Node__FuseISourceLocationget_SourceNearest_fn;
    type->interface3.fp_add_Unrooted = (void(*)(uObject*, uDelegate*))::g::Fuse::Node__FuseINotifyUnrootedadd_Unrooted_fn;
    type->interface3.fp_remove_Unrooted = (void(*)(uObject*, uDelegate*))::g::Fuse::Node__FuseINotifyUnrootedremove_Unrooted_fn;
    type->interface0.fp_Insert = (void(*)(uObject*, int32_t*, void*))::g::Fuse::Node__Insert_fn;
    type->interface2.fp_get_Properties = (void(*)(uObject*, ::g::Fuse::Properties**))::g::Fuse::Node__get_Properties_fn;
    type->interface4.fp_get_SourceLineNumber = (void(*)(uObject*, int32_t*))::g::Fuse::Node__get_SourceLineNumber_fn;
    type->interface4.fp_get_SourceFileName = (void(*)(uObject*, uString**))::g::Fuse::Node__get_SourceFileName_fn;
    type->interface5.fp_Add = (void(*)(uObject*, void*))::g::Fuse::Node__Add_fn;
    type->interface5.fp_Remove = (void(*)(uObject*, void*, bool*))::g::Fuse::Node__Remove_fn;
    return type;
}

// public SubButton() :46
void SubButton__ctor_8_fn(SubButton* __this)
{
    __this->ctor_8();
}

// public string get_ButtonName() :8
void SubButton__get_ButtonName_fn(SubButton* __this, uString** __retval)
{
    *__retval = __this->ButtonName();
}

// public void set_ButtonName(string value) :9
void SubButton__set_ButtonName_fn(SubButton* __this, uString* value)
{
    __this->ButtonName(value);
}

// public float4 get_ButtonTextColor() :23
void SubButton__get_ButtonTextColor_fn(SubButton* __this, ::g::Uno::Float4* __retval)
{
    *__retval = __this->ButtonTextColor();
}

// public void set_ButtonTextColor(float4 value) :24
void SubButton__set_ButtonTextColor_fn(SubButton* __this, ::g::Uno::Float4* value)
{
    __this->ButtonTextColor(*value);
}

// private void InitializeUX() :50
void SubButton__InitializeUX_fn(SubButton* __this)
{
    __this->InitializeUX();
}

// public SubButton New() :46
void SubButton__New4_fn(SubButton** __retval)
{
    *__retval = SubButton::New4();
}

// public void SetButtonName(string value, Uno.UX.IPropertyListener origin) :11
void SubButton__SetButtonName_fn(SubButton* __this, uString* value, uObject* origin)
{
    __this->SetButtonName(value, origin);
}

// public void SetButtonTextColor(float4 value, Uno.UX.IPropertyListener origin) :26
void SubButton__SetButtonTextColor_fn(SubButton* __this, ::g::Uno::Float4* value, uObject* origin)
{
    __this->SetButtonTextColor(*value, origin);
}

::g::Uno::UX::Selector SubButton::__selector0_;
::g::Uno::UX::Selector SubButton::__selector1_;
::g::Uno::UX::Selector SubButton::__selector2_;
::g::Uno::UX::Selector SubButton::__selector3_;
::g::Uno::UX::Selector SubButton::__selector4_;

// public SubButton() [instance] :46
void SubButton::ctor_8()
{
    ctor_7();
    InitializeUX();
}

// public string get_ButtonName() [instance] :8
uString* SubButton::ButtonName()
{
    return _field_ButtonName;
}

// public void set_ButtonName(string value) [instance] :9
void SubButton::ButtonName(uString* value)
{
    SetButtonName(value, NULL);
}

// public float4 get_ButtonTextColor() [instance] :23
::g::Uno::Float4 SubButton::ButtonTextColor()
{
    return _field_ButtonTextColor;
}

// public void set_ButtonTextColor(float4 value) [instance] :24
void SubButton::ButtonTextColor(::g::Uno::Float4 value)
{
    SetButtonTextColor(value, NULL);
}

// private void InitializeUX() [instance] :50
void SubButton::InitializeUX()
{
    ::g::Fuse::Reactive::Constant* temp2 = ::g::Fuse::Reactive::Constant::New1(this);
    sub = ::g::Fuse::Controls::Text::New3();
    sub_Value_inst = ::g::OSP_FuseControlsTextControl_Value_Property::New1(sub, SubButton::__selector0_);
    ::g::Fuse::Reactive::Property* temp3 = ::g::Fuse::Reactive::Property::New1(temp2, ::g::OSP_accessor_SubButton_ButtonName::Singleton());
    ::g::Fuse::Reactive::Constant* temp4 = ::g::Fuse::Reactive::Constant::New1(this);
    sub_TextColor_inst = ::g::OSP_FuseControlsTextControl_TextColor_Property::New1(sub, SubButton::__selector1_);
    ::g::Fuse::Reactive::Property* temp5 = ::g::Fuse::Reactive::Property::New1(temp4, ::g::OSP_accessor_SubButton_ButtonTextColor::Singleton());
    sub_Opacity_inst = ::g::OSP_FuseElementsElement_Opacity_Property::New1(sub, SubButton::__selector2_);
    loadn = ::g::Fuse::Controls::Image::New3();
    loadn_Opacity_inst = ::g::OSP_FuseElementsElement_Opacity_Property::New1(loadn, SubButton::__selector2_);
    ::g::Fuse::Triggers::WhileTrue* temp = ::g::Fuse::Triggers::WhileTrue::New2();
    temp_Value_inst = ::g::OSP_FuseTriggersWhileBool_Value_Property::New1(temp, SubButton::__selector0_);
    ::g::Fuse::Reactive::Data* temp6 = ::g::Fuse::Reactive::Data::New1(::STRINGS[5/*"visible"*/]);
    ::g::Fuse::Triggers::WhileFalse* temp1 = ::g::Fuse::Triggers::WhileFalse::New2();
    temp1_Value_inst = ::g::OSP_FuseTriggersWhileBool_Value_Property::New1(temp1, SubButton::__selector0_);
    ::g::Fuse::Reactive::Data* temp7 = ::g::Fuse::Reactive::Data::New1(::STRINGS[5/*"visible"*/]);
    ::g::Fuse::Reactive::DataBinding* temp8 = ::g::Fuse::Reactive::DataBinding::New1(sub_Value_inst, (uObject*)temp3, 3);
    ::g::Fuse::Reactive::DataBinding* temp9 = ::g::Fuse::Reactive::DataBinding::New1(sub_TextColor_inst, (uObject*)temp5, 3);
    ::g::Fuse::Animations::Change* temp10 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::TYPES[0/*Fuse.Animations.Change<float>*/], sub_Opacity_inst);
    ::g::Fuse::Animations::Spin* temp11 = ::g::Fuse::Animations::Spin::New2();
    ::g::Fuse::Animations::Change* temp12 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::TYPES[0/*Fuse.Animations.Change<float>*/], loadn_Opacity_inst);
    ::g::Fuse::Reactive::DataBinding* temp13 = ::g::Fuse::Reactive::DataBinding::New1(temp_Value_inst, (uObject*)temp6, 3);
    ::g::Fuse::Animations::Change* temp14 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::TYPES[0/*Fuse.Animations.Change<float>*/], sub_Opacity_inst);
    ::g::Fuse::Animations::Change* temp15 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::TYPES[0/*Fuse.Animations.Change<float>*/], loadn_Opacity_inst);
    ::g::Fuse::Reactive::DataBinding* temp16 = ::g::Fuse::Reactive::DataBinding::New1(temp1_Value_inst, (uObject*)temp7, 3);
    ButtonTextColor(::g::Uno::Float4__New2(1.0f, 1.0f, 1.0f, 1.0f));
    Color(::g::Uno::Float4__New2(0.1137255f, 0.4901961f, 0.2588235f, 1.0f));
    Width(::g::Uno::UX::Size__New1(80.0f, 4));
    Margin(::g::Uno::Float4__New2(0.0f, 5.0f, 0.0f, 5.0f));
    Padding(::g::Uno::Float4__New2(5.0f, 5.0f, 5.0f, 5.0f));
    SourceLineNumber(15);
    SourceFileName(::STRINGS[6/*"homepage.ux"*/]);
    uPtr(sub)->FontSize(15.0f);
    uPtr(sub)->TextAlignment(1);
    uPtr(sub)->Alignment(10);
    uPtr(sub)->Opacity(1.0f);
    uPtr(sub)->Name(SubButton::__selector3_);
    uPtr(sub)->SourceLineNumber(18);
    uPtr(sub)->SourceFileName(::STRINGS[6/*"homepage.ux"*/]);
    uPtr(sub)->Font(::g::MainView::Raleway());
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(sub)->Bindings()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp8);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(sub)->Bindings()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp9);
    temp3->SourceLineNumber(18);
    temp3->SourceFileName(::STRINGS[6/*"homepage.ux"*/]);
    temp2->SourceLineNumber(18);
    temp2->SourceFileName(::STRINGS[6/*"homepage.ux"*/]);
    temp5->SourceLineNumber(18);
    temp5->SourceFileName(::STRINGS[6/*"homepage.ux"*/]);
    temp4->SourceLineNumber(18);
    temp4->SourceFileName(::STRINGS[6/*"homepage.ux"*/]);
    uPtr(loadn)->Width(::g::Uno::UX::Size__New1(30.0f, 1));
    uPtr(loadn)->Opacity(0.0f);
    uPtr(loadn)->Name(SubButton::__selector4_);
    uPtr(loadn)->SourceLineNumber(19);
    uPtr(loadn)->SourceFileName(::STRINGS[6/*"homepage.ux"*/]);
    uPtr(loadn)->File(::g::Uno::UX::BundleFileSource::New1(::g::OSP_bundle::diskc6e80153()));
    temp->SourceLineNumber(23);
    temp->SourceFileName(::STRINGS[6/*"homepage.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Animators()), ::TYPES[2/*Uno.Collections.ICollection<Fuse.Animations.Animator>*/]), temp10);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Animators()), ::TYPES[2/*Uno.Collections.ICollection<Fuse.Animations.Animator>*/]), temp11);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Animators()), ::TYPES[2/*Uno.Collections.ICollection<Fuse.Animations.Animator>*/]), temp12);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Bindings()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp13);
    ::g::Fuse::Animations::Change__set_Value_fn(temp10, uCRef(0.0f));
    temp11->Frequency(1.0);
    temp11->Target(loadn);
    ::g::Fuse::Animations::Change__set_Value_fn(temp12, uCRef(1.0f));
    temp6->SourceLineNumber(23);
    temp6->SourceFileName(::STRINGS[6/*"homepage.ux"*/]);
    temp1->SourceLineNumber(29);
    temp1->SourceFileName(::STRINGS[6/*"homepage.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp1->Animators()), ::TYPES[2/*Uno.Collections.ICollection<Fuse.Animations.Animator>*/]), temp14);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp1->Animators()), ::TYPES[2/*Uno.Collections.ICollection<Fuse.Animations.Animator>*/]), temp15);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp1->Bindings()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp16);
    ::g::Fuse::Animations::Change__set_Value_fn(temp14, uCRef(1.0f));
    ::g::Fuse::Animations::Change__set_Value_fn(temp15, uCRef(0.0f));
    temp7->SourceLineNumber(29);
    temp7->SourceFileName(::STRINGS[6/*"homepage.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), sub);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), loadn);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), temp);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), temp1);
}

// public void SetButtonName(string value, Uno.UX.IPropertyListener origin) [instance] :11
void SubButton::SetButtonName(uString* value, uObject* origin)
{
    if (::g::Uno::String::op_Inequality(value, _field_ButtonName))
    {
        _field_ButtonName = value;
        OnPropertyChanged1(::g::Uno::UX::Selector__op_Implicit1(::STRINGS[7/*"ButtonName"*/]), origin);
    }
}

// public void SetButtonTextColor(float4 value, Uno.UX.IPropertyListener origin) [instance] :26
void SubButton::SetButtonTextColor(::g::Uno::Float4 value, uObject* origin)
{
    if (::g::Uno::Float4__op_Inequality(value, _field_ButtonTextColor))
    {
        _field_ButtonTextColor = value;
        OnPropertyChanged1(::g::Uno::UX::Selector__op_Implicit1(::STRINGS[8/*"ButtonTextC...*/]), origin);
    }
}

// public SubButton New() [static] :46
SubButton* SubButton::New4()
{
    SubButton* obj1 = (SubButton*)uNew(SubButton_typeof());
    obj1->ctor_8();
    return obj1;
}
// }

} // ::g
