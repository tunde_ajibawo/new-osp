// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/SubBut.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.MainView.h>
#include <_root.OSP_accessor_Su-14fa46a1.h>
#include <_root.OSP_accessor_Su-20033de.h>
#include <_root.OSP_accessor_Su-54859943.h>
#include <_root.OSP_accessor_SubBut_Icon.h>
#include <_root.OSP_FuseControl-8faf8b28.h>
#include <_root.OSP_FuseControl-b26a5bbf.h>
#include <_root.OSP_FuseElement-652603a7.h>
#include <_root.SubBut.h>
#include <Fuse.Controls.DockPanel.h>
#include <Fuse.Controls.Rectangle.h>
#include <Fuse.Controls.Shape.h>
#include <Fuse.Controls.Text.h>
#include <Fuse.Controls.TextAlignment.h>
#include <Fuse.Controls.TextControl.h>
#include <Fuse.Elements.Alignment.h>
#include <Fuse.Elements.Element.h>
#include <Fuse.Font.h>
#include <Fuse.Layer.h>
#include <Fuse.Layouts.Dock.h>
#include <Fuse.Reactive.BindingMode.h>
#include <Fuse.Reactive.Constan-264ec80.h>
#include <Fuse.Reactive.Constant.h>
#include <Fuse.Reactive.DataBinding.h>
#include <Fuse.Reactive.Expression.h>
#include <Fuse.Reactive.IExpression.h>
#include <Fuse.Reactive.Property.h>
#include <Uno.Bool.h>
#include <Uno.Float.h>
#include <Uno.Int.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.UX.Property.h>
#include <Uno.UX.Property1-1.h>
#include <Uno.UX.PropertyAccessor.h>
#include <Uno.UX.PropertyObject.h>
#include <Uno.UX.Selector.h>
static uString* STRINGS[8];
static uType* TYPES[2];

namespace g{

// public partial sealed class SubBut :2
// {
// static SubBut() :68
static void SubBut__cctor_5_fn(uType* __type)
{
    SubBut::__selector0_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[0/*"Value"*/]);
    SubBut::__selector1_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[1/*"TextColor"*/]);
    SubBut::__selector2_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[2/*"DockPanel.D...*/]);
}

static void SubBut_build(uType* type)
{
    ::STRINGS[0] = uString::Const("Value");
    ::STRINGS[1] = uString::Const("TextColor");
    ::STRINGS[2] = uString::Const("DockPanel.Dock");
    ::STRINGS[3] = uString::Const("profile.ux");
    ::STRINGS[4] = uString::Const("Btext");
    ::STRINGS[5] = uString::Const("Direction");
    ::STRINGS[6] = uString::Const("Icon");
    ::STRINGS[7] = uString::Const("IconColor");
    ::TYPES[0] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL);
    ::TYPES[1] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL);
    type->SetDependencies(
        ::g::MainView_typeof(),
        ::g::OSP_accessor_SubBut_Btext_typeof(),
        ::g::OSP_accessor_SubBut_Direction_typeof(),
        ::g::OSP_accessor_SubBut_Icon_typeof(),
        ::g::OSP_accessor_SubBut_IconColor_typeof());
    type->SetInterfaces(
        ::g::Uno::Collections::IList_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface0),
        ::g::Fuse::Scripting::IScriptObject_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface1),
        ::g::Fuse::IProperties_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface2),
        ::g::Fuse::INotifyUnrooted_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface3),
        ::g::Fuse::ISourceLocation_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface4),
        ::TYPES[1/*Uno.Collections.ICollection<Fuse.Binding>*/], offsetof(::g::Fuse::Controls::Panel_type, interface5),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface6),
        ::g::Uno::Collections::IList_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface7),
        ::g::Uno::UX::IPropertyListener_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface8),
        ::g::Fuse::ITemplateSource_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface9),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Visual_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface10),
        ::TYPES[0/*Uno.Collections.ICollection<Fuse.Node>*/], offsetof(::g::Fuse::Controls::Panel_type, interface11),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface12),
        ::g::Fuse::Triggers::Actions::IShow_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface13),
        ::g::Fuse::Triggers::Actions::IHide_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface14),
        ::g::Fuse::Triggers::Actions::ICollapse_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface15),
        ::g::Fuse::IActualPlacement_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface16),
        ::g::Fuse::Animations::IResize_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface17),
        ::g::Fuse::Drawing::ISurfaceDrawable_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface18));
    type->SetFields(116,
        ::g::Uno::String_typeof(), offsetof(SubBut, _field_Btext), 0,
        ::g::Uno::String_typeof(), offsetof(SubBut, _field_Icon), 0,
        ::g::Uno::String_typeof(), offsetof(SubBut, _field_Direction), 0,
        ::g::Uno::Float4_typeof(), offsetof(SubBut, _field_IconColor), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(SubBut, temp_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL), offsetof(SubBut, temp_TextColor_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(SubBut, temp1_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Fuse::Layouts::Dock_typeof(), NULL), offsetof(SubBut, this_DockPanel_Dock_inst), 0,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&SubBut::__selector0_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&SubBut::__selector1_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&SubBut::__selector2_, uFieldFlagsStatic);
}

::g::Fuse::Controls::Panel_type* SubBut_typeof()
{
    static uSStrong< ::g::Fuse::Controls::Panel_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Fuse::Controls::Button_typeof();
    options.FieldCount = 127;
    options.InterfaceCount = 19;
    options.DependencyCount = 5;
    options.ObjectSize = sizeof(SubBut);
    options.TypeSize = sizeof(::g::Fuse::Controls::Panel_type);
    type = (::g::Fuse::Controls::Panel_type*)uClassType::New("SubBut", options);
    type->fp_build_ = SubBut_build;
    type->fp_ctor_ = (void*)SubBut__New6_fn;
    type->fp_cctor_ = SubBut__cctor_5_fn;
    type->interface18.fp_Draw = (void(*)(uObject*, ::g::Fuse::Drawing::Surface*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableDraw_fn;
    type->interface18.fp_get_IsPrimary = (void(*)(uObject*, bool*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableget_IsPrimary_fn;
    type->interface18.fp_get_ElementSize = (void(*)(uObject*, ::g::Uno::Float2*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableget_ElementSize_fn;
    type->interface13.fp_Show = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsIShowShow_fn;
    type->interface15.fp_Collapse = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsICollapseCollapse_fn;
    type->interface14.fp_Hide = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsIHideHide_fn;
    type->interface17.fp_SetSize = (void(*)(uObject*, ::g::Uno::Float2*))::g::Fuse::Elements::Element__FuseAnimationsIResizeSetSize_fn;
    type->interface16.fp_get_ActualSize = (void(*)(uObject*, ::g::Uno::Float3*))::g::Fuse::Elements::Element__FuseIActualPlacementget_ActualSize_fn;
    type->interface16.fp_add_Placed = (void(*)(uObject*, uDelegate*))::g::Fuse::Elements::Element__add_Placed_fn;
    type->interface16.fp_remove_Placed = (void(*)(uObject*, uDelegate*))::g::Fuse::Elements::Element__remove_Placed_fn;
    type->interface10.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Visual__UnoCollectionsIEnumerableFuseVisualGetEnumerator_fn;
    type->interface11.fp_Clear = (void(*)(uObject*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeClear_fn;
    type->interface11.fp_Contains = (void(*)(uObject*, void*, bool*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeContains_fn;
    type->interface7.fp_RemoveAt = (void(*)(uObject*, int32_t*))::g::Fuse::Visual__UnoCollectionsIListFuseNodeRemoveAt_fn;
    type->interface12.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Visual__UnoCollectionsIEnumerableFuseNodeGetEnumerator_fn;
    type->interface11.fp_get_Count = (void(*)(uObject*, int32_t*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeget_Count_fn;
    type->interface7.fp_get_Item = (void(*)(uObject*, int32_t*, uTRef))::g::Fuse::Visual__UnoCollectionsIListFuseNodeget_Item_fn;
    type->interface7.fp_Insert = (void(*)(uObject*, int32_t*, void*))::g::Fuse::Visual__Insert1_fn;
    type->interface8.fp_OnPropertyChanged = (void(*)(uObject*, ::g::Uno::UX::PropertyObject*, ::g::Uno::UX::Selector*))::g::Fuse::Controls::Control__OnPropertyChanged2_fn;
    type->interface9.fp_FindTemplate = (void(*)(uObject*, uString*, ::g::Uno::UX::Template**))::g::Fuse::Visual__FindTemplate_fn;
    type->interface11.fp_Add = (void(*)(uObject*, void*))::g::Fuse::Visual__Add1_fn;
    type->interface11.fp_Remove = (void(*)(uObject*, void*, bool*))::g::Fuse::Visual__Remove1_fn;
    type->interface5.fp_Clear = (void(*)(uObject*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingClear_fn;
    type->interface5.fp_Contains = (void(*)(uObject*, void*, bool*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingContains_fn;
    type->interface0.fp_RemoveAt = (void(*)(uObject*, int32_t*))::g::Fuse::Node__UnoCollectionsIListFuseBindingRemoveAt_fn;
    type->interface6.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Node__UnoCollectionsIEnumerableFuseBindingGetEnumerator_fn;
    type->interface1.fp_SetScriptObject = (void(*)(uObject*, uObject*, ::g::Fuse::Scripting::Context*))::g::Fuse::Node__FuseScriptingIScriptObjectSetScriptObject_fn;
    type->interface5.fp_get_Count = (void(*)(uObject*, int32_t*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingget_Count_fn;
    type->interface0.fp_get_Item = (void(*)(uObject*, int32_t*, uTRef))::g::Fuse::Node__UnoCollectionsIListFuseBindingget_Item_fn;
    type->interface1.fp_get_ScriptObject = (void(*)(uObject*, uObject**))::g::Fuse::Node__FuseScriptingIScriptObjectget_ScriptObject_fn;
    type->interface1.fp_get_ScriptContext = (void(*)(uObject*, ::g::Fuse::Scripting::Context**))::g::Fuse::Node__FuseScriptingIScriptObjectget_ScriptContext_fn;
    type->interface4.fp_get_SourceNearest = (void(*)(uObject*, uObject**))::g::Fuse::Node__FuseISourceLocationget_SourceNearest_fn;
    type->interface3.fp_add_Unrooted = (void(*)(uObject*, uDelegate*))::g::Fuse::Node__FuseINotifyUnrootedadd_Unrooted_fn;
    type->interface3.fp_remove_Unrooted = (void(*)(uObject*, uDelegate*))::g::Fuse::Node__FuseINotifyUnrootedremove_Unrooted_fn;
    type->interface0.fp_Insert = (void(*)(uObject*, int32_t*, void*))::g::Fuse::Node__Insert_fn;
    type->interface2.fp_get_Properties = (void(*)(uObject*, ::g::Fuse::Properties**))::g::Fuse::Node__get_Properties_fn;
    type->interface4.fp_get_SourceLineNumber = (void(*)(uObject*, int32_t*))::g::Fuse::Node__get_SourceLineNumber_fn;
    type->interface4.fp_get_SourceFileName = (void(*)(uObject*, uString**))::g::Fuse::Node__get_SourceFileName_fn;
    type->interface5.fp_Add = (void(*)(uObject*, void*))::g::Fuse::Node__Add_fn;
    type->interface5.fp_Remove = (void(*)(uObject*, void*, bool*))::g::Fuse::Node__Remove_fn;
    return type;
}

// public SubBut() :72
void SubBut__ctor_9_fn(SubBut* __this)
{
    __this->ctor_9();
}

// public string get_Btext() :8
void SubBut__get_Btext_fn(SubBut* __this, uString** __retval)
{
    *__retval = __this->Btext();
}

// public void set_Btext(string value) :9
void SubBut__set_Btext_fn(SubBut* __this, uString* value)
{
    __this->Btext(value);
}

// public string get_Direction() :38
void SubBut__get_Direction_fn(SubBut* __this, uString** __retval)
{
    *__retval = __this->Direction();
}

// public void set_Direction(string value) :39
void SubBut__set_Direction_fn(SubBut* __this, uString* value)
{
    __this->Direction(value);
}

// public string get_Icon() :23
void SubBut__get_Icon_fn(SubBut* __this, uString** __retval)
{
    *__retval = __this->Icon();
}

// public void set_Icon(string value) :24
void SubBut__set_Icon_fn(SubBut* __this, uString* value)
{
    __this->Icon(value);
}

// public float4 get_IconColor() :53
void SubBut__get_IconColor_fn(SubBut* __this, ::g::Uno::Float4* __retval)
{
    *__retval = __this->IconColor();
}

// public void set_IconColor(float4 value) :54
void SubBut__set_IconColor_fn(SubBut* __this, ::g::Uno::Float4* value)
{
    __this->IconColor(*value);
}

// private void InitializeUX() :76
void SubBut__InitializeUX1_fn(SubBut* __this)
{
    __this->InitializeUX1();
}

// public SubBut New() :72
void SubBut__New6_fn(SubBut** __retval)
{
    *__retval = SubBut::New6();
}

// public void SetBtext(string value, Uno.UX.IPropertyListener origin) :11
void SubBut__SetBtext_fn(SubBut* __this, uString* value, uObject* origin)
{
    __this->SetBtext(value, origin);
}

// public void SetDirection(string value, Uno.UX.IPropertyListener origin) :41
void SubBut__SetDirection_fn(SubBut* __this, uString* value, uObject* origin)
{
    __this->SetDirection(value, origin);
}

// public void SetIcon(string value, Uno.UX.IPropertyListener origin) :26
void SubBut__SetIcon_fn(SubBut* __this, uString* value, uObject* origin)
{
    __this->SetIcon(value, origin);
}

// public void SetIconColor(float4 value, Uno.UX.IPropertyListener origin) :56
void SubBut__SetIconColor_fn(SubBut* __this, ::g::Uno::Float4* value, uObject* origin)
{
    __this->SetIconColor(*value, origin);
}

::g::Uno::UX::Selector SubBut::__selector0_;
::g::Uno::UX::Selector SubBut::__selector1_;
::g::Uno::UX::Selector SubBut::__selector2_;

// public SubBut() [instance] :72
void SubBut::ctor_9()
{
    ctor_8();
    InitializeUX1();
}

// public string get_Btext() [instance] :8
uString* SubBut::Btext()
{
    return _field_Btext;
}

// public void set_Btext(string value) [instance] :9
void SubBut::Btext(uString* value)
{
    SetBtext(value, NULL);
}

// public string get_Direction() [instance] :38
uString* SubBut::Direction()
{
    return _field_Direction;
}

// public void set_Direction(string value) [instance] :39
void SubBut::Direction(uString* value)
{
    SetDirection(value, NULL);
}

// public string get_Icon() [instance] :23
uString* SubBut::Icon()
{
    return _field_Icon;
}

// public void set_Icon(string value) [instance] :24
void SubBut::Icon(uString* value)
{
    SetIcon(value, NULL);
}

// public float4 get_IconColor() [instance] :53
::g::Uno::Float4 SubBut::IconColor()
{
    return _field_IconColor;
}

// public void set_IconColor(float4 value) [instance] :54
void SubBut::IconColor(::g::Uno::Float4 value)
{
    SetIconColor(value, NULL);
}

// private void InitializeUX() [instance] :76
void SubBut::InitializeUX1()
{
    ::g::Fuse::Reactive::Constant* temp2 = ::g::Fuse::Reactive::Constant::New1(this);
    ::g::Fuse::Controls::Text* temp = ::g::Fuse::Controls::Text::New3();
    temp_Value_inst = ::g::OSP_FuseControlsTextControl_Value_Property::New1(temp, SubBut::__selector0_);
    ::g::Fuse::Reactive::Property* temp3 = ::g::Fuse::Reactive::Property::New1(temp2, ::g::OSP_accessor_SubBut_Icon::Singleton());
    ::g::Fuse::Reactive::Constant* temp4 = ::g::Fuse::Reactive::Constant::New1(this);
    temp_TextColor_inst = ::g::OSP_FuseControlsTextControl_TextColor_Property::New1(temp, SubBut::__selector1_);
    ::g::Fuse::Reactive::Property* temp5 = ::g::Fuse::Reactive::Property::New1(temp4, ::g::OSP_accessor_SubBut_IconColor::Singleton());
    ::g::Fuse::Reactive::Constant* temp6 = ::g::Fuse::Reactive::Constant::New1(this);
    ::g::Fuse::Controls::Text* temp1 = ::g::Fuse::Controls::Text::New3();
    temp1_Value_inst = ::g::OSP_FuseControlsTextControl_Value_Property::New1(temp1, SubBut::__selector0_);
    ::g::Fuse::Reactive::Property* temp7 = ::g::Fuse::Reactive::Property::New1(temp6, ::g::OSP_accessor_SubBut_Btext::Singleton());
    ::g::Fuse::Reactive::Constant* temp8 = ::g::Fuse::Reactive::Constant::New1(this);
    this_DockPanel_Dock_inst = ::g::OSP_FuseElementsElement_DockPanelDock_Property::New1(this, SubBut::__selector2_);
    ::g::Fuse::Reactive::Property* temp9 = ::g::Fuse::Reactive::Property::New1(temp8, ::g::OSP_accessor_SubBut_Direction::Singleton());
    ::g::Fuse::Controls::DockPanel* temp10 = ::g::Fuse::Controls::DockPanel::New4();
    ::g::Fuse::Reactive::DataBinding* temp11 = ::g::Fuse::Reactive::DataBinding::New1(temp_Value_inst, (uObject*)temp3, 3);
    ::g::Fuse::Reactive::DataBinding* temp12 = ::g::Fuse::Reactive::DataBinding::New1(temp_TextColor_inst, (uObject*)temp5, 3);
    ::g::Fuse::Reactive::DataBinding* temp13 = ::g::Fuse::Reactive::DataBinding::New1(temp1_Value_inst, (uObject*)temp7, 3);
    ::g::Fuse::Controls::Rectangle* temp14 = ::g::Fuse::Controls::Rectangle::New3();
    ::g::Fuse::Reactive::DataBinding* temp15 = ::g::Fuse::Reactive::DataBinding::New1(this_DockPanel_Dock_inst, (uObject*)temp9, 3);
    Padding(::g::Uno::Float4__New2(5.0f, 5.0f, 5.0f, 5.0f));
    SourceLineNumber(97);
    SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    temp10->SourceLineNumber(102);
    temp10->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp10->Children()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Node>*/]), temp);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp10->Children()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Node>*/]), temp1);
    temp->Alignment(10);
    temp->Margin(::g::Uno::Float4__New2(5.0f, 0.0f, 5.0f, 0.0f));
    temp->SourceLineNumber(103);
    temp->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    ::g::Fuse::Controls::DockPanel::SetDock(temp, 0);
    temp->Font(::g::MainView::FontAwesome());
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Bindings()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp11);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Bindings()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp12);
    temp3->SourceLineNumber(103);
    temp3->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    temp2->SourceLineNumber(103);
    temp2->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    temp5->SourceLineNumber(103);
    temp5->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    temp4->SourceLineNumber(103);
    temp4->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    temp1->TextAlignment(1);
    temp1->TextColor(::g::Uno::Float4__New2(0.9686275f, 0.8980392f, 0.372549f, 1.0f));
    temp1->Alignment(10);
    temp1->SourceLineNumber(104);
    temp1->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    ::g::Fuse::Controls::DockPanel::SetDock(temp1, 0);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp1->Bindings()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp13);
    temp7->SourceLineNumber(104);
    temp7->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    temp6->SourceLineNumber(104);
    temp6->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    temp14->Color(::g::Uno::Float4__New2(0.9686275f, 0.9686275f, 0.9686275f, 1.0f));
    temp14->Layer(1);
    temp14->SourceLineNumber(106);
    temp14->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    temp9->SourceLineNumber(97);
    temp9->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    temp8->SourceLineNumber(97);
    temp8->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Node>*/]), temp10);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Node>*/]), temp14);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Bindings()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp15);
}

// public void SetBtext(string value, Uno.UX.IPropertyListener origin) [instance] :11
void SubBut::SetBtext(uString* value, uObject* origin)
{
    if (::g::Uno::String::op_Inequality(value, _field_Btext))
    {
        _field_Btext = value;
        OnPropertyChanged1(::g::Uno::UX::Selector__op_Implicit1(::STRINGS[4/*"Btext"*/]), origin);
    }
}

// public void SetDirection(string value, Uno.UX.IPropertyListener origin) [instance] :41
void SubBut::SetDirection(uString* value, uObject* origin)
{
    if (::g::Uno::String::op_Inequality(value, _field_Direction))
    {
        _field_Direction = value;
        OnPropertyChanged1(::g::Uno::UX::Selector__op_Implicit1(::STRINGS[5/*"Direction"*/]), origin);
    }
}

// public void SetIcon(string value, Uno.UX.IPropertyListener origin) [instance] :26
void SubBut::SetIcon(uString* value, uObject* origin)
{
    if (::g::Uno::String::op_Inequality(value, _field_Icon))
    {
        _field_Icon = value;
        OnPropertyChanged1(::g::Uno::UX::Selector__op_Implicit1(::STRINGS[6/*"Icon"*/]), origin);
    }
}

// public void SetIconColor(float4 value, Uno.UX.IPropertyListener origin) [instance] :56
void SubBut::SetIconColor(::g::Uno::Float4 value, uObject* origin)
{
    if (::g::Uno::Float4__op_Inequality(value, _field_IconColor))
    {
        _field_IconColor = value;
        OnPropertyChanged1(::g::Uno::UX::Selector__op_Implicit1(::STRINGS[7/*"IconColor"*/]), origin);
    }
}

// public SubBut New() [static] :72
SubBut* SubBut::New6()
{
    SubBut* obj1 = (SubBut*)uNew(SubBut_typeof());
    obj1->ctor_9();
    return obj1;
}
// }

} // ::g
