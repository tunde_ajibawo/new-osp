// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/History.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.History.h>
#include <_root.History.Template.h>
#include <_root.MainView.h>
#include <_root.OSP_FuseControl-b26a5bbf.h>
#include <Fuse.Binding.h>
#include <Fuse.Controls.DockPanel.h>
#include <Fuse.Controls.Rectangle.h>
#include <Fuse.Controls.Shadow.h>
#include <Fuse.Controls.Shape.h>
#include <Fuse.Controls.Text.h>
#include <Fuse.Controls.TextControl.h>
#include <Fuse.Elements.Element.h>
#include <Fuse.Font.h>
#include <Fuse.Layer.h>
#include <Fuse.Layouts.Dock.h>
#include <Fuse.Node.h>
#include <Fuse.Reactive.BindingMode.h>
#include <Fuse.Reactive.Data.h>
#include <Fuse.Reactive.DataBinding.h>
#include <Fuse.Reactive.Expression.h>
#include <Fuse.Reactive.IExpression.h>
#include <Fuse.Visual.h>
#include <Uno.Bool.h>
#include <Uno.Collections.ICollection-1.h>
#include <Uno.Collections.IList-1.h>
#include <Uno.Float.h>
#include <Uno.Float4.h>
#include <Uno.Int.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.UX.Property.h>
#include <Uno.UX.Property1-1.h>
#include <Uno.UX.Selector.h>
static uString* STRINGS[8];
static uType* TYPES[2];

namespace g{

// public partial sealed class History.Template :6
// {
// static Template() :21
static void History__Template__cctor__fn(uType* __type)
{
    History__Template::__selector0_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[0/*"Value"*/]);
    History__Template::__selector1_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[1/*"item"*/]);
}

static void History__Template_build(uType* type)
{
    ::STRINGS[0] = uString::Const("Value");
    ::STRINGS[1] = uString::Const("item");
    ::STRINGS[2] = uString::Const("location_name");
    ::STRINGS[3] = uString::Const("departure_location");
    ::STRINGS[4] = uString::Const("departure_datetime");
    ::STRINGS[5] = uString::Const("arrival_location");
    ::STRINGS[6] = uString::Const("arrival_datetime");
    ::STRINGS[7] = uString::Const("history.ux");
    ::TYPES[0] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL);
    ::TYPES[1] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL);
    type->SetDependencies(
        ::g::MainView_typeof());
    type->SetFields(2,
        ::g::History_typeof(), offsetof(History__Template, __parent1), uFieldFlagsWeak,
        ::g::History_typeof(), offsetof(History__Template, __parentInstance1), uFieldFlagsWeak,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(History__Template, temp_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(History__Template, temp1_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(History__Template, temp2_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(History__Template, temp3_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(History__Template, temp4_Value_inst), 0,
        ::g::Fuse::Controls::Rectangle_typeof(), offsetof(History__Template, item), 0,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&History__Template::__selector0_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&History__Template::__selector1_, uFieldFlagsStatic);
}

::g::Uno::UX::Template_type* History__Template_typeof()
{
    static uSStrong< ::g::Uno::UX::Template_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::Template_typeof();
    options.FieldCount = 12;
    options.DependencyCount = 1;
    options.ObjectSize = sizeof(History__Template);
    options.TypeSize = sizeof(::g::Uno::UX::Template_type);
    type = (::g::Uno::UX::Template_type*)uClassType::New("History.Template", options);
    type->fp_build_ = History__Template_build;
    type->fp_cctor_ = History__Template__cctor__fn;
    type->fp_New1 = (void(*)(::g::Uno::UX::Template*, uObject**))History__Template__New1_fn;
    return type;
}

// public Template(History parent, History parentInstance) :10
void History__Template__ctor_1_fn(History__Template* __this, ::g::History* parent, ::g::History* parentInstance)
{
    __this->ctor_1(parent, parentInstance);
}

// public override sealed object New() :24
void History__Template__New1_fn(History__Template* __this, uObject** __retval)
{
    ::g::Fuse::Controls::DockPanel* __self1 = ::g::Fuse::Controls::DockPanel::New4();
    ::g::Fuse::Controls::Text* temp = ::g::Fuse::Controls::Text::New3();
    __this->temp_Value_inst = ::g::OSP_FuseControlsTextControl_Value_Property::New1(temp, History__Template::__selector0_);
    ::g::Fuse::Reactive::Data* temp5 = ::g::Fuse::Reactive::Data::New1(::STRINGS[2/*"location_name"*/]);
    ::g::Fuse::Controls::Text* temp1 = ::g::Fuse::Controls::Text::New3();
    __this->temp1_Value_inst = ::g::OSP_FuseControlsTextControl_Value_Property::New1(temp1, History__Template::__selector0_);
    ::g::Fuse::Reactive::Data* temp6 = ::g::Fuse::Reactive::Data::New1(::STRINGS[3/*"departure_l...*/]);
    ::g::Fuse::Controls::Text* temp2 = ::g::Fuse::Controls::Text::New3();
    __this->temp2_Value_inst = ::g::OSP_FuseControlsTextControl_Value_Property::New1(temp2, History__Template::__selector0_);
    ::g::Fuse::Reactive::Data* temp7 = ::g::Fuse::Reactive::Data::New1(::STRINGS[4/*"departure_d...*/]);
    ::g::Fuse::Controls::Text* temp3 = ::g::Fuse::Controls::Text::New3();
    __this->temp3_Value_inst = ::g::OSP_FuseControlsTextControl_Value_Property::New1(temp3, History__Template::__selector0_);
    ::g::Fuse::Reactive::Data* temp8 = ::g::Fuse::Reactive::Data::New1(::STRINGS[5/*"arrival_loc...*/]);
    ::g::Fuse::Controls::Text* temp4 = ::g::Fuse::Controls::Text::New3();
    __this->temp4_Value_inst = ::g::OSP_FuseControlsTextControl_Value_Property::New1(temp4, History__Template::__selector0_);
    ::g::Fuse::Reactive::Data* temp9 = ::g::Fuse::Reactive::Data::New1(::STRINGS[6/*"arrival_dat...*/]);
    ::g::Fuse::Reactive::DataBinding* temp10 = ::g::Fuse::Reactive::DataBinding::New1(__this->temp_Value_inst, (uObject*)temp5, 3);
    ::g::Fuse::Reactive::DataBinding* temp11 = ::g::Fuse::Reactive::DataBinding::New1(__this->temp1_Value_inst, (uObject*)temp6, 3);
    ::g::Fuse::Reactive::DataBinding* temp12 = ::g::Fuse::Reactive::DataBinding::New1(__this->temp2_Value_inst, (uObject*)temp7, 3);
    ::g::Fuse::Reactive::DataBinding* temp13 = ::g::Fuse::Reactive::DataBinding::New1(__this->temp3_Value_inst, (uObject*)temp8, 3);
    ::g::Fuse::Reactive::DataBinding* temp14 = ::g::Fuse::Reactive::DataBinding::New1(__this->temp4_Value_inst, (uObject*)temp9, 3);
    __this->item = ::g::Fuse::Controls::Rectangle::New3();
    ::g::Fuse::Controls::Shadow* temp15 = ::g::Fuse::Controls::Shadow::New2();
    __self1->Margin(::g::Uno::Float4__New2(0.0f, 5.0f, 0.0f, 0.0f));
    __self1->Padding(::g::Uno::Float4__New2(15.0f, 15.0f, 15.0f, 15.0f));
    __self1->SourceLineNumber(119);
    __self1->SourceFileName(::STRINGS[7/*"history.ux"*/]);
    temp->FontSize(15.0f);
    temp->TextColor(::g::Uno::Float4__New2(0.2f, 0.2f, 0.2f, 1.0f));
    temp->Margin(::g::Uno::Float4__New2(0.0f, 0.0f, 0.0f, 10.0f));
    temp->SourceLineNumber(120);
    temp->SourceFileName(::STRINGS[7/*"history.ux"*/]);
    ::g::Fuse::Controls::DockPanel::SetDock(temp, 2);
    temp->Font(::g::MainView::Raleway());
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Bindings()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp10);
    temp5->SourceLineNumber(120);
    temp5->SourceFileName(::STRINGS[7/*"history.ux"*/]);
    temp1->FontSize(13.0f);
    temp1->SourceLineNumber(121);
    temp1->SourceFileName(::STRINGS[7/*"history.ux"*/]);
    ::g::Fuse::Controls::DockPanel::SetDock(temp1, 0);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp1->Bindings()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp11);
    temp6->SourceLineNumber(121);
    temp6->SourceFileName(::STRINGS[7/*"history.ux"*/]);
    temp2->FontSize(12.0f);
    temp2->TextColor(::g::Uno::Float4__New2(0.4666667f, 0.4666667f, 0.4666667f, 1.0f));
    temp2->SourceLineNumber(122);
    temp2->SourceFileName(::STRINGS[7/*"history.ux"*/]);
    ::g::Fuse::Controls::DockPanel::SetDock(temp2, 1);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp2->Bindings()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp12);
    temp7->SourceLineNumber(122);
    temp7->SourceFileName(::STRINGS[7/*"history.ux"*/]);
    temp3->FontSize(13.0f);
    temp3->SourceLineNumber(124);
    temp3->SourceFileName(::STRINGS[7/*"history.ux"*/]);
    ::g::Fuse::Controls::DockPanel::SetDock(temp3, 3);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp3->Bindings()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp13);
    temp8->SourceLineNumber(124);
    temp8->SourceFileName(::STRINGS[7/*"history.ux"*/]);
    temp4->FontSize(12.0f);
    temp4->TextColor(::g::Uno::Float4__New2(0.4666667f, 0.4666667f, 0.4666667f, 1.0f));
    temp4->SourceLineNumber(125);
    temp4->SourceFileName(::STRINGS[7/*"history.ux"*/]);
    ::g::Fuse::Controls::DockPanel::SetDock(temp4, 3);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp4->Bindings()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp14);
    temp9->SourceLineNumber(125);
    temp9->SourceFileName(::STRINGS[7/*"history.ux"*/]);
    uPtr(__this->item)->Color(::g::Uno::Float4__New2(1.0f, 0.9921569f, 0.9372549f, 1.0f));
    uPtr(__this->item)->Layer(1);
    uPtr(__this->item)->Name(History__Template::__selector1_);
    uPtr(__this->item)->SourceLineNumber(126);
    uPtr(__this->item)->SourceFileName(::STRINGS[7/*"history.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__this->item)->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp15);
    temp15->Distance(2.0f);
    temp15->Size(2.0f);
    temp15->SourceLineNumber(127);
    temp15->SourceFileName(::STRINGS[7/*"history.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(__self1->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(__self1->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp1);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(__self1->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp2);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(__self1->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp3);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(__self1->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp4);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(__self1->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), __this->item);
    return *__retval = __self1, void();
}

// public Template New(History parent, History parentInstance) :10
void History__Template__New2_fn(::g::History* parent, ::g::History* parentInstance, History__Template** __retval)
{
    *__retval = History__Template::New2(parent, parentInstance);
}

::g::Uno::UX::Selector History__Template::__selector0_;
::g::Uno::UX::Selector History__Template::__selector1_;

// public Template(History parent, History parentInstance) [instance] :10
void History__Template::ctor_1(::g::History* parent, ::g::History* parentInstance)
{
    ctor_(NULL, false);
    __parent1 = parent;
    __parentInstance1 = parentInstance;
}

// public Template New(History parent, History parentInstance) [static] :10
History__Template* History__Template::New2(::g::History* parent, ::g::History* parentInstance)
{
    History__Template* obj1 = (History__Template*)uNew(History__Template_typeof());
    obj1->ctor_1(parent, parentInstance);
    return obj1;
}
// }

} // ::g
