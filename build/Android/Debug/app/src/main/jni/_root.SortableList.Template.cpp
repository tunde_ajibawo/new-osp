// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/SortableList.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.Item.h>
#include <_root.SortableList.h>
#include <_root.SortableList.Template.h>
#include <Fuse.Node.h>
#include <Uno.Bool.h>
#include <Uno.Int.h>
#include <Uno.Object.h>
#include <Uno.String.h>
static uString* STRINGS[1];

namespace g{

// public partial sealed class SortableList.Template :36
// {
// static Template() :45
static void SortableList__Template__cctor__fn(uType* __type)
{
}

static void SortableList__Template_build(uType* type)
{
    ::STRINGS[0] = uString::Const("dashboardlist.ux");
    type->SetFields(2,
        ::g::SortableList_typeof(), offsetof(SortableList__Template, __parent1), uFieldFlagsWeak,
        ::g::SortableList_typeof(), offsetof(SortableList__Template, __parentInstance1), uFieldFlagsWeak);
}

::g::Uno::UX::Template_type* SortableList__Template_typeof()
{
    static uSStrong< ::g::Uno::UX::Template_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::Template_typeof();
    options.FieldCount = 4;
    options.ObjectSize = sizeof(SortableList__Template);
    options.TypeSize = sizeof(::g::Uno::UX::Template_type);
    type = (::g::Uno::UX::Template_type*)uClassType::New("SortableList.Template", options);
    type->fp_build_ = SortableList__Template_build;
    type->fp_cctor_ = SortableList__Template__cctor__fn;
    type->fp_New1 = (void(*)(::g::Uno::UX::Template*, uObject**))SortableList__Template__New1_fn;
    return type;
}

// public Template(SortableList parent, SortableList parentInstance) :40
void SortableList__Template__ctor_1_fn(SortableList__Template* __this, ::g::SortableList* parent, ::g::SortableList* parentInstance)
{
    __this->ctor_1(parent, parentInstance);
}

// public override sealed object New() :48
void SortableList__Template__New1_fn(SortableList__Template* __this, uObject** __retval)
{
    ::g::Item* __self1 = ::g::Item::New5();
    __self1->SourceLineNumber(110);
    __self1->SourceFileName(::STRINGS[0/*"dashboardli...*/]);
    return *__retval = __self1, void();
}

// public Template New(SortableList parent, SortableList parentInstance) :40
void SortableList__Template__New2_fn(::g::SortableList* parent, ::g::SortableList* parentInstance, SortableList__Template** __retval)
{
    *__retval = SortableList__Template::New2(parent, parentInstance);
}

// public Template(SortableList parent, SortableList parentInstance) [instance] :40
void SortableList__Template::ctor_1(::g::SortableList* parent, ::g::SortableList* parentInstance)
{
    ctor_(NULL, false);
    __parent1 = parent;
    __parentInstance1 = parentInstance;
}

// public Template New(SortableList parent, SortableList parentInstance) [static] :40
SortableList__Template* SortableList__Template::New2(::g::SortableList* parent, ::g::SortableList* parentInstance)
{
    SortableList__Template* obj1 = (SortableList__Template*)uNew(SortableList__Template_typeof());
    obj1->ctor_1(parent, parentInstance);
    return obj1;
}
// }

} // ::g
