// This file was generated based on /Users/tundeajibawo/Downloads/osp/OSP.unoproj.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.OSP_bundle.h>
#include <Uno.IO.Bundle.h>
#include <Uno.IO.BundleFile.h>
#include <Uno.String.h>
static uString* STRINGS[25];

namespace g{

// public static generated class OSP_bundle :0
// {
// static OSP_bundle() :0
static void OSP_bundle__cctor__fn(uType* __type)
{
    ::g::Uno::IO::Bundle_typeof()->Init();
    OSP_bundle::AlegreyaSansBoldac83ec20_ = uPtr(::g::Uno::IO::Bundle::Get(::STRINGS[0/*"OSP"*/]))->GetFile(::STRINGS[1/*"alegreyasan...*/]);
    OSP_bundle::AlegreyaSansBoldea43f500_ = uPtr(::g::Uno::IO::Bundle::Get(::STRINGS[0/*"OSP"*/]))->GetFile(::STRINGS[2/*"alegreyasan...*/]);
    OSP_bundle::BentonSansMediuma84c7b4c_ = uPtr(::g::Uno::IO::Bundle::Get(::STRINGS[0/*"OSP"*/]))->GetFile(::STRINGS[3/*"bentonsans-...*/]);
    OSP_bundle::Browser48e90536_ = uPtr(::g::Uno::IO::Bundle::Get(::STRINGS[0/*"OSP"*/]))->GetFile(::STRINGS[4/*"browser-675...*/]);
    OSP_bundle::Chat187ba3a6_ = uPtr(::g::Uno::IO::Bundle::Get(::STRINGS[0/*"OSP"*/]))->GetFile(::STRINGS[5/*"chat-65366d...*/]);
    OSP_bundle::Comment2c226469_ = uPtr(::g::Uno::IO::Bundle::Get(::STRINGS[0/*"OSP"*/]))->GetFile(::STRINGS[6/*"comment-aab...*/]);
    OSP_bundle::diskc6e80153_ = uPtr(::g::Uno::IO::Bundle::Get(::STRINGS[0/*"OSP"*/]))->GetFile(::STRINGS[7/*"disk-73c84d...*/]);
    OSP_bundle::dprb1594be8_ = uPtr(::g::Uno::IO::Bundle::Get(::STRINGS[0/*"OSP"*/]))->GetFile(::STRINGS[8/*"dpr-4ac79c4...*/]);
    OSP_bundle::Feeda3231344_ = uPtr(::g::Uno::IO::Bundle::Get(::STRINGS[0/*"OSP"*/]))->GetFile(::STRINGS[9/*"feed-858aae...*/]);
    OSP_bundle::FontAwesomef6831785_ = uPtr(::g::Uno::IO::Bundle::Get(::STRINGS[0/*"OSP"*/]))->GetFile(::STRINGS[10/*"fontawesome...*/]);
    OSP_bundle::goback248ef465_ = uPtr(::g::Uno::IO::Bundle::Get(::STRINGS[0/*"OSP"*/]))->GetFile(::STRINGS[11/*"goback-fe03...*/]);
    OSP_bundle::historya6ead22a_ = uPtr(::g::Uno::IO::Bundle::Get(::STRINGS[0/*"OSP"*/]))->GetFile(::STRINGS[12/*"history-84a...*/]);
    OSP_bundle::home1fe12191_ = uPtr(::g::Uno::IO::Bundle::Get(::STRINGS[0/*"OSP"*/]))->GetFile(::STRINGS[13/*"home-6ea9d6...*/]);
    OSP_bundle::LatoBold946ea59c_ = uPtr(::g::Uno::IO::Bundle::Get(::STRINGS[0/*"OSP"*/]))->GetFile(::STRINGS[14/*"lato-bold-5...*/]);
    OSP_bundle::LatoBolde6c51dbc_ = uPtr(::g::Uno::IO::Bundle::Get(::STRINGS[0/*"OSP"*/]))->GetFile(::STRINGS[15/*"lato-bold-b...*/]);
    OSP_bundle::LatoRegular2367fb87_ = uPtr(::g::Uno::IO::Bundle::Get(::STRINGS[0/*"OSP"*/]))->GetFile(::STRINGS[16/*"lato-regula...*/]);
    OSP_bundle::Likeb029c8b1_ = uPtr(::g::Uno::IO::Bundle::Get(::STRINGS[0/*"OSP"*/]))->GetFile(::STRINGS[17/*"like-72080f...*/]);
    OSP_bundle::Location77a90067_ = uPtr(::g::Uno::IO::Bundle::Get(::STRINGS[0/*"OSP"*/]))->GetFile(::STRINGS[18/*"location-35...*/]);
    OSP_bundle::navigationb176862e_ = uPtr(::g::Uno::IO::Bundle::Get(::STRINGS[0/*"OSP"*/]))->GetFile(::STRINGS[19/*"navigation-...*/]);
    OSP_bundle::password9aeb6d35_ = uPtr(::g::Uno::IO::Bundle::Get(::STRINGS[0/*"OSP"*/]))->GetFile(::STRINGS[20/*"password-ed...*/]);
    OSP_bundle::RalewaySemiBold0870d6b1_ = uPtr(::g::Uno::IO::Bundle::Get(::STRINGS[0/*"OSP"*/]))->GetFile(::STRINGS[21/*"raleway-sem...*/]);
    OSP_bundle::RobotoBold69495149_ = uPtr(::g::Uno::IO::Bundle::Get(::STRINGS[0/*"OSP"*/]))->GetFile(::STRINGS[22/*"roboto-bold...*/]);
    OSP_bundle::Sharec2cad64f_ = uPtr(::g::Uno::IO::Bundle::Get(::STRINGS[0/*"OSP"*/]))->GetFile(::STRINGS[23/*"share-cecc9...*/]);
    OSP_bundle::signupb2004114_ = uPtr(::g::Uno::IO::Bundle::Get(::STRINGS[0/*"OSP"*/]))->GetFile(::STRINGS[24/*"signup-b331...*/]);
}

static void OSP_bundle_build(uType* type)
{
    ::STRINGS[0] = uString::Const("OSP");
    ::STRINGS[1] = uString::Const("alegreyasans-bold-b2352e0f.otf");
    ::STRINGS[2] = uString::Const("alegreyasans-bold-5f46d3ef.otf");
    ::STRINGS[3] = uString::Const("bentonsans-medium-1c963ad3.otf");
    ::STRINGS[4] = uString::Const("browser-675e9dc7.png");
    ::STRINGS[5] = uString::Const("chat-65366d0d.png");
    ::STRINGS[6] = uString::Const("comment-aab939a2.png");
    ::STRINGS[7] = uString::Const("disk-73c84dec.png");
    ::STRINGS[8] = uString::Const("dpr-4ac79c41.png");
    ::STRINGS[9] = uString::Const("feed-858aae7d.png");
    ::STRINGS[10] = uString::Const("fontawesome-785298b4.otf");
    ::STRINGS[11] = uString::Const("goback-fe0319bc.png");
    ::STRINGS[12] = uString::Const("history-84a0c3d9.png");
    ::STRINGS[13] = uString::Const("home-6ea9d68a.png");
    ::STRINGS[14] = uString::Const("lato-bold-585fc8b5.ttf");
    ::STRINGS[15] = uString::Const("lato-bold-b2f600d5.ttf");
    ::STRINGS[16] = uString::Const("lato-regular-b6bc0428.ttf");
    ::STRINGS[17] = uString::Const("like-72080f00.png");
    ::STRINGS[18] = uString::Const("location-35a9d2e8.png");
    ::STRINGS[19] = uString::Const("navigation-0c9db205.js");
    ::STRINGS[20] = uString::Const("password-edd6ee24.js");
    ::STRINGS[21] = uString::Const("raleway-semibold-472acfaa.ttf");
    ::STRINGS[22] = uString::Const("roboto-bold-97b6bffa.ttf");
    ::STRINGS[23] = uString::Const("share-cecc9920.png");
    ::STRINGS[24] = uString::Const("signup-b331374d.js");
    type->SetFields(0,
        ::g::Uno::IO::BundleFile_typeof(), (uintptr_t)&OSP_bundle::AlegreyaSansBoldac83ec20_, uFieldFlagsStatic,
        ::g::Uno::IO::BundleFile_typeof(), (uintptr_t)&OSP_bundle::AlegreyaSansBoldea43f500_, uFieldFlagsStatic,
        ::g::Uno::IO::BundleFile_typeof(), (uintptr_t)&OSP_bundle::BentonSansMediuma84c7b4c_, uFieldFlagsStatic,
        ::g::Uno::IO::BundleFile_typeof(), (uintptr_t)&OSP_bundle::Browser48e90536_, uFieldFlagsStatic,
        ::g::Uno::IO::BundleFile_typeof(), (uintptr_t)&OSP_bundle::Chat187ba3a6_, uFieldFlagsStatic,
        ::g::Uno::IO::BundleFile_typeof(), (uintptr_t)&OSP_bundle::Comment2c226469_, uFieldFlagsStatic,
        ::g::Uno::IO::BundleFile_typeof(), (uintptr_t)&OSP_bundle::diskc6e80153_, uFieldFlagsStatic,
        ::g::Uno::IO::BundleFile_typeof(), (uintptr_t)&OSP_bundle::dprb1594be8_, uFieldFlagsStatic,
        ::g::Uno::IO::BundleFile_typeof(), (uintptr_t)&OSP_bundle::Feeda3231344_, uFieldFlagsStatic,
        ::g::Uno::IO::BundleFile_typeof(), (uintptr_t)&OSP_bundle::FontAwesomef6831785_, uFieldFlagsStatic,
        ::g::Uno::IO::BundleFile_typeof(), (uintptr_t)&OSP_bundle::goback248ef465_, uFieldFlagsStatic,
        ::g::Uno::IO::BundleFile_typeof(), (uintptr_t)&OSP_bundle::historya6ead22a_, uFieldFlagsStatic,
        ::g::Uno::IO::BundleFile_typeof(), (uintptr_t)&OSP_bundle::home1fe12191_, uFieldFlagsStatic,
        ::g::Uno::IO::BundleFile_typeof(), (uintptr_t)&OSP_bundle::LatoBold946ea59c_, uFieldFlagsStatic,
        ::g::Uno::IO::BundleFile_typeof(), (uintptr_t)&OSP_bundle::LatoBolde6c51dbc_, uFieldFlagsStatic,
        ::g::Uno::IO::BundleFile_typeof(), (uintptr_t)&OSP_bundle::LatoRegular2367fb87_, uFieldFlagsStatic,
        ::g::Uno::IO::BundleFile_typeof(), (uintptr_t)&OSP_bundle::Likeb029c8b1_, uFieldFlagsStatic,
        ::g::Uno::IO::BundleFile_typeof(), (uintptr_t)&OSP_bundle::Location77a90067_, uFieldFlagsStatic,
        ::g::Uno::IO::BundleFile_typeof(), (uintptr_t)&OSP_bundle::navigationb176862e_, uFieldFlagsStatic,
        ::g::Uno::IO::BundleFile_typeof(), (uintptr_t)&OSP_bundle::password9aeb6d35_, uFieldFlagsStatic,
        ::g::Uno::IO::BundleFile_typeof(), (uintptr_t)&OSP_bundle::RalewaySemiBold0870d6b1_, uFieldFlagsStatic,
        ::g::Uno::IO::BundleFile_typeof(), (uintptr_t)&OSP_bundle::RobotoBold69495149_, uFieldFlagsStatic,
        ::g::Uno::IO::BundleFile_typeof(), (uintptr_t)&OSP_bundle::Sharec2cad64f_, uFieldFlagsStatic,
        ::g::Uno::IO::BundleFile_typeof(), (uintptr_t)&OSP_bundle::signupb2004114_, uFieldFlagsStatic);
}

uClassType* OSP_bundle_typeof()
{
    static uSStrong<uClassType*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.FieldCount = 24;
    options.TypeSize = sizeof(uClassType);
    type = uClassType::New("OSP_bundle", options);
    type->fp_build_ = OSP_bundle_build;
    type->fp_cctor_ = OSP_bundle__cctor__fn;
    return type;
}

uSStrong< ::g::Uno::IO::BundleFile*> OSP_bundle::AlegreyaSansBoldac83ec20_;
uSStrong< ::g::Uno::IO::BundleFile*> OSP_bundle::AlegreyaSansBoldea43f500_;
uSStrong< ::g::Uno::IO::BundleFile*> OSP_bundle::BentonSansMediuma84c7b4c_;
uSStrong< ::g::Uno::IO::BundleFile*> OSP_bundle::Browser48e90536_;
uSStrong< ::g::Uno::IO::BundleFile*> OSP_bundle::Chat187ba3a6_;
uSStrong< ::g::Uno::IO::BundleFile*> OSP_bundle::Comment2c226469_;
uSStrong< ::g::Uno::IO::BundleFile*> OSP_bundle::diskc6e80153_;
uSStrong< ::g::Uno::IO::BundleFile*> OSP_bundle::dprb1594be8_;
uSStrong< ::g::Uno::IO::BundleFile*> OSP_bundle::Feeda3231344_;
uSStrong< ::g::Uno::IO::BundleFile*> OSP_bundle::FontAwesomef6831785_;
uSStrong< ::g::Uno::IO::BundleFile*> OSP_bundle::goback248ef465_;
uSStrong< ::g::Uno::IO::BundleFile*> OSP_bundle::historya6ead22a_;
uSStrong< ::g::Uno::IO::BundleFile*> OSP_bundle::home1fe12191_;
uSStrong< ::g::Uno::IO::BundleFile*> OSP_bundle::LatoBold946ea59c_;
uSStrong< ::g::Uno::IO::BundleFile*> OSP_bundle::LatoBolde6c51dbc_;
uSStrong< ::g::Uno::IO::BundleFile*> OSP_bundle::LatoRegular2367fb87_;
uSStrong< ::g::Uno::IO::BundleFile*> OSP_bundle::Likeb029c8b1_;
uSStrong< ::g::Uno::IO::BundleFile*> OSP_bundle::Location77a90067_;
uSStrong< ::g::Uno::IO::BundleFile*> OSP_bundle::navigationb176862e_;
uSStrong< ::g::Uno::IO::BundleFile*> OSP_bundle::password9aeb6d35_;
uSStrong< ::g::Uno::IO::BundleFile*> OSP_bundle::RalewaySemiBold0870d6b1_;
uSStrong< ::g::Uno::IO::BundleFile*> OSP_bundle::RobotoBold69495149_;
uSStrong< ::g::Uno::IO::BundleFile*> OSP_bundle::Sharec2cad64f_;
uSStrong< ::g::Uno::IO::BundleFile*> OSP_bundle::signupb2004114_;
// }

} // ::g
