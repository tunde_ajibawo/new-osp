// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/TopBar.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Fuse.Animations.IResize.h>
#include <Fuse.Binding.h>
#include <Fuse.Controls.DockPanel.h>
#include <Fuse.Drawing.ISurfaceDrawable.h>
#include <Fuse.IActualPlacement.h>
#include <Fuse.INotifyUnrooted.h>
#include <Fuse.IProperties.h>
#include <Fuse.ISourceLocation.h>
#include <Fuse.ITemplateSource.h>
#include <Fuse.Node.h>
#include <Fuse.Scripting.IScriptObject.h>
#include <Fuse.Triggers.Actions.IHide.h>
#include <Fuse.Triggers.Actions.IShow.h>
#include <Fuse.Triggers.Actions-ea70af1f.h>
#include <Fuse.Visual.h>
#include <Uno.Collections.ICollection-1.h>
#include <Uno.Collections.IEnumerable-1.h>
#include <Uno.Collections.IList-1.h>
#include <Uno.UX.IPropertyListener.h>
namespace g{namespace Fuse{namespace Reactive{struct EventBinding;}}}
namespace g{namespace Uno{namespace UX{struct Property1;}}}
namespace g{namespace Uno{namespace UX{struct Selector;}}}
namespace g{struct TopBar;}

namespace g{

// public partial sealed class TopBar :2
// {
::g::Fuse::Controls::Panel_type* TopBar_typeof();
void TopBar__ctor_8_fn(TopBar* __this);
void TopBar__InitializeUX_fn(TopBar* __this);
void TopBar__New5_fn(TopBar** __retval);
void TopBar__SetTitle_fn(TopBar* __this, uString* value, uObject* origin);
void TopBar__get_Title_fn(TopBar* __this, uString** __retval);
void TopBar__set_Title_fn(TopBar* __this, uString* value);

struct TopBar : ::g::Fuse::Controls::DockPanel
{
    uStrong<uString*> _field_Title;
    uStrong< ::g::Uno::UX::Property1*> temp_Value_inst;
    uStrong< ::g::Fuse::Reactive::EventBinding*> temp_eb22;
    static ::g::Uno::UX::Selector __selector0_;
    static ::g::Uno::UX::Selector& __selector0() { return TopBar_typeof()->Init(), __selector0_; }

    void ctor_8();
    void InitializeUX();
    void SetTitle(uString* value, uObject* origin);
    uString* Title();
    void Title(uString* value);
    static TopBar* New5();
};
// }

} // ::g
