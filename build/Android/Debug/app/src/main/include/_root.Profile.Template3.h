// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/Profile.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.UX.Template.h>
namespace g{namespace Uno{namespace UX{struct Property1;}}}
namespace g{namespace Uno{namespace UX{struct Selector;}}}
namespace g{struct Profile;}
namespace g{struct Profile__Template3;}

namespace g{

// public partial sealed class Profile.Template3 :96
// {
::g::Uno::UX::Template_type* Profile__Template3_typeof();
void Profile__Template3__ctor_1_fn(Profile__Template3* __this, ::g::Profile* parent, ::g::Profile* parentInstance);
void Profile__Template3__New1_fn(Profile__Template3* __this, uObject** __retval);
void Profile__Template3__New2_fn(::g::Profile* parent, ::g::Profile* parentInstance, Profile__Template3** __retval);

struct Profile__Template3 : ::g::Uno::UX::Template
{
    uWeak< ::g::Profile*> __parent1;
    uWeak< ::g::Profile*> __parentInstance1;
    uStrong< ::g::Uno::UX::Property1*> temp_PlaceHolderName_inst;
    static ::g::Uno::UX::Selector __selector0_;
    static ::g::Uno::UX::Selector& __selector0() { return Profile__Template3_typeof()->Init(), __selector0_; }

    void ctor_1(::g::Profile* parent, ::g::Profile* parentInstance);
    static Profile__Template3* New2(::g::Profile* parent, ::g::Profile* parentInstance);
};
// }

} // ::g
