// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.Float4.h>
#include <Uno.UX.Property1-1.h>
namespace g{namespace Uno{namespace UX{struct PropertyObject;}}}
namespace g{struct DropdownMenu;}
namespace g{struct OSP_DropdownMenu_TextColor_Property;}

namespace g{

// internal sealed class OSP_DropdownMenu_TextColor_Property :720
// {
::g::Uno::UX::Property1_type* OSP_DropdownMenu_TextColor_Property_typeof();
void OSP_DropdownMenu_TextColor_Property__Get1_fn(OSP_DropdownMenu_TextColor_Property* __this, ::g::Uno::UX::PropertyObject* obj, ::g::Uno::Float4* __retval);
void OSP_DropdownMenu_TextColor_Property__get_Object_fn(OSP_DropdownMenu_TextColor_Property* __this, ::g::Uno::UX::PropertyObject** __retval);
void OSP_DropdownMenu_TextColor_Property__Set1_fn(OSP_DropdownMenu_TextColor_Property* __this, ::g::Uno::UX::PropertyObject* obj, ::g::Uno::Float4* v, uObject* origin);
void OSP_DropdownMenu_TextColor_Property__get_SupportsOriginSetter_fn(OSP_DropdownMenu_TextColor_Property* __this, bool* __retval);

struct OSP_DropdownMenu_TextColor_Property : ::g::Uno::UX::Property1
{
    uWeak< ::g::DropdownMenu*> _obj;
};
// }

} // ::g
