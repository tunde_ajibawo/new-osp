// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.String.h>
#include <Uno.UX.Property1-1.h>
namespace g{namespace Fuse{namespace Controls{struct TextInput;}}}
namespace g{namespace Uno{namespace UX{struct PropertyObject;}}}
namespace g{namespace Uno{namespace UX{struct Selector;}}}
namespace g{struct OSP_FuseControlsTextInput_PlaceholderText_Property;}

namespace g{

// internal sealed class OSP_FuseControlsTextInput_PlaceholderText_Property :626
// {
::g::Uno::UX::Property1_type* OSP_FuseControlsTextInput_PlaceholderText_Property_typeof();
void OSP_FuseControlsTextInput_PlaceholderText_Property__ctor_3_fn(OSP_FuseControlsTextInput_PlaceholderText_Property* __this, ::g::Fuse::Controls::TextInput* obj, ::g::Uno::UX::Selector* name);
void OSP_FuseControlsTextInput_PlaceholderText_Property__Get1_fn(OSP_FuseControlsTextInput_PlaceholderText_Property* __this, ::g::Uno::UX::PropertyObject* obj, uString** __retval);
void OSP_FuseControlsTextInput_PlaceholderText_Property__New1_fn(::g::Fuse::Controls::TextInput* obj, ::g::Uno::UX::Selector* name, OSP_FuseControlsTextInput_PlaceholderText_Property** __retval);
void OSP_FuseControlsTextInput_PlaceholderText_Property__get_Object_fn(OSP_FuseControlsTextInput_PlaceholderText_Property* __this, ::g::Uno::UX::PropertyObject** __retval);
void OSP_FuseControlsTextInput_PlaceholderText_Property__Set1_fn(OSP_FuseControlsTextInput_PlaceholderText_Property* __this, ::g::Uno::UX::PropertyObject* obj, uString* v, uObject* origin);

struct OSP_FuseControlsTextInput_PlaceholderText_Property : ::g::Uno::UX::Property1
{
    uWeak< ::g::Fuse::Controls::TextInput*> _obj;

    void ctor_3(::g::Fuse::Controls::TextInput* obj, ::g::Uno::UX::Selector name);
    static OSP_FuseControlsTextInput_PlaceholderText_Property* New1(::g::Fuse::Controls::TextInput* obj, ::g::Uno::UX::Selector name);
};
// }

} // ::g
