// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Fuse.Drawing.Brush.h>
#include <Uno.UX.Property1-1.h>
namespace g{namespace Uno{namespace UX{struct PropertyObject;}}}
namespace g{struct DropdownSelectedItem;}
namespace g{struct OSP_DropdownSelectedItem_BorderColor_Property;}

namespace g{

// internal sealed class OSP_DropdownSelectedItem_BorderColor_Property :547
// {
::g::Uno::UX::Property1_type* OSP_DropdownSelectedItem_BorderColor_Property_typeof();
void OSP_DropdownSelectedItem_BorderColor_Property__Get1_fn(OSP_DropdownSelectedItem_BorderColor_Property* __this, ::g::Uno::UX::PropertyObject* obj, ::g::Fuse::Drawing::Brush** __retval);
void OSP_DropdownSelectedItem_BorderColor_Property__get_Object_fn(OSP_DropdownSelectedItem_BorderColor_Property* __this, ::g::Uno::UX::PropertyObject** __retval);
void OSP_DropdownSelectedItem_BorderColor_Property__Set1_fn(OSP_DropdownSelectedItem_BorderColor_Property* __this, ::g::Uno::UX::PropertyObject* obj, ::g::Fuse::Drawing::Brush* v, uObject* origin);
void OSP_DropdownSelectedItem_BorderColor_Property__get_SupportsOriginSetter_fn(OSP_DropdownSelectedItem_BorderColor_Property* __this, bool* __retval);

struct OSP_DropdownSelectedItem_BorderColor_Property : ::g::Uno::UX::Property1
{
    uWeak< ::g::DropdownSelectedItem*> _obj;
};
// }

} // ::g
