// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/Profile.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.UX.Template.h>
namespace g{namespace Uno{namespace UX{struct Property1;}}}
namespace g{namespace Uno{namespace UX{struct Selector;}}}
namespace g{struct Profile;}
namespace g{struct Profile__Template1;}

namespace g{

// public partial sealed class Profile.Template1 :36
// {
::g::Uno::UX::Template_type* Profile__Template1_typeof();
void Profile__Template1__ctor_1_fn(Profile__Template1* __this, ::g::Profile* parent, ::g::Profile* parentInstance);
void Profile__Template1__New1_fn(Profile__Template1* __this, uObject** __retval);
void Profile__Template1__New2_fn(::g::Profile* parent, ::g::Profile* parentInstance, Profile__Template1** __retval);

struct Profile__Template1 : ::g::Uno::UX::Template
{
    uWeak< ::g::Profile*> __parent1;
    uWeak< ::g::Profile*> __parentInstance1;
    uStrong< ::g::Uno::UX::Property1*> __self_PlaceHolderName_inst1;
    static ::g::Uno::UX::Selector __selector0_;
    static ::g::Uno::UX::Selector& __selector0() { return Profile__Template1_typeof()->Init(), __selector0_; }

    void ctor_1(::g::Profile* parent, ::g::Profile* parentInstance);
    static Profile__Template1* New2(::g::Profile* parent, ::g::Profile* parentInstance);
};
// }

} // ::g
