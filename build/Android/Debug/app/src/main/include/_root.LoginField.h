// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/LoginField.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Fuse.Animations.IResize.h>
#include <Fuse.Binding.h>
#include <Fuse.Controls.TextInput.h>
#include <Fuse.IActualPlacement.h>
#include <Fuse.INotifyUnrooted.h>
#include <Fuse.IProperties.h>
#include <Fuse.ISourceLocation.h>
#include <Fuse.ITemplateSource.h>
#include <Fuse.Node.h>
#include <Fuse.Scripting.IScriptObject.h>
#include <Fuse.Triggers.Actions.IHide.h>
#include <Fuse.Triggers.Actions.IShow.h>
#include <Fuse.Triggers.Actions-ea70af1f.h>
#include <Fuse.Triggers.IValue-1.h>
#include <Fuse.Visual.h>
#include <Uno.Collections.ICollection-1.h>
#include <Uno.Collections.IEnumerable-1.h>
#include <Uno.Collections.IList-1.h>
#include <Uno.Float4.h>
#include <Uno.String.h>
#include <Uno.UX.IPropertyListener.h>
namespace g{namespace Uno{namespace UX{struct Property1;}}}
namespace g{namespace Uno{namespace UX{struct Selector;}}}
namespace g{struct LoginField;}

namespace g{

// public partial sealed class LoginField :2
// {
::g::Fuse::Controls::TextInputControl_type* LoginField_typeof();
void LoginField__ctor_8_fn(LoginField* __this);
void LoginField__get_BgColor_fn(LoginField* __this, ::g::Uno::Float4* __retval);
void LoginField__set_BgColor_fn(LoginField* __this, ::g::Uno::Float4* value);
void LoginField__InitializeUX_fn(LoginField* __this);
void LoginField__New4_fn(LoginField** __retval);
void LoginField__get_PlaceHolder_fn(LoginField* __this, uString** __retval);
void LoginField__set_PlaceHolder_fn(LoginField* __this, uString* value);
void LoginField__get_PlaceHolderColor_fn(LoginField* __this, ::g::Uno::Float4* __retval);
void LoginField__set_PlaceHolderColor_fn(LoginField* __this, ::g::Uno::Float4* value);
void LoginField__SetBgColor_fn(LoginField* __this, ::g::Uno::Float4* value, uObject* origin);
void LoginField__SetPlaceHolder_fn(LoginField* __this, uString* value, uObject* origin);
void LoginField__SetPlaceHolderColor_fn(LoginField* __this, ::g::Uno::Float4* value, uObject* origin);
void LoginField__SetStrokeColor_fn(LoginField* __this, ::g::Uno::Float4* value, uObject* origin);
void LoginField__get_StrokeColor_fn(LoginField* __this, ::g::Uno::Float4* __retval);
void LoginField__set_StrokeColor_fn(LoginField* __this, ::g::Uno::Float4* value);

struct LoginField : ::g::Fuse::Controls::TextInput
{
    uStrong<uString*> _field_PlaceHolder;
    ::g::Uno::Float4 _field_PlaceHolderColor;
    ::g::Uno::Float4 _field_BgColor;
    ::g::Uno::Float4 _field_StrokeColor;
    uStrong< ::g::Uno::UX::Property1*> temp_Color_inst;
    uStrong< ::g::Uno::UX::Property1*> temp1_Color_inst;
    uStrong< ::g::Uno::UX::Property1*> this_PlaceholderText_inst;
    uStrong< ::g::Uno::UX::Property1*> this_PlaceholderColor_inst;
    static ::g::Uno::UX::Selector __selector0_;
    static ::g::Uno::UX::Selector& __selector0() { return LoginField_typeof()->Init(), __selector0_; }
    static ::g::Uno::UX::Selector __selector1_;
    static ::g::Uno::UX::Selector& __selector1() { return LoginField_typeof()->Init(), __selector1_; }
    static ::g::Uno::UX::Selector __selector2_;
    static ::g::Uno::UX::Selector& __selector2() { return LoginField_typeof()->Init(), __selector2_; }

    void ctor_8();
    ::g::Uno::Float4 BgColor();
    void BgColor(::g::Uno::Float4 value);
    void InitializeUX();
    uString* PlaceHolder();
    void PlaceHolder(uString* value);
    ::g::Uno::Float4 PlaceHolderColor();
    void PlaceHolderColor(::g::Uno::Float4 value);
    void SetBgColor(::g::Uno::Float4 value, uObject* origin);
    void SetPlaceHolder(uString* value, uObject* origin);
    void SetPlaceHolderColor(::g::Uno::Float4 value, uObject* origin);
    void SetStrokeColor(::g::Uno::Float4 value, uObject* origin);
    ::g::Uno::Float4 StrokeColor();
    void StrokeColor(::g::Uno::Float4 value);
    static LoginField* New4();
};
// }

} // ::g
