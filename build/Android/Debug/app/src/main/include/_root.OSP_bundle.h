// This file was generated based on /Users/tundeajibawo/Downloads/osp/OSP.unoproj.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.h>
namespace g{namespace Uno{namespace IO{struct BundleFile;}}}
namespace g{struct OSP_bundle;}

namespace g{

// public static generated class OSP_bundle :0
// {
uClassType* OSP_bundle_typeof();

struct OSP_bundle : uObject
{
    static uSStrong< ::g::Uno::IO::BundleFile*> AlegreyaSansBoldac83ec20_;
    static uSStrong< ::g::Uno::IO::BundleFile*>& AlegreyaSansBoldac83ec20() { return OSP_bundle_typeof()->Init(), AlegreyaSansBoldac83ec20_; }
    static uSStrong< ::g::Uno::IO::BundleFile*> AlegreyaSansBoldea43f500_;
    static uSStrong< ::g::Uno::IO::BundleFile*>& AlegreyaSansBoldea43f500() { return OSP_bundle_typeof()->Init(), AlegreyaSansBoldea43f500_; }
    static uSStrong< ::g::Uno::IO::BundleFile*> BentonSansMediuma84c7b4c_;
    static uSStrong< ::g::Uno::IO::BundleFile*>& BentonSansMediuma84c7b4c() { return OSP_bundle_typeof()->Init(), BentonSansMediuma84c7b4c_; }
    static uSStrong< ::g::Uno::IO::BundleFile*> Browser48e90536_;
    static uSStrong< ::g::Uno::IO::BundleFile*>& Browser48e90536() { return OSP_bundle_typeof()->Init(), Browser48e90536_; }
    static uSStrong< ::g::Uno::IO::BundleFile*> Chat187ba3a6_;
    static uSStrong< ::g::Uno::IO::BundleFile*>& Chat187ba3a6() { return OSP_bundle_typeof()->Init(), Chat187ba3a6_; }
    static uSStrong< ::g::Uno::IO::BundleFile*> Comment2c226469_;
    static uSStrong< ::g::Uno::IO::BundleFile*>& Comment2c226469() { return OSP_bundle_typeof()->Init(), Comment2c226469_; }
    static uSStrong< ::g::Uno::IO::BundleFile*> diskc6e80153_;
    static uSStrong< ::g::Uno::IO::BundleFile*>& diskc6e80153() { return OSP_bundle_typeof()->Init(), diskc6e80153_; }
    static uSStrong< ::g::Uno::IO::BundleFile*> dprb1594be8_;
    static uSStrong< ::g::Uno::IO::BundleFile*>& dprb1594be8() { return OSP_bundle_typeof()->Init(), dprb1594be8_; }
    static uSStrong< ::g::Uno::IO::BundleFile*> Feeda3231344_;
    static uSStrong< ::g::Uno::IO::BundleFile*>& Feeda3231344() { return OSP_bundle_typeof()->Init(), Feeda3231344_; }
    static uSStrong< ::g::Uno::IO::BundleFile*> FontAwesomef6831785_;
    static uSStrong< ::g::Uno::IO::BundleFile*>& FontAwesomef6831785() { return OSP_bundle_typeof()->Init(), FontAwesomef6831785_; }
    static uSStrong< ::g::Uno::IO::BundleFile*> goback248ef465_;
    static uSStrong< ::g::Uno::IO::BundleFile*>& goback248ef465() { return OSP_bundle_typeof()->Init(), goback248ef465_; }
    static uSStrong< ::g::Uno::IO::BundleFile*> historya6ead22a_;
    static uSStrong< ::g::Uno::IO::BundleFile*>& historya6ead22a() { return OSP_bundle_typeof()->Init(), historya6ead22a_; }
    static uSStrong< ::g::Uno::IO::BundleFile*> home1fe12191_;
    static uSStrong< ::g::Uno::IO::BundleFile*>& home1fe12191() { return OSP_bundle_typeof()->Init(), home1fe12191_; }
    static uSStrong< ::g::Uno::IO::BundleFile*> LatoBold946ea59c_;
    static uSStrong< ::g::Uno::IO::BundleFile*>& LatoBold946ea59c() { return OSP_bundle_typeof()->Init(), LatoBold946ea59c_; }
    static uSStrong< ::g::Uno::IO::BundleFile*> LatoBolde6c51dbc_;
    static uSStrong< ::g::Uno::IO::BundleFile*>& LatoBolde6c51dbc() { return OSP_bundle_typeof()->Init(), LatoBolde6c51dbc_; }
    static uSStrong< ::g::Uno::IO::BundleFile*> LatoRegular2367fb87_;
    static uSStrong< ::g::Uno::IO::BundleFile*>& LatoRegular2367fb87() { return OSP_bundle_typeof()->Init(), LatoRegular2367fb87_; }
    static uSStrong< ::g::Uno::IO::BundleFile*> Likeb029c8b1_;
    static uSStrong< ::g::Uno::IO::BundleFile*>& Likeb029c8b1() { return OSP_bundle_typeof()->Init(), Likeb029c8b1_; }
    static uSStrong< ::g::Uno::IO::BundleFile*> Location77a90067_;
    static uSStrong< ::g::Uno::IO::BundleFile*>& Location77a90067() { return OSP_bundle_typeof()->Init(), Location77a90067_; }
    static uSStrong< ::g::Uno::IO::BundleFile*> navigationb176862e_;
    static uSStrong< ::g::Uno::IO::BundleFile*>& navigationb176862e() { return OSP_bundle_typeof()->Init(), navigationb176862e_; }
    static uSStrong< ::g::Uno::IO::BundleFile*> password9aeb6d35_;
    static uSStrong< ::g::Uno::IO::BundleFile*>& password9aeb6d35() { return OSP_bundle_typeof()->Init(), password9aeb6d35_; }
    static uSStrong< ::g::Uno::IO::BundleFile*> RalewaySemiBold0870d6b1_;
    static uSStrong< ::g::Uno::IO::BundleFile*>& RalewaySemiBold0870d6b1() { return OSP_bundle_typeof()->Init(), RalewaySemiBold0870d6b1_; }
    static uSStrong< ::g::Uno::IO::BundleFile*> RobotoBold69495149_;
    static uSStrong< ::g::Uno::IO::BundleFile*>& RobotoBold69495149() { return OSP_bundle_typeof()->Init(), RobotoBold69495149_; }
    static uSStrong< ::g::Uno::IO::BundleFile*> Sharec2cad64f_;
    static uSStrong< ::g::Uno::IO::BundleFile*>& Sharec2cad64f() { return OSP_bundle_typeof()->Init(), Sharec2cad64f_; }
    static uSStrong< ::g::Uno::IO::BundleFile*> signupb2004114_;
    static uSStrong< ::g::Uno::IO::BundleFile*>& signupb2004114() { return OSP_bundle_typeof()->Init(), signupb2004114_; }
};
// }

} // ::g
