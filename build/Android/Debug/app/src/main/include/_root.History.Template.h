// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/History.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.UX.Template.h>
namespace g{namespace Fuse{namespace Controls{struct Rectangle;}}}
namespace g{namespace Uno{namespace UX{struct Property1;}}}
namespace g{namespace Uno{namespace UX{struct Selector;}}}
namespace g{struct History;}
namespace g{struct History__Template;}

namespace g{

// public partial sealed class History.Template :6
// {
::g::Uno::UX::Template_type* History__Template_typeof();
void History__Template__ctor_1_fn(History__Template* __this, ::g::History* parent, ::g::History* parentInstance);
void History__Template__New1_fn(History__Template* __this, uObject** __retval);
void History__Template__New2_fn(::g::History* parent, ::g::History* parentInstance, History__Template** __retval);

struct History__Template : ::g::Uno::UX::Template
{
    uWeak< ::g::History*> __parent1;
    uWeak< ::g::History*> __parentInstance1;
    uStrong< ::g::Uno::UX::Property1*> temp_Value_inst;
    uStrong< ::g::Uno::UX::Property1*> temp1_Value_inst;
    uStrong< ::g::Uno::UX::Property1*> temp2_Value_inst;
    uStrong< ::g::Uno::UX::Property1*> temp3_Value_inst;
    uStrong< ::g::Uno::UX::Property1*> temp4_Value_inst;
    uStrong< ::g::Fuse::Controls::Rectangle*> item;
    static ::g::Uno::UX::Selector __selector0_;
    static ::g::Uno::UX::Selector& __selector0() { return History__Template_typeof()->Init(), __selector0_; }
    static ::g::Uno::UX::Selector __selector1_;
    static ::g::Uno::UX::Selector& __selector1() { return History__Template_typeof()->Init(), __selector1_; }

    void ctor_1(::g::History* parent, ::g::History* parentInstance);
    static History__Template* New2(::g::History* parent, ::g::History* parentInstance);
};
// }

} // ::g
