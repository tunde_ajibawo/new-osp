// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/SubButton.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Fuse.Animations.IResize.h>
#include <Fuse.Binding.h>
#include <Fuse.Controls.Rectangle.h>
#include <Fuse.Drawing.IDrawObj-d34d045e.h>
#include <Fuse.Drawing.ISurfaceDrawable.h>
#include <Fuse.IActualPlacement.h>
#include <Fuse.INotifyUnrooted.h>
#include <Fuse.IProperties.h>
#include <Fuse.ISourceLocation.h>
#include <Fuse.ITemplateSource.h>
#include <Fuse.Node.h>
#include <Fuse.Scripting.IScriptObject.h>
#include <Fuse.Triggers.Actions.IHide.h>
#include <Fuse.Triggers.Actions.IShow.h>
#include <Fuse.Triggers.Actions-ea70af1f.h>
#include <Fuse.Visual.h>
#include <Uno.Collections.ICollection-1.h>
#include <Uno.Collections.IEnumerable-1.h>
#include <Uno.Collections.IList-1.h>
#include <Uno.Float4.h>
#include <Uno.UX.IPropertyListener.h>
namespace g{namespace Fuse{namespace Controls{struct Image;}}}
namespace g{namespace Fuse{namespace Controls{struct Text;}}}
namespace g{namespace Uno{namespace UX{struct Property1;}}}
namespace g{namespace Uno{namespace UX{struct Selector;}}}
namespace g{struct SubButton;}

namespace g{

// public partial sealed class SubButton :2
// {
::g::Fuse::Controls::Shape_type* SubButton_typeof();
void SubButton__ctor_8_fn(SubButton* __this);
void SubButton__get_ButtonName_fn(SubButton* __this, uString** __retval);
void SubButton__set_ButtonName_fn(SubButton* __this, uString* value);
void SubButton__get_ButtonTextColor_fn(SubButton* __this, ::g::Uno::Float4* __retval);
void SubButton__set_ButtonTextColor_fn(SubButton* __this, ::g::Uno::Float4* value);
void SubButton__InitializeUX_fn(SubButton* __this);
void SubButton__New4_fn(SubButton** __retval);
void SubButton__SetButtonName_fn(SubButton* __this, uString* value, uObject* origin);
void SubButton__SetButtonTextColor_fn(SubButton* __this, ::g::Uno::Float4* value, uObject* origin);

struct SubButton : ::g::Fuse::Controls::Rectangle
{
    uStrong<uString*> _field_ButtonName;
    ::g::Uno::Float4 _field_ButtonTextColor;
    uStrong< ::g::Uno::UX::Property1*> sub_Value_inst;
    uStrong< ::g::Uno::UX::Property1*> sub_TextColor_inst;
    uStrong< ::g::Uno::UX::Property1*> sub_Opacity_inst;
    uStrong< ::g::Uno::UX::Property1*> loadn_Opacity_inst;
    uStrong< ::g::Uno::UX::Property1*> temp_Value_inst;
    uStrong< ::g::Uno::UX::Property1*> temp1_Value_inst;
    uStrong< ::g::Fuse::Controls::Text*> sub;
    uStrong< ::g::Fuse::Controls::Image*> loadn;
    static ::g::Uno::UX::Selector __selector0_;
    static ::g::Uno::UX::Selector& __selector0() { return SubButton_typeof()->Init(), __selector0_; }
    static ::g::Uno::UX::Selector __selector1_;
    static ::g::Uno::UX::Selector& __selector1() { return SubButton_typeof()->Init(), __selector1_; }
    static ::g::Uno::UX::Selector __selector2_;
    static ::g::Uno::UX::Selector& __selector2() { return SubButton_typeof()->Init(), __selector2_; }
    static ::g::Uno::UX::Selector __selector3_;
    static ::g::Uno::UX::Selector& __selector3() { return SubButton_typeof()->Init(), __selector3_; }
    static ::g::Uno::UX::Selector __selector4_;
    static ::g::Uno::UX::Selector& __selector4() { return SubButton_typeof()->Init(), __selector4_; }

    void ctor_8();
    uString* ButtonName();
    void ButtonName(uString* value);
    ::g::Uno::Float4 ButtonTextColor();
    void ButtonTextColor(::g::Uno::Float4 value);
    void InitializeUX();
    void SetButtonName(uString* value, uObject* origin);
    void SetButtonTextColor(::g::Uno::Float4 value, uObject* origin);
    static SubButton* New4();
};
// }

} // ::g
