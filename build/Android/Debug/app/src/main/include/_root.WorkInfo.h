// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/WorkInfo.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Fuse.Animations.IResize.h>
#include <Fuse.Binding.h>
#include <Fuse.Controls.Page.h>
#include <Fuse.Drawing.ISurfaceDrawable.h>
#include <Fuse.IActualPlacement.h>
#include <Fuse.INotifyUnrooted.h>
#include <Fuse.IProperties.h>
#include <Fuse.ISourceLocation.h>
#include <Fuse.ITemplateSource.h>
#include <Fuse.Node.h>
#include <Fuse.Scripting.IScriptObject.h>
#include <Fuse.Triggers.Actions.IHide.h>
#include <Fuse.Triggers.Actions.IShow.h>
#include <Fuse.Triggers.Actions-ea70af1f.h>
#include <Fuse.Visual.h>
#include <Uno.Collections.ICollection-1.h>
#include <Uno.Collections.IEnumerable-1.h>
#include <Uno.Collections.IList-1.h>
#include <Uno.Float4.h>
#include <Uno.UX.IPropertyListener.h>
namespace g{namespace Fuse{namespace Controls{struct Image;}}}
namespace g{namespace Fuse{namespace Controls{struct PageControl;}}}
namespace g{namespace Fuse{namespace Controls{struct Panel;}}}
namespace g{namespace Fuse{namespace Controls{struct ScrollView;}}}
namespace g{namespace Fuse{namespace Controls{struct StackPanel;}}}
namespace g{namespace Fuse{namespace Navigation{struct Router;}}}
namespace g{namespace Fuse{namespace Reactive{struct EventBinding;}}}
namespace g{namespace Fuse{namespace Triggers{struct Busy;}}}
namespace g{namespace Uno{namespace UX{struct NameTable;}}}
namespace g{namespace Uno{namespace UX{struct Property1;}}}
namespace g{namespace Uno{namespace UX{struct Selector;}}}
namespace g{struct Tab;}
namespace g{struct WorkInfo;}

namespace g{

// public partial sealed class WorkInfo :2
// {
::g::Fuse::Controls::Panel_type* WorkInfo_typeof();
void WorkInfo__ctor_8_fn(WorkInfo* __this, ::g::Fuse::Navigation::Router* router1);
void WorkInfo__InitializeUX_fn(WorkInfo* __this);
void WorkInfo__New5_fn(::g::Fuse::Navigation::Router* router1, WorkInfo** __retval);

struct WorkInfo : ::g::Fuse::Controls::Page
{
    uStrong< ::g::Fuse::Navigation::Router*> router;
    uStrong< ::g::Uno::UX::Property1*> navigation_Active_inst;
    uStrong< ::g::Uno::UX::Property1*> permit_Color_inst;
    uStrong< ::g::Uno::UX::Property1*> permit_TColor_inst;
    uStrong< ::g::Uno::UX::Property1*> loadingPanel_Opacity_inst;
    uStrong< ::g::Uno::UX::Property1*> WorkPage_Opacity_inst;
    uStrong< ::g::Uno::UX::Property1*> temp_Value_inst;
    uStrong< ::g::Uno::UX::Property1*> load_Opacity_inst;
    uStrong< ::g::Uno::UX::Property1*> errorMsg_Opacity_inst;
    uStrong< ::g::Uno::UX::Property1*> temp1_Value_inst;
    uStrong< ::g::Uno::UX::Property1*> temp2_Value_inst;
    uStrong< ::g::Uno::UX::Property1*> temp3_Items_inst;
    uStrong< ::g::Uno::UX::Property1*> train_Color_inst;
    uStrong< ::g::Uno::UX::Property1*> train_TColor_inst;
    uStrong< ::g::Uno::UX::Property1*> loadingPanel1_Opacity_inst;
    uStrong< ::g::Uno::UX::Property1*> WorkPage1_Opacity_inst;
    uStrong< ::g::Uno::UX::Property1*> temp4_Value_inst;
    uStrong< ::g::Uno::UX::Property1*> load1_Opacity_inst;
    uStrong< ::g::Uno::UX::Property1*> errorMsg1_Opacity_inst;
    uStrong< ::g::Uno::UX::Property1*> temp5_Value_inst;
    uStrong< ::g::Uno::UX::Property1*> temp6_Value_inst;
    uStrong< ::g::Uno::UX::Property1*> temp7_Items_inst;
    uStrong< ::g::Uno::UX::Property1*> medical_Color_inst;
    uStrong< ::g::Uno::UX::Property1*> medical_TColor_inst;
    uStrong< ::g::Uno::UX::Property1*> loadingPanel2_Opacity_inst;
    uStrong< ::g::Uno::UX::Property1*> WorkPage2_Opacity_inst;
    uStrong< ::g::Uno::UX::Property1*> temp8_Value_inst;
    uStrong< ::g::Uno::UX::Property1*> load2_Opacity_inst;
    uStrong< ::g::Uno::UX::Property1*> errorMsg2_Opacity_inst;
    uStrong< ::g::Uno::UX::Property1*> temp9_Value_inst;
    uStrong< ::g::Uno::UX::Property1*> temp10_Value_inst;
    uStrong< ::g::Uno::UX::Property1*> temp11_Items_inst;
    uStrong< ::g::Fuse::Controls::Panel*> page1Tab;
    uStrong< ::g::Tab*> permit;
    uStrong< ::g::Fuse::Controls::Panel*> page2Tab;
    uStrong< ::g::Tab*> train;
    uStrong< ::g::Fuse::Reactive::EventBinding*> temp_eb26;
    uStrong< ::g::Fuse::Controls::Panel*> page3Tab;
    uStrong< ::g::Tab*> medical;
    uStrong< ::g::Fuse::Controls::PageControl*> navigation;
    uStrong< ::g::Fuse::Controls::Page*> page1;
    uStrong< ::g::Fuse::Controls::Panel*> loadingPanel;
    uStrong< ::g::Fuse::Controls::Image*> load;
    uStrong< ::g::Fuse::Controls::StackPanel*> errorMsg;
    uStrong< ::g::Fuse::Reactive::EventBinding*> temp_eb27;
    uStrong< ::g::Fuse::Reactive::EventBinding*> temp_eb28;
    uStrong< ::g::Fuse::Reactive::EventBinding*> temp_eb29;
    uStrong< ::g::Fuse::Triggers::Busy*> busy;
    uStrong< ::g::Fuse::Controls::ScrollView*> WorkPage;
    uStrong< ::g::Fuse::Controls::Page*> page2;
    uStrong< ::g::Fuse::Controls::Panel*> loadingPanel1;
    uStrong< ::g::Fuse::Controls::Image*> load1;
    uStrong< ::g::Fuse::Controls::StackPanel*> errorMsg1;
    uStrong< ::g::Fuse::Reactive::EventBinding*> temp_eb30;
    uStrong< ::g::Fuse::Reactive::EventBinding*> temp_eb31;
    uStrong< ::g::Fuse::Reactive::EventBinding*> temp_eb32;
    uStrong< ::g::Fuse::Triggers::Busy*> busy1;
    uStrong< ::g::Fuse::Controls::ScrollView*> WorkPage1;
    uStrong< ::g::Fuse::Controls::Page*> page3;
    uStrong< ::g::Fuse::Controls::Panel*> loadingPanel2;
    uStrong< ::g::Fuse::Controls::Image*> load2;
    uStrong< ::g::Fuse::Controls::StackPanel*> errorMsg2;
    uStrong< ::g::Fuse::Reactive::EventBinding*> temp_eb33;
    uStrong< ::g::Fuse::Reactive::EventBinding*> temp_eb34;
    uStrong< ::g::Fuse::Reactive::EventBinding*> temp_eb35;
    uStrong< ::g::Fuse::Triggers::Busy*> busy2;
    uStrong< ::g::Fuse::Controls::ScrollView*> WorkPage2;
    uStrong< ::g::Uno::UX::NameTable*> __g_nametable1;
    static uSStrong<uArray*> __g_static_nametable1_;
    static uSStrong<uArray*>& __g_static_nametable1() { return WorkInfo_typeof()->Init(), __g_static_nametable1_; }
    static ::g::Uno::UX::Selector __selector0_;
    static ::g::Uno::UX::Selector& __selector0() { return WorkInfo_typeof()->Init(), __selector0_; }
    static ::g::Uno::UX::Selector __selector1_;
    static ::g::Uno::UX::Selector& __selector1() { return WorkInfo_typeof()->Init(), __selector1_; }
    static ::g::Uno::UX::Selector __selector2_;
    static ::g::Uno::UX::Selector& __selector2() { return WorkInfo_typeof()->Init(), __selector2_; }
    static ::g::Uno::UX::Selector __selector3_;
    static ::g::Uno::UX::Selector& __selector3() { return WorkInfo_typeof()->Init(), __selector3_; }
    static ::g::Uno::UX::Selector __selector4_;
    static ::g::Uno::UX::Selector& __selector4() { return WorkInfo_typeof()->Init(), __selector4_; }
    static ::g::Uno::UX::Selector __selector5_;
    static ::g::Uno::UX::Selector& __selector5() { return WorkInfo_typeof()->Init(), __selector5_; }
    static ::g::Uno::UX::Selector __selector6_;
    static ::g::Uno::UX::Selector& __selector6() { return WorkInfo_typeof()->Init(), __selector6_; }
    static ::g::Uno::UX::Selector __selector7_;
    static ::g::Uno::UX::Selector& __selector7() { return WorkInfo_typeof()->Init(), __selector7_; }
    static ::g::Uno::UX::Selector __selector8_;
    static ::g::Uno::UX::Selector& __selector8() { return WorkInfo_typeof()->Init(), __selector8_; }
    static ::g::Uno::UX::Selector __selector9_;
    static ::g::Uno::UX::Selector& __selector9() { return WorkInfo_typeof()->Init(), __selector9_; }
    static ::g::Uno::UX::Selector __selector10_;
    static ::g::Uno::UX::Selector& __selector10() { return WorkInfo_typeof()->Init(), __selector10_; }
    static ::g::Uno::UX::Selector __selector11_;
    static ::g::Uno::UX::Selector& __selector11() { return WorkInfo_typeof()->Init(), __selector11_; }
    static ::g::Uno::UX::Selector __selector12_;
    static ::g::Uno::UX::Selector& __selector12() { return WorkInfo_typeof()->Init(), __selector12_; }
    static ::g::Uno::UX::Selector __selector13_;
    static ::g::Uno::UX::Selector& __selector13() { return WorkInfo_typeof()->Init(), __selector13_; }
    static ::g::Uno::UX::Selector __selector14_;
    static ::g::Uno::UX::Selector& __selector14() { return WorkInfo_typeof()->Init(), __selector14_; }
    static ::g::Uno::UX::Selector __selector15_;
    static ::g::Uno::UX::Selector& __selector15() { return WorkInfo_typeof()->Init(), __selector15_; }
    static ::g::Uno::UX::Selector __selector16_;
    static ::g::Uno::UX::Selector& __selector16() { return WorkInfo_typeof()->Init(), __selector16_; }
    static ::g::Uno::UX::Selector __selector17_;
    static ::g::Uno::UX::Selector& __selector17() { return WorkInfo_typeof()->Init(), __selector17_; }
    static ::g::Uno::UX::Selector __selector18_;
    static ::g::Uno::UX::Selector& __selector18() { return WorkInfo_typeof()->Init(), __selector18_; }
    static ::g::Uno::UX::Selector __selector19_;
    static ::g::Uno::UX::Selector& __selector19() { return WorkInfo_typeof()->Init(), __selector19_; }
    static ::g::Uno::UX::Selector __selector20_;
    static ::g::Uno::UX::Selector& __selector20() { return WorkInfo_typeof()->Init(), __selector20_; }
    static ::g::Uno::UX::Selector __selector21_;
    static ::g::Uno::UX::Selector& __selector21() { return WorkInfo_typeof()->Init(), __selector21_; }
    static ::g::Uno::UX::Selector __selector22_;
    static ::g::Uno::UX::Selector& __selector22() { return WorkInfo_typeof()->Init(), __selector22_; }
    static ::g::Uno::UX::Selector __selector23_;
    static ::g::Uno::UX::Selector& __selector23() { return WorkInfo_typeof()->Init(), __selector23_; }
    static ::g::Uno::UX::Selector __selector24_;
    static ::g::Uno::UX::Selector& __selector24() { return WorkInfo_typeof()->Init(), __selector24_; }
    static ::g::Uno::UX::Selector __selector25_;
    static ::g::Uno::UX::Selector& __selector25() { return WorkInfo_typeof()->Init(), __selector25_; }
    static ::g::Uno::UX::Selector __selector26_;
    static ::g::Uno::UX::Selector& __selector26() { return WorkInfo_typeof()->Init(), __selector26_; }
    static ::g::Uno::UX::Selector __selector27_;
    static ::g::Uno::UX::Selector& __selector27() { return WorkInfo_typeof()->Init(), __selector27_; }
    static ::g::Uno::UX::Selector __selector28_;
    static ::g::Uno::UX::Selector& __selector28() { return WorkInfo_typeof()->Init(), __selector28_; }
    static ::g::Uno::UX::Selector __selector29_;
    static ::g::Uno::UX::Selector& __selector29() { return WorkInfo_typeof()->Init(), __selector29_; }
    static ::g::Uno::UX::Selector __selector30_;
    static ::g::Uno::UX::Selector& __selector30() { return WorkInfo_typeof()->Init(), __selector30_; }

    void ctor_8(::g::Fuse::Navigation::Router* router1);
    void InitializeUX();
    static WorkInfo* New5(::g::Fuse::Navigation::Router* router1);
};
// }

} // ::g
