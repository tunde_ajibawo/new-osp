// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.Float.h>
#include <Uno.UX.Property1-1.h>
namespace g{namespace Uno{namespace UX{struct PropertyObject;}}}
namespace g{struct DropdownSelectedItem;}
namespace g{struct OSP_DropdownSelectedItem_FontSize_Property;}

namespace g{

// internal sealed class OSP_DropdownSelectedItem_FontSize_Property :520
// {
::g::Uno::UX::Property1_type* OSP_DropdownSelectedItem_FontSize_Property_typeof();
void OSP_DropdownSelectedItem_FontSize_Property__Get1_fn(OSP_DropdownSelectedItem_FontSize_Property* __this, ::g::Uno::UX::PropertyObject* obj, float* __retval);
void OSP_DropdownSelectedItem_FontSize_Property__get_Object_fn(OSP_DropdownSelectedItem_FontSize_Property* __this, ::g::Uno::UX::PropertyObject** __retval);
void OSP_DropdownSelectedItem_FontSize_Property__Set1_fn(OSP_DropdownSelectedItem_FontSize_Property* __this, ::g::Uno::UX::PropertyObject* obj, float* v, uObject* origin);
void OSP_DropdownSelectedItem_FontSize_Property__get_SupportsOriginSetter_fn(OSP_DropdownSelectedItem_FontSize_Property* __this, bool* __retval);

struct OSP_DropdownSelectedItem_FontSize_Property : ::g::Uno::UX::Property1
{
    uWeak< ::g::DropdownSelectedItem*> _obj;
};
// }

} // ::g
