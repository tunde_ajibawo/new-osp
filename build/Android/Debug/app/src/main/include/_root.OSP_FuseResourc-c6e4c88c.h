// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.String.h>
#include <Uno.UX.Property1-1.h>
namespace g{namespace Fuse{namespace Resources{struct HttpImageSource;}}}
namespace g{namespace Uno{namespace UX{struct PropertyObject;}}}
namespace g{namespace Uno{namespace UX{struct Selector;}}}
namespace g{struct OSP_FuseResourcesHttpImageSource_Url_Property;}

namespace g{

// internal sealed class OSP_FuseResourcesHttpImageSource_Url_Property :676
// {
::g::Uno::UX::Property1_type* OSP_FuseResourcesHttpImageSource_Url_Property_typeof();
void OSP_FuseResourcesHttpImageSource_Url_Property__ctor_3_fn(OSP_FuseResourcesHttpImageSource_Url_Property* __this, ::g::Fuse::Resources::HttpImageSource* obj, ::g::Uno::UX::Selector* name);
void OSP_FuseResourcesHttpImageSource_Url_Property__Get1_fn(OSP_FuseResourcesHttpImageSource_Url_Property* __this, ::g::Uno::UX::PropertyObject* obj, uString** __retval);
void OSP_FuseResourcesHttpImageSource_Url_Property__New1_fn(::g::Fuse::Resources::HttpImageSource* obj, ::g::Uno::UX::Selector* name, OSP_FuseResourcesHttpImageSource_Url_Property** __retval);
void OSP_FuseResourcesHttpImageSource_Url_Property__get_Object_fn(OSP_FuseResourcesHttpImageSource_Url_Property* __this, ::g::Uno::UX::PropertyObject** __retval);
void OSP_FuseResourcesHttpImageSource_Url_Property__Set1_fn(OSP_FuseResourcesHttpImageSource_Url_Property* __this, ::g::Uno::UX::PropertyObject* obj, uString* v, uObject* origin);

struct OSP_FuseResourcesHttpImageSource_Url_Property : ::g::Uno::UX::Property1
{
    uWeak< ::g::Fuse::Resources::HttpImageSource*> _obj;

    void ctor_3(::g::Fuse::Resources::HttpImageSource* obj, ::g::Uno::UX::Selector name);
    static OSP_FuseResourcesHttpImageSource_Url_Property* New1(::g::Fuse::Resources::HttpImageSource* obj, ::g::Uno::UX::Selector name);
};
// }

} // ::g
