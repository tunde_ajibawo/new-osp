// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.String.h>
#include <Uno.UX.Property1-1.h>
namespace g{namespace Uno{namespace UX{struct PropertyObject;}}}
namespace g{struct DropdownMenu;}
namespace g{struct OSP_DropdownMenu_Selected_Property;}

namespace g{

// internal sealed class OSP_DropdownMenu_Selected_Property :502
// {
::g::Uno::UX::Property1_type* OSP_DropdownMenu_Selected_Property_typeof();
void OSP_DropdownMenu_Selected_Property__Get1_fn(OSP_DropdownMenu_Selected_Property* __this, ::g::Uno::UX::PropertyObject* obj, uString** __retval);
void OSP_DropdownMenu_Selected_Property__get_Object_fn(OSP_DropdownMenu_Selected_Property* __this, ::g::Uno::UX::PropertyObject** __retval);
void OSP_DropdownMenu_Selected_Property__Set1_fn(OSP_DropdownMenu_Selected_Property* __this, ::g::Uno::UX::PropertyObject* obj, uString* v, uObject* origin);
void OSP_DropdownMenu_Selected_Property__get_SupportsOriginSetter_fn(OSP_DropdownMenu_Selected_Property* __this, bool* __retval);

struct OSP_DropdownMenu_Selected_Property : ::g::Uno::UX::Property1
{
    uWeak< ::g::DropdownMenu*> _obj;
};
// }

} // ::g
