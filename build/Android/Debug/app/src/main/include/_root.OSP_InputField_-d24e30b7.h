// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.String.h>
#include <Uno.UX.Property1-1.h>
namespace g{namespace Uno{namespace UX{struct PropertyObject;}}}
namespace g{namespace Uno{namespace UX{struct Selector;}}}
namespace g{struct InputField;}
namespace g{struct OSP_InputField_PlaceHolderName_Property;}

namespace g{

// internal sealed class OSP_InputField_PlaceHolderName_Property :684
// {
::g::Uno::UX::Property1_type* OSP_InputField_PlaceHolderName_Property_typeof();
void OSP_InputField_PlaceHolderName_Property__ctor_3_fn(OSP_InputField_PlaceHolderName_Property* __this, ::g::InputField* obj, ::g::Uno::UX::Selector* name);
void OSP_InputField_PlaceHolderName_Property__Get1_fn(OSP_InputField_PlaceHolderName_Property* __this, ::g::Uno::UX::PropertyObject* obj, uString** __retval);
void OSP_InputField_PlaceHolderName_Property__New1_fn(::g::InputField* obj, ::g::Uno::UX::Selector* name, OSP_InputField_PlaceHolderName_Property** __retval);
void OSP_InputField_PlaceHolderName_Property__get_Object_fn(OSP_InputField_PlaceHolderName_Property* __this, ::g::Uno::UX::PropertyObject** __retval);
void OSP_InputField_PlaceHolderName_Property__Set1_fn(OSP_InputField_PlaceHolderName_Property* __this, ::g::Uno::UX::PropertyObject* obj, uString* v, uObject* origin);
void OSP_InputField_PlaceHolderName_Property__get_SupportsOriginSetter_fn(OSP_InputField_PlaceHolderName_Property* __this, bool* __retval);

struct OSP_InputField_PlaceHolderName_Property : ::g::Uno::UX::Property1
{
    uWeak< ::g::InputField*> _obj;

    void ctor_3(::g::InputField* obj, ::g::Uno::UX::Selector name);
    static OSP_InputField_PlaceHolderName_Property* New1(::g::InputField* obj, ::g::Uno::UX::Selector name);
};
// }

} // ::g
