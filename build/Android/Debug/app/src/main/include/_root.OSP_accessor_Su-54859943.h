// This file was generated based on /Users/tundeajibawo/Downloads/osp/.uno/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.UX.PropertyAccessor.h>
namespace g{namespace Uno{namespace UX{struct PropertyObject;}}}
namespace g{namespace Uno{namespace UX{struct Selector;}}}
namespace g{struct OSP_accessor_SubBut_Direction;}

namespace g{

// internal sealed class OSP_accessor_SubBut_Direction :271
// {
::g::Uno::UX::PropertyAccessor_type* OSP_accessor_SubBut_Direction_typeof();
void OSP_accessor_SubBut_Direction__ctor_1_fn(OSP_accessor_SubBut_Direction* __this);
void OSP_accessor_SubBut_Direction__GetAsObject_fn(OSP_accessor_SubBut_Direction* __this, ::g::Uno::UX::PropertyObject* obj, uObject** __retval);
void OSP_accessor_SubBut_Direction__get_Name_fn(OSP_accessor_SubBut_Direction* __this, ::g::Uno::UX::Selector* __retval);
void OSP_accessor_SubBut_Direction__New1_fn(OSP_accessor_SubBut_Direction** __retval);
void OSP_accessor_SubBut_Direction__get_PropertyType_fn(OSP_accessor_SubBut_Direction* __this, uType** __retval);
void OSP_accessor_SubBut_Direction__SetAsObject_fn(OSP_accessor_SubBut_Direction* __this, ::g::Uno::UX::PropertyObject* obj, uObject* v, uObject* origin);
void OSP_accessor_SubBut_Direction__get_SupportsOriginSetter_fn(OSP_accessor_SubBut_Direction* __this, bool* __retval);

struct OSP_accessor_SubBut_Direction : ::g::Uno::UX::PropertyAccessor
{
    static uSStrong< ::g::Uno::UX::PropertyAccessor*> Singleton_;
    static uSStrong< ::g::Uno::UX::PropertyAccessor*>& Singleton() { return OSP_accessor_SubBut_Direction_typeof()->Init(), Singleton_; }
    static ::g::Uno::UX::Selector _name_;
    static ::g::Uno::UX::Selector& _name() { return OSP_accessor_SubBut_Direction_typeof()->Init(), _name_; }

    void ctor_1();
    static OSP_accessor_SubBut_Direction* New1();
};
// }

} // ::g
