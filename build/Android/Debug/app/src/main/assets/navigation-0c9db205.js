//console.log("I called this one");
var Timer = require("FuseJS/Timer");
//var login = require("js_files/login");
var API = require('js_files/api');
var Observable = require("FuseJS/Observable");
//var Storage = require("FuseJS/Storage");

var LoaderOpacity = Observable(1);
var PageOpacity = Observable(0);
var status = Observable(false);
var msg = Observable();
var msgColor = Observable();
var visible = Observable(false);

var OSPID = Observable();
var password = Observable();

var personnel_id;


Timer.create(function () {
    //console.log("This will run once, after 3 seconds");
   
    LoaderOpacity.value = 0;
    PageOpacity.value = 1;
}, 3000, false);



function login_val() {

    visible.value = true;
    
    API.auth(OSPID.value, password)
        .then(function(response){
            obj = JSON.parse(response._bodyText);
            if(response.ok)
            {
                
                status.value = true;
                msgColor.value = "#2AFFAA";
                msg.value = "";
                visible.value = false;
                API.saveOSPID(obj);
                router.goto("dashboard");
            }
            else{
                status.value = false;
                    msgColor.value = "#ff7f7f";
                    msg.value = obj["msg"];
                    visible.value = false;
            }
        }).catch(function(err){
                    status.value = false;
                    msgColor.value = "#ff7f7f";
                    msg.value = err;
                    console.log(err);
                    visible.value = false;
        });



    
    
    
   /**
    if (user == null) {

            //console.log('user does not exit');
            API.auth(OSPID.value)
            .then(function (response) {
                ok = response.ok;
                var obj = JSON.parse(response._bodyText);
                //console.dir(obj.history[0].arrival_location);
                //http status 200
                console.dir(response)
                if (ok) {
                    
                    status.value = true;
                    msgColor.value = "#2AFFAA";
                    msg.value = "";
                    visible.value = false;
                    personnel = obj.personnel[0];
                    permits = obj.permits;
                    histry = obj.history;
                    training = obj.training;
                    picture = obj.picture;
                    //console.log(JSON.stringify (permits));
                    API.saveUserID(personnel);
                    API.savePicture(picture, OSPID.value);
                    API.savePermits(permits);
                    API.saveHistory(histry);
                    API.saveTraining(training);
                    API.saveOSPID(OSPID.value);
                    router.push("dashboard");
                } 
                //http status other than 200
                else {
                    status.value = false;
                    msgColor.value = "#ff7f7f";
                    msg.value = obj["msg"];
                    visible.value = false;
                }
            })
            .catch(function (error) {
                console.log(JSON.stringify(error));
            });
            
            
        }
    else {
        //console.dir(JSON.stringify (user));
        //console.dir(API.readPermits(OSPID.value));
        status.value = true;
        msgColor.value = "#2AFFAA";
        msg.value = "";
        visible.value = false;
        API.saveOSPID(OSPID.value);
        //console.log(API.readOSPID());
        //API.deleteOSPID();
        //console.log(API.readOSPID());
        //dashboard(user);
        router.push("dashboard");
    }**/

}

function logout() {
    var userID = ""
    console.log("log out was callee");
    //API.saveUserID("");
    msg.value = null;
    msgColor.value = null;
    visible.value = true;
    status.value = false;

    //console.log(login.OSPID.value);

}

function home() {
    router.goto("home");
}

function dashboard() {
    router.goto("dashboard");
}

function capture() {
    router.goto("capture");
}

function history() {
    router.goto("history");
}

function profile() {
    //                  a console.log(OSPID.value);
    //console.log('Hey HI')
    router.push("profile");
}

function pictureUpload() {
    router.goto("picture");
}

function workinfo() {
    router.push("workinfo");
}

function swipecard() {
    router.goto("swipecard");
}

function notification() {
    router.push("notification");
}

function news() {
    router.push("news");
}

function request() {
    router.goto("request");
}

function forgotPassword() {
    router.goto("password");
}

function signup() {
    router.goto("signup");
}

function goBack() {
    router.goto("dashboard");
}

module.exports = {
    home: home,
    dashboard: dashboard,
    capture: capture,
    history: history,
    profile: profile,
    request: request,
    workinfo: workinfo,
    swipecard: swipecard,
    notification: notification,
    news: news,
    forgotPassword: forgotPassword,
    signup: signup,
    pictureUpload: pictureUpload,
    goBack: goBack,

    LoaderOpacity: LoaderOpacity,
    PageOpacity: PageOpacity,
    visible: visible,
    status: status,
    msg: msg,
    msgColor: msgColor,
    login_val: login_val,
    OSPID: OSPID,
    password: password,
    logout: logout
};