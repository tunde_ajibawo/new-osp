var Timer = require("FuseJS/Timer");
var Observable = require("FuseJS/Observable");
var visible = Observable(false);
var API = require('js_files/api');
var OSPID = Observable();

function goBack() {
	router.push("home");
}
function getPassword() {
	 
	Timer.create(function() {
	     console.log("This will run once, after 3 seconds");
	     visible.value = true;
	 }, 1000, false);
  
	 API.reset(OSPID.value)
        .then(function(response){
            obj = JSON.parse(response._bodyText);
            if(response.ok)
            {
                router.goto("home");
            }
            else{
                console.log(JSON.stringify(response));
            }
        }).catch(function(err){
                    
                    console.log(err);
                    
        });
}

module.exports = {
	OSPID: OSPID,
	visible: visible,
	goBack: goBack,
	getPassword: getPassword
};