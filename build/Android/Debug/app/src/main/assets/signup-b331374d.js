var Observable = require("FuseJS/Observable");
var API = require('js_files/api');
var visible = Observable(false);
var OSPID = Observable();
var password = Observable();
var email = Observable();

function goBack() {
	router.push("home");
}

function signup() {
	
	API.signup(OSPID.value, email.value, password.value)
        .then(function(response){
			
			//there is a 200 response
			if(response.ok)
            {
				Timer.create(function() {
					console.log("This will run once, after 3 seconds");
					visible.value = true;
				}, 1000, false);
				router.goto('home');
			}
			else{
				debug_log(JSON.stringify(response));
			}
        }).catch(function(err){
                  console.log(err);
        });
	
}

module.exports = {
	signup: signup,
	OSPID: OSPID,
	email: email,
	visible: visible,
	password: password,
	goBack: goBack
};

