[Uno.Compiler.UxGenerated]
public partial class Dashboard: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    global::Uno.UX.Property<float> mainAppTranslation_X_inst;
    global::Uno.UX.Property<float> topMenuTranslation_Y_inst;
    global::Uno.UX.Property<float> bottomMenuTranslation_Y_inst;
    global::Uno.UX.Property<float> middleRectangle_Opacity_inst;
    global::Uno.UX.Property<float> topMenuRotation_Degrees_inst;
    global::Uno.UX.Property<float> bottomMenuRotation_Degrees_inst;
    global::Uno.UX.Property<Uno.UX.Size> topRectangle_Width_inst;
    global::Uno.UX.Property<Uno.UX.Size> bottomRectangle_Width_inst;
    global::Uno.UX.Property<bool> temp_Value_inst;
    global::Uno.UX.Property<bool> temp1_Value_inst;
    global::Uno.UX.Property<object> temp2_Items_inst;
    global::Uno.UX.Property<object> temp3_Items_inst;
    global::Uno.UX.Property<object> temp4_Items_inst;
    internal global::Fuse.Controls.EdgeNavigator EdgeNavigator;
    internal global::Sidebar menu;
    internal global::Fuse.Reactive.EventBinding temp_eb7;
    internal global::Fuse.Reactive.EventBinding temp_eb8;
    internal global::Fuse.Controls.DockPanel content;
    internal global::Fuse.Translation mainAppTranslation;
    internal global::Fuse.Controls.Rectangle topRectangle;
    internal global::Fuse.Translation topMenuTranslation;
    internal global::Fuse.Rotation topMenuRotation;
    internal global::Fuse.Controls.Rectangle middleRectangle;
    internal global::Fuse.Controls.Rectangle bottomRectangle;
    internal global::Fuse.Translation bottomMenuTranslation;
    internal global::Fuse.Rotation bottomMenuRotation;
    internal global::Fuse.Controls.ScrollView parentScrollView;
    internal global::Fuse.Reactive.EventBinding temp_eb9;
    internal global::Fuse.Reactive.EventBinding temp_eb10;
    internal global::Fuse.Reactive.EventBinding temp_eb11;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "EdgeNavigator",
        "menu",
        "temp_eb7",
        "temp_eb8",
        "content",
        "mainAppTranslation",
        "topRectangle",
        "topMenuTranslation",
        "topMenuRotation",
        "middleRectangle",
        "bottomRectangle",
        "bottomMenuTranslation",
        "bottomMenuRotation",
        "parentScrollView",
        "temp_eb9",
        "temp_eb10",
        "temp_eb11"
    };
    static Dashboard()
    {
    }
    [global::Uno.UX.UXConstructor]
    public Dashboard(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        mainAppTranslation = new global::Fuse.Translation();
        mainAppTranslation_X_inst = new OSP_FuseTranslation_X_Property(mainAppTranslation, __selector0);
        topMenuTranslation = new global::Fuse.Translation();
        topMenuTranslation_Y_inst = new OSP_FuseTranslation_Y_Property(topMenuTranslation, __selector1);
        bottomMenuTranslation = new global::Fuse.Translation();
        bottomMenuTranslation_Y_inst = new OSP_FuseTranslation_Y_Property(bottomMenuTranslation, __selector1);
        middleRectangle = new global::Fuse.Controls.Rectangle();
        middleRectangle_Opacity_inst = new OSP_FuseElementsElement_Opacity_Property(middleRectangle, __selector2);
        topMenuRotation = new global::Fuse.Rotation();
        topMenuRotation_Degrees_inst = new OSP_FuseRotation_Degrees_Property(topMenuRotation, __selector3);
        bottomMenuRotation = new global::Fuse.Rotation();
        bottomMenuRotation_Degrees_inst = new OSP_FuseRotation_Degrees_Property(bottomMenuRotation, __selector3);
        topRectangle = new global::Fuse.Controls.Rectangle();
        topRectangle_Width_inst = new OSP_FuseElementsElement_Width_Property(topRectangle, __selector4);
        bottomRectangle = new global::Fuse.Controls.Rectangle();
        bottomRectangle_Width_inst = new OSP_FuseElementsElement_Width_Property(bottomRectangle, __selector4);
        var temp5 = new global::Fuse.Reactive.Data("setSidebarOpen");
        var temp6 = new global::Fuse.Reactive.Data("setSidebarClosed");
        var temp = new global::Fuse.Triggers.WhileTrue();
        temp_Value_inst = new OSP_FuseTriggersWhileBool_Value_Property(temp, __selector5);
        var temp7 = new global::Fuse.Reactive.Data("sidebarOpen");
        var temp1 = new global::Fuse.Triggers.WhileFalse();
        temp1_Value_inst = new OSP_FuseTriggersWhileBool_Value_Property(temp1, __selector5);
        var temp8 = new global::Fuse.Reactive.Data("sidebarOpen");
        parentScrollView = new global::Fuse.Controls.ScrollView();
        var temp2 = new global::SortableList(parentScrollView);
        temp2_Items_inst = new OSP_SortableList_Items_Property(temp2, __selector6);
        var temp9 = new global::Fuse.Reactive.Data("morning");
        var temp10 = new global::Fuse.Reactive.Data("notification");
        var temp3 = new global::SortableList(parentScrollView);
        temp3_Items_inst = new OSP_SortableList_Items_Property(temp3, __selector6);
        var temp11 = new global::Fuse.Reactive.Data("day");
        var temp12 = new global::Fuse.Reactive.Data("workinfo");
        var temp4 = new global::SortableList(parentScrollView);
        temp4_Items_inst = new OSP_SortableList_Items_Property(temp4, __selector6);
        var temp13 = new global::Fuse.Reactive.Data("evening");
        var temp14 = new global::Fuse.Reactive.Data("news");
        var temp15 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp16 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        EdgeNavigator = new global::Fuse.Controls.EdgeNavigator();
        menu = new global::Sidebar();
        var temp17 = new global::Fuse.Navigation.ActivatingAnimation();
        var temp18 = new global::Fuse.Animations.Change<float>(mainAppTranslation_X_inst);
        var temp19 = new global::Fuse.Animations.Change<float>(topMenuTranslation_Y_inst);
        var temp20 = new global::Fuse.Animations.Change<float>(bottomMenuTranslation_Y_inst);
        var temp21 = new global::Fuse.Animations.Change<float>(middleRectangle_Opacity_inst);
        var temp22 = new global::Fuse.Animations.Change<float>(topMenuRotation_Degrees_inst);
        var temp23 = new global::Fuse.Animations.Change<float>(bottomMenuRotation_Degrees_inst);
        var temp24 = new global::Fuse.Animations.Change<Uno.UX.Size>(topRectangle_Width_inst);
        var temp25 = new global::Fuse.Animations.Change<Uno.UX.Size>(bottomRectangle_Width_inst);
        var temp26 = new global::Fuse.Navigation.WhileActive();
        var temp27 = new global::Fuse.Triggers.Actions.Callback();
        temp_eb7 = new global::Fuse.Reactive.EventBinding(temp5);
        var temp28 = new global::Fuse.Navigation.WhileInactive();
        var temp29 = new global::Fuse.Triggers.Actions.Callback();
        temp_eb8 = new global::Fuse.Reactive.EventBinding(temp6);
        content = new global::Fuse.Controls.DockPanel();
        var temp30 = new global::Fuse.Controls.StackPanel();
        var temp31 = new global::Fuse.Controls.Grid();
        var temp32 = new global::Fuse.Controls.Panel();
        var temp33 = new global::Fuse.Gestures.Clicked();
        var temp34 = new global::Fuse.Navigation.NavigateTo();
        var temp35 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp7, Fuse.Reactive.BindingMode.Default);
        var temp36 = new global::Fuse.Gestures.Clicked();
        var temp37 = new global::Fuse.Navigation.NavigateTo();
        var temp38 = new global::Fuse.Reactive.DataBinding(temp1_Value_inst, temp8, Fuse.Reactive.BindingMode.Default);
        var temp39 = new global::Fuse.Controls.Text();
        var temp40 = new global::Fuse.Font(new global::Uno.UX.BundleFileSource(import("../../../../../assets/fonts/AlegreyaSans-Bold.otf")));
        var temp41 = new global::Fuse.Drawing.StaticSolidColor(float4(0.1137255f, 0.4901961f, 0.2588235f, 1f));
        var temp42 = new global::Fuse.Controls.Rectangle();
        var temp43 = new global::Fuse.Controls.StackPanel();
        var temp44 = new global::Fuse.Controls.Panel();
        var temp45 = new global::Fuse.Reactive.DataBinding(temp2_Items_inst, temp9, Fuse.Reactive.BindingMode.Default);
        var temp46 = new global::SquareButton();
        temp_eb9 = new global::Fuse.Reactive.EventBinding(temp10);
        var temp47 = new global::Fuse.Reactive.DataBinding(temp3_Items_inst, temp11, Fuse.Reactive.BindingMode.Default);
        var temp48 = new global::SquareButton();
        temp_eb10 = new global::Fuse.Reactive.EventBinding(temp12);
        var temp49 = new global::Fuse.Reactive.DataBinding(temp4_Items_inst, temp13, Fuse.Reactive.BindingMode.Default);
        var temp50 = new global::SquareButton();
        temp_eb11 = new global::Fuse.Reactive.EventBinding(temp14);
        var temp51 = new global::Fuse.Drawing.StaticSolidColor(float4(0.9333333f, 0.9333333f, 0.9333333f, 1f));
        var temp52 = new global::Fuse.Drawing.StaticSolidColor(float4(0.7333333f, 0.7333333f, 0.7333333f, 1f));
        this.SourceLineNumber = 1;
        this.SourceFileName = "dashboard.ux";
        temp15.LineNumber = 3;
        temp15.FileName = "dashboard.ux";
        temp15.SourceLineNumber = 3;
        temp15.SourceFileName = "dashboard.ux";
        temp15.File = new global::Uno.UX.BundleFileSource(import("../../../../../js_files/navigation.js"));
        temp16.Code = "\n\t\tvar Observable = require(\"FuseJS/Observable\");\n\t\tvar ImageTools = require(\"FuseJS/ImageTools\");\n\t\tvar imagePath = Observable();\n\t\tvar API = require('js_files/api.js');\n\t\tvar nav = require('js_files/navigation.js');\n\t\tvar value = Observable(false);\n\t\tvar ys = false;\n\t\tvar sidebarOpen = Observable(false);\n\t\tvar username = Observable();\n\t\t//sidebarOpen.value = ys;\n\t\tvar data = Observable();\n\t\tvar day = Observable();\n\t\tvar personnel_id = Observable();\n\t\tvar Storage = require(\"FuseJS/Storage\");\n\n\n\t\tdataPath = API.readOSPID();\n\t\tStorage.read(dataPath).then(function(content) {\n\t\t\t    var data = JSON.parse(content);\n\t\t\t    //console.dir(data.personnel[0].surname)\n\t\t\t    username.value = data.personnel[0].surname+' '+data.personnel[0].forenames;\n\t\t\t    ImageTools.getImageFromBase64(data.picture).\n        \t\t\tthen(function (image) \n                { \n                    //console.log(\"Scratch image path is: \" + image.path); \n                    imagePath.value = image.path\n                }).catch(function(anerr){console.log(JSON.stringify(anerr))});\n\n\t\t\t    \n        \t\t\tfor (var i =0; i < data.permits.length; i++) {\n\t\t\t\t\t\tday.add(new Item(i+1, data.permits[i].course, data.permits[i].expiry_date));\n\t\t\t\t\t}\n\n\n\t\t\t}, function(error) {\n\t\t\t    //For now, let's expect the error to be because of the file not being found.\n\t\t\t    //welcomeText.value = \"There is currently no local data stored\";\n\t\t\t});\n\t\t\n\n\n\t\tfunction setSidebarOpen() {\n\t\t\tsidebarOpen.value = true;\n\t\t};\n\t\tfunction setSidebarClosed() {\n\t\t\tsidebarOpen.value = false;\n\t\t};\n\n\t\tfunction Item(id, title, date) {\n\t        this.id = id;\n\t        this.title = title;\n\t        this.date = date;\n\t        this.selected = Observable(false);\n\t    }\n\n\t    var morning = Observable(\n\t        new Item(1, \"Permit about to expire\", \"20/10/2017\"),\n\t        new Item(2, \"Oil & Gas Training\", \"22/10/2017\"),\n\t        new Item(3, \"Fire drill\", \"2/11/2017\")\n\t    );\n\t    // var day = Observable(\n\t        \n\t    //     new Item(1, \"PMP\", \"05/08/2017\"),\n\t    //     new Item(2, \"OSP\", \"02/09/2017\"),\n\t    //     new Item(3, \"Learn something new\", \"10/09/2017\")\n\t    // );\n\t    var evening = Observable(\n\t        new Item(1, \"Dinner with mom\", \"20 min ago\"),\n\t        new Item(2, \"Play chess with a friend\", \"58 min ago\"),\n\t        new Item(3, \"Watch TV\", \"1 hr ago\")\n\t    );\n\n\t    function logout() {\n\t    \tusername.value = null;\n\t\t\t// nav.logout();\n\t\t\t//console.log(\"help\");\n\t\t\trouter.goto(\"home\");\n\t    }\n\n\t\tmodule.exports = {\n\t\t\tvalue: value,\n\t\t\tsidebarOpen: sidebarOpen,\n\t\t\tsetSidebarOpen: setSidebarOpen,\n\t\t\tsetSidebarClosed: setSidebarClosed,\n\t\t\tmorning: morning,\n\t        day: day,\n\t        evening: evening,\n\t        logout: logout,\n\t        username: username,\n\t\t\timage: imagePath,\n\t\t\tpersonnel_id: personnel_id\n\t\t};\n\t";
        temp16.LineNumber = 4;
        temp16.FileName = "dashboard.ux";
        temp16.SourceLineNumber = 4;
        temp16.SourceFileName = "dashboard.ux";
        EdgeNavigator.Name = __selector7;
        EdgeNavigator.SourceLineNumber = 99;
        EdgeNavigator.SourceFileName = "dashboard.ux";
        EdgeNavigator.Children.Add(menu);
        EdgeNavigator.Children.Add(content);
        menu.Width = new Uno.UX.Size(180f, Uno.UX.Unit.Unspecified);
        menu.Name = __selector8;
        menu.SourceLineNumber = 116;
        menu.SourceFileName = "dashboard.ux";
        global::Fuse.Navigation.EdgeNavigation.SetEdge(menu, Fuse.Navigation.NavigationEdge.Left);
        menu.Children.Add(temp17);
        menu.Children.Add(temp26);
        menu.Children.Add(temp28);
        temp17.SourceLineNumber = 118;
        temp17.SourceFileName = "dashboard.ux";
        temp17.Animators.Add(temp18);
        temp17.Animators.Add(temp19);
        temp17.Animators.Add(temp20);
        temp17.Animators.Add(temp21);
        temp17.Animators.Add(temp22);
        temp17.Animators.Add(temp23);
        temp17.Animators.Add(temp24);
        temp17.Animators.Add(temp25);
        temp18.Value = 180f;
        temp19.Value = 0f;
        temp20.Value = 0f;
        temp21.Value = 0f;
        temp21.Easing = Fuse.Animations.Easing.CircularOut;
        temp22.Value = 45f;
        temp22.Easing = Fuse.Animations.Easing.ExponentialIn;
        temp23.Value = -45f;
        temp23.Easing = Fuse.Animations.Easing.ExponentialIn;
        temp24.Value = new Uno.UX.Size(28f, Uno.UX.Unit.Unspecified);
        temp25.Value = new Uno.UX.Size(28f, Uno.UX.Unit.Unspecified);
        temp26.SourceLineNumber = 134;
        temp26.SourceFileName = "dashboard.ux";
        temp26.Actions.Add(temp27);
        temp26.Bindings.Add(temp_eb7);
        temp27.SourceLineNumber = 135;
        temp27.SourceFileName = "dashboard.ux";
        temp27.Handler += temp_eb7.OnEvent;
        temp5.SourceLineNumber = 135;
        temp5.SourceFileName = "dashboard.ux";
        temp28.SourceLineNumber = 137;
        temp28.SourceFileName = "dashboard.ux";
        temp28.Actions.Add(temp29);
        temp28.Bindings.Add(temp_eb8);
        temp29.SourceLineNumber = 138;
        temp29.SourceFileName = "dashboard.ux";
        temp29.Handler += temp_eb8.OnEvent;
        temp6.SourceLineNumber = 138;
        temp6.SourceFileName = "dashboard.ux";
        content.Name = __selector9;
        content.SourceLineNumber = 142;
        content.SourceFileName = "dashboard.ux";
        content.Background = temp51;
        content.Children.Add(mainAppTranslation);
        content.Children.Add(temp30);
        content.Children.Add(parentScrollView);
        mainAppTranslation.Name = __selector10;
        mainAppTranslation.SourceLineNumber = 144;
        mainAppTranslation.SourceFileName = "dashboard.ux";
        temp30.SourceLineNumber = 146;
        temp30.SourceFileName = "dashboard.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp30, Fuse.Layouts.Dock.Top);
        temp30.Children.Add(temp31);
        temp30.Children.Add(temp42);
        temp31.Columns = "auto,1*,auto";
        temp31.SourceLineNumber = 149;
        temp31.SourceFileName = "dashboard.ux";
        temp31.Background = temp41;
        temp31.Children.Add(temp32);
        temp31.Children.Add(temp39);
        temp32.HitTestMode = Fuse.Elements.HitTestMode.LocalBounds;
        temp32.Width = new Uno.UX.Size(32f, Uno.UX.Unit.Unspecified);
        temp32.Height = new Uno.UX.Size(32f, Uno.UX.Unit.Unspecified);
        temp32.Margin = float4(7f, 5f, 5f, 5f);
        temp32.SourceLineNumber = 150;
        temp32.SourceFileName = "dashboard.ux";
        temp32.Children.Add(temp);
        temp32.Children.Add(temp1);
        temp32.Children.Add(topRectangle);
        temp32.Children.Add(middleRectangle);
        temp32.Children.Add(bottomRectangle);
        temp.SourceLineNumber = 151;
        temp.SourceFileName = "dashboard.ux";
        temp.Nodes.Add(temp33);
        temp.Bindings.Add(temp35);
        temp33.SourceLineNumber = 152;
        temp33.SourceFileName = "dashboard.ux";
        temp33.Actions.Add(temp34);
        temp34.SourceLineNumber = 153;
        temp34.SourceFileName = "dashboard.ux";
        temp34.Target = content;
        temp7.SourceLineNumber = 151;
        temp7.SourceFileName = "dashboard.ux";
        temp1.SourceLineNumber = 156;
        temp1.SourceFileName = "dashboard.ux";
        temp1.Nodes.Add(temp36);
        temp1.Bindings.Add(temp38);
        temp36.SourceLineNumber = 157;
        temp36.SourceFileName = "dashboard.ux";
        temp36.Actions.Add(temp37);
        temp37.SourceLineNumber = 158;
        temp37.SourceFileName = "dashboard.ux";
        temp37.Target = menu;
        temp8.SourceLineNumber = 156;
        temp8.SourceFileName = "dashboard.ux";
        topRectangle.Color = float4(0.9686275f, 0.8980392f, 0.372549f, 1f);
        topRectangle.Width = new Uno.UX.Size(26f, Uno.UX.Unit.Unspecified);
        topRectangle.Height = new Uno.UX.Size(2f, Uno.UX.Unit.Unspecified);
        topRectangle.Name = __selector11;
        topRectangle.SourceLineNumber = 161;
        topRectangle.SourceFileName = "dashboard.ux";
        topRectangle.Children.Add(topMenuTranslation);
        topRectangle.Children.Add(topMenuRotation);
        topMenuTranslation.Y = -9f;
        topMenuTranslation.Name = __selector12;
        topMenuTranslation.SourceLineNumber = 162;
        topMenuTranslation.SourceFileName = "dashboard.ux";
        topMenuRotation.Name = __selector13;
        topMenuRotation.SourceLineNumber = 163;
        topMenuRotation.SourceFileName = "dashboard.ux";
        middleRectangle.Color = float4(0.9686275f, 0.8980392f, 0.372549f, 1f);
        middleRectangle.Width = new Uno.UX.Size(26f, Uno.UX.Unit.Unspecified);
        middleRectangle.Height = new Uno.UX.Size(2f, Uno.UX.Unit.Unspecified);
        middleRectangle.Name = __selector14;
        middleRectangle.SourceLineNumber = 165;
        middleRectangle.SourceFileName = "dashboard.ux";
        bottomRectangle.Color = float4(0.9686275f, 0.8980392f, 0.372549f, 1f);
        bottomRectangle.Width = new Uno.UX.Size(26f, Uno.UX.Unit.Unspecified);
        bottomRectangle.Height = new Uno.UX.Size(2f, Uno.UX.Unit.Unspecified);
        bottomRectangle.Name = __selector15;
        bottomRectangle.SourceLineNumber = 166;
        bottomRectangle.SourceFileName = "dashboard.ux";
        bottomRectangle.Children.Add(bottomMenuTranslation);
        bottomRectangle.Children.Add(bottomMenuRotation);
        bottomMenuTranslation.Y = 9f;
        bottomMenuTranslation.Name = __selector16;
        bottomMenuTranslation.SourceLineNumber = 167;
        bottomMenuTranslation.SourceFileName = "dashboard.ux";
        bottomMenuRotation.Name = __selector17;
        bottomMenuRotation.SourceLineNumber = 168;
        bottomMenuRotation.SourceFileName = "dashboard.ux";
        temp39.Value = "DASHBOARD";
        temp39.FontSize = 20f;
        temp39.TextAlignment = Fuse.Controls.TextAlignment.Center;
        temp39.TextColor = float4(1f, 1f, 1f, 1f);
        temp39.Alignment = Fuse.Elements.Alignment.VerticalCenter;
        temp39.Margin = float4(-20f, 5f, 0f, 0f);
        temp39.Padding = float4(2f, 2f, 2f, 2f);
        temp39.SourceLineNumber = 172;
        temp39.SourceFileName = "dashboard.ux";
        temp39.Font = temp40;
        temp42.Color = float4(0.2f, 0.2352941f, 0.282353f, 1f);
        temp42.Height = new Uno.UX.Size(1f, Uno.UX.Unit.Unspecified);
        temp42.Margin = float4(0f, 0f, 0f, 0f);
        temp42.SourceLineNumber = 178;
        temp42.SourceFileName = "dashboard.ux";
        parentScrollView.Name = __selector18;
        parentScrollView.SourceLineNumber = 182;
        parentScrollView.SourceFileName = "dashboard.ux";
        parentScrollView.Children.Add(temp43);
        temp43.ItemSpacing = 12f;
        temp43.Alignment = Fuse.Elements.Alignment.Top;
        temp43.Margin = float4(8f, 16f, 8f, 16f);
        temp43.SourceLineNumber = 183;
        temp43.SourceFileName = "dashboard.ux";
        temp43.Children.Add(temp44);
        temp43.Children.Add(temp2);
        temp43.Children.Add(temp46);
        temp43.Children.Add(temp3);
        temp43.Children.Add(temp48);
        temp43.Children.Add(temp4);
        temp43.Children.Add(temp50);
        temp44.Height = new Uno.UX.Size(7f, Uno.UX.Unit.Unspecified);
        temp44.SourceLineNumber = 184;
        temp44.SourceFileName = "dashboard.ux";
        temp2.Label = "Notifications";
        temp2.SourceLineNumber = 187;
        temp2.SourceFileName = "dashboard.ux";
        temp2.Bindings.Add(temp45);
        temp9.SourceLineNumber = 187;
        temp9.SourceFileName = "dashboard.ux";
        temp46.Textq = "View all";
        temp46.SourceLineNumber = 188;
        temp46.SourceFileName = "dashboard.ux";
        global::Fuse.Gestures.Clicked.AddHandler(temp46, temp_eb9.OnEvent);
        temp46.Bindings.Add(temp_eb9);
        temp10.SourceLineNumber = 188;
        temp10.SourceFileName = "dashboard.ux";
        temp3.Label = "Permits";
        temp3.SourceLineNumber = 191;
        temp3.SourceFileName = "dashboard.ux";
        temp3.Bindings.Add(temp47);
        temp11.SourceLineNumber = 191;
        temp11.SourceFileName = "dashboard.ux";
        temp48.Textq = "View all";
        temp48.SourceLineNumber = 192;
        temp48.SourceFileName = "dashboard.ux";
        global::Fuse.Gestures.Clicked.AddHandler(temp48, temp_eb10.OnEvent);
        temp48.Bindings.Add(temp_eb10);
        temp12.SourceLineNumber = 192;
        temp12.SourceFileName = "dashboard.ux";
        temp4.Label = "News";
        temp4.SourceLineNumber = 195;
        temp4.SourceFileName = "dashboard.ux";
        temp4.Bindings.Add(temp49);
        temp13.SourceLineNumber = 195;
        temp13.SourceFileName = "dashboard.ux";
        temp50.Textq = "View all";
        temp50.SourceLineNumber = 196;
        temp50.SourceFileName = "dashboard.ux";
        global::Fuse.Gestures.Clicked.AddHandler(temp50, temp_eb11.OnEvent);
        temp50.Bindings.Add(temp_eb11);
        temp14.SourceLineNumber = 196;
        temp14.SourceFileName = "dashboard.ux";
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(EdgeNavigator);
        __g_nametable.Objects.Add(menu);
        __g_nametable.Objects.Add(temp_eb7);
        __g_nametable.Objects.Add(temp_eb8);
        __g_nametable.Objects.Add(content);
        __g_nametable.Objects.Add(mainAppTranslation);
        __g_nametable.Objects.Add(topRectangle);
        __g_nametable.Objects.Add(topMenuTranslation);
        __g_nametable.Objects.Add(topMenuRotation);
        __g_nametable.Objects.Add(middleRectangle);
        __g_nametable.Objects.Add(bottomRectangle);
        __g_nametable.Objects.Add(bottomMenuTranslation);
        __g_nametable.Objects.Add(bottomMenuRotation);
        __g_nametable.Objects.Add(parentScrollView);
        __g_nametable.Objects.Add(temp_eb9);
        __g_nametable.Objects.Add(temp_eb10);
        __g_nametable.Objects.Add(temp_eb11);
        this.Background = temp52;
        this.Children.Add(temp15);
        this.Children.Add(temp16);
        this.Children.Add(EdgeNavigator);
    }
    static global::Uno.UX.Selector __selector0 = "X";
    static global::Uno.UX.Selector __selector1 = "Y";
    static global::Uno.UX.Selector __selector2 = "Opacity";
    static global::Uno.UX.Selector __selector3 = "Degrees";
    static global::Uno.UX.Selector __selector4 = "Width";
    static global::Uno.UX.Selector __selector5 = "Value";
    static global::Uno.UX.Selector __selector6 = "Items";
    static global::Uno.UX.Selector __selector7 = "EdgeNavigator";
    static global::Uno.UX.Selector __selector8 = "menu";
    static global::Uno.UX.Selector __selector9 = "content";
    static global::Uno.UX.Selector __selector10 = "mainAppTranslation";
    static global::Uno.UX.Selector __selector11 = "topRectangle";
    static global::Uno.UX.Selector __selector12 = "topMenuTranslation";
    static global::Uno.UX.Selector __selector13 = "topMenuRotation";
    static global::Uno.UX.Selector __selector14 = "middleRectangle";
    static global::Uno.UX.Selector __selector15 = "bottomRectangle";
    static global::Uno.UX.Selector __selector16 = "bottomMenuTranslation";
    static global::Uno.UX.Selector __selector17 = "bottomMenuRotation";
    static global::Uno.UX.Selector __selector18 = "parentScrollView";
}
