[Uno.Compiler.UxGenerated]
public partial class ForgotPassword: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    global::Uno.UX.Property<string> temp_Value_inst;
    internal global::Fuse.Reactive.EventBinding temp_eb14;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "temp_eb14"
    };
    static ForgotPassword()
    {
    }
    [global::Uno.UX.UXConstructor]
    public ForgotPassword(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp = new global::LoginField();
        temp_Value_inst = new OSP_FuseControlsTextInputControl_Value_Property(temp, __selector0);
        var temp1 = new global::Fuse.Reactive.Data("OSPID");
        var temp2 = new global::Fuse.Reactive.Data("getPassword");
        var temp3 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp4 = new global::Fuse.Controls.DockPanel();
        var temp5 = new global::TopBar();
        var temp6 = new global::Fuse.Controls.DockPanel();
        var temp7 = new global::Fuse.Controls.StackPanel();
        var temp8 = new global::Fuse.Controls.Text();
        var temp9 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp1, Fuse.Reactive.BindingMode.Default);
        var temp10 = new global::SubButton();
        temp_eb14 = new global::Fuse.Reactive.EventBinding(temp2);
        this.Color = float4(1f, 1f, 1f, 1f);
        this.SourceLineNumber = 1;
        this.SourceFileName = "forgotPassword.ux";
        temp3.LineNumber = 3;
        temp3.FileName = "forgotPassword.ux";
        temp3.SourceLineNumber = 3;
        temp3.SourceFileName = "forgotPassword.ux";
        temp3.File = new global::Uno.UX.BundleFileSource(import("../../../../../js_files/password.js"));
        temp4.SourceLineNumber = 5;
        temp4.SourceFileName = "forgotPassword.ux";
        temp4.Children.Add(temp5);
        temp4.Children.Add(temp6);
        temp5.Title = "PASSWORD RECOVERY";
        temp5.SourceLineNumber = 7;
        temp5.SourceFileName = "forgotPassword.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp5, Fuse.Layouts.Dock.Top);
        temp6.Color = float4(1f, 1f, 1f, 1f);
        temp6.Margin = float4(0f, 0f, 0f, 0f);
        temp6.SourceLineNumber = 9;
        temp6.SourceFileName = "forgotPassword.ux";
        temp6.Children.Add(temp7);
        temp7.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp7.Alignment = Fuse.Elements.Alignment.Center;
        temp7.SourceLineNumber = 11;
        temp7.SourceFileName = "forgotPassword.ux";
        temp7.Children.Add(temp8);
        temp7.Children.Add(temp);
        temp7.Children.Add(temp10);
        temp8.Value = "Please enter your OSP ID";
        temp8.FontSize = 12f;
        temp8.Color = float4(0.2f, 0.2f, 0.2f, 1f);
        temp8.Alignment = Fuse.Elements.Alignment.Center;
        temp8.Margin = float4(0f, 10f, 0f, 10f);
        temp8.SourceLineNumber = 12;
        temp8.SourceFileName = "forgotPassword.ux";
        temp8.Font = global::MainView.Raleway;
        temp.PlaceHolder = "OSPID";
        temp.PlaceHolderColor = float4(0.5490196f, 0.5843138f, 0.6313726f, 1f);
        temp.BgColor = float4(1f, 1f, 1f, 1f);
        temp.StrokeColor = float4(0.2f, 0.2f, 0.2f, 1f);
        temp.TextColor = float4(0.2f, 0.2f, 0.2f, 1f);
        temp.Height = new Uno.UX.Size(38f, Uno.UX.Unit.Unspecified);
        temp.Padding = float4(20f, 5f, 20f, 5f);
        temp.SourceLineNumber = 13;
        temp.SourceFileName = "forgotPassword.ux";
        temp.Bindings.Add(temp9);
        temp1.SourceLineNumber = 13;
        temp1.SourceFileName = "forgotPassword.ux";
        temp10.ButtonName = "SUBMIT";
        temp10.ButtonTextColor = float4(1f, 1f, 1f, 1f);
        temp10.Color = float4(0.1137255f, 0.4901961f, 0.2588235f, 1f);
        temp10.SourceLineNumber = 15;
        temp10.SourceFileName = "forgotPassword.ux";
        global::Fuse.Gestures.Clicked.AddHandler(temp10, temp_eb14.OnEvent);
        temp10.Bindings.Add(temp_eb14);
        temp2.SourceLineNumber = 15;
        temp2.SourceFileName = "forgotPassword.ux";
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(temp_eb14);
        this.Children.Add(temp3);
        this.Children.Add(temp4);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
}
