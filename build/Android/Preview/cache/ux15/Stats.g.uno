[Uno.Compiler.UxGenerated]
public partial class Stats: Fuse.Controls.Text
{
    static Stats()
    {
    }
    [global::Uno.UX.UXConstructor]
    public Stats()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        this.FontSize = 14f;
        this.TextAlignment = Fuse.Controls.TextAlignment.Center;
        this.Color = float4(0.1137255f, 0.4901961f, 0.2588235f, 1f);
        this.SourceLineNumber = 15;
        this.SourceFileName = "Sidebar.ux";
    }
}
