[Uno.Compiler.UxGenerated]
public partial class SortableList: Fuse.Controls.Panel
{
    object _field_Items;
    [global::Uno.UX.UXOriginSetter("SetItems")]
    public object Items
    {
        get { return _field_Items; }
        set { SetItems(value, null); }
    }
    public void SetItems(object value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_Items)
        {
            _field_Items = value;
            OnPropertyChanged("Items", origin);
        }
    }
    string _field_Label;
    [global::Uno.UX.UXOriginSetter("SetLabel")]
    public string Label
    {
        get { return _field_Label; }
        set { SetLabel(value, null); }
    }
    public void SetLabel(string value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_Label)
        {
            _field_Label = value;
            OnPropertyChanged("Label", origin);
        }
    }
    readonly Fuse.Controls.ScrollView parentScrollView;
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly SortableList __parent;
        [Uno.WeakReference] internal readonly SortableList __parentInstance;
        public Template(SortableList parent, SortableList parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::Item();
            __self.SourceLineNumber = 110;
            __self.SourceFileName = "dashboardlist.ux";
            return __self;
        }
    }
    global::Uno.UX.Property<string> temp_Value_inst;
    global::Uno.UX.Property<object> temp1_Items_inst;
    global::Uno.UX.Property<object> this_Items_inst;
    global::Uno.UX.Property<string> this_Label_inst;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "parentScrollView"
    };
    static SortableList()
    {
    }
    [global::Uno.UX.UXConstructor]
    public SortableList(
		[global::Uno.UX.UXParameter("parentScrollView")] Fuse.Controls.ScrollView parentScrollView)
    {
        this.parentScrollView = parentScrollView;
        InitializeUX();
    }
    void InitializeUX()
    {
        this_Items_inst = new OSP_SortableList_Items_Property(this, __selector0);
        this_Label_inst = new OSP_SortableList_Label_Property(this, __selector1);
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp2 = new global::Fuse.Reactive.Constant(this);
        var temp = new global::Fuse.Controls.Text();
        temp_Value_inst = new OSP_FuseControlsTextControl_Value_Property(temp, __selector2);
        var temp3 = new global::Fuse.Reactive.Property(temp2, OSP_accessor_SortableList_Label.Singleton);
        var temp1 = new global::Fuse.Reactive.Each();
        temp1_Items_inst = new OSP_FuseReactiveEach_Items_Property(temp1, __selector0);
        var temp4 = new global::Fuse.Reactive.Data("items");
        var temp5 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp6 = new global::Fuse.Controls.StackPanel();
        var temp7 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp3, Fuse.Reactive.BindingMode.Read);
        var temp8 = new global::Fuse.Controls.StackPanel();
        var temp9 = new Template(this, this);
        var temp10 = new global::Fuse.Reactive.DataBinding(temp1_Items_inst, temp4, Fuse.Reactive.BindingMode.Default);
        this.SourceLineNumber = 1;
        this.SourceFileName = "dashboardlist.ux";
        temp5.Code = "\n\tvar Observable = require(\"FuseJS/Observable\");\n\tvar items = this.Items.inner();\n\n\tvar selected = null;\n\tvar reordering = Observable(false);\n\n\tfunction select(args) {\n\t\tif (selected === null) {\n\t\t\tselected = args.data.id;\n\t\t\titems.forEach(function(x) {\n\t\t\t\tif (x.id === selected) {\n\t\t\t\t\tx.selected.value = true;\n\t\t\t\t}\n\t\t\t});\n\t\t}\n\t\treordering.value = true;\n\t}\n\n\tfunction deselect() {\n\t\tselected = null;\n\t\titems.forEach(function(x) {\n\t\t\tx.selected.value = false;\n\t\t});\n\t\treordering.value = false;\n\t}\n\n\tfunction hover(args) {\n\t\tif (reordering.value === true && selected !== null) {\n\t\t\tvar from;\n\t\t\tvar to;\n\t\t\titems.forEach(function(item, index) {\n\t\t\t\tif (item.id === selected) {\n\t\t\t\t\tfrom = index;\n\t\t\t\t}\n\t\t\t\tif (item.id === args.data.id) {\n\t\t\t\t\tto = index;\n\t\t\t\t}\n\t\t\t});\n\t\t\tif (to !== from && to !== undefined) {\n\t\t\t\tvar tmp = items.toArray();\n\t\t\t\tvar elem = tmp[from];\n\t\t\t\ttmp.splice(from, 1);\n\t\t\t\ttmp.splice(to, 0, elem);\n\t\t\t\titems.replaceAll(tmp);\n\t\t\t}\n\t\t}\n\t}\n\n\tmodule.exports = {\n\t\titems: items,\n\t\treordering: reordering,\n\t\tselect: select,\n\t\tdeselect: deselect,\n\t\thover: hover\n\t};\n\t";
        temp5.LineNumber = 7;
        temp5.FileName = "dashboardlist.ux";
        temp5.SourceLineNumber = 7;
        temp5.SourceFileName = "dashboardlist.ux";
        temp6.ItemSpacing = 8f;
        temp6.SourceLineNumber = 106;
        temp6.SourceFileName = "dashboardlist.ux";
        temp6.Children.Add(temp);
        temp6.Children.Add(temp8);
        temp.FontSize = 18f;
        temp.TextColor = float4(0.4666667f, 0.4666667f, 0.4666667f, 1f);
        temp.Alignment = Fuse.Elements.Alignment.CenterLeft;
        temp.Margin = float4(8f, 0f, 8f, 0f);
        temp.SourceLineNumber = 107;
        temp.SourceFileName = "dashboardlist.ux";
        temp.Font = Fuse.Font.Bold;
        temp.Bindings.Add(temp7);
        temp3.SourceLineNumber = 107;
        temp3.SourceFileName = "dashboardlist.ux";
        temp2.SourceLineNumber = 107;
        temp2.SourceFileName = "dashboardlist.ux";
        temp8.ItemSpacing = 1f;
        temp8.SourceLineNumber = 108;
        temp8.SourceFileName = "dashboardlist.ux";
        temp8.Children.Add(temp1);
        temp1.IdentityKey = "id";
        temp1.SourceLineNumber = 109;
        temp1.SourceFileName = "dashboardlist.ux";
        temp1.Templates.Add(temp9);
        temp1.Bindings.Add(temp10);
        temp4.SourceLineNumber = 109;
        temp4.SourceFileName = "dashboardlist.ux";
        __g_nametable.This = this;
        __g_nametable.Objects.Add(parentScrollView);
        __g_nametable.Properties.Add(this_Items_inst);
        __g_nametable.Properties.Add(this_Label_inst);
        this.Children.Add(temp5);
        this.Children.Add(temp6);
    }
    static global::Uno.UX.Selector __selector0 = "Items";
    static global::Uno.UX.Selector __selector1 = "Label";
    static global::Uno.UX.Selector __selector2 = "Value";
}
