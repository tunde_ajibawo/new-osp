var Observable = require('FuseJS/Observable');
var FileSystem = require("FuseJS/FileSystem");
var Storage = require("FuseJS/Storage");

var useridFilePath = FileSystem.dataDirectory + "/" + "testfile.json";
var ROOT_URL = "http://41.75.207.60/osp/public/api/";
var welcomeText;

//this makes call to the API
function apiFetch(path, options) {
    var url = encodeURI(ROOT_URL + path);
    //console.log(url)
    if (options === undefined) {
        options = {};
    }

    // If a body is provided, serialize it as JSON and set the Content-Type header
    if (options.body !== undefined) {
        options = Object.assign({}, options, {
            body: JSON.stringify(options.body),
            headers: Object.assign({}, options.headers, {
                "Content-Type": "application/json"
            })
        });
    }

    // Fetch the resource and parse the response as JSON
    return fetch(url, options).then(function(response) {
        //console.log(JSON.stringify(response));
        return response; // This returns a promise
    });
}



function auth(osp_id, password) {
    return apiFetch("login", {
        method: "POST",
        body: {
            osp_id: osp_id,
            password: password
        }
    });
}

function signup( osp_id, email, password)
{
    return apiFetch("signup", {
        method: "POST",
        body: {
            osp_id: osp_id,
            email: email,
            password: password
        }
    });
}

function reset( osp_id, email, password)
{
    return apiFetch("reset", {
        method: "POST",
        body: {
            osp_id: osp_id,
            email: email,
            password: password
        }
    });
}

//use this part to store the personnel_id on the  phone
function saveOSPID(storeObject)
{
    //var storeObject = OSPID;
    Storage.write(useridFilePath, JSON.stringify(storeObject));
    console.log('ehloo')
}

function readOSPID()
{
      return useridFilePath;
}

function deleteOSPID()
{
    
}

module.exports = {
    saveOSPID:saveOSPID,
    readOSPID: readOSPID,
    deleteOSPID: deleteOSPID,

    //API Related
    auth: auth,
    signup: signup,
    reset: reset

};