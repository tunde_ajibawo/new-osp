var permits = API.readPermits(data)
        for (var i =0; i < permits.length; i++) {
            day.add(new Item(i+1, permits[i].course, permits[i].expiry_date));
        }
        personnel_details = API.readUserID(data);
        //console.dir(personnel_details);
        username.value = personnel_details.surname+' '+personnel_details.forenames;
        var pics = API.readPicture(data)

                        
        //console.dir(pics[0].picture);
        ImageTools.getImageFromBase64(pics[0].picture).
        then(function (image) 
                { 
                    //console.log("Scratch image path is: " + image.path); 
                    imagePath.value = image.path
                }).catch(function(anerr){console.log(JSON.stringify(anerr))});




                

//var taffy = require("js_files/taffy").taffy();
//var moment = require("js_files/moment");
var loki = require("js_files/lokijs");


var filepath = FileSystem.dataDirectory + "/" + "osp.json";
var useridFilePath = FileSystem.dataDirectory + "/" + "testfile.tmp";

//use this part to store the personnel_id on the  phone
function saveOSPID(OSPID)
{
    FileSystem.writeTextToFileSync(useridFilePath, OSPID);
}

function readOSPID()
{
    var content =  FileSystem.readTextFromFileSync(useridFilePath);
    return content;
}

function deleteOSPID()
{
    FileSystem.deleteSync(useridFilePath);
}



var db = new loki(filepath, 
    {
        autoload: true,
        autoloadCallback : databaseInitialize,
        autosave: true, 
        autosaveInterval: 1000
    });

var ROOT_URL = "http://41.75.207.60/osp/public/api/";
var user_details;
var permits;
var picture;
var training;
var history;



function savePersonnel(details) {
    
}

function listTables(OSPID)
{
    //console.dir(db);

    var users = user_details.find();
    var prm = permits.find();
    var pics  = picture.find();
    var hist = history.find();
    var train = training.find();
    console.dir(users);
    console.dir(prm);
    console.dir(pics);
    console.dir(hist);
    console.dir(train);

   for (i=0; i < users.length; i++)
    {
        //console.dir(users[i]);
        //console.dir(user_details.remove(users[i]));
        //console.dir(history.remove(hist[i]));
        //console.dir (permits.remove(prm[i]));
        //console.dir (picture.remove(pics[i]));
    }  
    //console.dir(users);
    //console.dir( user_details.remove( user_details.find({'occupation': 'DECK' } )));
}
function removeUser(OSPID)
{
    
}

function checkDB() {

}

function databaseInitialize() {
    user_details = db.getCollection("user_details");
    permits = db.getCollection("permits");
    training = db.getCollection("training");
    history = db.getCollection("history");
    picture = db.getCollection("picture");

    if (user_details === null) {
      user_details = db.addCollection("user_details");

    }
    if (permits === null) {
        permits = db.addCollection("permits");
        
      }
      if (training === null) {
        training = db.addCollection("training");
      }
      if (history === null) {
        history = db.addCollection("history");
        
      }
      if (picture === null) {
        picture = db.addCollection("picture");
      }
    

    // kick off any program logic or start listening to external events
    //runProgramLogic();
  }


  function saveHistory(hist)
  {
    //console.dir(histry);
    if(histry != 0)
    {
        for (i=0; i < histry.length; i++)
        {
           //console.dir(histry[i]);
            history.insert(hist[i]);
        }
    }
   
    
  }

  function saveTraining(train)
  {
    if (train != 0 )
    {
        for(i=0; i < train.length; i++)
        {
            training.insert(train[i]);
        }
    }
  }

  function savePermits(permit)
{
    //console.dir(permit);
    if(permit != 0)
    {
        for(i=0; i < permit.length; i++)
        {
            //console.dir(permit[i]);
            permits.insert(permit[i]);
        }
    }
}

function savePicture(pics, OSPID)
{
    OSPID = +OSPID;
    picture.insert(
        {
            "picture": pics,
            "personnel_id" : OSPID
        });
}
function saveUserID(personnel) {
    //console.dir(valueToSave);
    user_details.insert(personnel);
    
}
      
function readUserID(OSPID) {
    OSPID = +OSPID;
    var user = user_details.findOne( {'personnel_id':  {'$aeq' : OSPID} });
    //listTables(OSPID);
    //console.dir(user);
    return user;
}

function readPermits(OSPID)
{
    OSPID = +OSPID;
    var permit = permits.find( {'personnel_id': {'$aeq' : OSPID}});
    //listTables(OSPID);
    return permit;   
}

function readPicture(OSPID){
    OSPID = +OSPID;
    var pic = picture.find( {'personnel_id': {'$aeq' : OSPID}} );
    return pic;
}

//this makes call to the API
function apiFetch(path, options) {
    var url = encodeURI(ROOT_URL + path);
    //console.log(url)
    if (options === undefined) {
        options = {};
    }

    // If a body is provided, serialize it as JSON and set the Content-Type header
    if (options.body !== undefined) {
        options = Object.assign({}, options, {
            body: JSON.stringify(options.body),
            headers: Object.assign({}, options.headers, {
                "Content-Type": "application/json"
            })
        });
    }

    // Fetch the resource and parse the response as JSON
    return fetch(url, options).then(function(response) {
        //console.log(JSON.stringify(response));
        return response; // This returns a promise
    });
}

function auth(osp_id) {
    return apiFetch("login", {
        method: "POST",
        body: {
            osp_id: osp_id
        }
    });
}


function getPersonnelDetails(osp_id) {

    var options = {
        method: 'GET'
    };

    return apiFetch('personnel/' + osp_id, options);
}

function getPermits(osp_id) {

    var options = {
        method: 'GET'
    };

    return apiFetch('permits/' + osp_id, options);
}

function getTrainings(osp_id) {

    var options = {
        method: 'GET'
            //headers: { "Content-type": "applications/json",  },
    };
    return apiFetch('training/' + osp_id, options);
}

function getPicture(osp_id) {
    var options = {
        method: 'GET'
    };
    return apiFetch('picture/' + osp_id, options);
}


module.exports = {
    auth: auth,
    getPersonnelDetails: getPersonnelDetails,
    getTrainings: getTrainings,
    saveUserID: saveUserID,
    readUserID: readUserID,
    getPermits: getPermits,
    getPicture: getPicture,
    listTables: listTables,
    readPermits: readPermits,
    savePicture: savePicture,
    savePermits: savePermits,
    saveHistory: saveHistory,
    saveTraining: saveTraining,
    readPicture: readPicture,
    readOSPID: readOSPID,
    deleteOSPID: deleteOSPID,
    saveOSPID: saveOSPID
        //saveDetails: saveDetails


};