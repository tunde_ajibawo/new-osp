package com.Bindings;

import com.uno.UnoObject;
import com.uno.BoolArray;
import com.uno.ByteArray;
import com.uno.CharArray;
import com.uno.DoubleArray;
import com.uno.FloatArray;
import com.uno.IntArray;
import com.uno.LongArray;
import com.uno.ObjectArray;
import com.uno.ShortArray;
import com.uno.StringArray;
public class ExternedBlockHost
{
    static void debug_log(Object message)
    {
        android.util.Log.d("OSP", (message==null ? "null" : message.toString()));
    }
    public static native long callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_BoolArrayToUnoArrayPtr0(final Object jarr,long jarrPtr);
    public static long boolArrayToUnoArrayPtr(final Object arr)
    {
        return (long) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.boolArrayToUnoArrayPtr(arr);
    }
    
    public static native long callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_ByteArrayToUnoArrayPtr1(final Object jarr,long jarrPtr);
    public static long byteArrayToUnoArrayPtr(final Object arr)
    {
        return (long) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.byteArrayToUnoArrayPtr(arr);
    }
    
    public static native long callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_CharArrayToUnoArrayPtr2(final Object jarr,long jarrPtr);
    public static long charArrayToUnoArrayPtr(final Object arr)
    {
        return (long) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.charArrayToUnoArrayPtr(arr);
    }
    
    public static native Object callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_CopyBoolArray3(final UnoObject array);
    public static Object copyBoolArray(final UnoObject array)
    {
        return (Object) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.copyBoolArray(array);
    }
    
    public static native Object callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_CopyByteArray4(final UnoObject array);
    public static Object copyByteArray(final UnoObject array)
    {
        return (Object) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.copyByteArray(array);
    }
    
    public static native Object callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_CopyCharArray5(final UnoObject array);
    public static Object copyCharArray(final UnoObject array)
    {
        return (Object) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.copyCharArray(array);
    }
    
    public static native Object callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_CopyDoubleArray6(final UnoObject array);
    public static Object copyDoubleArray(final UnoObject array)
    {
        return (Object) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.copyDoubleArray(array);
    }
    
    public static native Object callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_CopyFloatArray7(final UnoObject array);
    public static Object copyFloatArray(final UnoObject array)
    {
        return (Object) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.copyFloatArray(array);
    }
    
    public static native Object callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_CopyIntArray8(final UnoObject array);
    public static Object copyIntArray(final UnoObject array)
    {
        return (Object) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.copyIntArray(array);
    }
    
    public static native Object callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_CopyLongArray9(final UnoObject array);
    public static Object copyLongArray(final UnoObject array)
    {
        return (Object) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.copyLongArray(array);
    }
    
    public static native Object callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_CopyObjectArray10(final UnoObject array);
    public static Object copyObjectArray(final UnoObject array)
    {
        return (Object) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.copyObjectArray(array);
    }
    
    public static native Object callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_CopyShortArray11(final UnoObject array);
    public static Object copyShortArray(final UnoObject array)
    {
        return (Object) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.copyShortArray(array);
    }
    
    public static native Object callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_CopyStringArray12(final UnoObject array);
    public static Object copyStringArray(final UnoObject array)
    {
        return (Object) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.copyStringArray(array);
    }
    
    public static native Object callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_CopyUByteArray13(final UnoObject array);
    public static Object copyUByteArray(final UnoObject array)
    {
        return (Object) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.copyUByteArray(array);
    }
    
    public static native long callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_DoubleArrayToUnoArrayPtr14(final Object jarr,long jarrPtr);
    public static long doubleArrayToUnoArrayPtr(final Object arr)
    {
        return (long) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.doubleArrayToUnoArrayPtr(arr);
    }
    
    public static native long callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_FloatArrayToUnoArrayPtr15(final Object jarr,long jarrPtr);
    public static long floatArrayToUnoArrayPtr(final Object arr)
    {
        return (long) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.floatArrayToUnoArrayPtr(arr);
    }
    
    public static native boolean callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_GetBool16(final UnoObject array,final int index);
    public static boolean getBool(final UnoObject array,final int index)
    {
        return (boolean) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.getBool(array,index);
    }
    
    public static native byte callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_GetByte17(final UnoObject array,final int index);
    public static byte getByte(final UnoObject array,final int index)
    {
        return (byte) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.getByte(array,index);
    }
    
    public static native char callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_GetChar18(final UnoObject array,final int index);
    public static char getChar(final UnoObject array,final int index)
    {
        return (char) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.getChar(array,index);
    }
    
    public static native double callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_GetDouble19(final UnoObject array,final int index);
    public static double getDouble(final UnoObject array,final int index)
    {
        return (double) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.getDouble(array,index);
    }
    
    public static native float callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_GetFloat20(final UnoObject array,final int index);
    public static float getFloat(final UnoObject array,final int index)
    {
        return (float) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.getFloat(array,index);
    }
    
    public static native int callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_GetInt21(final UnoObject array,final int index);
    public static int getInt(final UnoObject array,final int index)
    {
        return (int) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.getInt(array,index);
    }
    
    public static native long callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_GetLong22(final UnoObject array,final int index);
    public static long getLong(final UnoObject array,final int index)
    {
        return (long) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.getLong(array,index);
    }
    
    public static native UnoObject callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_GetObject23(final UnoObject array,final int index);
    public static UnoObject getObject(final UnoObject array,final int index)
    {
        return (UnoObject) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.getObject(array,index);
    }
    
    public static native short callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_GetShort24(final UnoObject array,final int index);
    public static short getShort(final UnoObject array,final int index)
    {
        return (short) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.getShort(array,index);
    }
    
    public static native String callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_GetString25(final UnoObject array,final int index);
    public static String getString(final UnoObject array,final int index)
    {
        return (String) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.getString(array,index);
    }
    
    public static native byte callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_GetUByte26(final UnoObject array,final int index);
    public static byte getUByte(final UnoObject array,final int index)
    {
        return (byte) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.getUByte(array,index);
    }
    
    public static native long callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_IntArrayToUnoArrayPtr27(final Object jarr,long jarrPtr);
    public static long intArrayToUnoArrayPtr(final Object arr)
    {
        return (long) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.intArrayToUnoArrayPtr(arr);
    }
    
    public static native long callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_LongArrayToUnoArrayPtr28(final Object jarr,long jarrPtr);
    public static long longArrayToUnoArrayPtr(final Object arr)
    {
        return (long) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.longArrayToUnoArrayPtr(arr);
    }
    
    public static native long callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_NewBoolArrayPtr29(final int length);
    public static long newBoolArrayPtr(final int length)
    {
        return (long) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.newBoolArrayPtr(length);
    }
    
    public static native long callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_NewByteArrayPtr30(final int length,final boolean unoIsUnsigned);
    public static long newByteArrayPtr(final int length,final boolean unoIsUnsigned)
    {
        return (long) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.newByteArrayPtr(length,unoIsUnsigned);
    }
    
    public static native long callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_NewCharArrayPtr31(final int length);
    public static long newCharArrayPtr(final int length)
    {
        return (long) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.newCharArrayPtr(length);
    }
    
    public static native long callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_NewDoubleArrayPtr32(final int length);
    public static long newDoubleArrayPtr(final int length)
    {
        return (long) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.newDoubleArrayPtr(length);
    }
    
    public static native long callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_NewFloatArrayPtr33(final int length);
    public static long newFloatArrayPtr(final int length)
    {
        return (long) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.newFloatArrayPtr(length);
    }
    
    public static native long callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_NewIntArrayPtr34(final int length);
    public static long newIntArrayPtr(final int length)
    {
        return (long) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.newIntArrayPtr(length);
    }
    
    public static native long callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_NewLongArrayPtr35(final int length);
    public static long newLongArrayPtr(final int length)
    {
        return (long) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.newLongArrayPtr(length);
    }
    
    public static native long callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_NewObjectArrayPtr36(final int length);
    public static long newObjectArrayPtr(final int length)
    {
        return (long) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.newObjectArrayPtr(length);
    }
    
    public static native long callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_NewShortArrayPtr37(final int length);
    public static long newShortArrayPtr(final int length)
    {
        return (long) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.newShortArrayPtr(length);
    }
    
    public static native long callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_NewStringArrayPtr38(final int length);
    public static long newStringArrayPtr(final int length)
    {
        return (long) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.newStringArrayPtr(length);
    }
    
    public static native boolean callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_SetBool39(final UnoObject array,final int index,final boolean val);
    public static boolean setBool(final UnoObject array,final int index,final boolean val)
    {
        return (boolean) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.setBool(array,index,val);
    }
    
    public static native byte callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_SetByte40(final UnoObject array,final int index,final byte val);
    public static byte setByte(final UnoObject array,final int index,final byte val)
    {
        return (byte) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.setByte(array,index,val);
    }
    
    public static native char callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_SetChar41(final UnoObject array,final int index,final char val);
    public static char setChar(final UnoObject array,final int index,final char val)
    {
        return (char) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.setChar(array,index,val);
    }
    
    public static native double callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_SetDouble42(final UnoObject array,final int index,final double val);
    public static double setDouble(final UnoObject array,final int index,final double val)
    {
        return (double) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.setDouble(array,index,val);
    }
    
    public static native float callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_SetFloat43(final UnoObject array,final int index,final float val);
    public static float setFloat(final UnoObject array,final int index,final float val)
    {
        return (float) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.setFloat(array,index,val);
    }
    
    public static native int callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_SetInt44(final UnoObject array,final int index,final int val);
    public static int setInt(final UnoObject array,final int index,final int val)
    {
        return (int) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.setInt(array,index,val);
    }
    
    public static native long callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_SetLong45(final UnoObject array,final int index,final long val);
    public static long setLong(final UnoObject array,final int index,final long val)
    {
        return (long) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.setLong(array,index,val);
    }
    
    public static native UnoObject callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_SetObject46(final UnoObject array,final int index,final UnoObject val);
    public static UnoObject setObject(final UnoObject array,final int index,final UnoObject val)
    {
        return (UnoObject) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.setObject(array,index,val);
    }
    
    public static native short callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_SetShort47(final UnoObject array,final int index,final short val);
    public static short setShort(final UnoObject array,final int index,final short val)
    {
        return (short) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.setShort(array,index,val);
    }
    
    public static native String callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_SetString48(final UnoObject array,final int index,final String val);
    public static String setString(final UnoObject array,final int index,final String val)
    {
        return (String) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.setString(array,index,val);
    }
    
    public static native byte callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_SetUByte49(final UnoObject array,final int index,final byte val);
    public static byte setUByte(final UnoObject array,final int index,final byte val)
    {
        return (byte) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.setUByte(array,index,val);
    }
    
    public static native long callUno_Uno_Compiler_ExportTargetInterop_Foreign_Android_JavaToUnoArrayEntrypoints_ShortArrayToUnoArrayPtr50(final Object jarr,long jarrPtr);
    public static long shortArrayToUnoArrayPtr(final Object arr)
    {
        return (long) com.foreign.Uno.Compiler.ExportTargetInterop.Foreign.Android.JavaToUnoArrayEntrypoints.shortArrayToUnoArrayPtr(arr);
    }
    
    public static Object CreateInnerJava51(final Object directBuffer,final UnoObject unoObj)
    {
        return (Object) com.foreign.Uno.Compiler.ExportTargetInterop.ForeignDataView.CreateInnerJava51(directBuffer,unoObj);
    }
    
    public static Object GetJavaObject52()
    {
        return (Object) com.foreign.Uno.IO.AAssetManager.GetJavaObject52();
    }
    
    public static String GetUserConfigDirectory53()
    {
        return (String) com.foreign.Uno.IO.Directory.GetUserConfigDirectory53();
    }
    
    public static String GetUserDataDirectory54()
    {
        return (String) com.foreign.Uno.IO.Directory.GetUserDataDirectory54();
    }
    
    public static float _getDensity55()
    {
        return (float) com.foreign.Uno.Platform.AndroidDisplay._getDensity55();
    }
    
    public static native boolean callUno_Uno_Platform_AndroidDisplay__initializedGet57();
    public static native void callUno_Uno_Platform_AndroidDisplay__initializedSet57(final boolean setVal);
    public static native void callUno_Uno_Platform_AndroidDisplay_OnFrameCallback58(final UnoObject jthis,final double currentTime,final double deltaTime);
    public static void JavaEnableTicks56(final UnoObject _this)
    {
        com.foreign.Uno.Platform.AndroidDisplay.JavaEnableTicks56(_this);
    }
    
    public static void Quit59()
    {
        com.foreign.Uno.Runtime.Implementation.Internal.Unsafe.Quit59();
    }
    
    public static native void callUno_Uno_Collections_List_lt_string_gt__Add61(final UnoObject jthis,final String item);
    public static boolean JavaGetLocalAddresses60(final UnoObject addresses)
    {
        return (boolean) com.foreign.Uno.Net.Dns.JavaGetLocalAddresses60(addresses);
    }
    
    public static native void callUno_Uno_Permissions_Permissions_Failed62(final UnoObject promise);
    public static void permissionRequestFailed(final UnoObject x)
    {
        com.foreign.Uno.Permissions.Permissions.permissionRequestFailed(x);
    }
    
    public static native void callUno_Uno_Permissions_Permissions_Succeeded63(final UnoObject promise);
    public static void permissionRequestSucceeded(final UnoObject x)
    {
        com.foreign.Uno.Permissions.Permissions.permissionRequestSucceeded(x);
    }
    
    public static void requestPermission64(final UnoObject promise,final String permissionName)
    {
        com.foreign.Uno.Permissions.Permissions.requestPermission64(promise,permissionName);
    }
    
    public static void requestPermissions65(final UnoObject promise,final com.uno.StringArray permissionNames)
    {
        com.foreign.Uno.Permissions.Permissions.requestPermissions65(promise,permissionNames);
    }
    
    public static boolean shouldShowInformation66(final String x)
    {
        return (boolean) com.foreign.Uno.Permissions.Permissions.shouldShowInformation66(x);
    }
    
    public static native UnoObject callUno_Uno_Guid__ctor68(final com.uno.ByteArray bytes);
    public static UnoObject NewGuid67()
    {
        return (UnoObject) com.foreign.Uno.Guid.NewGuid67();
    }
    
    public static Object GetActivityClassInner69()
    {
        return (Object) com.foreign.Android.Base.JNI.GetActivityClassInner69();
    }
    
    public static Object GetActivityObjectInner70()
    {
        return (Object) com.foreign.Android.Base.JNI.GetActivityObjectInner70();
    }
    
    public static Object GetRootActivity71()
    {
        return (Object) com.foreign.Android.ActivityUtils.GetRootActivity71();
    }
    
    public static native boolean callUno_Android_ActivityUtils_OnReceived73(final int requestCode,final int resultCode,final Object data,long dataPtr);
    public static Object Init72()
    {
        return (Object) com.foreign.Android.ActivityUtils.Init72();
    }
    
    public static void StartActivity74(final Object _intent)
    {
        com.foreign.Android.ActivityUtils.StartActivity74(_intent);
    }
    
    public static void StartForResultJava75(final int id,final Object _intent)
    {
        com.foreign.Android.ActivityUtils.StartForResultJava75(id,_intent);
    }
    
    public static int GetID76(final String path)
    {
        return (int) com.foreign.Android.Resources.GetID76(path);
    }
    
    public static native Object callUno_Fuse_Platform_SystemUI_layoutAttachedToGet78();
    public static native void callUno_Fuse_Platform_SystemUI_layoutAttachedToSet78(final Object setVal,long setValPtr);
    public static native Object callUno_Fuse_Platform_SystemUI__keyboardListenerGet79();
    public static void Attach77(final Object _layout)
    {
        com.foreign.Fuse.Platform.SystemUI.Attach77(_layout);
    }
    
    public static native void callUno_Fuse_Platform_SystemUI_realWidthSet81(final int setVal);
    public static native void callUno_Fuse_Platform_SystemUI_realHeightSet82(final int setVal);
    public static native Object callUno_Fuse_Platform_SystemUI_SuperLayoutGet83();
    public static native int callUno_Fuse_Platform_SystemUI_realHeightGet82();
    public static native int callUno_Fuse_Platform_SystemUI_realWidthGet81();
    public static void CalcRealSizes80()
    {
        com.foreign.Fuse.Platform.SystemUI.CalcRealSizes80();
    }
    
    public static native void callUno_Fuse_Platform_SystemUI_SuperLayoutSet83(final Object setVal,long setValPtr);
    public static native void callUno_Fuse_Platform_SystemUI_RootLayoutSet85(final Object setVal,long setValPtr);
    public static native Object callUno_Fuse_Platform_SystemUI_RootLayoutGet85();
    public static native int callUno_Fuse_Platform_SystemUI_GetRealDisplayHeight88();
    public static native void callUno_Fuse_Platform_SystemUI_CompensateRootLayoutForSystemUI89();
    public static void CreateLayouts84()
    {
        com.foreign.Fuse.Platform.SystemUI.CreateLayouts84();
    }
    
    public static void Detach90()
    {
        com.foreign.Fuse.Platform.SystemUI.Detach90();
    }
    
    public static native void callUno_Fuse_Platform_SystemUI__systemUIStateSet92(final int setVal);
    public static native void callUno_Fuse_Platform_SystemUI_cppOnTopFrameChanged95(final int height);
    public static void EnterFullscreen91()
    {
        com.foreign.Fuse.Platform.SystemUI.EnterFullscreen91();
    }
    
    public static int GetAPILevel96()
    {
        return (int) com.foreign.Fuse.Platform.SystemUI.GetAPILevel96();
    }
    
    public static float GetDensity97()
    {
        return (float) com.foreign.Fuse.Platform.SystemUI.GetDensity97();
    }
    
    public static Object GetDisplayMetrics98()
    {
        return (Object) com.foreign.Fuse.Platform.SystemUI.GetDisplayMetrics98();
    }
    
    public static String GetOSVersion99()
    {
        return (String) com.foreign.Fuse.Platform.SystemUI.GetOSVersion99();
    }
    
    public static native int callUno_Fuse_Platform_SystemUI__systemUIStateGet92();
    public static native boolean callUno_Fuse_Platform_SystemUI_hasCachedStatusBarSizeGet101();
    public static native int callUno_Fuse_Platform_SystemUI_cachedOpenSizeGet102();
    public static native void callUno_Fuse_Platform_SystemUI_hasCachedStatusBarSizeSet101(final boolean setVal);
    public static native void callUno_Fuse_Platform_SystemUI_cachedOpenSizeSet102(final int setVal);
    public static float GetStatusBarHeight100()
    {
        return (float) com.foreign.Fuse.Platform.SystemUI.GetStatusBarHeight100();
    }
    
    public static int GetSuperLayoutHeight103()
    {
        return (int) com.foreign.Fuse.Platform.SystemUI.GetSuperLayoutHeight103();
    }
    
    public static void HideActionBar94()
    {
        com.foreign.Fuse.Platform.SystemUI.HideActionBar94();
    }
    
    public static void HideStatusBar93()
    {
        com.foreign.Fuse.Platform.SystemUI.HideStatusBar93();
    }
    
    public static native void callUno_Fuse_Platform_SystemUI_OnDestroy107();
    public static native void callUno_Fuse_Platform_SystemUI_OnConfigChanged108();
    public static void HookOntoRawActivityEvents104()
    {
        com.foreign.Fuse.Platform.SystemUI.HookOntoRawActivityEvents104();
    }
    
    public static native int callUno_Fuse_Platform_SystemUI_GetRealDisplayWidth109();
    public static native void callUno_Fuse_Platform_SystemUI_cppOnConfigChanged110();
    public static native void callUno_Fuse_Platform_SystemUI_ResendFrameSizes111();
    public static Object MakePostV11LayoutChangeListener86()
    {
        return (Object) com.foreign.Fuse.Platform.SystemUI.MakePostV11LayoutChangeListener86();
    }
    
    public static native void callUno_Fuse_Platform_SystemUI__keyboardListenerSet79(final Object setVal,long setValPtr);
    public static void OnCreate112()
    {
        com.foreign.Fuse.Platform.SystemUI.OnCreate112();
    }
    
    public static void OnPause105()
    {
        com.foreign.Fuse.Platform.SystemUI.OnPause105();
    }
    
    public static native void callUno_Fuse_Platform_SystemUI_UpdateStatusBar114();
    public static void OnResume106()
    {
        com.foreign.Fuse.Platform.SystemUI.OnResume106();
    }
    
    public static void SetAsRootView115(final Object view)
    {
        com.foreign.Fuse.Platform.SystemUI.SetAsRootView115(view);
    }
    
    public static void SetFrame87(final Object view,final int originX,final int originY,final int height)
    {
        com.foreign.Fuse.Platform.SystemUI.SetFrame87(view,originX,originY,height);
    }
    
    public static void ShowStatusBar116()
    {
        com.foreign.Fuse.Platform.SystemUI.ShowStatusBar116();
    }
    
    public static native int callUno_Fuse_Platform_SystemUI_lastKeyboardHeightGet117();
    public static native boolean callUno_Fuse_Platform_SystemUI_firstSizingGet118();
    public static native void callUno_Fuse_Platform_SystemUI_onHideKeyboard119(final int keyboardHeight,final boolean force);
    public static native void callUno_Fuse_Platform_SystemUI_onShowKeyboard120(final int keyboardHeight,final boolean force);
    public static native void callUno_Fuse_Platform_SystemUI_firstSizingSet118(final boolean setVal);
    public static void unoOnGlobalLayout113()
    {
        com.foreign.Fuse.Platform.SystemUI.unoOnGlobalLayout113();
    }
    
    public static native void callUno_Fuse_Internal_AndroidSystemFont_AddFamily122(final String name,final String language,final String variant);
    public static native void callUno_Fuse_Internal_AndroidSystemFont_AddFont123(final String path,final int index,final int weight,final boolean isItalic);
    public static native void callUno_Fuse_Internal_AndroidSystemFont_AddAlias124(final String name,final String to,final int weight);
    public static native void callUno_Fuse_Internal_AndroidSystemFont_ThrowUno125(final String message);
    public static void AddFonts121()
    {
        com.foreign.Fuse.Internal.AndroidSystemFont.AddFonts121();
    }
    
    public static int GetOrientation126(final Object stream)
    {
        return (int) com.foreign.Fuse.Resources.Exif.ExifAndroidImpl.GetOrientation126(stream);
    }
    
    public static Object InputStreamFromByteArray127(final Object buf)
    {
        return (Object) com.foreign.Fuse.Resources.Exif.ExifAndroidImpl.InputStreamFromByteArray127(buf);
    }
    
    public static Object Create128()
    {
        return (Object) com.foreign.Fuse.Controls.Native.Android.Button.Create128();
    }
    
    public static void SetText129(final Object handle,final String text)
    {
        com.foreign.Fuse.Controls.Native.Android.Button.SetText129(handle,text);
    }
    
    public static native void callUno_Action_Object(final com.foreign.Uno.Action_Object theDelegate,final Object arg,long argPtr);
    public static void InstallDrawlistener130(final UnoObject _this, final Object handle,final com.foreign.Uno.Action_Object callback)
    {
        com.foreign.Fuse.Controls.Native.Android.CanvasViewGroup.InstallDrawlistener130(_this, handle,callback);
    }
    
    public static Object Instantiate131()
    {
        return (Object) com.foreign.Fuse.Controls.Native.Android.CanvasViewGroup.Instantiate131();
    }
    
    public static Object Create132()
    {
        return (Object) com.foreign.Fuse.Controls.Native.Android.DatePickerView.Create132();
    }
    
    public static int GetApiLevel133()
    {
        return (int) com.foreign.Fuse.Controls.Native.Android.DatePickerView.GetApiLevel133();
    }
    
    public static long GetDateInMsSince1970InUtc134(final UnoObject _this, final Object datePickerHandle)
    {
        return (long) com.foreign.Fuse.Controls.Native.Android.DatePickerView.GetDateInMsSince1970InUtc134(_this, datePickerHandle);
    }
    
    public static void Init135(final UnoObject _this, final Object datePickerHandle)
    {
        com.foreign.Fuse.Controls.Native.Android.DatePickerView.Init135(_this, datePickerHandle);
    }
    
    public static void SetDate136(final UnoObject _this, final Object datePickerHandle,final long msSince1970InUtc)
    {
        com.foreign.Fuse.Controls.Native.Android.DatePickerView.SetDate136(_this, datePickerHandle,msSince1970InUtc);
    }
    
    public static void SetMaxValue137(final UnoObject _this, final Object datePickerHandle,final long msSince1970InUtc)
    {
        com.foreign.Fuse.Controls.Native.Android.DatePickerView.SetMaxValue137(_this, datePickerHandle,msSince1970InUtc);
    }
    
    public static void SetMinValue138(final UnoObject _this, final Object datePickerHandle,final long msSince1970InUtc)
    {
        com.foreign.Fuse.Controls.Native.Android.DatePickerView.SetMinValue138(_this, datePickerHandle,msSince1970InUtc);
    }
    
    public static void ClearListener139(final Object viewHandle)
    {
        com.foreign.Fuse.Controls.Native.Android.FocusChangedListener.ClearListener139(viewHandle);
    }
    
    public static native void callUno_Fuse_Controls_Native_Android_FocusChangedListener_OnFocusChange141(final UnoObject jthis,final boolean hasFocus);
    public static Object Create140(final UnoObject _this)
    {
        return (Object) com.foreign.Fuse.Controls.Native.Android.FocusChangedListener.Create140(_this);
    }
    
    public static void SetListener142(final Object viewHandle,final Object listenerHandle)
    {
        com.foreign.Fuse.Controls.Native.Android.FocusChangedListener.SetListener142(viewHandle,listenerHandle);
    }
    
    public static Object GetContext143(final Object viewHandle)
    {
        return (Object) com.foreign.Fuse.Controls.Native.Android.FocusManager.GetContext143(viewHandle);
    }
    
    public static Object GetWindowToken144(final Object viewHandle)
    {
        return (Object) com.foreign.Fuse.Controls.Native.Android.FocusManager.GetWindowToken144(viewHandle);
    }
    
    public static boolean HasFocus145(final Object viewHandle)
    {
        return (boolean) com.foreign.Fuse.Controls.Native.Android.FocusManager.HasFocus145(viewHandle);
    }
    
    public static void RequestRootViewFocus146(final Object viewHandle)
    {
        com.foreign.Fuse.Controls.Native.Android.FocusManager.RequestRootViewFocus146(viewHandle);
    }
    
    public static native void callUno_Action_String(final com.foreign.Uno.Action_String theDelegate,final String arg);
    public static void LoadAsync147(final String urlString,final com.foreign.Uno.Action_Object success,final com.foreign.Uno.Action_String error)
    {
        com.foreign.Fuse.Controls.Native.Android.HttpImageLoader.LoadAsync147(urlString,success,error);
    }
    
    public static void ClearBitmap148(final Object handle)
    {
        com.foreign.Fuse.Controls.Native.Android.ImageView.ClearBitmap148(handle);
    }
    
    public static Object Create149(final Object container)
    {
        return (Object) com.foreign.Fuse.Controls.Native.Android.ImageView.Create149(container);
    }
    
    public static Object CreateContainer150()
    {
        return (Object) com.foreign.Fuse.Controls.Native.Android.ImageView.CreateContainer150();
    }
    
    public static void GetImageSize151(final Object handle,final com.uno.IntArray wh)
    {
        com.foreign.Fuse.Controls.Native.Android.ImageView.GetImageSize151(handle,wh);
    }
    
    public static void SetBitmap152(final Object handle,final Object bitmapHandle)
    {
        com.foreign.Fuse.Controls.Native.Android.ImageView.SetBitmap152(handle,bitmapHandle);
    }
    
    public static void SetTint153(final Object handle,final int rgba)
    {
        com.foreign.Fuse.Controls.Native.Android.ImageView.SetTint153(handle,rgba);
    }
    
    public static void UpdateImageTransform1154(final Object handle,final float x,final float y,final float scaleX,final float scaleY)
    {
        com.foreign.Fuse.Controls.Native.Android.ImageView.UpdateImageTransform1154(handle,x,y,scaleX,scaleY);
    }
    
    public static void ClearOnTouchListener155(final Object viewHandle)
    {
        com.foreign.Fuse.Controls.Native.Android.InputDispatch.ClearOnTouchListener155(viewHandle);
    }
    
    public static native boolean callUno_Fuse_Controls_Native_Android_InputDispatch_OnTouch157(final Object view,final Object motionEvent,long viewPtr,long motionEventPtr);
    public static Object CreateTouchListener156()
    {
        return (Object) com.foreign.Fuse.Controls.Native.Android.InputDispatch.CreateTouchListener156();
    }
    
    public static void SetOnTouchListener158(final Object viewHandle,final Object listenerHandle)
    {
        com.foreign.Fuse.Controls.Native.Android.InputDispatch.SetOnTouchListener158(viewHandle,listenerHandle);
    }
    
    public static boolean ContainsKey1159(final Object handle,final Object key)
    {
        return (boolean) com.foreign.Fuse.Controls.Native.Android.JavaMap.ContainsKey1159(handle,key);
    }
    
    public static Object Create160()
    {
        return (Object) com.foreign.Fuse.Controls.Native.Android.JavaMap.Create160();
    }
    
    public static UnoObject Get1161(final Object handle,final Object key)
    {
        return (UnoObject) com.foreign.Fuse.Controls.Native.Android.JavaMap.Get1161(handle,key);
    }
    
    public static void Put162(final Object handle,final Object key,final UnoObject value)
    {
        com.foreign.Fuse.Controls.Native.Android.JavaMap.Put162(handle,key,value);
    }
    
    public static void Remove1163(final Object handle,final Object key)
    {
        com.foreign.Fuse.Controls.Native.Android.JavaMap.Remove1163(handle,key);
    }
    
    public static boolean Compare164(final Object handle1,final Object handle2)
    {
        return (boolean) com.foreign.Fuse.Controls.Native.Android.MotionEvent.Compare164(handle1,handle2);
    }
    
    public static int GetAction165(final UnoObject _this, final Object handle)
    {
        return (int) com.foreign.Fuse.Controls.Native.Android.MotionEvent.GetAction165(_this, handle);
    }
    
    public static int GetActionMasked166(final UnoObject _this, final Object handle)
    {
        return (int) com.foreign.Fuse.Controls.Native.Android.MotionEvent.GetActionMasked166(_this, handle);
    }
    
    public static long GetEventTime167(final UnoObject _this, final Object handle)
    {
        return (long) com.foreign.Fuse.Controls.Native.Android.MotionEvent.GetEventTime167(_this, handle);
    }
    
    public static void GetLocationOnScreen1168(final UnoObject _this, final Object viewHandle,final com.uno.IntArray result)
    {
        com.foreign.Fuse.Controls.Native.Android.MotionEvent.GetLocationOnScreen1168(_this, viewHandle,result);
    }
    
    public static int GetPointerCount169(final UnoObject _this, final Object handle)
    {
        return (int) com.foreign.Fuse.Controls.Native.Android.MotionEvent.GetPointerCount169(_this, handle);
    }
    
    public static int GetPointerId1170(final UnoObject _this, final Object handle,final int pointerIndex)
    {
        return (int) com.foreign.Fuse.Controls.Native.Android.MotionEvent.GetPointerId1170(_this, handle,pointerIndex);
    }
    
    public static int GetPointerIndexMask171()
    {
        return (int) com.foreign.Fuse.Controls.Native.Android.MotionEvent.GetPointerIndexMask171();
    }
    
    public static int GetPointerIndexShift172()
    {
        return (int) com.foreign.Fuse.Controls.Native.Android.MotionEvent.GetPointerIndexShift172();
    }
    
    public static float GetX173(final UnoObject _this, final Object handle,final int pointerIndex)
    {
        return (float) com.foreign.Fuse.Controls.Native.Android.MotionEvent.GetX173(_this, handle,pointerIndex);
    }
    
    public static float GetXPrecision174(final UnoObject _this, final Object handle)
    {
        return (float) com.foreign.Fuse.Controls.Native.Android.MotionEvent.GetXPrecision174(_this, handle);
    }
    
    public static float GetY175(final UnoObject _this, final Object handle,final int pointerIndex)
    {
        return (float) com.foreign.Fuse.Controls.Native.Android.MotionEvent.GetY175(_this, handle,pointerIndex);
    }
    
    public static float GetYPrecision176(final UnoObject _this, final Object handle)
    {
        return (float) com.foreign.Fuse.Controls.Native.Android.MotionEvent.GetYPrecision176(_this, handle);
    }
    
    public static Object Create177()
    {
        return (Object) com.foreign.Fuse.Controls.Native.Android.ScrollView.Create177();
    }
    
    public static native void callUno_Action_int_int_int_int(final com.foreign.Uno.Action_int_int_int_int theDelegate,final int arg1,final int arg2,final int arg3,final int arg4);
    public static void InstallCallback178(final UnoObject _this, final Object handle,final com.foreign.Uno.Action_int_int_int_int callback)
    {
        com.foreign.Fuse.Controls.Native.Android.ScrollView.InstallCallback178(_this, handle,callback);
    }
    
    public static void SetClipToBounds1179(final Object handle,final boolean clipToBounds)
    {
        com.foreign.Fuse.Controls.Native.Android.ScrollView.SetClipToBounds1179(handle,clipToBounds);
    }
    
    public static void SetIsHorizontal180(final UnoObject _this, final Object handle,final boolean isHorizontal)
    {
        com.foreign.Fuse.Controls.Native.Android.ScrollView.SetIsHorizontal180(_this, handle,isHorizontal);
    }
    
    public static void SetScrollPosition181(final Object handle,final int x,final int y)
    {
        com.foreign.Fuse.Controls.Native.Android.ScrollView.SetScrollPosition181(handle,x,y);
    }
    
    public static native void callUno_Fuse_Controls_Native_Android_Slider_OnSeekBarChanged183(final UnoObject jthis,final double rel,final boolean fromUser);
    public static void AddChangedCallback182(final UnoObject _this, final Object handle)
    {
        com.foreign.Fuse.Controls.Native.Android.Slider.AddChangedCallback182(_this, handle);
    }
    
    public static Object Create184()
    {
        return (Object) com.foreign.Fuse.Controls.Native.Android.Slider.Create184();
    }
    
    public static void SetProgress185(final Object handle,final double progress)
    {
        com.foreign.Fuse.Controls.Native.Android.Slider.SetProgress185(handle,progress);
    }
    
    public static void HideKeyboard186(final Object hideKeyboardContext,final Object hideKeyboardWindowToken)
    {
        com.foreign.Fuse.Controls.Native.Android.SoftKeyboard.HideKeyboard186(hideKeyboardContext,hideKeyboardWindowToken);
    }
    
    public static void ShowKeyboard187(final Object viewHandle)
    {
        com.foreign.Fuse.Controls.Native.Android.SoftKeyboard.ShowKeyboard187(viewHandle);
    }
    
    public static native void callUno_Fuse_Controls_Native_Android_SurfaceView_OnSurfaceRedrawNeeded189(final UnoObject jthis,final Object holder,long holderPtr);
    public static native void callUno_Fuse_Controls_Native_Android_SurfaceView_OnSurfaceChanged190(final UnoObject jthis,final Object holder,final int f,final int w,final int h,long holderPtr);
    public static native void callUno_Fuse_Controls_Native_Android_SurfaceView_OnSurfaceCreated191(final UnoObject jthis,final Object holder,long holderPtr);
    public static native void callUno_Fuse_Controls_Native_Android_SurfaceView_OnSurfaceDestroyed192(final UnoObject jthis,final Object holder,long holderPtr);
    public static void AddCallback188(final UnoObject _this, final Object handle)
    {
        com.foreign.Fuse.Controls.Native.Android.SurfaceView.AddCallback188(_this, handle);
    }
    
    public static Object Create193()
    {
        return (Object) com.foreign.Fuse.Controls.Native.Android.SurfaceView.Create193();
    }
    
    public static Object GetSurface194(final Object holder)
    {
        return (Object) com.foreign.Fuse.Controls.Native.Android.SurfaceView.GetSurface194(holder);
    }
    
    public static native void callUno_Fuse_Controls_Native_Android_Switch_OnToggleChanged196(final UnoObject jthis,final boolean value);
    public static void AddCallback195(final UnoObject _this, final Object handle)
    {
        com.foreign.Fuse.Controls.Native.Android.Switch.AddCallback195(_this, handle);
    }
    
    public static Object Create197()
    {
        return (Object) com.foreign.Fuse.Controls.Native.Android.Switch.Create197();
    }
    
    public static void SetValue198(final Object handle,final boolean value)
    {
        com.foreign.Fuse.Controls.Native.Android.Switch.SetValue198(handle,value);
    }
    
    public static void MakeItPlain199(final Object handle)
    {
        com.foreign.Fuse.Controls.Native.Android.TextEdit.MakeItPlain199(handle);
    }
    
    public static native boolean callUno_Fuse_Controls_Native_Android_TextInput_OnEditorAction201(final UnoObject jthis,final int actionCode);
    public static void AddEditorActionListener200(final UnoObject _this, final Object handle)
    {
        com.foreign.Fuse.Controls.Native.Android.TextInput.AddEditorActionListener200(_this, handle);
    }
    
    public static native void callUno_Fuse_Controls_Native_Android_TextInput_OnTextChanged203(final UnoObject jthis,final String value);
    public static void AddTextChangedListener202(final UnoObject _this, final Object handle)
    {
        com.foreign.Fuse.Controls.Native.Android.TextInput.AddTextChangedListener202(_this, handle);
    }
    
    public static void ClearFocus204(final Object handle)
    {
        com.foreign.Fuse.Controls.Native.Android.TextInput.ClearFocus204(handle);
    }
    
    public static Object Create1205()
    {
        return (Object) com.foreign.Fuse.Controls.Native.Android.TextInput.Create1205();
    }
    
    public static boolean HasFocus206(final Object viewHandle)
    {
        return (boolean) com.foreign.Fuse.Controls.Native.Android.TextInput.HasFocus206(viewHandle);
    }
    
    public static void Measure2207(final Object handle,final int w,final int h,final boolean hasX,final boolean hasY,final com.uno.IntArray measuredSize)
    {
        com.foreign.Fuse.Controls.Native.Android.TextInput.Measure2207(handle,w,h,hasX,hasY,measuredSize);
    }
    
    public static void RequestFocus208(final Object viewHandle)
    {
        com.foreign.Fuse.Controls.Native.Android.TextInput.RequestFocus208(viewHandle);
    }
    
    public static void SetCursorDrawableColor209(final UnoObject _this, final Object handle,final int color)
    {
        com.foreign.Fuse.Controls.Native.Android.TextInput.SetCursorDrawableColor209(_this, handle,color);
    }
    
    public static void SetImeOptions210(final Object handle,final int value)
    {
        com.foreign.Fuse.Controls.Native.Android.TextInput.SetImeOptions210(handle,value);
    }
    
    public static void SetInputType211(final Object handle,final int value)
    {
        com.foreign.Fuse.Controls.Native.Android.TextInput.SetInputType211(handle,value);
    }
    
    public static void SetPlaceholderColor212(final Object handle,final int value)
    {
        com.foreign.Fuse.Controls.Native.Android.TextInput.SetPlaceholderColor212(handle,value);
    }
    
    public static void SetPlaceholderText213(final Object handle,final String value)
    {
        com.foreign.Fuse.Controls.Native.Android.TextInput.SetPlaceholderText213(handle,value);
    }
    
    public static void SetSelectionColor214(final Object handle,final int color)
    {
        com.foreign.Fuse.Controls.Native.Android.TextInput.SetSelectionColor214(handle,color);
    }
    
    public static Object Create215()
    {
        return (Object) com.foreign.Fuse.Controls.Native.Android.TextureView.Create215();
    }
    
    public static native void callUno_Fuse_Controls_Native_Android_TextureView_OnSurfaceTextureAvailable217(final UnoObject jthis,final Object surface,long surfacePtr);
    public static native void callUno_Fuse_Controls_Native_Android_TextureView_OnSurfaceTextureDestroyed218(final UnoObject jthis);
    public static void InstallSurfaceListener216(final UnoObject _this, final Object handle)
    {
        com.foreign.Fuse.Controls.Native.Android.TextureView.InstallSurfaceListener216(_this, handle);
    }
    
    public static Object Create219()
    {
        return (Object) com.foreign.Fuse.Controls.Native.Android.TextView.Create219();
    }
    
    public static void SetFont220(final Object handle,final Object fontHandle)
    {
        com.foreign.Fuse.Controls.Native.Android.TextView.SetFont220(handle,fontHandle);
    }
    
    public static void SetFontSize221(final Object handle,final float size)
    {
        com.foreign.Fuse.Controls.Native.Android.TextView.SetFontSize221(handle,size);
    }
    
    public static void SetLineSpacing222(final Object handle,final float spacing)
    {
        com.foreign.Fuse.Controls.Native.Android.TextView.SetLineSpacing222(handle,spacing);
    }
    
    public static void SetMaxLength223(final Object handle,final int maxLength)
    {
        com.foreign.Fuse.Controls.Native.Android.TextView.SetMaxLength223(handle,maxLength);
    }
    
    public static void SetText224(final Object handle,final String text)
    {
        com.foreign.Fuse.Controls.Native.Android.TextView.SetText224(handle,text);
    }
    
    public static void SetTextAlignment225(final Object handle,final int alignment)
    {
        com.foreign.Fuse.Controls.Native.Android.TextView.SetTextAlignment225(handle,alignment);
    }
    
    public static void SetTextColor226(final Object handle,final int color)
    {
        com.foreign.Fuse.Controls.Native.Android.TextView.SetTextColor226(handle,color);
    }
    
    public static void SetTextWrapping227(final Object handle,final boolean wrap)
    {
        com.foreign.Fuse.Controls.Native.Android.TextView.SetTextWrapping227(handle,wrap);
    }
    
    public static Object Create228()
    {
        return (Object) com.foreign.Fuse.Controls.Native.Android.TimePickerView.Create228();
    }
    
    public static long GetTimeInMsSince1970InUtc229(final UnoObject _this, final Object timePickerHandle)
    {
        return (long) com.foreign.Fuse.Controls.Native.Android.TimePickerView.GetTimeInMsSince1970InUtc229(_this, timePickerHandle);
    }
    
    public static void SetIs24HourView230(final UnoObject _this, final Object timePickerHandle,final boolean value)
    {
        com.foreign.Fuse.Controls.Native.Android.TimePickerView.SetIs24HourView230(_this, timePickerHandle,value);
    }
    
    public static void SetTime231(final UnoObject _this, final Object timePickerHandle,final long msSince1970InUtc)
    {
        com.foreign.Fuse.Controls.Native.Android.TimePickerView.SetTime231(_this, timePickerHandle,msSince1970InUtc);
    }
    
    public static Object CreateFromBundleFile232(final String bundlePath)
    {
        return (Object) com.foreign.Fuse.Controls.Native.Android.Typeface.CreateFromBundleFile232(bundlePath);
    }
    
    public static Object CreateFromFileImpl233(final String path)
    {
        return (Object) com.foreign.Fuse.Controls.Native.Android.Typeface.CreateFromFileImpl233(path);
    }
    
    public static Object GetDefault234()
    {
        return (Object) com.foreign.Fuse.Controls.Native.Android.Typeface.GetDefault234();
    }
    
    public static void AddView235(final Object parentHandle,final Object childHandle)
    {
        com.foreign.Fuse.Controls.Native.Android.ViewGroup.AddView235(parentHandle,childHandle);
    }
    
    public static void AddView1236(final Object parentHandle,final Object childHandle,final int index)
    {
        com.foreign.Fuse.Controls.Native.Android.ViewGroup.AddView1236(parentHandle,childHandle,index);
    }
    
    public static Object Create237()
    {
        return (Object) com.foreign.Fuse.Controls.Native.Android.ViewGroup.Create237();
    }
    
    public static void RemoveView238(final Object parentHandle,final Object childHandle)
    {
        com.foreign.Fuse.Controls.Native.Android.ViewGroup.RemoveView238(parentHandle,childHandle);
    }
    
    public static Object Download1239(final String url)
    {
        return (Object) com.foreign.Fuse.Controls.Native.ImageLoader.ImageHandlePromise.Download1239(url);
    }
    
    public static Object LoadFile240(final String filePath)
    {
        return (Object) com.foreign.Fuse.Controls.Native.ImageLoader.LoadFile240(filePath);
    }
    
    public static Object LoadUri241(final String uri)
    {
        return (Object) com.foreign.Fuse.Controls.Native.ImageLoader.LoadUri241(uri);
    }
    
    public static void Release242(final Object bitmap)
    {
        com.foreign.Fuse.Controls.Native.ImageLoader.Release242(bitmap);
    }
    
    public static Object AllocPixelBuffer243(final int w,final int h)
    {
        return (Object) com.foreign.Fuse.Controls.Native.NativeViewRenderer.AllocPixelBuffer243(w,h);
    }
    
    public static void FreePixelBuffer244(final Object bitmap)
    {
        com.foreign.Fuse.Controls.Native.NativeViewRenderer.FreePixelBuffer244(bitmap);
    }
    
    public static void Upload245(final Object viewHandle,final Object pixelBuffer,final boolean reuse,final int w,final int h)
    {
        com.foreign.Fuse.Controls.Native.NativeViewRenderer.Upload245(viewHandle,pixelBuffer,reuse,w,h);
    }
    
    public static Object InstantiateViewGroupImpl246()
    {
        return (Object) com.foreign.Fuse.Controls.Native.ViewFactory.InstantiateViewGroupImpl246();
    }
    
    public static native Object callUno_Fuse_Controls_Native_ViewHandle_NativeHandleGet248(final UnoObject jthis);
    public static void BringToFront247(final UnoObject _this)
    {
        com.foreign.Fuse.Controls.Native.ViewHandle.BringToFront247(_this);
    }
    
    public static void CopyState249(final UnoObject sourceHandle,final UnoObject destHandle)
    {
        com.foreign.Fuse.Controls.Native.ViewHandle.CopyState249(sourceHandle,destHandle);
    }
    
    public static String Format250(final UnoObject _this)
    {
        return (String) com.foreign.Fuse.Controls.Native.ViewHandle.Format250(_this);
    }
    
    public static int GetMeasuredHeight251(final Object handle)
    {
        return (int) com.foreign.Fuse.Controls.Native.ViewHandle.GetMeasuredHeight251(handle);
    }
    
    public static int GetMeasuredWidth252(final Object handle)
    {
        return (int) com.foreign.Fuse.Controls.Native.ViewHandle.GetMeasuredWidth252(handle);
    }
    
    public static int IndexOfChild253(final UnoObject _this, final UnoObject childHandle)
    {
        return (int) com.foreign.Fuse.Controls.Native.ViewHandle.IndexOfChild253(_this, childHandle);
    }
    
    public static void InsertChild254(final UnoObject _this, final UnoObject childHandle)
    {
        com.foreign.Fuse.Controls.Native.ViewHandle.InsertChild254(_this, childHandle);
    }
    
    public static void InsertChild1255(final UnoObject _this, final UnoObject childHandle,final int index)
    {
        com.foreign.Fuse.Controls.Native.ViewHandle.InsertChild1255(_this, childHandle,index);
    }
    
    public static void InvalidateImpl256(final UnoObject _this)
    {
        com.foreign.Fuse.Controls.Native.ViewHandle.InvalidateImpl256(_this);
    }
    
    public static boolean IsViewGroup257(final UnoObject _this)
    {
        return (boolean) com.foreign.Fuse.Controls.Native.ViewHandle.IsViewGroup257(_this);
    }
    
    public static void Measure1258(final Object handle,final int w,final int h,final boolean hasX,final boolean hasY)
    {
        com.foreign.Fuse.Controls.Native.ViewHandle.Measure1258(handle,w,h,hasX,hasY);
    }
    
    public static void RemoveChild259(final UnoObject _this, final UnoObject childHandle)
    {
        com.foreign.Fuse.Controls.Native.ViewHandle.RemoveChild259(_this, childHandle);
    }
    
    public static void ResetLayoutParams260(final UnoObject _this)
    {
        com.foreign.Fuse.Controls.Native.ViewHandle.ResetLayoutParams260(_this);
    }
    
    public static void SetBackgroundColor261(final UnoObject _this, final int color)
    {
        com.foreign.Fuse.Controls.Native.ViewHandle.SetBackgroundColor261(_this, color);
    }
    
    public static void SetClipToBounds262(final UnoObject _this, final boolean clipToBounds)
    {
        com.foreign.Fuse.Controls.Native.ViewHandle.SetClipToBounds262(_this, clipToBounds);
    }
    
    public static void SetEnabled263(final UnoObject _this, final boolean value)
    {
        com.foreign.Fuse.Controls.Native.ViewHandle.SetEnabled263(_this, value);
    }
    
    public static void SetHitTestEnabled264(final UnoObject _this, final boolean enabled)
    {
        com.foreign.Fuse.Controls.Native.ViewHandle.SetHitTestEnabled264(_this, enabled);
    }
    
    public static void SetIsVisible265(final UnoObject _this, final boolean isVisible)
    {
        com.foreign.Fuse.Controls.Native.ViewHandle.SetIsVisible265(_this, isVisible);
    }
    
    public static void SetOpacity266(final UnoObject _this, final float value)
    {
        com.foreign.Fuse.Controls.Native.ViewHandle.SetOpacity266(_this, value);
    }
    
    public static void UpdateTransform267(final UnoObject _this, final float scaleX,final float scaleY,final float rotation,final float rotationX,final float rotationY)
    {
        com.foreign.Fuse.Controls.Native.ViewHandle.UpdateTransform267(_this, scaleX,scaleY,rotation,rotationX,rotationY);
    }
    
    public static void UpdateViewRectImpl268(final UnoObject _this, final int x,final int y,final int w,final int h)
    {
        com.foreign.Fuse.Controls.Native.ViewHandle.UpdateViewRectImpl268(_this, x,y,w,h);
    }
    
    public static native void callUno_Fuse_Controls_VideoImpl_Android_MediaPlayer_OnPrepared270(final UnoObject jthis);
    public static native void callUno_Fuse_Controls_VideoImpl_Android_MediaPlayer_OnCompletion271(final UnoObject jthis);
    public static native void callUno_Fuse_Controls_VideoImpl_Android_MediaPlayer_OnError272(final UnoObject jthis,final int what,final int extra);
    public static native void callUno_Fuse_Controls_VideoImpl_Android_MediaPlayer_OnBuffer273(final UnoObject jthis,final int percent);
    public static Object CreateMediaPlayer269(final UnoObject _this, final Object surfaceHandle)
    {
        return (Object) com.foreign.Fuse.Controls.VideoImpl.Android.MediaPlayer.CreateMediaPlayer269(_this, surfaceHandle);
    }
    
    public static Object CreateSurface274(final UnoObject _this, final Object surfaceTexture)
    {
        return (Object) com.foreign.Fuse.Controls.VideoImpl.Android.MediaPlayer.CreateSurface274(_this, surfaceTexture);
    }
    
    public static native void callUno_Fuse_Controls_VideoImpl_Android_MediaPlayer_OnFrameAvailable276(final UnoObject jthis);
    public static Object CreateSurfaceTexture275(final UnoObject _this, final int glHandle)
    {
        return (Object) com.foreign.Fuse.Controls.VideoImpl.Android.MediaPlayer.CreateSurfaceTexture275(_this, glHandle);
    }
    
    public static void Dispose1277(final Object mediaplayerHandle,final Object surfaceHandle,final Object surfaceTextureHandle)
    {
        com.foreign.Fuse.Controls.VideoImpl.Android.MediaPlayer.Dispose1277(mediaplayerHandle,surfaceHandle,surfaceTextureHandle);
    }
    
    public static int GetCurrentPosition278(final Object handle)
    {
        return (int) com.foreign.Fuse.Controls.VideoImpl.Android.MediaPlayer.GetCurrentPosition278(handle);
    }
    
    public static int GetDuration279(final Object handle)
    {
        return (int) com.foreign.Fuse.Controls.VideoImpl.Android.MediaPlayer.GetDuration279(handle);
    }
    
    public static int GetHeight280(final Object handle)
    {
        return (int) com.foreign.Fuse.Controls.VideoImpl.Android.MediaPlayer.GetHeight280(handle);
    }
    
    public static int GetOrientation281(final Object handle,final String dataSorucePath)
    {
        return (int) com.foreign.Fuse.Controls.VideoImpl.Android.MediaPlayer.GetOrientation281(handle,dataSorucePath);
    }
    
    public static int GetWidth282(final Object handle)
    {
        return (int) com.foreign.Fuse.Controls.VideoImpl.Android.MediaPlayer.GetWidth282(handle);
    }
    
    public static boolean IsHardwareAccelerated283()
    {
        return (boolean) com.foreign.Fuse.Controls.VideoImpl.Android.MediaPlayer.IsHardwareAccelerated283();
    }
    
    public static native void callUno_Fuse_Controls_VideoImpl_Android_MediaPlayer_OnErrorOccurred285(final UnoObject jthis,final String msg);
    public static void LoadAsyncAsset284(final UnoObject _this, final Object handle,final String assetName)
    {
        com.foreign.Fuse.Controls.VideoImpl.Android.MediaPlayer.LoadAsyncAsset284(_this, handle,assetName);
    }
    
    public static void LoadAsyncUrl286(final UnoObject _this, final Object handle,final String url)
    {
        com.foreign.Fuse.Controls.VideoImpl.Android.MediaPlayer.LoadAsyncUrl286(_this, handle,url);
    }
    
    public static void Pause1287(final Object handle)
    {
        com.foreign.Fuse.Controls.VideoImpl.Android.MediaPlayer.Pause1287(handle);
    }
    
    public static void Play1288(final Object handle)
    {
        com.foreign.Fuse.Controls.VideoImpl.Android.MediaPlayer.Play1288(handle);
    }
    
    public static void SeekTo289(final Object handle,final int position)
    {
        com.foreign.Fuse.Controls.VideoImpl.Android.MediaPlayer.SeekTo289(handle,position);
    }
    
    public static void SetVolume290(final Object handle,final float left,final float right)
    {
        com.foreign.Fuse.Controls.VideoImpl.Android.MediaPlayer.SetVolume290(handle,left,right);
    }
    
    public static void UpdateTexture1291(final Object surfaceTextureHandle)
    {
        com.foreign.Fuse.Controls.VideoImpl.Android.MediaPlayer.UpdateTexture1291(surfaceTextureHandle);
    }
    
    public static void CopyState292(final Object sourceHandle,final Object targetHandle,final boolean updateTextAlignment,final boolean isMultiline,final int width,final int height)
    {
        com.foreign.Fuse.Controls.TextEditRenderer.CopyState292(sourceHandle,targetHandle,updateTextAlignment,isMultiline,width,height);
    }
    
    public static Object CreateTextEdit293()
    {
        return (Object) com.foreign.Fuse.Controls.TextEditRenderer.CreateTextEdit293();
    }
    
    public static void ConcatTransform294(final Object cp,final Object m)
    {
        com.foreign.Fuse.Drawing.AndroidSurface.ConcatTransform294(cp,m);
    }
    
    public static Object CreateFillPaint295()
    {
        return (Object) com.foreign.Fuse.Drawing.AndroidSurface.CreateFillPaint295();
    }
    
    public static Object CreateLinearGradient296(final com.uno.IntArray colors,final com.uno.FloatArray stops)
    {
        return (Object) com.foreign.Fuse.Drawing.AndroidSurface.CreateLinearGradient296(colors,stops);
    }
    
    public static Object CreateStrokedPaint297(final float width,final int fjoin,final int fcap,final float miterLimit)
    {
        return (Object) com.foreign.Fuse.Drawing.AndroidSurface.CreateStrokedPaint297(width,fjoin,fcap,miterLimit);
    }
    
    public static void FillPathImage298(final Object cp,final Object pathAsObject,final Object imageAsObject,final float originX,final float originY,final float tileSizeX,final float tileSizeY,final float width,final float height,final boolean eoFill,final Object paintAsObject)
    {
        com.foreign.Fuse.Drawing.AndroidSurface.FillPathImage298(cp,pathAsObject,imageAsObject,originX,originY,tileSizeX,tileSizeY,width,height,eoFill,paintAsObject);
    }
    
    public static void FillPathLinearGradient299(final Object cp,final Object path,final Object gradientStore,final float startX,final float startY,final float endX,final float endY,final boolean eoFill,final Object pretendPaint)
    {
        com.foreign.Fuse.Drawing.AndroidSurface.FillPathLinearGradient299(cp,path,gradientStore,startX,startY,endX,endY,eoFill,pretendPaint);
    }
    
    public static void FillPathSolidColor300(final Object cp,final Object pathAsObject,final int color,final boolean eoFill,final Object pretendPaint)
    {
        com.foreign.Fuse.Drawing.AndroidSurface.FillPathSolidColor300(cp,pathAsObject,color,eoFill,pretendPaint);
    }
    
    public static Object NewContext301()
    {
        return (Object) com.foreign.Fuse.Drawing.AndroidSurface.NewContext301();
    }
    
    public static void PathClose302(final Object pathAsObject)
    {
        com.foreign.Fuse.Drawing.AndroidSurface.PathClose302(pathAsObject);
    }
    
    public static Object PathCreateMutable303()
    {
        return (Object) com.foreign.Fuse.Drawing.AndroidSurface.PathCreateMutable303();
    }
    
    public static void PathCurveTo304(final Object pathAsObject,final float x,final float y,final float ax,final float ay,final float bx,final float by)
    {
        com.foreign.Fuse.Drawing.AndroidSurface.PathCurveTo304(pathAsObject,x,y,ax,ay,bx,by);
    }
    
    public static void PathLineTo305(final Object pathAsObject,final float x,final float y)
    {
        com.foreign.Fuse.Drawing.AndroidSurface.PathLineTo305(pathAsObject,x,y);
    }
    
    public static void PathMoveTo306(final Object pathAsObject,final float x,final float y)
    {
        com.foreign.Fuse.Drawing.AndroidSurface.PathMoveTo306(pathAsObject,x,y);
    }
    
    public static void recycleBitmap307(final Object bit)
    {
        com.foreign.Fuse.Drawing.AndroidSurface.recycleBitmap307(bit);
    }
    
    public static void RestoreContextState308(final Object cp)
    {
        com.foreign.Fuse.Drawing.AndroidSurface.RestoreContextState308(cp);
    }
    
    public static int SaveContextState309(final Object cp)
    {
        return (int) com.foreign.Fuse.Drawing.AndroidSurface.SaveContextState309(cp);
    }
    
    public static Object ToMatrix310(final UnoObject _this, final com.uno.FloatArray matrix)
    {
        return (Object) com.foreign.Fuse.Drawing.AndroidSurface.ToMatrix310(_this, matrix);
    }
    
    public static void BeginImpl311(final Object _context,final int width,final int height,final int glTextureId)
    {
        com.foreign.Fuse.Drawing.GraphicsSurface.BeginImpl311(_context,width,height,glTextureId);
    }
    
    public static void EndImpl312(final Object context)
    {
        com.foreign.Fuse.Drawing.GraphicsSurface.EndImpl312(context);
    }
    
    public static void LoadBitmap313(final Object context,final int width,final int height)
    {
        com.foreign.Fuse.Drawing.GraphicsSurface.LoadBitmap313(context,width,height);
    }
    
    public static Object LoadImage314(final int glTextureId,final int width,final int height)
    {
        return (Object) com.foreign.Fuse.Drawing.GraphicsSurface.LoadImage314(glTextureId,width,height);
    }
    
    public static Object DummyBitmap315()
    {
        return (Object) com.foreign.Fuse.Drawing.NativeSurface.DummyBitmap315();
    }
    
    public static void SetCanvas316(final Object context,final Object canvas)
    {
        com.foreign.Fuse.Drawing.NativeSurface.SetCanvas316(context,canvas);
    }
    
    public static Object Create317(final String text)
    {
        return (Object) com.foreign.Fuse.Text.Bidirectional.Implementation.JavaRuns.Create317(text);
    }
    
    public static int GetBaseLevel318(final Object handle)
    {
        return (int) com.foreign.Fuse.Text.Bidirectional.Implementation.JavaRuns.GetBaseLevel318(handle);
    }
    
    public static int GetRunCount319(final Object handle)
    {
        return (int) com.foreign.Fuse.Text.Bidirectional.Implementation.JavaRuns.GetRunCount319(handle);
    }
    
    public static int GetRunLevel320(final Object handle,final int run)
    {
        return (int) com.foreign.Fuse.Text.Bidirectional.Implementation.JavaRuns.GetRunLevel320(handle,run);
    }
    
    public static int GetRunLimit321(final Object handle,final int run)
    {
        return (int) com.foreign.Fuse.Text.Bidirectional.Implementation.JavaRuns.GetRunLimit321(handle,run);
    }
    
    public static int GetRunStart322(final Object handle,final int run)
    {
        return (int) com.foreign.Fuse.Text.Bidirectional.Implementation.JavaRuns.GetRunStart322(handle,run);
    }
    
    public static void CopyLineBreaks323(final String text,final Object outByteBufferHandle)
    {
        com.foreign.Fuse.Text.Implementation.JavaLineBreaks.CopyLineBreaks323(text,outByteBufferHandle);
    }
    
    public static Object LaunchIntent324(final String action,final String uri,final String packageName,final String className)
    {
        return (Object) com.foreign.Fuse.Android.Bindings.AndroidDeviceInterop.LaunchIntent324(action,uri,packageName,className);
    }
    
    public static Object MakeBufferInputStream1325(final Object buf)
    {
        return (Object) com.foreign.Fuse.Android.Bindings.AndroidDeviceInterop.MakeBufferInputStream1325(buf);
    }
    
    public static Object MakeMediaDataSource1326(final Object buf)
    {
        return (Object) com.foreign.Fuse.Android.Bindings.AndroidDeviceInterop.MakeMediaDataSource1326(buf);
    }
    
    public static native String callUno_Uno_IO_BundleFile_BundlePathGet328(final UnoObject jthis);
    public static Object OpenAssetFileDescriptor327(final UnoObject bundle)
    {
        return (Object) com.foreign.Fuse.Android.Bindings.AndroidDeviceInterop.OpenAssetFileDescriptor327(bundle);
    }
    
    public static void AddJavascriptInterface329(final Object handle,final String name,final com.foreign.Uno.Action_String resultHandler)
    {
        com.foreign.Fuse.Android.Controls.WebViewUtils.WebViewForeign.AddJavascriptInterface329(handle,name,resultHandler);
    }
    
    public static boolean CanGoBack330(final Object handle)
    {
        return (boolean) com.foreign.Fuse.Android.Controls.WebViewUtils.WebViewForeign.CanGoBack330(handle);
    }
    
    public static boolean CanGoForward331(final Object handle)
    {
        return (boolean) com.foreign.Fuse.Android.Controls.WebViewUtils.WebViewForeign.CanGoForward331(handle);
    }
    
    public static native void callUno_Action_int(final com.foreign.Uno.Action_int theDelegate,final int arg);
    public static Object CreateAndSetWebChromeClient332(final Object webViewHandle,final com.foreign.Uno.Action_int onProgress)
    {
        return (Object) com.foreign.Fuse.Android.Controls.WebViewUtils.WebViewForeign.CreateAndSetWebChromeClient332(webViewHandle,onProgress);
    }
    
    public static native void callUno_Action(final com.foreign.Uno.Action theDelegate);
    public static native boolean callUno_Func(final com.foreign.Uno.Func theDelegate);
    public static Object CreateAndSetWebViewClient333(final Object webViewHandle,final com.foreign.Uno.Action loaded,final com.foreign.Uno.Action started,final com.foreign.Uno.Action changed,final com.foreign.Uno.Action_String onCustomURI,final com.uno.StringArray customURIs,final com.foreign.Uno.Func hasUriSchemeHandler)
    {
        return (Object) com.foreign.Fuse.Android.Controls.WebViewUtils.WebViewForeign.CreateAndSetWebViewClient333(webViewHandle,loaded,started,changed,onCustomURI,customURIs,hasUriSchemeHandler);
    }
    
    public static Object CreateWebView334(final boolean zoomEnabled,final boolean scrollEnabled)
    {
        return (Object) com.foreign.Fuse.Android.Controls.WebViewUtils.WebViewForeign.CreateWebView334(zoomEnabled,scrollEnabled);
    }
    
    public static double GetProgress335(final Object handle)
    {
        return (double) com.foreign.Fuse.Android.Controls.WebViewUtils.WebViewForeign.GetProgress335(handle);
    }
    
    public static String GetTitle336(final Object handle)
    {
        return (String) com.foreign.Fuse.Android.Controls.WebViewUtils.WebViewForeign.GetTitle336(handle);
    }
    
    public static String GetUrl337(final Object handle)
    {
        return (String) com.foreign.Fuse.Android.Controls.WebViewUtils.WebViewForeign.GetUrl337(handle);
    }
    
    public static void GoBack338(final Object handle)
    {
        com.foreign.Fuse.Android.Controls.WebViewUtils.WebViewForeign.GoBack338(handle);
    }
    
    public static void GoForward339(final Object handle)
    {
        com.foreign.Fuse.Android.Controls.WebViewUtils.WebViewForeign.GoForward339(handle);
    }
    
    public static void LoadHtml340(final Object handle,final String html,final String baseUrl)
    {
        com.foreign.Fuse.Android.Controls.WebViewUtils.WebViewForeign.LoadHtml340(handle,html,baseUrl);
    }
    
    public static void LoadUrl341(final Object handle,final String url)
    {
        com.foreign.Fuse.Android.Controls.WebViewUtils.WebViewForeign.LoadUrl341(handle,url);
    }
    
    public static void Reload342(final Object handle)
    {
        com.foreign.Fuse.Android.Controls.WebViewUtils.WebViewForeign.Reload342(handle);
    }
    
    public static void StopLoading343(final Object handle)
    {
        com.foreign.Fuse.Android.Controls.WebViewUtils.WebViewForeign.StopLoading343(handle);
    }
    
    public static void ClearRoot1344(final Object handle)
    {
        com.foreign.Fuse.Android.AppRoot.ClearRoot1344(handle);
    }
    
    public static native void callUno_Fuse_Android_AppRoot_OnTouchEvent__346(final Object motionEvent,long motionEventPtr);
    public static Object CreateRootView345()
    {
        return (Object) com.foreign.Fuse.Android.AppRoot.CreateRootView345();
    }
    
    public static void SetRootView1347(final Object handle,final Object rootHandle)
    {
        com.foreign.Fuse.Android.AppRoot.SetRootView1347(handle,rootHandle);
    }
    
    public static Object CreateBitmapARGB8888Impl348(final int width,final int height)
    {
        return (Object) com.foreign.Fuse.Android.Bitmap.CreateBitmapARGB8888Impl348(width,height);
    }
    
    public static void EraseColor1349(final Object handle,final int color)
    {
        com.foreign.Fuse.Android.Bitmap.EraseColor1349(handle,color);
    }
    
    public static void Recycle1350(final Object handle)
    {
        com.foreign.Fuse.Android.Bitmap.Recycle1350(handle);
    }
    
    public static Object Create351(final Object bitmapHandle)
    {
        return (Object) com.foreign.Fuse.Android.Canvas.Create351(bitmapHandle);
    }
    
    public static void Translate1352(final Object handle,final float dx,final float dy)
    {
        com.foreign.Fuse.Android.Canvas.Translate1352(handle,dx,dy);
    }
    
    public static void TexImage2D353(final int target,final int level,final Object bitmap,final int border)
    {
        com.foreign.Fuse.Android.GLUtils.TexImage2D353(target,level,bitmap,border);
    }
    
    public static Object Create354(final String text,final int bufStart,final int bufEnd,final Object paintHandle,final int outerWidth,final int align,final float spacingMult,final float spacingAdd,final boolean includePad,final int truncateAt,final int ellipsizedWith)
    {
        return (Object) com.foreign.Fuse.Android.StaticLayout.Create354(text,bufStart,bufEnd,paintHandle,outerWidth,align,spacingMult,spacingAdd,includePad,truncateAt,ellipsizedWith);
    }
    
    public static Object Create1355(final String text,final Object paintHandle,final int width,final int align,final float spacingMult,final float spacingAdd,final boolean includePad)
    {
        return (Object) com.foreign.Fuse.Android.StaticLayout.Create1355(text,paintHandle,width,align,spacingMult,spacingAdd,includePad);
    }
    
    public static void Draw1356(final Object layoutHandle,final Object canvasHandle)
    {
        com.foreign.Fuse.Android.StaticLayout.Draw1356(layoutHandle,canvasHandle);
    }
    
    public static float GetDesiredWidthImpl357(final String text,final Object paintHandle)
    {
        return (float) com.foreign.Fuse.Android.StaticLayout.GetDesiredWidthImpl357(text,paintHandle);
    }
    
    public static int GetEllipsizedWidth358(final Object handle)
    {
        return (int) com.foreign.Fuse.Android.StaticLayout.GetEllipsizedWidth358(handle);
    }
    
    public static int GetHeight359(final Object handle)
    {
        return (int) com.foreign.Fuse.Android.StaticLayout.GetHeight359(handle);
    }
    
    public static int GetLineBaseline1360(final Object handle,final int line)
    {
        return (int) com.foreign.Fuse.Android.StaticLayout.GetLineBaseline1360(handle,line);
    }
    
    public static int GetLineCount361(final Object handle)
    {
        return (int) com.foreign.Fuse.Android.StaticLayout.GetLineCount361(handle);
    }
    
    public static int GetLineEnd1362(final Object handle,final int line)
    {
        return (int) com.foreign.Fuse.Android.StaticLayout.GetLineEnd1362(handle,line);
    }
    
    public static float GetLineLeft1363(final Object handle,final int line)
    {
        return (float) com.foreign.Fuse.Android.StaticLayout.GetLineLeft1363(handle,line);
    }
    
    public static int GetLineStart1364(final Object handle,final int line)
    {
        return (int) com.foreign.Fuse.Android.StaticLayout.GetLineStart1364(handle,line);
    }
    
    public static int GetWidth365(final Object handle)
    {
        return (int) com.foreign.Fuse.Android.StaticLayout.GetWidth365(handle);
    }
    
    public static int GetStatusBarColor366()
    {
        return (int) com.foreign.Fuse.Android.StatusBarHelper.GetStatusBarColor366();
    }
    
    public static native void callUno_Fuse_Android_StatusBarConfig_UpdateStatusBar368();
    public static void InstallGlobalListener367()
    {
        com.foreign.Fuse.Android.StatusBarHelper.InstallGlobalListener367();
    }
    
    public static boolean SetStatusBarColor369(final int color)
    {
        return (boolean) com.foreign.Fuse.Android.StatusBarHelper.SetStatusBarColor369(color);
    }
    
    public static Object Create370()
    {
        return (Object) com.foreign.Fuse.Android.TextPaint.Create370();
    }
    
    public static void GetTextBounds371(final Object handle,final String text,final int start,final int end,final com.uno.IntArray r)
    {
        com.foreign.Fuse.Android.TextPaint.GetTextBounds371(handle,text,start,end,r);
    }
    
    public static void SetAntiAlias372(final Object handle,final boolean value)
    {
        com.foreign.Fuse.Android.TextPaint.SetAntiAlias372(handle,value);
    }
    
    public static void SetColor373(final Object handle,final int color)
    {
        com.foreign.Fuse.Android.TextPaint.SetColor373(handle,color);
    }
    
    public static void SetTextSize374(final Object handle,final float textSize)
    {
        com.foreign.Fuse.Android.TextPaint.SetTextSize374(handle,textSize);
    }
    
    public static void SetTypeface375(final Object paintHandle,final Object typefaceHandle)
    {
        com.foreign.Fuse.Android.TextPaint.SetTypeface375(paintHandle,typefaceHandle);
    }
    
    public static String GetCacheDirectory376()
    {
        return (String) com.foreign.Fuse.FileSystem.AndroidPaths.GetCacheDirectory376();
    }
    
    public static String GetExternalCacheDirectory377()
    {
        return (String) com.foreign.Fuse.FileSystem.AndroidPaths.GetExternalCacheDirectory377();
    }
    
    public static String GetExternalFilesDirectory378()
    {
        return (String) com.foreign.Fuse.FileSystem.AndroidPaths.GetExternalFilesDirectory378();
    }
    
    public static String GetFilesDirectory379()
    {
        return (String) com.foreign.Fuse.FileSystem.AndroidPaths.GetFilesDirectory379();
    }
    
    public static String GetCacheDirectory380()
    {
        return (String) com.foreign.Fuse.FileSystem.UnifiedPaths.GetCacheDirectory380();
    }
    
    public static String GetDataDirectory381()
    {
        return (String) com.foreign.Fuse.FileSystem.UnifiedPaths.GetDataDirectory381();
    }
    
    public static void Crop382(final String path,final int width,final int height,final int x,final int y,final com.foreign.Uno.Action_String onSuccess,final com.foreign.Uno.Action_String onFail,final boolean performInPlace)
    {
        com.foreign.Fuse.ImageTools.AndroidImageUtils.Crop382(path,width,height,x,y,onSuccess,onFail,performInPlace);
    }
    
    public static void GetBase64FromImage383(final String path,final com.foreign.Uno.Action_String onSuccess,final com.foreign.Uno.Action_String onFail)
    {
        com.foreign.Fuse.ImageTools.AndroidImageUtils.GetBase64FromImage383(path,onSuccess,onFail);
    }
    
    public static void GetImageFromBase64384(final String b64,final com.foreign.Uno.Action_String onSuccess,final com.foreign.Uno.Action_String onFail)
    {
        com.foreign.Fuse.ImageTools.AndroidImageUtils.GetImageFromBase64384(b64,onSuccess,onFail);
    }
    
    public static void GetImageFromBuffer385(final com.uno.ByteArray bytes,final com.foreign.Uno.Action_String onSuccess,final com.foreign.Uno.Action_String onFail)
    {
        com.foreign.Fuse.ImageTools.AndroidImageUtils.GetImageFromBuffer385(bytes,onSuccess,onFail);
    }
    
    public static String GetImageFromBufferSync386(final com.uno.ByteArray bytes)
    {
        return (String) com.foreign.Fuse.ImageTools.AndroidImageUtils.GetImageFromBufferSync386(bytes);
    }
    
    public static void GetSizeInternal387(final String path,final com.uno.IntArray values)
    {
        com.foreign.Fuse.ImageTools.AndroidImageUtils.GetSizeInternal387(path,values);
    }
    
    public static void Resize388(final String path,final int width,final int height,final int mode,final com.foreign.Uno.Action_String onSuccess,final com.foreign.Uno.Action_String onFail,final boolean performInPlace)
    {
        com.foreign.Fuse.ImageTools.AndroidImageUtils.Resize388(path,width,height,mode,onSuccess,onFail,performInPlace);
    }
    
    public static void HandleIntent389(final UnoObject _this, final int resultCode,final Object intent,final Object photo,final com.foreign.Uno.Action_String onComplete,final com.foreign.Uno.Action_String onFail)
    {
        com.foreign.Fuse.Camera.TakePictureCallback.HandleIntent389(_this, resultCode,intent,photo,onComplete,onFail);
    }
    
    public static Object CreateImage390()
    {
        return (Object) com.foreign.Fuse.Camera.TakePictureCommand.CreateImage390();
    }
    
    public static Object CreateIntent391(final Object photo)
    {
        return (Object) com.foreign.Fuse.Camera.TakePictureCommand.CreateIntent391(photo);
    }
    
    public static void AddToCameraRollInternal392(final String path,final com.foreign.Uno.Action success,final com.foreign.Uno.Action_String reject)
    {
        com.foreign.Fuse.CameraRoll.AddPicturePermissionCheckCommand.AddToCameraRollInternal392(path,success,reject);
    }
    
    public static void HandleIntent393(final UnoObject _this, final int resultCode,final Object intent,final com.foreign.Uno.Action_String onComplete,final com.foreign.Uno.Action_String onFail)
    {
        com.foreign.Fuse.CameraRoll.SelectPictureClosure.HandleIntent393(_this, resultCode,intent,onComplete,onFail);
    }
    
    public static Object CreateIntent394()
    {
        return (Object) com.foreign.Fuse.CameraRoll.SelectPicturePermissionCheckCommand.CreateIntent394();
    }
    
    public static int GetBuildVersion395()
    {
        return (int) com.foreign.Fuse.AndroidProperties.GetBuildVersion395();
    }
    
    public static String GetReleaseVersion396()
    {
        return (String) com.foreign.Fuse.AndroidProperties.GetReleaseVersion396();
    }
    
    public static native Object callUno_Neovisionaries_WebSocketClient__webSocketGet398(final UnoObject jthis);
    public static void Close397(final UnoObject _this)
    {
        com.foreign.Neovisionaries.WebSocketClient.Close397(_this);
    }
    
    public static void Connect399(final UnoObject _this)
    {
        com.foreign.Neovisionaries.WebSocketClient.Connect399(_this);
    }
    
    public static native void callUno_Action_ByteArray(final com.foreign.Uno.Action_ByteArray theDelegate,final com.uno.ByteArray arg);
    public static native void callUno_Neovisionaries_WebSocketClient__webSocketSet398(final UnoObject jthis,final Object setVal,long setValPtr);
    public static void Create400(final UnoObject _this, final String url,final com.uno.StringArray protocols,final com.foreign.Uno.Action open,final com.foreign.Uno.Action close,final com.foreign.Uno.Action_String error,final com.foreign.Uno.Action_String receiveMessageHandler,final com.foreign.Uno.Action_ByteArray receiveDataHandler)
    {
        com.foreign.Neovisionaries.WebSocketClient.Create400(_this, url,protocols,open,close,error,receiveMessageHandler,receiveDataHandler);
    }
    
    public static void Send401(final UnoObject _this, final com.uno.ByteArray data)
    {
        com.foreign.Neovisionaries.WebSocketClient.Send401(_this, data);
    }
    
    public static void Send1402(final UnoObject _this, final String data)
    {
        com.foreign.Neovisionaries.WebSocketClient.Send1402(_this, data);
    }
    
    public static void SetHeader403(final UnoObject _this, final String key,final String value)
    {
        com.foreign.Neovisionaries.WebSocketClient.SetHeader403(_this, key,value);
    }
    
    public static String GetGUID404()
    {
        return (String) com.foreign.Outracks.Simulator.Client.DeviceInfo.GetGUID404();
    }
    
    public static String GetName405()
    {
        return (String) com.foreign.Outracks.Simulator.Client.DeviceInfo.GetName405();
    }
    
}
