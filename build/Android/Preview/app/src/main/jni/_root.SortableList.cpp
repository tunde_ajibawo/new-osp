// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/SortableList.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.OSP_accessor_So-4c215fe5.h>
#include <_root.OSP_FuseControl-b26a5bbf.h>
#include <_root.OSP_FuseReactiv-f855d0e4.h>
#include <_root.OSP_SortableLis-75b032ef.h>
#include <_root.OSP_SortableLis-bef53953.h>
#include <_root.SortableList.h>
#include <_root.SortableList.Template.h>
#include <Fuse.Controls.ScrollView.h>
#include <Fuse.Controls.StackPanel.h>
#include <Fuse.Controls.Text.h>
#include <Fuse.Controls.TextControl.h>
#include <Fuse.Elements.Alignment.h>
#include <Fuse.Elements.Element.h>
#include <Fuse.Font.h>
#include <Fuse.Reactive.BindingMode.h>
#include <Fuse.Reactive.Constan-264ec80.h>
#include <Fuse.Reactive.Constant.h>
#include <Fuse.Reactive.Data.h>
#include <Fuse.Reactive.DataBinding.h>
#include <Fuse.Reactive.Each.h>
#include <Fuse.Reactive.Expression.h>
#include <Fuse.Reactive.IExpression.h>
#include <Fuse.Reactive.Instantiator.h>
#include <Fuse.Reactive.JavaScript.h>
#include <Fuse.Reactive.Property.h>
#include <Uno.Bool.h>
#include <Uno.Float.h>
#include <Uno.Float4.h>
#include <Uno.Int.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.UX.NameTable.h>
#include <Uno.UX.Property.h>
#include <Uno.UX.Property1-1.h>
#include <Uno.UX.PropertyAccessor.h>
#include <Uno.UX.PropertyObject.h>
#include <Uno.UX.Selector.h>
#include <Uno.UX.Template.h>

namespace g{

// public partial sealed class SortableList :2
// {
// static SortableList() :64
static void SortableList__cctor_4_fn(uType* __type)
{
    SortableList::__g_static_nametable1_ = uArray::Init<uString*>(::g::Uno::String_typeof()->Array(), 1, uString::Const("parentScrollView"));
    SortableList::__selector0_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("Items"));
    SortableList::__selector1_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("Label"));
    SortableList::__selector2_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("Value"));
}

static void SortableList_build(uType* type)
{
    type->SetDependencies(
        ::g::Fuse::Font_typeof(),
        ::g::OSP_accessor_SortableList_Label_typeof());
    type->SetInterfaces(
        ::g::Uno::Collections::IList_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface0),
        ::g::Fuse::Scripting::IScriptObject_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface1),
        ::g::Fuse::IProperties_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface2),
        ::g::Fuse::INotifyUnrooted_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface3),
        ::g::Fuse::ISourceLocation_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface4),
        ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface5),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface6),
        ::g::Uno::Collections::IList_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface7),
        ::g::Uno::UX::IPropertyListener_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface8),
        ::g::Fuse::ITemplateSource_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface9),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Visual_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface10),
        ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface11),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface12),
        ::g::Fuse::Triggers::Actions::IShow_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface13),
        ::g::Fuse::Triggers::Actions::IHide_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface14),
        ::g::Fuse::Triggers::Actions::ICollapse_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface15),
        ::g::Fuse::IActualPlacement_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface16),
        ::g::Fuse::Animations::IResize_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface17),
        ::g::Fuse::Drawing::ISurfaceDrawable_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface18));
    type->SetFields(120,
        uObject_typeof(), offsetof(SortableList, _field_Items), 0,
        ::g::Uno::String_typeof(), offsetof(SortableList, _field_Label), 0,
        ::g::Fuse::Controls::ScrollView_typeof(), offsetof(SortableList, parentScrollView), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(SortableList, temp_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(uObject_typeof(), NULL), offsetof(SortableList, temp1_Items_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(uObject_typeof(), NULL), offsetof(SortableList, this_Items_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(SortableList, this_Label_inst), 0,
        ::g::Uno::UX::NameTable_typeof(), offsetof(SortableList, __g_nametable1), 0,
        ::g::Uno::String_typeof()->Array(), (uintptr_t)&SortableList::__g_static_nametable1_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&SortableList::__selector0_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&SortableList::__selector1_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&SortableList::__selector2_, uFieldFlagsStatic);
    type->Reflection.SetFunctions(7,
        new uFunction("get_Items", NULL, (void*)SortableList__get_Items_fn, 0, false, uObject_typeof(), 0),
        new uFunction("set_Items", NULL, (void*)SortableList__set_Items_fn, 0, false, uVoid_typeof(), 1, uObject_typeof()),
        new uFunction("get_Label", NULL, (void*)SortableList__get_Label_fn, 0, false, ::g::Uno::String_typeof(), 0),
        new uFunction("set_Label", NULL, (void*)SortableList__set_Label_fn, 0, false, uVoid_typeof(), 1, ::g::Uno::String_typeof()),
        new uFunction(".ctor", NULL, (void*)SortableList__New4_fn, 0, true, type, 1, ::g::Fuse::Controls::ScrollView_typeof()),
        new uFunction("SetItems", NULL, (void*)SortableList__SetItems_fn, 0, false, uVoid_typeof(), 2, uObject_typeof(), ::g::Uno::UX::IPropertyListener_typeof()),
        new uFunction("SetLabel", NULL, (void*)SortableList__SetLabel_fn, 0, false, uVoid_typeof(), 2, ::g::Uno::String_typeof(), ::g::Uno::UX::IPropertyListener_typeof()));
}

::g::Fuse::Controls::Panel_type* SortableList_typeof()
{
    static uSStrong< ::g::Fuse::Controls::Panel_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Fuse::Controls::Panel_typeof();
    options.FieldCount = 132;
    options.InterfaceCount = 19;
    options.DependencyCount = 2;
    options.ObjectSize = sizeof(SortableList);
    options.TypeSize = sizeof(::g::Fuse::Controls::Panel_type);
    type = (::g::Fuse::Controls::Panel_type*)uClassType::New("SortableList", options);
    type->fp_build_ = SortableList_build;
    type->fp_cctor_ = SortableList__cctor_4_fn;
    type->interface18.fp_Draw = (void(*)(uObject*, ::g::Fuse::Drawing::Surface*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableDraw_fn;
    type->interface18.fp_get_IsPrimary = (void(*)(uObject*, bool*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableget_IsPrimary_fn;
    type->interface18.fp_get_ElementSize = (void(*)(uObject*, ::g::Uno::Float2*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableget_ElementSize_fn;
    type->interface13.fp_Show = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsIShowShow_fn;
    type->interface15.fp_Collapse = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsICollapseCollapse_fn;
    type->interface14.fp_Hide = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsIHideHide_fn;
    type->interface17.fp_SetSize = (void(*)(uObject*, ::g::Uno::Float2*))::g::Fuse::Elements::Element__FuseAnimationsIResizeSetSize_fn;
    type->interface16.fp_get_ActualSize = (void(*)(uObject*, ::g::Uno::Float3*))::g::Fuse::Elements::Element__FuseIActualPlacementget_ActualSize_fn;
    type->interface16.fp_get_ActualPosition = (void(*)(uObject*, ::g::Uno::Float3*))::g::Fuse::Elements::Element__FuseIActualPlacementget_ActualPosition_fn;
    type->interface16.fp_add_Placed = (void(*)(uObject*, uDelegate*))::g::Fuse::Elements::Element__add_Placed_fn;
    type->interface16.fp_remove_Placed = (void(*)(uObject*, uDelegate*))::g::Fuse::Elements::Element__remove_Placed_fn;
    type->interface10.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Visual__UnoCollectionsIEnumerableFuseVisualGetEnumerator_fn;
    type->interface11.fp_Clear = (void(*)(uObject*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeClear_fn;
    type->interface11.fp_Contains = (void(*)(uObject*, void*, bool*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeContains_fn;
    type->interface7.fp_RemoveAt = (void(*)(uObject*, int32_t*))::g::Fuse::Visual__UnoCollectionsIListFuseNodeRemoveAt_fn;
    type->interface12.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Visual__UnoCollectionsIEnumerableFuseNodeGetEnumerator_fn;
    type->interface11.fp_get_Count = (void(*)(uObject*, int32_t*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeget_Count_fn;
    type->interface7.fp_get_Item = (void(*)(uObject*, int32_t*, uTRef))::g::Fuse::Visual__UnoCollectionsIListFuseNodeget_Item_fn;
    type->interface7.fp_Insert = (void(*)(uObject*, int32_t*, void*))::g::Fuse::Visual__Insert1_fn;
    type->interface8.fp_OnPropertyChanged = (void(*)(uObject*, ::g::Uno::UX::PropertyObject*, ::g::Uno::UX::Selector*))::g::Fuse::Controls::Control__OnPropertyChanged2_fn;
    type->interface9.fp_FindTemplate = (void(*)(uObject*, uString*, ::g::Uno::UX::Template**))::g::Fuse::Visual__FindTemplate_fn;
    type->interface11.fp_Add = (void(*)(uObject*, void*))::g::Fuse::Visual__Add1_fn;
    type->interface11.fp_Remove = (void(*)(uObject*, void*, bool*))::g::Fuse::Visual__Remove1_fn;
    type->interface5.fp_Clear = (void(*)(uObject*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingClear_fn;
    type->interface5.fp_Contains = (void(*)(uObject*, void*, bool*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingContains_fn;
    type->interface0.fp_RemoveAt = (void(*)(uObject*, int32_t*))::g::Fuse::Node__UnoCollectionsIListFuseBindingRemoveAt_fn;
    type->interface6.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Node__UnoCollectionsIEnumerableFuseBindingGetEnumerator_fn;
    type->interface1.fp_SetScriptObject = (void(*)(uObject*, uObject*, ::g::Fuse::Scripting::Context*))::g::Fuse::Node__FuseScriptingIScriptObjectSetScriptObject_fn;
    type->interface5.fp_get_Count = (void(*)(uObject*, int32_t*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingget_Count_fn;
    type->interface0.fp_get_Item = (void(*)(uObject*, int32_t*, uTRef))::g::Fuse::Node__UnoCollectionsIListFuseBindingget_Item_fn;
    type->interface1.fp_get_ScriptObject = (void(*)(uObject*, uObject**))::g::Fuse::Node__FuseScriptingIScriptObjectget_ScriptObject_fn;
    type->interface1.fp_get_ScriptContext = (void(*)(uObject*, ::g::Fuse::Scripting::Context**))::g::Fuse::Node__FuseScriptingIScriptObjectget_ScriptContext_fn;
    type->interface4.fp_get_SourceNearest = (void(*)(uObject*, uObject**))::g::Fuse::Node__FuseISourceLocationget_SourceNearest_fn;
    type->interface3.fp_add_Unrooted = (void(*)(uObject*, uDelegate*))::g::Fuse::Node__FuseINotifyUnrootedadd_Unrooted_fn;
    type->interface3.fp_remove_Unrooted = (void(*)(uObject*, uDelegate*))::g::Fuse::Node__FuseINotifyUnrootedremove_Unrooted_fn;
    type->interface0.fp_Insert = (void(*)(uObject*, int32_t*, void*))::g::Fuse::Node__Insert_fn;
    type->interface2.fp_get_Properties = (void(*)(uObject*, ::g::Fuse::Properties**))::g::Fuse::Node__get_Properties_fn;
    type->interface4.fp_get_SourceLineNumber = (void(*)(uObject*, int32_t*))::g::Fuse::Node__get_SourceLineNumber_fn;
    type->interface4.fp_get_SourceFileName = (void(*)(uObject*, uString**))::g::Fuse::Node__get_SourceFileName_fn;
    type->interface5.fp_Add = (void(*)(uObject*, void*))::g::Fuse::Node__Add_fn;
    type->interface5.fp_Remove = (void(*)(uObject*, void*, bool*))::g::Fuse::Node__Remove_fn;
    return type;
}

// public SortableList(Fuse.Controls.ScrollView parentScrollView) :68
void SortableList__ctor_7_fn(SortableList* __this, ::g::Fuse::Controls::ScrollView* parentScrollView1)
{
    __this->ctor_7(parentScrollView1);
}

// private void InitializeUX() :74
void SortableList__InitializeUX_fn(SortableList* __this)
{
    __this->InitializeUX();
}

// public object get_Items() :8
void SortableList__get_Items_fn(SortableList* __this, uObject** __retval)
{
    *__retval = __this->Items();
}

// public void set_Items(object value) :9
void SortableList__set_Items_fn(SortableList* __this, uObject* value)
{
    __this->Items(value);
}

// public string get_Label() :23
void SortableList__get_Label_fn(SortableList* __this, uString** __retval)
{
    *__retval = __this->Label();
}

// public void set_Label(string value) :24
void SortableList__set_Label_fn(SortableList* __this, uString* value)
{
    __this->Label(value);
}

// public SortableList New(Fuse.Controls.ScrollView parentScrollView) :68
void SortableList__New4_fn(::g::Fuse::Controls::ScrollView* parentScrollView1, SortableList** __retval)
{
    *__retval = SortableList::New4(parentScrollView1);
}

// public void SetItems(object value, Uno.UX.IPropertyListener origin) :11
void SortableList__SetItems_fn(SortableList* __this, uObject* value, uObject* origin)
{
    __this->SetItems(value, origin);
}

// public void SetLabel(string value, Uno.UX.IPropertyListener origin) :26
void SortableList__SetLabel_fn(SortableList* __this, uString* value, uObject* origin)
{
    __this->SetLabel(value, origin);
}

uSStrong<uArray*> SortableList::__g_static_nametable1_;
::g::Uno::UX::Selector SortableList::__selector0_;
::g::Uno::UX::Selector SortableList::__selector1_;
::g::Uno::UX::Selector SortableList::__selector2_;

// public SortableList(Fuse.Controls.ScrollView parentScrollView) [instance] :68
void SortableList::ctor_7(::g::Fuse::Controls::ScrollView* parentScrollView1)
{
    uStackFrame __("SortableList", ".ctor(Fuse.Controls.ScrollView)");
    ctor_6();
    parentScrollView = parentScrollView1;
    InitializeUX();
}

// private void InitializeUX() [instance] :74
void SortableList::InitializeUX()
{
    uStackFrame __("SortableList", "InitializeUX()");
    this_Items_inst = ::g::OSP_SortableList_Items_Property::New1(this, SortableList::__selector0_);
    this_Label_inst = ::g::OSP_SortableList_Label_Property::New1(this, SortableList::__selector1_);
    __g_nametable1 = ::g::Uno::UX::NameTable::New1(NULL, SortableList::__g_static_nametable1_);
    ::g::Fuse::Reactive::Constant* temp2 = ::g::Fuse::Reactive::Constant::New1(this);
    ::g::Fuse::Controls::Text* temp = ::g::Fuse::Controls::Text::New3();
    temp_Value_inst = ::g::OSP_FuseControlsTextControl_Value_Property::New1(temp, SortableList::__selector2_);
    ::g::Fuse::Reactive::Property* temp3 = ::g::Fuse::Reactive::Property::New1(temp2, ::g::OSP_accessor_SortableList_Label::Singleton());
    ::g::Fuse::Reactive::Each* temp1 = ::g::Fuse::Reactive::Each::New4();
    temp1_Items_inst = ::g::OSP_FuseReactiveEach_Items_Property::New1(temp1, SortableList::__selector0_);
    ::g::Fuse::Reactive::Data* temp4 = ::g::Fuse::Reactive::Data::New1(uString::Const("items"));
    ::g::Fuse::Reactive::JavaScript* temp5 = ::g::Fuse::Reactive::JavaScript::New2(__g_nametable1);
    ::g::Fuse::Controls::StackPanel* temp6 = ::g::Fuse::Controls::StackPanel::New4();
    ::g::Fuse::Reactive::DataBinding* temp7 = ::g::Fuse::Reactive::DataBinding::New1(temp_Value_inst, (uObject*)temp3, 1);
    ::g::Fuse::Controls::StackPanel* temp8 = ::g::Fuse::Controls::StackPanel::New4();
    SortableList__Template* temp9 = SortableList__Template::New2(this, this);
    ::g::Fuse::Reactive::DataBinding* temp10 = ::g::Fuse::Reactive::DataBinding::New1(temp1_Items_inst, (uObject*)temp4, 3);
    SourceLineNumber(1);
    SourceFileName(uString::Const("dashboardlist.ux"));
    temp5->Code(uString::Const("\n"
        "\tvar Observable = require(\"FuseJS/Observable\");\n"
        "\tvar items = this.Items.inner();\n"
        "\n"
        "\tvar selected = null;\n"
        "\tvar reordering = Observable(false);\n"
        "\n"
        "\tfunction select(args) {\n"
        "\t\tif (selected === null) {\n"
        "\t\t\tselected = args.data.id;\n"
        "\t\t\titems.forEach(function(x) {\n"
        "\t\t\t\tif (x.id === selected) {\n"
        "\t\t\t\t\tx.selected.value = true;\n"
        "\t\t\t\t}\n"
        "\t\t\t});\n"
        "\t\t}\n"
        "\t\treordering.value = true;\n"
        "\t}\n"
        "\n"
        "\tfunction deselect() {\n"
        "\t\tselected = null;\n"
        "\t\titems.forEach(function(x) {\n"
        "\t\t\tx.selected.value = false;\n"
        "\t\t});\n"
        "\t\treordering.value = false;\n"
        "\t}\n"
        "\n"
        "\tfunction hover(args) {\n"
        "\t\tif (reordering.value === true && selected !== null) {\n"
        "\t\t\tvar from;\n"
        "\t\t\tvar to;\n"
        "\t\t\titems.forEach(function(item, index) {\n"
        "\t\t\t\tif (item.id === selected) {\n"
        "\t\t\t\t\tfrom = index;\n"
        "\t\t\t\t}\n"
        "\t\t\t\tif (item.id === args.data.id) {\n"
        "\t\t\t\t\tto = index;\n"
        "\t\t\t\t}\n"
        "\t\t\t});\n"
        "\t\t\tif (to !== from && to !== undefined) {\n"
        "\t\t\t\tvar tmp = items.toArray();\n"
        "\t\t\t\tvar elem = tmp[from];\n"
        "\t\t\t\ttmp.splice(from, 1);\n"
        "\t\t\t\ttmp.splice(to, 0, elem);\n"
        "\t\t\t\titems.replaceAll(tmp);\n"
        "\t\t\t}\n"
        "\t\t}\n"
        "\t}\n"
        "\n"
        "\tmodule.exports = {\n"
        "\t\titems: items,\n"
        "\t\treordering: reordering,\n"
        "\t\tselect: select,\n"
        "\t\tdeselect: deselect,\n"
        "\t\thover: hover\n"
        "\t};\n"
        "\t"));
    temp5->LineNumber(7);
    temp5->FileName(uString::Const("dashboardlist.ux"));
    temp5->SourceLineNumber(7);
    temp5->SourceFileName(uString::Const("dashboardlist.ux"));
    temp6->ItemSpacing(8.0f);
    temp6->SourceLineNumber(106);
    temp6->SourceFileName(uString::Const("dashboardlist.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp6->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp6->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp8);
    temp->FontSize(18.0f);
    temp->TextColor(::g::Uno::Float4__New2(0.4666667f, 0.4666667f, 0.4666667f, 1.0f));
    temp->Alignment(9);
    temp->Margin(::g::Uno::Float4__New2(8.0f, 0.0f, 8.0f, 0.0f));
    temp->SourceLineNumber(107);
    temp->SourceFileName(uString::Const("dashboardlist.ux"));
    temp->Font(::g::Fuse::Font::Bold());
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp7);
    temp3->SourceLineNumber(107);
    temp3->SourceFileName(uString::Const("dashboardlist.ux"));
    temp2->SourceLineNumber(107);
    temp2->SourceFileName(uString::Const("dashboardlist.ux"));
    temp8->ItemSpacing(1.0f);
    temp8->SourceLineNumber(108);
    temp8->SourceFileName(uString::Const("dashboardlist.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp8->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp1);
    temp1->IdentityKey(uString::Const("id"));
    temp1->SourceLineNumber(109);
    temp1->SourceFileName(uString::Const("dashboardlist.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp1->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp9);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp1->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp10);
    temp4->SourceLineNumber(109);
    temp4->SourceFileName(uString::Const("dashboardlist.ux"));
    uPtr(__g_nametable1)->This(this);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), parentScrollView);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Properties()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Property_typeof(), NULL)), this_Items_inst);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Properties()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Property_typeof(), NULL)), this_Label_inst);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp5);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp6);
}

// public object get_Items() [instance] :8
uObject* SortableList::Items()
{
    return _field_Items;
}

// public void set_Items(object value) [instance] :9
void SortableList::Items(uObject* value)
{
    uStackFrame __("SortableList", "set_Items(object)");
    SetItems(value, NULL);
}

// public string get_Label() [instance] :23
uString* SortableList::Label()
{
    return _field_Label;
}

// public void set_Label(string value) [instance] :24
void SortableList::Label(uString* value)
{
    uStackFrame __("SortableList", "set_Label(string)");
    SetLabel(value, NULL);
}

// public void SetItems(object value, Uno.UX.IPropertyListener origin) [instance] :11
void SortableList::SetItems(uObject* value, uObject* origin)
{
    uStackFrame __("SortableList", "SetItems(object,Uno.UX.IPropertyListener)");

    if (value != _field_Items)
    {
        _field_Items = value;
        OnPropertyChanged1(::g::Uno::UX::Selector__op_Implicit1(uString::Const("Items")), origin);
    }
}

// public void SetLabel(string value, Uno.UX.IPropertyListener origin) [instance] :26
void SortableList::SetLabel(uString* value, uObject* origin)
{
    uStackFrame __("SortableList", "SetLabel(string,Uno.UX.IPropertyListener)");

    if (::g::Uno::String::op_Inequality(value, _field_Label))
    {
        _field_Label = value;
        OnPropertyChanged1(::g::Uno::UX::Selector__op_Implicit1(uString::Const("Label")), origin);
    }
}

// public SortableList New(Fuse.Controls.ScrollView parentScrollView) [static] :68
SortableList* SortableList::New4(::g::Fuse::Controls::ScrollView* parentScrollView1)
{
    SortableList* obj1 = (SortableList*)uNew(SortableList_typeof());
    obj1->ctor_7(parentScrollView1);
    return obj1;
}
// }

} // ::g
