// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/Profile.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.InputField.h>
#include <_root.OSP_InputField_-d24e30b7.h>
#include <_root.Profile.h>
#include <_root.Profile.Template3.h>
#include <Fuse.Binding.h>
#include <Fuse.Controls.DockPanel.h>
#include <Fuse.Node.h>
#include <Fuse.Reactive.BindingMode.h>
#include <Fuse.Reactive.Data.h>
#include <Fuse.Reactive.DataBinding.h>
#include <Fuse.Reactive.Expression.h>
#include <Fuse.Reactive.IExpression.h>
#include <Fuse.Visual.h>
#include <Uno.Bool.h>
#include <Uno.Collections.ICollection-1.h>
#include <Uno.Collections.IList-1.h>
#include <Uno.Int.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.UX.Property.h>
#include <Uno.UX.Property1-1.h>
#include <Uno.UX.Selector.h>
static uString* STRINGS[4];
static uType* TYPES[2];

namespace g{

// public partial sealed class Profile.Template3 :96
// {
// static Template3() :106
static void Profile__Template3__cctor__fn(uType* __type)
{
    Profile__Template3::__selector0_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[0/*"PlaceHolder...*/]);
}

static void Profile__Template3_build(uType* type)
{
    ::STRINGS[0] = uString::Const("PlaceHolderName");
    ::STRINGS[1] = uString::Const("birth_date");
    ::STRINGS[2] = uString::Const("profile.ux");
    ::STRINGS[3] = uString::Const("D.O.B.");
    ::TYPES[0] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL);
    ::TYPES[1] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL);
    type->SetFields(2,
        ::g::Profile_typeof(), offsetof(Profile__Template3, __parent1), uFieldFlagsWeak,
        ::g::Profile_typeof(), offsetof(Profile__Template3, __parentInstance1), uFieldFlagsWeak,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(Profile__Template3, temp_PlaceHolderName_inst), 0,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Profile__Template3::__selector0_, uFieldFlagsStatic);
    type->Reflection.SetFunctions(1,
        new uFunction(".ctor", NULL, (void*)Profile__Template3__New2_fn, 0, true, type, 2, ::g::Profile_typeof(), ::g::Profile_typeof()));
}

::g::Uno::UX::Template_type* Profile__Template3_typeof()
{
    static uSStrong< ::g::Uno::UX::Template_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::Template_typeof();
    options.FieldCount = 6;
    options.ObjectSize = sizeof(Profile__Template3);
    options.TypeSize = sizeof(::g::Uno::UX::Template_type);
    type = (::g::Uno::UX::Template_type*)uClassType::New("Profile.Template3", options);
    type->fp_build_ = Profile__Template3_build;
    type->fp_cctor_ = Profile__Template3__cctor__fn;
    type->fp_New1 = (void(*)(::g::Uno::UX::Template*, uObject**))Profile__Template3__New1_fn;
    return type;
}

// public Template3(Profile parent, Profile parentInstance) :100
void Profile__Template3__ctor_1_fn(Profile__Template3* __this, ::g::Profile* parent, ::g::Profile* parentInstance)
{
    __this->ctor_1(parent, parentInstance);
}

// public override sealed object New() :109
void Profile__Template3__New1_fn(Profile__Template3* __this, uObject** __retval)
{
    uStackFrame __("Profile.Template3", "New()");
    ::g::Fuse::Controls::DockPanel* __self1 = ::g::Fuse::Controls::DockPanel::New4();
    ::g::InputField* temp = ::g::InputField::New4();
    __this->temp_PlaceHolderName_inst = ::g::OSP_InputField_PlaceHolderName_Property::New1(temp, Profile__Template3::__selector0_);
    ::g::Fuse::Reactive::Data* temp1 = ::g::Fuse::Reactive::Data::New1(::STRINGS[1/*"birth_date"*/]);
    ::g::Fuse::Reactive::DataBinding* temp2 = ::g::Fuse::Reactive::DataBinding::New1(__this->temp_PlaceHolderName_inst, (uObject*)temp1, 3);
    __self1->SourceLineNumber(164);
    __self1->SourceFileName(::STRINGS[2/*"profile.ux"*/]);
    temp->Title(::STRINGS[3/*"D.O.B."*/]);
    temp->SourceLineNumber(165);
    temp->SourceFileName(::STRINGS[2/*"profile.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Bindings()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp2);
    temp1->SourceLineNumber(165);
    temp1->SourceFileName(::STRINGS[2/*"profile.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(__self1->Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp);
    return *__retval = __self1, void();
}

// public Template3 New(Profile parent, Profile parentInstance) :100
void Profile__Template3__New2_fn(::g::Profile* parent, ::g::Profile* parentInstance, Profile__Template3** __retval)
{
    *__retval = Profile__Template3::New2(parent, parentInstance);
}

::g::Uno::UX::Selector Profile__Template3::__selector0_;

// public Template3(Profile parent, Profile parentInstance) [instance] :100
void Profile__Template3::ctor_1(::g::Profile* parent, ::g::Profile* parentInstance)
{
    ctor_(NULL, false);
    __parent1 = parent;
    __parentInstance1 = parentInstance;
}

// public Template3 New(Profile parent, Profile parentInstance) [static] :100
Profile__Template3* Profile__Template3::New2(::g::Profile* parent, ::g::Profile* parentInstance)
{
    Profile__Template3* obj1 = (Profile__Template3*)uNew(Profile__Template3_typeof());
    obj1->ctor_1(parent, parentInstance);
    return obj1;
}
// }

} // ::g
