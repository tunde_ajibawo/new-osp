// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/InputField.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.InputField.h>
#include <_root.MainView.h>
#include <_root.OSP_accessor_In-1a1bca9e.h>
#include <_root.OSP_accessor_In-340dcd91.h>
#include <_root.OSP_accessor_In-433e39cd.h>
#include <_root.OSP_accessor_In-4ef0ebca.h>
#include <_root.OSP_FuseControl-569ba08.h>
#include <_root.OSP_FuseControl-8faf8b28.h>
#include <_root.OSP_FuseControl-b26a5bbf.h>
#include <Fuse.Controls.DockPanel.h>
#include <Fuse.Controls.Rectangle.h>
#include <Fuse.Controls.Shape.h>
#include <Fuse.Controls.Text.h>
#include <Fuse.Controls.TextControl.h>
#include <Fuse.Controls.TextWrapping.h>
#include <Fuse.Elements.Alignment.h>
#include <Fuse.Elements.Element.h>
#include <Fuse.Font.h>
#include <Fuse.Layouts.Dock.h>
#include <Fuse.Reactive.BindingMode.h>
#include <Fuse.Reactive.Constan-264ec80.h>
#include <Fuse.Reactive.Constant.h>
#include <Fuse.Reactive.DataBinding.h>
#include <Fuse.Reactive.Expression.h>
#include <Fuse.Reactive.IExpression.h>
#include <Fuse.Reactive.Property.h>
#include <Uno.Bool.h>
#include <Uno.Float.h>
#include <Uno.Int.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.UX.Property.h>
#include <Uno.UX.Property1-1.h>
#include <Uno.UX.PropertyAccessor.h>
#include <Uno.UX.PropertyObject.h>
#include <Uno.UX.Selector.h>
#include <Uno.UX.Size.h>
#include <Uno.UX.Unit.h>
static uString* STRINGS[8];
static uType* TYPES[2];

namespace g{

// public partial sealed class InputField :2
// {
// static InputField() :69
static void InputField__cctor_4_fn(uType* __type)
{
    InputField::__selector0_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[0/*"Value"*/]);
    InputField::__selector1_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[1/*"TextColor"*/]);
    InputField::__selector2_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[2/*"Color"*/]);
}

static void InputField_build(uType* type)
{
    ::STRINGS[0] = uString::Const("Value");
    ::STRINGS[1] = uString::Const("TextColor");
    ::STRINGS[2] = uString::Const("Color");
    ::STRINGS[3] = uString::Const("profile.ux");
    ::STRINGS[4] = uString::Const("BorderColor");
    ::STRINGS[5] = uString::Const("FieldColor");
    ::STRINGS[6] = uString::Const("PlaceHolderName");
    ::STRINGS[7] = uString::Const("Title");
    ::TYPES[0] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL);
    ::TYPES[1] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL);
    type->SetDependencies(
        ::g::MainView_typeof(),
        ::g::OSP_accessor_InputField_BorderColor_typeof(),
        ::g::OSP_accessor_InputField_FieldColor_typeof(),
        ::g::OSP_accessor_InputField_PlaceHolderName_typeof(),
        ::g::OSP_accessor_InputField_Title_typeof());
    type->SetInterfaces(
        ::g::Uno::Collections::IList_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface0),
        ::g::Fuse::Scripting::IScriptObject_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface1),
        ::g::Fuse::IProperties_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface2),
        ::g::Fuse::INotifyUnrooted_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface3),
        ::g::Fuse::ISourceLocation_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface4),
        ::TYPES[1/*Uno.Collections.ICollection<Fuse.Binding>*/], offsetof(::g::Fuse::Controls::Panel_type, interface5),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface6),
        ::g::Uno::Collections::IList_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface7),
        ::g::Uno::UX::IPropertyListener_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface8),
        ::g::Fuse::ITemplateSource_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface9),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Visual_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface10),
        ::TYPES[0/*Uno.Collections.ICollection<Fuse.Node>*/], offsetof(::g::Fuse::Controls::Panel_type, interface11),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface12),
        ::g::Fuse::Triggers::Actions::IShow_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface13),
        ::g::Fuse::Triggers::Actions::IHide_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface14),
        ::g::Fuse::Triggers::Actions::ICollapse_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface15),
        ::g::Fuse::IActualPlacement_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface16),
        ::g::Fuse::Animations::IResize_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface17),
        ::g::Fuse::Drawing::ISurfaceDrawable_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface18));
    type->SetFields(120,
        ::g::Uno::String_typeof(), offsetof(InputField, _field_PlaceHolderName), 0,
        ::g::Uno::String_typeof(), offsetof(InputField, _field_Title), 0,
        ::g::Uno::Float4_typeof(), offsetof(InputField, _field_BorderColor), 0,
        ::g::Uno::Float4_typeof(), offsetof(InputField, _field_FieldColor), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(InputField, temp_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL), offsetof(InputField, temp_TextColor_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(InputField, temp1_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL), offsetof(InputField, temp1_TextColor_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL), offsetof(InputField, temp2_Color_inst), 0,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&InputField::__selector0_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&InputField::__selector1_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&InputField::__selector2_, uFieldFlagsStatic);
    type->Reflection.SetFunctions(13,
        new uFunction("get_BorderColor", NULL, (void*)InputField__get_BorderColor_fn, 0, false, ::g::Uno::Float4_typeof(), 0),
        new uFunction("set_BorderColor", NULL, (void*)InputField__set_BorderColor_fn, 0, false, uVoid_typeof(), 1, ::g::Uno::Float4_typeof()),
        new uFunction("get_FieldColor", NULL, (void*)InputField__get_FieldColor_fn, 0, false, ::g::Uno::Float4_typeof(), 0),
        new uFunction("set_FieldColor", NULL, (void*)InputField__set_FieldColor_fn, 0, false, uVoid_typeof(), 1, ::g::Uno::Float4_typeof()),
        new uFunction(".ctor", NULL, (void*)InputField__New4_fn, 0, true, type, 0),
        new uFunction("get_PlaceHolderName", NULL, (void*)InputField__get_PlaceHolderName_fn, 0, false, ::g::Uno::String_typeof(), 0),
        new uFunction("set_PlaceHolderName", NULL, (void*)InputField__set_PlaceHolderName_fn, 0, false, uVoid_typeof(), 1, ::g::Uno::String_typeof()),
        new uFunction("SetBorderColor", NULL, (void*)InputField__SetBorderColor_fn, 0, false, uVoid_typeof(), 2, ::g::Uno::Float4_typeof(), ::g::Uno::UX::IPropertyListener_typeof()),
        new uFunction("SetFieldColor", NULL, (void*)InputField__SetFieldColor_fn, 0, false, uVoid_typeof(), 2, ::g::Uno::Float4_typeof(), ::g::Uno::UX::IPropertyListener_typeof()),
        new uFunction("SetPlaceHolderName", NULL, (void*)InputField__SetPlaceHolderName_fn, 0, false, uVoid_typeof(), 2, ::g::Uno::String_typeof(), ::g::Uno::UX::IPropertyListener_typeof()),
        new uFunction("SetTitle", NULL, (void*)InputField__SetTitle_fn, 0, false, uVoid_typeof(), 2, ::g::Uno::String_typeof(), ::g::Uno::UX::IPropertyListener_typeof()),
        new uFunction("get_Title", NULL, (void*)InputField__get_Title_fn, 0, false, ::g::Uno::String_typeof(), 0),
        new uFunction("set_Title", NULL, (void*)InputField__set_Title_fn, 0, false, uVoid_typeof(), 1, ::g::Uno::String_typeof()));
}

::g::Fuse::Controls::Panel_type* InputField_typeof()
{
    static uSStrong< ::g::Fuse::Controls::Panel_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Fuse::Controls::Panel_typeof();
    options.FieldCount = 132;
    options.InterfaceCount = 19;
    options.DependencyCount = 5;
    options.ObjectSize = sizeof(InputField);
    options.TypeSize = sizeof(::g::Fuse::Controls::Panel_type);
    type = (::g::Fuse::Controls::Panel_type*)uClassType::New("InputField", options);
    type->fp_build_ = InputField_build;
    type->fp_ctor_ = (void*)InputField__New4_fn;
    type->fp_cctor_ = InputField__cctor_4_fn;
    type->interface18.fp_Draw = (void(*)(uObject*, ::g::Fuse::Drawing::Surface*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableDraw_fn;
    type->interface18.fp_get_IsPrimary = (void(*)(uObject*, bool*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableget_IsPrimary_fn;
    type->interface18.fp_get_ElementSize = (void(*)(uObject*, ::g::Uno::Float2*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableget_ElementSize_fn;
    type->interface13.fp_Show = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsIShowShow_fn;
    type->interface15.fp_Collapse = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsICollapseCollapse_fn;
    type->interface14.fp_Hide = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsIHideHide_fn;
    type->interface17.fp_SetSize = (void(*)(uObject*, ::g::Uno::Float2*))::g::Fuse::Elements::Element__FuseAnimationsIResizeSetSize_fn;
    type->interface16.fp_get_ActualSize = (void(*)(uObject*, ::g::Uno::Float3*))::g::Fuse::Elements::Element__FuseIActualPlacementget_ActualSize_fn;
    type->interface16.fp_get_ActualPosition = (void(*)(uObject*, ::g::Uno::Float3*))::g::Fuse::Elements::Element__FuseIActualPlacementget_ActualPosition_fn;
    type->interface16.fp_add_Placed = (void(*)(uObject*, uDelegate*))::g::Fuse::Elements::Element__add_Placed_fn;
    type->interface16.fp_remove_Placed = (void(*)(uObject*, uDelegate*))::g::Fuse::Elements::Element__remove_Placed_fn;
    type->interface10.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Visual__UnoCollectionsIEnumerableFuseVisualGetEnumerator_fn;
    type->interface11.fp_Clear = (void(*)(uObject*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeClear_fn;
    type->interface11.fp_Contains = (void(*)(uObject*, void*, bool*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeContains_fn;
    type->interface7.fp_RemoveAt = (void(*)(uObject*, int32_t*))::g::Fuse::Visual__UnoCollectionsIListFuseNodeRemoveAt_fn;
    type->interface12.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Visual__UnoCollectionsIEnumerableFuseNodeGetEnumerator_fn;
    type->interface11.fp_get_Count = (void(*)(uObject*, int32_t*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeget_Count_fn;
    type->interface7.fp_get_Item = (void(*)(uObject*, int32_t*, uTRef))::g::Fuse::Visual__UnoCollectionsIListFuseNodeget_Item_fn;
    type->interface7.fp_Insert = (void(*)(uObject*, int32_t*, void*))::g::Fuse::Visual__Insert1_fn;
    type->interface8.fp_OnPropertyChanged = (void(*)(uObject*, ::g::Uno::UX::PropertyObject*, ::g::Uno::UX::Selector*))::g::Fuse::Controls::Control__OnPropertyChanged2_fn;
    type->interface9.fp_FindTemplate = (void(*)(uObject*, uString*, ::g::Uno::UX::Template**))::g::Fuse::Visual__FindTemplate_fn;
    type->interface11.fp_Add = (void(*)(uObject*, void*))::g::Fuse::Visual__Add1_fn;
    type->interface11.fp_Remove = (void(*)(uObject*, void*, bool*))::g::Fuse::Visual__Remove1_fn;
    type->interface5.fp_Clear = (void(*)(uObject*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingClear_fn;
    type->interface5.fp_Contains = (void(*)(uObject*, void*, bool*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingContains_fn;
    type->interface0.fp_RemoveAt = (void(*)(uObject*, int32_t*))::g::Fuse::Node__UnoCollectionsIListFuseBindingRemoveAt_fn;
    type->interface6.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Node__UnoCollectionsIEnumerableFuseBindingGetEnumerator_fn;
    type->interface1.fp_SetScriptObject = (void(*)(uObject*, uObject*, ::g::Fuse::Scripting::Context*))::g::Fuse::Node__FuseScriptingIScriptObjectSetScriptObject_fn;
    type->interface5.fp_get_Count = (void(*)(uObject*, int32_t*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingget_Count_fn;
    type->interface0.fp_get_Item = (void(*)(uObject*, int32_t*, uTRef))::g::Fuse::Node__UnoCollectionsIListFuseBindingget_Item_fn;
    type->interface1.fp_get_ScriptObject = (void(*)(uObject*, uObject**))::g::Fuse::Node__FuseScriptingIScriptObjectget_ScriptObject_fn;
    type->interface1.fp_get_ScriptContext = (void(*)(uObject*, ::g::Fuse::Scripting::Context**))::g::Fuse::Node__FuseScriptingIScriptObjectget_ScriptContext_fn;
    type->interface4.fp_get_SourceNearest = (void(*)(uObject*, uObject**))::g::Fuse::Node__FuseISourceLocationget_SourceNearest_fn;
    type->interface3.fp_add_Unrooted = (void(*)(uObject*, uDelegate*))::g::Fuse::Node__FuseINotifyUnrootedadd_Unrooted_fn;
    type->interface3.fp_remove_Unrooted = (void(*)(uObject*, uDelegate*))::g::Fuse::Node__FuseINotifyUnrootedremove_Unrooted_fn;
    type->interface0.fp_Insert = (void(*)(uObject*, int32_t*, void*))::g::Fuse::Node__Insert_fn;
    type->interface2.fp_get_Properties = (void(*)(uObject*, ::g::Fuse::Properties**))::g::Fuse::Node__get_Properties_fn;
    type->interface4.fp_get_SourceLineNumber = (void(*)(uObject*, int32_t*))::g::Fuse::Node__get_SourceLineNumber_fn;
    type->interface4.fp_get_SourceFileName = (void(*)(uObject*, uString**))::g::Fuse::Node__get_SourceFileName_fn;
    type->interface5.fp_Add = (void(*)(uObject*, void*))::g::Fuse::Node__Add_fn;
    type->interface5.fp_Remove = (void(*)(uObject*, void*, bool*))::g::Fuse::Node__Remove_fn;
    return type;
}

// public InputField() :73
void InputField__ctor_7_fn(InputField* __this)
{
    __this->ctor_7();
}

// public float4 get_BorderColor() :38
void InputField__get_BorderColor_fn(InputField* __this, ::g::Uno::Float4* __retval)
{
    *__retval = __this->BorderColor();
}

// public void set_BorderColor(float4 value) :39
void InputField__set_BorderColor_fn(InputField* __this, ::g::Uno::Float4* value)
{
    __this->BorderColor(*value);
}

// public float4 get_FieldColor() :53
void InputField__get_FieldColor_fn(InputField* __this, ::g::Uno::Float4* __retval)
{
    *__retval = __this->FieldColor();
}

// public void set_FieldColor(float4 value) :54
void InputField__set_FieldColor_fn(InputField* __this, ::g::Uno::Float4* value)
{
    __this->FieldColor(*value);
}

// private void InitializeUX() :77
void InputField__InitializeUX_fn(InputField* __this)
{
    __this->InitializeUX();
}

// public InputField New() :73
void InputField__New4_fn(InputField** __retval)
{
    *__retval = InputField::New4();
}

// public string get_PlaceHolderName() :8
void InputField__get_PlaceHolderName_fn(InputField* __this, uString** __retval)
{
    *__retval = __this->PlaceHolderName();
}

// public void set_PlaceHolderName(string value) :9
void InputField__set_PlaceHolderName_fn(InputField* __this, uString* value)
{
    __this->PlaceHolderName(value);
}

// public void SetBorderColor(float4 value, Uno.UX.IPropertyListener origin) :41
void InputField__SetBorderColor_fn(InputField* __this, ::g::Uno::Float4* value, uObject* origin)
{
    __this->SetBorderColor(*value, origin);
}

// public void SetFieldColor(float4 value, Uno.UX.IPropertyListener origin) :56
void InputField__SetFieldColor_fn(InputField* __this, ::g::Uno::Float4* value, uObject* origin)
{
    __this->SetFieldColor(*value, origin);
}

// public void SetPlaceHolderName(string value, Uno.UX.IPropertyListener origin) :11
void InputField__SetPlaceHolderName_fn(InputField* __this, uString* value, uObject* origin)
{
    __this->SetPlaceHolderName(value, origin);
}

// public void SetTitle(string value, Uno.UX.IPropertyListener origin) :26
void InputField__SetTitle_fn(InputField* __this, uString* value, uObject* origin)
{
    __this->SetTitle(value, origin);
}

// public string get_Title() :23
void InputField__get_Title_fn(InputField* __this, uString** __retval)
{
    *__retval = __this->Title();
}

// public void set_Title(string value) :24
void InputField__set_Title_fn(InputField* __this, uString* value)
{
    __this->Title(value);
}

::g::Uno::UX::Selector InputField::__selector0_;
::g::Uno::UX::Selector InputField::__selector1_;
::g::Uno::UX::Selector InputField::__selector2_;

// public InputField() [instance] :73
void InputField::ctor_7()
{
    uStackFrame __("InputField", ".ctor()");
    ctor_6();
    InitializeUX();
}

// public float4 get_BorderColor() [instance] :38
::g::Uno::Float4 InputField::BorderColor()
{
    return _field_BorderColor;
}

// public void set_BorderColor(float4 value) [instance] :39
void InputField::BorderColor(::g::Uno::Float4 value)
{
    uStackFrame __("InputField", "set_BorderColor(float4)");
    SetBorderColor(value, NULL);
}

// public float4 get_FieldColor() [instance] :53
::g::Uno::Float4 InputField::FieldColor()
{
    return _field_FieldColor;
}

// public void set_FieldColor(float4 value) [instance] :54
void InputField::FieldColor(::g::Uno::Float4 value)
{
    uStackFrame __("InputField", "set_FieldColor(float4)");
    SetFieldColor(value, NULL);
}

// private void InitializeUX() [instance] :77
void InputField::InitializeUX()
{
    uStackFrame __("InputField", "InitializeUX()");
    ::g::Fuse::Reactive::Constant* temp3 = ::g::Fuse::Reactive::Constant::New1(this);
    ::g::Fuse::Controls::Text* temp = ::g::Fuse::Controls::Text::New3();
    temp_Value_inst = ::g::OSP_FuseControlsTextControl_Value_Property::New1(temp, InputField::__selector0_);
    ::g::Fuse::Reactive::Property* temp4 = ::g::Fuse::Reactive::Property::New1(temp3, ::g::OSP_accessor_InputField_Title::Singleton());
    ::g::Fuse::Reactive::Constant* temp5 = ::g::Fuse::Reactive::Constant::New1(this);
    temp_TextColor_inst = ::g::OSP_FuseControlsTextControl_TextColor_Property::New1(temp, InputField::__selector1_);
    ::g::Fuse::Reactive::Property* temp6 = ::g::Fuse::Reactive::Property::New1(temp5, ::g::OSP_accessor_InputField_FieldColor::Singleton());
    ::g::Fuse::Reactive::Constant* temp7 = ::g::Fuse::Reactive::Constant::New1(this);
    ::g::Fuse::Controls::Text* temp1 = ::g::Fuse::Controls::Text::New3();
    temp1_Value_inst = ::g::OSP_FuseControlsTextControl_Value_Property::New1(temp1, InputField::__selector0_);
    ::g::Fuse::Reactive::Property* temp8 = ::g::Fuse::Reactive::Property::New1(temp7, ::g::OSP_accessor_InputField_PlaceHolderName::Singleton());
    ::g::Fuse::Reactive::Constant* temp9 = ::g::Fuse::Reactive::Constant::New1(this);
    temp1_TextColor_inst = ::g::OSP_FuseControlsTextControl_TextColor_Property::New1(temp1, InputField::__selector1_);
    ::g::Fuse::Reactive::Property* temp10 = ::g::Fuse::Reactive::Property::New1(temp9, ::g::OSP_accessor_InputField_FieldColor::Singleton());
    ::g::Fuse::Reactive::Constant* temp11 = ::g::Fuse::Reactive::Constant::New1(this);
    ::g::Fuse::Controls::Rectangle* temp2 = ::g::Fuse::Controls::Rectangle::New3();
    temp2_Color_inst = ::g::OSP_FuseControlsShape_Color_Property::New1(temp2, InputField::__selector2_);
    ::g::Fuse::Reactive::Property* temp12 = ::g::Fuse::Reactive::Property::New1(temp11, ::g::OSP_accessor_InputField_BorderColor::Singleton());
    ::g::Fuse::Controls::DockPanel* temp13 = ::g::Fuse::Controls::DockPanel::New4();
    ::g::Fuse::Reactive::DataBinding* temp14 = ::g::Fuse::Reactive::DataBinding::New1(temp_Value_inst, (uObject*)temp4, 3);
    ::g::Fuse::Reactive::DataBinding* temp15 = ::g::Fuse::Reactive::DataBinding::New1(temp_TextColor_inst, (uObject*)temp6, 3);
    ::g::Fuse::Reactive::DataBinding* temp16 = ::g::Fuse::Reactive::DataBinding::New1(temp1_Value_inst, (uObject*)temp8, 3);
    ::g::Fuse::Reactive::DataBinding* temp17 = ::g::Fuse::Reactive::DataBinding::New1(temp1_TextColor_inst, (uObject*)temp10, 3);
    ::g::Fuse::Reactive::DataBinding* temp18 = ::g::Fuse::Reactive::DataBinding::New1(temp2_Color_inst, (uObject*)temp12, 3);
    BorderColor(::g::Uno::Float4__New2(0.8352941f, 0.8352941f, 0.8352941f, 1.0f));
    FieldColor(::g::Uno::Float4__New2(0.6666667f, 0.6666667f, 0.6666667f, 1.0f));
    Width(::g::Uno::UX::Size__New1(80.0f, 4));
    Margin(::g::Uno::Float4__New2(0.0f, 5.0f, 0.0f, 5.0f));
    SourceLineNumber(118);
    SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    temp13->SourceLineNumber(124);
    temp13->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp13->Children()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Node>*/]), temp);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp13->Children()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Node>*/]), temp1);
    temp->TextWrapping(1);
    temp->FontSize(11.0f);
    temp->Width(::g::Uno::UX::Size__New1(30.0f, 4));
    temp->Alignment(8);
    temp->SourceLineNumber(126);
    temp->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    ::g::Fuse::Controls::DockPanel::SetDock(temp, 0);
    temp->Font(::g::MainView::Raleway());
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Bindings()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp14);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Bindings()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp15);
    temp4->SourceLineNumber(126);
    temp4->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    temp3->SourceLineNumber(126);
    temp3->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    temp6->SourceLineNumber(126);
    temp6->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    temp5->SourceLineNumber(126);
    temp5->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    temp1->TextWrapping(1);
    temp1->FontSize(11.0f);
    temp1->Width(::g::Uno::UX::Size__New1(60.0f, 4));
    temp1->Alignment(8);
    temp1->Margin(::g::Uno::Float4__New2(20.0f, 0.0f, 0.0f, 0.0f));
    temp1->SourceLineNumber(128);
    temp1->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    ::g::Fuse::Controls::DockPanel::SetDock(temp1, 0);
    temp1->Font(::g::MainView::Raleway());
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp1->Bindings()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp16);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp1->Bindings()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp17);
    temp8->SourceLineNumber(128);
    temp8->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    temp7->SourceLineNumber(128);
    temp7->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    temp10->SourceLineNumber(128);
    temp10->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    temp9->SourceLineNumber(128);
    temp9->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    temp2->Height(::g::Uno::UX::Size__New1(1.0f, 1));
    temp2->Alignment(12);
    temp2->SourceLineNumber(130);
    temp2->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp2->Bindings()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp18);
    temp12->SourceLineNumber(130);
    temp12->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    temp11->SourceLineNumber(130);
    temp11->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Node>*/]), temp13);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Node>*/]), temp2);
}

// public string get_PlaceHolderName() [instance] :8
uString* InputField::PlaceHolderName()
{
    return _field_PlaceHolderName;
}

// public void set_PlaceHolderName(string value) [instance] :9
void InputField::PlaceHolderName(uString* value)
{
    uStackFrame __("InputField", "set_PlaceHolderName(string)");
    SetPlaceHolderName(value, NULL);
}

// public void SetBorderColor(float4 value, Uno.UX.IPropertyListener origin) [instance] :41
void InputField::SetBorderColor(::g::Uno::Float4 value, uObject* origin)
{
    uStackFrame __("InputField", "SetBorderColor(float4,Uno.UX.IPropertyListener)");

    if (::g::Uno::Float4__op_Inequality(value, _field_BorderColor))
    {
        _field_BorderColor = value;
        OnPropertyChanged1(::g::Uno::UX::Selector__op_Implicit1(::STRINGS[4/*"BorderColor"*/]), origin);
    }
}

// public void SetFieldColor(float4 value, Uno.UX.IPropertyListener origin) [instance] :56
void InputField::SetFieldColor(::g::Uno::Float4 value, uObject* origin)
{
    uStackFrame __("InputField", "SetFieldColor(float4,Uno.UX.IPropertyListener)");

    if (::g::Uno::Float4__op_Inequality(value, _field_FieldColor))
    {
        _field_FieldColor = value;
        OnPropertyChanged1(::g::Uno::UX::Selector__op_Implicit1(::STRINGS[5/*"FieldColor"*/]), origin);
    }
}

// public void SetPlaceHolderName(string value, Uno.UX.IPropertyListener origin) [instance] :11
void InputField::SetPlaceHolderName(uString* value, uObject* origin)
{
    uStackFrame __("InputField", "SetPlaceHolderName(string,Uno.UX.IPropertyListener)");

    if (::g::Uno::String::op_Inequality(value, _field_PlaceHolderName))
    {
        _field_PlaceHolderName = value;
        OnPropertyChanged1(::g::Uno::UX::Selector__op_Implicit1(::STRINGS[6/*"PlaceHolder...*/]), origin);
    }
}

// public void SetTitle(string value, Uno.UX.IPropertyListener origin) [instance] :26
void InputField::SetTitle(uString* value, uObject* origin)
{
    uStackFrame __("InputField", "SetTitle(string,Uno.UX.IPropertyListener)");

    if (::g::Uno::String::op_Inequality(value, _field_Title))
    {
        _field_Title = value;
        OnPropertyChanged1(::g::Uno::UX::Selector__op_Implicit1(::STRINGS[7/*"Title"*/]), origin);
    }
}

// public string get_Title() [instance] :23
uString* InputField::Title()
{
    return _field_Title;
}

// public void set_Title(string value) [instance] :24
void InputField::Title(uString* value)
{
    uStackFrame __("InputField", "set_Title(string)");
    SetTitle(value, NULL);
}

// public InputField New() [static] :73
InputField* InputField::New4()
{
    InputField* obj1 = (InputField*)uNew(InputField_typeof());
    obj1->ctor_7();
    return obj1;
}
// }

} // ::g
