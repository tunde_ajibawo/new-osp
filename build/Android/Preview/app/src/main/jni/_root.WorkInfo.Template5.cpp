// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/WorkInfo.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.WorkInfo.h>
#include <_root.WorkInfo.Template5.h>
#include <Fuse.Controls.Panel.h>
#include <Fuse.Elements.Element.h>
#include <Fuse.Node.h>
#include <Uno.Bool.h>
#include <Uno.Float.h>
#include <Uno.Float4.h>
#include <Uno.Int.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.UX.Size.h>
#include <Uno.UX.Unit.h>
static uString* STRINGS[1];

namespace g{

// public partial sealed class WorkInfo.Template5 :163
// {
// static Template5() :172
static void WorkInfo__Template5__cctor__fn(uType* __type)
{
}

static void WorkInfo__Template5_build(uType* type)
{
    ::STRINGS[0] = uString::Const("workinfo.ux");
    type->SetFields(2,
        ::g::WorkInfo_typeof(), offsetof(WorkInfo__Template5, __parent1), uFieldFlagsWeak,
        ::g::WorkInfo_typeof(), offsetof(WorkInfo__Template5, __parentInstance1), uFieldFlagsWeak);
    type->Reflection.SetFunctions(1,
        new uFunction(".ctor", NULL, (void*)WorkInfo__Template5__New2_fn, 0, true, type, 2, ::g::WorkInfo_typeof(), ::g::WorkInfo_typeof()));
}

::g::Uno::UX::Template_type* WorkInfo__Template5_typeof()
{
    static uSStrong< ::g::Uno::UX::Template_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::Template_typeof();
    options.FieldCount = 4;
    options.ObjectSize = sizeof(WorkInfo__Template5);
    options.TypeSize = sizeof(::g::Uno::UX::Template_type);
    type = (::g::Uno::UX::Template_type*)uClassType::New("WorkInfo.Template5", options);
    type->fp_build_ = WorkInfo__Template5_build;
    type->fp_cctor_ = WorkInfo__Template5__cctor__fn;
    type->fp_New1 = (void(*)(::g::Uno::UX::Template*, uObject**))WorkInfo__Template5__New1_fn;
    return type;
}

// public Template5(WorkInfo parent, WorkInfo parentInstance) :167
void WorkInfo__Template5__ctor_1_fn(WorkInfo__Template5* __this, ::g::WorkInfo* parent, ::g::WorkInfo* parentInstance)
{
    __this->ctor_1(parent, parentInstance);
}

// public override sealed object New() :175
void WorkInfo__Template5__New1_fn(WorkInfo__Template5* __this, uObject** __retval)
{
    uStackFrame __("WorkInfo.Template5", "New()");
    ::g::Fuse::Controls::Panel* __self1 = ::g::Fuse::Controls::Panel::New3();
    __self1->Width(::g::Uno::UX::Size__New1(80.0f, 4));
    __self1->Margin(::g::Uno::Float4__New2(0.0f, 5.0f, 0.0f, 5.0f));
    __self1->SourceLineNumber(278);
    __self1->SourceFileName(::STRINGS[0/*"workinfo.ux"*/]);
    return *__retval = __self1, void();
}

// public Template5 New(WorkInfo parent, WorkInfo parentInstance) :167
void WorkInfo__Template5__New2_fn(::g::WorkInfo* parent, ::g::WorkInfo* parentInstance, WorkInfo__Template5** __retval)
{
    *__retval = WorkInfo__Template5::New2(parent, parentInstance);
}

// public Template5(WorkInfo parent, WorkInfo parentInstance) [instance] :167
void WorkInfo__Template5::ctor_1(::g::WorkInfo* parent, ::g::WorkInfo* parentInstance)
{
    ctor_(NULL, false);
    __parent1 = parent;
    __parentInstance1 = parentInstance;
}

// public Template5 New(WorkInfo parent, WorkInfo parentInstance) [static] :167
WorkInfo__Template5* WorkInfo__Template5::New2(::g::WorkInfo* parent, ::g::WorkInfo* parentInstance)
{
    WorkInfo__Template5* obj1 = (WorkInfo__Template5*)uNew(WorkInfo__Template5_typeof());
    obj1->ctor_1(parent, parentInstance);
    return obj1;
}
// }

} // ::g
