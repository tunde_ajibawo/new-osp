// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/WorkInfo.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.InputField.h>
#include <_root.OSP_InputField_-d24e30b7.h>
#include <_root.WorkInfo.h>
#include <_root.WorkInfo.Template9.h>
#include <Fuse.Binding.h>
#include <Fuse.Node.h>
#include <Fuse.Reactive.BindingMode.h>
#include <Fuse.Reactive.Data.h>
#include <Fuse.Reactive.DataBinding.h>
#include <Fuse.Reactive.Expression.h>
#include <Fuse.Reactive.IExpression.h>
#include <Uno.Bool.h>
#include <Uno.Collections.ICollection-1.h>
#include <Uno.Collections.IList-1.h>
#include <Uno.Int.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.UX.Property.h>
#include <Uno.UX.Property1-1.h>
#include <Uno.UX.Selector.h>
static uString* STRINGS[4];
static uType* TYPES[1];

namespace g{

// public partial sealed class WorkInfo.Template9 :276
// {
// static Template9() :286
static void WorkInfo__Template9__cctor__fn(uType* __type)
{
    WorkInfo__Template9::__selector0_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[0/*"PlaceHolder...*/]);
}

static void WorkInfo__Template9_build(uType* type)
{
    ::STRINGS[0] = uString::Const("PlaceHolderName");
    ::STRINGS[1] = uString::Const("expiry_date");
    ::STRINGS[2] = uString::Const("EXPIRY DATE");
    ::STRINGS[3] = uString::Const("workinfo.ux");
    ::TYPES[0] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL);
    type->SetFields(2,
        ::g::WorkInfo_typeof(), offsetof(WorkInfo__Template9, __parent1), uFieldFlagsWeak,
        ::g::WorkInfo_typeof(), offsetof(WorkInfo__Template9, __parentInstance1), uFieldFlagsWeak,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(WorkInfo__Template9, __self_PlaceHolderName_inst1), 0,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&WorkInfo__Template9::__selector0_, uFieldFlagsStatic);
    type->Reflection.SetFunctions(1,
        new uFunction(".ctor", NULL, (void*)WorkInfo__Template9__New2_fn, 0, true, type, 2, ::g::WorkInfo_typeof(), ::g::WorkInfo_typeof()));
}

::g::Uno::UX::Template_type* WorkInfo__Template9_typeof()
{
    static uSStrong< ::g::Uno::UX::Template_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::Template_typeof();
    options.FieldCount = 6;
    options.ObjectSize = sizeof(WorkInfo__Template9);
    options.TypeSize = sizeof(::g::Uno::UX::Template_type);
    type = (::g::Uno::UX::Template_type*)uClassType::New("WorkInfo.Template9", options);
    type->fp_build_ = WorkInfo__Template9_build;
    type->fp_cctor_ = WorkInfo__Template9__cctor__fn;
    type->fp_New1 = (void(*)(::g::Uno::UX::Template*, uObject**))WorkInfo__Template9__New1_fn;
    return type;
}

// public Template9(WorkInfo parent, WorkInfo parentInstance) :280
void WorkInfo__Template9__ctor_1_fn(WorkInfo__Template9* __this, ::g::WorkInfo* parent, ::g::WorkInfo* parentInstance)
{
    __this->ctor_1(parent, parentInstance);
}

// public override sealed object New() :289
void WorkInfo__Template9__New1_fn(WorkInfo__Template9* __this, uObject** __retval)
{
    uStackFrame __("WorkInfo.Template9", "New()");
    ::g::InputField* __self1 = ::g::InputField::New4();
    __this->__self_PlaceHolderName_inst1 = ::g::OSP_InputField_PlaceHolderName_Property::New1(__self1, WorkInfo__Template9::__selector0_);
    ::g::Fuse::Reactive::Data* temp = ::g::Fuse::Reactive::Data::New1(::STRINGS[1/*"expiry_date"*/]);
    ::g::Fuse::Reactive::DataBinding* temp1 = ::g::Fuse::Reactive::DataBinding::New1(__this->__self_PlaceHolderName_inst1, (uObject*)temp, 3);
    __self1->Title(::STRINGS[2/*"EXPIRY DATE"*/]);
    __self1->SourceLineNumber(344);
    __self1->SourceFileName(::STRINGS[3/*"workinfo.ux"*/]);
    temp->SourceLineNumber(344);
    temp->SourceFileName(::STRINGS[3/*"workinfo.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(__self1->Bindings()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp1);
    return *__retval = __self1, void();
}

// public Template9 New(WorkInfo parent, WorkInfo parentInstance) :280
void WorkInfo__Template9__New2_fn(::g::WorkInfo* parent, ::g::WorkInfo* parentInstance, WorkInfo__Template9** __retval)
{
    *__retval = WorkInfo__Template9::New2(parent, parentInstance);
}

::g::Uno::UX::Selector WorkInfo__Template9::__selector0_;

// public Template9(WorkInfo parent, WorkInfo parentInstance) [instance] :280
void WorkInfo__Template9::ctor_1(::g::WorkInfo* parent, ::g::WorkInfo* parentInstance)
{
    ctor_(NULL, false);
    __parent1 = parent;
    __parentInstance1 = parentInstance;
}

// public Template9 New(WorkInfo parent, WorkInfo parentInstance) [static] :280
WorkInfo__Template9* WorkInfo__Template9::New2(::g::WorkInfo* parent, ::g::WorkInfo* parentInstance)
{
    WorkInfo__Template9* obj1 = (WorkInfo__Template9*)uNew(WorkInfo__Template9_typeof());
    obj1->ctor_1(parent, parentInstance);
    return obj1;
}
// }

} // ::g
