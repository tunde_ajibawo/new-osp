// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/DropdownMenu.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.DropdownMenu.h>
#include <_root.DropdownMenu.Template.h>
#include <_root.DropdownOption.h>
#include <_root.OSP_accessor_Dr-22a07ec4.h>
#include <_root.OSP_accessor_Dr-716a8965.h>
#include <_root.OSP_accessor_Dr-8318b526.h>
#include <_root.OSP_accessor_Dr-94c1f4f3.h>
#include <_root.OSP_DropdownOpt-13ac1cb6.h>
#include <_root.OSP_DropdownOpt-40057edf.h>
#include <_root.OSP_DropdownOpt-4c4009d4.h>
#include <_root.OSP_DropdownOpt-a046902a.h>
#include <_root.OSP_DropdownOpt-d4f121f1.h>
#include <Fuse.Binding.h>
#include <Fuse.Drawing.Brush.h>
#include <Fuse.Gestures.Clicked.h>
#include <Fuse.Gestures.ClickedHandler.h>
#include <Fuse.Node.h>
#include <Fuse.Reactive.BindingMode.h>
#include <Fuse.Reactive.Constan-264ec80.h>
#include <Fuse.Reactive.Constant.h>
#include <Fuse.Reactive.Data.h>
#include <Fuse.Reactive.DataBinding.h>
#include <Fuse.Reactive.EventBinding.h>
#include <Fuse.Reactive.Expression.h>
#include <Fuse.Reactive.IExpression.h>
#include <Fuse.Reactive.Property.h>
#include <Fuse.Visual.h>
#include <Uno.Bool.h>
#include <Uno.Collections.ICollection-1.h>
#include <Uno.Collections.IList-1.h>
#include <Uno.Float.h>
#include <Uno.Int.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.UX.Property.h>
#include <Uno.UX.Property1-1.h>
#include <Uno.UX.PropertyAccessor.h>
#include <Uno.UX.Selector.h>
static uString* STRINGS[8];
static uType* TYPES[2];

namespace g{

// public partial sealed class DropdownMenu.Template :95
// {
// static Template() :110
static void DropdownMenu__Template__cctor__fn(uType* __type)
{
    DropdownMenu__Template::__selector0_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[0/*"FontSize"*/]);
    DropdownMenu__Template::__selector1_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[1/*"TextColor"*/]);
    DropdownMenu__Template::__selector2_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[2/*"Text"*/]);
    DropdownMenu__Template::__selector3_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[3/*"BackgroundC...*/]);
    DropdownMenu__Template::__selector4_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[4/*"BorderColor"*/]);
}

static void DropdownMenu__Template_build(uType* type)
{
    ::STRINGS[0] = uString::Const("FontSize");
    ::STRINGS[1] = uString::Const("TextColor");
    ::STRINGS[2] = uString::Const("Text");
    ::STRINGS[3] = uString::Const("BackgroundColor");
    ::STRINGS[4] = uString::Const("BorderColor");
    ::STRINGS[5] = uString::Const("name");
    ::STRINGS[6] = uString::Const("onSelected");
    ::STRINGS[7] = uString::Const("dropdown.ux");
    ::TYPES[0] = ::g::Fuse::Gestures::ClickedHandler_typeof();
    ::TYPES[1] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL);
    type->SetDependencies(
        ::g::OSP_accessor_DropdownMenu_BackgroundColor_typeof(),
        ::g::OSP_accessor_DropdownMenu_BorderColor_typeof(),
        ::g::OSP_accessor_DropdownMenu_FontSize_typeof(),
        ::g::OSP_accessor_DropdownMenu_TextColor_typeof());
    type->SetFields(2,
        ::g::DropdownMenu_typeof(), offsetof(DropdownMenu__Template, __parent1), uFieldFlagsWeak,
        ::g::DropdownMenu_typeof(), offsetof(DropdownMenu__Template, __parentInstance1), uFieldFlagsWeak,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(DropdownMenu__Template, __self_FontSize_inst1), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL), offsetof(DropdownMenu__Template, __self_TextColor_inst1), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(DropdownMenu__Template, __self_Text_inst1), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Fuse::Drawing::Brush_typeof(), NULL), offsetof(DropdownMenu__Template, __self_BackgroundColor_inst1), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Fuse::Drawing::Brush_typeof(), NULL), offsetof(DropdownMenu__Template, __self_BorderColor_inst1), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(DropdownMenu__Template, temp_eb13), 0,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&DropdownMenu__Template::__selector0_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&DropdownMenu__Template::__selector1_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&DropdownMenu__Template::__selector2_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&DropdownMenu__Template::__selector3_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&DropdownMenu__Template::__selector4_, uFieldFlagsStatic);
    type->Reflection.SetFunctions(1,
        new uFunction(".ctor", NULL, (void*)DropdownMenu__Template__New2_fn, 0, true, type, 2, ::g::DropdownMenu_typeof(), ::g::DropdownMenu_typeof()));
}

::g::Uno::UX::Template_type* DropdownMenu__Template_typeof()
{
    static uSStrong< ::g::Uno::UX::Template_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::Template_typeof();
    options.FieldCount = 15;
    options.DependencyCount = 4;
    options.ObjectSize = sizeof(DropdownMenu__Template);
    options.TypeSize = sizeof(::g::Uno::UX::Template_type);
    type = (::g::Uno::UX::Template_type*)uClassType::New("DropdownMenu.Template", options);
    type->fp_build_ = DropdownMenu__Template_build;
    type->fp_cctor_ = DropdownMenu__Template__cctor__fn;
    type->fp_New1 = (void(*)(::g::Uno::UX::Template*, uObject**))DropdownMenu__Template__New1_fn;
    return type;
}

// public Template(DropdownMenu parent, DropdownMenu parentInstance) :99
void DropdownMenu__Template__ctor_1_fn(DropdownMenu__Template* __this, ::g::DropdownMenu* parent, ::g::DropdownMenu* parentInstance)
{
    __this->ctor_1(parent, parentInstance);
}

// public override sealed object New() :113
void DropdownMenu__Template__New1_fn(DropdownMenu__Template* __this, uObject** __retval)
{
    uStackFrame __("DropdownMenu.Template", "New()");
    ::g::DropdownOption* __self1 = ::g::DropdownOption::New4();
    ::g::Fuse::Reactive::Constant* temp = ::g::Fuse::Reactive::Constant::New1(__this->__parent1);
    __this->__self_FontSize_inst1 = ::g::OSP_DropdownOption_FontSize_Property::New1(__self1, DropdownMenu__Template::__selector0_);
    ::g::Fuse::Reactive::Property* temp1 = ::g::Fuse::Reactive::Property::New1(temp, ::g::OSP_accessor_DropdownMenu_FontSize::Singleton());
    ::g::Fuse::Reactive::Constant* temp2 = ::g::Fuse::Reactive::Constant::New1(__this->__parent1);
    __this->__self_TextColor_inst1 = ::g::OSP_DropdownOption_TextColor_Property::New1(__self1, DropdownMenu__Template::__selector1_);
    ::g::Fuse::Reactive::Property* temp3 = ::g::Fuse::Reactive::Property::New1(temp2, ::g::OSP_accessor_DropdownMenu_TextColor::Singleton());
    __this->__self_Text_inst1 = ::g::OSP_DropdownOption_Text_Property::New1(__self1, DropdownMenu__Template::__selector2_);
    ::g::Fuse::Reactive::Data* temp4 = ::g::Fuse::Reactive::Data::New1(::STRINGS[5/*"name"*/]);
    ::g::Fuse::Reactive::Constant* temp5 = ::g::Fuse::Reactive::Constant::New1(__this->__parent1);
    __this->__self_BackgroundColor_inst1 = ::g::OSP_DropdownOption_BackgroundColor_Property::New1(__self1, DropdownMenu__Template::__selector3_);
    ::g::Fuse::Reactive::Property* temp6 = ::g::Fuse::Reactive::Property::New1(temp5, ::g::OSP_accessor_DropdownMenu_BackgroundColor::Singleton());
    ::g::Fuse::Reactive::Data* temp7 = ::g::Fuse::Reactive::Data::New1(::STRINGS[6/*"onSelected"*/]);
    ::g::Fuse::Reactive::Constant* temp8 = ::g::Fuse::Reactive::Constant::New1(__this->__parent1);
    __this->__self_BorderColor_inst1 = ::g::OSP_DropdownOption_BorderColor_Property::New1(__self1, DropdownMenu__Template::__selector4_);
    ::g::Fuse::Reactive::Property* temp9 = ::g::Fuse::Reactive::Property::New1(temp8, ::g::OSP_accessor_DropdownMenu_BorderColor::Singleton());
    ::g::Fuse::Reactive::DataBinding* temp10 = ::g::Fuse::Reactive::DataBinding::New1(__this->__self_FontSize_inst1, (uObject*)temp1, 1);
    ::g::Fuse::Reactive::DataBinding* temp11 = ::g::Fuse::Reactive::DataBinding::New1(__this->__self_TextColor_inst1, (uObject*)temp3, 1);
    ::g::Fuse::Reactive::DataBinding* temp12 = ::g::Fuse::Reactive::DataBinding::New1(__this->__self_Text_inst1, (uObject*)temp4, 3);
    ::g::Fuse::Reactive::DataBinding* temp13 = ::g::Fuse::Reactive::DataBinding::New1(__this->__self_BackgroundColor_inst1, (uObject*)temp6, 1);
    __this->temp_eb13 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp7);
    ::g::Fuse::Reactive::DataBinding* temp14 = ::g::Fuse::Reactive::DataBinding::New1(__this->__self_BorderColor_inst1, (uObject*)temp9, 1);
    __self1->SourceLineNumber(97);
    __self1->SourceFileName(::STRINGS[7/*"dropdown.ux"*/]);
    ::g::Fuse::Gestures::Clicked::AddHandler(__self1, uDelegate::New(::TYPES[0/*Fuse.Gestures.ClickedHandler*/], (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(__this->temp_eb13)));
    temp1->SourceLineNumber(97);
    temp1->SourceFileName(::STRINGS[7/*"dropdown.ux"*/]);
    temp->SourceLineNumber(97);
    temp->SourceFileName(::STRINGS[7/*"dropdown.ux"*/]);
    temp3->SourceLineNumber(97);
    temp3->SourceFileName(::STRINGS[7/*"dropdown.ux"*/]);
    temp2->SourceLineNumber(97);
    temp2->SourceFileName(::STRINGS[7/*"dropdown.ux"*/]);
    temp4->SourceLineNumber(97);
    temp4->SourceFileName(::STRINGS[7/*"dropdown.ux"*/]);
    temp6->SourceLineNumber(97);
    temp6->SourceFileName(::STRINGS[7/*"dropdown.ux"*/]);
    temp5->SourceLineNumber(97);
    temp5->SourceFileName(::STRINGS[7/*"dropdown.ux"*/]);
    temp7->SourceLineNumber(97);
    temp7->SourceFileName(::STRINGS[7/*"dropdown.ux"*/]);
    temp9->SourceLineNumber(97);
    temp9->SourceFileName(::STRINGS[7/*"dropdown.ux"*/]);
    temp8->SourceLineNumber(97);
    temp8->SourceFileName(::STRINGS[7/*"dropdown.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(__self1->Bindings()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp10);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(__self1->Bindings()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp11);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(__self1->Bindings()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp12);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(__self1->Bindings()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp13);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(__self1->Bindings()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Binding>*/]), __this->temp_eb13);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(__self1->Bindings()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp14);
    return *__retval = __self1, void();
}

// public Template New(DropdownMenu parent, DropdownMenu parentInstance) :99
void DropdownMenu__Template__New2_fn(::g::DropdownMenu* parent, ::g::DropdownMenu* parentInstance, DropdownMenu__Template** __retval)
{
    *__retval = DropdownMenu__Template::New2(parent, parentInstance);
}

::g::Uno::UX::Selector DropdownMenu__Template::__selector0_;
::g::Uno::UX::Selector DropdownMenu__Template::__selector1_;
::g::Uno::UX::Selector DropdownMenu__Template::__selector2_;
::g::Uno::UX::Selector DropdownMenu__Template::__selector3_;
::g::Uno::UX::Selector DropdownMenu__Template::__selector4_;

// public Template(DropdownMenu parent, DropdownMenu parentInstance) [instance] :99
void DropdownMenu__Template::ctor_1(::g::DropdownMenu* parent, ::g::DropdownMenu* parentInstance)
{
    ctor_(NULL, false);
    __parent1 = parent;
    __parentInstance1 = parentInstance;
}

// public Template New(DropdownMenu parent, DropdownMenu parentInstance) [static] :99
DropdownMenu__Template* DropdownMenu__Template::New2(::g::DropdownMenu* parent, ::g::DropdownMenu* parentInstance)
{
    DropdownMenu__Template* obj1 = (DropdownMenu__Template*)uNew(DropdownMenu__Template_typeof());
    obj1->ctor_1(parent, parentInstance);
    return obj1;
}
// }

} // ::g
