// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.OSP_accessor_Tab_TColor.h>
#include <_root.Tab.h>
#include <Uno.Bool.h>
#include <Uno.Float4.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.Type.h>
#include <Uno.UX.IPropertyListener.h>
#include <Uno.UX.PropertyObject.h>
#include <Uno.UX.Selector.h>
static uString* STRINGS[1];
static uType* TYPES[2];

namespace g{

// internal sealed class OSP_accessor_Tab_TColor :171
// {
// static generated OSP_accessor_Tab_TColor() :171
static void OSP_accessor_Tab_TColor__cctor__fn(uType* __type)
{
    OSP_accessor_Tab_TColor::Singleton_ = OSP_accessor_Tab_TColor::New1();
    OSP_accessor_Tab_TColor::_name_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[0/*"TColor"*/]);
}

static void OSP_accessor_Tab_TColor_build(uType* type)
{
    ::STRINGS[0] = uString::Const("TColor");
    ::TYPES[0] = ::g::Tab_typeof();
    ::TYPES[1] = ::g::Uno::Type_typeof();
    type->SetFields(0,
        ::g::Uno::UX::PropertyAccessor_typeof(), (uintptr_t)&OSP_accessor_Tab_TColor::Singleton_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&OSP_accessor_Tab_TColor::_name_, uFieldFlagsStatic);
}

::g::Uno::UX::PropertyAccessor_type* OSP_accessor_Tab_TColor_typeof()
{
    static uSStrong< ::g::Uno::UX::PropertyAccessor_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::PropertyAccessor_typeof();
    options.FieldCount = 2;
    options.ObjectSize = sizeof(OSP_accessor_Tab_TColor);
    options.TypeSize = sizeof(::g::Uno::UX::PropertyAccessor_type);
    type = (::g::Uno::UX::PropertyAccessor_type*)uClassType::New("OSP_accessor_Tab_TColor", options);
    type->fp_build_ = OSP_accessor_Tab_TColor_build;
    type->fp_ctor_ = (void*)OSP_accessor_Tab_TColor__New1_fn;
    type->fp_cctor_ = OSP_accessor_Tab_TColor__cctor__fn;
    type->fp_GetAsObject = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::PropertyObject*, uObject**))OSP_accessor_Tab_TColor__GetAsObject_fn;
    type->fp_get_Name = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::Selector*))OSP_accessor_Tab_TColor__get_Name_fn;
    type->fp_get_PropertyType = (void(*)(::g::Uno::UX::PropertyAccessor*, uType**))OSP_accessor_Tab_TColor__get_PropertyType_fn;
    type->fp_SetAsObject = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::PropertyObject*, uObject*, uObject*))OSP_accessor_Tab_TColor__SetAsObject_fn;
    type->fp_get_SupportsOriginSetter = (void(*)(::g::Uno::UX::PropertyAccessor*, bool*))OSP_accessor_Tab_TColor__get_SupportsOriginSetter_fn;
    return type;
}

// public generated OSP_accessor_Tab_TColor() :171
void OSP_accessor_Tab_TColor__ctor_1_fn(OSP_accessor_Tab_TColor* __this)
{
    __this->ctor_1();
}

// public override sealed object GetAsObject(Uno.UX.PropertyObject obj) :177
void OSP_accessor_Tab_TColor__GetAsObject_fn(OSP_accessor_Tab_TColor* __this, ::g::Uno::UX::PropertyObject* obj, uObject** __retval)
{
    uStackFrame __("OSP_accessor_Tab_TColor", "GetAsObject(Uno.UX.PropertyObject)");
    return *__retval = uBox(::g::Uno::Float4_typeof(), uPtr(uCast< ::g::Tab*>(obj, ::TYPES[0/*Tab*/]))->TColor()), void();
}

// public override sealed Uno.UX.Selector get_Name() :174
void OSP_accessor_Tab_TColor__get_Name_fn(OSP_accessor_Tab_TColor* __this, ::g::Uno::UX::Selector* __retval)
{
    return *__retval = OSP_accessor_Tab_TColor::_name_, void();
}

// public generated OSP_accessor_Tab_TColor New() :171
void OSP_accessor_Tab_TColor__New1_fn(OSP_accessor_Tab_TColor** __retval)
{
    *__retval = OSP_accessor_Tab_TColor::New1();
}

// public override sealed Uno.Type get_PropertyType() :176
void OSP_accessor_Tab_TColor__get_PropertyType_fn(OSP_accessor_Tab_TColor* __this, uType** __retval)
{
    return *__retval = ::g::Uno::Float4_typeof(), void();
}

// public override sealed void SetAsObject(Uno.UX.PropertyObject obj, object v, Uno.UX.IPropertyListener origin) :178
void OSP_accessor_Tab_TColor__SetAsObject_fn(OSP_accessor_Tab_TColor* __this, ::g::Uno::UX::PropertyObject* obj, uObject* v, uObject* origin)
{
    uStackFrame __("OSP_accessor_Tab_TColor", "SetAsObject(Uno.UX.PropertyObject,object,Uno.UX.IPropertyListener)");
    uPtr(uCast< ::g::Tab*>(obj, ::TYPES[0/*Tab*/]))->SetTColor(uUnbox< ::g::Uno::Float4>(::g::Uno::Float4_typeof(), v), origin);
}

// public override sealed bool get_SupportsOriginSetter() :179
void OSP_accessor_Tab_TColor__get_SupportsOriginSetter_fn(OSP_accessor_Tab_TColor* __this, bool* __retval)
{
    return *__retval = true, void();
}

uSStrong< ::g::Uno::UX::PropertyAccessor*> OSP_accessor_Tab_TColor::Singleton_;
::g::Uno::UX::Selector OSP_accessor_Tab_TColor::_name_;

// public generated OSP_accessor_Tab_TColor() [instance] :171
void OSP_accessor_Tab_TColor::ctor_1()
{
    ctor_();
}

// public generated OSP_accessor_Tab_TColor New() [static] :171
OSP_accessor_Tab_TColor* OSP_accessor_Tab_TColor::New1()
{
    OSP_accessor_Tab_TColor* obj1 = (OSP_accessor_Tab_TColor*)uNew(OSP_accessor_Tab_TColor_typeof());
    obj1->ctor_1();
    return obj1;
}
// }

} // ::g
