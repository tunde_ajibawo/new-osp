// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.DropdownSelectedItem.h>
#include <_root.OSP_DropdownSel-5e7e0c31.h>
#include <Uno.Bool.h>
#include <Uno.UX.IPropertyListener.h>
#include <Uno.UX.PropertyObject.h>
#include <Uno.UX.Selector.h>
static uType* TYPES[1];

namespace g{

// internal sealed class OSP_DropdownSelectedItem_BorderColor_Property :547
// {
static void OSP_DropdownSelectedItem_BorderColor_Property_build(uType* type)
{
    ::TYPES[0] = ::g::DropdownSelectedItem_typeof();
    type->SetBase(::g::Uno::UX::Property1_typeof()->MakeType(::g::Fuse::Drawing::Brush_typeof(), NULL));
    type->SetFields(1,
        ::TYPES[0/*DropdownSelectedItem*/], offsetof(OSP_DropdownSelectedItem_BorderColor_Property, _obj), uFieldFlagsWeak);
}

::g::Uno::UX::Property1_type* OSP_DropdownSelectedItem_BorderColor_Property_typeof()
{
    static uSStrong< ::g::Uno::UX::Property1_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::Property1_typeof();
    options.FieldCount = 2;
    options.ObjectSize = sizeof(OSP_DropdownSelectedItem_BorderColor_Property);
    options.TypeSize = sizeof(::g::Uno::UX::Property1_type);
    type = (::g::Uno::UX::Property1_type*)uClassType::New("OSP_DropdownSelectedItem_BorderColor_Property", options);
    type->fp_build_ = OSP_DropdownSelectedItem_BorderColor_Property_build;
    type->fp_Get1 = (void(*)(::g::Uno::UX::Property1*, ::g::Uno::UX::PropertyObject*, uTRef))OSP_DropdownSelectedItem_BorderColor_Property__Get1_fn;
    type->fp_get_Object = (void(*)(::g::Uno::UX::Property*, ::g::Uno::UX::PropertyObject**))OSP_DropdownSelectedItem_BorderColor_Property__get_Object_fn;
    type->fp_Set1 = (void(*)(::g::Uno::UX::Property1*, ::g::Uno::UX::PropertyObject*, void*, uObject*))OSP_DropdownSelectedItem_BorderColor_Property__Set1_fn;
    type->fp_get_SupportsOriginSetter = (void(*)(::g::Uno::UX::PropertyAccessor*, bool*))OSP_DropdownSelectedItem_BorderColor_Property__get_SupportsOriginSetter_fn;
    return type;
}

// public OSP_DropdownSelectedItem_BorderColor_Property(DropdownSelectedItem obj, Uno.UX.Selector name) :550
void OSP_DropdownSelectedItem_BorderColor_Property__ctor_3_fn(OSP_DropdownSelectedItem_BorderColor_Property* __this, ::g::DropdownSelectedItem* obj, ::g::Uno::UX::Selector* name)
{
    __this->ctor_3(obj, *name);
}

// public override sealed Fuse.Drawing.Brush Get(Uno.UX.PropertyObject obj) :552
void OSP_DropdownSelectedItem_BorderColor_Property__Get1_fn(OSP_DropdownSelectedItem_BorderColor_Property* __this, ::g::Uno::UX::PropertyObject* obj, ::g::Fuse::Drawing::Brush** __retval)
{
    uStackFrame __("OSP_DropdownSelectedItem_BorderColor_Property", "Get(Uno.UX.PropertyObject)");
    return *__retval = uPtr(uCast< ::g::DropdownSelectedItem*>(obj, ::TYPES[0/*DropdownSelectedItem*/]))->BorderColor(), void();
}

// public OSP_DropdownSelectedItem_BorderColor_Property New(DropdownSelectedItem obj, Uno.UX.Selector name) :550
void OSP_DropdownSelectedItem_BorderColor_Property__New1_fn(::g::DropdownSelectedItem* obj, ::g::Uno::UX::Selector* name, OSP_DropdownSelectedItem_BorderColor_Property** __retval)
{
    *__retval = OSP_DropdownSelectedItem_BorderColor_Property::New1(obj, *name);
}

// public override sealed Uno.UX.PropertyObject get_Object() :551
void OSP_DropdownSelectedItem_BorderColor_Property__get_Object_fn(OSP_DropdownSelectedItem_BorderColor_Property* __this, ::g::Uno::UX::PropertyObject** __retval)
{
    return *__retval = __this->_obj, void();
}

// public override sealed void Set(Uno.UX.PropertyObject obj, Fuse.Drawing.Brush v, Uno.UX.IPropertyListener origin) :553
void OSP_DropdownSelectedItem_BorderColor_Property__Set1_fn(OSP_DropdownSelectedItem_BorderColor_Property* __this, ::g::Uno::UX::PropertyObject* obj, ::g::Fuse::Drawing::Brush* v, uObject* origin)
{
    uStackFrame __("OSP_DropdownSelectedItem_BorderColor_Property", "Set(Uno.UX.PropertyObject,Fuse.Drawing.Brush,Uno.UX.IPropertyListener)");
    uPtr(uCast< ::g::DropdownSelectedItem*>(obj, ::TYPES[0/*DropdownSelectedItem*/]))->SetBorderColor(v, origin);
}

// public override sealed bool get_SupportsOriginSetter() :554
void OSP_DropdownSelectedItem_BorderColor_Property__get_SupportsOriginSetter_fn(OSP_DropdownSelectedItem_BorderColor_Property* __this, bool* __retval)
{
    return *__retval = true, void();
}

// public OSP_DropdownSelectedItem_BorderColor_Property(DropdownSelectedItem obj, Uno.UX.Selector name) [instance] :550
void OSP_DropdownSelectedItem_BorderColor_Property::ctor_3(::g::DropdownSelectedItem* obj, ::g::Uno::UX::Selector name)
{
    ctor_2(name);
    _obj = obj;
}

// public OSP_DropdownSelectedItem_BorderColor_Property New(DropdownSelectedItem obj, Uno.UX.Selector name) [static] :550
OSP_DropdownSelectedItem_BorderColor_Property* OSP_DropdownSelectedItem_BorderColor_Property::New1(::g::DropdownSelectedItem* obj, ::g::Uno::UX::Selector name)
{
    OSP_DropdownSelectedItem_BorderColor_Property* obj1 = (OSP_DropdownSelectedItem_BorderColor_Property*)uNew(OSP_DropdownSelectedItem_BorderColor_Property_typeof());
    obj1->ctor_3(obj, name);
    return obj1;
}
// }

} // ::g
