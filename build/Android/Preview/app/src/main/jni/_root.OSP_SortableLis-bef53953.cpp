// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.OSP_SortableLis-bef53953.h>
#include <_root.SortableList.h>
#include <Uno.Bool.h>
#include <Uno.UX.IPropertyListener.h>
#include <Uno.UX.PropertyObject.h>
#include <Uno.UX.Selector.h>
static uType* TYPES[1];

namespace g{

// internal sealed class OSP_SortableList_Label_Property :702
// {
static void OSP_SortableList_Label_Property_build(uType* type)
{
    ::TYPES[0] = ::g::SortableList_typeof();
    type->SetBase(::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL));
    type->SetFields(1,
        ::TYPES[0/*SortableList*/], offsetof(OSP_SortableList_Label_Property, _obj), uFieldFlagsWeak);
}

::g::Uno::UX::Property1_type* OSP_SortableList_Label_Property_typeof()
{
    static uSStrong< ::g::Uno::UX::Property1_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::Property1_typeof();
    options.FieldCount = 2;
    options.ObjectSize = sizeof(OSP_SortableList_Label_Property);
    options.TypeSize = sizeof(::g::Uno::UX::Property1_type);
    type = (::g::Uno::UX::Property1_type*)uClassType::New("OSP_SortableList_Label_Property", options);
    type->fp_build_ = OSP_SortableList_Label_Property_build;
    type->fp_Get1 = (void(*)(::g::Uno::UX::Property1*, ::g::Uno::UX::PropertyObject*, uTRef))OSP_SortableList_Label_Property__Get1_fn;
    type->fp_get_Object = (void(*)(::g::Uno::UX::Property*, ::g::Uno::UX::PropertyObject**))OSP_SortableList_Label_Property__get_Object_fn;
    type->fp_Set1 = (void(*)(::g::Uno::UX::Property1*, ::g::Uno::UX::PropertyObject*, void*, uObject*))OSP_SortableList_Label_Property__Set1_fn;
    type->fp_get_SupportsOriginSetter = (void(*)(::g::Uno::UX::PropertyAccessor*, bool*))OSP_SortableList_Label_Property__get_SupportsOriginSetter_fn;
    return type;
}

// public OSP_SortableList_Label_Property(SortableList obj, Uno.UX.Selector name) :705
void OSP_SortableList_Label_Property__ctor_3_fn(OSP_SortableList_Label_Property* __this, ::g::SortableList* obj, ::g::Uno::UX::Selector* name)
{
    __this->ctor_3(obj, *name);
}

// public override sealed string Get(Uno.UX.PropertyObject obj) :707
void OSP_SortableList_Label_Property__Get1_fn(OSP_SortableList_Label_Property* __this, ::g::Uno::UX::PropertyObject* obj, uString** __retval)
{
    uStackFrame __("OSP_SortableList_Label_Property", "Get(Uno.UX.PropertyObject)");
    return *__retval = uPtr(uCast< ::g::SortableList*>(obj, ::TYPES[0/*SortableList*/]))->Label(), void();
}

// public OSP_SortableList_Label_Property New(SortableList obj, Uno.UX.Selector name) :705
void OSP_SortableList_Label_Property__New1_fn(::g::SortableList* obj, ::g::Uno::UX::Selector* name, OSP_SortableList_Label_Property** __retval)
{
    *__retval = OSP_SortableList_Label_Property::New1(obj, *name);
}

// public override sealed Uno.UX.PropertyObject get_Object() :706
void OSP_SortableList_Label_Property__get_Object_fn(OSP_SortableList_Label_Property* __this, ::g::Uno::UX::PropertyObject** __retval)
{
    return *__retval = __this->_obj, void();
}

// public override sealed void Set(Uno.UX.PropertyObject obj, string v, Uno.UX.IPropertyListener origin) :708
void OSP_SortableList_Label_Property__Set1_fn(OSP_SortableList_Label_Property* __this, ::g::Uno::UX::PropertyObject* obj, uString* v, uObject* origin)
{
    uStackFrame __("OSP_SortableList_Label_Property", "Set(Uno.UX.PropertyObject,string,Uno.UX.IPropertyListener)");
    uPtr(uCast< ::g::SortableList*>(obj, ::TYPES[0/*SortableList*/]))->SetLabel(v, origin);
}

// public override sealed bool get_SupportsOriginSetter() :709
void OSP_SortableList_Label_Property__get_SupportsOriginSetter_fn(OSP_SortableList_Label_Property* __this, bool* __retval)
{
    return *__retval = true, void();
}

// public OSP_SortableList_Label_Property(SortableList obj, Uno.UX.Selector name) [instance] :705
void OSP_SortableList_Label_Property::ctor_3(::g::SortableList* obj, ::g::Uno::UX::Selector name)
{
    ctor_2(name);
    _obj = obj;
}

// public OSP_SortableList_Label_Property New(SortableList obj, Uno.UX.Selector name) [static] :705
OSP_SortableList_Label_Property* OSP_SortableList_Label_Property::New1(::g::SortableList* obj, ::g::Uno::UX::Selector name)
{
    OSP_SortableList_Label_Property* obj1 = (OSP_SortableList_Label_Property*)uNew(OSP_SortableList_Label_Property_typeof());
    obj1->ctor_3(obj, name);
    return obj1;
}
// }

} // ::g
