// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.InputField.h>
#include <_root.OSP_accessor_In-4ef0ebca.h>
#include <Uno.Bool.h>
#include <Uno.Float4.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.Type.h>
#include <Uno.UX.IPropertyListener.h>
#include <Uno.UX.PropertyObject.h>
#include <Uno.UX.Selector.h>
static uString* STRINGS[1];
static uType* TYPES[2];

namespace g{

// internal sealed class OSP_accessor_InputField_BorderColor :351
// {
// static generated OSP_accessor_InputField_BorderColor() :351
static void OSP_accessor_InputField_BorderColor__cctor__fn(uType* __type)
{
    OSP_accessor_InputField_BorderColor::Singleton_ = OSP_accessor_InputField_BorderColor::New1();
    OSP_accessor_InputField_BorderColor::_name_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[0/*"BorderColor"*/]);
}

static void OSP_accessor_InputField_BorderColor_build(uType* type)
{
    ::STRINGS[0] = uString::Const("BorderColor");
    ::TYPES[0] = ::g::InputField_typeof();
    ::TYPES[1] = ::g::Uno::Type_typeof();
    type->SetFields(0,
        ::g::Uno::UX::PropertyAccessor_typeof(), (uintptr_t)&OSP_accessor_InputField_BorderColor::Singleton_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&OSP_accessor_InputField_BorderColor::_name_, uFieldFlagsStatic);
}

::g::Uno::UX::PropertyAccessor_type* OSP_accessor_InputField_BorderColor_typeof()
{
    static uSStrong< ::g::Uno::UX::PropertyAccessor_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::PropertyAccessor_typeof();
    options.FieldCount = 2;
    options.ObjectSize = sizeof(OSP_accessor_InputField_BorderColor);
    options.TypeSize = sizeof(::g::Uno::UX::PropertyAccessor_type);
    type = (::g::Uno::UX::PropertyAccessor_type*)uClassType::New("OSP_accessor_InputField_BorderColor", options);
    type->fp_build_ = OSP_accessor_InputField_BorderColor_build;
    type->fp_ctor_ = (void*)OSP_accessor_InputField_BorderColor__New1_fn;
    type->fp_cctor_ = OSP_accessor_InputField_BorderColor__cctor__fn;
    type->fp_GetAsObject = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::PropertyObject*, uObject**))OSP_accessor_InputField_BorderColor__GetAsObject_fn;
    type->fp_get_Name = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::Selector*))OSP_accessor_InputField_BorderColor__get_Name_fn;
    type->fp_get_PropertyType = (void(*)(::g::Uno::UX::PropertyAccessor*, uType**))OSP_accessor_InputField_BorderColor__get_PropertyType_fn;
    type->fp_SetAsObject = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::PropertyObject*, uObject*, uObject*))OSP_accessor_InputField_BorderColor__SetAsObject_fn;
    type->fp_get_SupportsOriginSetter = (void(*)(::g::Uno::UX::PropertyAccessor*, bool*))OSP_accessor_InputField_BorderColor__get_SupportsOriginSetter_fn;
    return type;
}

// public generated OSP_accessor_InputField_BorderColor() :351
void OSP_accessor_InputField_BorderColor__ctor_1_fn(OSP_accessor_InputField_BorderColor* __this)
{
    __this->ctor_1();
}

// public override sealed object GetAsObject(Uno.UX.PropertyObject obj) :357
void OSP_accessor_InputField_BorderColor__GetAsObject_fn(OSP_accessor_InputField_BorderColor* __this, ::g::Uno::UX::PropertyObject* obj, uObject** __retval)
{
    uStackFrame __("OSP_accessor_InputField_BorderColor", "GetAsObject(Uno.UX.PropertyObject)");
    return *__retval = uBox(::g::Uno::Float4_typeof(), uPtr(uCast< ::g::InputField*>(obj, ::TYPES[0/*InputField*/]))->BorderColor()), void();
}

// public override sealed Uno.UX.Selector get_Name() :354
void OSP_accessor_InputField_BorderColor__get_Name_fn(OSP_accessor_InputField_BorderColor* __this, ::g::Uno::UX::Selector* __retval)
{
    return *__retval = OSP_accessor_InputField_BorderColor::_name_, void();
}

// public generated OSP_accessor_InputField_BorderColor New() :351
void OSP_accessor_InputField_BorderColor__New1_fn(OSP_accessor_InputField_BorderColor** __retval)
{
    *__retval = OSP_accessor_InputField_BorderColor::New1();
}

// public override sealed Uno.Type get_PropertyType() :356
void OSP_accessor_InputField_BorderColor__get_PropertyType_fn(OSP_accessor_InputField_BorderColor* __this, uType** __retval)
{
    return *__retval = ::g::Uno::Float4_typeof(), void();
}

// public override sealed void SetAsObject(Uno.UX.PropertyObject obj, object v, Uno.UX.IPropertyListener origin) :358
void OSP_accessor_InputField_BorderColor__SetAsObject_fn(OSP_accessor_InputField_BorderColor* __this, ::g::Uno::UX::PropertyObject* obj, uObject* v, uObject* origin)
{
    uStackFrame __("OSP_accessor_InputField_BorderColor", "SetAsObject(Uno.UX.PropertyObject,object,Uno.UX.IPropertyListener)");
    uPtr(uCast< ::g::InputField*>(obj, ::TYPES[0/*InputField*/]))->SetBorderColor(uUnbox< ::g::Uno::Float4>(::g::Uno::Float4_typeof(), v), origin);
}

// public override sealed bool get_SupportsOriginSetter() :359
void OSP_accessor_InputField_BorderColor__get_SupportsOriginSetter_fn(OSP_accessor_InputField_BorderColor* __this, bool* __retval)
{
    return *__retval = true, void();
}

uSStrong< ::g::Uno::UX::PropertyAccessor*> OSP_accessor_InputField_BorderColor::Singleton_;
::g::Uno::UX::Selector OSP_accessor_InputField_BorderColor::_name_;

// public generated OSP_accessor_InputField_BorderColor() [instance] :351
void OSP_accessor_InputField_BorderColor::ctor_1()
{
    ctor_();
}

// public generated OSP_accessor_InputField_BorderColor New() [static] :351
OSP_accessor_InputField_BorderColor* OSP_accessor_InputField_BorderColor::New1()
{
    OSP_accessor_InputField_BorderColor* obj1 = (OSP_accessor_InputField_BorderColor*)uNew(OSP_accessor_InputField_BorderColor_typeof());
    obj1->ctor_1();
    return obj1;
}
// }

} // ::g
