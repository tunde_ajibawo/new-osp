// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/History.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.History.h>
#include <_root.History.Template.h>
#include <_root.OSP_bundle.h>
#include <_root.OSP_FuseControl-83fd107.h>
#include <_root.OSP_FuseControl-bf62c319.h>
#include <_root.OSP_FuseReactiv-f855d0e4.h>
#include <_root.Tab.h>
#include <_root.TopBar.h>
#include <Fuse.Animations.Animator.h>
#include <Fuse.Animations.Change-1.h>
#include <Fuse.Controls.Control.h>
#include <Fuse.Controls.DockPanel.h>
#include <Fuse.Controls.Grid.h>
#include <Fuse.Controls.Navigat-70e90308.h>
#include <Fuse.Controls.PageControl.h>
#include <Fuse.Controls.Panel.h>
#include <Fuse.Controls.ScrollView.h>
#include <Fuse.Controls.StackPanel.h>
#include <Fuse.Drawing.Brush.h>
#include <Fuse.Drawing.StaticSolidColor.h>
#include <Fuse.Elements.Element.h>
#include <Fuse.Gestures.Clicked.h>
#include <Fuse.Layouts.Dock.h>
#include <Fuse.Navigation.Router.h>
#include <Fuse.Navigation.While-89f5a828.h>
#include <Fuse.Navigation.WhileActive.h>
#include <Fuse.Reactive.BindingMode.h>
#include <Fuse.Reactive.Data.h>
#include <Fuse.Reactive.DataBinding.h>
#include <Fuse.Reactive.Each.h>
#include <Fuse.Reactive.EventBinding.h>
#include <Fuse.Reactive.Expression.h>
#include <Fuse.Reactive.IExpression.h>
#include <Fuse.Reactive.Instantiator.h>
#include <Fuse.Reactive.JavaScript.h>
#include <Fuse.Triggers.Actions.Callback.h>
#include <Fuse.Triggers.Actions.Set-1.h>
#include <Fuse.Triggers.Actions-fcab7e57.h>
#include <Fuse.Triggers.Trigger.h>
#include <Fuse.VisualEventHandler.h>
#include <Uno.Float.h>
#include <Uno.Int.h>
#include <Uno.IO.BundleFile.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.UX.BundleFileSource.h>
#include <Uno.UX.FileSource.h>
#include <Uno.UX.NameTable.h>
#include <Uno.UX.Property.h>
#include <Uno.UX.Property1-1.h>
#include <Uno.UX.Selector.h>
#include <Uno.UX.Size.h>
#include <Uno.UX.Template.h>
#include <Uno.UX.Unit.h>

namespace g{

// public partial sealed class History :2
// {
// static History() :131
static void History__cctor_4_fn(uType* __type)
{
    History::__g_static_nametable1_ = uArray::Init<uString*>(::g::Uno::String_typeof()->Array(), 6, uString::Const("router"), uString::Const("page1Tab"), uString::Const("trip"), uString::Const("temp_eb15"), uString::Const("navigation"), uString::Const("page1"));
    History::__selector0_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("Active"));
    History::__selector1_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("Color"));
    History::__selector2_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("Items"));
    History::__selector3_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("page1Tab"));
    History::__selector4_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("trip"));
    History::__selector5_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("navigation"));
    History::__selector6_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("page1"));
}

static void History_build(uType* type)
{
    type->SetDependencies(
        ::g::OSP_bundle_typeof());
    type->SetInterfaces(
        ::g::Uno::Collections::IList_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface0),
        ::g::Fuse::Scripting::IScriptObject_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface1),
        ::g::Fuse::IProperties_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface2),
        ::g::Fuse::INotifyUnrooted_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface3),
        ::g::Fuse::ISourceLocation_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface4),
        ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface5),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface6),
        ::g::Uno::Collections::IList_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface7),
        ::g::Uno::UX::IPropertyListener_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface8),
        ::g::Fuse::ITemplateSource_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface9),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Visual_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface10),
        ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface11),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface12),
        ::g::Fuse::Triggers::Actions::IShow_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface13),
        ::g::Fuse::Triggers::Actions::IHide_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface14),
        ::g::Fuse::Triggers::Actions::ICollapse_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface15),
        ::g::Fuse::IActualPlacement_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface16),
        ::g::Fuse::Animations::IResize_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface17),
        ::g::Fuse::Drawing::ISurfaceDrawable_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface18));
    type->SetFields(126,
        ::g::Fuse::Navigation::Router_typeof(), offsetof(History, router), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Fuse::Visual_typeof(), NULL), offsetof(History, navigation_Active_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL), offsetof(History, trip_Color_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(uObject_typeof(), NULL), offsetof(History, temp_Items_inst), 0,
        ::g::Fuse::Controls::Panel_typeof(), offsetof(History, page1Tab), 0,
        ::g::Tab_typeof(), offsetof(History, trip), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(History, temp_eb15), 0,
        ::g::Fuse::Controls::PageControl_typeof(), offsetof(History, navigation), 0,
        ::g::Fuse::Controls::Page_typeof(), offsetof(History, page1), 0,
        ::g::Uno::UX::NameTable_typeof(), offsetof(History, __g_nametable1), 0,
        ::g::Uno::String_typeof()->Array(), (uintptr_t)&History::__g_static_nametable1_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&History::__selector0_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&History::__selector1_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&History::__selector2_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&History::__selector3_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&History::__selector4_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&History::__selector5_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&History::__selector6_, uFieldFlagsStatic);
    type->Reflection.SetFunctions(1,
        new uFunction(".ctor", NULL, (void*)History__New5_fn, 0, true, type, 1, ::g::Fuse::Navigation::Router_typeof()));
}

::g::Fuse::Controls::Panel_type* History_typeof()
{
    static uSStrong< ::g::Fuse::Controls::Panel_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Fuse::Controls::Page_typeof();
    options.FieldCount = 144;
    options.InterfaceCount = 19;
    options.DependencyCount = 1;
    options.ObjectSize = sizeof(History);
    options.TypeSize = sizeof(::g::Fuse::Controls::Panel_type);
    type = (::g::Fuse::Controls::Panel_type*)uClassType::New("History", options);
    type->fp_build_ = History_build;
    type->fp_cctor_ = History__cctor_4_fn;
    type->interface18.fp_Draw = (void(*)(uObject*, ::g::Fuse::Drawing::Surface*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableDraw_fn;
    type->interface18.fp_get_IsPrimary = (void(*)(uObject*, bool*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableget_IsPrimary_fn;
    type->interface18.fp_get_ElementSize = (void(*)(uObject*, ::g::Uno::Float2*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableget_ElementSize_fn;
    type->interface13.fp_Show = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsIShowShow_fn;
    type->interface15.fp_Collapse = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsICollapseCollapse_fn;
    type->interface14.fp_Hide = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsIHideHide_fn;
    type->interface17.fp_SetSize = (void(*)(uObject*, ::g::Uno::Float2*))::g::Fuse::Elements::Element__FuseAnimationsIResizeSetSize_fn;
    type->interface16.fp_get_ActualSize = (void(*)(uObject*, ::g::Uno::Float3*))::g::Fuse::Elements::Element__FuseIActualPlacementget_ActualSize_fn;
    type->interface16.fp_get_ActualPosition = (void(*)(uObject*, ::g::Uno::Float3*))::g::Fuse::Elements::Element__FuseIActualPlacementget_ActualPosition_fn;
    type->interface16.fp_add_Placed = (void(*)(uObject*, uDelegate*))::g::Fuse::Elements::Element__add_Placed_fn;
    type->interface16.fp_remove_Placed = (void(*)(uObject*, uDelegate*))::g::Fuse::Elements::Element__remove_Placed_fn;
    type->interface10.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Visual__UnoCollectionsIEnumerableFuseVisualGetEnumerator_fn;
    type->interface11.fp_Clear = (void(*)(uObject*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeClear_fn;
    type->interface11.fp_Contains = (void(*)(uObject*, void*, bool*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeContains_fn;
    type->interface7.fp_RemoveAt = (void(*)(uObject*, int32_t*))::g::Fuse::Visual__UnoCollectionsIListFuseNodeRemoveAt_fn;
    type->interface12.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Visual__UnoCollectionsIEnumerableFuseNodeGetEnumerator_fn;
    type->interface11.fp_get_Count = (void(*)(uObject*, int32_t*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeget_Count_fn;
    type->interface7.fp_get_Item = (void(*)(uObject*, int32_t*, uTRef))::g::Fuse::Visual__UnoCollectionsIListFuseNodeget_Item_fn;
    type->interface7.fp_Insert = (void(*)(uObject*, int32_t*, void*))::g::Fuse::Visual__Insert1_fn;
    type->interface8.fp_OnPropertyChanged = (void(*)(uObject*, ::g::Uno::UX::PropertyObject*, ::g::Uno::UX::Selector*))::g::Fuse::Controls::Control__OnPropertyChanged2_fn;
    type->interface9.fp_FindTemplate = (void(*)(uObject*, uString*, ::g::Uno::UX::Template**))::g::Fuse::Visual__FindTemplate_fn;
    type->interface11.fp_Add = (void(*)(uObject*, void*))::g::Fuse::Visual__Add1_fn;
    type->interface11.fp_Remove = (void(*)(uObject*, void*, bool*))::g::Fuse::Visual__Remove1_fn;
    type->interface5.fp_Clear = (void(*)(uObject*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingClear_fn;
    type->interface5.fp_Contains = (void(*)(uObject*, void*, bool*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingContains_fn;
    type->interface0.fp_RemoveAt = (void(*)(uObject*, int32_t*))::g::Fuse::Node__UnoCollectionsIListFuseBindingRemoveAt_fn;
    type->interface6.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Node__UnoCollectionsIEnumerableFuseBindingGetEnumerator_fn;
    type->interface1.fp_SetScriptObject = (void(*)(uObject*, uObject*, ::g::Fuse::Scripting::Context*))::g::Fuse::Node__FuseScriptingIScriptObjectSetScriptObject_fn;
    type->interface5.fp_get_Count = (void(*)(uObject*, int32_t*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingget_Count_fn;
    type->interface0.fp_get_Item = (void(*)(uObject*, int32_t*, uTRef))::g::Fuse::Node__UnoCollectionsIListFuseBindingget_Item_fn;
    type->interface1.fp_get_ScriptObject = (void(*)(uObject*, uObject**))::g::Fuse::Node__FuseScriptingIScriptObjectget_ScriptObject_fn;
    type->interface1.fp_get_ScriptContext = (void(*)(uObject*, ::g::Fuse::Scripting::Context**))::g::Fuse::Node__FuseScriptingIScriptObjectget_ScriptContext_fn;
    type->interface4.fp_get_SourceNearest = (void(*)(uObject*, uObject**))::g::Fuse::Node__FuseISourceLocationget_SourceNearest_fn;
    type->interface3.fp_add_Unrooted = (void(*)(uObject*, uDelegate*))::g::Fuse::Node__FuseINotifyUnrootedadd_Unrooted_fn;
    type->interface3.fp_remove_Unrooted = (void(*)(uObject*, uDelegate*))::g::Fuse::Node__FuseINotifyUnrootedremove_Unrooted_fn;
    type->interface0.fp_Insert = (void(*)(uObject*, int32_t*, void*))::g::Fuse::Node__Insert_fn;
    type->interface2.fp_get_Properties = (void(*)(uObject*, ::g::Fuse::Properties**))::g::Fuse::Node__get_Properties_fn;
    type->interface4.fp_get_SourceLineNumber = (void(*)(uObject*, int32_t*))::g::Fuse::Node__get_SourceLineNumber_fn;
    type->interface4.fp_get_SourceFileName = (void(*)(uObject*, uString**))::g::Fuse::Node__get_SourceFileName_fn;
    type->interface5.fp_Add = (void(*)(uObject*, void*))::g::Fuse::Node__Add_fn;
    type->interface5.fp_Remove = (void(*)(uObject*, void*, bool*))::g::Fuse::Node__Remove_fn;
    return type;
}

// public History(Fuse.Navigation.Router router) :135
void History__ctor_8_fn(History* __this, ::g::Fuse::Navigation::Router* router1)
{
    __this->ctor_8(router1);
}

// private void InitializeUX() :141
void History__InitializeUX_fn(History* __this)
{
    __this->InitializeUX();
}

// public History New(Fuse.Navigation.Router router) :135
void History__New5_fn(::g::Fuse::Navigation::Router* router1, History** __retval)
{
    *__retval = History::New5(router1);
}

uSStrong<uArray*> History::__g_static_nametable1_;
::g::Uno::UX::Selector History::__selector0_;
::g::Uno::UX::Selector History::__selector1_;
::g::Uno::UX::Selector History::__selector2_;
::g::Uno::UX::Selector History::__selector3_;
::g::Uno::UX::Selector History::__selector4_;
::g::Uno::UX::Selector History::__selector5_;
::g::Uno::UX::Selector History::__selector6_;

// public History(Fuse.Navigation.Router router) [instance] :135
void History::ctor_8(::g::Fuse::Navigation::Router* router1)
{
    uStackFrame __("History", ".ctor(Fuse.Navigation.Router)");
    ctor_7();
    router = router1;
    InitializeUX();
}

// private void InitializeUX() [instance] :141
void History::InitializeUX()
{
    uStackFrame __("History", "InitializeUX()");
    __g_nametable1 = ::g::Uno::UX::NameTable::New1(NULL, History::__g_static_nametable1_);
    navigation = ::g::Fuse::Controls::PageControl::New4();
    navigation_Active_inst = ::g::OSP_FuseControlsNavigationControl_Active_Property::New1(navigation, History::__selector0_);
    ::g::Fuse::Reactive::Data* temp1 = ::g::Fuse::Reactive::Data::New1(uString::Const("Loadtraining"));
    trip = ::g::Tab::New4();
    trip_Color_inst = ::g::OSP_FuseControlsPanel_Color_Property::New1(trip, History::__selector1_);
    ::g::Fuse::Reactive::Each* temp = ::g::Fuse::Reactive::Each::New4();
    temp_Items_inst = ::g::OSP_FuseReactiveEach_Items_Property::New1(temp, History::__selector2_);
    ::g::Fuse::Reactive::Data* temp2 = ::g::Fuse::Reactive::Data::New1(uString::Const("history"));
    ::g::Fuse::Reactive::JavaScript* temp3 = ::g::Fuse::Reactive::JavaScript::New2(__g_nametable1);
    ::g::Fuse::Reactive::JavaScript* temp4 = ::g::Fuse::Reactive::JavaScript::New2(__g_nametable1);
    ::g::Fuse::Controls::DockPanel* temp5 = ::g::Fuse::Controls::DockPanel::New4();
    ::g::TopBar* temp6 = ::g::TopBar::New5();
    ::g::Fuse::Controls::DockPanel* temp7 = ::g::Fuse::Controls::DockPanel::New4();
    ::g::Fuse::Controls::Grid* temp8 = ::g::Fuse::Controls::Grid::New4();
    page1Tab = ::g::Fuse::Controls::Panel::New3();
    ::g::Fuse::Gestures::Clicked* temp9 = ::g::Fuse::Gestures::Clicked::New2();
    ::g::Fuse::Triggers::Actions::Set* temp10 = (::g::Fuse::Triggers::Actions::Set*)::g::Fuse::Triggers::Actions::Set::New2(::g::Fuse::Triggers::Actions::Set_typeof()->MakeType(::g::Fuse::Visual_typeof(), NULL), navigation_Active_inst);
    ::g::Fuse::Triggers::Actions::Callback* temp11 = ::g::Fuse::Triggers::Actions::Callback::New2();
    temp_eb15 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp1);
    ::g::Fuse::Drawing::StaticSolidColor* temp12 = ::g::Fuse::Drawing::StaticSolidColor::New2(::g::Uno::Float4__New2(0.9686275f, 0.8980392f, 0.3686275f, 1.0f));
    page1 = ::g::Fuse::Controls::Page::New4();
    ::g::Fuse::Navigation::WhileActive* temp13 = ::g::Fuse::Navigation::WhileActive::New2();
    ::g::Fuse::Animations::Change* temp14 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL), trip_Color_inst);
    ::g::Fuse::Controls::ScrollView* temp15 = ::g::Fuse::Controls::ScrollView::New4();
    ::g::Fuse::Controls::StackPanel* temp16 = ::g::Fuse::Controls::StackPanel::New4();
    History__Template* temp17 = History__Template::New2(this, this);
    ::g::Fuse::Reactive::DataBinding* temp18 = ::g::Fuse::Reactive::DataBinding::New1(temp_Items_inst, (uObject*)temp2, 3);
    ::g::Fuse::Drawing::StaticSolidColor* temp19 = ::g::Fuse::Drawing::StaticSolidColor::New2(::g::Uno::Float4__New2(1.0f, 1.0f, 1.0f, 1.0f));
    Color(::g::Uno::Float4__New2(1.0f, 1.0f, 1.0f, 1.0f));
    SourceLineNumber(1);
    SourceFileName(uString::Const("history.ux"));
    temp3->LineNumber(3);
    temp3->FileName(uString::Const("history.ux"));
    temp3->SourceLineNumber(3);
    temp3->SourceFileName(uString::Const("history.ux"));
    temp3->File(::g::Uno::UX::BundleFileSource::New1(::g::OSP_bundle::navigationb176862e()));
    temp4->Code(uString::Const("\n"
        "\t\tvar API = require('/js_files/api.js');\n"
        "\t\tvar Observable = require('FuseJS/Observable');\n"
        "\t\tvar permit = Observable();\n"
        "\t\tvar history = Observable();\n"
        "\t\tvar medical = Observable();\n"
        "\t\tvar Storage = require(\"FuseJS/Storage\");\n"
        "\t\tdataPath = API.readOSPID();\n"
        "\n"
        "\t\tvar error = Observable(false);\n"
        "\t\tvar Msg = Observable(\"There was an error\");\n"
        "\n"
        "\t\tfunction Loadtraining() {\n"
        "\t\t\terror.value = false;\n"
        "\t\t\t//busy1.activate();\n"
        "\t\t\tStorage.read(dataPath).then(function(content) {\n"
        "\t\t\t    var cont = JSON.parse(content);\n"
        "\t\t\t    personnel_id = cont.personnel[0].personnel_id;\n"
        "\t\t\t\t\tfetch('http://41.75.207.60/osp/public/api/history/'+personnel_id )\n"
        "\t\t\t\t.then(function(response) { \n"
        "\t\t\t\tstatus = response.status;  // Get the HTTP status code\n"
        "    \t\t\tresponse_ok = response.ok; // Is response.status in the 200-range?\n"
        "\t\t\t\treturn response.json(); \n"
        "\t\t\t})\n"
        "\t\t\t.then(function (responseObject) {\n"
        "\t\t\t\tconsole.dir(responseObject)\n"
        "\t\t\t\thistory.replaceAll(responseObject);\n"
        "\t\t\t\tconsole.log(\"it works: \"+JSON.stringify(responseObject));\n"
        "\t\t\t\t// console.log(error.value);\n"
        "\t\t\t\t //busy1.deactivate();\n"
        "\t\t\t}).catch(function(err) {\n"
        "\t\t\t\tconsole.log(\"There was an error\");\n"
        "\t\t\t\t // busy.deactivate();\n"
        "\t\t\t\t if (status == 502) {\n"
        "\t\t\t\t\t// console.log(\"Error Address\");\n"
        "\t\t\t\t\tMsg.value = \"Wrong Address\";\n"
        "\t\t\t\t}else{\n"
        "\t\t\t\t\t// console.log(JSON.stringify(responseObject));\n"
        "\t\t\t\t\t// console.log(\"Record was not found\");\n"
        "\t\t\t\t\tMsg.value = \"User has no trips record\";\n"
        "\t\t\t\t}\n"
        "\t\t\t\terror.value = true;\n"
        "\t\t\t});\n"
        "\t\t\t    \n"
        "\t\t\t    \n"
        "\t\t\t    \n"
        "\n"
        "\t\t\t}, function(error) {\n"
        "\t\t\t    //For now, let's expect the error to be because of the file not being found.\n"
        "\t\t\t    //welcomeText.value = \"There is currently no local data stored\";\n"
        "\t\t\t});\n"
        "\n"
        "\t\t\t\n"
        "\t\t\t\n"
        "\t\t\t\n"
        "\t\t}\n"
        "\n"
        "\t\t\n"
        "\n"
        "\t\t\n"
        "\t\tmodule.exports={\n"
        "\t\t\tpermit: permit,\n"
        "\t\t\thistory: history,\n"
        "\t\t\tmedical: medical,\n"
        "\t\t\t\n"
        "\t\t\tLoadtraining: Loadtraining,\n"
        "\t\t\t\n"
        "\t\t\terror: error,\n"
        "\t\t\tMsg: Msg\n"
        "\t\t};\n"
        "\t"));
    temp4->LineNumber(4);
    temp4->FileName(uString::Const("history.ux"));
    temp4->SourceLineNumber(4);
    temp4->SourceFileName(uString::Const("history.ux"));
    temp5->SourceLineNumber(93);
    temp5->SourceFileName(uString::Const("history.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp5->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp6);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp5->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp7);
    temp6->Title(uString::Const("HISTORY"));
    temp6->SourceLineNumber(95);
    temp6->SourceFileName(uString::Const("history.ux"));
    ::g::Fuse::Controls::DockPanel::SetDock(temp6, 2);
    temp7->Padding(::g::Uno::Float4__New2(0.0f, 20.0f, 0.0f, 0.0f));
    temp7->SourceLineNumber(97);
    temp7->SourceFileName(uString::Const("history.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp7->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp8);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp7->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), navigation);
    temp8->ColumnCount(3);
    temp8->Width(::g::Uno::UX::Size__New1(90.0f, 4));
    temp8->Height(::g::Uno::UX::Size__New1(40.0f, 1));
    temp8->SourceLineNumber(98);
    temp8->SourceFileName(uString::Const("history.ux"));
    ::g::Fuse::Controls::DockPanel::SetDock(temp8, 2);
    temp8->Background(temp12);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp8->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), page1Tab);
    uPtr(page1Tab)->Name(History::__selector3_);
    uPtr(page1Tab)->SourceLineNumber(99);
    uPtr(page1Tab)->SourceFileName(uString::Const("history.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(page1Tab)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), trip);
    uPtr(trip)->Text(uString::Const("HISTORY"));
    uPtr(trip)->Color(::g::Uno::Float4__New2(0.1137255f, 0.4901961f, 0.2588235f, 1.0f));
    uPtr(trip)->Name(History::__selector4_);
    uPtr(trip)->SourceLineNumber(100);
    uPtr(trip)->SourceFileName(uString::Const("history.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(trip)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp9);
    temp9->SourceLineNumber(101);
    temp9->SourceFileName(uString::Const("history.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp9->Actions()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Triggers::Actions::TriggerAction_typeof(), NULL)), temp10);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp9->Actions()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Triggers::Actions::TriggerAction_typeof(), NULL)), temp11);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp9->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp_eb15);
    ::g::Fuse::Triggers::Actions::Set__set_Value_fn(temp10, page1);
    temp10->SourceLineNumber(102);
    temp10->SourceFileName(uString::Const("history.ux"));
    temp11->SourceLineNumber(103);
    temp11->SourceFileName(uString::Const("history.ux"));
    temp11->add_Handler(uDelegate::New(::g::Fuse::VisualEventHandler_typeof(), (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb15)));
    temp1->SourceLineNumber(103);
    temp1->SourceFileName(uString::Const("history.ux"));
    uPtr(navigation)->Name(History::__selector5_);
    uPtr(navigation)->SourceLineNumber(110);
    uPtr(navigation)->SourceFileName(uString::Const("history.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(navigation)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), page1);
    uPtr(page1)->Name(History::__selector6_);
    uPtr(page1)->SourceLineNumber(111);
    uPtr(page1)->SourceFileName(uString::Const("history.ux"));
    uPtr(page1)->Background(temp19);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(page1)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp13);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(page1)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp15);
    temp13->Threshold(0.5f);
    temp13->SourceLineNumber(113);
    temp13->SourceFileName(uString::Const("history.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp13->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp14);
    ::g::Fuse::Animations::Change__set_Value_fn(temp14, uCRef(::g::Uno::Float4__New2(0.9686275f, 0.8980392f, 0.3686275f, 1.0f)));
    temp15->SourceLineNumber(116);
    temp15->SourceFileName(uString::Const("history.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp15->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp16);
    temp16->Width(::g::Uno::UX::Size__New1(90.0f, 4));
    temp16->Margin(::g::Uno::Float4__New2(0.0f, 10.0f, 0.0f, 0.0f));
    temp16->SourceLineNumber(117);
    temp16->SourceFileName(uString::Const("history.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp16->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp);
    temp->SourceLineNumber(118);
    temp->SourceFileName(uString::Const("history.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp17);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp18);
    temp2->SourceLineNumber(118);
    temp2->SourceFileName(uString::Const("history.ux"));
    uPtr(__g_nametable1)->This(this);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), router);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), page1Tab);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), trip);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), temp_eb15);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), navigation);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), page1);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp3);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp4);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp5);
}

// public History New(Fuse.Navigation.Router router) [static] :135
History* History::New5(::g::Fuse::Navigation::Router* router1)
{
    History* obj1 = (History*)uNew(History_typeof());
    obj1->ctor_8(router1);
    return obj1;
}
// }

} // ::g
