// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/Notification.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.Notification.h>
#include <_root.Notification.Template.h>
#include <_root.TaskItem.h>
#include <Fuse.Node.h>
#include <Uno.Bool.h>
#include <Uno.Int.h>
#include <Uno.Object.h>
#include <Uno.String.h>
static uString* STRINGS[1];

namespace g{

// public partial sealed class Notification.Template :6
// {
// static Template() :15
static void Notification__Template__cctor__fn(uType* __type)
{
}

static void Notification__Template_build(uType* type)
{
    ::STRINGS[0] = uString::Const("notification.ux");
    type->SetFields(2,
        ::g::Notification_typeof(), offsetof(Notification__Template, __parent1), uFieldFlagsWeak,
        ::g::Notification_typeof(), offsetof(Notification__Template, __parentInstance1), uFieldFlagsWeak);
    type->Reflection.SetFunctions(1,
        new uFunction(".ctor", NULL, (void*)Notification__Template__New2_fn, 0, true, type, 2, ::g::Notification_typeof(), ::g::Notification_typeof()));
}

::g::Uno::UX::Template_type* Notification__Template_typeof()
{
    static uSStrong< ::g::Uno::UX::Template_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::Template_typeof();
    options.FieldCount = 4;
    options.ObjectSize = sizeof(Notification__Template);
    options.TypeSize = sizeof(::g::Uno::UX::Template_type);
    type = (::g::Uno::UX::Template_type*)uClassType::New("Notification.Template", options);
    type->fp_build_ = Notification__Template_build;
    type->fp_cctor_ = Notification__Template__cctor__fn;
    type->fp_New1 = (void(*)(::g::Uno::UX::Template*, uObject**))Notification__Template__New1_fn;
    return type;
}

// public Template(Notification parent, Notification parentInstance) :10
void Notification__Template__ctor_1_fn(Notification__Template* __this, ::g::Notification* parent, ::g::Notification* parentInstance)
{
    __this->ctor_1(parent, parentInstance);
}

// public override sealed object New() :18
void Notification__Template__New1_fn(Notification__Template* __this, uObject** __retval)
{
    ::g::TaskItem* __self1 = ::g::TaskItem::New5();
    __self1->SourceLineNumber(55);
    __self1->SourceFileName(::STRINGS[0/*"notificatio...*/]);
    return *__retval = __self1, void();
}

// public Template New(Notification parent, Notification parentInstance) :10
void Notification__Template__New2_fn(::g::Notification* parent, ::g::Notification* parentInstance, Notification__Template** __retval)
{
    *__retval = Notification__Template::New2(parent, parentInstance);
}

// public Template(Notification parent, Notification parentInstance) [instance] :10
void Notification__Template::ctor_1(::g::Notification* parent, ::g::Notification* parentInstance)
{
    ctor_(NULL, false);
    __parent1 = parent;
    __parentInstance1 = parentInstance;
}

// public Template New(Notification parent, Notification parentInstance) [static] :10
Notification__Template* Notification__Template::New2(::g::Notification* parent, ::g::Notification* parentInstance)
{
    Notification__Template* obj1 = (Notification__Template*)uNew(Notification__Template_typeof());
    obj1->ctor_1(parent, parentInstance);
    return obj1;
}
// }

} // ::g
