// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.DropdownSelectedItem.h>
#include <_root.OSP_accessor_Dr-d429fb4c.h>
#include <Uno.Bool.h>
#include <Uno.Float.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.Type.h>
#include <Uno.UX.IPropertyListener.h>
#include <Uno.UX.PropertyObject.h>
#include <Uno.UX.Selector.h>
static uString* STRINGS[1];
static uType* TYPES[2];

namespace g{

// internal sealed class OSP_accessor_DropdownSelectedItem_FontSize :71
// {
// static generated OSP_accessor_DropdownSelectedItem_FontSize() :71
static void OSP_accessor_DropdownSelectedItem_FontSize__cctor__fn(uType* __type)
{
    OSP_accessor_DropdownSelectedItem_FontSize::Singleton_ = OSP_accessor_DropdownSelectedItem_FontSize::New1();
    OSP_accessor_DropdownSelectedItem_FontSize::_name_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[0/*"FontSize"*/]);
}

static void OSP_accessor_DropdownSelectedItem_FontSize_build(uType* type)
{
    ::STRINGS[0] = uString::Const("FontSize");
    ::TYPES[0] = ::g::DropdownSelectedItem_typeof();
    ::TYPES[1] = ::g::Uno::Type_typeof();
    type->SetFields(0,
        ::g::Uno::UX::PropertyAccessor_typeof(), (uintptr_t)&OSP_accessor_DropdownSelectedItem_FontSize::Singleton_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&OSP_accessor_DropdownSelectedItem_FontSize::_name_, uFieldFlagsStatic);
}

::g::Uno::UX::PropertyAccessor_type* OSP_accessor_DropdownSelectedItem_FontSize_typeof()
{
    static uSStrong< ::g::Uno::UX::PropertyAccessor_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::PropertyAccessor_typeof();
    options.FieldCount = 2;
    options.ObjectSize = sizeof(OSP_accessor_DropdownSelectedItem_FontSize);
    options.TypeSize = sizeof(::g::Uno::UX::PropertyAccessor_type);
    type = (::g::Uno::UX::PropertyAccessor_type*)uClassType::New("OSP_accessor_DropdownSelectedItem_FontSize", options);
    type->fp_build_ = OSP_accessor_DropdownSelectedItem_FontSize_build;
    type->fp_ctor_ = (void*)OSP_accessor_DropdownSelectedItem_FontSize__New1_fn;
    type->fp_cctor_ = OSP_accessor_DropdownSelectedItem_FontSize__cctor__fn;
    type->fp_GetAsObject = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::PropertyObject*, uObject**))OSP_accessor_DropdownSelectedItem_FontSize__GetAsObject_fn;
    type->fp_get_Name = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::Selector*))OSP_accessor_DropdownSelectedItem_FontSize__get_Name_fn;
    type->fp_get_PropertyType = (void(*)(::g::Uno::UX::PropertyAccessor*, uType**))OSP_accessor_DropdownSelectedItem_FontSize__get_PropertyType_fn;
    type->fp_SetAsObject = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::PropertyObject*, uObject*, uObject*))OSP_accessor_DropdownSelectedItem_FontSize__SetAsObject_fn;
    type->fp_get_SupportsOriginSetter = (void(*)(::g::Uno::UX::PropertyAccessor*, bool*))OSP_accessor_DropdownSelectedItem_FontSize__get_SupportsOriginSetter_fn;
    return type;
}

// public generated OSP_accessor_DropdownSelectedItem_FontSize() :71
void OSP_accessor_DropdownSelectedItem_FontSize__ctor_1_fn(OSP_accessor_DropdownSelectedItem_FontSize* __this)
{
    __this->ctor_1();
}

// public override sealed object GetAsObject(Uno.UX.PropertyObject obj) :77
void OSP_accessor_DropdownSelectedItem_FontSize__GetAsObject_fn(OSP_accessor_DropdownSelectedItem_FontSize* __this, ::g::Uno::UX::PropertyObject* obj, uObject** __retval)
{
    uStackFrame __("OSP_accessor_DropdownSelectedItem_FontSize", "GetAsObject(Uno.UX.PropertyObject)");
    return *__retval = uBox(::g::Uno::Float_typeof(), uPtr(uCast< ::g::DropdownSelectedItem*>(obj, ::TYPES[0/*DropdownSelectedItem*/]))->FontSize()), void();
}

// public override sealed Uno.UX.Selector get_Name() :74
void OSP_accessor_DropdownSelectedItem_FontSize__get_Name_fn(OSP_accessor_DropdownSelectedItem_FontSize* __this, ::g::Uno::UX::Selector* __retval)
{
    return *__retval = OSP_accessor_DropdownSelectedItem_FontSize::_name_, void();
}

// public generated OSP_accessor_DropdownSelectedItem_FontSize New() :71
void OSP_accessor_DropdownSelectedItem_FontSize__New1_fn(OSP_accessor_DropdownSelectedItem_FontSize** __retval)
{
    *__retval = OSP_accessor_DropdownSelectedItem_FontSize::New1();
}

// public override sealed Uno.Type get_PropertyType() :76
void OSP_accessor_DropdownSelectedItem_FontSize__get_PropertyType_fn(OSP_accessor_DropdownSelectedItem_FontSize* __this, uType** __retval)
{
    return *__retval = ::g::Uno::Float_typeof(), void();
}

// public override sealed void SetAsObject(Uno.UX.PropertyObject obj, object v, Uno.UX.IPropertyListener origin) :78
void OSP_accessor_DropdownSelectedItem_FontSize__SetAsObject_fn(OSP_accessor_DropdownSelectedItem_FontSize* __this, ::g::Uno::UX::PropertyObject* obj, uObject* v, uObject* origin)
{
    uStackFrame __("OSP_accessor_DropdownSelectedItem_FontSize", "SetAsObject(Uno.UX.PropertyObject,object,Uno.UX.IPropertyListener)");
    uPtr(uCast< ::g::DropdownSelectedItem*>(obj, ::TYPES[0/*DropdownSelectedItem*/]))->SetFontSize(uUnbox<float>(::g::Uno::Float_typeof(), v), origin);
}

// public override sealed bool get_SupportsOriginSetter() :79
void OSP_accessor_DropdownSelectedItem_FontSize__get_SupportsOriginSetter_fn(OSP_accessor_DropdownSelectedItem_FontSize* __this, bool* __retval)
{
    return *__retval = true, void();
}

uSStrong< ::g::Uno::UX::PropertyAccessor*> OSP_accessor_DropdownSelectedItem_FontSize::Singleton_;
::g::Uno::UX::Selector OSP_accessor_DropdownSelectedItem_FontSize::_name_;

// public generated OSP_accessor_DropdownSelectedItem_FontSize() [instance] :71
void OSP_accessor_DropdownSelectedItem_FontSize::ctor_1()
{
    ctor_();
}

// public generated OSP_accessor_DropdownSelectedItem_FontSize New() [static] :71
OSP_accessor_DropdownSelectedItem_FontSize* OSP_accessor_DropdownSelectedItem_FontSize::New1()
{
    OSP_accessor_DropdownSelectedItem_FontSize* obj1 = (OSP_accessor_DropdownSelectedItem_FontSize*)uNew(OSP_accessor_DropdownSelectedItem_FontSize_typeof());
    obj1->ctor_1();
    return obj1;
}
// }

} // ::g
