// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.OSP_SortableLis-75b032ef.h>
#include <_root.SortableList.h>
#include <Uno.Bool.h>
#include <Uno.UX.IPropertyListener.h>
#include <Uno.UX.PropertyObject.h>
#include <Uno.UX.Selector.h>
static uType* TYPES[1];

namespace g{

// internal sealed class OSP_SortableList_Items_Property :452
// {
static void OSP_SortableList_Items_Property_build(uType* type)
{
    ::TYPES[0] = ::g::SortableList_typeof();
    type->SetBase(::g::Uno::UX::Property1_typeof()->MakeType(uObject_typeof(), NULL));
    type->SetFields(1,
        ::TYPES[0/*SortableList*/], offsetof(OSP_SortableList_Items_Property, _obj), uFieldFlagsWeak);
}

::g::Uno::UX::Property1_type* OSP_SortableList_Items_Property_typeof()
{
    static uSStrong< ::g::Uno::UX::Property1_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::Property1_typeof();
    options.FieldCount = 2;
    options.ObjectSize = sizeof(OSP_SortableList_Items_Property);
    options.TypeSize = sizeof(::g::Uno::UX::Property1_type);
    type = (::g::Uno::UX::Property1_type*)uClassType::New("OSP_SortableList_Items_Property", options);
    type->fp_build_ = OSP_SortableList_Items_Property_build;
    type->fp_Get1 = (void(*)(::g::Uno::UX::Property1*, ::g::Uno::UX::PropertyObject*, uTRef))OSP_SortableList_Items_Property__Get1_fn;
    type->fp_get_Object = (void(*)(::g::Uno::UX::Property*, ::g::Uno::UX::PropertyObject**))OSP_SortableList_Items_Property__get_Object_fn;
    type->fp_Set1 = (void(*)(::g::Uno::UX::Property1*, ::g::Uno::UX::PropertyObject*, void*, uObject*))OSP_SortableList_Items_Property__Set1_fn;
    type->fp_get_SupportsOriginSetter = (void(*)(::g::Uno::UX::PropertyAccessor*, bool*))OSP_SortableList_Items_Property__get_SupportsOriginSetter_fn;
    return type;
}

// public OSP_SortableList_Items_Property(SortableList obj, Uno.UX.Selector name) :455
void OSP_SortableList_Items_Property__ctor_3_fn(OSP_SortableList_Items_Property* __this, ::g::SortableList* obj, ::g::Uno::UX::Selector* name)
{
    __this->ctor_3(obj, *name);
}

// public override sealed object Get(Uno.UX.PropertyObject obj) :457
void OSP_SortableList_Items_Property__Get1_fn(OSP_SortableList_Items_Property* __this, ::g::Uno::UX::PropertyObject* obj, uObject** __retval)
{
    uStackFrame __("OSP_SortableList_Items_Property", "Get(Uno.UX.PropertyObject)");
    return *__retval = uPtr(uCast< ::g::SortableList*>(obj, ::TYPES[0/*SortableList*/]))->Items(), void();
}

// public OSP_SortableList_Items_Property New(SortableList obj, Uno.UX.Selector name) :455
void OSP_SortableList_Items_Property__New1_fn(::g::SortableList* obj, ::g::Uno::UX::Selector* name, OSP_SortableList_Items_Property** __retval)
{
    *__retval = OSP_SortableList_Items_Property::New1(obj, *name);
}

// public override sealed Uno.UX.PropertyObject get_Object() :456
void OSP_SortableList_Items_Property__get_Object_fn(OSP_SortableList_Items_Property* __this, ::g::Uno::UX::PropertyObject** __retval)
{
    return *__retval = __this->_obj, void();
}

// public override sealed void Set(Uno.UX.PropertyObject obj, object v, Uno.UX.IPropertyListener origin) :458
void OSP_SortableList_Items_Property__Set1_fn(OSP_SortableList_Items_Property* __this, ::g::Uno::UX::PropertyObject* obj, uObject* v, uObject* origin)
{
    uStackFrame __("OSP_SortableList_Items_Property", "Set(Uno.UX.PropertyObject,object,Uno.UX.IPropertyListener)");
    uPtr(uCast< ::g::SortableList*>(obj, ::TYPES[0/*SortableList*/]))->SetItems(v, origin);
}

// public override sealed bool get_SupportsOriginSetter() :459
void OSP_SortableList_Items_Property__get_SupportsOriginSetter_fn(OSP_SortableList_Items_Property* __this, bool* __retval)
{
    return *__retval = true, void();
}

// public OSP_SortableList_Items_Property(SortableList obj, Uno.UX.Selector name) [instance] :455
void OSP_SortableList_Items_Property::ctor_3(::g::SortableList* obj, ::g::Uno::UX::Selector name)
{
    ctor_2(name);
    _obj = obj;
}

// public OSP_SortableList_Items_Property New(SortableList obj, Uno.UX.Selector name) [static] :455
OSP_SortableList_Items_Property* OSP_SortableList_Items_Property::New1(::g::SortableList* obj, ::g::Uno::UX::Selector name)
{
    OSP_SortableList_Items_Property* obj1 = (OSP_SortableList_Items_Property*)uNew(OSP_SortableList_Items_Property_typeof());
    obj1->ctor_3(obj, name);
    return obj1;
}
// }

} // ::g
