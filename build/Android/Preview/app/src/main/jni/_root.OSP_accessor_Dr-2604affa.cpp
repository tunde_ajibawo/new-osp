// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.DropdownSelectedItem.h>
#include <_root.OSP_accessor_Dr-2604affa.h>
#include <Uno.Bool.h>
#include <Uno.Float4.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.Type.h>
#include <Uno.UX.IPropertyListener.h>
#include <Uno.UX.PropertyObject.h>
#include <Uno.UX.Selector.h>
static uString* STRINGS[1];
static uType* TYPES[2];

namespace g{

// internal sealed class OSP_accessor_DropdownSelectedItem_TextColor :81
// {
// static generated OSP_accessor_DropdownSelectedItem_TextColor() :81
static void OSP_accessor_DropdownSelectedItem_TextColor__cctor__fn(uType* __type)
{
    OSP_accessor_DropdownSelectedItem_TextColor::Singleton_ = OSP_accessor_DropdownSelectedItem_TextColor::New1();
    OSP_accessor_DropdownSelectedItem_TextColor::_name_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[0/*"TextColor"*/]);
}

static void OSP_accessor_DropdownSelectedItem_TextColor_build(uType* type)
{
    ::STRINGS[0] = uString::Const("TextColor");
    ::TYPES[0] = ::g::DropdownSelectedItem_typeof();
    ::TYPES[1] = ::g::Uno::Type_typeof();
    type->SetFields(0,
        ::g::Uno::UX::PropertyAccessor_typeof(), (uintptr_t)&OSP_accessor_DropdownSelectedItem_TextColor::Singleton_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&OSP_accessor_DropdownSelectedItem_TextColor::_name_, uFieldFlagsStatic);
}

::g::Uno::UX::PropertyAccessor_type* OSP_accessor_DropdownSelectedItem_TextColor_typeof()
{
    static uSStrong< ::g::Uno::UX::PropertyAccessor_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::PropertyAccessor_typeof();
    options.FieldCount = 2;
    options.ObjectSize = sizeof(OSP_accessor_DropdownSelectedItem_TextColor);
    options.TypeSize = sizeof(::g::Uno::UX::PropertyAccessor_type);
    type = (::g::Uno::UX::PropertyAccessor_type*)uClassType::New("OSP_accessor_DropdownSelectedItem_TextColor", options);
    type->fp_build_ = OSP_accessor_DropdownSelectedItem_TextColor_build;
    type->fp_ctor_ = (void*)OSP_accessor_DropdownSelectedItem_TextColor__New1_fn;
    type->fp_cctor_ = OSP_accessor_DropdownSelectedItem_TextColor__cctor__fn;
    type->fp_GetAsObject = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::PropertyObject*, uObject**))OSP_accessor_DropdownSelectedItem_TextColor__GetAsObject_fn;
    type->fp_get_Name = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::Selector*))OSP_accessor_DropdownSelectedItem_TextColor__get_Name_fn;
    type->fp_get_PropertyType = (void(*)(::g::Uno::UX::PropertyAccessor*, uType**))OSP_accessor_DropdownSelectedItem_TextColor__get_PropertyType_fn;
    type->fp_SetAsObject = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::PropertyObject*, uObject*, uObject*))OSP_accessor_DropdownSelectedItem_TextColor__SetAsObject_fn;
    type->fp_get_SupportsOriginSetter = (void(*)(::g::Uno::UX::PropertyAccessor*, bool*))OSP_accessor_DropdownSelectedItem_TextColor__get_SupportsOriginSetter_fn;
    return type;
}

// public generated OSP_accessor_DropdownSelectedItem_TextColor() :81
void OSP_accessor_DropdownSelectedItem_TextColor__ctor_1_fn(OSP_accessor_DropdownSelectedItem_TextColor* __this)
{
    __this->ctor_1();
}

// public override sealed object GetAsObject(Uno.UX.PropertyObject obj) :87
void OSP_accessor_DropdownSelectedItem_TextColor__GetAsObject_fn(OSP_accessor_DropdownSelectedItem_TextColor* __this, ::g::Uno::UX::PropertyObject* obj, uObject** __retval)
{
    uStackFrame __("OSP_accessor_DropdownSelectedItem_TextColor", "GetAsObject(Uno.UX.PropertyObject)");
    return *__retval = uBox(::g::Uno::Float4_typeof(), uPtr(uCast< ::g::DropdownSelectedItem*>(obj, ::TYPES[0/*DropdownSelectedItem*/]))->TextColor()), void();
}

// public override sealed Uno.UX.Selector get_Name() :84
void OSP_accessor_DropdownSelectedItem_TextColor__get_Name_fn(OSP_accessor_DropdownSelectedItem_TextColor* __this, ::g::Uno::UX::Selector* __retval)
{
    return *__retval = OSP_accessor_DropdownSelectedItem_TextColor::_name_, void();
}

// public generated OSP_accessor_DropdownSelectedItem_TextColor New() :81
void OSP_accessor_DropdownSelectedItem_TextColor__New1_fn(OSP_accessor_DropdownSelectedItem_TextColor** __retval)
{
    *__retval = OSP_accessor_DropdownSelectedItem_TextColor::New1();
}

// public override sealed Uno.Type get_PropertyType() :86
void OSP_accessor_DropdownSelectedItem_TextColor__get_PropertyType_fn(OSP_accessor_DropdownSelectedItem_TextColor* __this, uType** __retval)
{
    return *__retval = ::g::Uno::Float4_typeof(), void();
}

// public override sealed void SetAsObject(Uno.UX.PropertyObject obj, object v, Uno.UX.IPropertyListener origin) :88
void OSP_accessor_DropdownSelectedItem_TextColor__SetAsObject_fn(OSP_accessor_DropdownSelectedItem_TextColor* __this, ::g::Uno::UX::PropertyObject* obj, uObject* v, uObject* origin)
{
    uStackFrame __("OSP_accessor_DropdownSelectedItem_TextColor", "SetAsObject(Uno.UX.PropertyObject,object,Uno.UX.IPropertyListener)");
    uPtr(uCast< ::g::DropdownSelectedItem*>(obj, ::TYPES[0/*DropdownSelectedItem*/]))->SetTextColor(uUnbox< ::g::Uno::Float4>(::g::Uno::Float4_typeof(), v), origin);
}

// public override sealed bool get_SupportsOriginSetter() :89
void OSP_accessor_DropdownSelectedItem_TextColor__get_SupportsOriginSetter_fn(OSP_accessor_DropdownSelectedItem_TextColor* __this, bool* __retval)
{
    return *__retval = true, void();
}

uSStrong< ::g::Uno::UX::PropertyAccessor*> OSP_accessor_DropdownSelectedItem_TextColor::Singleton_;
::g::Uno::UX::Selector OSP_accessor_DropdownSelectedItem_TextColor::_name_;

// public generated OSP_accessor_DropdownSelectedItem_TextColor() [instance] :81
void OSP_accessor_DropdownSelectedItem_TextColor::ctor_1()
{
    ctor_();
}

// public generated OSP_accessor_DropdownSelectedItem_TextColor New() [static] :81
OSP_accessor_DropdownSelectedItem_TextColor* OSP_accessor_DropdownSelectedItem_TextColor::New1()
{
    OSP_accessor_DropdownSelectedItem_TextColor* obj1 = (OSP_accessor_DropdownSelectedItem_TextColor*)uNew(OSP_accessor_DropdownSelectedItem_TextColor_typeof());
    obj1->ctor_1();
    return obj1;
}
// }

} // ::g
