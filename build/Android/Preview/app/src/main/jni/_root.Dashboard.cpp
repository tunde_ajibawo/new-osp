// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/Dashboard.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.Dashboard.h>
#include <_root.OSP_bundle.h>
#include <_root.OSP_FuseElement-78dfea6e.h>
#include <_root.OSP_FuseElement-82fc869f.h>
#include <_root.OSP_FuseRotatio-6a5246a5.h>
#include <_root.OSP_FuseTransla-34b0f5a4.h>
#include <_root.OSP_FuseTransla-34c51e25.h>
#include <_root.OSP_FuseTrigger-1375289b.h>
#include <_root.OSP_SortableLis-75b032ef.h>
#include <_root.Sidebar.h>
#include <_root.SortableList.h>
#include <_root.SquareButton.h>
#include <Fuse.Animations.Animator.h>
#include <Fuse.Animations.Change-1.h>
#include <Fuse.Animations.Easing.h>
#include <Fuse.Animations.TrackAnimator.h>
#include <Fuse.Controls.Control.h>
#include <Fuse.Controls.DockPanel.h>
#include <Fuse.Controls.EdgeNavigator.h>
#include <Fuse.Controls.Grid.h>
#include <Fuse.Controls.Panel.h>
#include <Fuse.Controls.Rectangle.h>
#include <Fuse.Controls.ScrollView.h>
#include <Fuse.Controls.Shape.h>
#include <Fuse.Controls.StackPanel.h>
#include <Fuse.Controls.Text.h>
#include <Fuse.Controls.TextAlignment.h>
#include <Fuse.Controls.TextControl.h>
#include <Fuse.Drawing.Brush.h>
#include <Fuse.Drawing.StaticSolidColor.h>
#include <Fuse.Elements.Alignment.h>
#include <Fuse.Elements.Element.h>
#include <Fuse.Elements.HitTestMode.h>
#include <Fuse.Font.h>
#include <Fuse.Gestures.Clicked.h>
#include <Fuse.Gestures.ClickedHandler.h>
#include <Fuse.Layouts.Dock.h>
#include <Fuse.Navigation.Activ-5bc1c951.h>
#include <Fuse.Navigation.EdgeNavigation.h>
#include <Fuse.Navigation.NavigateTo.h>
#include <Fuse.Navigation.NavigationEdge.h>
#include <Fuse.Navigation.Router.h>
#include <Fuse.Navigation.WhileActive.h>
#include <Fuse.Navigation.WhileInactive.h>
#include <Fuse.NodeGroupBase.h>
#include <Fuse.Reactive.BindingMode.h>
#include <Fuse.Reactive.Data.h>
#include <Fuse.Reactive.DataBinding.h>
#include <Fuse.Reactive.EventBinding.h>
#include <Fuse.Reactive.Expression.h>
#include <Fuse.Reactive.IExpression.h>
#include <Fuse.Reactive.JavaScript.h>
#include <Fuse.Rotation.h>
#include <Fuse.Translation.h>
#include <Fuse.Triggers.Actions.Callback.h>
#include <Fuse.Triggers.Actions-fcab7e57.h>
#include <Fuse.Triggers.Trigger.h>
#include <Fuse.Triggers.WhileBool.h>
#include <Fuse.Triggers.WhileFalse.h>
#include <Fuse.Triggers.WhileTrue.h>
#include <Fuse.VisualEventHandler.h>
#include <Uno.Bool.h>
#include <Uno.Float.h>
#include <Uno.Float4.h>
#include <Uno.Int.h>
#include <Uno.IO.BundleFile.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.UX.BundleFileSource.h>
#include <Uno.UX.FileSource.h>
#include <Uno.UX.NameTable.h>
#include <Uno.UX.Property.h>
#include <Uno.UX.Property1-1.h>
#include <Uno.UX.Selector.h>
#include <Uno.UX.Unit.h>
static uString* STRINGS[42];
static uType* TYPES[10];

namespace g{

// public partial sealed class Dashboard :2
// {
// static Dashboard() :56
static void Dashboard__cctor_4_fn(uType* __type)
{
    Dashboard::__g_static_nametable1_ = uArray::Init<uString*>(::TYPES[0/*string[]*/], 18, ::STRINGS[0/*"router"*/], ::STRINGS[1/*"EdgeNavigator"*/], ::STRINGS[2/*"menu"*/], ::STRINGS[3/*"temp_eb7"*/], ::STRINGS[4/*"temp_eb8"*/], ::STRINGS[5/*"content"*/], ::STRINGS[6/*"mainAppTran...*/], ::STRINGS[7/*"topRectangle"*/], ::STRINGS[8/*"topMenuTran...*/], ::STRINGS[9/*"topMenuRota...*/], ::STRINGS[10/*"middleRecta...*/], ::STRINGS[11/*"bottomRecta...*/], ::STRINGS[12/*"bottomMenuT...*/], ::STRINGS[13/*"bottomMenuR...*/], ::STRINGS[14/*"parentScrol...*/], ::STRINGS[15/*"temp_eb9"*/], ::STRINGS[16/*"temp_eb10"*/], ::STRINGS[17/*"temp_eb11"*/]);
    Dashboard::__selector0_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[18/*"X"*/]);
    Dashboard::__selector1_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[19/*"Y"*/]);
    Dashboard::__selector2_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[20/*"Opacity"*/]);
    Dashboard::__selector3_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[21/*"Degrees"*/]);
    Dashboard::__selector4_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[22/*"Width"*/]);
    Dashboard::__selector5_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[23/*"Value"*/]);
    Dashboard::__selector6_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[24/*"Items"*/]);
    Dashboard::__selector7_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[1/*"EdgeNavigator"*/]);
    Dashboard::__selector8_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[2/*"menu"*/]);
    Dashboard::__selector9_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[5/*"content"*/]);
    Dashboard::__selector10_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[6/*"mainAppTran...*/]);
    Dashboard::__selector11_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[7/*"topRectangle"*/]);
    Dashboard::__selector12_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[8/*"topMenuTran...*/]);
    Dashboard::__selector13_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[9/*"topMenuRota...*/]);
    Dashboard::__selector14_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[10/*"middleRecta...*/]);
    Dashboard::__selector15_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[11/*"bottomRecta...*/]);
    Dashboard::__selector16_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[12/*"bottomMenuT...*/]);
    Dashboard::__selector17_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[13/*"bottomMenuR...*/]);
    Dashboard::__selector18_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[14/*"parentScrol...*/]);
}

static void Dashboard_build(uType* type)
{
    ::STRINGS[0] = uString::Const("router");
    ::STRINGS[1] = uString::Const("EdgeNavigator");
    ::STRINGS[2] = uString::Const("menu");
    ::STRINGS[3] = uString::Const("temp_eb7");
    ::STRINGS[4] = uString::Const("temp_eb8");
    ::STRINGS[5] = uString::Const("content");
    ::STRINGS[6] = uString::Const("mainAppTranslation");
    ::STRINGS[7] = uString::Const("topRectangle");
    ::STRINGS[8] = uString::Const("topMenuTranslation");
    ::STRINGS[9] = uString::Const("topMenuRotation");
    ::STRINGS[10] = uString::Const("middleRectangle");
    ::STRINGS[11] = uString::Const("bottomRectangle");
    ::STRINGS[12] = uString::Const("bottomMenuTranslation");
    ::STRINGS[13] = uString::Const("bottomMenuRotation");
    ::STRINGS[14] = uString::Const("parentScrollView");
    ::STRINGS[15] = uString::Const("temp_eb9");
    ::STRINGS[16] = uString::Const("temp_eb10");
    ::STRINGS[17] = uString::Const("temp_eb11");
    ::STRINGS[18] = uString::Const("X");
    ::STRINGS[19] = uString::Const("Y");
    ::STRINGS[20] = uString::Const("Opacity");
    ::STRINGS[21] = uString::Const("Degrees");
    ::STRINGS[22] = uString::Const("Width");
    ::STRINGS[23] = uString::Const("Value");
    ::STRINGS[24] = uString::Const("Items");
    ::STRINGS[25] = uString::Const("setSidebarOpen");
    ::STRINGS[26] = uString::Const("setSidebarClosed");
    ::STRINGS[27] = uString::Const("sidebarOpen");
    ::STRINGS[28] = uString::Const("morning");
    ::STRINGS[29] = uString::Const("notification");
    ::STRINGS[30] = uString::Const("day");
    ::STRINGS[31] = uString::Const("workinfo");
    ::STRINGS[32] = uString::Const("evening");
    ::STRINGS[33] = uString::Const("news");
    ::STRINGS[34] = uString::Const("dashboard.ux");
    ::STRINGS[35] = uString::Const("\n"
        "\t\tvar Observable = require(\"FuseJS/Observable\");\n"
        "\t\tvar ImageTools = require(\"FuseJS/ImageTools\");\n"
        "\t\tvar imagePath = Observable();\n"
        "\t\tvar API = require('js_files/api.js');\n"
        "\t\tvar nav = require('js_files/navigation.js');\n"
        "\t\tvar value = Observable(false);\n"
        "\t\tvar ys = false;\n"
        "\t\tvar sidebarOpen = Observable(false);\n"
        "\t\tvar username = Observable();\n"
        "\t\t//sidebarOpen.value = ys;\n"
        "\t\tvar data = Observable();\n"
        "\t\tvar day = Observable();\n"
        "\t\tvar personnel_id = Observable();\n"
        "\t\tvar Storage = require(\"FuseJS/Storage\");\n"
        "\n"
        "\n"
        "\t\tdataPath = API.readOSPID();\n"
        "\t\tStorage.read(dataPath).then(function(content) {\n"
        "\t\t\t    var data = JSON.parse(content);\n"
        "\t\t\t    //console.dir(data.personnel[0].surname)\n"
        "\t\t\t    username.value = data.personnel[0].surname+' '+data.personnel[0].forenames;\n"
        "\t\t\t    ImageTools.getImageFromBase64(data.picture).\n"
        "        \t\t\tthen(function (image) \n"
        "                { \n"
        "                    //console.log(\"Scratch image path is: \" + image.path); \n"
        "                    imagePath.value = image.path\n"
        "                }).catch(function(anerr){console.log(JSON.stringify(anerr))});\n"
        "\n"
        "\t\t\t    \n"
        "        \t\t\tfor (var i =0; i < data.permits.length; i++) {\n"
        "\t\t\t\t\t\tday.add(new Item(i+1, data.permits[i].course, data.permits[i].expiry_date));\n"
        "\t\t\t\t\t}\n"
        "\n"
        "\n"
        "\t\t\t}, function(error) {\n"
        "\t\t\t    //For now, let's expect the error to be because of the file not being found.\n"
        "\t\t\t    //welcomeText.value = \"There is currently no local data stored\";\n"
        "\t\t\t});\n"
        "\t\t\n"
        "\n"
        "\n"
        "\t\tfunction setSidebarOpen() {\n"
        "\t\t\tsidebarOpen.value = true;\n"
        "\t\t};\n"
        "\t\tfunction setSidebarClosed() {\n"
        "\t\t\tsidebarOpen.value = false;\n"
        "\t\t};\n"
        "\n"
        "\t\tfunction Item(id, title, date) {\n"
        "\t        this.id = id;\n"
        "\t        this.title = title;\n"
        "\t        this.date = date;\n"
        "\t        this.selected = Observable(false);\n"
        "\t    }\n"
        "\n"
        "\t    var morning = Observable(\n"
        "\t        new Item(1, \"Permit about to expire\", \"20/10/2017\"),\n"
        "\t        new Item(2, \"Oil & Gas Training\", \"22/10/2017\"),\n"
        "\t        new Item(3, \"Fire drill\", \"2/11/2017\")\n"
        "\t    );\n"
        "\t    // var day = Observable(\n"
        "\t        \n"
        "\t    //     new Item(1, \"PMP\", \"05/08/2017\"),\n"
        "\t    //     new Item(2, \"OSP\", \"02/09/2017\"),\n"
        "\t    //     new Item(3, \"Learn something new\", \"10/09/2017\")\n"
        "\t    // );\n"
        "\t    var evening = Observable(\n"
        "\t        new Item(1, \"Dinner with mom\", \"20 min ago\"),\n"
        "\t        new Item(2, \"Play chess with a friend\", \"58 min ago\"),\n"
        "\t        new Item(3, \"Watch TV\", \"1 hr ago\")\n"
        "\t    );\n"
        "\n"
        "\t    function logout() {\n"
        "\t    \tusername.value = null;\n"
        "\t\t\t// nav.logout();\n"
        "\t\t\t//console.log(\"help\");\n"
        "\t\t\trouter.goto(\"home\");\n"
        "\t    }\n"
        "\n"
        "\t\tmodule.exports = {\n"
        "\t\t\tvalue: value,\n"
        "\t\t\tsidebarOpen: sidebarOpen,\n"
        "\t\t\tsetSidebarOpen: setSidebarOpen,\n"
        "\t\t\tsetSidebarClosed: setSidebarClosed,\n"
        "\t\t\tmorning: morning,\n"
        "\t        day: day,\n"
        "\t        evening: evening,\n"
        "\t        logout: logout,\n"
        "\t        username: username,\n"
        "\t\t\timage: imagePath,\n"
        "\t\t\tpersonnel_id: personnel_id\n"
        "\t\t};\n"
        "\t");
    ::STRINGS[36] = uString::Const("auto,1*,auto");
    ::STRINGS[37] = uString::Const("DASHBOARD");
    ::STRINGS[38] = uString::Const("Notifications");
    ::STRINGS[39] = uString::Const("View all");
    ::STRINGS[40] = uString::Const("Permits");
    ::STRINGS[41] = uString::Const("News");
    ::TYPES[0] = ::g::Uno::String_typeof()->Array();
    ::TYPES[1] = ::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL);
    ::TYPES[2] = ::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::UX::Size_typeof(), NULL);
    ::TYPES[3] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL);
    ::TYPES[4] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL);
    ::TYPES[5] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Triggers::Actions::TriggerAction_typeof(), NULL);
    ::TYPES[6] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL);
    ::TYPES[7] = ::g::Fuse::VisualEventHandler_typeof();
    ::TYPES[8] = ::g::Fuse::Gestures::ClickedHandler_typeof();
    ::TYPES[9] = ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL);
    type->SetDependencies(
        ::g::Fuse::Animations::Easing_typeof(),
        ::g::Fuse::Navigation::EdgeNavigation_typeof(),
        ::g::OSP_bundle_typeof());
    type->SetInterfaces(
        ::g::Uno::Collections::IList_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface0),
        ::g::Fuse::Scripting::IScriptObject_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface1),
        ::g::Fuse::IProperties_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface2),
        ::g::Fuse::INotifyUnrooted_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface3),
        ::g::Fuse::ISourceLocation_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface4),
        ::TYPES[6/*Uno.Collections.ICollection<Fuse.Binding>*/], offsetof(::g::Fuse::Controls::Panel_type, interface5),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface6),
        ::g::Uno::Collections::IList_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface7),
        ::g::Uno::UX::IPropertyListener_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface8),
        ::g::Fuse::ITemplateSource_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface9),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Visual_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface10),
        ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/], offsetof(::g::Fuse::Controls::Panel_type, interface11),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface12),
        ::g::Fuse::Triggers::Actions::IShow_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface13),
        ::g::Fuse::Triggers::Actions::IHide_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface14),
        ::g::Fuse::Triggers::Actions::ICollapse_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface15),
        ::g::Fuse::IActualPlacement_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface16),
        ::g::Fuse::Animations::IResize_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface17),
        ::g::Fuse::Drawing::ISurfaceDrawable_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface18));
    type->SetFields(126,
        ::g::Fuse::Navigation::Router_typeof(), offsetof(Dashboard, router), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(Dashboard, mainAppTranslation_X_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(Dashboard, topMenuTranslation_Y_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(Dashboard, bottomMenuTranslation_Y_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(Dashboard, middleRectangle_Opacity_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(Dashboard, topMenuRotation_Degrees_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(Dashboard, bottomMenuRotation_Degrees_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::UX::Size_typeof(), NULL), offsetof(Dashboard, topRectangle_Width_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::UX::Size_typeof(), NULL), offsetof(Dashboard, bottomRectangle_Width_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Bool_typeof(), NULL), offsetof(Dashboard, temp_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Bool_typeof(), NULL), offsetof(Dashboard, temp1_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(uObject_typeof(), NULL), offsetof(Dashboard, temp2_Items_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(uObject_typeof(), NULL), offsetof(Dashboard, temp3_Items_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(uObject_typeof(), NULL), offsetof(Dashboard, temp4_Items_inst), 0,
        ::g::Fuse::Controls::EdgeNavigator_typeof(), offsetof(Dashboard, EdgeNavigator), 0,
        ::g::Sidebar_typeof(), offsetof(Dashboard, menu), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(Dashboard, temp_eb7), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(Dashboard, temp_eb8), 0,
        ::g::Fuse::Controls::DockPanel_typeof(), offsetof(Dashboard, content), 0,
        ::g::Fuse::Translation_typeof(), offsetof(Dashboard, mainAppTranslation), 0,
        ::g::Fuse::Controls::Rectangle_typeof(), offsetof(Dashboard, topRectangle), 0,
        ::g::Fuse::Translation_typeof(), offsetof(Dashboard, topMenuTranslation), 0,
        ::g::Fuse::Rotation_typeof(), offsetof(Dashboard, topMenuRotation), 0,
        ::g::Fuse::Controls::Rectangle_typeof(), offsetof(Dashboard, middleRectangle), 0,
        ::g::Fuse::Controls::Rectangle_typeof(), offsetof(Dashboard, bottomRectangle), 0,
        ::g::Fuse::Translation_typeof(), offsetof(Dashboard, bottomMenuTranslation), 0,
        ::g::Fuse::Rotation_typeof(), offsetof(Dashboard, bottomMenuRotation), 0,
        ::g::Fuse::Controls::ScrollView_typeof(), offsetof(Dashboard, parentScrollView), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(Dashboard, temp_eb9), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(Dashboard, temp_eb10), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(Dashboard, temp_eb11), 0,
        ::g::Uno::UX::NameTable_typeof(), offsetof(Dashboard, __g_nametable1), 0,
        ::TYPES[0/*string[]*/], (uintptr_t)&Dashboard::__g_static_nametable1_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Dashboard::__selector0_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Dashboard::__selector1_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Dashboard::__selector2_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Dashboard::__selector3_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Dashboard::__selector4_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Dashboard::__selector5_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Dashboard::__selector6_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Dashboard::__selector7_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Dashboard::__selector8_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Dashboard::__selector9_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Dashboard::__selector10_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Dashboard::__selector11_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Dashboard::__selector12_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Dashboard::__selector13_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Dashboard::__selector14_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Dashboard::__selector15_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Dashboard::__selector16_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Dashboard::__selector17_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Dashboard::__selector18_, uFieldFlagsStatic);
    type->Reflection.SetFunctions(1,
        new uFunction(".ctor", NULL, (void*)Dashboard__New5_fn, 0, true, type, 1, ::g::Fuse::Navigation::Router_typeof()));
}

::g::Fuse::Controls::Panel_type* Dashboard_typeof()
{
    static uSStrong< ::g::Fuse::Controls::Panel_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Fuse::Controls::Page_typeof();
    options.FieldCount = 178;
    options.InterfaceCount = 19;
    options.DependencyCount = 3;
    options.ObjectSize = sizeof(Dashboard);
    options.TypeSize = sizeof(::g::Fuse::Controls::Panel_type);
    type = (::g::Fuse::Controls::Panel_type*)uClassType::New("Dashboard", options);
    type->fp_build_ = Dashboard_build;
    type->fp_cctor_ = Dashboard__cctor_4_fn;
    type->interface18.fp_Draw = (void(*)(uObject*, ::g::Fuse::Drawing::Surface*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableDraw_fn;
    type->interface18.fp_get_IsPrimary = (void(*)(uObject*, bool*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableget_IsPrimary_fn;
    type->interface18.fp_get_ElementSize = (void(*)(uObject*, ::g::Uno::Float2*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableget_ElementSize_fn;
    type->interface13.fp_Show = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsIShowShow_fn;
    type->interface15.fp_Collapse = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsICollapseCollapse_fn;
    type->interface14.fp_Hide = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsIHideHide_fn;
    type->interface17.fp_SetSize = (void(*)(uObject*, ::g::Uno::Float2*))::g::Fuse::Elements::Element__FuseAnimationsIResizeSetSize_fn;
    type->interface16.fp_get_ActualSize = (void(*)(uObject*, ::g::Uno::Float3*))::g::Fuse::Elements::Element__FuseIActualPlacementget_ActualSize_fn;
    type->interface16.fp_get_ActualPosition = (void(*)(uObject*, ::g::Uno::Float3*))::g::Fuse::Elements::Element__FuseIActualPlacementget_ActualPosition_fn;
    type->interface16.fp_add_Placed = (void(*)(uObject*, uDelegate*))::g::Fuse::Elements::Element__add_Placed_fn;
    type->interface16.fp_remove_Placed = (void(*)(uObject*, uDelegate*))::g::Fuse::Elements::Element__remove_Placed_fn;
    type->interface10.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Visual__UnoCollectionsIEnumerableFuseVisualGetEnumerator_fn;
    type->interface11.fp_Clear = (void(*)(uObject*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeClear_fn;
    type->interface11.fp_Contains = (void(*)(uObject*, void*, bool*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeContains_fn;
    type->interface7.fp_RemoveAt = (void(*)(uObject*, int32_t*))::g::Fuse::Visual__UnoCollectionsIListFuseNodeRemoveAt_fn;
    type->interface12.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Visual__UnoCollectionsIEnumerableFuseNodeGetEnumerator_fn;
    type->interface11.fp_get_Count = (void(*)(uObject*, int32_t*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeget_Count_fn;
    type->interface7.fp_get_Item = (void(*)(uObject*, int32_t*, uTRef))::g::Fuse::Visual__UnoCollectionsIListFuseNodeget_Item_fn;
    type->interface7.fp_Insert = (void(*)(uObject*, int32_t*, void*))::g::Fuse::Visual__Insert1_fn;
    type->interface8.fp_OnPropertyChanged = (void(*)(uObject*, ::g::Uno::UX::PropertyObject*, ::g::Uno::UX::Selector*))::g::Fuse::Controls::Control__OnPropertyChanged2_fn;
    type->interface9.fp_FindTemplate = (void(*)(uObject*, uString*, ::g::Uno::UX::Template**))::g::Fuse::Visual__FindTemplate_fn;
    type->interface11.fp_Add = (void(*)(uObject*, void*))::g::Fuse::Visual__Add1_fn;
    type->interface11.fp_Remove = (void(*)(uObject*, void*, bool*))::g::Fuse::Visual__Remove1_fn;
    type->interface5.fp_Clear = (void(*)(uObject*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingClear_fn;
    type->interface5.fp_Contains = (void(*)(uObject*, void*, bool*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingContains_fn;
    type->interface0.fp_RemoveAt = (void(*)(uObject*, int32_t*))::g::Fuse::Node__UnoCollectionsIListFuseBindingRemoveAt_fn;
    type->interface6.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Node__UnoCollectionsIEnumerableFuseBindingGetEnumerator_fn;
    type->interface1.fp_SetScriptObject = (void(*)(uObject*, uObject*, ::g::Fuse::Scripting::Context*))::g::Fuse::Node__FuseScriptingIScriptObjectSetScriptObject_fn;
    type->interface5.fp_get_Count = (void(*)(uObject*, int32_t*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingget_Count_fn;
    type->interface0.fp_get_Item = (void(*)(uObject*, int32_t*, uTRef))::g::Fuse::Node__UnoCollectionsIListFuseBindingget_Item_fn;
    type->interface1.fp_get_ScriptObject = (void(*)(uObject*, uObject**))::g::Fuse::Node__FuseScriptingIScriptObjectget_ScriptObject_fn;
    type->interface1.fp_get_ScriptContext = (void(*)(uObject*, ::g::Fuse::Scripting::Context**))::g::Fuse::Node__FuseScriptingIScriptObjectget_ScriptContext_fn;
    type->interface4.fp_get_SourceNearest = (void(*)(uObject*, uObject**))::g::Fuse::Node__FuseISourceLocationget_SourceNearest_fn;
    type->interface3.fp_add_Unrooted = (void(*)(uObject*, uDelegate*))::g::Fuse::Node__FuseINotifyUnrootedadd_Unrooted_fn;
    type->interface3.fp_remove_Unrooted = (void(*)(uObject*, uDelegate*))::g::Fuse::Node__FuseINotifyUnrootedremove_Unrooted_fn;
    type->interface0.fp_Insert = (void(*)(uObject*, int32_t*, void*))::g::Fuse::Node__Insert_fn;
    type->interface2.fp_get_Properties = (void(*)(uObject*, ::g::Fuse::Properties**))::g::Fuse::Node__get_Properties_fn;
    type->interface4.fp_get_SourceLineNumber = (void(*)(uObject*, int32_t*))::g::Fuse::Node__get_SourceLineNumber_fn;
    type->interface4.fp_get_SourceFileName = (void(*)(uObject*, uString**))::g::Fuse::Node__get_SourceFileName_fn;
    type->interface5.fp_Add = (void(*)(uObject*, void*))::g::Fuse::Node__Add_fn;
    type->interface5.fp_Remove = (void(*)(uObject*, void*, bool*))::g::Fuse::Node__Remove_fn;
    return type;
}

// public Dashboard(Fuse.Navigation.Router router) :60
void Dashboard__ctor_8_fn(Dashboard* __this, ::g::Fuse::Navigation::Router* router1)
{
    __this->ctor_8(router1);
}

// private void InitializeUX() :66
void Dashboard__InitializeUX_fn(Dashboard* __this)
{
    __this->InitializeUX();
}

// public Dashboard New(Fuse.Navigation.Router router) :60
void Dashboard__New5_fn(::g::Fuse::Navigation::Router* router1, Dashboard** __retval)
{
    *__retval = Dashboard::New5(router1);
}

uSStrong<uArray*> Dashboard::__g_static_nametable1_;
::g::Uno::UX::Selector Dashboard::__selector0_;
::g::Uno::UX::Selector Dashboard::__selector1_;
::g::Uno::UX::Selector Dashboard::__selector2_;
::g::Uno::UX::Selector Dashboard::__selector3_;
::g::Uno::UX::Selector Dashboard::__selector4_;
::g::Uno::UX::Selector Dashboard::__selector5_;
::g::Uno::UX::Selector Dashboard::__selector6_;
::g::Uno::UX::Selector Dashboard::__selector7_;
::g::Uno::UX::Selector Dashboard::__selector8_;
::g::Uno::UX::Selector Dashboard::__selector9_;
::g::Uno::UX::Selector Dashboard::__selector10_;
::g::Uno::UX::Selector Dashboard::__selector11_;
::g::Uno::UX::Selector Dashboard::__selector12_;
::g::Uno::UX::Selector Dashboard::__selector13_;
::g::Uno::UX::Selector Dashboard::__selector14_;
::g::Uno::UX::Selector Dashboard::__selector15_;
::g::Uno::UX::Selector Dashboard::__selector16_;
::g::Uno::UX::Selector Dashboard::__selector17_;
::g::Uno::UX::Selector Dashboard::__selector18_;

// public Dashboard(Fuse.Navigation.Router router) [instance] :60
void Dashboard::ctor_8(::g::Fuse::Navigation::Router* router1)
{
    uStackFrame __("Dashboard", ".ctor(Fuse.Navigation.Router)");
    ctor_7();
    router = router1;
    InitializeUX();
}

// private void InitializeUX() [instance] :66
void Dashboard::InitializeUX()
{
    uStackFrame __("Dashboard", "InitializeUX()");
    __g_nametable1 = ::g::Uno::UX::NameTable::New1(NULL, Dashboard::__g_static_nametable1_);
    mainAppTranslation = ::g::Fuse::Translation::New2();
    mainAppTranslation_X_inst = ::g::OSP_FuseTranslation_X_Property::New1(mainAppTranslation, Dashboard::__selector0_);
    topMenuTranslation = ::g::Fuse::Translation::New2();
    topMenuTranslation_Y_inst = ::g::OSP_FuseTranslation_Y_Property::New1(topMenuTranslation, Dashboard::__selector1_);
    bottomMenuTranslation = ::g::Fuse::Translation::New2();
    bottomMenuTranslation_Y_inst = ::g::OSP_FuseTranslation_Y_Property::New1(bottomMenuTranslation, Dashboard::__selector1_);
    middleRectangle = ::g::Fuse::Controls::Rectangle::New3();
    middleRectangle_Opacity_inst = ::g::OSP_FuseElementsElement_Opacity_Property::New1(middleRectangle, Dashboard::__selector2_);
    topMenuRotation = ::g::Fuse::Rotation::New2();
    topMenuRotation_Degrees_inst = ::g::OSP_FuseRotation_Degrees_Property::New1(topMenuRotation, Dashboard::__selector3_);
    bottomMenuRotation = ::g::Fuse::Rotation::New2();
    bottomMenuRotation_Degrees_inst = ::g::OSP_FuseRotation_Degrees_Property::New1(bottomMenuRotation, Dashboard::__selector3_);
    topRectangle = ::g::Fuse::Controls::Rectangle::New3();
    topRectangle_Width_inst = ::g::OSP_FuseElementsElement_Width_Property::New1(topRectangle, Dashboard::__selector4_);
    bottomRectangle = ::g::Fuse::Controls::Rectangle::New3();
    bottomRectangle_Width_inst = ::g::OSP_FuseElementsElement_Width_Property::New1(bottomRectangle, Dashboard::__selector4_);
    ::g::Fuse::Reactive::Data* temp5 = ::g::Fuse::Reactive::Data::New1(::STRINGS[25/*"setSidebarO...*/]);
    ::g::Fuse::Reactive::Data* temp6 = ::g::Fuse::Reactive::Data::New1(::STRINGS[26/*"setSidebarC...*/]);
    ::g::Fuse::Triggers::WhileTrue* temp = ::g::Fuse::Triggers::WhileTrue::New2();
    temp_Value_inst = ::g::OSP_FuseTriggersWhileBool_Value_Property::New1(temp, Dashboard::__selector5_);
    ::g::Fuse::Reactive::Data* temp7 = ::g::Fuse::Reactive::Data::New1(::STRINGS[27/*"sidebarOpen"*/]);
    ::g::Fuse::Triggers::WhileFalse* temp1 = ::g::Fuse::Triggers::WhileFalse::New2();
    temp1_Value_inst = ::g::OSP_FuseTriggersWhileBool_Value_Property::New1(temp1, Dashboard::__selector5_);
    ::g::Fuse::Reactive::Data* temp8 = ::g::Fuse::Reactive::Data::New1(::STRINGS[27/*"sidebarOpen"*/]);
    parentScrollView = ::g::Fuse::Controls::ScrollView::New4();
    ::g::SortableList* temp2 = ::g::SortableList::New4(parentScrollView);
    temp2_Items_inst = ::g::OSP_SortableList_Items_Property::New1(temp2, Dashboard::__selector6_);
    ::g::Fuse::Reactive::Data* temp9 = ::g::Fuse::Reactive::Data::New1(::STRINGS[28/*"morning"*/]);
    ::g::Fuse::Reactive::Data* temp10 = ::g::Fuse::Reactive::Data::New1(::STRINGS[29/*"notification"*/]);
    ::g::SortableList* temp3 = ::g::SortableList::New4(parentScrollView);
    temp3_Items_inst = ::g::OSP_SortableList_Items_Property::New1(temp3, Dashboard::__selector6_);
    ::g::Fuse::Reactive::Data* temp11 = ::g::Fuse::Reactive::Data::New1(::STRINGS[30/*"day"*/]);
    ::g::Fuse::Reactive::Data* temp12 = ::g::Fuse::Reactive::Data::New1(::STRINGS[31/*"workinfo"*/]);
    ::g::SortableList* temp4 = ::g::SortableList::New4(parentScrollView);
    temp4_Items_inst = ::g::OSP_SortableList_Items_Property::New1(temp4, Dashboard::__selector6_);
    ::g::Fuse::Reactive::Data* temp13 = ::g::Fuse::Reactive::Data::New1(::STRINGS[32/*"evening"*/]);
    ::g::Fuse::Reactive::Data* temp14 = ::g::Fuse::Reactive::Data::New1(::STRINGS[33/*"news"*/]);
    ::g::Fuse::Reactive::JavaScript* temp15 = ::g::Fuse::Reactive::JavaScript::New2(__g_nametable1);
    ::g::Fuse::Reactive::JavaScript* temp16 = ::g::Fuse::Reactive::JavaScript::New2(__g_nametable1);
    EdgeNavigator = ::g::Fuse::Controls::EdgeNavigator::New4();
    menu = ::g::Sidebar::New5();
    ::g::Fuse::Navigation::ActivatingAnimation* temp17 = ::g::Fuse::Navigation::ActivatingAnimation::New2();
    ::g::Fuse::Animations::Change* temp18 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::TYPES[1/*Fuse.Animations.Change<float>*/], mainAppTranslation_X_inst);
    ::g::Fuse::Animations::Change* temp19 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::TYPES[1/*Fuse.Animations.Change<float>*/], topMenuTranslation_Y_inst);
    ::g::Fuse::Animations::Change* temp20 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::TYPES[1/*Fuse.Animations.Change<float>*/], bottomMenuTranslation_Y_inst);
    ::g::Fuse::Animations::Change* temp21 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::TYPES[1/*Fuse.Animations.Change<float>*/], middleRectangle_Opacity_inst);
    ::g::Fuse::Animations::Change* temp22 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::TYPES[1/*Fuse.Animations.Change<float>*/], topMenuRotation_Degrees_inst);
    ::g::Fuse::Animations::Change* temp23 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::TYPES[1/*Fuse.Animations.Change<float>*/], bottomMenuRotation_Degrees_inst);
    ::g::Fuse::Animations::Change* temp24 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::TYPES[2/*Fuse.Animations.Change<Uno.UX.Size>*/], topRectangle_Width_inst);
    ::g::Fuse::Animations::Change* temp25 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::TYPES[2/*Fuse.Animations.Change<Uno.UX.Size>*/], bottomRectangle_Width_inst);
    ::g::Fuse::Navigation::WhileActive* temp26 = ::g::Fuse::Navigation::WhileActive::New2();
    ::g::Fuse::Triggers::Actions::Callback* temp27 = ::g::Fuse::Triggers::Actions::Callback::New2();
    temp_eb7 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp5);
    ::g::Fuse::Navigation::WhileInactive* temp28 = ::g::Fuse::Navigation::WhileInactive::New2();
    ::g::Fuse::Triggers::Actions::Callback* temp29 = ::g::Fuse::Triggers::Actions::Callback::New2();
    temp_eb8 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp6);
    content = ::g::Fuse::Controls::DockPanel::New4();
    ::g::Fuse::Controls::StackPanel* temp30 = ::g::Fuse::Controls::StackPanel::New4();
    ::g::Fuse::Controls::Grid* temp31 = ::g::Fuse::Controls::Grid::New4();
    ::g::Fuse::Controls::Panel* temp32 = ::g::Fuse::Controls::Panel::New3();
    ::g::Fuse::Gestures::Clicked* temp33 = ::g::Fuse::Gestures::Clicked::New2();
    ::g::Fuse::Navigation::NavigateTo* temp34 = ::g::Fuse::Navigation::NavigateTo::New2();
    ::g::Fuse::Reactive::DataBinding* temp35 = ::g::Fuse::Reactive::DataBinding::New1(temp_Value_inst, (uObject*)temp7, 3);
    ::g::Fuse::Gestures::Clicked* temp36 = ::g::Fuse::Gestures::Clicked::New2();
    ::g::Fuse::Navigation::NavigateTo* temp37 = ::g::Fuse::Navigation::NavigateTo::New2();
    ::g::Fuse::Reactive::DataBinding* temp38 = ::g::Fuse::Reactive::DataBinding::New1(temp1_Value_inst, (uObject*)temp8, 3);
    ::g::Fuse::Controls::Text* temp39 = ::g::Fuse::Controls::Text::New3();
    ::g::Fuse::Font* temp40 = ::g::Fuse::Font::New2(::g::Uno::UX::BundleFileSource::New1(::g::OSP_bundle::AlegreyaSansBoldac83ec20()));
    ::g::Fuse::Drawing::StaticSolidColor* temp41 = ::g::Fuse::Drawing::StaticSolidColor::New2(::g::Uno::Float4__New2(0.1137255f, 0.4901961f, 0.2588235f, 1.0f));
    ::g::Fuse::Controls::Rectangle* temp42 = ::g::Fuse::Controls::Rectangle::New3();
    ::g::Fuse::Controls::StackPanel* temp43 = ::g::Fuse::Controls::StackPanel::New4();
    ::g::Fuse::Controls::Panel* temp44 = ::g::Fuse::Controls::Panel::New3();
    ::g::Fuse::Reactive::DataBinding* temp45 = ::g::Fuse::Reactive::DataBinding::New1(temp2_Items_inst, (uObject*)temp9, 3);
    ::g::SquareButton* temp46 = ::g::SquareButton::New6();
    temp_eb9 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp10);
    ::g::Fuse::Reactive::DataBinding* temp47 = ::g::Fuse::Reactive::DataBinding::New1(temp3_Items_inst, (uObject*)temp11, 3);
    ::g::SquareButton* temp48 = ::g::SquareButton::New6();
    temp_eb10 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp12);
    ::g::Fuse::Reactive::DataBinding* temp49 = ::g::Fuse::Reactive::DataBinding::New1(temp4_Items_inst, (uObject*)temp13, 3);
    ::g::SquareButton* temp50 = ::g::SquareButton::New6();
    temp_eb11 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp14);
    ::g::Fuse::Drawing::StaticSolidColor* temp51 = ::g::Fuse::Drawing::StaticSolidColor::New2(::g::Uno::Float4__New2(0.9333333f, 0.9333333f, 0.9333333f, 1.0f));
    ::g::Fuse::Drawing::StaticSolidColor* temp52 = ::g::Fuse::Drawing::StaticSolidColor::New2(::g::Uno::Float4__New2(0.7333333f, 0.7333333f, 0.7333333f, 1.0f));
    SourceLineNumber(1);
    SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    temp15->LineNumber(3);
    temp15->FileName(::STRINGS[34/*"dashboard.ux"*/]);
    temp15->SourceLineNumber(3);
    temp15->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    temp15->File(::g::Uno::UX::BundleFileSource::New1(::g::OSP_bundle::navigationb176862e()));
    temp16->Code(::STRINGS[35/*"\n\t\tvar O...*/]);
    temp16->LineNumber(4);
    temp16->FileName(::STRINGS[34/*"dashboard.ux"*/]);
    temp16->SourceLineNumber(4);
    temp16->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    uPtr(EdgeNavigator)->Name(Dashboard::__selector7_);
    uPtr(EdgeNavigator)->SourceLineNumber(99);
    uPtr(EdgeNavigator)->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(EdgeNavigator)->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), menu);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(EdgeNavigator)->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), content);
    uPtr(menu)->Width(::g::Uno::UX::Size__New1(180.0f, 1));
    uPtr(menu)->Name(Dashboard::__selector8_);
    uPtr(menu)->SourceLineNumber(116);
    uPtr(menu)->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    ::g::Fuse::Navigation::EdgeNavigation::SetEdge(menu, 0);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(menu)->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), temp17);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(menu)->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), temp26);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(menu)->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), temp28);
    temp17->SourceLineNumber(118);
    temp17->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp17->Animators()), ::TYPES[4/*Uno.Collections.ICollection<Fuse.Animations.Animator>*/]), temp18);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp17->Animators()), ::TYPES[4/*Uno.Collections.ICollection<Fuse.Animations.Animator>*/]), temp19);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp17->Animators()), ::TYPES[4/*Uno.Collections.ICollection<Fuse.Animations.Animator>*/]), temp20);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp17->Animators()), ::TYPES[4/*Uno.Collections.ICollection<Fuse.Animations.Animator>*/]), temp21);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp17->Animators()), ::TYPES[4/*Uno.Collections.ICollection<Fuse.Animations.Animator>*/]), temp22);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp17->Animators()), ::TYPES[4/*Uno.Collections.ICollection<Fuse.Animations.Animator>*/]), temp23);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp17->Animators()), ::TYPES[4/*Uno.Collections.ICollection<Fuse.Animations.Animator>*/]), temp24);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp17->Animators()), ::TYPES[4/*Uno.Collections.ICollection<Fuse.Animations.Animator>*/]), temp25);
    ::g::Fuse::Animations::Change__set_Value_fn(temp18, uCRef(180.0f));
    ::g::Fuse::Animations::Change__set_Value_fn(temp19, uCRef(0.0f));
    ::g::Fuse::Animations::Change__set_Value_fn(temp20, uCRef(0.0f));
    ::g::Fuse::Animations::Change__set_Value_fn(temp21, uCRef(0.0f));
    temp21->Easing(::g::Fuse::Animations::Easing::CircularOut());
    ::g::Fuse::Animations::Change__set_Value_fn(temp22, uCRef(45.0f));
    temp22->Easing(::g::Fuse::Animations::Easing::ExponentialIn());
    ::g::Fuse::Animations::Change__set_Value_fn(temp23, uCRef(-45.0f));
    temp23->Easing(::g::Fuse::Animations::Easing::ExponentialIn());
    ::g::Fuse::Animations::Change__set_Value_fn(temp24, uCRef(::g::Uno::UX::Size__New1(28.0f, 1)));
    ::g::Fuse::Animations::Change__set_Value_fn(temp25, uCRef(::g::Uno::UX::Size__New1(28.0f, 1)));
    temp26->SourceLineNumber(134);
    temp26->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp26->Actions()), ::TYPES[5/*Uno.Collections.ICollection<Fuse.Triggers.Actions.TriggerAction>*/]), temp27);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp26->Bindings()), ::TYPES[6/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp_eb7);
    temp27->SourceLineNumber(135);
    temp27->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    temp27->add_Handler(uDelegate::New(::TYPES[7/*Fuse.VisualEventHandler*/], (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb7)));
    temp5->SourceLineNumber(135);
    temp5->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    temp28->SourceLineNumber(137);
    temp28->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp28->Actions()), ::TYPES[5/*Uno.Collections.ICollection<Fuse.Triggers.Actions.TriggerAction>*/]), temp29);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp28->Bindings()), ::TYPES[6/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp_eb8);
    temp29->SourceLineNumber(138);
    temp29->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    temp29->add_Handler(uDelegate::New(::TYPES[7/*Fuse.VisualEventHandler*/], (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb8)));
    temp6->SourceLineNumber(138);
    temp6->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    uPtr(content)->Name(Dashboard::__selector9_);
    uPtr(content)->SourceLineNumber(142);
    uPtr(content)->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    uPtr(content)->Background(temp51);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(content)->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), mainAppTranslation);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(content)->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), temp30);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(content)->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), parentScrollView);
    uPtr(mainAppTranslation)->Name(Dashboard::__selector10_);
    uPtr(mainAppTranslation)->SourceLineNumber(144);
    uPtr(mainAppTranslation)->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    temp30->SourceLineNumber(146);
    temp30->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    ::g::Fuse::Controls::DockPanel::SetDock(temp30, 2);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp30->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), temp31);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp30->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), temp42);
    temp31->Columns(::STRINGS[36/*"auto,1*,auto"*/]);
    temp31->SourceLineNumber(149);
    temp31->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    temp31->Background(temp41);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp31->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), temp32);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp31->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), temp39);
    temp32->HitTestMode(2);
    temp32->Width(::g::Uno::UX::Size__New1(32.0f, 1));
    temp32->Height(::g::Uno::UX::Size__New1(32.0f, 1));
    temp32->Margin(::g::Uno::Float4__New2(7.0f, 5.0f, 5.0f, 5.0f));
    temp32->SourceLineNumber(150);
    temp32->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp32->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), temp);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp32->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), temp1);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp32->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), topRectangle);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp32->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), middleRectangle);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp32->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), bottomRectangle);
    temp->SourceLineNumber(151);
    temp->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Nodes()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), temp33);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Bindings()), ::TYPES[6/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp35);
    temp33->SourceLineNumber(152);
    temp33->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp33->Actions()), ::TYPES[5/*Uno.Collections.ICollection<Fuse.Triggers.Actions.TriggerAction>*/]), temp34);
    temp34->SourceLineNumber(153);
    temp34->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    temp34->Target(content);
    temp7->SourceLineNumber(151);
    temp7->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    temp1->SourceLineNumber(156);
    temp1->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp1->Nodes()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), temp36);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp1->Bindings()), ::TYPES[6/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp38);
    temp36->SourceLineNumber(157);
    temp36->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp36->Actions()), ::TYPES[5/*Uno.Collections.ICollection<Fuse.Triggers.Actions.TriggerAction>*/]), temp37);
    temp37->SourceLineNumber(158);
    temp37->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    temp37->Target(menu);
    temp8->SourceLineNumber(156);
    temp8->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    uPtr(topRectangle)->Color(::g::Uno::Float4__New2(0.9686275f, 0.8980392f, 0.372549f, 1.0f));
    uPtr(topRectangle)->Width(::g::Uno::UX::Size__New1(26.0f, 1));
    uPtr(topRectangle)->Height(::g::Uno::UX::Size__New1(2.0f, 1));
    uPtr(topRectangle)->Name(Dashboard::__selector11_);
    uPtr(topRectangle)->SourceLineNumber(161);
    uPtr(topRectangle)->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(topRectangle)->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), topMenuTranslation);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(topRectangle)->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), topMenuRotation);
    uPtr(topMenuTranslation)->Y(-9.0f);
    uPtr(topMenuTranslation)->Name(Dashboard::__selector12_);
    uPtr(topMenuTranslation)->SourceLineNumber(162);
    uPtr(topMenuTranslation)->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    uPtr(topMenuRotation)->Name(Dashboard::__selector13_);
    uPtr(topMenuRotation)->SourceLineNumber(163);
    uPtr(topMenuRotation)->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    uPtr(middleRectangle)->Color(::g::Uno::Float4__New2(0.9686275f, 0.8980392f, 0.372549f, 1.0f));
    uPtr(middleRectangle)->Width(::g::Uno::UX::Size__New1(26.0f, 1));
    uPtr(middleRectangle)->Height(::g::Uno::UX::Size__New1(2.0f, 1));
    uPtr(middleRectangle)->Name(Dashboard::__selector14_);
    uPtr(middleRectangle)->SourceLineNumber(165);
    uPtr(middleRectangle)->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    uPtr(bottomRectangle)->Color(::g::Uno::Float4__New2(0.9686275f, 0.8980392f, 0.372549f, 1.0f));
    uPtr(bottomRectangle)->Width(::g::Uno::UX::Size__New1(26.0f, 1));
    uPtr(bottomRectangle)->Height(::g::Uno::UX::Size__New1(2.0f, 1));
    uPtr(bottomRectangle)->Name(Dashboard::__selector15_);
    uPtr(bottomRectangle)->SourceLineNumber(166);
    uPtr(bottomRectangle)->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(bottomRectangle)->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), bottomMenuTranslation);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(bottomRectangle)->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), bottomMenuRotation);
    uPtr(bottomMenuTranslation)->Y(9.0f);
    uPtr(bottomMenuTranslation)->Name(Dashboard::__selector16_);
    uPtr(bottomMenuTranslation)->SourceLineNumber(167);
    uPtr(bottomMenuTranslation)->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    uPtr(bottomMenuRotation)->Name(Dashboard::__selector17_);
    uPtr(bottomMenuRotation)->SourceLineNumber(168);
    uPtr(bottomMenuRotation)->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    temp39->Value(::STRINGS[37/*"DASHBOARD"*/]);
    temp39->FontSize(20.0f);
    temp39->TextAlignment(1);
    temp39->TextColor(::g::Uno::Float4__New2(1.0f, 1.0f, 1.0f, 1.0f));
    temp39->Alignment(8);
    temp39->Margin(::g::Uno::Float4__New2(-20.0f, 5.0f, 0.0f, 0.0f));
    temp39->Padding(::g::Uno::Float4__New2(2.0f, 2.0f, 2.0f, 2.0f));
    temp39->SourceLineNumber(172);
    temp39->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    temp39->Font(temp40);
    temp42->Color(::g::Uno::Float4__New2(0.2f, 0.2352941f, 0.282353f, 1.0f));
    temp42->Height(::g::Uno::UX::Size__New1(1.0f, 1));
    temp42->Margin(::g::Uno::Float4__New2(0.0f, 0.0f, 0.0f, 0.0f));
    temp42->SourceLineNumber(178);
    temp42->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    uPtr(parentScrollView)->Name(Dashboard::__selector18_);
    uPtr(parentScrollView)->SourceLineNumber(182);
    uPtr(parentScrollView)->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(parentScrollView)->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), temp43);
    temp43->ItemSpacing(12.0f);
    temp43->Alignment(4);
    temp43->Margin(::g::Uno::Float4__New2(8.0f, 16.0f, 8.0f, 16.0f));
    temp43->SourceLineNumber(183);
    temp43->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp43->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), temp44);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp43->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), temp2);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp43->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), temp46);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp43->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), temp3);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp43->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), temp48);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp43->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), temp4);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp43->Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), temp50);
    temp44->Height(::g::Uno::UX::Size__New1(7.0f, 1));
    temp44->SourceLineNumber(184);
    temp44->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    temp2->Label(::STRINGS[38/*"Notifications"*/]);
    temp2->SourceLineNumber(187);
    temp2->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp2->Bindings()), ::TYPES[6/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp45);
    temp9->SourceLineNumber(187);
    temp9->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    temp46->Textq(::STRINGS[39/*"View all"*/]);
    temp46->SourceLineNumber(188);
    temp46->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    ::g::Fuse::Gestures::Clicked::AddHandler(temp46, uDelegate::New(::TYPES[8/*Fuse.Gestures.ClickedHandler*/], (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb9)));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp46->Bindings()), ::TYPES[6/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp_eb9);
    temp10->SourceLineNumber(188);
    temp10->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    temp3->Label(::STRINGS[40/*"Permits"*/]);
    temp3->SourceLineNumber(191);
    temp3->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp3->Bindings()), ::TYPES[6/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp47);
    temp11->SourceLineNumber(191);
    temp11->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    temp48->Textq(::STRINGS[39/*"View all"*/]);
    temp48->SourceLineNumber(192);
    temp48->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    ::g::Fuse::Gestures::Clicked::AddHandler(temp48, uDelegate::New(::TYPES[8/*Fuse.Gestures.ClickedHandler*/], (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb10)));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp48->Bindings()), ::TYPES[6/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp_eb10);
    temp12->SourceLineNumber(192);
    temp12->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    temp4->Label(::STRINGS[41/*"News"*/]);
    temp4->SourceLineNumber(195);
    temp4->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp4->Bindings()), ::TYPES[6/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp49);
    temp13->SourceLineNumber(195);
    temp13->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    temp50->Textq(::STRINGS[39/*"View all"*/]);
    temp50->SourceLineNumber(196);
    temp50->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    ::g::Fuse::Gestures::Clicked::AddHandler(temp50, uDelegate::New(::TYPES[8/*Fuse.Gestures.ClickedHandler*/], (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb11)));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp50->Bindings()), ::TYPES[6/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp_eb11);
    temp14->SourceLineNumber(196);
    temp14->SourceFileName(::STRINGS[34/*"dashboard.ux"*/]);
    uPtr(__g_nametable1)->This(this);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::TYPES[9/*Uno.Collections.ICollection<object>*/]), router);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::TYPES[9/*Uno.Collections.ICollection<object>*/]), EdgeNavigator);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::TYPES[9/*Uno.Collections.ICollection<object>*/]), menu);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::TYPES[9/*Uno.Collections.ICollection<object>*/]), temp_eb7);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::TYPES[9/*Uno.Collections.ICollection<object>*/]), temp_eb8);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::TYPES[9/*Uno.Collections.ICollection<object>*/]), content);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::TYPES[9/*Uno.Collections.ICollection<object>*/]), mainAppTranslation);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::TYPES[9/*Uno.Collections.ICollection<object>*/]), topRectangle);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::TYPES[9/*Uno.Collections.ICollection<object>*/]), topMenuTranslation);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::TYPES[9/*Uno.Collections.ICollection<object>*/]), topMenuRotation);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::TYPES[9/*Uno.Collections.ICollection<object>*/]), middleRectangle);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::TYPES[9/*Uno.Collections.ICollection<object>*/]), bottomRectangle);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::TYPES[9/*Uno.Collections.ICollection<object>*/]), bottomMenuTranslation);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::TYPES[9/*Uno.Collections.ICollection<object>*/]), bottomMenuRotation);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::TYPES[9/*Uno.Collections.ICollection<object>*/]), parentScrollView);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::TYPES[9/*Uno.Collections.ICollection<object>*/]), temp_eb9);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::TYPES[9/*Uno.Collections.ICollection<object>*/]), temp_eb10);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::TYPES[9/*Uno.Collections.ICollection<object>*/]), temp_eb11);
    Background(temp52);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), temp15);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), temp16);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::TYPES[3/*Uno.Collections.ICollection<Fuse.Node>*/]), EdgeNavigator);
}

// public Dashboard New(Fuse.Navigation.Router router) [static] :60
Dashboard* Dashboard::New5(::g::Fuse::Navigation::Router* router1)
{
    Dashboard* obj1 = (Dashboard*)uNew(Dashboard_typeof());
    obj1->ctor_8(router1);
    return obj1;
}
// }

} // ::g
