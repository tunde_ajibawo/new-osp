// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/Field.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.Field.h>
#include <_root.MainView.h>
#include <_root.OSP_accessor_Fi-48aea26e.h>
#include <_root.OSP_accessor_Fi-dec9cc47.h>
#include <_root.OSP_accessor_Field_Title.h>
#include <_root.OSP_FuseControl-8faf8b28.h>
#include <_root.OSP_FuseControl-b26a5bbf.h>
#include <Fuse.Controls.Text.h>
#include <Fuse.Controls.TextControl.h>
#include <Fuse.Controls.TextWrapping.h>
#include <Fuse.Elements.Alignment.h>
#include <Fuse.Elements.Element.h>
#include <Fuse.Font.h>
#include <Fuse.Layouts.Dock.h>
#include <Fuse.Reactive.BindingMode.h>
#include <Fuse.Reactive.Constan-264ec80.h>
#include <Fuse.Reactive.Constant.h>
#include <Fuse.Reactive.DataBinding.h>
#include <Fuse.Reactive.Expression.h>
#include <Fuse.Reactive.IExpression.h>
#include <Fuse.Reactive.Property.h>
#include <Uno.Bool.h>
#include <Uno.Float.h>
#include <Uno.Int.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.UX.Property.h>
#include <Uno.UX.Property1-1.h>
#include <Uno.UX.PropertyAccessor.h>
#include <Uno.UX.PropertyObject.h>
#include <Uno.UX.Selector.h>
#include <Uno.UX.Size.h>
#include <Uno.UX.Unit.h>
static uString* STRINGS[6];
static uType* TYPES[2];

namespace g{

// public partial sealed class Field :2
// {
// static Field() :53
static void Field__cctor_4_fn(uType* __type)
{
    Field::__selector0_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[0/*"Value"*/]);
    Field::__selector1_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[1/*"TextColor"*/]);
}

static void Field_build(uType* type)
{
    ::STRINGS[0] = uString::Const("Value");
    ::STRINGS[1] = uString::Const("TextColor");
    ::STRINGS[2] = uString::Const("history.ux");
    ::STRINGS[3] = uString::Const("FieldColor");
    ::STRINGS[4] = uString::Const("PlaceHolderName");
    ::STRINGS[5] = uString::Const("Title");
    ::TYPES[0] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL);
    ::TYPES[1] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL);
    type->SetDependencies(
        ::g::MainView_typeof(),
        ::g::OSP_accessor_Field_FieldColor_typeof(),
        ::g::OSP_accessor_Field_PlaceHolderName_typeof(),
        ::g::OSP_accessor_Field_Title_typeof());
    type->SetInterfaces(
        ::g::Uno::Collections::IList_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface0),
        ::g::Fuse::Scripting::IScriptObject_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface1),
        ::g::Fuse::IProperties_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface2),
        ::g::Fuse::INotifyUnrooted_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface3),
        ::g::Fuse::ISourceLocation_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface4),
        ::TYPES[0/*Uno.Collections.ICollection<Fuse.Binding>*/], offsetof(::g::Fuse::Controls::Panel_type, interface5),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface6),
        ::g::Uno::Collections::IList_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface7),
        ::g::Uno::UX::IPropertyListener_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface8),
        ::g::Fuse::ITemplateSource_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface9),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Visual_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface10),
        ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/], offsetof(::g::Fuse::Controls::Panel_type, interface11),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface12),
        ::g::Fuse::Triggers::Actions::IShow_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface13),
        ::g::Fuse::Triggers::Actions::IHide_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface14),
        ::g::Fuse::Triggers::Actions::ICollapse_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface15),
        ::g::Fuse::IActualPlacement_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface16),
        ::g::Fuse::Animations::IResize_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface17),
        ::g::Fuse::Drawing::ISurfaceDrawable_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface18));
    type->SetFields(121,
        ::g::Uno::String_typeof(), offsetof(Field, _field_PlaceHolderName), 0,
        ::g::Uno::String_typeof(), offsetof(Field, _field_Title), 0,
        ::g::Uno::Float4_typeof(), offsetof(Field, _field_FieldColor), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(Field, temp_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL), offsetof(Field, temp_TextColor_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(Field, temp1_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL), offsetof(Field, temp1_TextColor_inst), 0,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Field::__selector0_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Field::__selector1_, uFieldFlagsStatic);
    type->Reflection.SetFunctions(10,
        new uFunction("get_FieldColor", NULL, (void*)Field__get_FieldColor_fn, 0, false, ::g::Uno::Float4_typeof(), 0),
        new uFunction("set_FieldColor", NULL, (void*)Field__set_FieldColor_fn, 0, false, uVoid_typeof(), 1, ::g::Uno::Float4_typeof()),
        new uFunction(".ctor", NULL, (void*)Field__New5_fn, 0, true, type, 0),
        new uFunction("get_PlaceHolderName", NULL, (void*)Field__get_PlaceHolderName_fn, 0, false, ::g::Uno::String_typeof(), 0),
        new uFunction("set_PlaceHolderName", NULL, (void*)Field__set_PlaceHolderName_fn, 0, false, uVoid_typeof(), 1, ::g::Uno::String_typeof()),
        new uFunction("SetFieldColor", NULL, (void*)Field__SetFieldColor_fn, 0, false, uVoid_typeof(), 2, ::g::Uno::Float4_typeof(), ::g::Uno::UX::IPropertyListener_typeof()),
        new uFunction("SetPlaceHolderName", NULL, (void*)Field__SetPlaceHolderName_fn, 0, false, uVoid_typeof(), 2, ::g::Uno::String_typeof(), ::g::Uno::UX::IPropertyListener_typeof()),
        new uFunction("SetTitle", NULL, (void*)Field__SetTitle_fn, 0, false, uVoid_typeof(), 2, ::g::Uno::String_typeof(), ::g::Uno::UX::IPropertyListener_typeof()),
        new uFunction("get_Title", NULL, (void*)Field__get_Title_fn, 0, false, ::g::Uno::String_typeof(), 0),
        new uFunction("set_Title", NULL, (void*)Field__set_Title_fn, 0, false, uVoid_typeof(), 1, ::g::Uno::String_typeof()));
}

::g::Fuse::Controls::Panel_type* Field_typeof()
{
    static uSStrong< ::g::Fuse::Controls::Panel_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Fuse::Controls::DockPanel_typeof();
    options.FieldCount = 130;
    options.InterfaceCount = 19;
    options.DependencyCount = 4;
    options.ObjectSize = sizeof(Field);
    options.TypeSize = sizeof(::g::Fuse::Controls::Panel_type);
    type = (::g::Fuse::Controls::Panel_type*)uClassType::New("Field", options);
    type->fp_build_ = Field_build;
    type->fp_ctor_ = (void*)Field__New5_fn;
    type->fp_cctor_ = Field__cctor_4_fn;
    type->interface18.fp_Draw = (void(*)(uObject*, ::g::Fuse::Drawing::Surface*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableDraw_fn;
    type->interface18.fp_get_IsPrimary = (void(*)(uObject*, bool*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableget_IsPrimary_fn;
    type->interface18.fp_get_ElementSize = (void(*)(uObject*, ::g::Uno::Float2*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableget_ElementSize_fn;
    type->interface13.fp_Show = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsIShowShow_fn;
    type->interface15.fp_Collapse = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsICollapseCollapse_fn;
    type->interface14.fp_Hide = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsIHideHide_fn;
    type->interface17.fp_SetSize = (void(*)(uObject*, ::g::Uno::Float2*))::g::Fuse::Elements::Element__FuseAnimationsIResizeSetSize_fn;
    type->interface16.fp_get_ActualSize = (void(*)(uObject*, ::g::Uno::Float3*))::g::Fuse::Elements::Element__FuseIActualPlacementget_ActualSize_fn;
    type->interface16.fp_get_ActualPosition = (void(*)(uObject*, ::g::Uno::Float3*))::g::Fuse::Elements::Element__FuseIActualPlacementget_ActualPosition_fn;
    type->interface16.fp_add_Placed = (void(*)(uObject*, uDelegate*))::g::Fuse::Elements::Element__add_Placed_fn;
    type->interface16.fp_remove_Placed = (void(*)(uObject*, uDelegate*))::g::Fuse::Elements::Element__remove_Placed_fn;
    type->interface10.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Visual__UnoCollectionsIEnumerableFuseVisualGetEnumerator_fn;
    type->interface11.fp_Clear = (void(*)(uObject*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeClear_fn;
    type->interface11.fp_Contains = (void(*)(uObject*, void*, bool*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeContains_fn;
    type->interface7.fp_RemoveAt = (void(*)(uObject*, int32_t*))::g::Fuse::Visual__UnoCollectionsIListFuseNodeRemoveAt_fn;
    type->interface12.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Visual__UnoCollectionsIEnumerableFuseNodeGetEnumerator_fn;
    type->interface11.fp_get_Count = (void(*)(uObject*, int32_t*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeget_Count_fn;
    type->interface7.fp_get_Item = (void(*)(uObject*, int32_t*, uTRef))::g::Fuse::Visual__UnoCollectionsIListFuseNodeget_Item_fn;
    type->interface7.fp_Insert = (void(*)(uObject*, int32_t*, void*))::g::Fuse::Visual__Insert1_fn;
    type->interface8.fp_OnPropertyChanged = (void(*)(uObject*, ::g::Uno::UX::PropertyObject*, ::g::Uno::UX::Selector*))::g::Fuse::Controls::Control__OnPropertyChanged2_fn;
    type->interface9.fp_FindTemplate = (void(*)(uObject*, uString*, ::g::Uno::UX::Template**))::g::Fuse::Visual__FindTemplate_fn;
    type->interface11.fp_Add = (void(*)(uObject*, void*))::g::Fuse::Visual__Add1_fn;
    type->interface11.fp_Remove = (void(*)(uObject*, void*, bool*))::g::Fuse::Visual__Remove1_fn;
    type->interface5.fp_Clear = (void(*)(uObject*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingClear_fn;
    type->interface5.fp_Contains = (void(*)(uObject*, void*, bool*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingContains_fn;
    type->interface0.fp_RemoveAt = (void(*)(uObject*, int32_t*))::g::Fuse::Node__UnoCollectionsIListFuseBindingRemoveAt_fn;
    type->interface6.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Node__UnoCollectionsIEnumerableFuseBindingGetEnumerator_fn;
    type->interface1.fp_SetScriptObject = (void(*)(uObject*, uObject*, ::g::Fuse::Scripting::Context*))::g::Fuse::Node__FuseScriptingIScriptObjectSetScriptObject_fn;
    type->interface5.fp_get_Count = (void(*)(uObject*, int32_t*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingget_Count_fn;
    type->interface0.fp_get_Item = (void(*)(uObject*, int32_t*, uTRef))::g::Fuse::Node__UnoCollectionsIListFuseBindingget_Item_fn;
    type->interface1.fp_get_ScriptObject = (void(*)(uObject*, uObject**))::g::Fuse::Node__FuseScriptingIScriptObjectget_ScriptObject_fn;
    type->interface1.fp_get_ScriptContext = (void(*)(uObject*, ::g::Fuse::Scripting::Context**))::g::Fuse::Node__FuseScriptingIScriptObjectget_ScriptContext_fn;
    type->interface4.fp_get_SourceNearest = (void(*)(uObject*, uObject**))::g::Fuse::Node__FuseISourceLocationget_SourceNearest_fn;
    type->interface3.fp_add_Unrooted = (void(*)(uObject*, uDelegate*))::g::Fuse::Node__FuseINotifyUnrootedadd_Unrooted_fn;
    type->interface3.fp_remove_Unrooted = (void(*)(uObject*, uDelegate*))::g::Fuse::Node__FuseINotifyUnrootedremove_Unrooted_fn;
    type->interface0.fp_Insert = (void(*)(uObject*, int32_t*, void*))::g::Fuse::Node__Insert_fn;
    type->interface2.fp_get_Properties = (void(*)(uObject*, ::g::Fuse::Properties**))::g::Fuse::Node__get_Properties_fn;
    type->interface4.fp_get_SourceLineNumber = (void(*)(uObject*, int32_t*))::g::Fuse::Node__get_SourceLineNumber_fn;
    type->interface4.fp_get_SourceFileName = (void(*)(uObject*, uString**))::g::Fuse::Node__get_SourceFileName_fn;
    type->interface5.fp_Add = (void(*)(uObject*, void*))::g::Fuse::Node__Add_fn;
    type->interface5.fp_Remove = (void(*)(uObject*, void*, bool*))::g::Fuse::Node__Remove_fn;
    return type;
}

// public Field() :57
void Field__ctor_8_fn(Field* __this)
{
    __this->ctor_8();
}

// public float4 get_FieldColor() :38
void Field__get_FieldColor_fn(Field* __this, ::g::Uno::Float4* __retval)
{
    *__retval = __this->FieldColor();
}

// public void set_FieldColor(float4 value) :39
void Field__set_FieldColor_fn(Field* __this, ::g::Uno::Float4* value)
{
    __this->FieldColor(*value);
}

// private void InitializeUX() :61
void Field__InitializeUX_fn(Field* __this)
{
    __this->InitializeUX();
}

// public Field New() :57
void Field__New5_fn(Field** __retval)
{
    *__retval = Field::New5();
}

// public string get_PlaceHolderName() :8
void Field__get_PlaceHolderName_fn(Field* __this, uString** __retval)
{
    *__retval = __this->PlaceHolderName();
}

// public void set_PlaceHolderName(string value) :9
void Field__set_PlaceHolderName_fn(Field* __this, uString* value)
{
    __this->PlaceHolderName(value);
}

// public void SetFieldColor(float4 value, Uno.UX.IPropertyListener origin) :41
void Field__SetFieldColor_fn(Field* __this, ::g::Uno::Float4* value, uObject* origin)
{
    __this->SetFieldColor(*value, origin);
}

// public void SetPlaceHolderName(string value, Uno.UX.IPropertyListener origin) :11
void Field__SetPlaceHolderName_fn(Field* __this, uString* value, uObject* origin)
{
    __this->SetPlaceHolderName(value, origin);
}

// public void SetTitle(string value, Uno.UX.IPropertyListener origin) :26
void Field__SetTitle_fn(Field* __this, uString* value, uObject* origin)
{
    __this->SetTitle(value, origin);
}

// public string get_Title() :23
void Field__get_Title_fn(Field* __this, uString** __retval)
{
    *__retval = __this->Title();
}

// public void set_Title(string value) :24
void Field__set_Title_fn(Field* __this, uString* value)
{
    __this->Title(value);
}

::g::Uno::UX::Selector Field::__selector0_;
::g::Uno::UX::Selector Field::__selector1_;

// public Field() [instance] :57
void Field::ctor_8()
{
    uStackFrame __("Field", ".ctor()");
    ctor_7();
    InitializeUX();
}

// public float4 get_FieldColor() [instance] :38
::g::Uno::Float4 Field::FieldColor()
{
    return _field_FieldColor;
}

// public void set_FieldColor(float4 value) [instance] :39
void Field::FieldColor(::g::Uno::Float4 value)
{
    uStackFrame __("Field", "set_FieldColor(float4)");
    SetFieldColor(value, NULL);
}

// private void InitializeUX() [instance] :61
void Field::InitializeUX()
{
    uStackFrame __("Field", "InitializeUX()");
    ::g::Fuse::Reactive::Constant* temp2 = ::g::Fuse::Reactive::Constant::New1(this);
    ::g::Fuse::Controls::Text* temp = ::g::Fuse::Controls::Text::New3();
    temp_Value_inst = ::g::OSP_FuseControlsTextControl_Value_Property::New1(temp, Field::__selector0_);
    ::g::Fuse::Reactive::Property* temp3 = ::g::Fuse::Reactive::Property::New1(temp2, ::g::OSP_accessor_Field_Title::Singleton());
    ::g::Fuse::Reactive::Constant* temp4 = ::g::Fuse::Reactive::Constant::New1(this);
    temp_TextColor_inst = ::g::OSP_FuseControlsTextControl_TextColor_Property::New1(temp, Field::__selector1_);
    ::g::Fuse::Reactive::Property* temp5 = ::g::Fuse::Reactive::Property::New1(temp4, ::g::OSP_accessor_Field_FieldColor::Singleton());
    ::g::Fuse::Reactive::Constant* temp6 = ::g::Fuse::Reactive::Constant::New1(this);
    ::g::Fuse::Controls::Text* temp1 = ::g::Fuse::Controls::Text::New3();
    temp1_Value_inst = ::g::OSP_FuseControlsTextControl_Value_Property::New1(temp1, Field::__selector0_);
    ::g::Fuse::Reactive::Property* temp7 = ::g::Fuse::Reactive::Property::New1(temp6, ::g::OSP_accessor_Field_PlaceHolderName::Singleton());
    ::g::Fuse::Reactive::Constant* temp8 = ::g::Fuse::Reactive::Constant::New1(this);
    temp1_TextColor_inst = ::g::OSP_FuseControlsTextControl_TextColor_Property::New1(temp1, Field::__selector1_);
    ::g::Fuse::Reactive::Property* temp9 = ::g::Fuse::Reactive::Property::New1(temp8, ::g::OSP_accessor_Field_FieldColor::Singleton());
    ::g::Fuse::Reactive::DataBinding* temp10 = ::g::Fuse::Reactive::DataBinding::New1(temp_Value_inst, (uObject*)temp3, 3);
    ::g::Fuse::Reactive::DataBinding* temp11 = ::g::Fuse::Reactive::DataBinding::New1(temp_TextColor_inst, (uObject*)temp5, 3);
    ::g::Fuse::Reactive::DataBinding* temp12 = ::g::Fuse::Reactive::DataBinding::New1(temp1_Value_inst, (uObject*)temp7, 3);
    ::g::Fuse::Reactive::DataBinding* temp13 = ::g::Fuse::Reactive::DataBinding::New1(temp1_TextColor_inst, (uObject*)temp9, 3);
    FieldColor(::g::Uno::Float4__New2(0.0f, 0.0f, 0.0f, 1.0f));
    SourceLineNumber(84);
    SourceFileName(::STRINGS[2/*"history.ux"*/]);
    temp->TextWrapping(1);
    temp->FontSize(11.0f);
    temp->Width(::g::Uno::UX::Size__New1(30.0f, 4));
    temp->Alignment(8);
    temp->SourceLineNumber(88);
    temp->SourceFileName(::STRINGS[2/*"history.ux"*/]);
    ::g::Fuse::Controls::DockPanel::SetDock(temp, 0);
    temp->Font(::g::MainView::Raleway());
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Bindings()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp10);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Bindings()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp11);
    temp3->SourceLineNumber(88);
    temp3->SourceFileName(::STRINGS[2/*"history.ux"*/]);
    temp2->SourceLineNumber(88);
    temp2->SourceFileName(::STRINGS[2/*"history.ux"*/]);
    temp5->SourceLineNumber(88);
    temp5->SourceFileName(::STRINGS[2/*"history.ux"*/]);
    temp4->SourceLineNumber(88);
    temp4->SourceFileName(::STRINGS[2/*"history.ux"*/]);
    temp1->TextWrapping(1);
    temp1->FontSize(11.0f);
    temp1->Width(::g::Uno::UX::Size__New1(60.0f, 4));
    temp1->Alignment(8);
    temp1->Margin(::g::Uno::Float4__New2(20.0f, 0.0f, 0.0f, 0.0f));
    temp1->SourceLineNumber(90);
    temp1->SourceFileName(::STRINGS[2/*"history.ux"*/]);
    ::g::Fuse::Controls::DockPanel::SetDock(temp1, 0);
    temp1->Font(::g::MainView::Raleway());
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp1->Bindings()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp12);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp1->Bindings()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp13);
    temp7->SourceLineNumber(90);
    temp7->SourceFileName(::STRINGS[2/*"history.ux"*/]);
    temp6->SourceLineNumber(90);
    temp6->SourceFileName(::STRINGS[2/*"history.ux"*/]);
    temp9->SourceLineNumber(90);
    temp9->SourceFileName(::STRINGS[2/*"history.ux"*/]);
    temp8->SourceLineNumber(90);
    temp8->SourceFileName(::STRINGS[2/*"history.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Node>*/]), temp1);
}

// public string get_PlaceHolderName() [instance] :8
uString* Field::PlaceHolderName()
{
    return _field_PlaceHolderName;
}

// public void set_PlaceHolderName(string value) [instance] :9
void Field::PlaceHolderName(uString* value)
{
    uStackFrame __("Field", "set_PlaceHolderName(string)");
    SetPlaceHolderName(value, NULL);
}

// public void SetFieldColor(float4 value, Uno.UX.IPropertyListener origin) [instance] :41
void Field::SetFieldColor(::g::Uno::Float4 value, uObject* origin)
{
    uStackFrame __("Field", "SetFieldColor(float4,Uno.UX.IPropertyListener)");

    if (::g::Uno::Float4__op_Inequality(value, _field_FieldColor))
    {
        _field_FieldColor = value;
        OnPropertyChanged1(::g::Uno::UX::Selector__op_Implicit1(::STRINGS[3/*"FieldColor"*/]), origin);
    }
}

// public void SetPlaceHolderName(string value, Uno.UX.IPropertyListener origin) [instance] :11
void Field::SetPlaceHolderName(uString* value, uObject* origin)
{
    uStackFrame __("Field", "SetPlaceHolderName(string,Uno.UX.IPropertyListener)");

    if (::g::Uno::String::op_Inequality(value, _field_PlaceHolderName))
    {
        _field_PlaceHolderName = value;
        OnPropertyChanged1(::g::Uno::UX::Selector__op_Implicit1(::STRINGS[4/*"PlaceHolder...*/]), origin);
    }
}

// public void SetTitle(string value, Uno.UX.IPropertyListener origin) [instance] :26
void Field::SetTitle(uString* value, uObject* origin)
{
    uStackFrame __("Field", "SetTitle(string,Uno.UX.IPropertyListener)");

    if (::g::Uno::String::op_Inequality(value, _field_Title))
    {
        _field_Title = value;
        OnPropertyChanged1(::g::Uno::UX::Selector__op_Implicit1(::STRINGS[5/*"Title"*/]), origin);
    }
}

// public string get_Title() [instance] :23
uString* Field::Title()
{
    return _field_Title;
}

// public void set_Title(string value) [instance] :24
void Field::Title(uString* value)
{
    uStackFrame __("Field", "set_Title(string)");
    SetTitle(value, NULL);
}

// public Field New() [static] :57
Field* Field::New5()
{
    Field* obj1 = (Field*)uNew(Field_typeof());
    obj1->ctor_8();
    return obj1;
}
// }

} // ::g
