// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/Profile.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.InputField.h>
#include <_root.OSP_InputField_-d24e30b7.h>
#include <_root.Profile.h>
#include <_root.Profile.Template5.h>
#include <Fuse.Binding.h>
#include <Fuse.Node.h>
#include <Fuse.Reactive.BindingMode.h>
#include <Fuse.Reactive.Data.h>
#include <Fuse.Reactive.DataBinding.h>
#include <Fuse.Reactive.Expression.h>
#include <Fuse.Reactive.IExpression.h>
#include <Uno.Bool.h>
#include <Uno.Collections.ICollection-1.h>
#include <Uno.Collections.IList-1.h>
#include <Uno.Float.h>
#include <Uno.Float4.h>
#include <Uno.Int.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.UX.Property.h>
#include <Uno.UX.Property1-1.h>
#include <Uno.UX.Selector.h>
static uString* STRINGS[4];
static uType* TYPES[1];

namespace g{

// public partial sealed class Profile.Template5 :160
// {
// static Template5() :170
static void Profile__Template5__cctor__fn(uType* __type)
{
    Profile__Template5::__selector0_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[0/*"PlaceHolder...*/]);
}

static void Profile__Template5_build(uType* type)
{
    ::STRINGS[0] = uString::Const("PlaceHolderName");
    ::STRINGS[1] = uString::Const("email_address");
    ::STRINGS[2] = uString::Const("EMAIL");
    ::STRINGS[3] = uString::Const("profile.ux");
    ::TYPES[0] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL);
    type->SetFields(2,
        ::g::Profile_typeof(), offsetof(Profile__Template5, __parent1), uFieldFlagsWeak,
        ::g::Profile_typeof(), offsetof(Profile__Template5, __parentInstance1), uFieldFlagsWeak,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(Profile__Template5, __self_PlaceHolderName_inst1), 0,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&Profile__Template5::__selector0_, uFieldFlagsStatic);
    type->Reflection.SetFunctions(1,
        new uFunction(".ctor", NULL, (void*)Profile__Template5__New2_fn, 0, true, type, 2, ::g::Profile_typeof(), ::g::Profile_typeof()));
}

::g::Uno::UX::Template_type* Profile__Template5_typeof()
{
    static uSStrong< ::g::Uno::UX::Template_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::Template_typeof();
    options.FieldCount = 6;
    options.ObjectSize = sizeof(Profile__Template5);
    options.TypeSize = sizeof(::g::Uno::UX::Template_type);
    type = (::g::Uno::UX::Template_type*)uClassType::New("Profile.Template5", options);
    type->fp_build_ = Profile__Template5_build;
    type->fp_cctor_ = Profile__Template5__cctor__fn;
    type->fp_New1 = (void(*)(::g::Uno::UX::Template*, uObject**))Profile__Template5__New1_fn;
    return type;
}

// public Template5(Profile parent, Profile parentInstance) :164
void Profile__Template5__ctor_1_fn(Profile__Template5* __this, ::g::Profile* parent, ::g::Profile* parentInstance)
{
    __this->ctor_1(parent, parentInstance);
}

// public override sealed object New() :173
void Profile__Template5__New1_fn(Profile__Template5* __this, uObject** __retval)
{
    uStackFrame __("Profile.Template5", "New()");
    ::g::InputField* __self1 = ::g::InputField::New4();
    __this->__self_PlaceHolderName_inst1 = ::g::OSP_InputField_PlaceHolderName_Property::New1(__self1, Profile__Template5::__selector0_);
    ::g::Fuse::Reactive::Data* temp = ::g::Fuse::Reactive::Data::New1(::STRINGS[1/*"email_address"*/]);
    ::g::Fuse::Reactive::DataBinding* temp1 = ::g::Fuse::Reactive::DataBinding::New1(__this->__self_PlaceHolderName_inst1, (uObject*)temp, 3);
    __self1->Title(::STRINGS[2/*"EMAIL"*/]);
    __self1->BorderColor(::g::Uno::Float4__New2(0.8352941f, 0.8352941f, 0.8352941f, 1.0f));
    __self1->SourceLineNumber(169);
    __self1->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    temp->SourceLineNumber(169);
    temp->SourceFileName(::STRINGS[3/*"profile.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(__self1->Bindings()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp1);
    return *__retval = __self1, void();
}

// public Template5 New(Profile parent, Profile parentInstance) :164
void Profile__Template5__New2_fn(::g::Profile* parent, ::g::Profile* parentInstance, Profile__Template5** __retval)
{
    *__retval = Profile__Template5::New2(parent, parentInstance);
}

::g::Uno::UX::Selector Profile__Template5::__selector0_;

// public Template5(Profile parent, Profile parentInstance) [instance] :164
void Profile__Template5::ctor_1(::g::Profile* parent, ::g::Profile* parentInstance)
{
    ctor_(NULL, false);
    __parent1 = parent;
    __parentInstance1 = parentInstance;
}

// public Template5 New(Profile parent, Profile parentInstance) [static] :164
Profile__Template5* Profile__Template5::New2(::g::Profile* parent, ::g::Profile* parentInstance)
{
    Profile__Template5* obj1 = (Profile__Template5*)uNew(Profile__Template5_typeof());
    obj1->ctor_1(parent, parentInstance);
    return obj1;
}
// }

} // ::g
