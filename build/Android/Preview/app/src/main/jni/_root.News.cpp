// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/News.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.MainView.h>
#include <_root.News.h>
#include <_root.News.Template.h>
#include <_root.OSP_bundle.h>
#include <_root.OSP_FuseControl-b26a5bbf.h>
#include <_root.OSP_FuseElement-78dfea6e.h>
#include <_root.OSP_FuseReactiv-f855d0e4.h>
#include <_root.OSP_FuseTrigger-1375289b.h>
#include <_root.SubBut.h>
#include <_root.TopBar.h>
#include <Fuse.Animations.Animator.h>
#include <Fuse.Animations.Change-1.h>
#include <Fuse.Animations.Spin.h>
#include <Fuse.Animations.TrackAnimator.h>
#include <Fuse.Controls.ClientPanel.h>
#include <Fuse.Controls.DockPanel.h>
#include <Fuse.Controls.Grid.h>
#include <Fuse.Controls.Image.h>
#include <Fuse.Controls.Panel.h>
#include <Fuse.Controls.ScrollView.h>
#include <Fuse.Controls.StackPanel.h>
#include <Fuse.Controls.Text.h>
#include <Fuse.Controls.TextAlignment.h>
#include <Fuse.Controls.TextControl.h>
#include <Fuse.Elements.Alignment.h>
#include <Fuse.Elements.Element.h>
#include <Fuse.Font.h>
#include <Fuse.Gestures.Clicked.h>
#include <Fuse.Gestures.ClickedHandler.h>
#include <Fuse.Layouts.Dock.h>
#include <Fuse.Navigation.Activated.h>
#include <Fuse.Navigation.Router.h>
#include <Fuse.Reactive.BindingMode.h>
#include <Fuse.Reactive.Data.h>
#include <Fuse.Reactive.DataBinding.h>
#include <Fuse.Reactive.Each.h>
#include <Fuse.Reactive.EventBinding.h>
#include <Fuse.Reactive.Expression.h>
#include <Fuse.Reactive.IExpression.h>
#include <Fuse.Reactive.Instantiator.h>
#include <Fuse.Reactive.JavaScript.h>
#include <Fuse.Triggers.Busy.h>
#include <Fuse.Triggers.PulseTr-e6f97a32.h>
#include <Fuse.Triggers.PulseTrigger-1.h>
#include <Fuse.Triggers.Trigger.h>
#include <Fuse.Triggers.WhileBool.h>
#include <Fuse.Triggers.WhileBusy.h>
#include <Fuse.Triggers.WhileFalse.h>
#include <Fuse.Triggers.WhileTrue.h>
#include <Uno.Bool.h>
#include <Uno.Double.h>
#include <Uno.EventArgs.h>
#include <Uno.Float.h>
#include <Uno.Float4.h>
#include <Uno.Int.h>
#include <Uno.IO.BundleFile.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.UX.BundleFileSource.h>
#include <Uno.UX.FileSource.h>
#include <Uno.UX.NameTable.h>
#include <Uno.UX.Property.h>
#include <Uno.UX.Property1-1.h>
#include <Uno.UX.Selector.h>
#include <Uno.UX.Size.h>
#include <Uno.UX.Template.h>
#include <Uno.UX.Unit.h>

namespace g{

// public partial sealed class News :2
// {
// static News() :54
static void News__cctor_4_fn(uType* __type)
{
    News::__g_static_nametable1_ = uArray::Init<uString*>(::g::Uno::String_typeof()->Array(), 9, uString::Const("router"), uString::Const("NewPage"), uString::Const("temp_eb19"), uString::Const("busy"), uString::Const("loadingPanel"), uString::Const("load"), uString::Const("errorMsg"), uString::Const("temp_eb20"), uString::Const("temp_eb21"));
    News::__selector0_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("Opacity"));
    News::__selector1_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("Items"));
    News::__selector2_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("Value"));
    News::__selector3_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("NewPage"));
    News::__selector4_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("busy"));
    News::__selector5_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("loadingPanel"));
    News::__selector6_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("load"));
    News::__selector7_ = ::g::Uno::UX::Selector__op_Implicit1(uString::Const("errorMsg"));
}

static void News_build(uType* type)
{
    type->SetDependencies(
        ::g::MainView_typeof(),
        ::g::OSP_bundle_typeof());
    type->SetInterfaces(
        ::g::Uno::Collections::IList_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface0),
        ::g::Fuse::Scripting::IScriptObject_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface1),
        ::g::Fuse::IProperties_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface2),
        ::g::Fuse::INotifyUnrooted_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface3),
        ::g::Fuse::ISourceLocation_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface4),
        ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface5),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface6),
        ::g::Uno::Collections::IList_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface7),
        ::g::Uno::UX::IPropertyListener_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface8),
        ::g::Fuse::ITemplateSource_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface9),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Visual_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface10),
        ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface11),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::Panel_type, interface12),
        ::g::Fuse::Triggers::Actions::IShow_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface13),
        ::g::Fuse::Triggers::Actions::IHide_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface14),
        ::g::Fuse::Triggers::Actions::ICollapse_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface15),
        ::g::Fuse::IActualPlacement_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface16),
        ::g::Fuse::Animations::IResize_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface17),
        ::g::Fuse::Drawing::ISurfaceDrawable_typeof(), offsetof(::g::Fuse::Controls::Panel_type, interface18));
    type->SetFields(126,
        ::g::Fuse::Navigation::Router_typeof(), offsetof(News, router), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(News, loadingPanel_Opacity_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(News, NewPage_Opacity_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(uObject_typeof(), NULL), offsetof(News, temp_Items_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(News, temp1_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(News, load_Opacity_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), offsetof(News, errorMsg_Opacity_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Bool_typeof(), NULL), offsetof(News, temp2_Value_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Bool_typeof(), NULL), offsetof(News, temp3_Value_inst), 0,
        ::g::Fuse::Controls::Grid_typeof(), offsetof(News, NewPage), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(News, temp_eb19), 0,
        ::g::Fuse::Triggers::Busy_typeof(), offsetof(News, busy), 0,
        ::g::Fuse::Controls::ClientPanel_typeof(), offsetof(News, loadingPanel), 0,
        ::g::Fuse::Controls::Image_typeof(), offsetof(News, load), 0,
        ::g::Fuse::Controls::StackPanel_typeof(), offsetof(News, errorMsg), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(News, temp_eb20), 0,
        ::g::Fuse::Reactive::EventBinding_typeof(), offsetof(News, temp_eb21), 0,
        ::g::Uno::UX::NameTable_typeof(), offsetof(News, __g_nametable1), 0,
        ::g::Uno::String_typeof()->Array(), (uintptr_t)&News::__g_static_nametable1_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&News::__selector0_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&News::__selector1_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&News::__selector2_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&News::__selector3_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&News::__selector4_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&News::__selector5_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&News::__selector6_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&News::__selector7_, uFieldFlagsStatic);
    type->Reflection.SetFunctions(1,
        new uFunction(".ctor", NULL, (void*)News__New5_fn, 0, true, type, 1, ::g::Fuse::Navigation::Router_typeof()));
}

::g::Fuse::Controls::Panel_type* News_typeof()
{
    static uSStrong< ::g::Fuse::Controls::Panel_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Fuse::Controls::Page_typeof();
    options.FieldCount = 153;
    options.InterfaceCount = 19;
    options.DependencyCount = 2;
    options.ObjectSize = sizeof(News);
    options.TypeSize = sizeof(::g::Fuse::Controls::Panel_type);
    type = (::g::Fuse::Controls::Panel_type*)uClassType::New("News", options);
    type->fp_build_ = News_build;
    type->fp_cctor_ = News__cctor_4_fn;
    type->interface18.fp_Draw = (void(*)(uObject*, ::g::Fuse::Drawing::Surface*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableDraw_fn;
    type->interface18.fp_get_IsPrimary = (void(*)(uObject*, bool*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableget_IsPrimary_fn;
    type->interface18.fp_get_ElementSize = (void(*)(uObject*, ::g::Uno::Float2*))::g::Fuse::Controls::Panel__FuseDrawingISurfaceDrawableget_ElementSize_fn;
    type->interface13.fp_Show = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsIShowShow_fn;
    type->interface15.fp_Collapse = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsICollapseCollapse_fn;
    type->interface14.fp_Hide = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsIHideHide_fn;
    type->interface17.fp_SetSize = (void(*)(uObject*, ::g::Uno::Float2*))::g::Fuse::Elements::Element__FuseAnimationsIResizeSetSize_fn;
    type->interface16.fp_get_ActualSize = (void(*)(uObject*, ::g::Uno::Float3*))::g::Fuse::Elements::Element__FuseIActualPlacementget_ActualSize_fn;
    type->interface16.fp_get_ActualPosition = (void(*)(uObject*, ::g::Uno::Float3*))::g::Fuse::Elements::Element__FuseIActualPlacementget_ActualPosition_fn;
    type->interface16.fp_add_Placed = (void(*)(uObject*, uDelegate*))::g::Fuse::Elements::Element__add_Placed_fn;
    type->interface16.fp_remove_Placed = (void(*)(uObject*, uDelegate*))::g::Fuse::Elements::Element__remove_Placed_fn;
    type->interface10.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Visual__UnoCollectionsIEnumerableFuseVisualGetEnumerator_fn;
    type->interface11.fp_Clear = (void(*)(uObject*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeClear_fn;
    type->interface11.fp_Contains = (void(*)(uObject*, void*, bool*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeContains_fn;
    type->interface7.fp_RemoveAt = (void(*)(uObject*, int32_t*))::g::Fuse::Visual__UnoCollectionsIListFuseNodeRemoveAt_fn;
    type->interface12.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Visual__UnoCollectionsIEnumerableFuseNodeGetEnumerator_fn;
    type->interface11.fp_get_Count = (void(*)(uObject*, int32_t*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeget_Count_fn;
    type->interface7.fp_get_Item = (void(*)(uObject*, int32_t*, uTRef))::g::Fuse::Visual__UnoCollectionsIListFuseNodeget_Item_fn;
    type->interface7.fp_Insert = (void(*)(uObject*, int32_t*, void*))::g::Fuse::Visual__Insert1_fn;
    type->interface8.fp_OnPropertyChanged = (void(*)(uObject*, ::g::Uno::UX::PropertyObject*, ::g::Uno::UX::Selector*))::g::Fuse::Controls::Control__OnPropertyChanged2_fn;
    type->interface9.fp_FindTemplate = (void(*)(uObject*, uString*, ::g::Uno::UX::Template**))::g::Fuse::Visual__FindTemplate_fn;
    type->interface11.fp_Add = (void(*)(uObject*, void*))::g::Fuse::Visual__Add1_fn;
    type->interface11.fp_Remove = (void(*)(uObject*, void*, bool*))::g::Fuse::Visual__Remove1_fn;
    type->interface5.fp_Clear = (void(*)(uObject*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingClear_fn;
    type->interface5.fp_Contains = (void(*)(uObject*, void*, bool*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingContains_fn;
    type->interface0.fp_RemoveAt = (void(*)(uObject*, int32_t*))::g::Fuse::Node__UnoCollectionsIListFuseBindingRemoveAt_fn;
    type->interface6.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Node__UnoCollectionsIEnumerableFuseBindingGetEnumerator_fn;
    type->interface1.fp_SetScriptObject = (void(*)(uObject*, uObject*, ::g::Fuse::Scripting::Context*))::g::Fuse::Node__FuseScriptingIScriptObjectSetScriptObject_fn;
    type->interface5.fp_get_Count = (void(*)(uObject*, int32_t*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingget_Count_fn;
    type->interface0.fp_get_Item = (void(*)(uObject*, int32_t*, uTRef))::g::Fuse::Node__UnoCollectionsIListFuseBindingget_Item_fn;
    type->interface1.fp_get_ScriptObject = (void(*)(uObject*, uObject**))::g::Fuse::Node__FuseScriptingIScriptObjectget_ScriptObject_fn;
    type->interface1.fp_get_ScriptContext = (void(*)(uObject*, ::g::Fuse::Scripting::Context**))::g::Fuse::Node__FuseScriptingIScriptObjectget_ScriptContext_fn;
    type->interface4.fp_get_SourceNearest = (void(*)(uObject*, uObject**))::g::Fuse::Node__FuseISourceLocationget_SourceNearest_fn;
    type->interface3.fp_add_Unrooted = (void(*)(uObject*, uDelegate*))::g::Fuse::Node__FuseINotifyUnrootedadd_Unrooted_fn;
    type->interface3.fp_remove_Unrooted = (void(*)(uObject*, uDelegate*))::g::Fuse::Node__FuseINotifyUnrootedremove_Unrooted_fn;
    type->interface0.fp_Insert = (void(*)(uObject*, int32_t*, void*))::g::Fuse::Node__Insert_fn;
    type->interface2.fp_get_Properties = (void(*)(uObject*, ::g::Fuse::Properties**))::g::Fuse::Node__get_Properties_fn;
    type->interface4.fp_get_SourceLineNumber = (void(*)(uObject*, int32_t*))::g::Fuse::Node__get_SourceLineNumber_fn;
    type->interface4.fp_get_SourceFileName = (void(*)(uObject*, uString**))::g::Fuse::Node__get_SourceFileName_fn;
    type->interface5.fp_Add = (void(*)(uObject*, void*))::g::Fuse::Node__Add_fn;
    type->interface5.fp_Remove = (void(*)(uObject*, void*, bool*))::g::Fuse::Node__Remove_fn;
    return type;
}

// public News(Fuse.Navigation.Router router) :58
void News__ctor_8_fn(News* __this, ::g::Fuse::Navigation::Router* router1)
{
    __this->ctor_8(router1);
}

// private void InitializeUX() :64
void News__InitializeUX_fn(News* __this)
{
    __this->InitializeUX();
}

// public News New(Fuse.Navigation.Router router) :58
void News__New5_fn(::g::Fuse::Navigation::Router* router1, News** __retval)
{
    *__retval = News::New5(router1);
}

uSStrong<uArray*> News::__g_static_nametable1_;
::g::Uno::UX::Selector News::__selector0_;
::g::Uno::UX::Selector News::__selector1_;
::g::Uno::UX::Selector News::__selector2_;
::g::Uno::UX::Selector News::__selector3_;
::g::Uno::UX::Selector News::__selector4_;
::g::Uno::UX::Selector News::__selector5_;
::g::Uno::UX::Selector News::__selector6_;
::g::Uno::UX::Selector News::__selector7_;

// public News(Fuse.Navigation.Router router) [instance] :58
void News::ctor_8(::g::Fuse::Navigation::Router* router1)
{
    uStackFrame __("News", ".ctor(Fuse.Navigation.Router)");
    ctor_7();
    router = router1;
    InitializeUX();
}

// private void InitializeUX() [instance] :64
void News::InitializeUX()
{
    uStackFrame __("News", "InitializeUX()");
    __g_nametable1 = ::g::Uno::UX::NameTable::New1(NULL, News::__g_static_nametable1_);
    loadingPanel = ::g::Fuse::Controls::ClientPanel::New5();
    loadingPanel_Opacity_inst = ::g::OSP_FuseElementsElement_Opacity_Property::New1(loadingPanel, News::__selector0_);
    NewPage = ::g::Fuse::Controls::Grid::New4();
    NewPage_Opacity_inst = ::g::OSP_FuseElementsElement_Opacity_Property::New1(NewPage, News::__selector0_);
    ::g::Fuse::Reactive::Data* temp4 = ::g::Fuse::Reactive::Data::New1(uString::Const("startLoad"));
    ::g::Fuse::Reactive::Each* temp = ::g::Fuse::Reactive::Each::New4();
    temp_Items_inst = ::g::OSP_FuseReactiveEach_Items_Property::New1(temp, News::__selector1_);
    ::g::Fuse::Reactive::Data* temp5 = ::g::Fuse::Reactive::Data::New1(uString::Const("data"));
    ::g::Fuse::Controls::Text* temp1 = ::g::Fuse::Controls::Text::New3();
    temp1_Value_inst = ::g::OSP_FuseControlsTextControl_Value_Property::New1(temp1, News::__selector2_);
    ::g::Fuse::Reactive::Data* temp6 = ::g::Fuse::Reactive::Data::New1(uString::Const("Msg"));
    ::g::Fuse::Reactive::Data* temp7 = ::g::Fuse::Reactive::Data::New1(uString::Const("startLoad"));
    ::g::Fuse::Reactive::Data* temp8 = ::g::Fuse::Reactive::Data::New1(uString::Const("dashboard"));
    load = ::g::Fuse::Controls::Image::New3();
    load_Opacity_inst = ::g::OSP_FuseElementsElement_Opacity_Property::New1(load, News::__selector0_);
    errorMsg = ::g::Fuse::Controls::StackPanel::New4();
    errorMsg_Opacity_inst = ::g::OSP_FuseElementsElement_Opacity_Property::New1(errorMsg, News::__selector0_);
    ::g::Fuse::Triggers::WhileTrue* temp2 = ::g::Fuse::Triggers::WhileTrue::New2();
    temp2_Value_inst = ::g::OSP_FuseTriggersWhileBool_Value_Property::New1(temp2, News::__selector2_);
    ::g::Fuse::Reactive::Data* temp9 = ::g::Fuse::Reactive::Data::New1(uString::Const("error"));
    ::g::Fuse::Triggers::WhileFalse* temp3 = ::g::Fuse::Triggers::WhileFalse::New2();
    temp3_Value_inst = ::g::OSP_FuseTriggersWhileBool_Value_Property::New1(temp3, News::__selector2_);
    ::g::Fuse::Reactive::Data* temp10 = ::g::Fuse::Reactive::Data::New1(uString::Const("error"));
    ::g::Fuse::Reactive::JavaScript* temp11 = ::g::Fuse::Reactive::JavaScript::New2(__g_nametable1);
    ::g::Fuse::Reactive::JavaScript* temp12 = ::g::Fuse::Reactive::JavaScript::New2(__g_nametable1);
    ::g::Fuse::Triggers::WhileBusy* temp13 = ::g::Fuse::Triggers::WhileBusy::New2();
    ::g::Fuse::Animations::Change* temp14 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), loadingPanel_Opacity_inst);
    ::g::Fuse::Animations::Change* temp15 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), NewPage_Opacity_inst);
    ::g::Fuse::Navigation::Activated* temp16 = ::g::Fuse::Navigation::Activated::New2();
    temp_eb19 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp4);
    busy = ::g::Fuse::Triggers::Busy::New2();
    ::g::TopBar* temp17 = ::g::TopBar::New5();
    ::g::Fuse::Controls::DockPanel* temp18 = ::g::Fuse::Controls::DockPanel::New4();
    ::g::Fuse::Controls::ScrollView* temp19 = ::g::Fuse::Controls::ScrollView::New4();
    ::g::Fuse::Controls::StackPanel* temp20 = ::g::Fuse::Controls::StackPanel::New4();
    ::g::Fuse::Controls::Panel* temp21 = ::g::Fuse::Controls::Panel::New3();
    News__Template* temp22 = News__Template::New2(this, this);
    ::g::Fuse::Reactive::DataBinding* temp23 = ::g::Fuse::Reactive::DataBinding::New1(temp_Items_inst, (uObject*)temp5, 3);
    ::g::Fuse::Reactive::DataBinding* temp24 = ::g::Fuse::Reactive::DataBinding::New1(temp1_Value_inst, (uObject*)temp6, 3);
    ::g::Fuse::Controls::DockPanel* temp25 = ::g::Fuse::Controls::DockPanel::New4();
    ::g::SubBut* temp26 = ::g::SubBut::New6();
    temp_eb20 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp7);
    ::g::SubBut* temp27 = ::g::SubBut::New6();
    temp_eb21 = ::g::Fuse::Reactive::EventBinding::New1((uObject*)temp8);
    ::g::Fuse::Animations::Change* temp28 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), load_Opacity_inst);
    ::g::Fuse::Animations::Change* temp29 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), errorMsg_Opacity_inst);
    ::g::Fuse::Reactive::DataBinding* temp30 = ::g::Fuse::Reactive::DataBinding::New1(temp2_Value_inst, (uObject*)temp9, 3);
    ::g::Fuse::Animations::Change* temp31 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), load_Opacity_inst);
    ::g::Fuse::Animations::Spin* temp32 = ::g::Fuse::Animations::Spin::New2();
    ::g::Fuse::Animations::Change* temp33 = (::g::Fuse::Animations::Change*)::g::Fuse::Animations::Change::New2(::g::Fuse::Animations::Change_typeof()->MakeType(::g::Uno::Float_typeof(), NULL), errorMsg_Opacity_inst);
    ::g::Fuse::Reactive::DataBinding* temp34 = ::g::Fuse::Reactive::DataBinding::New1(temp3_Value_inst, (uObject*)temp10, 3);
    Color(::g::Uno::Float4__New2(1.0f, 1.0f, 1.0f, 1.0f));
    SourceLineNumber(1);
    SourceFileName(uString::Const("news.ux"));
    temp11->LineNumber(3);
    temp11->FileName(uString::Const("news.ux"));
    temp11->SourceLineNumber(3);
    temp11->SourceFileName(uString::Const("news.ux"));
    temp11->File(::g::Uno::UX::BundleFileSource::New1(::g::OSP_bundle::navigationb176862e()));
    temp12->Code(uString::Const("\n"
        "\t\tvar Observable = require(\"FuseJS/Observable\");\n"
        "\t\tvar data = Observable();\n"
        "\t\tvar error = Observable(false);\n"
        "\t\tvar Msg = Observable(\"There was an error\");\n"
        "\n"
        "\t\tfunction startLoad() {\n"
        "\t\t\tbusy.activate();\n"
        "\t\t\tfetch('http://az664292.vo.msecnd.net/files/P6FteBeij9A7jTXL-edgenavresponse.json')\n"
        "\t\t\t.then(function(response) { return response.json(); })\n"
        "\t\t\t.then(function (responseObject) {\n"
        "\t\t\t\tdata.replaceAll(responseObject.responseData);\n"
        "\t\t\t\t// console.log(\"it works: \"+JSON.stringify(responseObject));\n"
        "\t\t\t\t// console.log(error.value);\n"
        "\t\t\t\t busy.deactivate();\n"
        "\t\t\t}).catch(function(err) {\n"
        "\t\t\t\tconsole.log(\"There was an error\");\n"
        "\t\t\t\t // busy.deactivate();\n"
        "\t\t\t\t error.value = true;\n"
        "\t\t\t});\n"
        "\t\t}\n"
        "\n"
        "\t\t\t// fetch('http://az664292.vo.msecnd.net/files/P6FteBeij9A7jTXL-edgenavresponse.json')\n"
        "\t\t\t// .then(function(response) { return response.json(); })\n"
        "\t\t\t// .then(function(responseObject) { data.value = responseObject; });\n"
        "\t\t\n"
        "\t\tmodule.exports = {\n"
        "\t\t\tdata: data,\n"
        "\t\t\tstartLoad: startLoad,\n"
        "\t\t\terror: error,\n"
        "\t\t\tMsg: Msg\n"
        "\t\t};\n"
        "\n"
        "\t"));
    temp12->LineNumber(4);
    temp12->FileName(uString::Const("news.ux"));
    temp12->SourceLineNumber(4);
    temp12->SourceFileName(uString::Const("news.ux"));
    uPtr(NewPage)->Rows(uString::Const("1*,15*"));
    uPtr(NewPage)->Opacity(1.0f);
    uPtr(NewPage)->Name(News::__selector3_);
    uPtr(NewPage)->SourceLineNumber(39);
    uPtr(NewPage)->SourceFileName(uString::Const("news.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(NewPage)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp13);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(NewPage)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp16);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(NewPage)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), busy);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(NewPage)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp17);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(NewPage)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp18);
    temp13->SourceLineNumber(41);
    temp13->SourceFileName(uString::Const("news.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp13->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp14);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp13->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp15);
    ::g::Fuse::Animations::Change__set_Value_fn(temp14, uCRef(1.0f));
    temp14->Duration(0.5);
    ::g::Fuse::Animations::Change__set_Value_fn(temp15, uCRef(0.0f));
    temp16->SourceLineNumber(46);
    temp16->SourceFileName(uString::Const("news.ux"));
    temp16->add_Handler(uDelegate::New(::g::Fuse::Triggers::PulseTrigger__PulseHandler_typeof()->MakeType(::g::Uno::EventArgs_typeof(), NULL), (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb19)));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp16->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp_eb19);
    temp4->SourceLineNumber(46);
    temp4->SourceFileName(uString::Const("news.ux"));
    uPtr(busy)->IsActive(false);
    uPtr(busy)->Name(News::__selector4_);
    uPtr(busy)->SourceLineNumber(48);
    uPtr(busy)->SourceFileName(uString::Const("news.ux"));
    temp17->Title(uString::Const("NEWS"));
    temp17->SourceLineNumber(50);
    temp17->SourceFileName(uString::Const("news.ux"));
    temp18->Padding(::g::Uno::Float4__New2(0.0f, 20.0f, 0.0f, 0.0f));
    temp18->SourceLineNumber(52);
    temp18->SourceFileName(uString::Const("news.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp18->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp19);
    temp19->SourceLineNumber(54);
    temp19->SourceFileName(uString::Const("news.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp19->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp20);
    temp20->Alignment(4);
    temp20->SourceLineNumber(55);
    temp20->SourceFileName(uString::Const("news.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp20->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp21);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp20->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp);
    temp21->Height(::g::Uno::UX::Size__New1(7.0f, 1));
    temp21->SourceLineNumber(56);
    temp21->SourceFileName(uString::Const("news.ux"));
    temp->SourceLineNumber(57);
    temp->SourceFileName(uString::Const("news.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Templates()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Uno::UX::Template_typeof(), NULL)), temp22);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp23);
    temp5->SourceLineNumber(57);
    temp5->SourceFileName(uString::Const("news.ux"));
    uPtr(loadingPanel)->Color(::g::Uno::Float4__New2(1.0f, 1.0f, 1.0f, 1.0f));
    uPtr(loadingPanel)->Width(::g::Uno::UX::Size__New1(100.0f, 4));
    uPtr(loadingPanel)->Height(::g::Uno::UX::Size__New1(100.0f, 4));
    uPtr(loadingPanel)->Alignment(4);
    uPtr(loadingPanel)->Opacity(0.0f);
    uPtr(loadingPanel)->Name(News::__selector5_);
    uPtr(loadingPanel)->SourceLineNumber(67);
    uPtr(loadingPanel)->SourceFileName(uString::Const("news.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(loadingPanel)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), load);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(loadingPanel)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), errorMsg);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(loadingPanel)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp2);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(loadingPanel)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp3);
    uPtr(load)->Width(::g::Uno::UX::Size__New1(50.0f, 1));
    uPtr(load)->Height(::g::Uno::UX::Size__New1(50.0f, 1));
    uPtr(load)->Alignment(10);
    uPtr(load)->Opacity(1.0f);
    uPtr(load)->Name(News::__selector6_);
    uPtr(load)->SourceLineNumber(69);
    uPtr(load)->SourceFileName(uString::Const("news.ux"));
    uPtr(load)->File(::g::Uno::UX::BundleFileSource::New1(::g::OSP_bundle::diskc6e80153()));
    uPtr(errorMsg)->Alignment(10);
    uPtr(errorMsg)->Opacity(0.0f);
    uPtr(errorMsg)->Name(News::__selector7_);
    uPtr(errorMsg)->SourceLineNumber(70);
    uPtr(errorMsg)->SourceFileName(uString::Const("news.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(errorMsg)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp1);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(errorMsg)->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp25);
    temp1->TextAlignment(1);
    temp1->Color(::g::Uno::Float4__New2(0.8f, 0.0f, 0.0f, 1.0f));
    temp1->Alignment(10);
    temp1->SourceLineNumber(71);
    temp1->SourceFileName(uString::Const("news.ux"));
    temp1->Font(::g::MainView::Raleway());
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp1->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp24);
    temp6->SourceLineNumber(71);
    temp6->SourceFileName(uString::Const("news.ux"));
    temp25->Width(::g::Uno::UX::Size__New1(180.0f, 1));
    temp25->Margin(::g::Uno::Float4__New2(0.0f, 10.0f, 0.0f, 10.0f));
    temp25->SourceLineNumber(72);
    temp25->SourceFileName(uString::Const("news.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp25->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp26);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp25->Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp27);
    temp26->Btext(uString::Const("Retry"));
    temp26->Icon(uString::Const("\357\200\241"));
    temp26->IconColor(::g::Uno::Float4__New2(0.3333333f, 1.0f, 0.4980392f, 1.0f));
    temp26->SourceLineNumber(73);
    temp26->SourceFileName(uString::Const("news.ux"));
    ::g::Fuse::Controls::DockPanel::SetDock(temp26, 0);
    ::g::Fuse::Gestures::Clicked::AddHandler(temp26, uDelegate::New(::g::Fuse::Gestures::ClickedHandler_typeof(), (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb20)));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp26->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp_eb20);
    temp7->SourceLineNumber(73);
    temp7->SourceFileName(uString::Const("news.ux"));
    temp27->Btext(uString::Const("Cancel"));
    temp27->Icon(uString::Const("\357\200\215"));
    temp27->IconColor(::g::Uno::Float4__New2(0.8f, 0.0f, 0.0f, 1.0f));
    temp27->SourceLineNumber(74);
    temp27->SourceFileName(uString::Const("news.ux"));
    ::g::Fuse::Controls::DockPanel::SetDock(temp27, 1);
    ::g::Fuse::Gestures::Clicked::AddHandler(temp27, uDelegate::New(::g::Fuse::Gestures::ClickedHandler_typeof(), (void*)::g::Fuse::Reactive::EventBinding__OnEvent_fn, uPtr(temp_eb21)));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp27->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp_eb21);
    temp8->SourceLineNumber(74);
    temp8->SourceFileName(uString::Const("news.ux"));
    temp2->SourceLineNumber(80);
    temp2->SourceFileName(uString::Const("news.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp2->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp28);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp2->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp29);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp2->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp30);
    ::g::Fuse::Animations::Change__set_Value_fn(temp28, uCRef(0.0f));
    temp28->Duration(0.5);
    ::g::Fuse::Animations::Change__set_Value_fn(temp29, uCRef(1.0f));
    temp29->Duration(0.5);
    temp9->SourceLineNumber(80);
    temp9->SourceFileName(uString::Const("news.ux"));
    temp3->SourceLineNumber(85);
    temp3->SourceFileName(uString::Const("news.ux"));
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp3->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp31);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp3->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp32);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp3->Animators()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Animations::Animator_typeof(), NULL)), temp33);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp3->Bindings()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL)), temp34);
    ::g::Fuse::Animations::Change__set_Value_fn(temp31, uCRef(1.0f));
    temp31->Duration(0.5);
    temp32->Frequency(1.0);
    temp32->Target(load);
    ::g::Fuse::Animations::Change__set_Value_fn(temp33, uCRef(0.0f));
    temp33->Duration(0.5);
    temp10->SourceLineNumber(85);
    temp10->SourceFileName(uString::Const("news.ux"));
    uPtr(__g_nametable1)->This(this);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), router);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), NewPage);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), temp_eb19);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), busy);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), loadingPanel);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), load);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), errorMsg);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), temp_eb20);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(uPtr(__g_nametable1)->Objects()), ::g::Uno::Collections::ICollection_typeof()->MakeType(uObject_typeof(), NULL)), temp_eb21);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp11);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), temp12);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), NewPage);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL)), loadingPanel);
}

// public News New(Fuse.Navigation.Router router) [static] :58
News* News::New5(::g::Fuse::Navigation::Router* router1)
{
    News* obj1 = (News*)uNew(News_typeof());
    obj1->ctor_8(router1);
    return obj1;
}
// }

} // ::g
