// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.DropdownOption.h>
#include <_root.OSP_accessor_Dr-303d8156.h>
#include <Fuse.Drawing.Brush.h>
#include <Uno.Bool.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.Type.h>
#include <Uno.UX.IPropertyListener.h>
#include <Uno.UX.PropertyObject.h>
#include <Uno.UX.Selector.h>
static uString* STRINGS[1];
static uType* TYPES[3];

namespace g{

// internal sealed class OSP_accessor_DropdownOption_BorderColor :31
// {
// static generated OSP_accessor_DropdownOption_BorderColor() :31
static void OSP_accessor_DropdownOption_BorderColor__cctor__fn(uType* __type)
{
    OSP_accessor_DropdownOption_BorderColor::Singleton_ = OSP_accessor_DropdownOption_BorderColor::New1();
    OSP_accessor_DropdownOption_BorderColor::_name_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[0/*"BorderColor"*/]);
}

static void OSP_accessor_DropdownOption_BorderColor_build(uType* type)
{
    ::STRINGS[0] = uString::Const("BorderColor");
    ::TYPES[0] = ::g::DropdownOption_typeof();
    ::TYPES[1] = ::g::Fuse::Drawing::Brush_typeof();
    ::TYPES[2] = ::g::Uno::Type_typeof();
    type->SetFields(0,
        ::g::Uno::UX::PropertyAccessor_typeof(), (uintptr_t)&OSP_accessor_DropdownOption_BorderColor::Singleton_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&OSP_accessor_DropdownOption_BorderColor::_name_, uFieldFlagsStatic);
}

::g::Uno::UX::PropertyAccessor_type* OSP_accessor_DropdownOption_BorderColor_typeof()
{
    static uSStrong< ::g::Uno::UX::PropertyAccessor_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::PropertyAccessor_typeof();
    options.FieldCount = 2;
    options.ObjectSize = sizeof(OSP_accessor_DropdownOption_BorderColor);
    options.TypeSize = sizeof(::g::Uno::UX::PropertyAccessor_type);
    type = (::g::Uno::UX::PropertyAccessor_type*)uClassType::New("OSP_accessor_DropdownOption_BorderColor", options);
    type->fp_build_ = OSP_accessor_DropdownOption_BorderColor_build;
    type->fp_ctor_ = (void*)OSP_accessor_DropdownOption_BorderColor__New1_fn;
    type->fp_cctor_ = OSP_accessor_DropdownOption_BorderColor__cctor__fn;
    type->fp_GetAsObject = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::PropertyObject*, uObject**))OSP_accessor_DropdownOption_BorderColor__GetAsObject_fn;
    type->fp_get_Name = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::Selector*))OSP_accessor_DropdownOption_BorderColor__get_Name_fn;
    type->fp_get_PropertyType = (void(*)(::g::Uno::UX::PropertyAccessor*, uType**))OSP_accessor_DropdownOption_BorderColor__get_PropertyType_fn;
    type->fp_SetAsObject = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::PropertyObject*, uObject*, uObject*))OSP_accessor_DropdownOption_BorderColor__SetAsObject_fn;
    type->fp_get_SupportsOriginSetter = (void(*)(::g::Uno::UX::PropertyAccessor*, bool*))OSP_accessor_DropdownOption_BorderColor__get_SupportsOriginSetter_fn;
    return type;
}

// public generated OSP_accessor_DropdownOption_BorderColor() :31
void OSP_accessor_DropdownOption_BorderColor__ctor_1_fn(OSP_accessor_DropdownOption_BorderColor* __this)
{
    __this->ctor_1();
}

// public override sealed object GetAsObject(Uno.UX.PropertyObject obj) :37
void OSP_accessor_DropdownOption_BorderColor__GetAsObject_fn(OSP_accessor_DropdownOption_BorderColor* __this, ::g::Uno::UX::PropertyObject* obj, uObject** __retval)
{
    uStackFrame __("OSP_accessor_DropdownOption_BorderColor", "GetAsObject(Uno.UX.PropertyObject)");
    return *__retval = uPtr(uCast< ::g::DropdownOption*>(obj, ::TYPES[0/*DropdownOption*/]))->BorderColor(), void();
}

// public override sealed Uno.UX.Selector get_Name() :34
void OSP_accessor_DropdownOption_BorderColor__get_Name_fn(OSP_accessor_DropdownOption_BorderColor* __this, ::g::Uno::UX::Selector* __retval)
{
    return *__retval = OSP_accessor_DropdownOption_BorderColor::_name_, void();
}

// public generated OSP_accessor_DropdownOption_BorderColor New() :31
void OSP_accessor_DropdownOption_BorderColor__New1_fn(OSP_accessor_DropdownOption_BorderColor** __retval)
{
    *__retval = OSP_accessor_DropdownOption_BorderColor::New1();
}

// public override sealed Uno.Type get_PropertyType() :36
void OSP_accessor_DropdownOption_BorderColor__get_PropertyType_fn(OSP_accessor_DropdownOption_BorderColor* __this, uType** __retval)
{
    return *__retval = ::TYPES[1/*Fuse.Drawing.Brush*/], void();
}

// public override sealed void SetAsObject(Uno.UX.PropertyObject obj, object v, Uno.UX.IPropertyListener origin) :38
void OSP_accessor_DropdownOption_BorderColor__SetAsObject_fn(OSP_accessor_DropdownOption_BorderColor* __this, ::g::Uno::UX::PropertyObject* obj, uObject* v, uObject* origin)
{
    uStackFrame __("OSP_accessor_DropdownOption_BorderColor", "SetAsObject(Uno.UX.PropertyObject,object,Uno.UX.IPropertyListener)");
    uPtr(uCast< ::g::DropdownOption*>(obj, ::TYPES[0/*DropdownOption*/]))->SetBorderColor(uCast< ::g::Fuse::Drawing::Brush*>(v, ::TYPES[1/*Fuse.Drawing.Brush*/]), origin);
}

// public override sealed bool get_SupportsOriginSetter() :39
void OSP_accessor_DropdownOption_BorderColor__get_SupportsOriginSetter_fn(OSP_accessor_DropdownOption_BorderColor* __this, bool* __retval)
{
    return *__retval = true, void();
}

uSStrong< ::g::Uno::UX::PropertyAccessor*> OSP_accessor_DropdownOption_BorderColor::Singleton_;
::g::Uno::UX::Selector OSP_accessor_DropdownOption_BorderColor::_name_;

// public generated OSP_accessor_DropdownOption_BorderColor() [instance] :31
void OSP_accessor_DropdownOption_BorderColor::ctor_1()
{
    ctor_();
}

// public generated OSP_accessor_DropdownOption_BorderColor New() [static] :31
OSP_accessor_DropdownOption_BorderColor* OSP_accessor_DropdownOption_BorderColor::New1()
{
    OSP_accessor_DropdownOption_BorderColor* obj1 = (OSP_accessor_DropdownOption_BorderColor*)uNew(OSP_accessor_DropdownOption_BorderColor_typeof());
    obj1->ctor_1();
    return obj1;
}
// }

} // ::g
