// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.OSP_accessor_Sq-a5cc3c2e.h>
#include <_root.SquareButton.h>
#include <Uno.Bool.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.Type.h>
#include <Uno.UX.IPropertyListener.h>
#include <Uno.UX.PropertyObject.h>
#include <Uno.UX.Selector.h>
static uString* STRINGS[1];
static uType* TYPES[3];

namespace g{

// internal sealed class OSP_accessor_SquareButton_Textq :1
// {
// static generated OSP_accessor_SquareButton_Textq() :1
static void OSP_accessor_SquareButton_Textq__cctor__fn(uType* __type)
{
    OSP_accessor_SquareButton_Textq::Singleton_ = OSP_accessor_SquareButton_Textq::New1();
    OSP_accessor_SquareButton_Textq::_name_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[0/*"Textq"*/]);
}

static void OSP_accessor_SquareButton_Textq_build(uType* type)
{
    ::STRINGS[0] = uString::Const("Textq");
    ::TYPES[0] = ::g::SquareButton_typeof();
    ::TYPES[1] = ::g::Uno::String_typeof();
    ::TYPES[2] = ::g::Uno::Type_typeof();
    type->SetFields(0,
        ::g::Uno::UX::PropertyAccessor_typeof(), (uintptr_t)&OSP_accessor_SquareButton_Textq::Singleton_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&OSP_accessor_SquareButton_Textq::_name_, uFieldFlagsStatic);
}

::g::Uno::UX::PropertyAccessor_type* OSP_accessor_SquareButton_Textq_typeof()
{
    static uSStrong< ::g::Uno::UX::PropertyAccessor_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::PropertyAccessor_typeof();
    options.FieldCount = 2;
    options.ObjectSize = sizeof(OSP_accessor_SquareButton_Textq);
    options.TypeSize = sizeof(::g::Uno::UX::PropertyAccessor_type);
    type = (::g::Uno::UX::PropertyAccessor_type*)uClassType::New("OSP_accessor_SquareButton_Textq", options);
    type->fp_build_ = OSP_accessor_SquareButton_Textq_build;
    type->fp_ctor_ = (void*)OSP_accessor_SquareButton_Textq__New1_fn;
    type->fp_cctor_ = OSP_accessor_SquareButton_Textq__cctor__fn;
    type->fp_GetAsObject = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::PropertyObject*, uObject**))OSP_accessor_SquareButton_Textq__GetAsObject_fn;
    type->fp_get_Name = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::Selector*))OSP_accessor_SquareButton_Textq__get_Name_fn;
    type->fp_get_PropertyType = (void(*)(::g::Uno::UX::PropertyAccessor*, uType**))OSP_accessor_SquareButton_Textq__get_PropertyType_fn;
    type->fp_SetAsObject = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::PropertyObject*, uObject*, uObject*))OSP_accessor_SquareButton_Textq__SetAsObject_fn;
    type->fp_get_SupportsOriginSetter = (void(*)(::g::Uno::UX::PropertyAccessor*, bool*))OSP_accessor_SquareButton_Textq__get_SupportsOriginSetter_fn;
    return type;
}

// public generated OSP_accessor_SquareButton_Textq() :1
void OSP_accessor_SquareButton_Textq__ctor_1_fn(OSP_accessor_SquareButton_Textq* __this)
{
    __this->ctor_1();
}

// public override sealed object GetAsObject(Uno.UX.PropertyObject obj) :7
void OSP_accessor_SquareButton_Textq__GetAsObject_fn(OSP_accessor_SquareButton_Textq* __this, ::g::Uno::UX::PropertyObject* obj, uObject** __retval)
{
    uStackFrame __("OSP_accessor_SquareButton_Textq", "GetAsObject(Uno.UX.PropertyObject)");
    return *__retval = uPtr(uCast< ::g::SquareButton*>(obj, ::TYPES[0/*SquareButton*/]))->Textq(), void();
}

// public override sealed Uno.UX.Selector get_Name() :4
void OSP_accessor_SquareButton_Textq__get_Name_fn(OSP_accessor_SquareButton_Textq* __this, ::g::Uno::UX::Selector* __retval)
{
    return *__retval = OSP_accessor_SquareButton_Textq::_name_, void();
}

// public generated OSP_accessor_SquareButton_Textq New() :1
void OSP_accessor_SquareButton_Textq__New1_fn(OSP_accessor_SquareButton_Textq** __retval)
{
    *__retval = OSP_accessor_SquareButton_Textq::New1();
}

// public override sealed Uno.Type get_PropertyType() :6
void OSP_accessor_SquareButton_Textq__get_PropertyType_fn(OSP_accessor_SquareButton_Textq* __this, uType** __retval)
{
    return *__retval = ::TYPES[1/*string*/], void();
}

// public override sealed void SetAsObject(Uno.UX.PropertyObject obj, object v, Uno.UX.IPropertyListener origin) :8
void OSP_accessor_SquareButton_Textq__SetAsObject_fn(OSP_accessor_SquareButton_Textq* __this, ::g::Uno::UX::PropertyObject* obj, uObject* v, uObject* origin)
{
    uStackFrame __("OSP_accessor_SquareButton_Textq", "SetAsObject(Uno.UX.PropertyObject,object,Uno.UX.IPropertyListener)");
    uPtr(uCast< ::g::SquareButton*>(obj, ::TYPES[0/*SquareButton*/]))->SetTextq(uCast<uString*>(v, ::TYPES[1/*string*/]), origin);
}

// public override sealed bool get_SupportsOriginSetter() :9
void OSP_accessor_SquareButton_Textq__get_SupportsOriginSetter_fn(OSP_accessor_SquareButton_Textq* __this, bool* __retval)
{
    return *__retval = true, void();
}

uSStrong< ::g::Uno::UX::PropertyAccessor*> OSP_accessor_SquareButton_Textq::Singleton_;
::g::Uno::UX::Selector OSP_accessor_SquareButton_Textq::_name_;

// public generated OSP_accessor_SquareButton_Textq() [instance] :1
void OSP_accessor_SquareButton_Textq::ctor_1()
{
    ctor_();
}

// public generated OSP_accessor_SquareButton_Textq New() [static] :1
OSP_accessor_SquareButton_Textq* OSP_accessor_SquareButton_Textq::New1()
{
    OSP_accessor_SquareButton_Textq* obj1 = (OSP_accessor_SquareButton_Textq*)uNew(OSP_accessor_SquareButton_Textq_typeof());
    obj1->ctor_1();
    return obj1;
}
// }

} // ::g
