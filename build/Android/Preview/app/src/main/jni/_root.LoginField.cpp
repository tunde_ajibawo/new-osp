// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/LoginField.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.LoginField.h>
#include <_root.MainView.h>
#include <_root.OSP_accessor_Lo-40e30f6f.h>
#include <_root.OSP_accessor_Lo-5466f775.h>
#include <_root.OSP_accessor_Lo-b9ee0e3a.h>
#include <_root.OSP_accessor_Lo-bb48c508.h>
#include <_root.OSP_FuseControl-569ba08.h>
#include <_root.OSP_FuseControl-b0f0449b.h>
#include <_root.OSP_FuseControl-efd79a87.h>
#include <_root.OSP_FuseDrawing-245ed517.h>
#include <Fuse.Controls.Rectangle.h>
#include <Fuse.Controls.Shape.h>
#include <Fuse.Controls.TextAlignment.h>
#include <Fuse.Controls.TextInputControl.h>
#include <Fuse.Drawing.Stroke.h>
#include <Fuse.Elements.Element.h>
#include <Fuse.Font.h>
#include <Fuse.Layer.h>
#include <Fuse.Reactive.BindingMode.h>
#include <Fuse.Reactive.Constan-264ec80.h>
#include <Fuse.Reactive.Constant.h>
#include <Fuse.Reactive.DataBinding.h>
#include <Fuse.Reactive.Expression.h>
#include <Fuse.Reactive.IExpression.h>
#include <Fuse.Reactive.Property.h>
#include <Uno.Bool.h>
#include <Uno.Float.h>
#include <Uno.Int.h>
#include <Uno.Object.h>
#include <Uno.UX.Property.h>
#include <Uno.UX.Property1-1.h>
#include <Uno.UX.PropertyAccessor.h>
#include <Uno.UX.PropertyObject.h>
#include <Uno.UX.Selector.h>
#include <Uno.UX.Size.h>
#include <Uno.UX.Unit.h>
static uString* STRINGS[8];
static uType* TYPES[3];

namespace g{

// public partial sealed class LoginField :2
// {
// static LoginField() :68
static void LoginField__cctor_3_fn(uType* __type)
{
    LoginField::__selector0_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[0/*"Color"*/]);
    LoginField::__selector1_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[1/*"Placeholder...*/]);
    LoginField::__selector2_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[2/*"Placeholder...*/]);
}

static void LoginField_build(uType* type)
{
    ::STRINGS[0] = uString::Const("Color");
    ::STRINGS[1] = uString::Const("PlaceholderText");
    ::STRINGS[2] = uString::Const("PlaceholderColor");
    ::STRINGS[3] = uString::Const("homepage.ux");
    ::STRINGS[4] = uString::Const("BgColor");
    ::STRINGS[5] = uString::Const("PlaceHolder");
    ::STRINGS[6] = uString::Const("PlaceHolderColor");
    ::STRINGS[7] = uString::Const("StrokeColor");
    ::TYPES[0] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Drawing::Stroke_typeof(), NULL);
    ::TYPES[1] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL);
    ::TYPES[2] = ::g::Uno::Collections::ICollection_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL);
    type->SetDependencies(
        ::g::MainView_typeof(),
        ::g::OSP_accessor_LoginField_BgColor_typeof(),
        ::g::OSP_accessor_LoginField_PlaceHolder_typeof(),
        ::g::OSP_accessor_LoginField_PlaceHolderColor_typeof(),
        ::g::OSP_accessor_LoginField_StrokeColor_typeof());
    type->SetInterfaces(
        ::g::Uno::Collections::IList_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::TextInput_type, interface0),
        ::g::Fuse::Scripting::IScriptObject_typeof(), offsetof(::g::Fuse::Controls::TextInput_type, interface1),
        ::g::Fuse::IProperties_typeof(), offsetof(::g::Fuse::Controls::TextInput_type, interface2),
        ::g::Fuse::INotifyUnrooted_typeof(), offsetof(::g::Fuse::Controls::TextInput_type, interface3),
        ::g::Fuse::ISourceLocation_typeof(), offsetof(::g::Fuse::Controls::TextInput_type, interface4),
        ::TYPES[1/*Uno.Collections.ICollection<Fuse.Binding>*/], offsetof(::g::Fuse::Controls::TextInput_type, interface5),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Binding_typeof(), NULL), offsetof(::g::Fuse::Controls::TextInput_type, interface6),
        ::g::Uno::Collections::IList_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::TextInput_type, interface7),
        ::g::Uno::UX::IPropertyListener_typeof(), offsetof(::g::Fuse::Controls::TextInput_type, interface8),
        ::g::Fuse::ITemplateSource_typeof(), offsetof(::g::Fuse::Controls::TextInput_type, interface9),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Visual_typeof(), NULL), offsetof(::g::Fuse::Controls::TextInput_type, interface10),
        ::TYPES[2/*Uno.Collections.ICollection<Fuse.Node>*/], offsetof(::g::Fuse::Controls::TextInput_type, interface11),
        ::g::Uno::Collections::IEnumerable_typeof()->MakeType(::g::Fuse::Node_typeof(), NULL), offsetof(::g::Fuse::Controls::TextInput_type, interface12),
        ::g::Fuse::Triggers::Actions::IShow_typeof(), offsetof(::g::Fuse::Controls::TextInput_type, interface13),
        ::g::Fuse::Triggers::Actions::IHide_typeof(), offsetof(::g::Fuse::Controls::TextInput_type, interface14),
        ::g::Fuse::Triggers::Actions::ICollapse_typeof(), offsetof(::g::Fuse::Controls::TextInput_type, interface15),
        ::g::Fuse::IActualPlacement_typeof(), offsetof(::g::Fuse::Controls::TextInput_type, interface16),
        ::g::Fuse::Animations::IResize_typeof(), offsetof(::g::Fuse::Controls::TextInput_type, interface17),
        ::g::Fuse::Triggers::IValue_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(::g::Fuse::Controls::TextInput_type, interface18),
        ::g::Fuse::Controls::ITextEditControl_typeof(), offsetof(::g::Fuse::Controls::TextInput_type, interface19));
    type->SetFields(115,
        ::g::Uno::String_typeof(), offsetof(LoginField, _field_PlaceHolder), 0,
        ::g::Uno::Float4_typeof(), offsetof(LoginField, _field_PlaceHolderColor), 0,
        ::g::Uno::Float4_typeof(), offsetof(LoginField, _field_BgColor), 0,
        ::g::Uno::Float4_typeof(), offsetof(LoginField, _field_StrokeColor), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL), offsetof(LoginField, temp_Color_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL), offsetof(LoginField, temp1_Color_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::String_typeof(), NULL), offsetof(LoginField, this_PlaceholderText_inst), 0,
        ::g::Uno::UX::Property1_typeof()->MakeType(::g::Uno::Float4_typeof(), NULL), offsetof(LoginField, this_PlaceholderColor_inst), 0,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&LoginField::__selector0_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&LoginField::__selector1_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&LoginField::__selector2_, uFieldFlagsStatic);
    type->Reflection.SetFunctions(13,
        new uFunction("get_BgColor", NULL, (void*)LoginField__get_BgColor_fn, 0, false, ::g::Uno::Float4_typeof(), 0),
        new uFunction("set_BgColor", NULL, (void*)LoginField__set_BgColor_fn, 0, false, uVoid_typeof(), 1, ::g::Uno::Float4_typeof()),
        new uFunction(".ctor", NULL, (void*)LoginField__New4_fn, 0, true, type, 0),
        new uFunction("get_PlaceHolder", NULL, (void*)LoginField__get_PlaceHolder_fn, 0, false, ::g::Uno::String_typeof(), 0),
        new uFunction("set_PlaceHolder", NULL, (void*)LoginField__set_PlaceHolder_fn, 0, false, uVoid_typeof(), 1, ::g::Uno::String_typeof()),
        new uFunction("get_PlaceHolderColor", NULL, (void*)LoginField__get_PlaceHolderColor_fn, 0, false, ::g::Uno::Float4_typeof(), 0),
        new uFunction("set_PlaceHolderColor", NULL, (void*)LoginField__set_PlaceHolderColor_fn, 0, false, uVoid_typeof(), 1, ::g::Uno::Float4_typeof()),
        new uFunction("SetBgColor", NULL, (void*)LoginField__SetBgColor_fn, 0, false, uVoid_typeof(), 2, ::g::Uno::Float4_typeof(), ::g::Uno::UX::IPropertyListener_typeof()),
        new uFunction("SetPlaceHolder", NULL, (void*)LoginField__SetPlaceHolder_fn, 0, false, uVoid_typeof(), 2, ::g::Uno::String_typeof(), ::g::Uno::UX::IPropertyListener_typeof()),
        new uFunction("SetPlaceHolderColor", NULL, (void*)LoginField__SetPlaceHolderColor_fn, 0, false, uVoid_typeof(), 2, ::g::Uno::Float4_typeof(), ::g::Uno::UX::IPropertyListener_typeof()),
        new uFunction("SetStrokeColor", NULL, (void*)LoginField__SetStrokeColor_fn, 0, false, uVoid_typeof(), 2, ::g::Uno::Float4_typeof(), ::g::Uno::UX::IPropertyListener_typeof()),
        new uFunction("get_StrokeColor", NULL, (void*)LoginField__get_StrokeColor_fn, 0, false, ::g::Uno::Float4_typeof(), 0),
        new uFunction("set_StrokeColor", NULL, (void*)LoginField__set_StrokeColor_fn, 0, false, uVoid_typeof(), 1, ::g::Uno::Float4_typeof()));
}

::g::Fuse::Controls::TextInput_type* LoginField_typeof()
{
    static uSStrong< ::g::Fuse::Controls::TextInput_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Fuse::Controls::TextInput_typeof();
    options.FieldCount = 126;
    options.InterfaceCount = 20;
    options.DependencyCount = 5;
    options.ObjectSize = sizeof(LoginField);
    options.TypeSize = sizeof(::g::Fuse::Controls::TextInput_type);
    type = (::g::Fuse::Controls::TextInput_type*)uClassType::New("LoginField", options);
    type->fp_build_ = LoginField_build;
    type->fp_ctor_ = (void*)LoginField__New4_fn;
    type->fp_cctor_ = LoginField__cctor_3_fn;
    type->interface19.fp_get_IsPassword = (void(*)(uObject*, bool*))::g::Fuse::Controls::TextInput__get_IsPassword_fn;
    type->interface19.fp_set_IsPassword = (void(*)(uObject*, bool*))::g::Fuse::Controls::TextInput__set_IsPassword_fn;
    type->interface19.fp_add_ActionTriggered = (void(*)(uObject*, uDelegate*))::g::Fuse::Controls::TextInput__add_ActionTriggered_fn;
    type->interface19.fp_remove_ActionTriggered = (void(*)(uObject*, uDelegate*))::g::Fuse::Controls::TextInput__remove_ActionTriggered_fn;
    type->interface18.fp_get_Value = (void(*)(uObject*, uTRef))::g::Fuse::Controls::TextInputControl__get_Value_fn;
    type->interface18.fp_set_Value = (void(*)(uObject*, void*))::g::Fuse::Controls::TextInputControl__set_Value_fn;
    type->interface18.fp_add_ValueChanged = (void(*)(uObject*, uDelegate*))::g::Fuse::Controls::TextInputControl__add_ValueChanged_fn;
    type->interface18.fp_remove_ValueChanged = (void(*)(uObject*, uDelegate*))::g::Fuse::Controls::TextInputControl__remove_ValueChanged_fn;
    type->interface13.fp_Show = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsIShowShow_fn;
    type->interface15.fp_Collapse = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsICollapseCollapse_fn;
    type->interface14.fp_Hide = (void(*)(uObject*))::g::Fuse::Elements::Element__FuseTriggersActionsIHideHide_fn;
    type->interface17.fp_SetSize = (void(*)(uObject*, ::g::Uno::Float2*))::g::Fuse::Elements::Element__FuseAnimationsIResizeSetSize_fn;
    type->interface16.fp_get_ActualSize = (void(*)(uObject*, ::g::Uno::Float3*))::g::Fuse::Elements::Element__FuseIActualPlacementget_ActualSize_fn;
    type->interface16.fp_get_ActualPosition = (void(*)(uObject*, ::g::Uno::Float3*))::g::Fuse::Elements::Element__FuseIActualPlacementget_ActualPosition_fn;
    type->interface16.fp_add_Placed = (void(*)(uObject*, uDelegate*))::g::Fuse::Elements::Element__add_Placed_fn;
    type->interface16.fp_remove_Placed = (void(*)(uObject*, uDelegate*))::g::Fuse::Elements::Element__remove_Placed_fn;
    type->interface10.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Visual__UnoCollectionsIEnumerableFuseVisualGetEnumerator_fn;
    type->interface11.fp_Clear = (void(*)(uObject*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeClear_fn;
    type->interface11.fp_Contains = (void(*)(uObject*, void*, bool*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeContains_fn;
    type->interface7.fp_RemoveAt = (void(*)(uObject*, int32_t*))::g::Fuse::Visual__UnoCollectionsIListFuseNodeRemoveAt_fn;
    type->interface12.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Visual__UnoCollectionsIEnumerableFuseNodeGetEnumerator_fn;
    type->interface11.fp_get_Count = (void(*)(uObject*, int32_t*))::g::Fuse::Visual__UnoCollectionsICollectionFuseNodeget_Count_fn;
    type->interface7.fp_get_Item = (void(*)(uObject*, int32_t*, uTRef))::g::Fuse::Visual__UnoCollectionsIListFuseNodeget_Item_fn;
    type->interface7.fp_Insert = (void(*)(uObject*, int32_t*, void*))::g::Fuse::Visual__Insert1_fn;
    type->interface8.fp_OnPropertyChanged = (void(*)(uObject*, ::g::Uno::UX::PropertyObject*, ::g::Uno::UX::Selector*))::g::Fuse::Controls::TextInputControl__OnPropertyChanged2_fn;
    type->interface9.fp_FindTemplate = (void(*)(uObject*, uString*, ::g::Uno::UX::Template**))::g::Fuse::Visual__FindTemplate_fn;
    type->interface11.fp_Add = (void(*)(uObject*, void*))::g::Fuse::Visual__Add1_fn;
    type->interface11.fp_Remove = (void(*)(uObject*, void*, bool*))::g::Fuse::Visual__Remove1_fn;
    type->interface5.fp_Clear = (void(*)(uObject*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingClear_fn;
    type->interface5.fp_Contains = (void(*)(uObject*, void*, bool*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingContains_fn;
    type->interface0.fp_RemoveAt = (void(*)(uObject*, int32_t*))::g::Fuse::Node__UnoCollectionsIListFuseBindingRemoveAt_fn;
    type->interface6.fp_GetEnumerator = (void(*)(uObject*, uObject**))::g::Fuse::Node__UnoCollectionsIEnumerableFuseBindingGetEnumerator_fn;
    type->interface1.fp_SetScriptObject = (void(*)(uObject*, uObject*, ::g::Fuse::Scripting::Context*))::g::Fuse::Node__FuseScriptingIScriptObjectSetScriptObject_fn;
    type->interface5.fp_get_Count = (void(*)(uObject*, int32_t*))::g::Fuse::Node__UnoCollectionsICollectionFuseBindingget_Count_fn;
    type->interface0.fp_get_Item = (void(*)(uObject*, int32_t*, uTRef))::g::Fuse::Node__UnoCollectionsIListFuseBindingget_Item_fn;
    type->interface1.fp_get_ScriptObject = (void(*)(uObject*, uObject**))::g::Fuse::Node__FuseScriptingIScriptObjectget_ScriptObject_fn;
    type->interface1.fp_get_ScriptContext = (void(*)(uObject*, ::g::Fuse::Scripting::Context**))::g::Fuse::Node__FuseScriptingIScriptObjectget_ScriptContext_fn;
    type->interface4.fp_get_SourceNearest = (void(*)(uObject*, uObject**))::g::Fuse::Node__FuseISourceLocationget_SourceNearest_fn;
    type->interface3.fp_add_Unrooted = (void(*)(uObject*, uDelegate*))::g::Fuse::Node__FuseINotifyUnrootedadd_Unrooted_fn;
    type->interface3.fp_remove_Unrooted = (void(*)(uObject*, uDelegate*))::g::Fuse::Node__FuseINotifyUnrootedremove_Unrooted_fn;
    type->interface0.fp_Insert = (void(*)(uObject*, int32_t*, void*))::g::Fuse::Node__Insert_fn;
    type->interface2.fp_get_Properties = (void(*)(uObject*, ::g::Fuse::Properties**))::g::Fuse::Node__get_Properties_fn;
    type->interface4.fp_get_SourceLineNumber = (void(*)(uObject*, int32_t*))::g::Fuse::Node__get_SourceLineNumber_fn;
    type->interface4.fp_get_SourceFileName = (void(*)(uObject*, uString**))::g::Fuse::Node__get_SourceFileName_fn;
    type->interface5.fp_Add = (void(*)(uObject*, void*))::g::Fuse::Node__Add_fn;
    type->interface5.fp_Remove = (void(*)(uObject*, void*, bool*))::g::Fuse::Node__Remove_fn;
    return type;
}

// public LoginField() :72
void LoginField__ctor_8_fn(LoginField* __this)
{
    __this->ctor_8();
}

// public float4 get_BgColor() :38
void LoginField__get_BgColor_fn(LoginField* __this, ::g::Uno::Float4* __retval)
{
    *__retval = __this->BgColor();
}

// public void set_BgColor(float4 value) :39
void LoginField__set_BgColor_fn(LoginField* __this, ::g::Uno::Float4* value)
{
    __this->BgColor(*value);
}

// private void InitializeUX() :76
void LoginField__InitializeUX_fn(LoginField* __this)
{
    __this->InitializeUX();
}

// public LoginField New() :72
void LoginField__New4_fn(LoginField** __retval)
{
    *__retval = LoginField::New4();
}

// public string get_PlaceHolder() :8
void LoginField__get_PlaceHolder_fn(LoginField* __this, uString** __retval)
{
    *__retval = __this->PlaceHolder();
}

// public void set_PlaceHolder(string value) :9
void LoginField__set_PlaceHolder_fn(LoginField* __this, uString* value)
{
    __this->PlaceHolder(value);
}

// public float4 get_PlaceHolderColor() :23
void LoginField__get_PlaceHolderColor_fn(LoginField* __this, ::g::Uno::Float4* __retval)
{
    *__retval = __this->PlaceHolderColor();
}

// public void set_PlaceHolderColor(float4 value) :24
void LoginField__set_PlaceHolderColor_fn(LoginField* __this, ::g::Uno::Float4* value)
{
    __this->PlaceHolderColor(*value);
}

// public void SetBgColor(float4 value, Uno.UX.IPropertyListener origin) :41
void LoginField__SetBgColor_fn(LoginField* __this, ::g::Uno::Float4* value, uObject* origin)
{
    __this->SetBgColor(*value, origin);
}

// public void SetPlaceHolder(string value, Uno.UX.IPropertyListener origin) :11
void LoginField__SetPlaceHolder_fn(LoginField* __this, uString* value, uObject* origin)
{
    __this->SetPlaceHolder(value, origin);
}

// public void SetPlaceHolderColor(float4 value, Uno.UX.IPropertyListener origin) :26
void LoginField__SetPlaceHolderColor_fn(LoginField* __this, ::g::Uno::Float4* value, uObject* origin)
{
    __this->SetPlaceHolderColor(*value, origin);
}

// public void SetStrokeColor(float4 value, Uno.UX.IPropertyListener origin) :56
void LoginField__SetStrokeColor_fn(LoginField* __this, ::g::Uno::Float4* value, uObject* origin)
{
    __this->SetStrokeColor(*value, origin);
}

// public float4 get_StrokeColor() :53
void LoginField__get_StrokeColor_fn(LoginField* __this, ::g::Uno::Float4* __retval)
{
    *__retval = __this->StrokeColor();
}

// public void set_StrokeColor(float4 value) :54
void LoginField__set_StrokeColor_fn(LoginField* __this, ::g::Uno::Float4* value)
{
    __this->StrokeColor(*value);
}

::g::Uno::UX::Selector LoginField::__selector0_;
::g::Uno::UX::Selector LoginField::__selector1_;
::g::Uno::UX::Selector LoginField::__selector2_;

// public LoginField() [instance] :72
void LoginField::ctor_8()
{
    uStackFrame __("LoginField", ".ctor()");
    ctor_7();
    InitializeUX();
}

// public float4 get_BgColor() [instance] :38
::g::Uno::Float4 LoginField::BgColor()
{
    return _field_BgColor;
}

// public void set_BgColor(float4 value) [instance] :39
void LoginField::BgColor(::g::Uno::Float4 value)
{
    uStackFrame __("LoginField", "set_BgColor(float4)");
    SetBgColor(value, NULL);
}

// private void InitializeUX() [instance] :76
void LoginField::InitializeUX()
{
    uStackFrame __("LoginField", "InitializeUX()");
    ::g::Fuse::Reactive::Constant* temp2 = ::g::Fuse::Reactive::Constant::New1(this);
    ::g::Fuse::Reactive::Constant* temp3 = ::g::Fuse::Reactive::Constant::New1(this);
    ::g::Fuse::Controls::Rectangle* temp = ::g::Fuse::Controls::Rectangle::New3();
    temp_Color_inst = ::g::OSP_FuseControlsShape_Color_Property::New1(temp, LoginField::__selector0_);
    ::g::Fuse::Reactive::Property* temp4 = ::g::Fuse::Reactive::Property::New1(temp3, ::g::OSP_accessor_LoginField_BgColor::Singleton());
    ::g::Fuse::Drawing::Stroke* temp1 = ::g::Fuse::Drawing::Stroke::New2();
    temp1_Color_inst = ::g::OSP_FuseDrawingStroke_Color_Property::New1(temp1, LoginField::__selector0_);
    ::g::Fuse::Reactive::Property* temp5 = ::g::Fuse::Reactive::Property::New1(temp2, ::g::OSP_accessor_LoginField_StrokeColor::Singleton());
    ::g::Fuse::Reactive::Constant* temp6 = ::g::Fuse::Reactive::Constant::New1(this);
    this_PlaceholderText_inst = ::g::OSP_FuseControlsTextInput_PlaceholderText_Property::New1(this, LoginField::__selector1_);
    ::g::Fuse::Reactive::Property* temp7 = ::g::Fuse::Reactive::Property::New1(temp6, ::g::OSP_accessor_LoginField_PlaceHolder::Singleton());
    ::g::Fuse::Reactive::Constant* temp8 = ::g::Fuse::Reactive::Constant::New1(this);
    this_PlaceholderColor_inst = ::g::OSP_FuseControlsTextInput_PlaceholderColor_Property::New1(this, LoginField::__selector2_);
    ::g::Fuse::Reactive::Property* temp9 = ::g::Fuse::Reactive::Property::New1(temp8, ::g::OSP_accessor_LoginField_PlaceHolderColor::Singleton());
    ::g::Fuse::Reactive::DataBinding* temp10 = ::g::Fuse::Reactive::DataBinding::New1(temp_Color_inst, (uObject*)temp4, 3);
    ::g::Fuse::Reactive::DataBinding* temp11 = ::g::Fuse::Reactive::DataBinding::New1(temp1_Color_inst, (uObject*)temp5, 3);
    ::g::Fuse::Reactive::DataBinding* temp12 = ::g::Fuse::Reactive::DataBinding::New1(this_PlaceholderText_inst, (uObject*)temp7, 3);
    ::g::Fuse::Reactive::DataBinding* temp13 = ::g::Fuse::Reactive::DataBinding::New1(this_PlaceholderColor_inst, (uObject*)temp9, 3);
    BgColor(::g::Uno::Float4__New2(0.06666667f, 0.2078431f, 0.4117647f, 1.0f));
    StrokeColor(::g::Uno::Float4__New2(0.06666667f, 0.2078431f, 0.4117647f, 1.0f));
    FontSize(12.0f);
    TextAlignment(0);
    TextColor(::g::Uno::Float4__New2(0.2f, 0.2f, 0.2f, 1.0f));
    Width(::g::Uno::UX::Size__New1(80.0f, 4));
    Margin(::g::Uno::Float4__New2(0.0f, 4.0f, 0.0f, 4.0f));
    SourceLineNumber(5);
    SourceFileName(::STRINGS[3/*"homepage.ux"*/]);
    temp->Width(::g::Uno::UX::Size__New1(100.0f, 4));
    temp->Layer(1);
    temp->SourceLineNumber(10);
    temp->SourceFileName(::STRINGS[3/*"homepage.ux"*/]);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Strokes()), ::TYPES[0/*Uno.Collections.ICollection<Fuse.Drawing.Stroke>*/]), temp1);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Bindings()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp10);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(temp->Bindings()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp11);
    temp1->Width(1.0f);
    temp5->SourceLineNumber(11);
    temp5->SourceFileName(::STRINGS[3/*"homepage.ux"*/]);
    temp2->SourceLineNumber(11);
    temp2->SourceFileName(::STRINGS[3/*"homepage.ux"*/]);
    temp4->SourceLineNumber(10);
    temp4->SourceFileName(::STRINGS[3/*"homepage.ux"*/]);
    temp3->SourceLineNumber(10);
    temp3->SourceFileName(::STRINGS[3/*"homepage.ux"*/]);
    temp7->SourceLineNumber(5);
    temp7->SourceFileName(::STRINGS[3/*"homepage.ux"*/]);
    temp6->SourceLineNumber(5);
    temp6->SourceFileName(::STRINGS[3/*"homepage.ux"*/]);
    temp9->SourceLineNumber(5);
    temp9->SourceFileName(::STRINGS[3/*"homepage.ux"*/]);
    temp8->SourceLineNumber(5);
    temp8->SourceFileName(::STRINGS[3/*"homepage.ux"*/]);
    Font(::g::MainView::Raleway());
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Children()), ::TYPES[2/*Uno.Collections.ICollection<Fuse.Node>*/]), temp);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Bindings()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp12);
    ::g::Uno::Collections::ICollection::Add_ex(uInterface(uPtr(Bindings()), ::TYPES[1/*Uno.Collections.ICollection<Fuse.Binding>*/]), temp13);
}

// public string get_PlaceHolder() [instance] :8
uString* LoginField::PlaceHolder()
{
    return _field_PlaceHolder;
}

// public void set_PlaceHolder(string value) [instance] :9
void LoginField::PlaceHolder(uString* value)
{
    uStackFrame __("LoginField", "set_PlaceHolder(string)");
    SetPlaceHolder(value, NULL);
}

// public float4 get_PlaceHolderColor() [instance] :23
::g::Uno::Float4 LoginField::PlaceHolderColor()
{
    return _field_PlaceHolderColor;
}

// public void set_PlaceHolderColor(float4 value) [instance] :24
void LoginField::PlaceHolderColor(::g::Uno::Float4 value)
{
    uStackFrame __("LoginField", "set_PlaceHolderColor(float4)");
    SetPlaceHolderColor(value, NULL);
}

// public void SetBgColor(float4 value, Uno.UX.IPropertyListener origin) [instance] :41
void LoginField::SetBgColor(::g::Uno::Float4 value, uObject* origin)
{
    uStackFrame __("LoginField", "SetBgColor(float4,Uno.UX.IPropertyListener)");

    if (::g::Uno::Float4__op_Inequality(value, _field_BgColor))
    {
        _field_BgColor = value;
        OnPropertyChanged1(::g::Uno::UX::Selector__op_Implicit1(::STRINGS[4/*"BgColor"*/]), origin);
    }
}

// public void SetPlaceHolder(string value, Uno.UX.IPropertyListener origin) [instance] :11
void LoginField::SetPlaceHolder(uString* value, uObject* origin)
{
    uStackFrame __("LoginField", "SetPlaceHolder(string,Uno.UX.IPropertyListener)");

    if (::g::Uno::String::op_Inequality(value, _field_PlaceHolder))
    {
        _field_PlaceHolder = value;
        OnPropertyChanged1(::g::Uno::UX::Selector__op_Implicit1(::STRINGS[5/*"PlaceHolder"*/]), origin);
    }
}

// public void SetPlaceHolderColor(float4 value, Uno.UX.IPropertyListener origin) [instance] :26
void LoginField::SetPlaceHolderColor(::g::Uno::Float4 value, uObject* origin)
{
    uStackFrame __("LoginField", "SetPlaceHolderColor(float4,Uno.UX.IPropertyListener)");

    if (::g::Uno::Float4__op_Inequality(value, _field_PlaceHolderColor))
    {
        _field_PlaceHolderColor = value;
        OnPropertyChanged1(::g::Uno::UX::Selector__op_Implicit1(::STRINGS[6/*"PlaceHolder...*/]), origin);
    }
}

// public void SetStrokeColor(float4 value, Uno.UX.IPropertyListener origin) [instance] :56
void LoginField::SetStrokeColor(::g::Uno::Float4 value, uObject* origin)
{
    uStackFrame __("LoginField", "SetStrokeColor(float4,Uno.UX.IPropertyListener)");

    if (::g::Uno::Float4__op_Inequality(value, _field_StrokeColor))
    {
        _field_StrokeColor = value;
        OnPropertyChanged1(::g::Uno::UX::Selector__op_Implicit1(::STRINGS[7/*"StrokeColor"*/]), origin);
    }
}

// public float4 get_StrokeColor() [instance] :53
::g::Uno::Float4 LoginField::StrokeColor()
{
    return _field_StrokeColor;
}

// public void set_StrokeColor(float4 value) [instance] :54
void LoginField::StrokeColor(::g::Uno::Float4 value)
{
    uStackFrame __("LoginField", "set_StrokeColor(float4)");
    SetStrokeColor(value, NULL);
}

// public LoginField New() [static] :72
LoginField* LoginField::New4()
{
    LoginField* obj1 = (LoginField*)uNew(LoginField_typeof());
    obj1->ctor_8();
    return obj1;
}
// }

} // ::g
