// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.InputField.h>
#include <_root.OSP_accessor_In-433e39cd.h>
#include <Uno.Bool.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.Type.h>
#include <Uno.UX.IPropertyListener.h>
#include <Uno.UX.PropertyObject.h>
#include <Uno.UX.Selector.h>
static uString* STRINGS[1];
static uType* TYPES[3];

namespace g{

// internal sealed class OSP_accessor_InputField_PlaceHolderName :341
// {
// static generated OSP_accessor_InputField_PlaceHolderName() :341
static void OSP_accessor_InputField_PlaceHolderName__cctor__fn(uType* __type)
{
    OSP_accessor_InputField_PlaceHolderName::Singleton_ = OSP_accessor_InputField_PlaceHolderName::New1();
    OSP_accessor_InputField_PlaceHolderName::_name_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[0/*"PlaceHolder...*/]);
}

static void OSP_accessor_InputField_PlaceHolderName_build(uType* type)
{
    ::STRINGS[0] = uString::Const("PlaceHolderName");
    ::TYPES[0] = ::g::InputField_typeof();
    ::TYPES[1] = ::g::Uno::String_typeof();
    ::TYPES[2] = ::g::Uno::Type_typeof();
    type->SetFields(0,
        ::g::Uno::UX::PropertyAccessor_typeof(), (uintptr_t)&OSP_accessor_InputField_PlaceHolderName::Singleton_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&OSP_accessor_InputField_PlaceHolderName::_name_, uFieldFlagsStatic);
}

::g::Uno::UX::PropertyAccessor_type* OSP_accessor_InputField_PlaceHolderName_typeof()
{
    static uSStrong< ::g::Uno::UX::PropertyAccessor_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::PropertyAccessor_typeof();
    options.FieldCount = 2;
    options.ObjectSize = sizeof(OSP_accessor_InputField_PlaceHolderName);
    options.TypeSize = sizeof(::g::Uno::UX::PropertyAccessor_type);
    type = (::g::Uno::UX::PropertyAccessor_type*)uClassType::New("OSP_accessor_InputField_PlaceHolderName", options);
    type->fp_build_ = OSP_accessor_InputField_PlaceHolderName_build;
    type->fp_ctor_ = (void*)OSP_accessor_InputField_PlaceHolderName__New1_fn;
    type->fp_cctor_ = OSP_accessor_InputField_PlaceHolderName__cctor__fn;
    type->fp_GetAsObject = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::PropertyObject*, uObject**))OSP_accessor_InputField_PlaceHolderName__GetAsObject_fn;
    type->fp_get_Name = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::Selector*))OSP_accessor_InputField_PlaceHolderName__get_Name_fn;
    type->fp_get_PropertyType = (void(*)(::g::Uno::UX::PropertyAccessor*, uType**))OSP_accessor_InputField_PlaceHolderName__get_PropertyType_fn;
    type->fp_SetAsObject = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::PropertyObject*, uObject*, uObject*))OSP_accessor_InputField_PlaceHolderName__SetAsObject_fn;
    type->fp_get_SupportsOriginSetter = (void(*)(::g::Uno::UX::PropertyAccessor*, bool*))OSP_accessor_InputField_PlaceHolderName__get_SupportsOriginSetter_fn;
    return type;
}

// public generated OSP_accessor_InputField_PlaceHolderName() :341
void OSP_accessor_InputField_PlaceHolderName__ctor_1_fn(OSP_accessor_InputField_PlaceHolderName* __this)
{
    __this->ctor_1();
}

// public override sealed object GetAsObject(Uno.UX.PropertyObject obj) :347
void OSP_accessor_InputField_PlaceHolderName__GetAsObject_fn(OSP_accessor_InputField_PlaceHolderName* __this, ::g::Uno::UX::PropertyObject* obj, uObject** __retval)
{
    uStackFrame __("OSP_accessor_InputField_PlaceHolderName", "GetAsObject(Uno.UX.PropertyObject)");
    return *__retval = uPtr(uCast< ::g::InputField*>(obj, ::TYPES[0/*InputField*/]))->PlaceHolderName(), void();
}

// public override sealed Uno.UX.Selector get_Name() :344
void OSP_accessor_InputField_PlaceHolderName__get_Name_fn(OSP_accessor_InputField_PlaceHolderName* __this, ::g::Uno::UX::Selector* __retval)
{
    return *__retval = OSP_accessor_InputField_PlaceHolderName::_name_, void();
}

// public generated OSP_accessor_InputField_PlaceHolderName New() :341
void OSP_accessor_InputField_PlaceHolderName__New1_fn(OSP_accessor_InputField_PlaceHolderName** __retval)
{
    *__retval = OSP_accessor_InputField_PlaceHolderName::New1();
}

// public override sealed Uno.Type get_PropertyType() :346
void OSP_accessor_InputField_PlaceHolderName__get_PropertyType_fn(OSP_accessor_InputField_PlaceHolderName* __this, uType** __retval)
{
    return *__retval = ::TYPES[1/*string*/], void();
}

// public override sealed void SetAsObject(Uno.UX.PropertyObject obj, object v, Uno.UX.IPropertyListener origin) :348
void OSP_accessor_InputField_PlaceHolderName__SetAsObject_fn(OSP_accessor_InputField_PlaceHolderName* __this, ::g::Uno::UX::PropertyObject* obj, uObject* v, uObject* origin)
{
    uStackFrame __("OSP_accessor_InputField_PlaceHolderName", "SetAsObject(Uno.UX.PropertyObject,object,Uno.UX.IPropertyListener)");
    uPtr(uCast< ::g::InputField*>(obj, ::TYPES[0/*InputField*/]))->SetPlaceHolderName(uCast<uString*>(v, ::TYPES[1/*string*/]), origin);
}

// public override sealed bool get_SupportsOriginSetter() :349
void OSP_accessor_InputField_PlaceHolderName__get_SupportsOriginSetter_fn(OSP_accessor_InputField_PlaceHolderName* __this, bool* __retval)
{
    return *__retval = true, void();
}

uSStrong< ::g::Uno::UX::PropertyAccessor*> OSP_accessor_InputField_PlaceHolderName::Singleton_;
::g::Uno::UX::Selector OSP_accessor_InputField_PlaceHolderName::_name_;

// public generated OSP_accessor_InputField_PlaceHolderName() [instance] :341
void OSP_accessor_InputField_PlaceHolderName::ctor_1()
{
    ctor_();
}

// public generated OSP_accessor_InputField_PlaceHolderName New() [static] :341
OSP_accessor_InputField_PlaceHolderName* OSP_accessor_InputField_PlaceHolderName::New1()
{
    OSP_accessor_InputField_PlaceHolderName* obj1 = (OSP_accessor_InputField_PlaceHolderName*)uNew(OSP_accessor_InputField_PlaceHolderName_typeof());
    obj1->ctor_1();
    return obj1;
}
// }

} // ::g
