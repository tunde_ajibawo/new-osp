// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#include <_root.OSP_accessor_To-39f04fcf.h>
#include <_root.TopBar.h>
#include <Uno.Bool.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.Type.h>
#include <Uno.UX.IPropertyListener.h>
#include <Uno.UX.PropertyObject.h>
#include <Uno.UX.Selector.h>
static uString* STRINGS[1];
static uType* TYPES[3];

namespace g{

// internal sealed class OSP_accessor_TopBar_Title :311
// {
// static generated OSP_accessor_TopBar_Title() :311
static void OSP_accessor_TopBar_Title__cctor__fn(uType* __type)
{
    OSP_accessor_TopBar_Title::Singleton_ = OSP_accessor_TopBar_Title::New1();
    OSP_accessor_TopBar_Title::_name_ = ::g::Uno::UX::Selector__op_Implicit1(::STRINGS[0/*"Title"*/]);
}

static void OSP_accessor_TopBar_Title_build(uType* type)
{
    ::STRINGS[0] = uString::Const("Title");
    ::TYPES[0] = ::g::TopBar_typeof();
    ::TYPES[1] = ::g::Uno::String_typeof();
    ::TYPES[2] = ::g::Uno::Type_typeof();
    type->SetFields(0,
        ::g::Uno::UX::PropertyAccessor_typeof(), (uintptr_t)&OSP_accessor_TopBar_Title::Singleton_, uFieldFlagsStatic,
        ::g::Uno::UX::Selector_typeof(), (uintptr_t)&OSP_accessor_TopBar_Title::_name_, uFieldFlagsStatic);
}

::g::Uno::UX::PropertyAccessor_type* OSP_accessor_TopBar_Title_typeof()
{
    static uSStrong< ::g::Uno::UX::PropertyAccessor_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Uno::UX::PropertyAccessor_typeof();
    options.FieldCount = 2;
    options.ObjectSize = sizeof(OSP_accessor_TopBar_Title);
    options.TypeSize = sizeof(::g::Uno::UX::PropertyAccessor_type);
    type = (::g::Uno::UX::PropertyAccessor_type*)uClassType::New("OSP_accessor_TopBar_Title", options);
    type->fp_build_ = OSP_accessor_TopBar_Title_build;
    type->fp_ctor_ = (void*)OSP_accessor_TopBar_Title__New1_fn;
    type->fp_cctor_ = OSP_accessor_TopBar_Title__cctor__fn;
    type->fp_GetAsObject = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::PropertyObject*, uObject**))OSP_accessor_TopBar_Title__GetAsObject_fn;
    type->fp_get_Name = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::Selector*))OSP_accessor_TopBar_Title__get_Name_fn;
    type->fp_get_PropertyType = (void(*)(::g::Uno::UX::PropertyAccessor*, uType**))OSP_accessor_TopBar_Title__get_PropertyType_fn;
    type->fp_SetAsObject = (void(*)(::g::Uno::UX::PropertyAccessor*, ::g::Uno::UX::PropertyObject*, uObject*, uObject*))OSP_accessor_TopBar_Title__SetAsObject_fn;
    type->fp_get_SupportsOriginSetter = (void(*)(::g::Uno::UX::PropertyAccessor*, bool*))OSP_accessor_TopBar_Title__get_SupportsOriginSetter_fn;
    return type;
}

// public generated OSP_accessor_TopBar_Title() :311
void OSP_accessor_TopBar_Title__ctor_1_fn(OSP_accessor_TopBar_Title* __this)
{
    __this->ctor_1();
}

// public override sealed object GetAsObject(Uno.UX.PropertyObject obj) :317
void OSP_accessor_TopBar_Title__GetAsObject_fn(OSP_accessor_TopBar_Title* __this, ::g::Uno::UX::PropertyObject* obj, uObject** __retval)
{
    uStackFrame __("OSP_accessor_TopBar_Title", "GetAsObject(Uno.UX.PropertyObject)");
    return *__retval = uPtr(uCast< ::g::TopBar*>(obj, ::TYPES[0/*TopBar*/]))->Title(), void();
}

// public override sealed Uno.UX.Selector get_Name() :314
void OSP_accessor_TopBar_Title__get_Name_fn(OSP_accessor_TopBar_Title* __this, ::g::Uno::UX::Selector* __retval)
{
    return *__retval = OSP_accessor_TopBar_Title::_name_, void();
}

// public generated OSP_accessor_TopBar_Title New() :311
void OSP_accessor_TopBar_Title__New1_fn(OSP_accessor_TopBar_Title** __retval)
{
    *__retval = OSP_accessor_TopBar_Title::New1();
}

// public override sealed Uno.Type get_PropertyType() :316
void OSP_accessor_TopBar_Title__get_PropertyType_fn(OSP_accessor_TopBar_Title* __this, uType** __retval)
{
    return *__retval = ::TYPES[1/*string*/], void();
}

// public override sealed void SetAsObject(Uno.UX.PropertyObject obj, object v, Uno.UX.IPropertyListener origin) :318
void OSP_accessor_TopBar_Title__SetAsObject_fn(OSP_accessor_TopBar_Title* __this, ::g::Uno::UX::PropertyObject* obj, uObject* v, uObject* origin)
{
    uStackFrame __("OSP_accessor_TopBar_Title", "SetAsObject(Uno.UX.PropertyObject,object,Uno.UX.IPropertyListener)");
    uPtr(uCast< ::g::TopBar*>(obj, ::TYPES[0/*TopBar*/]))->SetTitle(uCast<uString*>(v, ::TYPES[1/*string*/]), origin);
}

// public override sealed bool get_SupportsOriginSetter() :319
void OSP_accessor_TopBar_Title__get_SupportsOriginSetter_fn(OSP_accessor_TopBar_Title* __this, bool* __retval)
{
    return *__retval = true, void();
}

uSStrong< ::g::Uno::UX::PropertyAccessor*> OSP_accessor_TopBar_Title::Singleton_;
::g::Uno::UX::Selector OSP_accessor_TopBar_Title::_name_;

// public generated OSP_accessor_TopBar_Title() [instance] :311
void OSP_accessor_TopBar_Title::ctor_1()
{
    ctor_();
}

// public generated OSP_accessor_TopBar_Title New() [static] :311
OSP_accessor_TopBar_Title* OSP_accessor_TopBar_Title::New1()
{
    OSP_accessor_TopBar_Title* obj1 = (OSP_accessor_TopBar_Title*)uNew(OSP_accessor_TopBar_Title_typeof());
    obj1->ctor_1();
    return obj1;
}
// }

} // ::g
