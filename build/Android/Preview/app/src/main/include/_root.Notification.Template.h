// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/Notification.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.UX.Template.h>
namespace g{struct Notification;}
namespace g{struct Notification__Template;}

namespace g{

// public partial sealed class Notification.Template :6
// {
::g::Uno::UX::Template_type* Notification__Template_typeof();
void Notification__Template__ctor_1_fn(Notification__Template* __this, ::g::Notification* parent, ::g::Notification* parentInstance);
void Notification__Template__New1_fn(Notification__Template* __this, uObject** __retval);
void Notification__Template__New2_fn(::g::Notification* parent, ::g::Notification* parentInstance, Notification__Template** __retval);

struct Notification__Template : ::g::Uno::UX::Template
{
    uWeak< ::g::Notification*> __parent1;
    uWeak< ::g::Notification*> __parentInstance1;

    void ctor_1(::g::Notification* parent, ::g::Notification* parentInstance);
    static Notification__Template* New2(::g::Notification* parent, ::g::Notification* parentInstance);
};
// }

} // ::g
