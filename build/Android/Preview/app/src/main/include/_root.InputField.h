// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/InputField.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Fuse.Animations.IResize.h>
#include <Fuse.Binding.h>
#include <Fuse.Controls.Panel.h>
#include <Fuse.Drawing.ISurfaceDrawable.h>
#include <Fuse.IActualPlacement.h>
#include <Fuse.INotifyUnrooted.h>
#include <Fuse.IProperties.h>
#include <Fuse.ISourceLocation.h>
#include <Fuse.ITemplateSource.h>
#include <Fuse.Node.h>
#include <Fuse.Scripting.IScriptObject.h>
#include <Fuse.Triggers.Actions.IHide.h>
#include <Fuse.Triggers.Actions.IShow.h>
#include <Fuse.Triggers.Actions-ea70af1f.h>
#include <Fuse.Visual.h>
#include <Uno.Collections.ICollection-1.h>
#include <Uno.Collections.IEnumerable-1.h>
#include <Uno.Collections.IList-1.h>
#include <Uno.Float4.h>
#include <Uno.UX.IPropertyListener.h>
namespace g{namespace Uno{namespace UX{struct Property1;}}}
namespace g{namespace Uno{namespace UX{struct Selector;}}}
namespace g{struct InputField;}

namespace g{

// public partial sealed class InputField :2
// {
::g::Fuse::Controls::Panel_type* InputField_typeof();
void InputField__ctor_7_fn(InputField* __this);
void InputField__get_BorderColor_fn(InputField* __this, ::g::Uno::Float4* __retval);
void InputField__set_BorderColor_fn(InputField* __this, ::g::Uno::Float4* value);
void InputField__get_FieldColor_fn(InputField* __this, ::g::Uno::Float4* __retval);
void InputField__set_FieldColor_fn(InputField* __this, ::g::Uno::Float4* value);
void InputField__InitializeUX_fn(InputField* __this);
void InputField__New4_fn(InputField** __retval);
void InputField__get_PlaceHolderName_fn(InputField* __this, uString** __retval);
void InputField__set_PlaceHolderName_fn(InputField* __this, uString* value);
void InputField__SetBorderColor_fn(InputField* __this, ::g::Uno::Float4* value, uObject* origin);
void InputField__SetFieldColor_fn(InputField* __this, ::g::Uno::Float4* value, uObject* origin);
void InputField__SetPlaceHolderName_fn(InputField* __this, uString* value, uObject* origin);
void InputField__SetTitle_fn(InputField* __this, uString* value, uObject* origin);
void InputField__get_Title_fn(InputField* __this, uString** __retval);
void InputField__set_Title_fn(InputField* __this, uString* value);

struct InputField : ::g::Fuse::Controls::Panel
{
    uStrong<uString*> _field_PlaceHolderName;
    uStrong<uString*> _field_Title;
    ::g::Uno::Float4 _field_BorderColor;
    ::g::Uno::Float4 _field_FieldColor;
    uStrong< ::g::Uno::UX::Property1*> temp_Value_inst;
    uStrong< ::g::Uno::UX::Property1*> temp_TextColor_inst;
    uStrong< ::g::Uno::UX::Property1*> temp1_Value_inst;
    uStrong< ::g::Uno::UX::Property1*> temp1_TextColor_inst;
    uStrong< ::g::Uno::UX::Property1*> temp2_Color_inst;
    static ::g::Uno::UX::Selector __selector0_;
    static ::g::Uno::UX::Selector& __selector0() { return InputField_typeof()->Init(), __selector0_; }
    static ::g::Uno::UX::Selector __selector1_;
    static ::g::Uno::UX::Selector& __selector1() { return InputField_typeof()->Init(), __selector1_; }
    static ::g::Uno::UX::Selector __selector2_;
    static ::g::Uno::UX::Selector& __selector2() { return InputField_typeof()->Init(), __selector2_; }

    void ctor_7();
    ::g::Uno::Float4 BorderColor();
    void BorderColor(::g::Uno::Float4 value);
    ::g::Uno::Float4 FieldColor();
    void FieldColor(::g::Uno::Float4 value);
    void InitializeUX();
    uString* PlaceHolderName();
    void PlaceHolderName(uString* value);
    void SetBorderColor(::g::Uno::Float4 value, uObject* origin);
    void SetFieldColor(::g::Uno::Float4 value, uObject* origin);
    void SetPlaceHolderName(uString* value, uObject* origin);
    void SetTitle(uString* value, uObject* origin);
    uString* Title();
    void Title(uString* value);
    static InputField* New4();
};
// }

} // ::g
