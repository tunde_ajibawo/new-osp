// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.UX.PropertyAccessor.h>
namespace g{namespace Uno{namespace UX{struct PropertyObject;}}}
namespace g{namespace Uno{namespace UX{struct Selector;}}}
namespace g{struct OSP_accessor_InputField_FieldColor;}

namespace g{

// internal sealed class OSP_accessor_InputField_FieldColor :331
// {
::g::Uno::UX::PropertyAccessor_type* OSP_accessor_InputField_FieldColor_typeof();
void OSP_accessor_InputField_FieldColor__ctor_1_fn(OSP_accessor_InputField_FieldColor* __this);
void OSP_accessor_InputField_FieldColor__GetAsObject_fn(OSP_accessor_InputField_FieldColor* __this, ::g::Uno::UX::PropertyObject* obj, uObject** __retval);
void OSP_accessor_InputField_FieldColor__get_Name_fn(OSP_accessor_InputField_FieldColor* __this, ::g::Uno::UX::Selector* __retval);
void OSP_accessor_InputField_FieldColor__New1_fn(OSP_accessor_InputField_FieldColor** __retval);
void OSP_accessor_InputField_FieldColor__get_PropertyType_fn(OSP_accessor_InputField_FieldColor* __this, uType** __retval);
void OSP_accessor_InputField_FieldColor__SetAsObject_fn(OSP_accessor_InputField_FieldColor* __this, ::g::Uno::UX::PropertyObject* obj, uObject* v, uObject* origin);
void OSP_accessor_InputField_FieldColor__get_SupportsOriginSetter_fn(OSP_accessor_InputField_FieldColor* __this, bool* __retval);

struct OSP_accessor_InputField_FieldColor : ::g::Uno::UX::PropertyAccessor
{
    static uSStrong< ::g::Uno::UX::PropertyAccessor*> Singleton_;
    static uSStrong< ::g::Uno::UX::PropertyAccessor*>& Singleton() { return OSP_accessor_InputField_FieldColor_typeof()->Init(), Singleton_; }
    static ::g::Uno::UX::Selector _name_;
    static ::g::Uno::UX::Selector& _name() { return OSP_accessor_InputField_FieldColor_typeof()->Init(), _name_; }

    void ctor_1();
    static OSP_accessor_InputField_FieldColor* New1();
};
// }

} // ::g
