// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.Float.h>
#include <Uno.UX.Property1-1.h>
namespace g{namespace Fuse{struct Translation;}}
namespace g{namespace Uno{namespace UX{struct PropertyObject;}}}
namespace g{namespace Uno{namespace UX{struct Selector;}}}
namespace g{struct OSP_FuseTranslation_Y_Property;}

namespace g{

// internal sealed class OSP_FuseTranslation_Y_Property :411
// {
::g::Uno::UX::Property1_type* OSP_FuseTranslation_Y_Property_typeof();
void OSP_FuseTranslation_Y_Property__ctor_3_fn(OSP_FuseTranslation_Y_Property* __this, ::g::Fuse::Translation* obj, ::g::Uno::UX::Selector* name);
void OSP_FuseTranslation_Y_Property__Get1_fn(OSP_FuseTranslation_Y_Property* __this, ::g::Uno::UX::PropertyObject* obj, float* __retval);
void OSP_FuseTranslation_Y_Property__New1_fn(::g::Fuse::Translation* obj, ::g::Uno::UX::Selector* name, OSP_FuseTranslation_Y_Property** __retval);
void OSP_FuseTranslation_Y_Property__get_Object_fn(OSP_FuseTranslation_Y_Property* __this, ::g::Uno::UX::PropertyObject** __retval);
void OSP_FuseTranslation_Y_Property__Set1_fn(OSP_FuseTranslation_Y_Property* __this, ::g::Uno::UX::PropertyObject* obj, float* v, uObject* origin);

struct OSP_FuseTranslation_Y_Property : ::g::Uno::UX::Property1
{
    uWeak< ::g::Fuse::Translation*> _obj;

    void ctor_3(::g::Fuse::Translation* obj, ::g::Uno::UX::Selector name);
    static OSP_FuseTranslation_Y_Property* New1(::g::Fuse::Translation* obj, ::g::Uno::UX::Selector name);
};
// }

} // ::g
