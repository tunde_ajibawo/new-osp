// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.UX.PropertyAccessor.h>
namespace g{namespace Uno{namespace UX{struct PropertyObject;}}}
namespace g{namespace Uno{namespace UX{struct Selector;}}}
namespace g{struct OSP_accessor_SortableList_Label;}

namespace g{

// internal sealed class OSP_accessor_SortableList_Label :11
// {
::g::Uno::UX::PropertyAccessor_type* OSP_accessor_SortableList_Label_typeof();
void OSP_accessor_SortableList_Label__ctor_1_fn(OSP_accessor_SortableList_Label* __this);
void OSP_accessor_SortableList_Label__GetAsObject_fn(OSP_accessor_SortableList_Label* __this, ::g::Uno::UX::PropertyObject* obj, uObject** __retval);
void OSP_accessor_SortableList_Label__get_Name_fn(OSP_accessor_SortableList_Label* __this, ::g::Uno::UX::Selector* __retval);
void OSP_accessor_SortableList_Label__New1_fn(OSP_accessor_SortableList_Label** __retval);
void OSP_accessor_SortableList_Label__get_PropertyType_fn(OSP_accessor_SortableList_Label* __this, uType** __retval);
void OSP_accessor_SortableList_Label__SetAsObject_fn(OSP_accessor_SortableList_Label* __this, ::g::Uno::UX::PropertyObject* obj, uObject* v, uObject* origin);
void OSP_accessor_SortableList_Label__get_SupportsOriginSetter_fn(OSP_accessor_SortableList_Label* __this, bool* __retval);

struct OSP_accessor_SortableList_Label : ::g::Uno::UX::PropertyAccessor
{
    static uSStrong< ::g::Uno::UX::PropertyAccessor*> Singleton_;
    static uSStrong< ::g::Uno::UX::PropertyAccessor*>& Singleton() { return OSP_accessor_SortableList_Label_typeof()->Init(), Singleton_; }
    static ::g::Uno::UX::Selector _name_;
    static ::g::Uno::UX::Selector& _name() { return OSP_accessor_SortableList_Label_typeof()->Init(), _name_; }

    void ctor_1();
    static OSP_accessor_SortableList_Label* New1();
};
// }

} // ::g
