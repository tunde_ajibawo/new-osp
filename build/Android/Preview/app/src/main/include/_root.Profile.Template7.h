// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/Profile.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.UX.Template.h>
namespace g{struct Profile;}
namespace g{struct Profile__Template7;}

namespace g{

// public partial sealed class Profile.Template7 :222
// {
::g::Uno::UX::Template_type* Profile__Template7_typeof();
void Profile__Template7__ctor_1_fn(Profile__Template7* __this, ::g::Profile* parent, ::g::Profile* parentInstance);
void Profile__Template7__New1_fn(Profile__Template7* __this, uObject** __retval);
void Profile__Template7__New2_fn(::g::Profile* parent, ::g::Profile* parentInstance, Profile__Template7** __retval);

struct Profile__Template7 : ::g::Uno::UX::Template
{
    uWeak< ::g::Profile*> __parent1;
    uWeak< ::g::Profile*> __parentInstance1;

    void ctor_1(::g::Profile* parent, ::g::Profile* parentInstance);
    static Profile__Template7* New2(::g::Profile* parent, ::g::Profile* parentInstance);
};
// }

} // ::g
