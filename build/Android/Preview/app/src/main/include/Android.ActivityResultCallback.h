// This file was generated based on /usr/local/share/uno/Packages/Android.ActivityUtils/1.9.0/ActivityUtils.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.Delegate.h>

namespace g{
namespace Android{

// public extern delegate void ActivityResultCallback(int resultCode, Java.Object intent, object info) :9
uDelegateType* ActivityResultCallback_typeof();

}} // ::g::Android
