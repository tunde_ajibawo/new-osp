// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/SortableList.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.UX.Template.h>
namespace g{struct SortableList;}
namespace g{struct SortableList__Template;}

namespace g{

// public partial sealed class SortableList.Template :36
// {
::g::Uno::UX::Template_type* SortableList__Template_typeof();
void SortableList__Template__ctor_1_fn(SortableList__Template* __this, ::g::SortableList* parent, ::g::SortableList* parentInstance);
void SortableList__Template__New1_fn(SortableList__Template* __this, uObject** __retval);
void SortableList__Template__New2_fn(::g::SortableList* parent, ::g::SortableList* parentInstance, SortableList__Template** __retval);

struct SortableList__Template : ::g::Uno::UX::Template
{
    uWeak< ::g::SortableList*> __parent1;
    uWeak< ::g::SortableList*> __parentInstance1;

    void ctor_1(::g::SortableList* parent, ::g::SortableList* parentInstance);
    static SortableList__Template* New2(::g::SortableList* parent, ::g::SortableList* parentInstance);
};
// }

} // ::g
