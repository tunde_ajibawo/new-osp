// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/SubBut.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Fuse.Animations.IResize.h>
#include <Fuse.Binding.h>
#include <Fuse.Controls.Button.h>
#include <Fuse.Drawing.ISurfaceDrawable.h>
#include <Fuse.IActualPlacement.h>
#include <Fuse.INotifyUnrooted.h>
#include <Fuse.IProperties.h>
#include <Fuse.ISourceLocation.h>
#include <Fuse.ITemplateSource.h>
#include <Fuse.Node.h>
#include <Fuse.Scripting.IScriptObject.h>
#include <Fuse.Triggers.Actions.IHide.h>
#include <Fuse.Triggers.Actions.IShow.h>
#include <Fuse.Triggers.Actions-ea70af1f.h>
#include <Fuse.Visual.h>
#include <Uno.Collections.ICollection-1.h>
#include <Uno.Collections.IEnumerable-1.h>
#include <Uno.Collections.IList-1.h>
#include <Uno.Float4.h>
#include <Uno.UX.IPropertyListener.h>
namespace g{namespace Uno{namespace UX{struct Property1;}}}
namespace g{namespace Uno{namespace UX{struct Selector;}}}
namespace g{struct SubBut;}

namespace g{

// public partial sealed class SubBut :2
// {
::g::Fuse::Controls::Panel_type* SubBut_typeof();
void SubBut__ctor_9_fn(SubBut* __this);
void SubBut__get_Btext_fn(SubBut* __this, uString** __retval);
void SubBut__set_Btext_fn(SubBut* __this, uString* value);
void SubBut__get_Direction_fn(SubBut* __this, uString** __retval);
void SubBut__set_Direction_fn(SubBut* __this, uString* value);
void SubBut__get_Icon_fn(SubBut* __this, uString** __retval);
void SubBut__set_Icon_fn(SubBut* __this, uString* value);
void SubBut__get_IconColor_fn(SubBut* __this, ::g::Uno::Float4* __retval);
void SubBut__set_IconColor_fn(SubBut* __this, ::g::Uno::Float4* value);
void SubBut__InitializeUX1_fn(SubBut* __this);
void SubBut__New6_fn(SubBut** __retval);
void SubBut__SetBtext_fn(SubBut* __this, uString* value, uObject* origin);
void SubBut__SetDirection_fn(SubBut* __this, uString* value, uObject* origin);
void SubBut__SetIcon_fn(SubBut* __this, uString* value, uObject* origin);
void SubBut__SetIconColor_fn(SubBut* __this, ::g::Uno::Float4* value, uObject* origin);

struct SubBut : ::g::Fuse::Controls::Button
{
    uStrong<uString*> _field_Btext;
    uStrong<uString*> _field_Icon;
    uStrong<uString*> _field_Direction;
    ::g::Uno::Float4 _field_IconColor;
    uStrong< ::g::Uno::UX::Property1*> temp_Value_inst;
    uStrong< ::g::Uno::UX::Property1*> temp_TextColor_inst;
    uStrong< ::g::Uno::UX::Property1*> temp1_Value_inst;
    uStrong< ::g::Uno::UX::Property1*> this_DockPanel_Dock_inst;
    static ::g::Uno::UX::Selector __selector0_;
    static ::g::Uno::UX::Selector& __selector0() { return SubBut_typeof()->Init(), __selector0_; }
    static ::g::Uno::UX::Selector __selector1_;
    static ::g::Uno::UX::Selector& __selector1() { return SubBut_typeof()->Init(), __selector1_; }
    static ::g::Uno::UX::Selector __selector2_;
    static ::g::Uno::UX::Selector& __selector2() { return SubBut_typeof()->Init(), __selector2_; }

    void ctor_9();
    uString* Btext();
    void Btext(uString* value);
    uString* Direction();
    void Direction(uString* value);
    uString* Icon();
    void Icon(uString* value);
    ::g::Uno::Float4 IconColor();
    void IconColor(::g::Uno::Float4 value);
    void InitializeUX1();
    void SetBtext(uString* value, uObject* origin);
    void SetDirection(uString* value, uObject* origin);
    void SetIcon(uString* value, uObject* origin);
    void SetIconColor(::g::Uno::Float4 value, uObject* origin);
    static SubBut* New6();
};
// }

} // ::g
