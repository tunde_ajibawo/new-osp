// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Fuse.Layouts.Dock.h>
#include <Uno.UX.Property1-1.h>
namespace g{namespace Fuse{namespace Elements{struct Element;}}}
namespace g{namespace Uno{namespace UX{struct PropertyObject;}}}
namespace g{namespace Uno{namespace UX{struct Selector;}}}
namespace g{struct OSP_FuseElementsElement_DockPanelDock_Property;}

namespace g{

// internal sealed class OSP_FuseElementsElement_DockPanelDock_Property :660
// {
::g::Uno::UX::Property1_type* OSP_FuseElementsElement_DockPanelDock_Property_typeof();
void OSP_FuseElementsElement_DockPanelDock_Property__ctor_3_fn(OSP_FuseElementsElement_DockPanelDock_Property* __this, ::g::Fuse::Elements::Element* obj, ::g::Uno::UX::Selector* name);
void OSP_FuseElementsElement_DockPanelDock_Property__Get1_fn(OSP_FuseElementsElement_DockPanelDock_Property* __this, ::g::Uno::UX::PropertyObject* obj, int32_t* __retval);
void OSP_FuseElementsElement_DockPanelDock_Property__New1_fn(::g::Fuse::Elements::Element* obj, ::g::Uno::UX::Selector* name, OSP_FuseElementsElement_DockPanelDock_Property** __retval);
void OSP_FuseElementsElement_DockPanelDock_Property__get_Object_fn(OSP_FuseElementsElement_DockPanelDock_Property* __this, ::g::Uno::UX::PropertyObject** __retval);
void OSP_FuseElementsElement_DockPanelDock_Property__Set1_fn(OSP_FuseElementsElement_DockPanelDock_Property* __this, ::g::Uno::UX::PropertyObject* obj, int32_t* v, uObject* origin);

struct OSP_FuseElementsElement_DockPanelDock_Property : ::g::Uno::UX::Property1
{
    uWeak< ::g::Fuse::Elements::Element*> _obj;

    void ctor_3(::g::Fuse::Elements::Element* obj, ::g::Uno::UX::Selector name);
    static OSP_FuseElementsElement_DockPanelDock_Property* New1(::g::Fuse::Elements::Element* obj, ::g::Uno::UX::Selector name);
};
// }

} // ::g
