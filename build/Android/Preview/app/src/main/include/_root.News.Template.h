// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/News.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.UX.Template.h>
namespace g{struct News;}
namespace g{struct News__Template;}

namespace g{

// public partial sealed class News.Template :6
// {
::g::Uno::UX::Template_type* News__Template_typeof();
void News__Template__ctor_1_fn(News__Template* __this, ::g::News* parent, ::g::News* parentInstance);
void News__Template__New1_fn(News__Template* __this, uObject** __retval);
void News__Template__New2_fn(::g::News* parent, ::g::News* parentInstance, News__Template** __retval);

struct News__Template : ::g::Uno::UX::Template
{
    uWeak< ::g::News*> __parent1;
    uWeak< ::g::News*> __parentInstance1;

    void ctor_1(::g::News* parent, ::g::News* parentInstance);
    static News__Template* New2(::g::News* parent, ::g::News* parentInstance);
};
// }

} // ::g
