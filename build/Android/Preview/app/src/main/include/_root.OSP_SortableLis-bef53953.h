// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.String.h>
#include <Uno.UX.Property1-1.h>
namespace g{namespace Uno{namespace UX{struct PropertyObject;}}}
namespace g{namespace Uno{namespace UX{struct Selector;}}}
namespace g{struct OSP_SortableList_Label_Property;}
namespace g{struct SortableList;}

namespace g{

// internal sealed class OSP_SortableList_Label_Property :702
// {
::g::Uno::UX::Property1_type* OSP_SortableList_Label_Property_typeof();
void OSP_SortableList_Label_Property__ctor_3_fn(OSP_SortableList_Label_Property* __this, ::g::SortableList* obj, ::g::Uno::UX::Selector* name);
void OSP_SortableList_Label_Property__Get1_fn(OSP_SortableList_Label_Property* __this, ::g::Uno::UX::PropertyObject* obj, uString** __retval);
void OSP_SortableList_Label_Property__New1_fn(::g::SortableList* obj, ::g::Uno::UX::Selector* name, OSP_SortableList_Label_Property** __retval);
void OSP_SortableList_Label_Property__get_Object_fn(OSP_SortableList_Label_Property* __this, ::g::Uno::UX::PropertyObject** __retval);
void OSP_SortableList_Label_Property__Set1_fn(OSP_SortableList_Label_Property* __this, ::g::Uno::UX::PropertyObject* obj, uString* v, uObject* origin);
void OSP_SortableList_Label_Property__get_SupportsOriginSetter_fn(OSP_SortableList_Label_Property* __this, bool* __retval);

struct OSP_SortableList_Label_Property : ::g::Uno::UX::Property1
{
    uWeak< ::g::SortableList*> _obj;

    void ctor_3(::g::SortableList* obj, ::g::Uno::UX::Selector name);
    static OSP_SortableList_Label_Property* New1(::g::SortableList* obj, ::g::Uno::UX::Selector name);
};
// }

} // ::g
