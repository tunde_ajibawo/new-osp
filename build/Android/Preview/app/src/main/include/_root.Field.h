// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/Field.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Fuse.Animations.IResize.h>
#include <Fuse.Binding.h>
#include <Fuse.Controls.DockPanel.h>
#include <Fuse.Drawing.ISurfaceDrawable.h>
#include <Fuse.IActualPlacement.h>
#include <Fuse.INotifyUnrooted.h>
#include <Fuse.IProperties.h>
#include <Fuse.ISourceLocation.h>
#include <Fuse.ITemplateSource.h>
#include <Fuse.Node.h>
#include <Fuse.Scripting.IScriptObject.h>
#include <Fuse.Triggers.Actions.IHide.h>
#include <Fuse.Triggers.Actions.IShow.h>
#include <Fuse.Triggers.Actions-ea70af1f.h>
#include <Fuse.Visual.h>
#include <Uno.Collections.ICollection-1.h>
#include <Uno.Collections.IEnumerable-1.h>
#include <Uno.Collections.IList-1.h>
#include <Uno.Float4.h>
#include <Uno.UX.IPropertyListener.h>
namespace g{namespace Uno{namespace UX{struct Property1;}}}
namespace g{namespace Uno{namespace UX{struct Selector;}}}
namespace g{struct Field;}

namespace g{

// public partial sealed class Field :2
// {
::g::Fuse::Controls::Panel_type* Field_typeof();
void Field__ctor_8_fn(Field* __this);
void Field__get_FieldColor_fn(Field* __this, ::g::Uno::Float4* __retval);
void Field__set_FieldColor_fn(Field* __this, ::g::Uno::Float4* value);
void Field__InitializeUX_fn(Field* __this);
void Field__New5_fn(Field** __retval);
void Field__get_PlaceHolderName_fn(Field* __this, uString** __retval);
void Field__set_PlaceHolderName_fn(Field* __this, uString* value);
void Field__SetFieldColor_fn(Field* __this, ::g::Uno::Float4* value, uObject* origin);
void Field__SetPlaceHolderName_fn(Field* __this, uString* value, uObject* origin);
void Field__SetTitle_fn(Field* __this, uString* value, uObject* origin);
void Field__get_Title_fn(Field* __this, uString** __retval);
void Field__set_Title_fn(Field* __this, uString* value);

struct Field : ::g::Fuse::Controls::DockPanel
{
    uStrong<uString*> _field_PlaceHolderName;
    uStrong<uString*> _field_Title;
    ::g::Uno::Float4 _field_FieldColor;
    uStrong< ::g::Uno::UX::Property1*> temp_Value_inst;
    uStrong< ::g::Uno::UX::Property1*> temp_TextColor_inst;
    uStrong< ::g::Uno::UX::Property1*> temp1_Value_inst;
    uStrong< ::g::Uno::UX::Property1*> temp1_TextColor_inst;
    static ::g::Uno::UX::Selector __selector0_;
    static ::g::Uno::UX::Selector& __selector0() { return Field_typeof()->Init(), __selector0_; }
    static ::g::Uno::UX::Selector __selector1_;
    static ::g::Uno::UX::Selector& __selector1() { return Field_typeof()->Init(), __selector1_; }

    void ctor_8();
    ::g::Uno::Float4 FieldColor();
    void FieldColor(::g::Uno::Float4 value);
    void InitializeUX();
    uString* PlaceHolderName();
    void PlaceHolderName(uString* value);
    void SetFieldColor(::g::Uno::Float4 value, uObject* origin);
    void SetPlaceHolderName(uString* value, uObject* origin);
    void SetTitle(uString* value, uObject* origin);
    uString* Title();
    void Title(uString* value);
    static Field* New5();
};
// }

} // ::g
