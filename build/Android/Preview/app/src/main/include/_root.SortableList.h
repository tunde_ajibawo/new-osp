// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/SortableList.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Fuse.Animations.IResize.h>
#include <Fuse.Binding.h>
#include <Fuse.Controls.Panel.h>
#include <Fuse.Drawing.ISurfaceDrawable.h>
#include <Fuse.IActualPlacement.h>
#include <Fuse.INotifyUnrooted.h>
#include <Fuse.IProperties.h>
#include <Fuse.ISourceLocation.h>
#include <Fuse.ITemplateSource.h>
#include <Fuse.Node.h>
#include <Fuse.Scripting.IScriptObject.h>
#include <Fuse.Triggers.Actions.IHide.h>
#include <Fuse.Triggers.Actions.IShow.h>
#include <Fuse.Triggers.Actions-ea70af1f.h>
#include <Fuse.Visual.h>
#include <Uno.Collections.ICollection-1.h>
#include <Uno.Collections.IEnumerable-1.h>
#include <Uno.Collections.IList-1.h>
#include <Uno.UX.IPropertyListener.h>
namespace g{namespace Fuse{namespace Controls{struct ScrollView;}}}
namespace g{namespace Uno{namespace UX{struct NameTable;}}}
namespace g{namespace Uno{namespace UX{struct Property1;}}}
namespace g{namespace Uno{namespace UX{struct Selector;}}}
namespace g{struct SortableList;}

namespace g{

// public partial sealed class SortableList :2
// {
::g::Fuse::Controls::Panel_type* SortableList_typeof();
void SortableList__ctor_7_fn(SortableList* __this, ::g::Fuse::Controls::ScrollView* parentScrollView1);
void SortableList__InitializeUX_fn(SortableList* __this);
void SortableList__get_Items_fn(SortableList* __this, uObject** __retval);
void SortableList__set_Items_fn(SortableList* __this, uObject* value);
void SortableList__get_Label_fn(SortableList* __this, uString** __retval);
void SortableList__set_Label_fn(SortableList* __this, uString* value);
void SortableList__New4_fn(::g::Fuse::Controls::ScrollView* parentScrollView1, SortableList** __retval);
void SortableList__SetItems_fn(SortableList* __this, uObject* value, uObject* origin);
void SortableList__SetLabel_fn(SortableList* __this, uString* value, uObject* origin);

struct SortableList : ::g::Fuse::Controls::Panel
{
    uStrong<uObject*> _field_Items;
    uStrong<uString*> _field_Label;
    uStrong< ::g::Fuse::Controls::ScrollView*> parentScrollView;
    uStrong< ::g::Uno::UX::Property1*> temp_Value_inst;
    uStrong< ::g::Uno::UX::Property1*> temp1_Items_inst;
    uStrong< ::g::Uno::UX::Property1*> this_Items_inst;
    uStrong< ::g::Uno::UX::Property1*> this_Label_inst;
    uStrong< ::g::Uno::UX::NameTable*> __g_nametable1;
    static uSStrong<uArray*> __g_static_nametable1_;
    static uSStrong<uArray*>& __g_static_nametable1() { return SortableList_typeof()->Init(), __g_static_nametable1_; }
    static ::g::Uno::UX::Selector __selector0_;
    static ::g::Uno::UX::Selector& __selector0() { return SortableList_typeof()->Init(), __selector0_; }
    static ::g::Uno::UX::Selector __selector1_;
    static ::g::Uno::UX::Selector& __selector1() { return SortableList_typeof()->Init(), __selector1_; }
    static ::g::Uno::UX::Selector __selector2_;
    static ::g::Uno::UX::Selector& __selector2() { return SortableList_typeof()->Init(), __selector2_; }

    void ctor_7(::g::Fuse::Controls::ScrollView* parentScrollView1);
    void InitializeUX();
    uObject* Items();
    void Items(uObject* value);
    uString* Label();
    void Label(uString* value);
    void SetItems(uObject* value, uObject* origin);
    void SetLabel(uString* value, uObject* origin);
    static SortableList* New4(::g::Fuse::Controls::ScrollView* parentScrollView1);
};
// }

} // ::g
