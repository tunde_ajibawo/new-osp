// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/WorkInfo.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.UX.Template.h>
namespace g{namespace Uno{namespace UX{struct Property1;}}}
namespace g{namespace Uno{namespace UX{struct Selector;}}}
namespace g{struct WorkInfo;}
namespace g{struct WorkInfo__Template8;}

namespace g{

// public partial sealed class WorkInfo.Template8 :246
// {
::g::Uno::UX::Template_type* WorkInfo__Template8_typeof();
void WorkInfo__Template8__ctor_1_fn(WorkInfo__Template8* __this, ::g::WorkInfo* parent, ::g::WorkInfo* parentInstance);
void WorkInfo__Template8__New1_fn(WorkInfo__Template8* __this, uObject** __retval);
void WorkInfo__Template8__New2_fn(::g::WorkInfo* parent, ::g::WorkInfo* parentInstance, WorkInfo__Template8** __retval);

struct WorkInfo__Template8 : ::g::Uno::UX::Template
{
    uWeak< ::g::WorkInfo*> __parent1;
    uWeak< ::g::WorkInfo*> __parentInstance1;
    uStrong< ::g::Uno::UX::Property1*> __self_PlaceHolderName_inst1;
    static ::g::Uno::UX::Selector __selector0_;
    static ::g::Uno::UX::Selector& __selector0() { return WorkInfo__Template8_typeof()->Init(), __selector0_; }

    void ctor_1(::g::WorkInfo* parent, ::g::WorkInfo* parentInstance);
    static WorkInfo__Template8* New2(::g::WorkInfo* parent, ::g::WorkInfo* parentInstance);
};
// }

} // ::g
