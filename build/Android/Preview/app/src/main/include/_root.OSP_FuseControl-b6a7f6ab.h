// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.UX.FileSource.h>
#include <Uno.UX.Property1-1.h>
namespace g{namespace Fuse{namespace Controls{struct Image;}}}
namespace g{namespace Uno{namespace UX{struct PropertyObject;}}}
namespace g{namespace Uno{namespace UX{struct Selector;}}}
namespace g{struct OSP_FuseControlsImage_File_Property;}

namespace g{

// internal sealed class OSP_FuseControlsImage_File_Property :668
// {
::g::Uno::UX::Property1_type* OSP_FuseControlsImage_File_Property_typeof();
void OSP_FuseControlsImage_File_Property__ctor_3_fn(OSP_FuseControlsImage_File_Property* __this, ::g::Fuse::Controls::Image* obj, ::g::Uno::UX::Selector* name);
void OSP_FuseControlsImage_File_Property__Get1_fn(OSP_FuseControlsImage_File_Property* __this, ::g::Uno::UX::PropertyObject* obj, ::g::Uno::UX::FileSource** __retval);
void OSP_FuseControlsImage_File_Property__New1_fn(::g::Fuse::Controls::Image* obj, ::g::Uno::UX::Selector* name, OSP_FuseControlsImage_File_Property** __retval);
void OSP_FuseControlsImage_File_Property__get_Object_fn(OSP_FuseControlsImage_File_Property* __this, ::g::Uno::UX::PropertyObject** __retval);
void OSP_FuseControlsImage_File_Property__Set1_fn(OSP_FuseControlsImage_File_Property* __this, ::g::Uno::UX::PropertyObject* obj, ::g::Uno::UX::FileSource* v, uObject* origin);

struct OSP_FuseControlsImage_File_Property : ::g::Uno::UX::Property1
{
    uWeak< ::g::Fuse::Controls::Image*> _obj;

    void ctor_3(::g::Fuse::Controls::Image* obj, ::g::Uno::UX::Selector name);
    static OSP_FuseControlsImage_File_Property* New1(::g::Fuse::Controls::Image* obj, ::g::Uno::UX::Selector name);
};
// }

} // ::g
