// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/SquareButton.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Fuse.Animations.IResize.h>
#include <Fuse.Binding.h>
#include <Fuse.Controls.Button.h>
#include <Fuse.Drawing.ISurfaceDrawable.h>
#include <Fuse.IActualPlacement.h>
#include <Fuse.INotifyUnrooted.h>
#include <Fuse.IProperties.h>
#include <Fuse.ISourceLocation.h>
#include <Fuse.ITemplateSource.h>
#include <Fuse.Node.h>
#include <Fuse.Scripting.IScriptObject.h>
#include <Fuse.Triggers.Actions.IHide.h>
#include <Fuse.Triggers.Actions.IShow.h>
#include <Fuse.Triggers.Actions-ea70af1f.h>
#include <Fuse.Visual.h>
#include <Uno.Collections.ICollection-1.h>
#include <Uno.Collections.IEnumerable-1.h>
#include <Uno.Collections.IList-1.h>
#include <Uno.UX.IPropertyListener.h>
namespace g{namespace Uno{namespace UX{struct Property1;}}}
namespace g{namespace Uno{namespace UX{struct Selector;}}}
namespace g{struct SquareButton;}

namespace g{

// public partial sealed class SquareButton :2
// {
::g::Fuse::Controls::Panel_type* SquareButton_typeof();
void SquareButton__ctor_9_fn(SquareButton* __this);
void SquareButton__InitializeUX1_fn(SquareButton* __this);
void SquareButton__New6_fn(SquareButton** __retval);
void SquareButton__SetTextq_fn(SquareButton* __this, uString* value, uObject* origin);
void SquareButton__get_Textq_fn(SquareButton* __this, uString** __retval);
void SquareButton__set_Textq_fn(SquareButton* __this, uString* value);

struct SquareButton : ::g::Fuse::Controls::Button
{
    uStrong<uString*> _field_Textq;
    uStrong< ::g::Uno::UX::Property1*> temp_Value_inst;
    static ::g::Uno::UX::Selector __selector0_;
    static ::g::Uno::UX::Selector& __selector0() { return SquareButton_typeof()->Init(), __selector0_; }

    void ctor_9();
    void InitializeUX1();
    void SetTextq(uString* value, uObject* origin);
    uString* Textq();
    void Textq(uString* value);
    static SquareButton* New6();
};
// }

} // ::g
