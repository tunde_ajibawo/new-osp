// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Fuse.Drawing.Brush.h>
#include <Uno.UX.Property1-1.h>
namespace g{namespace Fuse{namespace Drawing{struct Stroke;}}}
namespace g{namespace Uno{namespace UX{struct PropertyObject;}}}
namespace g{namespace Uno{namespace UX{struct Selector;}}}
namespace g{struct OSP_FuseDrawingStroke_Brush_Property;}

namespace g{

// internal sealed class OSP_FuseDrawingStroke_Brush_Property :477
// {
::g::Uno::UX::Property1_type* OSP_FuseDrawingStroke_Brush_Property_typeof();
void OSP_FuseDrawingStroke_Brush_Property__ctor_3_fn(OSP_FuseDrawingStroke_Brush_Property* __this, ::g::Fuse::Drawing::Stroke* obj, ::g::Uno::UX::Selector* name);
void OSP_FuseDrawingStroke_Brush_Property__Get1_fn(OSP_FuseDrawingStroke_Brush_Property* __this, ::g::Uno::UX::PropertyObject* obj, ::g::Fuse::Drawing::Brush** __retval);
void OSP_FuseDrawingStroke_Brush_Property__New1_fn(::g::Fuse::Drawing::Stroke* obj, ::g::Uno::UX::Selector* name, OSP_FuseDrawingStroke_Brush_Property** __retval);
void OSP_FuseDrawingStroke_Brush_Property__get_Object_fn(OSP_FuseDrawingStroke_Brush_Property* __this, ::g::Uno::UX::PropertyObject** __retval);
void OSP_FuseDrawingStroke_Brush_Property__Set1_fn(OSP_FuseDrawingStroke_Brush_Property* __this, ::g::Uno::UX::PropertyObject* obj, ::g::Fuse::Drawing::Brush* v, uObject* origin);

struct OSP_FuseDrawingStroke_Brush_Property : ::g::Uno::UX::Property1
{
    uWeak< ::g::Fuse::Drawing::Stroke*> _obj;

    void ctor_3(::g::Fuse::Drawing::Stroke* obj, ::g::Uno::UX::Selector name);
    static OSP_FuseDrawingStroke_Brush_Property* New1(::g::Fuse::Drawing::Stroke* obj, ::g::Uno::UX::Selector name);
};
// }

} // ::g
