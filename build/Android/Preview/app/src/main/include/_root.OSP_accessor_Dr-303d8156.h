// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.UX.PropertyAccessor.h>
namespace g{namespace Uno{namespace UX{struct PropertyObject;}}}
namespace g{namespace Uno{namespace UX{struct Selector;}}}
namespace g{struct OSP_accessor_DropdownOption_BorderColor;}

namespace g{

// internal sealed class OSP_accessor_DropdownOption_BorderColor :31
// {
::g::Uno::UX::PropertyAccessor_type* OSP_accessor_DropdownOption_BorderColor_typeof();
void OSP_accessor_DropdownOption_BorderColor__ctor_1_fn(OSP_accessor_DropdownOption_BorderColor* __this);
void OSP_accessor_DropdownOption_BorderColor__GetAsObject_fn(OSP_accessor_DropdownOption_BorderColor* __this, ::g::Uno::UX::PropertyObject* obj, uObject** __retval);
void OSP_accessor_DropdownOption_BorderColor__get_Name_fn(OSP_accessor_DropdownOption_BorderColor* __this, ::g::Uno::UX::Selector* __retval);
void OSP_accessor_DropdownOption_BorderColor__New1_fn(OSP_accessor_DropdownOption_BorderColor** __retval);
void OSP_accessor_DropdownOption_BorderColor__get_PropertyType_fn(OSP_accessor_DropdownOption_BorderColor* __this, uType** __retval);
void OSP_accessor_DropdownOption_BorderColor__SetAsObject_fn(OSP_accessor_DropdownOption_BorderColor* __this, ::g::Uno::UX::PropertyObject* obj, uObject* v, uObject* origin);
void OSP_accessor_DropdownOption_BorderColor__get_SupportsOriginSetter_fn(OSP_accessor_DropdownOption_BorderColor* __this, bool* __retval);

struct OSP_accessor_DropdownOption_BorderColor : ::g::Uno::UX::PropertyAccessor
{
    static uSStrong< ::g::Uno::UX::PropertyAccessor*> Singleton_;
    static uSStrong< ::g::Uno::UX::PropertyAccessor*>& Singleton() { return OSP_accessor_DropdownOption_BorderColor_typeof()->Init(), Singleton_; }
    static ::g::Uno::UX::Selector _name_;
    static ::g::Uno::UX::Selector& _name() { return OSP_accessor_DropdownOption_BorderColor_typeof()->Init(), _name_; }

    void ctor_1();
    static OSP_accessor_DropdownOption_BorderColor* New1();
};
// }

} // ::g
