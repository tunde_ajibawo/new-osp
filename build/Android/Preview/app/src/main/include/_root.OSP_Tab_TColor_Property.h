// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/OSP.unoproj.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.Float4.h>
#include <Uno.UX.Property1-1.h>
namespace g{namespace Uno{namespace UX{struct PropertyObject;}}}
namespace g{namespace Uno{namespace UX{struct Selector;}}}
namespace g{struct OSP_Tab_TColor_Property;}
namespace g{struct Tab;}

namespace g{

// internal sealed class OSP_Tab_TColor_Property :693
// {
::g::Uno::UX::Property1_type* OSP_Tab_TColor_Property_typeof();
void OSP_Tab_TColor_Property__ctor_3_fn(OSP_Tab_TColor_Property* __this, ::g::Tab* obj, ::g::Uno::UX::Selector* name);
void OSP_Tab_TColor_Property__Get1_fn(OSP_Tab_TColor_Property* __this, ::g::Uno::UX::PropertyObject* obj, ::g::Uno::Float4* __retval);
void OSP_Tab_TColor_Property__New1_fn(::g::Tab* obj, ::g::Uno::UX::Selector* name, OSP_Tab_TColor_Property** __retval);
void OSP_Tab_TColor_Property__get_Object_fn(OSP_Tab_TColor_Property* __this, ::g::Uno::UX::PropertyObject** __retval);
void OSP_Tab_TColor_Property__Set1_fn(OSP_Tab_TColor_Property* __this, ::g::Uno::UX::PropertyObject* obj, ::g::Uno::Float4* v, uObject* origin);
void OSP_Tab_TColor_Property__get_SupportsOriginSetter_fn(OSP_Tab_TColor_Property* __this, bool* __retval);

struct OSP_Tab_TColor_Property : ::g::Uno::UX::Property1
{
    uWeak< ::g::Tab*> _obj;

    void ctor_3(::g::Tab* obj, ::g::Uno::UX::Selector name);
    static OSP_Tab_TColor_Property* New1(::g::Tab* obj, ::g::Uno::UX::Selector name);
};
// }

} // ::g
