// This file was generated based on /Users/tundeajibawo/Downloads/osp/build/Android/Preview/cache/ux15/WorkInfo.g.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.UX.Template.h>
namespace g{struct WorkInfo;}
namespace g{struct WorkInfo__Template5;}

namespace g{

// public partial sealed class WorkInfo.Template5 :163
// {
::g::Uno::UX::Template_type* WorkInfo__Template5_typeof();
void WorkInfo__Template5__ctor_1_fn(WorkInfo__Template5* __this, ::g::WorkInfo* parent, ::g::WorkInfo* parentInstance);
void WorkInfo__Template5__New1_fn(WorkInfo__Template5* __this, uObject** __retval);
void WorkInfo__Template5__New2_fn(::g::WorkInfo* parent, ::g::WorkInfo* parentInstance, WorkInfo__Template5** __retval);

struct WorkInfo__Template5 : ::g::Uno::UX::Template
{
    uWeak< ::g::WorkInfo*> __parent1;
    uWeak< ::g::WorkInfo*> __parentInstance1;

    void ctor_1(::g::WorkInfo* parent, ::g::WorkInfo* parentInstance);
    static WorkInfo__Template5* New2(::g::WorkInfo* parent, ::g::WorkInfo* parentInstance);
};
// }

} // ::g
