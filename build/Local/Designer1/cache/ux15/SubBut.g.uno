[Uno.Compiler.UxGenerated]
public partial class SubBut: Fuse.Controls.Button
{
    string _field_Btext;
    [global::Uno.UX.UXOriginSetter("SetBtext")]
    public string Btext
    {
        get { return _field_Btext; }
        set { SetBtext(value, null); }
    }
    public void SetBtext(string value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_Btext)
        {
            _field_Btext = value;
            OnPropertyChanged("Btext", origin);
        }
    }
    string _field_Icon;
    [global::Uno.UX.UXOriginSetter("SetIcon")]
    public string Icon
    {
        get { return _field_Icon; }
        set { SetIcon(value, null); }
    }
    public void SetIcon(string value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_Icon)
        {
            _field_Icon = value;
            OnPropertyChanged("Icon", origin);
        }
    }
    string _field_Direction;
    [global::Uno.UX.UXOriginSetter("SetDirection")]
    public string Direction
    {
        get { return _field_Direction; }
        set { SetDirection(value, null); }
    }
    public void SetDirection(string value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_Direction)
        {
            _field_Direction = value;
            OnPropertyChanged("Direction", origin);
        }
    }
    float4 _field_IconColor;
    [global::Uno.UX.UXOriginSetter("SetIconColor")]
    public float4 IconColor
    {
        get { return _field_IconColor; }
        set { SetIconColor(value, null); }
    }
    public void SetIconColor(float4 value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_IconColor)
        {
            _field_IconColor = value;
            OnPropertyChanged("IconColor", origin);
        }
    }
    global::Uno.UX.Property<string> temp_Value_inst;
    global::Uno.UX.Property<float4> temp_TextColor_inst;
    global::Uno.UX.Property<string> temp1_Value_inst;
    global::Uno.UX.Property<Fuse.Layouts.Dock> this_DockPanel_Dock_inst;
    static SubBut()
    {
    }
    [global::Uno.UX.UXConstructor]
    public SubBut()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp2 = new global::Fuse.Reactive.Constant(this);
        var temp = new global::Fuse.Controls.Text();
        temp_Value_inst = new OSP_FuseControlsTextControl_Value_Property(temp, __selector0);
        var temp3 = new global::Fuse.Reactive.Property(temp2, OSP_accessor_SubBut_Icon.Singleton);
        var temp4 = new global::Fuse.Reactive.Constant(this);
        temp_TextColor_inst = new OSP_FuseControlsTextControl_TextColor_Property(temp, __selector1);
        var temp5 = new global::Fuse.Reactive.Property(temp4, OSP_accessor_SubBut_IconColor.Singleton);
        var temp6 = new global::Fuse.Reactive.Constant(this);
        var temp1 = new global::Fuse.Controls.Text();
        temp1_Value_inst = new OSP_FuseControlsTextControl_Value_Property(temp1, __selector0);
        var temp7 = new global::Fuse.Reactive.Property(temp6, OSP_accessor_SubBut_Btext.Singleton);
        var temp8 = new global::Fuse.Reactive.Constant(this);
        this_DockPanel_Dock_inst = new OSP_FuseElementsElement_DockPanelDock_Property(this, __selector2);
        var temp9 = new global::Fuse.Reactive.Property(temp8, OSP_accessor_SubBut_Direction.Singleton);
        var temp10 = new global::Fuse.Controls.DockPanel();
        var temp11 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp3, Fuse.Reactive.BindingMode.Default);
        var temp12 = new global::Fuse.Reactive.DataBinding(temp_TextColor_inst, temp5, Fuse.Reactive.BindingMode.Default);
        var temp13 = new global::Fuse.Reactive.DataBinding(temp1_Value_inst, temp7, Fuse.Reactive.BindingMode.Default);
        var temp14 = new global::Fuse.Controls.Rectangle();
        var temp15 = new global::Fuse.Reactive.DataBinding(this_DockPanel_Dock_inst, temp9, Fuse.Reactive.BindingMode.Default);
        this.Padding = float4(5f, 5f, 5f, 5f);
        this.SourceLineNumber = 97;
        this.SourceFileName = "profile.ux";
        temp10.SourceLineNumber = 102;
        temp10.SourceFileName = "profile.ux";
        temp10.Children.Add(temp);
        temp10.Children.Add(temp1);
        temp.Alignment = Fuse.Elements.Alignment.Center;
        temp.Margin = float4(5f, 0f, 5f, 0f);
        temp.SourceLineNumber = 103;
        temp.SourceFileName = "profile.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp, Fuse.Layouts.Dock.Left);
        temp.Font = global::MainView.FontAwesome;
        temp.Bindings.Add(temp11);
        temp.Bindings.Add(temp12);
        temp3.SourceLineNumber = 103;
        temp3.SourceFileName = "profile.ux";
        temp2.SourceLineNumber = 103;
        temp2.SourceFileName = "profile.ux";
        temp5.SourceLineNumber = 103;
        temp5.SourceFileName = "profile.ux";
        temp4.SourceLineNumber = 103;
        temp4.SourceFileName = "profile.ux";
        temp1.TextAlignment = Fuse.Controls.TextAlignment.Center;
        temp1.TextColor = float4(0.9686275f, 0.8980392f, 0.372549f, 1f);
        temp1.Alignment = Fuse.Elements.Alignment.Center;
        temp1.SourceLineNumber = 104;
        temp1.SourceFileName = "profile.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp1, Fuse.Layouts.Dock.Left);
        temp1.Bindings.Add(temp13);
        temp7.SourceLineNumber = 104;
        temp7.SourceFileName = "profile.ux";
        temp6.SourceLineNumber = 104;
        temp6.SourceFileName = "profile.ux";
        temp14.Color = float4(0.9686275f, 0.9686275f, 0.9686275f, 1f);
        temp14.Layer = Fuse.Layer.Background;
        temp14.SourceLineNumber = 106;
        temp14.SourceFileName = "profile.ux";
        temp9.SourceLineNumber = 97;
        temp9.SourceFileName = "profile.ux";
        temp8.SourceLineNumber = 97;
        temp8.SourceFileName = "profile.ux";
        this.Children.Add(temp10);
        this.Children.Add(temp14);
        this.Bindings.Add(temp15);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
    static global::Uno.UX.Selector __selector1 = "TextColor";
    static global::Uno.UX.Selector __selector2 = "DockPanel.Dock";
}
