[Uno.Compiler.UxGenerated]
public partial class LoginField: Fuse.Controls.TextInput
{
    string _field_PlaceHolder;
    [global::Uno.UX.UXOriginSetter("SetPlaceHolder")]
    public string PlaceHolder
    {
        get { return _field_PlaceHolder; }
        set { SetPlaceHolder(value, null); }
    }
    public void SetPlaceHolder(string value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_PlaceHolder)
        {
            _field_PlaceHolder = value;
            OnPropertyChanged("PlaceHolder", origin);
        }
    }
    float4 _field_PlaceHolderColor;
    [global::Uno.UX.UXOriginSetter("SetPlaceHolderColor")]
    public float4 PlaceHolderColor
    {
        get { return _field_PlaceHolderColor; }
        set { SetPlaceHolderColor(value, null); }
    }
    public void SetPlaceHolderColor(float4 value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_PlaceHolderColor)
        {
            _field_PlaceHolderColor = value;
            OnPropertyChanged("PlaceHolderColor", origin);
        }
    }
    float4 _field_BgColor;
    [global::Uno.UX.UXOriginSetter("SetBgColor")]
    public float4 BgColor
    {
        get { return _field_BgColor; }
        set { SetBgColor(value, null); }
    }
    public void SetBgColor(float4 value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_BgColor)
        {
            _field_BgColor = value;
            OnPropertyChanged("BgColor", origin);
        }
    }
    float4 _field_StrokeColor;
    [global::Uno.UX.UXOriginSetter("SetStrokeColor")]
    public float4 StrokeColor
    {
        get { return _field_StrokeColor; }
        set { SetStrokeColor(value, null); }
    }
    public void SetStrokeColor(float4 value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_StrokeColor)
        {
            _field_StrokeColor = value;
            OnPropertyChanged("StrokeColor", origin);
        }
    }
    global::Uno.UX.Property<float4> temp_Color_inst;
    global::Uno.UX.Property<float4> temp1_Color_inst;
    global::Uno.UX.Property<string> this_PlaceholderText_inst;
    global::Uno.UX.Property<float4> this_PlaceholderColor_inst;
    static LoginField()
    {
    }
    [global::Uno.UX.UXConstructor]
    public LoginField()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp2 = new global::Fuse.Reactive.Constant(this);
        var temp3 = new global::Fuse.Reactive.Constant(this);
        var temp = new global::Fuse.Controls.Rectangle();
        temp_Color_inst = new OSP_FuseControlsShape_Color_Property(temp, __selector0);
        var temp4 = new global::Fuse.Reactive.Property(temp3, OSP_accessor_LoginField_BgColor.Singleton);
        var temp1 = new global::Fuse.Drawing.Stroke();
        temp1_Color_inst = new OSP_FuseDrawingStroke_Color_Property(temp1, __selector0);
        var temp5 = new global::Fuse.Reactive.Property(temp2, OSP_accessor_LoginField_StrokeColor.Singleton);
        var temp6 = new global::Fuse.Reactive.Constant(this);
        this_PlaceholderText_inst = new OSP_FuseControlsTextInput_PlaceholderText_Property(this, __selector1);
        var temp7 = new global::Fuse.Reactive.Property(temp6, OSP_accessor_LoginField_PlaceHolder.Singleton);
        var temp8 = new global::Fuse.Reactive.Constant(this);
        this_PlaceholderColor_inst = new OSP_FuseControlsTextInput_PlaceholderColor_Property(this, __selector2);
        var temp9 = new global::Fuse.Reactive.Property(temp8, OSP_accessor_LoginField_PlaceHolderColor.Singleton);
        var temp10 = new global::Fuse.Reactive.DataBinding(temp_Color_inst, temp4, Fuse.Reactive.BindingMode.Default);
        var temp11 = new global::Fuse.Reactive.DataBinding(temp1_Color_inst, temp5, Fuse.Reactive.BindingMode.Default);
        var temp12 = new global::Fuse.Reactive.DataBinding(this_PlaceholderText_inst, temp7, Fuse.Reactive.BindingMode.Default);
        var temp13 = new global::Fuse.Reactive.DataBinding(this_PlaceholderColor_inst, temp9, Fuse.Reactive.BindingMode.Default);
        this.BgColor = float4(0.06666667f, 0.2078431f, 0.4117647f, 1f);
        this.StrokeColor = float4(0.06666667f, 0.2078431f, 0.4117647f, 1f);
        this.FontSize = 12f;
        this.TextAlignment = Fuse.Controls.TextAlignment.Left;
        this.TextColor = float4(0.2f, 0.2f, 0.2f, 1f);
        this.Width = new Uno.UX.Size(80f, Uno.UX.Unit.Percent);
        this.Margin = float4(0f, 4f, 0f, 4f);
        this.SourceLineNumber = 5;
        this.SourceFileName = "homepage.ux";
        temp.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp.Layer = Fuse.Layer.Background;
        temp.SourceLineNumber = 10;
        temp.SourceFileName = "homepage.ux";
        temp.Strokes.Add(temp1);
        temp.Bindings.Add(temp10);
        temp.Bindings.Add(temp11);
        temp1.Width = 1f;
        temp5.SourceLineNumber = 11;
        temp5.SourceFileName = "homepage.ux";
        temp2.SourceLineNumber = 11;
        temp2.SourceFileName = "homepage.ux";
        temp4.SourceLineNumber = 10;
        temp4.SourceFileName = "homepage.ux";
        temp3.SourceLineNumber = 10;
        temp3.SourceFileName = "homepage.ux";
        temp7.SourceLineNumber = 5;
        temp7.SourceFileName = "homepage.ux";
        temp6.SourceLineNumber = 5;
        temp6.SourceFileName = "homepage.ux";
        temp9.SourceLineNumber = 5;
        temp9.SourceFileName = "homepage.ux";
        temp8.SourceLineNumber = 5;
        temp8.SourceFileName = "homepage.ux";
        this.Font = global::MainView.Raleway;
        this.Children.Add(temp);
        this.Bindings.Add(temp12);
        this.Bindings.Add(temp13);
    }
    static global::Uno.UX.Selector __selector0 = "Color";
    static global::Uno.UX.Selector __selector1 = "PlaceholderText";
    static global::Uno.UX.Selector __selector2 = "PlaceholderColor";
}
