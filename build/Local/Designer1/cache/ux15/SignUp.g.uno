[Uno.Compiler.UxGenerated]
public partial class SignUp: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    global::Uno.UX.Property<string> temp_Value_inst;
    global::Uno.UX.Property<string> temp1_Value_inst;
    internal global::Fuse.Reactive.EventBinding temp_eb6;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "temp_eb6"
    };
    static SignUp()
    {
    }
    [global::Uno.UX.UXConstructor]
    public SignUp(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp = new global::LoginField();
        temp_Value_inst = new OSP_FuseControlsTextInputControl_Value_Property(temp, __selector0);
        var temp2 = new global::Fuse.Reactive.Data("OSPID");
        var temp1 = new global::LoginField();
        temp1_Value_inst = new OSP_FuseControlsTextInputControl_Value_Property(temp1, __selector0);
        var temp3 = new global::Fuse.Reactive.Data("password");
        var temp4 = new global::Fuse.Reactive.Data("signup");
        var temp5 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp6 = new global::Fuse.Controls.DockPanel();
        var temp7 = new global::TopBar();
        var temp8 = new global::Fuse.Controls.DockPanel();
        var temp9 = new global::Fuse.Controls.StackPanel();
        var temp10 = new global::Fuse.Controls.Text();
        var temp11 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp2, Fuse.Reactive.BindingMode.Default);
        var temp12 = new global::Fuse.Reactive.DataBinding(temp1_Value_inst, temp3, Fuse.Reactive.BindingMode.Default);
        var temp13 = new global::SubButton();
        temp_eb6 = new global::Fuse.Reactive.EventBinding(temp4);
        this.Color = float4(1f, 1f, 1f, 1f);
        this.SourceLineNumber = 1;
        this.SourceFileName = "SignUp.ux";
        temp5.LineNumber = 3;
        temp5.FileName = "SignUp.ux";
        temp5.SourceLineNumber = 3;
        temp5.SourceFileName = "SignUp.ux";
        temp5.File = new global::Uno.UX.BundleFileSource(import("../../../../../js_files/signup.js"));
        temp6.SourceLineNumber = 5;
        temp6.SourceFileName = "SignUp.ux";
        temp6.Children.Add(temp7);
        temp6.Children.Add(temp8);
        temp7.Title = "SIGN UP";
        temp7.SourceLineNumber = 7;
        temp7.SourceFileName = "SignUp.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp7, Fuse.Layouts.Dock.Top);
        temp8.Color = float4(1f, 1f, 1f, 1f);
        temp8.Margin = float4(0f, 0f, 0f, 0f);
        temp8.SourceLineNumber = 9;
        temp8.SourceFileName = "SignUp.ux";
        temp8.Children.Add(temp9);
        temp9.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp9.Alignment = Fuse.Elements.Alignment.Center;
        temp9.SourceLineNumber = 11;
        temp9.SourceFileName = "SignUp.ux";
        temp9.Children.Add(temp10);
        temp9.Children.Add(temp);
        temp9.Children.Add(temp1);
        temp9.Children.Add(temp13);
        temp10.Value = "Please fill the form below to register";
        temp10.FontSize = 12f;
        temp10.Color = float4(0.06666667f, 0.2078431f, 0.4117647f, 1f);
        temp10.Alignment = Fuse.Elements.Alignment.Center;
        temp10.Margin = float4(0f, 10f, 0f, 10f);
        temp10.SourceLineNumber = 12;
        temp10.SourceFileName = "SignUp.ux";
        temp10.Font = global::MainView.Raleway;
        temp.PlaceHolder = "OSPID";
        temp.PlaceHolderColor = float4(0.5490196f, 0.5843138f, 0.6313726f, 1f);
        temp.BgColor = float4(1f, 1f, 1f, 1f);
        temp.TextColor = float4(0.06666667f, 0.2078431f, 0.4117647f, 1f);
        temp.Height = new Uno.UX.Size(38f, Uno.UX.Unit.Unspecified);
        temp.Padding = float4(20f, 5f, 20f, 5f);
        temp.SourceLineNumber = 14;
        temp.SourceFileName = "SignUp.ux";
        temp.Bindings.Add(temp11);
        temp2.SourceLineNumber = 14;
        temp2.SourceFileName = "SignUp.ux";
        temp1.PlaceHolder = "Password";
        temp1.PlaceHolderColor = float4(0.5490196f, 0.5843138f, 0.6313726f, 1f);
        temp1.BgColor = float4(1f, 1f, 1f, 1f);
        temp1.TextColor = float4(0.06666667f, 0.2078431f, 0.4117647f, 1f);
        temp1.Height = new Uno.UX.Size(38f, Uno.UX.Unit.Unspecified);
        temp1.Padding = float4(20f, 5f, 20f, 5f);
        temp1.SourceLineNumber = 15;
        temp1.SourceFileName = "SignUp.ux";
        temp1.Bindings.Add(temp12);
        temp3.SourceLineNumber = 15;
        temp3.SourceFileName = "SignUp.ux";
        temp13.ButtonName = "SUBMIT";
        temp13.SourceLineNumber = 18;
        temp13.SourceFileName = "SignUp.ux";
        global::Fuse.Gestures.Clicked.AddHandler(temp13, temp_eb6.OnEvent);
        temp13.Bindings.Add(temp_eb6);
        temp4.SourceLineNumber = 18;
        temp4.SourceFileName = "SignUp.ux";
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(temp_eb6);
        this.Children.Add(temp5);
        this.Children.Add(temp6);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
}
