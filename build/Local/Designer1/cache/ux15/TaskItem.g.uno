[Uno.Compiler.UxGenerated]
public partial class TaskItem: Fuse.Controls.DockPanel
{
    global::Uno.UX.Property<string> temp_Value_inst;
    global::Uno.UX.Property<string> temp1_Value_inst;
    global::Uno.UX.Property<string> temp2_Value_inst;
    global::Uno.UX.Property<string> temp3_Value_inst;
    static TaskItem()
    {
    }
    [global::Uno.UX.UXConstructor]
    public TaskItem()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp = new global::Fuse.Controls.Text();
        temp_Value_inst = new OSP_FuseControlsTextControl_Value_Property(temp, __selector0);
        var temp4 = new global::Fuse.Reactive.Data("letter");
        var temp1 = new global::Fuse.Controls.Text();
        temp1_Value_inst = new OSP_FuseControlsTextControl_Value_Property(temp1, __selector0);
        var temp5 = new global::Fuse.Reactive.Data("text");
        var temp2 = new global::Fuse.Controls.Text();
        temp2_Value_inst = new OSP_FuseControlsTextControl_Value_Property(temp2, __selector0);
        var temp6 = new global::Fuse.Reactive.Data("date");
        var temp3 = new global::Fuse.Controls.Text();
        temp3_Value_inst = new OSP_FuseControlsTextControl_Value_Property(temp3, __selector0);
        var temp7 = new global::Fuse.Reactive.Data("timeSlot");
        var temp8 = new global::Fuse.Controls.Circle();
        var temp9 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp4, Fuse.Reactive.BindingMode.Default);
        var temp10 = new global::Fuse.Controls.StackPanel();
        var temp11 = new global::Fuse.Reactive.DataBinding(temp1_Value_inst, temp5, Fuse.Reactive.BindingMode.Default);
        var temp12 = new global::Fuse.Controls.DockPanel();
        var temp13 = new global::Fuse.Reactive.DataBinding(temp2_Value_inst, temp6, Fuse.Reactive.BindingMode.Default);
        var temp14 = new global::Fuse.Reactive.DataBinding(temp3_Value_inst, temp7, Fuse.Reactive.BindingMode.Default);
        var temp15 = new global::Fuse.Drawing.StaticSolidColor(float4(1f, 1f, 1f, 1f));
        this.Height = new Uno.UX.Size(80f, Uno.UX.Unit.Unspecified);
        this.Margin = float4(1f, 1f, 1f, 1f);
        this.Padding = float4(15f, 20f, 15f, 20f);
        this.SourceLineNumber = 27;
        this.SourceFileName = "notification.ux";
        temp8.Color = float4(1f, 0.972549f, 0.7607843f, 1f);
        temp8.Width = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        temp8.Height = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        temp8.SourceLineNumber = 30;
        temp8.SourceFileName = "notification.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp8, Fuse.Layouts.Dock.Left);
        temp8.Children.Add(temp);
        temp.TextAlignment = Fuse.Controls.TextAlignment.Center;
        temp.TextColor = float4(0.3333333f, 0.3333333f, 0.3333333f, 1f);
        temp.Alignment = Fuse.Elements.Alignment.Center;
        temp.SourceLineNumber = 31;
        temp.SourceFileName = "notification.ux";
        temp.Font = global::MainView.Raleway;
        temp.Bindings.Add(temp9);
        temp4.SourceLineNumber = 31;
        temp4.SourceFileName = "notification.ux";
        temp10.Margin = float4(40f, 0f, 40f, 0f);
        temp10.SourceLineNumber = 33;
        temp10.SourceFileName = "notification.ux";
        temp10.Children.Add(temp1);
        temp10.Children.Add(temp12);
        temp1.TextWrapping = Fuse.Controls.TextWrapping.Wrap;
        temp1.SourceLineNumber = 34;
        temp1.SourceFileName = "notification.ux";
        temp1.Bindings.Add(temp11);
        temp5.SourceLineNumber = 34;
        temp5.SourceFileName = "notification.ux";
        temp12.SourceLineNumber = 35;
        temp12.SourceFileName = "notification.ux";
        temp12.Children.Add(temp2);
        temp12.Children.Add(temp3);
        temp2.TextWrapping = Fuse.Controls.TextWrapping.Wrap;
        temp2.Color = float4(0.6f, 0.6f, 0.6f, 1f);
        temp2.Margin = float4(0f, 0f, 15f, 0f);
        temp2.SourceLineNumber = 36;
        temp2.SourceFileName = "notification.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp2, Fuse.Layouts.Dock.Left);
        temp2.Bindings.Add(temp13);
        temp6.SourceLineNumber = 36;
        temp6.SourceFileName = "notification.ux";
        temp3.TextWrapping = Fuse.Controls.TextWrapping.Wrap;
        temp3.Color = float4(0.6f, 0.6f, 0.6f, 1f);
        temp3.SourceLineNumber = 42;
        temp3.SourceFileName = "notification.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp3, Fuse.Layouts.Dock.Left);
        temp3.Bindings.Add(temp14);
        temp7.SourceLineNumber = 42;
        temp7.SourceFileName = "notification.ux";
        this.Background = temp15;
        this.Children.Add(temp8);
        this.Children.Add(temp10);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
}
