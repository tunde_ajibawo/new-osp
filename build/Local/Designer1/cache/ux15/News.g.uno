[Uno.Compiler.UxGenerated]
public partial class News: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly News __parent;
        [Uno.WeakReference] internal readonly News __parentInstance;
        public Template(News parent, News parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::NewsItem();
            __self.SourceLineNumber = 58;
            __self.SourceFileName = "news.ux";
            return __self;
        }
    }
    global::Uno.UX.Property<float> loadingPanel_Opacity_inst;
    global::Uno.UX.Property<float> NewPage_Opacity_inst;
    global::Uno.UX.Property<object> temp_Items_inst;
    global::Uno.UX.Property<string> temp1_Value_inst;
    global::Uno.UX.Property<float> load_Opacity_inst;
    global::Uno.UX.Property<float> errorMsg_Opacity_inst;
    global::Uno.UX.Property<bool> temp2_Value_inst;
    global::Uno.UX.Property<bool> temp3_Value_inst;
    internal global::Fuse.Controls.Grid NewPage;
    internal global::Fuse.Reactive.EventBinding temp_eb19;
    internal global::Fuse.Triggers.Busy busy;
    internal global::Fuse.Controls.ClientPanel loadingPanel;
    internal global::Fuse.Controls.Image load;
    internal global::Fuse.Controls.StackPanel errorMsg;
    internal global::Fuse.Reactive.EventBinding temp_eb20;
    internal global::Fuse.Reactive.EventBinding temp_eb21;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "NewPage",
        "temp_eb19",
        "busy",
        "loadingPanel",
        "load",
        "errorMsg",
        "temp_eb20",
        "temp_eb21"
    };
    static News()
    {
    }
    [global::Uno.UX.UXConstructor]
    public News(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        loadingPanel = new global::Fuse.Controls.ClientPanel();
        loadingPanel_Opacity_inst = new OSP_FuseElementsElement_Opacity_Property(loadingPanel, __selector0);
        NewPage = new global::Fuse.Controls.Grid();
        NewPage_Opacity_inst = new OSP_FuseElementsElement_Opacity_Property(NewPage, __selector0);
        var temp4 = new global::Fuse.Reactive.Data("startLoad");
        var temp = new global::Fuse.Reactive.Each();
        temp_Items_inst = new OSP_FuseReactiveEach_Items_Property(temp, __selector1);
        var temp5 = new global::Fuse.Reactive.Data("data");
        var temp1 = new global::Fuse.Controls.Text();
        temp1_Value_inst = new OSP_FuseControlsTextControl_Value_Property(temp1, __selector2);
        var temp6 = new global::Fuse.Reactive.Data("Msg");
        var temp7 = new global::Fuse.Reactive.Data("startLoad");
        var temp8 = new global::Fuse.Reactive.Data("dashboard");
        load = new global::Fuse.Controls.Image();
        load_Opacity_inst = new OSP_FuseElementsElement_Opacity_Property(load, __selector0);
        errorMsg = new global::Fuse.Controls.StackPanel();
        errorMsg_Opacity_inst = new OSP_FuseElementsElement_Opacity_Property(errorMsg, __selector0);
        var temp2 = new global::Fuse.Triggers.WhileTrue();
        temp2_Value_inst = new OSP_FuseTriggersWhileBool_Value_Property(temp2, __selector2);
        var temp9 = new global::Fuse.Reactive.Data("error");
        var temp3 = new global::Fuse.Triggers.WhileFalse();
        temp3_Value_inst = new OSP_FuseTriggersWhileBool_Value_Property(temp3, __selector2);
        var temp10 = new global::Fuse.Reactive.Data("error");
        var temp11 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp12 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp13 = new global::Fuse.Triggers.WhileBusy();
        var temp14 = new global::Fuse.Animations.Change<float>(loadingPanel_Opacity_inst);
        var temp15 = new global::Fuse.Animations.Change<float>(NewPage_Opacity_inst);
        var temp16 = new global::Fuse.Navigation.Activated();
        temp_eb19 = new global::Fuse.Reactive.EventBinding(temp4);
        busy = new global::Fuse.Triggers.Busy();
        var temp17 = new global::TopBar();
        var temp18 = new global::Fuse.Controls.DockPanel();
        var temp19 = new global::Fuse.Controls.ScrollView();
        var temp20 = new global::Fuse.Controls.StackPanel();
        var temp21 = new global::Fuse.Controls.Panel();
        var temp22 = new Template(this, this);
        var temp23 = new global::Fuse.Reactive.DataBinding(temp_Items_inst, temp5, Fuse.Reactive.BindingMode.Default);
        var temp24 = new global::Fuse.Reactive.DataBinding(temp1_Value_inst, temp6, Fuse.Reactive.BindingMode.Default);
        var temp25 = new global::Fuse.Controls.DockPanel();
        var temp26 = new global::SubBut();
        temp_eb20 = new global::Fuse.Reactive.EventBinding(temp7);
        var temp27 = new global::SubBut();
        temp_eb21 = new global::Fuse.Reactive.EventBinding(temp8);
        var temp28 = new global::Fuse.Animations.Change<float>(load_Opacity_inst);
        var temp29 = new global::Fuse.Animations.Change<float>(errorMsg_Opacity_inst);
        var temp30 = new global::Fuse.Reactive.DataBinding(temp2_Value_inst, temp9, Fuse.Reactive.BindingMode.Default);
        var temp31 = new global::Fuse.Animations.Change<float>(load_Opacity_inst);
        var temp32 = new global::Fuse.Animations.Spin();
        var temp33 = new global::Fuse.Animations.Change<float>(errorMsg_Opacity_inst);
        var temp34 = new global::Fuse.Reactive.DataBinding(temp3_Value_inst, temp10, Fuse.Reactive.BindingMode.Default);
        this.Color = float4(1f, 1f, 1f, 1f);
        this.SourceLineNumber = 1;
        this.SourceFileName = "news.ux";
        temp11.LineNumber = 3;
        temp11.FileName = "news.ux";
        temp11.SourceLineNumber = 3;
        temp11.SourceFileName = "news.ux";
        temp11.File = new global::Uno.UX.BundleFileSource(import("../../../../../js_files/navigation.js"));
        temp12.Code = "\n\t\tvar Observable = require(\"FuseJS/Observable\");\n\t\tvar data = Observable();\n\t\tvar error = Observable(false);\n\t\tvar Msg = Observable(\"There was an error\");\n\n\t\tfunction startLoad() {\n\t\t\tbusy.activate();\n\t\t\tfetch('http://az664292.vo.msecnd.net/files/P6FteBeij9A7jTXL-edgenavresponse.json')\n\t\t\t.then(function(response) { return response.json(); })\n\t\t\t.then(function (responseObject) {\n\t\t\t\tdata.replaceAll(responseObject.responseData);\n\t\t\t\t// console.log(\"it works: \"+JSON.stringify(responseObject));\n\t\t\t\t// console.log(error.value);\n\t\t\t\t busy.deactivate();\n\t\t\t}).catch(function(err) {\n\t\t\t\tconsole.log(\"There was an error\");\n\t\t\t\t // busy.deactivate();\n\t\t\t\t error.value = true;\n\t\t\t});\n\t\t}\n\n\t\t\t// fetch('http://az664292.vo.msecnd.net/files/P6FteBeij9A7jTXL-edgenavresponse.json')\n\t\t\t// .then(function(response) { return response.json(); })\n\t\t\t// .then(function(responseObject) { data.value = responseObject; });\n\t\t\n\t\tmodule.exports = {\n\t\t\tdata: data,\n\t\t\tstartLoad: startLoad,\n\t\t\terror: error,\n\t\t\tMsg: Msg\n\t\t};\n\n\t";
        temp12.LineNumber = 4;
        temp12.FileName = "news.ux";
        temp12.SourceLineNumber = 4;
        temp12.SourceFileName = "news.ux";
        NewPage.Rows = "1*,15*";
        NewPage.Opacity = 1f;
        NewPage.Name = __selector3;
        NewPage.SourceLineNumber = 39;
        NewPage.SourceFileName = "news.ux";
        NewPage.Children.Add(temp13);
        NewPage.Children.Add(temp16);
        NewPage.Children.Add(busy);
        NewPage.Children.Add(temp17);
        NewPage.Children.Add(temp18);
        temp13.SourceLineNumber = 41;
        temp13.SourceFileName = "news.ux";
        temp13.Animators.Add(temp14);
        temp13.Animators.Add(temp15);
        temp14.Value = 1f;
        temp14.Duration = 0.5;
        temp15.Value = 0f;
        temp16.SourceLineNumber = 46;
        temp16.SourceFileName = "news.ux";
        temp16.Handler += temp_eb19.OnEvent;
        temp16.Bindings.Add(temp_eb19);
        temp4.SourceLineNumber = 46;
        temp4.SourceFileName = "news.ux";
        busy.IsActive = false;
        busy.Name = __selector4;
        busy.SourceLineNumber = 48;
        busy.SourceFileName = "news.ux";
        temp17.Title = "NEWS";
        temp17.SourceLineNumber = 50;
        temp17.SourceFileName = "news.ux";
        temp18.Padding = float4(0f, 20f, 0f, 0f);
        temp18.SourceLineNumber = 52;
        temp18.SourceFileName = "news.ux";
        temp18.Children.Add(temp19);
        temp19.SourceLineNumber = 54;
        temp19.SourceFileName = "news.ux";
        temp19.Children.Add(temp20);
        temp20.Alignment = Fuse.Elements.Alignment.Top;
        temp20.SourceLineNumber = 55;
        temp20.SourceFileName = "news.ux";
        temp20.Children.Add(temp21);
        temp20.Children.Add(temp);
        temp21.Height = new Uno.UX.Size(7f, Uno.UX.Unit.Unspecified);
        temp21.SourceLineNumber = 56;
        temp21.SourceFileName = "news.ux";
        temp.SourceLineNumber = 57;
        temp.SourceFileName = "news.ux";
        temp.Templates.Add(temp22);
        temp.Bindings.Add(temp23);
        temp5.SourceLineNumber = 57;
        temp5.SourceFileName = "news.ux";
        loadingPanel.Color = float4(1f, 1f, 1f, 1f);
        loadingPanel.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        loadingPanel.Height = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        loadingPanel.Alignment = Fuse.Elements.Alignment.Top;
        loadingPanel.Opacity = 0f;
        loadingPanel.Name = __selector5;
        loadingPanel.SourceLineNumber = 67;
        loadingPanel.SourceFileName = "news.ux";
        loadingPanel.Children.Add(load);
        loadingPanel.Children.Add(errorMsg);
        loadingPanel.Children.Add(temp2);
        loadingPanel.Children.Add(temp3);
        load.Width = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        load.Height = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        load.Alignment = Fuse.Elements.Alignment.Center;
        load.Opacity = 1f;
        load.Name = __selector6;
        load.SourceLineNumber = 69;
        load.SourceFileName = "news.ux";
        load.File = new global::Uno.UX.BundleFileSource(import("../../../../../Assets/disk.png"));
        errorMsg.Alignment = Fuse.Elements.Alignment.Center;
        errorMsg.Opacity = 0f;
        errorMsg.Name = __selector7;
        errorMsg.SourceLineNumber = 70;
        errorMsg.SourceFileName = "news.ux";
        errorMsg.Children.Add(temp1);
        errorMsg.Children.Add(temp25);
        temp1.TextAlignment = Fuse.Controls.TextAlignment.Center;
        temp1.Color = float4(0.8f, 0f, 0f, 1f);
        temp1.Alignment = Fuse.Elements.Alignment.Center;
        temp1.SourceLineNumber = 71;
        temp1.SourceFileName = "news.ux";
        temp1.Font = global::MainView.Raleway;
        temp1.Bindings.Add(temp24);
        temp6.SourceLineNumber = 71;
        temp6.SourceFileName = "news.ux";
        temp25.Width = new Uno.UX.Size(180f, Uno.UX.Unit.Unspecified);
        temp25.Margin = float4(0f, 10f, 0f, 10f);
        temp25.SourceLineNumber = 72;
        temp25.SourceFileName = "news.ux";
        temp25.Children.Add(temp26);
        temp25.Children.Add(temp27);
        temp26.Btext = "Retry";
        temp26.Icon = "\uF021";
        temp26.IconColor = float4(0.3333333f, 1f, 0.4980392f, 1f);
        temp26.SourceLineNumber = 73;
        temp26.SourceFileName = "news.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp26, Fuse.Layouts.Dock.Left);
        global::Fuse.Gestures.Clicked.AddHandler(temp26, temp_eb20.OnEvent);
        temp26.Bindings.Add(temp_eb20);
        temp7.SourceLineNumber = 73;
        temp7.SourceFileName = "news.ux";
        temp27.Btext = "Cancel";
        temp27.Icon = "\uF00D";
        temp27.IconColor = float4(0.8f, 0f, 0f, 1f);
        temp27.SourceLineNumber = 74;
        temp27.SourceFileName = "news.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp27, Fuse.Layouts.Dock.Right);
        global::Fuse.Gestures.Clicked.AddHandler(temp27, temp_eb21.OnEvent);
        temp27.Bindings.Add(temp_eb21);
        temp8.SourceLineNumber = 74;
        temp8.SourceFileName = "news.ux";
        temp2.SourceLineNumber = 80;
        temp2.SourceFileName = "news.ux";
        temp2.Animators.Add(temp28);
        temp2.Animators.Add(temp29);
        temp2.Bindings.Add(temp30);
        temp28.Value = 0f;
        temp28.Duration = 0.5;
        temp29.Value = 1f;
        temp29.Duration = 0.5;
        temp9.SourceLineNumber = 80;
        temp9.SourceFileName = "news.ux";
        temp3.SourceLineNumber = 85;
        temp3.SourceFileName = "news.ux";
        temp3.Animators.Add(temp31);
        temp3.Animators.Add(temp32);
        temp3.Animators.Add(temp33);
        temp3.Bindings.Add(temp34);
        temp31.Value = 1f;
        temp31.Duration = 0.5;
        temp32.Frequency = 1;
        temp32.Target = load;
        temp33.Value = 0f;
        temp33.Duration = 0.5;
        temp10.SourceLineNumber = 85;
        temp10.SourceFileName = "news.ux";
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(NewPage);
        __g_nametable.Objects.Add(temp_eb19);
        __g_nametable.Objects.Add(busy);
        __g_nametable.Objects.Add(loadingPanel);
        __g_nametable.Objects.Add(load);
        __g_nametable.Objects.Add(errorMsg);
        __g_nametable.Objects.Add(temp_eb20);
        __g_nametable.Objects.Add(temp_eb21);
        this.Children.Add(temp11);
        this.Children.Add(temp12);
        this.Children.Add(NewPage);
        this.Children.Add(loadingPanel);
    }
    static global::Uno.UX.Selector __selector0 = "Opacity";
    static global::Uno.UX.Selector __selector1 = "Items";
    static global::Uno.UX.Selector __selector2 = "Value";
    static global::Uno.UX.Selector __selector3 = "NewPage";
    static global::Uno.UX.Selector __selector4 = "busy";
    static global::Uno.UX.Selector __selector5 = "loadingPanel";
    static global::Uno.UX.Selector __selector6 = "load";
    static global::Uno.UX.Selector __selector7 = "errorMsg";
}
