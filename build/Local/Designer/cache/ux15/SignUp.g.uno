[Uno.Compiler.UxGenerated]
public partial class SignUp: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    global::Uno.UX.Property<string> temp_Value_inst;
    global::Uno.UX.Property<string> temp1_Value_inst;
    global::Uno.UX.Property<string> temp2_Value_inst;
    internal global::Fuse.Reactive.EventBinding temp_eb6;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "temp_eb6"
    };
    static SignUp()
    {
    }
    [global::Uno.UX.UXConstructor]
    public SignUp(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp = new global::LoginField();
        temp_Value_inst = new OSP_FuseControlsTextInputControl_Value_Property(temp, __selector0);
        var temp3 = new global::Fuse.Reactive.Data("email");
        var temp1 = new global::LoginField();
        temp1_Value_inst = new OSP_FuseControlsTextInputControl_Value_Property(temp1, __selector0);
        var temp4 = new global::Fuse.Reactive.Data("OSPID");
        var temp2 = new global::LoginField();
        temp2_Value_inst = new OSP_FuseControlsTextInputControl_Value_Property(temp2, __selector0);
        var temp5 = new global::Fuse.Reactive.Data("password");
        var temp6 = new global::Fuse.Reactive.Data("signup");
        var temp7 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp8 = new global::Fuse.Controls.DockPanel();
        var temp9 = new global::TopBar();
        var temp10 = new global::Fuse.Controls.DockPanel();
        var temp11 = new global::Fuse.Controls.StackPanel();
        var temp12 = new global::Fuse.Controls.Text();
        var temp13 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp3, Fuse.Reactive.BindingMode.Default);
        var temp14 = new global::Fuse.Reactive.DataBinding(temp1_Value_inst, temp4, Fuse.Reactive.BindingMode.Default);
        var temp15 = new global::Fuse.Reactive.DataBinding(temp2_Value_inst, temp5, Fuse.Reactive.BindingMode.Default);
        var temp16 = new global::SubButton();
        temp_eb6 = new global::Fuse.Reactive.EventBinding(temp6);
        this.Color = float4(1f, 1f, 1f, 1f);
        this.SourceLineNumber = 1;
        this.SourceFileName = "SignUp.ux";
        temp7.LineNumber = 3;
        temp7.FileName = "SignUp.ux";
        temp7.SourceLineNumber = 3;
        temp7.SourceFileName = "SignUp.ux";
        temp7.File = new global::Uno.UX.BundleFileSource(import("../../../../../js_files/signup.js"));
        temp8.SourceLineNumber = 5;
        temp8.SourceFileName = "SignUp.ux";
        temp8.Children.Add(temp9);
        temp8.Children.Add(temp10);
        temp9.Title = "SIGN UP";
        temp9.SourceLineNumber = 7;
        temp9.SourceFileName = "SignUp.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp9, Fuse.Layouts.Dock.Top);
        temp10.Color = float4(1f, 1f, 1f, 1f);
        temp10.Margin = float4(0f, 0f, 0f, 0f);
        temp10.SourceLineNumber = 9;
        temp10.SourceFileName = "SignUp.ux";
        temp10.Children.Add(temp11);
        temp11.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp11.Alignment = Fuse.Elements.Alignment.Center;
        temp11.SourceLineNumber = 11;
        temp11.SourceFileName = "SignUp.ux";
        temp11.Children.Add(temp12);
        temp11.Children.Add(temp);
        temp11.Children.Add(temp1);
        temp11.Children.Add(temp2);
        temp11.Children.Add(temp16);
        temp12.Value = "Please fill the form below to register";
        temp12.FontSize = 12f;
        temp12.Color = float4(0.06666667f, 0.2078431f, 0.4117647f, 1f);
        temp12.Alignment = Fuse.Elements.Alignment.Center;
        temp12.Margin = float4(0f, 10f, 0f, 10f);
        temp12.SourceLineNumber = 12;
        temp12.SourceFileName = "SignUp.ux";
        temp12.Font = global::MainView.Raleway;
        temp.PlaceHolder = "Email";
        temp.PlaceHolderColor = float4(0.5490196f, 0.5843138f, 0.6313726f, 1f);
        temp.BgColor = float4(1f, 1f, 1f, 1f);
        temp.TextColor = float4(0.06666667f, 0.2078431f, 0.4117647f, 1f);
        temp.Height = new Uno.UX.Size(38f, Uno.UX.Unit.Unspecified);
        temp.Padding = float4(20f, 5f, 20f, 5f);
        temp.SourceLineNumber = 13;
        temp.SourceFileName = "SignUp.ux";
        temp.Bindings.Add(temp13);
        temp3.SourceLineNumber = 13;
        temp3.SourceFileName = "SignUp.ux";
        temp1.PlaceHolder = "OSPID";
        temp1.PlaceHolderColor = float4(0.5490196f, 0.5843138f, 0.6313726f, 1f);
        temp1.BgColor = float4(1f, 1f, 1f, 1f);
        temp1.TextColor = float4(0.06666667f, 0.2078431f, 0.4117647f, 1f);
        temp1.Height = new Uno.UX.Size(38f, Uno.UX.Unit.Unspecified);
        temp1.Padding = float4(20f, 5f, 20f, 5f);
        temp1.SourceLineNumber = 14;
        temp1.SourceFileName = "SignUp.ux";
        temp1.Bindings.Add(temp14);
        temp4.SourceLineNumber = 14;
        temp4.SourceFileName = "SignUp.ux";
        temp2.PlaceHolder = "Password";
        temp2.PlaceHolderColor = float4(0.5490196f, 0.5843138f, 0.6313726f, 1f);
        temp2.BgColor = float4(1f, 1f, 1f, 1f);
        temp2.IsPassword = true;
        temp2.TextColor = float4(0.06666667f, 0.2078431f, 0.4117647f, 1f);
        temp2.Height = new Uno.UX.Size(38f, Uno.UX.Unit.Unspecified);
        temp2.Padding = float4(20f, 5f, 20f, 5f);
        temp2.SourceLineNumber = 15;
        temp2.SourceFileName = "SignUp.ux";
        temp2.Bindings.Add(temp15);
        temp5.SourceLineNumber = 15;
        temp5.SourceFileName = "SignUp.ux";
        temp16.ButtonName = "SUBMIT";
        temp16.SourceLineNumber = 18;
        temp16.SourceFileName = "SignUp.ux";
        global::Fuse.Gestures.Clicked.AddHandler(temp16, temp_eb6.OnEvent);
        temp16.Bindings.Add(temp_eb6);
        temp6.SourceLineNumber = 18;
        temp6.SourceFileName = "SignUp.ux";
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(temp_eb6);
        this.Children.Add(temp7);
        this.Children.Add(temp8);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
}
