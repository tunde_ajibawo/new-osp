[Uno.Compiler.UxGenerated]
public partial class WorkInfo: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly WorkInfo __parent;
        [Uno.WeakReference] internal readonly WorkInfo __parentInstance;
        public Template(WorkInfo parent, WorkInfo parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> __self_PlaceHolderName_inst;
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::InputField();
            __self_PlaceHolderName_inst = new OSP_InputField_PlaceHolderName_Property(__self, __selector0);
            var temp = new global::Fuse.Reactive.Data("course");
            var temp1 = new global::Fuse.Reactive.DataBinding(__self_PlaceHolderName_inst, temp, Fuse.Reactive.BindingMode.Default);
            __self.Title = "TITLE";
            __self.SourceLineNumber = 270;
            __self.SourceFileName = "workinfo.ux";
            temp.SourceLineNumber = 270;
            temp.SourceFileName = "workinfo.ux";
            __self.Bindings.Add(temp1);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "PlaceHolderName";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template1: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly WorkInfo __parent;
        [Uno.WeakReference] internal readonly WorkInfo __parentInstance;
        public Template1(WorkInfo parent, WorkInfo parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> __self_PlaceHolderName_inst;
        static Template1()
        {
        }
        public override object New()
        {
            var __self = new global::InputField();
            __self_PlaceHolderName_inst = new OSP_InputField_PlaceHolderName_Property(__self, __selector0);
            var temp = new global::Fuse.Reactive.Data("description");
            var temp1 = new global::Fuse.Reactive.DataBinding(__self_PlaceHolderName_inst, temp, Fuse.Reactive.BindingMode.Default);
            __self.Title = "DESCRIPTION";
            __self.SourceLineNumber = 271;
            __self.SourceFileName = "workinfo.ux";
            temp.SourceLineNumber = 271;
            temp.SourceFileName = "workinfo.ux";
            __self.Bindings.Add(temp1);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "PlaceHolderName";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template2: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly WorkInfo __parent;
        [Uno.WeakReference] internal readonly WorkInfo __parentInstance;
        public Template2(WorkInfo parent, WorkInfo parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> __self_PlaceHolderName_inst;
        static Template2()
        {
        }
        public override object New()
        {
            var __self = new global::InputField();
            __self_PlaceHolderName_inst = new OSP_InputField_PlaceHolderName_Property(__self, __selector0);
            var temp = new global::Fuse.Reactive.Data("issue_date");
            var temp1 = new global::Fuse.Reactive.DataBinding(__self_PlaceHolderName_inst, temp, Fuse.Reactive.BindingMode.Default);
            __self.Title = "ISSUE DATE";
            __self.SourceLineNumber = 273;
            __self.SourceFileName = "workinfo.ux";
            temp.SourceLineNumber = 273;
            temp.SourceFileName = "workinfo.ux";
            __self.Bindings.Add(temp1);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "PlaceHolderName";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template3: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly WorkInfo __parent;
        [Uno.WeakReference] internal readonly WorkInfo __parentInstance;
        public Template3(WorkInfo parent, WorkInfo parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> __self_PlaceHolderName_inst;
        static Template3()
        {
        }
        public override object New()
        {
            var __self = new global::InputField();
            __self_PlaceHolderName_inst = new OSP_InputField_PlaceHolderName_Property(__self, __selector0);
            var temp = new global::Fuse.Reactive.Data("expiry_date");
            var temp1 = new global::Fuse.Reactive.DataBinding(__self_PlaceHolderName_inst, temp, Fuse.Reactive.BindingMode.Default);
            __self.Title = "EXPIRY DATE";
            __self.SourceLineNumber = 274;
            __self.SourceFileName = "workinfo.ux";
            temp.SourceLineNumber = 274;
            temp.SourceFileName = "workinfo.ux";
            __self.Bindings.Add(temp1);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "PlaceHolderName";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template4: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly WorkInfo __parent;
        [Uno.WeakReference] internal readonly WorkInfo __parentInstance;
        public Template4(WorkInfo parent, WorkInfo parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> __self_PlaceHolderName_inst;
        static Template4()
        {
        }
        public override object New()
        {
            var __self = new global::InputField();
            var temp = new global::Fuse.Reactive.Data("valid_for");
            var temp1 = " months";
            var temp2 = new global::Fuse.Reactive.Constant(temp1);
            __self_PlaceHolderName_inst = new OSP_InputField_PlaceHolderName_Property(__self, __selector0);
            var temp3 = new global::Fuse.Reactive.Add(temp, temp2);
            var temp4 = new global::Fuse.Reactive.DataBinding(__self_PlaceHolderName_inst, temp3, Fuse.Reactive.BindingMode.Default);
            __self.Title = "VALID FOR";
            __self.SourceLineNumber = 276;
            __self.SourceFileName = "workinfo.ux";
            temp3.SourceLineNumber = 276;
            temp3.SourceFileName = "workinfo.ux";
            temp.SourceLineNumber = 276;
            temp.SourceFileName = "workinfo.ux";
            temp2.SourceLineNumber = 276;
            temp2.SourceFileName = "workinfo.ux";
            __self.Bindings.Add(temp4);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "PlaceHolderName";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template5: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly WorkInfo __parent;
        [Uno.WeakReference] internal readonly WorkInfo __parentInstance;
        public Template5(WorkInfo parent, WorkInfo parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        static Template5()
        {
        }
        public override object New()
        {
            var __self = new global::Fuse.Controls.Panel();
            __self.Width = new Uno.UX.Size(80f, Uno.UX.Unit.Percent);
            __self.Margin = float4(0f, 5f, 0f, 5f);
            __self.SourceLineNumber = 278;
            __self.SourceFileName = "workinfo.ux";
            return __self;
        }
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template6: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly WorkInfo __parent;
        [Uno.WeakReference] internal readonly WorkInfo __parentInstance;
        public Template6(WorkInfo parent, WorkInfo parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> __self_PlaceHolderName_inst;
        static Template6()
        {
        }
        public override object New()
        {
            var __self = new global::InputField();
            __self_PlaceHolderName_inst = new OSP_InputField_PlaceHolderName_Property(__self, __selector0);
            var temp = new global::Fuse.Reactive.Data("course");
            var temp1 = new global::Fuse.Reactive.DataBinding(__self_PlaceHolderName_inst, temp, Fuse.Reactive.BindingMode.Default);
            __self.Title = "TITLE";
            __self.SourceLineNumber = 340;
            __self.SourceFileName = "workinfo.ux";
            temp.SourceLineNumber = 340;
            temp.SourceFileName = "workinfo.ux";
            __self.Bindings.Add(temp1);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "PlaceHolderName";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template7: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly WorkInfo __parent;
        [Uno.WeakReference] internal readonly WorkInfo __parentInstance;
        public Template7(WorkInfo parent, WorkInfo parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> __self_PlaceHolderName_inst;
        static Template7()
        {
        }
        public override object New()
        {
            var __self = new global::InputField();
            __self_PlaceHolderName_inst = new OSP_InputField_PlaceHolderName_Property(__self, __selector0);
            var temp = new global::Fuse.Reactive.Data("description");
            var temp1 = new global::Fuse.Reactive.DataBinding(__self_PlaceHolderName_inst, temp, Fuse.Reactive.BindingMode.Default);
            __self.Title = "DESCRIPTION";
            __self.SourceLineNumber = 341;
            __self.SourceFileName = "workinfo.ux";
            temp.SourceLineNumber = 341;
            temp.SourceFileName = "workinfo.ux";
            __self.Bindings.Add(temp1);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "PlaceHolderName";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template8: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly WorkInfo __parent;
        [Uno.WeakReference] internal readonly WorkInfo __parentInstance;
        public Template8(WorkInfo parent, WorkInfo parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> __self_PlaceHolderName_inst;
        static Template8()
        {
        }
        public override object New()
        {
            var __self = new global::InputField();
            __self_PlaceHolderName_inst = new OSP_InputField_PlaceHolderName_Property(__self, __selector0);
            var temp = new global::Fuse.Reactive.Data("issue_date");
            var temp1 = new global::Fuse.Reactive.DataBinding(__self_PlaceHolderName_inst, temp, Fuse.Reactive.BindingMode.Default);
            __self.Title = "ISSUE DATE";
            __self.SourceLineNumber = 343;
            __self.SourceFileName = "workinfo.ux";
            temp.SourceLineNumber = 343;
            temp.SourceFileName = "workinfo.ux";
            __self.Bindings.Add(temp1);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "PlaceHolderName";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template9: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly WorkInfo __parent;
        [Uno.WeakReference] internal readonly WorkInfo __parentInstance;
        public Template9(WorkInfo parent, WorkInfo parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> __self_PlaceHolderName_inst;
        static Template9()
        {
        }
        public override object New()
        {
            var __self = new global::InputField();
            __self_PlaceHolderName_inst = new OSP_InputField_PlaceHolderName_Property(__self, __selector0);
            var temp = new global::Fuse.Reactive.Data("expiry_date");
            var temp1 = new global::Fuse.Reactive.DataBinding(__self_PlaceHolderName_inst, temp, Fuse.Reactive.BindingMode.Default);
            __self.Title = "EXPIRY DATE";
            __self.SourceLineNumber = 344;
            __self.SourceFileName = "workinfo.ux";
            temp.SourceLineNumber = 344;
            temp.SourceFileName = "workinfo.ux";
            __self.Bindings.Add(temp1);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "PlaceHolderName";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template10: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly WorkInfo __parent;
        [Uno.WeakReference] internal readonly WorkInfo __parentInstance;
        public Template10(WorkInfo parent, WorkInfo parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> __self_PlaceHolderName_inst;
        static Template10()
        {
        }
        public override object New()
        {
            var __self = new global::InputField();
            var temp = new global::Fuse.Reactive.Data("valid_for");
            var temp1 = " months";
            var temp2 = new global::Fuse.Reactive.Constant(temp1);
            __self_PlaceHolderName_inst = new OSP_InputField_PlaceHolderName_Property(__self, __selector0);
            var temp3 = new global::Fuse.Reactive.Add(temp, temp2);
            var temp4 = new global::Fuse.Reactive.DataBinding(__self_PlaceHolderName_inst, temp3, Fuse.Reactive.BindingMode.Default);
            __self.Title = "VALID FOR";
            __self.SourceLineNumber = 346;
            __self.SourceFileName = "workinfo.ux";
            temp3.SourceLineNumber = 346;
            temp3.SourceFileName = "workinfo.ux";
            temp.SourceLineNumber = 346;
            temp.SourceFileName = "workinfo.ux";
            temp2.SourceLineNumber = 346;
            temp2.SourceFileName = "workinfo.ux";
            __self.Bindings.Add(temp4);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "PlaceHolderName";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template11: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly WorkInfo __parent;
        [Uno.WeakReference] internal readonly WorkInfo __parentInstance;
        public Template11(WorkInfo parent, WorkInfo parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> __self_PlaceHolderName_inst;
        static Template11()
        {
        }
        public override object New()
        {
            var __self = new global::InputField();
            __self_PlaceHolderName_inst = new OSP_InputField_PlaceHolderName_Property(__self, __selector0);
            var temp = new global::Fuse.Reactive.Data("course");
            var temp1 = new global::Fuse.Reactive.DataBinding(__self_PlaceHolderName_inst, temp, Fuse.Reactive.BindingMode.Default);
            __self.Title = "TITLE";
            __self.SourceLineNumber = 408;
            __self.SourceFileName = "workinfo.ux";
            temp.SourceLineNumber = 408;
            temp.SourceFileName = "workinfo.ux";
            __self.Bindings.Add(temp1);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "PlaceHolderName";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template12: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly WorkInfo __parent;
        [Uno.WeakReference] internal readonly WorkInfo __parentInstance;
        public Template12(WorkInfo parent, WorkInfo parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> __self_PlaceHolderName_inst;
        static Template12()
        {
        }
        public override object New()
        {
            var __self = new global::InputField();
            __self_PlaceHolderName_inst = new OSP_InputField_PlaceHolderName_Property(__self, __selector0);
            var temp = new global::Fuse.Reactive.Data("description");
            var temp1 = new global::Fuse.Reactive.DataBinding(__self_PlaceHolderName_inst, temp, Fuse.Reactive.BindingMode.Default);
            __self.Title = "DESCRIPTION";
            __self.SourceLineNumber = 409;
            __self.SourceFileName = "workinfo.ux";
            temp.SourceLineNumber = 409;
            temp.SourceFileName = "workinfo.ux";
            __self.Bindings.Add(temp1);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "PlaceHolderName";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template13: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly WorkInfo __parent;
        [Uno.WeakReference] internal readonly WorkInfo __parentInstance;
        public Template13(WorkInfo parent, WorkInfo parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> __self_PlaceHolderName_inst;
        static Template13()
        {
        }
        public override object New()
        {
            var __self = new global::InputField();
            __self_PlaceHolderName_inst = new OSP_InputField_PlaceHolderName_Property(__self, __selector0);
            var temp = new global::Fuse.Reactive.Data("issue_date");
            var temp1 = new global::Fuse.Reactive.DataBinding(__self_PlaceHolderName_inst, temp, Fuse.Reactive.BindingMode.Default);
            __self.Title = "ISSUE DATE";
            __self.SourceLineNumber = 411;
            __self.SourceFileName = "workinfo.ux";
            temp.SourceLineNumber = 411;
            temp.SourceFileName = "workinfo.ux";
            __self.Bindings.Add(temp1);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "PlaceHolderName";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template14: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly WorkInfo __parent;
        [Uno.WeakReference] internal readonly WorkInfo __parentInstance;
        public Template14(WorkInfo parent, WorkInfo parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> __self_PlaceHolderName_inst;
        static Template14()
        {
        }
        public override object New()
        {
            var __self = new global::InputField();
            __self_PlaceHolderName_inst = new OSP_InputField_PlaceHolderName_Property(__self, __selector0);
            var temp = new global::Fuse.Reactive.Data("expiry_date");
            var temp1 = new global::Fuse.Reactive.DataBinding(__self_PlaceHolderName_inst, temp, Fuse.Reactive.BindingMode.Default);
            __self.Title = "EXPIRY DATE";
            __self.SourceLineNumber = 412;
            __self.SourceFileName = "workinfo.ux";
            temp.SourceLineNumber = 412;
            temp.SourceFileName = "workinfo.ux";
            __self.Bindings.Add(temp1);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "PlaceHolderName";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template15: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly WorkInfo __parent;
        [Uno.WeakReference] internal readonly WorkInfo __parentInstance;
        public Template15(WorkInfo parent, WorkInfo parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> __self_PlaceHolderName_inst;
        static Template15()
        {
        }
        public override object New()
        {
            var __self = new global::InputField();
            var temp = new global::Fuse.Reactive.Data("valid_for");
            var temp1 = " months";
            var temp2 = new global::Fuse.Reactive.Constant(temp1);
            __self_PlaceHolderName_inst = new OSP_InputField_PlaceHolderName_Property(__self, __selector0);
            var temp3 = new global::Fuse.Reactive.Add(temp, temp2);
            var temp4 = new global::Fuse.Reactive.DataBinding(__self_PlaceHolderName_inst, temp3, Fuse.Reactive.BindingMode.Default);
            __self.Title = "VALID FOR";
            __self.SourceLineNumber = 414;
            __self.SourceFileName = "workinfo.ux";
            temp3.SourceLineNumber = 414;
            temp3.SourceFileName = "workinfo.ux";
            temp.SourceLineNumber = 414;
            temp.SourceFileName = "workinfo.ux";
            temp2.SourceLineNumber = 414;
            temp2.SourceFileName = "workinfo.ux";
            __self.Bindings.Add(temp4);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "PlaceHolderName";
    }
    global::Uno.UX.Property<Fuse.Visual> navigation_Active_inst;
    global::Uno.UX.Property<float4> permit_Color_inst;
    global::Uno.UX.Property<float4> permit_TColor_inst;
    global::Uno.UX.Property<float> loadingPanel_Opacity_inst;
    global::Uno.UX.Property<float> WorkPage_Opacity_inst;
    global::Uno.UX.Property<string> temp_Value_inst;
    global::Uno.UX.Property<float> load_Opacity_inst;
    global::Uno.UX.Property<float> errorMsg_Opacity_inst;
    global::Uno.UX.Property<bool> temp1_Value_inst;
    global::Uno.UX.Property<bool> temp2_Value_inst;
    global::Uno.UX.Property<object> temp3_Items_inst;
    global::Uno.UX.Property<float4> train_Color_inst;
    global::Uno.UX.Property<float4> train_TColor_inst;
    global::Uno.UX.Property<float> loadingPanel1_Opacity_inst;
    global::Uno.UX.Property<float> WorkPage1_Opacity_inst;
    global::Uno.UX.Property<string> temp4_Value_inst;
    global::Uno.UX.Property<float> load1_Opacity_inst;
    global::Uno.UX.Property<float> errorMsg1_Opacity_inst;
    global::Uno.UX.Property<bool> temp5_Value_inst;
    global::Uno.UX.Property<bool> temp6_Value_inst;
    global::Uno.UX.Property<object> temp7_Items_inst;
    global::Uno.UX.Property<float4> medical_Color_inst;
    global::Uno.UX.Property<float4> medical_TColor_inst;
    global::Uno.UX.Property<float> loadingPanel2_Opacity_inst;
    global::Uno.UX.Property<float> WorkPage2_Opacity_inst;
    global::Uno.UX.Property<string> temp8_Value_inst;
    global::Uno.UX.Property<float> load2_Opacity_inst;
    global::Uno.UX.Property<float> errorMsg2_Opacity_inst;
    global::Uno.UX.Property<bool> temp9_Value_inst;
    global::Uno.UX.Property<bool> temp10_Value_inst;
    global::Uno.UX.Property<object> temp11_Items_inst;
    internal global::Fuse.Controls.Panel page1Tab;
    internal global::Tab permit;
    internal global::Fuse.Controls.Panel page2Tab;
    internal global::Tab train;
    internal global::Fuse.Reactive.EventBinding temp_eb26;
    internal global::Fuse.Controls.Panel page3Tab;
    internal global::Tab medical;
    internal global::Fuse.Controls.PageControl navigation;
    internal global::Fuse.Controls.Page page1;
    internal global::Fuse.Controls.Panel loadingPanel;
    internal global::Fuse.Controls.Image load;
    internal global::Fuse.Controls.StackPanel errorMsg;
    internal global::Fuse.Reactive.EventBinding temp_eb27;
    internal global::Fuse.Reactive.EventBinding temp_eb28;
    internal global::Fuse.Reactive.EventBinding temp_eb29;
    internal global::Fuse.Triggers.Busy busy;
    internal global::Fuse.Controls.ScrollView WorkPage;
    internal global::Fuse.Controls.Page page2;
    internal global::Fuse.Controls.Panel loadingPanel1;
    internal global::Fuse.Controls.Image load1;
    internal global::Fuse.Controls.StackPanel errorMsg1;
    internal global::Fuse.Reactive.EventBinding temp_eb30;
    internal global::Fuse.Reactive.EventBinding temp_eb31;
    internal global::Fuse.Reactive.EventBinding temp_eb32;
    internal global::Fuse.Triggers.Busy busy1;
    internal global::Fuse.Controls.ScrollView WorkPage1;
    internal global::Fuse.Controls.Page page3;
    internal global::Fuse.Controls.Panel loadingPanel2;
    internal global::Fuse.Controls.Image load2;
    internal global::Fuse.Controls.StackPanel errorMsg2;
    internal global::Fuse.Reactive.EventBinding temp_eb33;
    internal global::Fuse.Reactive.EventBinding temp_eb34;
    internal global::Fuse.Reactive.EventBinding temp_eb35;
    internal global::Fuse.Triggers.Busy busy2;
    internal global::Fuse.Controls.ScrollView WorkPage2;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "page1Tab",
        "permit",
        "page2Tab",
        "train",
        "temp_eb26",
        "page3Tab",
        "medical",
        "navigation",
        "page1",
        "loadingPanel",
        "load",
        "errorMsg",
        "temp_eb27",
        "temp_eb28",
        "temp_eb29",
        "busy",
        "WorkPage",
        "page2",
        "loadingPanel1",
        "load1",
        "errorMsg1",
        "temp_eb30",
        "temp_eb31",
        "temp_eb32",
        "busy1",
        "WorkPage1",
        "page3",
        "loadingPanel2",
        "load2",
        "errorMsg2",
        "temp_eb33",
        "temp_eb34",
        "temp_eb35",
        "busy2",
        "WorkPage2"
    };
    static WorkInfo()
    {
    }
    [global::Uno.UX.UXConstructor]
    public WorkInfo(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        navigation = new global::Fuse.Controls.PageControl();
        navigation_Active_inst = new OSP_FuseControlsNavigationControl_Active_Property(navigation, __selector0);
        var temp12 = new global::Fuse.Reactive.Data("Loadtraining");
        permit = new global::Tab();
        permit_Color_inst = new OSP_FuseControlsPanel_Color_Property(permit, __selector1);
        permit_TColor_inst = new OSP_Tab_TColor_Property(permit, __selector2);
        loadingPanel = new global::Fuse.Controls.Panel();
        loadingPanel_Opacity_inst = new OSP_FuseElementsElement_Opacity_Property(loadingPanel, __selector3);
        WorkPage = new global::Fuse.Controls.ScrollView();
        WorkPage_Opacity_inst = new OSP_FuseElementsElement_Opacity_Property(WorkPage, __selector3);
        var temp = new global::Fuse.Controls.Text();
        temp_Value_inst = new OSP_FuseControlsTextControl_Value_Property(temp, __selector4);
        var temp13 = new global::Fuse.Reactive.Data("Msg");
        var temp14 = new global::Fuse.Reactive.Data("startLoad");
        var temp15 = new global::Fuse.Reactive.Data("dashboard");
        load = new global::Fuse.Controls.Image();
        load_Opacity_inst = new OSP_FuseElementsElement_Opacity_Property(load, __selector3);
        errorMsg = new global::Fuse.Controls.StackPanel();
        errorMsg_Opacity_inst = new OSP_FuseElementsElement_Opacity_Property(errorMsg, __selector3);
        var temp1 = new global::Fuse.Triggers.WhileTrue();
        temp1_Value_inst = new OSP_FuseTriggersWhileBool_Value_Property(temp1, __selector4);
        var temp16 = new global::Fuse.Reactive.Data("error");
        var temp2 = new global::Fuse.Triggers.WhileFalse();
        temp2_Value_inst = new OSP_FuseTriggersWhileBool_Value_Property(temp2, __selector4);
        var temp17 = new global::Fuse.Reactive.Data("error");
        var temp18 = new global::Fuse.Reactive.Data("startLoad");
        var temp3 = new global::Fuse.Reactive.Each();
        temp3_Items_inst = new OSP_FuseReactiveEach_Items_Property(temp3, __selector5);
        var temp19 = new global::Fuse.Reactive.Data("permit");
        train = new global::Tab();
        train_Color_inst = new OSP_FuseControlsPanel_Color_Property(train, __selector1);
        train_TColor_inst = new OSP_Tab_TColor_Property(train, __selector2);
        loadingPanel1 = new global::Fuse.Controls.Panel();
        loadingPanel1_Opacity_inst = new OSP_FuseElementsElement_Opacity_Property(loadingPanel1, __selector3);
        WorkPage1 = new global::Fuse.Controls.ScrollView();
        WorkPage1_Opacity_inst = new OSP_FuseElementsElement_Opacity_Property(WorkPage1, __selector3);
        var temp4 = new global::Fuse.Controls.Text();
        temp4_Value_inst = new OSP_FuseControlsTextControl_Value_Property(temp4, __selector4);
        var temp20 = new global::Fuse.Reactive.Data("Msg");
        var temp21 = new global::Fuse.Reactive.Data("Loadtraining");
        var temp22 = new global::Fuse.Reactive.Data("dashboard");
        load1 = new global::Fuse.Controls.Image();
        load1_Opacity_inst = new OSP_FuseElementsElement_Opacity_Property(load1, __selector3);
        errorMsg1 = new global::Fuse.Controls.StackPanel();
        errorMsg1_Opacity_inst = new OSP_FuseElementsElement_Opacity_Property(errorMsg1, __selector3);
        var temp5 = new global::Fuse.Triggers.WhileTrue();
        temp5_Value_inst = new OSP_FuseTriggersWhileBool_Value_Property(temp5, __selector4);
        var temp23 = new global::Fuse.Reactive.Data("error");
        var temp6 = new global::Fuse.Triggers.WhileFalse();
        temp6_Value_inst = new OSP_FuseTriggersWhileBool_Value_Property(temp6, __selector4);
        var temp24 = new global::Fuse.Reactive.Data("error");
        var temp25 = new global::Fuse.Reactive.Data("Loadtraining");
        var temp7 = new global::Fuse.Reactive.Each();
        temp7_Items_inst = new OSP_FuseReactiveEach_Items_Property(temp7, __selector5);
        var temp26 = new global::Fuse.Reactive.Data("training");
        medical = new global::Tab();
        medical_Color_inst = new OSP_FuseControlsPanel_Color_Property(medical, __selector1);
        medical_TColor_inst = new OSP_Tab_TColor_Property(medical, __selector2);
        loadingPanel2 = new global::Fuse.Controls.Panel();
        loadingPanel2_Opacity_inst = new OSP_FuseElementsElement_Opacity_Property(loadingPanel2, __selector3);
        WorkPage2 = new global::Fuse.Controls.ScrollView();
        WorkPage2_Opacity_inst = new OSP_FuseElementsElement_Opacity_Property(WorkPage2, __selector3);
        var temp8 = new global::Fuse.Controls.Text();
        temp8_Value_inst = new OSP_FuseControlsTextControl_Value_Property(temp8, __selector4);
        var temp27 = new global::Fuse.Reactive.Data("Msg");
        var temp28 = new global::Fuse.Reactive.Data("LoadMedical");
        var temp29 = new global::Fuse.Reactive.Data("dashboard");
        load2 = new global::Fuse.Controls.Image();
        load2_Opacity_inst = new OSP_FuseElementsElement_Opacity_Property(load2, __selector3);
        errorMsg2 = new global::Fuse.Controls.StackPanel();
        errorMsg2_Opacity_inst = new OSP_FuseElementsElement_Opacity_Property(errorMsg2, __selector3);
        var temp9 = new global::Fuse.Triggers.WhileTrue();
        temp9_Value_inst = new OSP_FuseTriggersWhileBool_Value_Property(temp9, __selector4);
        var temp30 = new global::Fuse.Reactive.Data("error");
        var temp10 = new global::Fuse.Triggers.WhileFalse();
        temp10_Value_inst = new OSP_FuseTriggersWhileBool_Value_Property(temp10, __selector4);
        var temp31 = new global::Fuse.Reactive.Data("error");
        var temp32 = new global::Fuse.Reactive.Data("LoadMedical");
        var temp11 = new global::Fuse.Reactive.Each();
        temp11_Items_inst = new OSP_FuseReactiveEach_Items_Property(temp11, __selector5);
        var temp33 = new global::Fuse.Reactive.Data("medical");
        var temp34 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp35 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp36 = new global::Fuse.Controls.Grid();
        var temp37 = new global::TopBar();
        var temp38 = new global::Fuse.Controls.StackPanel();
        var temp39 = new global::Fuse.Controls.Grid();
        page1Tab = new global::Fuse.Controls.Panel();
        var temp40 = new global::Fuse.Gestures.Clicked();
        var temp41 = new global::Fuse.Triggers.Actions.Set<Fuse.Visual>(navigation_Active_inst);
        page2Tab = new global::Fuse.Controls.Panel();
        var temp42 = new global::Fuse.Gestures.Clicked();
        var temp43 = new global::Fuse.Triggers.Actions.Set<Fuse.Visual>(navigation_Active_inst);
        var temp44 = new global::Fuse.Triggers.Actions.Callback();
        temp_eb26 = new global::Fuse.Reactive.EventBinding(temp12);
        page3Tab = new global::Fuse.Controls.Panel();
        var temp45 = new global::Fuse.Gestures.Clicked();
        var temp46 = new global::Fuse.Triggers.Actions.Set<Fuse.Visual>(navigation_Active_inst);
        var temp47 = new global::Fuse.Drawing.StaticSolidColor(float4(0.9686275f, 0.8980392f, 0.3686275f, 1f));
        page1 = new global::Fuse.Controls.Page();
        var temp48 = new global::Fuse.Navigation.WhileActive();
        var temp49 = new global::Fuse.Animations.Change<float4>(permit_Color_inst);
        var temp50 = new global::Fuse.Animations.Change<float4>(permit_TColor_inst);
        var temp51 = new global::Fuse.Triggers.WhileBusy();
        var temp52 = new global::Fuse.Animations.Change<float>(loadingPanel_Opacity_inst);
        var temp53 = new global::Fuse.Animations.Change<float>(WorkPage_Opacity_inst);
        var temp54 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp13, Fuse.Reactive.BindingMode.Default);
        var temp55 = new global::Fuse.Controls.DockPanel();
        var temp56 = new global::SubBut();
        temp_eb27 = new global::Fuse.Reactive.EventBinding(temp14);
        var temp57 = new global::SubBut();
        temp_eb28 = new global::Fuse.Reactive.EventBinding(temp15);
        var temp58 = new global::Fuse.Animations.Change<float>(load_Opacity_inst);
        var temp59 = new global::Fuse.Animations.Change<float>(errorMsg_Opacity_inst);
        var temp60 = new global::Fuse.Reactive.DataBinding(temp1_Value_inst, temp16, Fuse.Reactive.BindingMode.Default);
        var temp61 = new global::Fuse.Animations.Change<float>(load_Opacity_inst);
        var temp62 = new global::Fuse.Animations.Spin();
        var temp63 = new global::Fuse.Animations.Change<float>(errorMsg_Opacity_inst);
        var temp64 = new global::Fuse.Reactive.DataBinding(temp2_Value_inst, temp17, Fuse.Reactive.BindingMode.Default);
        var temp65 = new global::Fuse.Navigation.Activated();
        temp_eb29 = new global::Fuse.Reactive.EventBinding(temp18);
        busy = new global::Fuse.Triggers.Busy();
        var temp66 = new global::Fuse.Controls.StackPanel();
        var temp67 = new Template(this, this);
        var temp68 = new Template1(this, this);
        var temp69 = new Template2(this, this);
        var temp70 = new Template3(this, this);
        var temp71 = new Template4(this, this);
        var temp72 = new Template5(this, this);
        var temp73 = new global::Fuse.Reactive.DataBinding(temp3_Items_inst, temp19, Fuse.Reactive.BindingMode.Default);
        var temp74 = new global::Fuse.Drawing.StaticSolidColor(float4(1f, 1f, 1f, 1f));
        page2 = new global::Fuse.Controls.Page();
        var temp75 = new global::Fuse.Navigation.WhileActive();
        var temp76 = new global::Fuse.Animations.Change<float4>(train_Color_inst);
        var temp77 = new global::Fuse.Animations.Change<float4>(train_TColor_inst);
        var temp78 = new global::Fuse.Navigation.WhileInactive();
        var temp79 = new global::Fuse.Animations.Change<float4>(train_Color_inst);
        var temp80 = new global::Fuse.Triggers.WhileBusy();
        var temp81 = new global::Fuse.Animations.Change<float>(loadingPanel1_Opacity_inst);
        var temp82 = new global::Fuse.Animations.Change<float>(WorkPage1_Opacity_inst);
        var temp83 = new global::Fuse.Reactive.DataBinding(temp4_Value_inst, temp20, Fuse.Reactive.BindingMode.Default);
        var temp84 = new global::Fuse.Controls.DockPanel();
        var temp85 = new global::SubBut();
        temp_eb30 = new global::Fuse.Reactive.EventBinding(temp21);
        var temp86 = new global::SubBut();
        temp_eb31 = new global::Fuse.Reactive.EventBinding(temp22);
        var temp87 = new global::Fuse.Animations.Change<float>(load1_Opacity_inst);
        var temp88 = new global::Fuse.Animations.Change<float>(errorMsg1_Opacity_inst);
        var temp89 = new global::Fuse.Reactive.DataBinding(temp5_Value_inst, temp23, Fuse.Reactive.BindingMode.Default);
        var temp90 = new global::Fuse.Animations.Change<float>(load1_Opacity_inst);
        var temp91 = new global::Fuse.Animations.Spin();
        var temp92 = new global::Fuse.Animations.Change<float>(errorMsg1_Opacity_inst);
        var temp93 = new global::Fuse.Reactive.DataBinding(temp6_Value_inst, temp24, Fuse.Reactive.BindingMode.Default);
        var temp94 = new global::Fuse.Navigation.Activated();
        temp_eb32 = new global::Fuse.Reactive.EventBinding(temp25);
        busy1 = new global::Fuse.Triggers.Busy();
        var temp95 = new global::Fuse.Controls.StackPanel();
        var temp96 = new Template6(this, this);
        var temp97 = new Template7(this, this);
        var temp98 = new Template8(this, this);
        var temp99 = new Template9(this, this);
        var temp100 = new Template10(this, this);
        var temp101 = new global::Fuse.Reactive.DataBinding(temp7_Items_inst, temp26, Fuse.Reactive.BindingMode.Default);
        var temp102 = new global::Fuse.Drawing.StaticSolidColor(float4(1f, 1f, 1f, 1f));
        page3 = new global::Fuse.Controls.Page();
        var temp103 = new global::Fuse.Navigation.WhileActive();
        var temp104 = new global::Fuse.Animations.Change<float4>(medical_Color_inst);
        var temp105 = new global::Fuse.Animations.Change<float4>(medical_TColor_inst);
        var temp106 = new global::Fuse.Navigation.WhileInactive();
        var temp107 = new global::Fuse.Animations.Change<float4>(medical_Color_inst);
        var temp108 = new global::Fuse.Triggers.WhileBusy();
        var temp109 = new global::Fuse.Animations.Change<float>(loadingPanel2_Opacity_inst);
        var temp110 = new global::Fuse.Animations.Change<float>(WorkPage2_Opacity_inst);
        var temp111 = new global::Fuse.Reactive.DataBinding(temp8_Value_inst, temp27, Fuse.Reactive.BindingMode.Default);
        var temp112 = new global::Fuse.Controls.DockPanel();
        var temp113 = new global::SubBut();
        temp_eb33 = new global::Fuse.Reactive.EventBinding(temp28);
        var temp114 = new global::SubBut();
        temp_eb34 = new global::Fuse.Reactive.EventBinding(temp29);
        var temp115 = new global::Fuse.Animations.Change<float>(load2_Opacity_inst);
        var temp116 = new global::Fuse.Animations.Change<float>(errorMsg2_Opacity_inst);
        var temp117 = new global::Fuse.Reactive.DataBinding(temp9_Value_inst, temp30, Fuse.Reactive.BindingMode.Default);
        var temp118 = new global::Fuse.Animations.Change<float>(load2_Opacity_inst);
        var temp119 = new global::Fuse.Animations.Spin();
        var temp120 = new global::Fuse.Animations.Change<float>(errorMsg2_Opacity_inst);
        var temp121 = new global::Fuse.Reactive.DataBinding(temp10_Value_inst, temp31, Fuse.Reactive.BindingMode.Default);
        var temp122 = new global::Fuse.Navigation.Activated();
        temp_eb35 = new global::Fuse.Reactive.EventBinding(temp32);
        busy2 = new global::Fuse.Triggers.Busy();
        var temp123 = new global::Fuse.Controls.StackPanel();
        var temp124 = new Template11(this, this);
        var temp125 = new Template12(this, this);
        var temp126 = new Template13(this, this);
        var temp127 = new Template14(this, this);
        var temp128 = new Template15(this, this);
        var temp129 = new global::Fuse.Reactive.DataBinding(temp11_Items_inst, temp33, Fuse.Reactive.BindingMode.Default);
        var temp130 = new global::Fuse.Drawing.StaticSolidColor(float4(1f, 1f, 1f, 1f));
        this.Color = float4(1f, 1f, 1f, 1f);
        this.SourceLineNumber = 1;
        this.SourceFileName = "workinfo.ux";
        temp34.LineNumber = 3;
        temp34.FileName = "workinfo.ux";
        temp34.SourceLineNumber = 3;
        temp34.SourceFileName = "workinfo.ux";
        temp34.File = new global::Uno.UX.BundleFileSource(import("../../../../../js_files/navigation.js"));
        temp35.Code = "\n\t\tvar API = require('/js_files/api.js');\n\t\tvar Observable = require('FuseJS/Observable');\n\t\tvar Storage = require(\"FuseJS/Storage\");\n\t\tdataPath = API.readOSPID();\n\t\tvar permit = Observable();\n\t\tvar training = Observable();\n\t\tvar medical = Observable();\n\n\t\tvar login = require('js_files/login.js');\n\t\tvar error = Observable(false);\n\t\tvar Msg = Observable(\"There was an error\");\n\n\t\tfunction startLoad() {\n\t\t\terror.value = false;\n\t\t\tbusy.activate();\n\t\t\tStorage.read(dataPath).then(function(content) {\n\t\t\t    var cont = JSON.parse(content);\n\t\t\t    personnel_id = cont.personnel[0].personnel_id;\n\t\t\t    fetch('http://41.75.207.60/osp/public/api/permits/'+personnel_id )\n\t\t\t\t\t.then(function(response) { \n\t\t\t\t\t\tstatus = response.status;  // Get the HTTP status code\n\t\t    \t\t\tresponse_ok = response.ok; // Is response.status in the 200-range?\n\t\t\t\t\t\treturn response.json(); \n\t\t\t\t\t})\n\t\t\t\t.then(function (responseObject) {\n\t\t\t\tif(responseObject != 0)\n\t\t\t\t{\n\t\t\t\t\tpermit.replaceAll(responseObject);\n\t\t\t\t\t//console.log(\"it works: \"+JSON.stringify(responseObject));\t\n\t\t\t\t}\n\t\t\t\telse\n\t\t\t\t{\n\t\t\t\t\tMsg.value = \"User has no permits record\";\n\t\t\t\t\t \n\t\t\t\t}\n\t\t\t\tbusy.deactivate();\n\t\t\t}).catch(function(err) {\n\t\t\t\tconsole.log(err+ \" dSome There was an error\");\n\t\t\t\tconsole.log(status)\n\t\t\t\tif (status == 502) {\n\t\t\t\t\t// console.log(\"Error Address\");\n\t\t\t\t\tMsg.value = \"Wrong Address\";\n\t\t\t\t}else{\n\t\t\t\t console.log(JSON.stringify(responseObject));\n\t\t\t\t\tconsole.log(\"Record was not found\");\n\t\t\t\t\tMsg.value = \"User has no permits record\";\n\t\t\t\t}\n\t\t\t\t// console.log(response_ok);\n\t\t\t\t busy.deactivate();\n\t\t\t\terror.value = true;\n\t\t\t});\n\t\t\t    \n\t\t\t    \n\t\t\t    \n\n\t\t\t}, function(error) {\n\t\t\t    //For now, let's expect the error to be because of the file not being found.\n\t\t\t    //welcomeText.value = \"There is currently no local data stored\";\n\t\t\t});\n\t\t\t\n\t\t}\n\n\t\tfunction dstartLoad() {\n\t\t\terror.value = false;\n\t\t\tbusy.activate();\n\t\t\t\n\t\t}\n\n\t\tfunction Loadtraining() {\n\t\t\terror.value = false;\n\t\t\tbusy1.activate();\n\t\t\tStorage.read(dataPath).then(function(content) {\n\t\t\t    var cont = JSON.parse(content);\n\t\t\t    personnel_id = cont.personnel[0].personnel_id;\n\t\t\t    fetch('http://41.75.207.60/osp/public/api/training/'+personnel_id )\n\t\t\t\t\t.then(function(response) { \n\t\t\t\t\t\tstatus = response.status;  // Get the HTTP status code\n\t\t    \t\t\tresponse_ok = response.ok; // Is response.status in the 200-range?\n\t\t\t\t\t\treturn response.json(); \n\t\t\t\t\t})\n\t\t\t\t.then(function (responseObject) {\n\t\t\t\tif(responseObject != 0)\n\t\t\t\t{\n\t\t\t\t\ttraining.replaceAll(responseObject);\n\t\t\t\t\t//console.log(\"it works: \"+JSON.stringify(responseObject));\t\n\t\t\t\t}\n\t\t\t\telse\n\t\t\t\t{\n\t\t\t\t\tMsg.value = \"User has no training record\";\n\t\t\t\t\t \n\t\t\t\t}\n\t\t\t\tbusy1.deactivate();\n\t\t\t}).catch(function(err) {\n\t\t\t\tconsole.log(err+ \" dSome There was an error\");\n\t\t\t\tconsole.log(status)\n\t\t\t\tif (status == 502) {\n\t\t\t\t\t// console.log(\"Error Address\");\n\t\t\t\t\tMsg.value = \"Wrong Address\";\n\t\t\t\t}else{\n\t\t\t\t console.log(JSON.stringify(responseObject));\n\t\t\t\t\tconsole.log(\"Record was not found\");\n\t\t\t\t\tMsg.value = \"User has no training record\";\n\t\t\t\t}\n\t\t\t\t// console.log(response_ok);\n\t\t\t\t busy1.deactivate();\n\t\t\t\terror.value = true;\n\t\t\t});\n\t\t\t    \n\t\t\t    \n\t\t\t    \n\n\t\t\t}, function(error) {\n\t\t\t    //For now, let's expect the error to be because of the file not being found.\n\t\t\t    //welcomeText.value = \"There is currently no local data stored\";\n\t\t\t});\n\t\t}\n\n\t\tfunction LoadMedical() {\n\t\t\terror.value = false;\n\t\t\tbusy2.activate();\n\t\t\tStorage.read(dataPath).then(function(content) {\n\t\t\t    var cont = JSON.parse(content);\n\t\t\t    personnel_id = cont.personnel[0].personnel_id;\n\t\t\t    fetch('http://41.75.207.60/osp/public/api/medical/'+personnel_id )\n\t\t\t\t\t.then(function(response) { \n\t\t\t\t\t\tstatus = response.status;  // Get the HTTP status code\n\t\t    \t\t\tresponse_ok = response.ok; // Is response.status in the 200-range?\n\t\t\t\t\t\treturn response.json(); \n\t\t\t\t\t})\n\t\t\t\t.then(function (responseObject) {\n\t\t\t\tif(responseObject != 0)\n\t\t\t\t{\n\t\t\t\t\tmedical.replaceAll(responseObject);\n\t\t\t\t\t//console.log(\"it works: \"+JSON.stringify(responseObject));\t\n\t\t\t\t}\n\t\t\t\telse\n\t\t\t\t{\n\t\t\t\t\tMsg.value = \"User has no medical record\";\n\t\t\t\t\t \n\t\t\t\t}\n\t\t\t\tbusy2.deactivate();\n\t\t\t}).catch(function(err) {\n\t\t\t\tconsole.log(err+ \" dSome There was an error\");\n\t\t\t\tconsole.log(status)\n\t\t\t\tif (status == 502) {\n\t\t\t\t\t// console.log(\"Error Address\");\n\t\t\t\t\tMsg.value = \"Wrong Address\";\n\t\t\t\t}else{\n\t\t\t\t console.log(JSON.stringify(responseObject));\n\t\t\t\t\tconsole.log(\"Record was not found\");\n\t\t\t\t\tMsg.value = \"User has no medical record\";\n\t\t\t\t}\n\t\t\t\t// console.log(response_ok);\n\t\t\t\t busy2.deactivate();\n\t\t\t\terror.value = true;\n\t\t\t});\n\t\t\t    \n\t\t\t    \n\t\t\t    \n\n\t\t\t}, function(error) {\n\t\t\t    //For now, let's expect the error to be because of the file not being found.\n\t\t\t    //welcomeText.value = \"There is currently no local data stored\";\n\t\t\t});\n\n\t\t}\n\n\t\t\n\t\tmodule.exports={\n\t\t\tpermit: permit,\n\t\t\ttraining: training,\n\t\t\tmedical: medical,\n\t\t\tstartLoad: startLoad,\n\t\t\tLoadtraining: Loadtraining,\n\t\t\tLoadMedical: LoadMedical,\n\t\t\terror: error,\n\t\t\tMsg: Msg\n\t\t};\n\t";
        temp35.LineNumber = 4;
        temp35.FileName = "workinfo.ux";
        temp35.SourceLineNumber = 4;
        temp35.SourceFileName = "workinfo.ux";
        temp36.Rows = "1*,15*";
        temp36.SourceLineNumber = 189;
        temp36.SourceFileName = "workinfo.ux";
        temp36.Children.Add(temp37);
        temp36.Children.Add(temp38);
        temp37.Title = "WORK INFO";
        temp37.SourceLineNumber = 191;
        temp37.SourceFileName = "workinfo.ux";
        temp38.Padding = float4(0f, 20f, 0f, 0f);
        temp38.SourceLineNumber = 193;
        temp38.SourceFileName = "workinfo.ux";
        temp38.Children.Add(temp39);
        temp38.Children.Add(navigation);
        temp39.ColumnCount = 3;
        temp39.Width = new Uno.UX.Size(90f, Uno.UX.Unit.Percent);
        temp39.Height = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        temp39.SourceLineNumber = 194;
        temp39.SourceFileName = "workinfo.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp39, Fuse.Layouts.Dock.Top);
        temp39.Background = temp47;
        temp39.Children.Add(page1Tab);
        temp39.Children.Add(page2Tab);
        temp39.Children.Add(page3Tab);
        page1Tab.Name = __selector6;
        page1Tab.SourceLineNumber = 195;
        page1Tab.SourceFileName = "workinfo.ux";
        page1Tab.Children.Add(permit);
        permit.Text = "PERMIT";
        permit.Color = float4(0f, 0.1647059f, 0.2901961f, 1f);
        permit.Name = __selector7;
        permit.SourceLineNumber = 196;
        permit.SourceFileName = "workinfo.ux";
        permit.Children.Add(temp40);
        temp40.SourceLineNumber = 197;
        temp40.SourceFileName = "workinfo.ux";
        temp40.Actions.Add(temp41);
        temp41.Value = page1;
        temp41.SourceLineNumber = 198;
        temp41.SourceFileName = "workinfo.ux";
        page2Tab.Name = __selector8;
        page2Tab.SourceLineNumber = 202;
        page2Tab.SourceFileName = "workinfo.ux";
        page2Tab.Children.Add(train);
        train.Text = "TRAINING";
        train.Name = __selector9;
        train.SourceLineNumber = 203;
        train.SourceFileName = "workinfo.ux";
        train.Children.Add(temp42);
        temp42.SourceLineNumber = 204;
        temp42.SourceFileName = "workinfo.ux";
        temp42.Actions.Add(temp43);
        temp42.Actions.Add(temp44);
        temp42.Bindings.Add(temp_eb26);
        temp43.Value = page2;
        temp43.SourceLineNumber = 205;
        temp43.SourceFileName = "workinfo.ux";
        temp44.SourceLineNumber = 206;
        temp44.SourceFileName = "workinfo.ux";
        temp44.Handler += temp_eb26.OnEvent;
        temp12.SourceLineNumber = 206;
        temp12.SourceFileName = "workinfo.ux";
        page3Tab.Name = __selector10;
        page3Tab.SourceLineNumber = 210;
        page3Tab.SourceFileName = "workinfo.ux";
        page3Tab.Children.Add(medical);
        medical.Text = "MEDICAL";
        medical.Name = __selector11;
        medical.SourceLineNumber = 211;
        medical.SourceFileName = "workinfo.ux";
        medical.Children.Add(temp45);
        temp45.SourceLineNumber = 212;
        temp45.SourceFileName = "workinfo.ux";
        temp45.Actions.Add(temp46);
        temp46.Value = page3;
        temp46.SourceLineNumber = 213;
        temp46.SourceFileName = "workinfo.ux";
        navigation.Name = __selector12;
        navigation.SourceLineNumber = 219;
        navigation.SourceFileName = "workinfo.ux";
        navigation.Children.Add(page1);
        navigation.Children.Add(page2);
        navigation.Children.Add(page3);
        page1.Name = __selector13;
        page1.SourceLineNumber = 220;
        page1.SourceFileName = "workinfo.ux";
        page1.Background = temp74;
        page1.Children.Add(temp48);
        page1.Children.Add(temp51);
        page1.Children.Add(loadingPanel);
        page1.Children.Add(temp65);
        page1.Children.Add(busy);
        page1.Children.Add(WorkPage);
        temp48.Threshold = 0.5f;
        temp48.SourceLineNumber = 222;
        temp48.SourceFileName = "workinfo.ux";
        temp48.Animators.Add(temp49);
        temp48.Animators.Add(temp50);
        temp49.Value = float4(0.9686275f, 0.8980392f, 0.3686275f, 1f);
        temp50.Value = float4(0f, 0.1647059f, 0.2901961f, 1f);
        temp51.SourceLineNumber = 227;
        temp51.SourceFileName = "workinfo.ux";
        temp51.Animators.Add(temp52);
        temp51.Animators.Add(temp53);
        temp52.Value = 1f;
        temp52.Duration = 0.5;
        temp53.Value = 0f;
        loadingPanel.Color = float4(0.9686275f, 0.8980392f, 0.372549f, 1f);
        loadingPanel.Width = new Uno.UX.Size(90f, Uno.UX.Unit.Percent);
        loadingPanel.Height = new Uno.UX.Size(500f, Uno.UX.Unit.Unspecified);
        loadingPanel.Alignment = Fuse.Elements.Alignment.Center;
        loadingPanel.Opacity = 0f;
        loadingPanel.Name = __selector14;
        loadingPanel.SourceLineNumber = 232;
        loadingPanel.SourceFileName = "workinfo.ux";
        loadingPanel.Children.Add(load);
        loadingPanel.Children.Add(errorMsg);
        loadingPanel.Children.Add(temp1);
        loadingPanel.Children.Add(temp2);
        load.Width = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        load.Height = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        load.Alignment = Fuse.Elements.Alignment.Center;
        load.Opacity = 1f;
        load.Name = __selector15;
        load.SourceLineNumber = 234;
        load.SourceFileName = "workinfo.ux";
        load.File = new global::Uno.UX.BundleFileSource(import("../../../../../Assets/disk.png"));
        errorMsg.Alignment = Fuse.Elements.Alignment.Center;
        errorMsg.Opacity = 0f;
        errorMsg.Name = __selector16;
        errorMsg.SourceLineNumber = 235;
        errorMsg.SourceFileName = "workinfo.ux";
        errorMsg.Children.Add(temp);
        errorMsg.Children.Add(temp55);
        temp.TextAlignment = Fuse.Controls.TextAlignment.Center;
        temp.Color = float4(0.8f, 0f, 0f, 1f);
        temp.Alignment = Fuse.Elements.Alignment.Center;
        temp.SourceLineNumber = 236;
        temp.SourceFileName = "workinfo.ux";
        temp.Font = global::MainView.Raleway;
        temp.Bindings.Add(temp54);
        temp13.SourceLineNumber = 236;
        temp13.SourceFileName = "workinfo.ux";
        temp55.Margin = float4(0f, 10f, 0f, 10f);
        temp55.SourceLineNumber = 237;
        temp55.SourceFileName = "workinfo.ux";
        temp55.Children.Add(temp56);
        temp55.Children.Add(temp57);
        temp56.Btext = "Retry";
        temp56.Icon = "\uF021";
        temp56.IconColor = float4(0.3333333f, 1f, 0.4980392f, 1f);
        temp56.Margin = float4(10f, 0f, 10f, 0f);
        temp56.SourceLineNumber = 238;
        temp56.SourceFileName = "workinfo.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp56, Fuse.Layouts.Dock.Left);
        global::Fuse.Gestures.Clicked.AddHandler(temp56, temp_eb27.OnEvent);
        temp56.Bindings.Add(temp_eb27);
        temp14.SourceLineNumber = 238;
        temp14.SourceFileName = "workinfo.ux";
        temp57.Btext = "Cancel";
        temp57.Icon = "\uF00D";
        temp57.IconColor = float4(0.8f, 0f, 0f, 1f);
        temp57.Margin = float4(10f, 0f, 10f, 0f);
        temp57.SourceLineNumber = 239;
        temp57.SourceFileName = "workinfo.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp57, Fuse.Layouts.Dock.Left);
        global::Fuse.Gestures.Clicked.AddHandler(temp57, temp_eb28.OnEvent);
        temp57.Bindings.Add(temp_eb28);
        temp15.SourceLineNumber = 239;
        temp15.SourceFileName = "workinfo.ux";
        temp1.SourceLineNumber = 245;
        temp1.SourceFileName = "workinfo.ux";
        temp1.Animators.Add(temp58);
        temp1.Animators.Add(temp59);
        temp1.Bindings.Add(temp60);
        temp58.Value = 0f;
        temp58.Duration = 0.5;
        temp59.Value = 1f;
        temp59.Duration = 0.5;
        temp16.SourceLineNumber = 245;
        temp16.SourceFileName = "workinfo.ux";
        temp2.SourceLineNumber = 250;
        temp2.SourceFileName = "workinfo.ux";
        temp2.Animators.Add(temp61);
        temp2.Animators.Add(temp62);
        temp2.Animators.Add(temp63);
        temp2.Bindings.Add(temp64);
        temp61.Value = 1f;
        temp61.Duration = 0.5;
        temp62.Frequency = 1;
        temp62.Target = load;
        temp63.Value = 0f;
        temp63.Duration = 0.5;
        temp17.SourceLineNumber = 250;
        temp17.SourceFileName = "workinfo.ux";
        temp65.SourceLineNumber = 263;
        temp65.SourceFileName = "workinfo.ux";
        temp65.Handler += temp_eb29.OnEvent;
        temp65.Bindings.Add(temp_eb29);
        temp18.SourceLineNumber = 263;
        temp18.SourceFileName = "workinfo.ux";
        busy.IsActive = false;
        busy.Name = __selector17;
        busy.SourceLineNumber = 265;
        busy.SourceFileName = "workinfo.ux";
        WorkPage.Opacity = 1f;
        WorkPage.Name = __selector18;
        WorkPage.SourceLineNumber = 267;
        WorkPage.SourceFileName = "workinfo.ux";
        WorkPage.Children.Add(temp66);
        temp66.Margin = float4(0f, 20f, 0f, 20f);
        temp66.SourceLineNumber = 268;
        temp66.SourceFileName = "workinfo.ux";
        temp66.Children.Add(temp3);
        temp3.SourceLineNumber = 269;
        temp3.SourceFileName = "workinfo.ux";
        temp3.Templates.Add(temp67);
        temp3.Templates.Add(temp68);
        temp3.Templates.Add(temp69);
        temp3.Templates.Add(temp70);
        temp3.Templates.Add(temp71);
        temp3.Templates.Add(temp72);
        temp3.Bindings.Add(temp73);
        temp19.SourceLineNumber = 269;
        temp19.SourceFileName = "workinfo.ux";
        page2.Name = __selector19;
        page2.SourceLineNumber = 285;
        page2.SourceFileName = "workinfo.ux";
        page2.Background = temp102;
        page2.Children.Add(temp75);
        page2.Children.Add(temp78);
        page2.Children.Add(temp80);
        page2.Children.Add(loadingPanel1);
        page2.Children.Add(temp94);
        page2.Children.Add(busy1);
        page2.Children.Add(WorkPage1);
        temp75.Threshold = 0.5f;
        temp75.SourceLineNumber = 289;
        temp75.SourceFileName = "workinfo.ux";
        temp75.Animators.Add(temp76);
        temp75.Animators.Add(temp77);
        temp76.Value = float4(0.9686275f, 0.8980392f, 0.3686275f, 1f);
        temp77.Value = float4(0f, 0.1647059f, 0.2901961f, 1f);
        temp78.SourceLineNumber = 293;
        temp78.SourceFileName = "workinfo.ux";
        temp78.Animators.Add(temp79);
        temp79.Value = float4(0f, 0.1647059f, 0.2901961f, 1f);
        temp80.SourceLineNumber = 297;
        temp80.SourceFileName = "workinfo.ux";
        temp80.Animators.Add(temp81);
        temp80.Animators.Add(temp82);
        temp81.Value = 1f;
        temp81.Duration = 0.5;
        temp82.Value = 0f;
        loadingPanel1.Color = float4(0.9686275f, 0.8980392f, 0.372549f, 1f);
        loadingPanel1.Width = new Uno.UX.Size(90f, Uno.UX.Unit.Percent);
        loadingPanel1.Height = new Uno.UX.Size(500f, Uno.UX.Unit.Unspecified);
        loadingPanel1.Alignment = Fuse.Elements.Alignment.Center;
        loadingPanel1.Opacity = 0f;
        loadingPanel1.Name = __selector20;
        loadingPanel1.SourceLineNumber = 302;
        loadingPanel1.SourceFileName = "workinfo.ux";
        loadingPanel1.Children.Add(load1);
        loadingPanel1.Children.Add(errorMsg1);
        loadingPanel1.Children.Add(temp5);
        loadingPanel1.Children.Add(temp6);
        load1.Width = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        load1.Height = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        load1.Alignment = Fuse.Elements.Alignment.Center;
        load1.Opacity = 1f;
        load1.Name = __selector21;
        load1.SourceLineNumber = 304;
        load1.SourceFileName = "workinfo.ux";
        load1.File = new global::Uno.UX.BundleFileSource(import("../../../../../Assets/disk.png"));
        errorMsg1.Alignment = Fuse.Elements.Alignment.Center;
        errorMsg1.Opacity = 0f;
        errorMsg1.Name = __selector22;
        errorMsg1.SourceLineNumber = 305;
        errorMsg1.SourceFileName = "workinfo.ux";
        errorMsg1.Children.Add(temp4);
        errorMsg1.Children.Add(temp84);
        temp4.TextAlignment = Fuse.Controls.TextAlignment.Center;
        temp4.Color = float4(0.8f, 0f, 0f, 1f);
        temp4.Alignment = Fuse.Elements.Alignment.Center;
        temp4.SourceLineNumber = 306;
        temp4.SourceFileName = "workinfo.ux";
        temp4.Font = global::MainView.Raleway;
        temp4.Bindings.Add(temp83);
        temp20.SourceLineNumber = 306;
        temp20.SourceFileName = "workinfo.ux";
        temp84.Margin = float4(0f, 10f, 0f, 10f);
        temp84.SourceLineNumber = 307;
        temp84.SourceFileName = "workinfo.ux";
        temp84.Children.Add(temp85);
        temp84.Children.Add(temp86);
        temp85.Btext = "Retry";
        temp85.Icon = "\uF021";
        temp85.IconColor = float4(0.3333333f, 1f, 0.4980392f, 1f);
        temp85.Margin = float4(10f, 0f, 10f, 0f);
        temp85.SourceLineNumber = 308;
        temp85.SourceFileName = "workinfo.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp85, Fuse.Layouts.Dock.Left);
        global::Fuse.Gestures.Clicked.AddHandler(temp85, temp_eb30.OnEvent);
        temp85.Bindings.Add(temp_eb30);
        temp21.SourceLineNumber = 308;
        temp21.SourceFileName = "workinfo.ux";
        temp86.Btext = "Cancel";
        temp86.Icon = "\uF00D";
        temp86.IconColor = float4(0.8f, 0f, 0f, 1f);
        temp86.Margin = float4(10f, 0f, 10f, 0f);
        temp86.SourceLineNumber = 309;
        temp86.SourceFileName = "workinfo.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp86, Fuse.Layouts.Dock.Left);
        global::Fuse.Gestures.Clicked.AddHandler(temp86, temp_eb31.OnEvent);
        temp86.Bindings.Add(temp_eb31);
        temp22.SourceLineNumber = 309;
        temp22.SourceFileName = "workinfo.ux";
        temp5.SourceLineNumber = 315;
        temp5.SourceFileName = "workinfo.ux";
        temp5.Animators.Add(temp87);
        temp5.Animators.Add(temp88);
        temp5.Bindings.Add(temp89);
        temp87.Value = 0f;
        temp87.Duration = 0.5;
        temp88.Value = 1f;
        temp88.Duration = 0.5;
        temp23.SourceLineNumber = 315;
        temp23.SourceFileName = "workinfo.ux";
        temp6.SourceLineNumber = 320;
        temp6.SourceFileName = "workinfo.ux";
        temp6.Animators.Add(temp90);
        temp6.Animators.Add(temp91);
        temp6.Animators.Add(temp92);
        temp6.Bindings.Add(temp93);
        temp90.Value = 1f;
        temp90.Duration = 0.5;
        temp91.Frequency = 1;
        temp91.Target = load1;
        temp92.Value = 0f;
        temp92.Duration = 0.5;
        temp24.SourceLineNumber = 320;
        temp24.SourceFileName = "workinfo.ux";
        temp94.SourceLineNumber = 333;
        temp94.SourceFileName = "workinfo.ux";
        temp94.Handler += temp_eb32.OnEvent;
        temp94.Bindings.Add(temp_eb32);
        temp25.SourceLineNumber = 333;
        temp25.SourceFileName = "workinfo.ux";
        busy1.IsActive = false;
        busy1.Name = __selector23;
        busy1.SourceLineNumber = 335;
        busy1.SourceFileName = "workinfo.ux";
        WorkPage1.Opacity = 1f;
        WorkPage1.Name = __selector24;
        WorkPage1.SourceLineNumber = 337;
        WorkPage1.SourceFileName = "workinfo.ux";
        WorkPage1.Children.Add(temp95);
        temp95.Margin = float4(0f, 20f, 0f, 20f);
        temp95.SourceLineNumber = 338;
        temp95.SourceFileName = "workinfo.ux";
        temp95.Children.Add(temp7);
        temp7.SourceLineNumber = 339;
        temp7.SourceFileName = "workinfo.ux";
        temp7.Templates.Add(temp96);
        temp7.Templates.Add(temp97);
        temp7.Templates.Add(temp98);
        temp7.Templates.Add(temp99);
        temp7.Templates.Add(temp100);
        temp7.Bindings.Add(temp101);
        temp26.SourceLineNumber = 339;
        temp26.SourceFileName = "workinfo.ux";
        page3.Name = __selector25;
        page3.SourceLineNumber = 353;
        page3.SourceFileName = "workinfo.ux";
        page3.Background = temp130;
        page3.Children.Add(temp103);
        page3.Children.Add(temp106);
        page3.Children.Add(temp108);
        page3.Children.Add(loadingPanel2);
        page3.Children.Add(temp122);
        page3.Children.Add(busy2);
        page3.Children.Add(WorkPage2);
        temp103.Threshold = 0.5f;
        temp103.SourceLineNumber = 354;
        temp103.SourceFileName = "workinfo.ux";
        temp103.Animators.Add(temp104);
        temp103.Animators.Add(temp105);
        temp104.Value = float4(0.9686275f, 0.8980392f, 0.3686275f, 1f);
        temp105.Value = float4(0f, 0.1647059f, 0.2901961f, 1f);
        temp106.SourceLineNumber = 358;
        temp106.SourceFileName = "workinfo.ux";
        temp106.Animators.Add(temp107);
        temp107.Value = float4(0f, 0.1647059f, 0.2901961f, 1f);
        temp108.SourceLineNumber = 365;
        temp108.SourceFileName = "workinfo.ux";
        temp108.Animators.Add(temp109);
        temp108.Animators.Add(temp110);
        temp109.Value = 1f;
        temp109.Duration = 0.5;
        temp110.Value = 0f;
        loadingPanel2.Color = float4(0.9686275f, 0.8980392f, 0.372549f, 1f);
        loadingPanel2.Width = new Uno.UX.Size(90f, Uno.UX.Unit.Percent);
        loadingPanel2.Height = new Uno.UX.Size(500f, Uno.UX.Unit.Unspecified);
        loadingPanel2.Alignment = Fuse.Elements.Alignment.Center;
        loadingPanel2.Opacity = 0f;
        loadingPanel2.Name = __selector26;
        loadingPanel2.SourceLineNumber = 370;
        loadingPanel2.SourceFileName = "workinfo.ux";
        loadingPanel2.Children.Add(load2);
        loadingPanel2.Children.Add(errorMsg2);
        loadingPanel2.Children.Add(temp9);
        loadingPanel2.Children.Add(temp10);
        load2.Width = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        load2.Height = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        load2.Alignment = Fuse.Elements.Alignment.Center;
        load2.Opacity = 1f;
        load2.Name = __selector27;
        load2.SourceLineNumber = 372;
        load2.SourceFileName = "workinfo.ux";
        load2.File = new global::Uno.UX.BundleFileSource(import("../../../../../Assets/disk.png"));
        errorMsg2.Alignment = Fuse.Elements.Alignment.Center;
        errorMsg2.Opacity = 0f;
        errorMsg2.Name = __selector28;
        errorMsg2.SourceLineNumber = 373;
        errorMsg2.SourceFileName = "workinfo.ux";
        errorMsg2.Children.Add(temp8);
        errorMsg2.Children.Add(temp112);
        temp8.TextAlignment = Fuse.Controls.TextAlignment.Center;
        temp8.Color = float4(0.8f, 0f, 0f, 1f);
        temp8.Alignment = Fuse.Elements.Alignment.Center;
        temp8.SourceLineNumber = 374;
        temp8.SourceFileName = "workinfo.ux";
        temp8.Font = global::MainView.Raleway;
        temp8.Bindings.Add(temp111);
        temp27.SourceLineNumber = 374;
        temp27.SourceFileName = "workinfo.ux";
        temp112.Margin = float4(0f, 10f, 0f, 10f);
        temp112.SourceLineNumber = 375;
        temp112.SourceFileName = "workinfo.ux";
        temp112.Children.Add(temp113);
        temp112.Children.Add(temp114);
        temp113.Btext = "Retry";
        temp113.Icon = "\uF021";
        temp113.IconColor = float4(0.3333333f, 1f, 0.4980392f, 1f);
        temp113.Margin = float4(10f, 0f, 10f, 0f);
        temp113.SourceLineNumber = 376;
        temp113.SourceFileName = "workinfo.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp113, Fuse.Layouts.Dock.Left);
        global::Fuse.Gestures.Clicked.AddHandler(temp113, temp_eb33.OnEvent);
        temp113.Bindings.Add(temp_eb33);
        temp28.SourceLineNumber = 376;
        temp28.SourceFileName = "workinfo.ux";
        temp114.Btext = "Cancel";
        temp114.Icon = "\uF00D";
        temp114.IconColor = float4(0.8f, 0f, 0f, 1f);
        temp114.Margin = float4(10f, 0f, 10f, 0f);
        temp114.SourceLineNumber = 377;
        temp114.SourceFileName = "workinfo.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp114, Fuse.Layouts.Dock.Left);
        global::Fuse.Gestures.Clicked.AddHandler(temp114, temp_eb34.OnEvent);
        temp114.Bindings.Add(temp_eb34);
        temp29.SourceLineNumber = 377;
        temp29.SourceFileName = "workinfo.ux";
        temp9.SourceLineNumber = 383;
        temp9.SourceFileName = "workinfo.ux";
        temp9.Animators.Add(temp115);
        temp9.Animators.Add(temp116);
        temp9.Bindings.Add(temp117);
        temp115.Value = 0f;
        temp115.Duration = 0.5;
        temp116.Value = 1f;
        temp116.Duration = 0.5;
        temp30.SourceLineNumber = 383;
        temp30.SourceFileName = "workinfo.ux";
        temp10.SourceLineNumber = 388;
        temp10.SourceFileName = "workinfo.ux";
        temp10.Animators.Add(temp118);
        temp10.Animators.Add(temp119);
        temp10.Animators.Add(temp120);
        temp10.Bindings.Add(temp121);
        temp118.Value = 1f;
        temp118.Duration = 0.5;
        temp119.Frequency = 1;
        temp119.Target = load1;
        temp120.Value = 0f;
        temp120.Duration = 0.5;
        temp31.SourceLineNumber = 388;
        temp31.SourceFileName = "workinfo.ux";
        temp122.SourceLineNumber = 401;
        temp122.SourceFileName = "workinfo.ux";
        temp122.Handler += temp_eb35.OnEvent;
        temp122.Bindings.Add(temp_eb35);
        temp32.SourceLineNumber = 401;
        temp32.SourceFileName = "workinfo.ux";
        busy2.IsActive = false;
        busy2.Name = __selector29;
        busy2.SourceLineNumber = 403;
        busy2.SourceFileName = "workinfo.ux";
        WorkPage2.Opacity = 1f;
        WorkPage2.Name = __selector30;
        WorkPage2.SourceLineNumber = 405;
        WorkPage2.SourceFileName = "workinfo.ux";
        WorkPage2.Children.Add(temp123);
        temp123.Margin = float4(0f, 20f, 0f, 20f);
        temp123.SourceLineNumber = 406;
        temp123.SourceFileName = "workinfo.ux";
        temp123.Children.Add(temp11);
        temp11.SourceLineNumber = 407;
        temp11.SourceFileName = "workinfo.ux";
        temp11.Templates.Add(temp124);
        temp11.Templates.Add(temp125);
        temp11.Templates.Add(temp126);
        temp11.Templates.Add(temp127);
        temp11.Templates.Add(temp128);
        temp11.Bindings.Add(temp129);
        temp33.SourceLineNumber = 407;
        temp33.SourceFileName = "workinfo.ux";
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(page1Tab);
        __g_nametable.Objects.Add(permit);
        __g_nametable.Objects.Add(page2Tab);
        __g_nametable.Objects.Add(train);
        __g_nametable.Objects.Add(temp_eb26);
        __g_nametable.Objects.Add(page3Tab);
        __g_nametable.Objects.Add(medical);
        __g_nametable.Objects.Add(navigation);
        __g_nametable.Objects.Add(page1);
        __g_nametable.Objects.Add(loadingPanel);
        __g_nametable.Objects.Add(load);
        __g_nametable.Objects.Add(errorMsg);
        __g_nametable.Objects.Add(temp_eb27);
        __g_nametable.Objects.Add(temp_eb28);
        __g_nametable.Objects.Add(temp_eb29);
        __g_nametable.Objects.Add(busy);
        __g_nametable.Objects.Add(WorkPage);
        __g_nametable.Objects.Add(page2);
        __g_nametable.Objects.Add(loadingPanel1);
        __g_nametable.Objects.Add(load1);
        __g_nametable.Objects.Add(errorMsg1);
        __g_nametable.Objects.Add(temp_eb30);
        __g_nametable.Objects.Add(temp_eb31);
        __g_nametable.Objects.Add(temp_eb32);
        __g_nametable.Objects.Add(busy1);
        __g_nametable.Objects.Add(WorkPage1);
        __g_nametable.Objects.Add(page3);
        __g_nametable.Objects.Add(loadingPanel2);
        __g_nametable.Objects.Add(load2);
        __g_nametable.Objects.Add(errorMsg2);
        __g_nametable.Objects.Add(temp_eb33);
        __g_nametable.Objects.Add(temp_eb34);
        __g_nametable.Objects.Add(temp_eb35);
        __g_nametable.Objects.Add(busy2);
        __g_nametable.Objects.Add(WorkPage2);
        this.Children.Add(temp34);
        this.Children.Add(temp35);
        this.Children.Add(temp36);
    }
    static global::Uno.UX.Selector __selector0 = "Active";
    static global::Uno.UX.Selector __selector1 = "Color";
    static global::Uno.UX.Selector __selector2 = "TColor";
    static global::Uno.UX.Selector __selector3 = "Opacity";
    static global::Uno.UX.Selector __selector4 = "Value";
    static global::Uno.UX.Selector __selector5 = "Items";
    static global::Uno.UX.Selector __selector6 = "page1Tab";
    static global::Uno.UX.Selector __selector7 = "permit";
    static global::Uno.UX.Selector __selector8 = "page2Tab";
    static global::Uno.UX.Selector __selector9 = "train";
    static global::Uno.UX.Selector __selector10 = "page3Tab";
    static global::Uno.UX.Selector __selector11 = "medical";
    static global::Uno.UX.Selector __selector12 = "navigation";
    static global::Uno.UX.Selector __selector13 = "page1";
    static global::Uno.UX.Selector __selector14 = "loadingPanel";
    static global::Uno.UX.Selector __selector15 = "load";
    static global::Uno.UX.Selector __selector16 = "errorMsg";
    static global::Uno.UX.Selector __selector17 = "busy";
    static global::Uno.UX.Selector __selector18 = "WorkPage";
    static global::Uno.UX.Selector __selector19 = "page2";
    static global::Uno.UX.Selector __selector20 = "loadingPanel1";
    static global::Uno.UX.Selector __selector21 = "load1";
    static global::Uno.UX.Selector __selector22 = "errorMsg1";
    static global::Uno.UX.Selector __selector23 = "busy1";
    static global::Uno.UX.Selector __selector24 = "WorkPage1";
    static global::Uno.UX.Selector __selector25 = "page3";
    static global::Uno.UX.Selector __selector26 = "loadingPanel2";
    static global::Uno.UX.Selector __selector27 = "load2";
    static global::Uno.UX.Selector __selector28 = "errorMsg2";
    static global::Uno.UX.Selector __selector29 = "busy2";
    static global::Uno.UX.Selector __selector30 = "WorkPage2";
}
