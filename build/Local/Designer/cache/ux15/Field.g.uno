[Uno.Compiler.UxGenerated]
public partial class Field: Fuse.Controls.DockPanel
{
    string _field_PlaceHolderName;
    [global::Uno.UX.UXOriginSetter("SetPlaceHolderName")]
    public string PlaceHolderName
    {
        get { return _field_PlaceHolderName; }
        set { SetPlaceHolderName(value, null); }
    }
    public void SetPlaceHolderName(string value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_PlaceHolderName)
        {
            _field_PlaceHolderName = value;
            OnPropertyChanged("PlaceHolderName", origin);
        }
    }
    string _field_Title;
    [global::Uno.UX.UXOriginSetter("SetTitle")]
    public string Title
    {
        get { return _field_Title; }
        set { SetTitle(value, null); }
    }
    public void SetTitle(string value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_Title)
        {
            _field_Title = value;
            OnPropertyChanged("Title", origin);
        }
    }
    float4 _field_FieldColor;
    [global::Uno.UX.UXOriginSetter("SetFieldColor")]
    public float4 FieldColor
    {
        get { return _field_FieldColor; }
        set { SetFieldColor(value, null); }
    }
    public void SetFieldColor(float4 value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_FieldColor)
        {
            _field_FieldColor = value;
            OnPropertyChanged("FieldColor", origin);
        }
    }
    global::Uno.UX.Property<string> temp_Value_inst;
    global::Uno.UX.Property<float4> temp_TextColor_inst;
    global::Uno.UX.Property<string> temp1_Value_inst;
    global::Uno.UX.Property<float4> temp1_TextColor_inst;
    static Field()
    {
    }
    [global::Uno.UX.UXConstructor]
    public Field()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp2 = new global::Fuse.Reactive.Constant(this);
        var temp = new global::Fuse.Controls.Text();
        temp_Value_inst = new OSP_FuseControlsTextControl_Value_Property(temp, __selector0);
        var temp3 = new global::Fuse.Reactive.Property(temp2, OSP_accessor_Field_Title.Singleton);
        var temp4 = new global::Fuse.Reactive.Constant(this);
        temp_TextColor_inst = new OSP_FuseControlsTextControl_TextColor_Property(temp, __selector1);
        var temp5 = new global::Fuse.Reactive.Property(temp4, OSP_accessor_Field_FieldColor.Singleton);
        var temp6 = new global::Fuse.Reactive.Constant(this);
        var temp1 = new global::Fuse.Controls.Text();
        temp1_Value_inst = new OSP_FuseControlsTextControl_Value_Property(temp1, __selector0);
        var temp7 = new global::Fuse.Reactive.Property(temp6, OSP_accessor_Field_PlaceHolderName.Singleton);
        var temp8 = new global::Fuse.Reactive.Constant(this);
        temp1_TextColor_inst = new OSP_FuseControlsTextControl_TextColor_Property(temp1, __selector1);
        var temp9 = new global::Fuse.Reactive.Property(temp8, OSP_accessor_Field_FieldColor.Singleton);
        var temp10 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp3, Fuse.Reactive.BindingMode.Default);
        var temp11 = new global::Fuse.Reactive.DataBinding(temp_TextColor_inst, temp5, Fuse.Reactive.BindingMode.Default);
        var temp12 = new global::Fuse.Reactive.DataBinding(temp1_Value_inst, temp7, Fuse.Reactive.BindingMode.Default);
        var temp13 = new global::Fuse.Reactive.DataBinding(temp1_TextColor_inst, temp9, Fuse.Reactive.BindingMode.Default);
        this.FieldColor = float4(0f, 0f, 0f, 1f);
        this.SourceLineNumber = 84;
        this.SourceFileName = "history.ux";
        temp.TextWrapping = Fuse.Controls.TextWrapping.Wrap;
        temp.FontSize = 11f;
        temp.Width = new Uno.UX.Size(30f, Uno.UX.Unit.Percent);
        temp.Alignment = Fuse.Elements.Alignment.VerticalCenter;
        temp.SourceLineNumber = 88;
        temp.SourceFileName = "history.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp, Fuse.Layouts.Dock.Left);
        temp.Font = global::MainView.Raleway;
        temp.Bindings.Add(temp10);
        temp.Bindings.Add(temp11);
        temp3.SourceLineNumber = 88;
        temp3.SourceFileName = "history.ux";
        temp2.SourceLineNumber = 88;
        temp2.SourceFileName = "history.ux";
        temp5.SourceLineNumber = 88;
        temp5.SourceFileName = "history.ux";
        temp4.SourceLineNumber = 88;
        temp4.SourceFileName = "history.ux";
        temp1.TextWrapping = Fuse.Controls.TextWrapping.Wrap;
        temp1.FontSize = 11f;
        temp1.Width = new Uno.UX.Size(60f, Uno.UX.Unit.Percent);
        temp1.Alignment = Fuse.Elements.Alignment.VerticalCenter;
        temp1.Margin = float4(20f, 0f, 0f, 0f);
        temp1.SourceLineNumber = 90;
        temp1.SourceFileName = "history.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp1, Fuse.Layouts.Dock.Left);
        temp1.Font = global::MainView.Raleway;
        temp1.Bindings.Add(temp12);
        temp1.Bindings.Add(temp13);
        temp7.SourceLineNumber = 90;
        temp7.SourceFileName = "history.ux";
        temp6.SourceLineNumber = 90;
        temp6.SourceFileName = "history.ux";
        temp9.SourceLineNumber = 90;
        temp9.SourceFileName = "history.ux";
        temp8.SourceLineNumber = 90;
        temp8.SourceFileName = "history.ux";
        this.Children.Add(temp);
        this.Children.Add(temp1);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
    static global::Uno.UX.Selector __selector1 = "TextColor";
}
