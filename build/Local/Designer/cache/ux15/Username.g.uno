[Uno.Compiler.UxGenerated]
public partial class Username: Fuse.Controls.Text
{
    static Username()
    {
    }
    [global::Uno.UX.UXConstructor]
    public Username()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp = new global::Fuse.Font(new global::Uno.UX.BundleFileSource(import("../../../../../Assets/fonts/AlegreyaSans-Bold.otf")));
        this.FontSize = 13f;
        this.TextAlignment = Fuse.Controls.TextAlignment.Center;
        this.Color = float4(0.2f, 0.2f, 0.2f, 1f);
        this.Margin = float4(0f, 8f, 0f, 0f);
        this.SourceLineNumber = 12;
        this.SourceFileName = "Sidebar.ux";
        this.Font = temp;
    }
}
